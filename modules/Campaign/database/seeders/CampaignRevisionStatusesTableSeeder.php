<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignRevisionStatus;

class CampaignRevisionStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $statuses = [
            [
                'id'   => CampaignRevisionStatus::ID_DRAFT,
                'name' => CampaignRevisionStatus::DRAFT,
            ],
            [
                'id'   => CampaignRevisionStatus::ID_LIVE,
                'name' => CampaignRevisionStatus::LIVE,
            ],
            [
                'id'   => CampaignRevisionStatus::ID_SUBMITTED,
                'name' => CampaignRevisionStatus::SUBMITTED,
            ],
            [
                'id'   => CampaignRevisionStatus::ID_PROCESSING,
                'name' => CampaignRevisionStatus::PROCESSING,
            ],
        ];

        foreach ($statuses as $status) {
            CampaignRevisionStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
