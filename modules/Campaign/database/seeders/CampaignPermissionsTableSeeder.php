<?php

namespace Modules\Campaign\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Campaign\Policies\CampaignPolicy;
use Modules\User\Models\Role;

class CampaignPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [
            CampaignPolicy::PERMISSION_LIST_CAMPAIGN,
            CampaignPolicy::PERMISSION_LIST_ADVERTISER_CAMPAIGN,
            CampaignPolicy::PERMISSION_VIEW_CAMPAIGN,
            CampaignPolicy::PERMISSION_CANCEL_CAMPAIGN,
            CampaignPolicy::PERMISSION_PAUSE_CAMPAIGN,
            CampaignPolicy::PERMISSION_RESUME_CAMPAIGN,
        ],
        Role::ID_ADMIN_READ_ONLY => [
            CampaignPolicy::PERMISSION_LIST_CAMPAIGN,
            CampaignPolicy::PERMISSION_LIST_ADVERTISER_CAMPAIGN,
            CampaignPolicy::PERMISSION_VIEW_CAMPAIGN,
        ],
        Role::ID_ADVERTISER      => [
            CampaignPolicy::PERMISSION_CREATE_CAMPAIGN,
            CampaignPolicy::PERMISSION_DELETE_CAMPAIGN,
            CampaignPolicy::PERMISSION_LIST_CAMPAIGN,
            CampaignPolicy::PERMISSION_UPDATE_CAMPAIGN,
            CampaignPolicy::PERMISSION_VIEW_CAMPAIGN,
            CampaignPolicy::PERMISSION_CANCEL_CAMPAIGN,
            CampaignPolicy::PERMISSION_PAUSE_CAMPAIGN,
            CampaignPolicy::PERMISSION_RESUME_CAMPAIGN,
            CampaignPolicy::PERMISSION_MANAGE_CAMPAIGN_CREATIVE,
            CampaignPolicy::PERMISSION_CLONE_CAMPAIGN,
        ],
    ];
}
