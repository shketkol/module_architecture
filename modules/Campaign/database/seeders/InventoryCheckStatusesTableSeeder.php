<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\InventoryCheckStatus;

class InventoryCheckStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $steps = [
            [
                'id'   => InventoryCheckStatus::ID_AVAILABLE,
                'name' => InventoryCheckStatus::AVAILABLE,
            ],
            [
                'id'   => InventoryCheckStatus::ID_YX_CONNECTION_FAILED,
                'name' => InventoryCheckStatus::YX_CONNECTION_FAILED,
            ],
            [
                'id'   => InventoryCheckStatus::ID_UNAVAILABLE,
                'name' => InventoryCheckStatus::UNAVAILABLE,
            ],
            [
                'id'   => InventoryCheckStatus::ID_AVAILABILITY_UNDER_MIN_SPEND,
                'name' => InventoryCheckStatus::AVAILABILITY_UNDER_MIN_SPEND,
            ],
            [
                'id'   => InventoryCheckStatus::ID_REQUEST_UNDER_MIN_PER_DAY,
                'name' => InventoryCheckStatus::REQUEST_UNDER_MIN_PER_DAY,
            ],
            [
                'id'   => InventoryCheckStatus::ID_INTERNAL_IN_PROGRESS,
                'name' => InventoryCheckStatus::INTERNAL_IN_PROGRESS,
            ],
            [
                'id'   => InventoryCheckStatus::ID_INTERNAL_FAILED,
                'name' => InventoryCheckStatus::INTERNAL_FAILED,
            ],
        ];

        foreach ($steps as $step) {
            InventoryCheckStatus::updateOrCreate(
                ['id' => Arr::get($step, 'id')],
                $step
            );
        }
    }
}
