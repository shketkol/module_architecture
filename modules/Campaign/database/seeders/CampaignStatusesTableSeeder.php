<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStatus;

class CampaignStatusesTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $statuses = [
            [
                'id'   => CampaignStatus::ID_DRAFT,
                'name' => CampaignStatus::DRAFT,
            ],
            [
                'id'   => CampaignStatus::ID_PENDING_APPROVAL,
                'name' => CampaignStatus::PENDING_APPROVAL,
            ],
            [
                'id'   => CampaignStatus::ID_READY,
                'name' => CampaignStatus::READY,
            ],
            [
                'id'   => CampaignStatus::ID_LIVE,
                'name' => CampaignStatus::LIVE,
            ],
            [
                'id'   => CampaignStatus::ID_PAUSED,
                'name' => CampaignStatus::PAUSED,
            ],
            [
                'id'   => CampaignStatus::ID_COMPLETED,
                'name' => CampaignStatus::COMPLETED,
            ],
            [
                'id'   => CampaignStatus::ID_CANCELED,
                'name' => CampaignStatus::CANCELED,
            ],
            [
                'id'   => CampaignStatus::ID_PROCESSING,
                'name' => CampaignStatus::PROCESSING,
            ],
            [
                'id'   => CampaignStatus::ID_SUSPENDED,
                'name' => CampaignStatus::SUSPENDED,
            ],
        ];

        foreach ($statuses as $status) {
            CampaignStatus::updateOrCreate(
                ['id' => Arr::get($status, 'id')],
                $status
            );
        }
    }
}
