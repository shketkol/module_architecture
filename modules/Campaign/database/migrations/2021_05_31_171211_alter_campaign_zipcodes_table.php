<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignZipcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_zipcodes', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);
            $table->dropIndex('campaign_zipcodes_campaign_id_foreign');

            $table->dropForeign(['zipcode_id']);
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_zipcodes');

            if ($doctrineTable->hasIndex('campaign_zipcodes_zipcode_id_index')) {
                $table->dropIndex('campaign_zipcodes_zipcode_id_index');
            }

            $table->string('campaign_type')->nullable();

            $table->dropPrimary(['zipcode_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_zipcodes', function (Blueprint $table) {
            $table->primary([
                'zipcode_id',
                'campaign_id',
                'excluded',
                'campaign_type',
            ], 'campaign_zipcodes_pk');

            $table->index(['zipcode_id']);
            $table->foreign('zipcode_id')
                ->references('id')
                ->on('zipcodes')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_zipcodes')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_zipcodes', function (Blueprint $table) {
            $table->dropForeign(['zipcode_id']);
            $table->dropIndex(['zipcode_id']);

            $table->dropPrimary(['zipcode_id', 'campaign_id', 'campaign_type']);
            $table->primary(['zipcode_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['zipcode_id']);
            $table->foreign('zipcode_id')
                ->references('id')
                ->on('zipcodes')
                ->onDelete('cascade');
        });
    }
}
