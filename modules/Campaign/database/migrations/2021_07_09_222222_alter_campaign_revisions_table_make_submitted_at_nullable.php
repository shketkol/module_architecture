<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCampaignRevisionsTableMakeSubmittedAtNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if (Schema::hasColumn('campaign_revisions', 'submitted_at')) {
            Schema::table('campaign_revisions', function (Blueprint $table) {
                $table->dateTime('submitted_at')->default(null)->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        if (Schema::hasColumn('campaign_revisions', 'submitted_at')) {
            Schema::table('campaign_revisions', function (Blueprint $table) {
                $table->dateTime('submitted_at')->nullable(false)->change();
            });
        }
    }
}
