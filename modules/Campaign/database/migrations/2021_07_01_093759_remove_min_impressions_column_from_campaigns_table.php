<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveMinImpressionsColumnFromCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('min_impressions');
        });

        Schema::table('inventory_check_history', function (Blueprint $table) {
            $table->dropColumn('min_daily_impressions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        if (!Schema::hasColumn('campaigns', 'min_impressions')) {
            Schema::table('campaigns', function (Blueprint $table) {
                $table->bigInteger('min_impressions')->after('impressions')->nullable();
            });
        }

        if (!Schema::hasColumn('inventory_check_history', 'min_daily_impressions')) {
            Schema::table('inventory_check_history', function (Blueprint $table) {
                $table->bigInteger('min_daily_impressions')->after('available_impressions')->nullable();
            });
        }
    }
}
