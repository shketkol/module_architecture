<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStep;

class AddDeviceStepToCampaignTable extends Migration
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->databaseManager = app(DatabaseManager::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function up()
    {
        $this->databaseManager->beginTransaction();

        try {
            DB::table('campaigns')
              ->where('step_id', '>', CampaignStep::ID_DEVICES)
              ->update(['step_id' => CampaignStep::ID_DEVICES]);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        $this->databaseManager->beginTransaction();

        try {
            DB::table('campaigns')
              ->where('step_id', '>', CampaignStep::ID_DEVICES)
              ->decrement('step_id');
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }
}
