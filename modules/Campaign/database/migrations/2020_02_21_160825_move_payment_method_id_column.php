<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MovePaymentMethodIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->bigInteger('payment_method_id')->unsigned()->nullable();
        });

        Schema::table('campaigns', function (Blueprint $table) {
            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropForeign(['payment_method_id']);
        });

        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('payment_method_id');
        });
    }
}
