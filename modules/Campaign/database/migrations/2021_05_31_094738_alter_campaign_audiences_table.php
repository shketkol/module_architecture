<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignAudiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_audiences', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);
            $table->dropIndex('campaign_audiences_campaign_id_foreign');

            $table->dropForeign(['audience_id']);
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_audiences');

            if ($doctrineTable->hasIndex('campaign_audiences_audience_id_index')) {
                $table->dropIndex('campaign_audiences_audience_id_index');
            }

            $table->string('campaign_type')->nullable();

            $table->dropPrimary(['audience_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_audiences', function (Blueprint $table) {
            $table->primary(['audience_id', 'campaign_id', 'campaign_type'], 'campaign_audiences_pk');

            $table->index(['audience_id']);
            $table->foreign('audience_id')
                ->references('id')
                ->on('audiences')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_audiences')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_audiences', function (Blueprint $table) {
            $table->dropForeign(['audience_id']);
            $table->dropIndex(['audience_id']);

            $table->dropPrimary(['audience_id', 'campaign_id', 'campaign_type']);
            $table->primary(['audience_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['audience_id']);
            $table->foreign('audience_id')
                ->references('id')
                ->on('audiences')
                ->onDelete('cascade');
        });
    }
}
