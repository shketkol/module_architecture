<?php

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Migrations\Migration;
use Modules\Campaign\Models\CampaignStatus;

class UpdateStepIdInDraftCampaigns extends Migration
{
    const OLD_STEP_ID_AGES = 3;
    const OLD_STEP_ID_AUDIENCES = 4;
    const OLD_STEP_ID_LOCATIONS = 5;
    const OLD_STEP_ID_GENRES = 6;
    const OLD_STEP_ID_DEVICES = 7;
    const OLD_STEP_ID_BUDGET = 8;

    const NEW_STEP_ID_AGES = 5;
    const NEW_STEP_ID_AUDIENCES = 6;
    const NEW_STEP_ID_GENRES = 8;

    /**
     * Draft campaigns with no-filled budget step(all steps included budget) should be migrated to
     * appropriate step according to provided mapping.
     *
     * Since budget step is fundamental for new dynamic-inventory draft campaign will be force navigated to budget
     *
     * Provided migration will guarantee that all targetings after new budget order (3rd step)
     * will be loaded and displayed appropriately on the details/navigation/sidebar.
     */
    private $stepMigrationMap = [
        self::NEW_STEP_ID_GENRES => [
            self::OLD_STEP_ID_GENRES,
            self::OLD_STEP_ID_DEVICES,
            self::OLD_STEP_ID_BUDGET
        ],
        self::NEW_STEP_ID_AUDIENCES => [
            self::OLD_STEP_ID_AUDIENCES,
            self::OLD_STEP_ID_LOCATIONS,
        ],
        self::NEW_STEP_ID_AGES => [
            self::OLD_STEP_ID_AGES
        ],
    ];

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->databaseManager = app(DatabaseManager::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->databaseManager->beginTransaction();

        try {
            foreach ($this->stepMigrationMap as $newStepId => $oldStepsArray) {
                DB::table('campaigns')
                    ->whereIn('step_id', $oldStepsArray)
                    ->where(['status_id' => CampaignStatus::ID_DRAFT])
                    ->update(['step_id' => $newStepId]);
            }
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){}
}
