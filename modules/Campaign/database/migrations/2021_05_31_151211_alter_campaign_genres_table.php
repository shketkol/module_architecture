<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_genres', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);
            $table->dropIndex('campaign_genres_campaign_id_foreign');

            $table->dropForeign(['genre_id']);
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_genres');

            if ($doctrineTable->hasIndex('campaign_genres_genre_id_index')) {
                $table->dropIndex('campaign_genres_genre_id_index');
            }

            $table->string('campaign_type')->nullable();

            $table->dropPrimary(['genre_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_genres', function (Blueprint $table) {
            $table->primary([
                'genre_id',
                'campaign_id',
                'excluded',
                'campaign_type',
            ], 'campaign_genres_pk');

            $table->index(['genre_id']);
            $table->foreign('genre_id')
                ->references('id')
                ->on('genres')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_genres')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_genres', function (Blueprint $table) {
            $table->dropForeign(['genre_id']);
            $table->dropIndex(['genre_id']);

            $table->dropPrimary(['genre_id', 'campaign_id', 'campaign_type']);
            $table->primary(['genre_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['genre_id']);
            $table->foreign('genre_id')
                ->references('id')
                ->on('genres')
                ->onDelete('cascade');
        });
    }
}
