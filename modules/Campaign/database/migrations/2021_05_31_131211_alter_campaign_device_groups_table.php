<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignDeviceGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_device_groups', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);
            $table->dropIndex('campaign_device_groups_campaign_id_foreign');

            $table->dropForeign(['device_group_id']);
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_device_groups');

            if ($doctrineTable->hasIndex('campaign_device_groups_device_group_id_index')) {
                $table->dropIndex('campaign_device_groups_device_group_id_index');
            }

            $table->string('campaign_type')->nullable();

            $table->dropPrimary(['device_group_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_device_groups', function (Blueprint $table) {
            $table->primary([
                'device_group_id',
                'campaign_id',
                'excluded',
                'campaign_type',
            ], 'campaign_device_groups_pk');

            $table->index(['device_group_id']);
            $table->foreign('device_group_id')
                ->references('id')
                ->on('device_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_device_groups')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_device_groups', function (Blueprint $table) {
            $table->dropForeign(['device_group_id']);
            $table->dropIndex(['device_group_id']);

            $table->dropPrimary(['device_group_id', 'campaign_id', 'campaign_type']);
            $table->primary(['device_group_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['device_group_id']);
            $table->foreign('device_group_id')
                ->references('id')
                ->on('device_groups')
                ->onDelete('cascade');
        });
    }
}
