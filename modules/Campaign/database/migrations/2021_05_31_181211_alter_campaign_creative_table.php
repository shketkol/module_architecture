<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignCreativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_creative', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);

            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_creative');

            $table->dropForeign(['creative_id']);

            if ($doctrineTable->hasIndex('campaign_creative_creative_id_index')) {
                $table->dropIndex('campaign_creative_creative_id_index');
            }

            $table->string('campaign_type')->nullable()->after('campaign_id');

            $table->dropPrimary(['creative_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_creative', function (Blueprint $table) {
            $table->primary([
                'creative_id',
                'campaign_id',
                'campaign_type',
            ], 'campaign_creative_pk');

            $table->foreign('creative_id')
                ->references('id')
                ->on('creatives')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_creative')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_creative', function (Blueprint $table) {
            $table->dropForeign(['creative_id']);
            $table->dropIndex('campaign_creative_creative_id_foreign');

            $table->dropPrimary(['creative_id', 'campaign_id', 'campaign_type']);
            $table->primary(['creative_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['creative_id']);
            $table->foreign('creative_id')
                ->references('id')
                ->on('creatives')
                ->onDelete('cascade');
        });
    }
}
