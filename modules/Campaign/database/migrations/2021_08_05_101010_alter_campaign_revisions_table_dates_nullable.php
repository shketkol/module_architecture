<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCampaignRevisionsTableDatesNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if (Schema::hasColumn('campaign_revisions', 'date_start')) {
            Schema::table('campaign_revisions', function (Blueprint $table) {
                $table->dateTime('date_start')->nullable()->default(null)->change();
            });
        }

        if (Schema::hasColumn('campaign_revisions', 'date_end')) {
            Schema::table('campaign_revisions', function (Blueprint $table) {
                $table->dateTime('date_end')->nullable()->default(null)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        if (Schema::hasColumn('campaign_revisions', 'date_start')) {
            Schema::table('campaign_revisions', function (Blueprint $table) {
                $table->dateTime('date_start')->nullable(false)->change();
            });
        }

        if (Schema::hasColumn('campaign_revisions', 'date_end')) {
            Schema::table('campaign_revisions', function (Blueprint $table) {
                $table->dateTime('date_end')->nullable(false)->change();
            });
        }
    }
}
