<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_locations', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);
            $table->dropIndex('campaign_locations_campaign_id_foreign');

            $table->dropForeign(['location_id']);
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_locations');

            if ($doctrineTable->hasIndex('campaign_locations_location_id_index')) {
                $table->dropIndex('campaign_locations_location_id_index');
            }

            $table->string('campaign_type')->nullable();

            $table->dropPrimary(['location_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_locations', function (Blueprint $table) {
            $table->primary([
                'location_id',
                'campaign_id',
                'excluded',
                'campaign_type',
            ], 'campaign_locations_pk');

            $table->index(['location_id']);
            $table->foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_locations')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_locations', function (Blueprint $table) {
            $table->dropForeign(['location_id']);
            $table->dropIndex(['location_id']);

            $table->dropPrimary(['location_id', 'campaign_id', 'campaign_type']);
            $table->primary(['location_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['location_id']);
            $table->foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onDelete('cascade');
        });
    }
}
