<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignAgeGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_age_groups', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);
            $table->dropIndex('campaign_age_groups_campaign_id_foreign');

            $table->dropForeign(['age_group_id']);
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_age_groups');

            if ($doctrineTable->hasIndex('campaign_age_groups_age_group_id_index')) {
                $table->dropIndex('campaign_age_groups_age_group_id_index');
            }

            $table->string('campaign_type')->nullable();

            $table->dropPrimary(['age_group_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_age_groups', function (Blueprint $table) {
            $table->primary(['age_group_id', 'campaign_id', 'campaign_type'], 'campaign_age_groups_pk');

            $table->index(['age_group_id']);
            $table->foreign('age_group_id')
                ->references('id')
                ->on('age_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_age_groups')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_age_groups', function (Blueprint $table) {
            $table->dropForeign(['age_group_id']);
            $table->dropIndex(['age_group_id']);

            $table->dropPrimary(['age_group_id', 'campaign_id', 'campaign_type']);
            $table->primary(['age_group_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['age_group_id']);
            $table->foreign('age_group_id')
                ->references('id')
                ->on('age_groups')
                ->onDelete('cascade');
        });
    }
}
