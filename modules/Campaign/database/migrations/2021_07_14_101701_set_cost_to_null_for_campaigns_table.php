<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignStatus;

class SetCostToNullForCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('campaigns')->update(['cost' => null]);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        //
    }
}
