<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeNullableInventoryCheckHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('inventory_check_history', function (Blueprint $table) {
            $table->bigInteger('available_impressions')->nullable()->change();
            $table->bigInteger('min_daily_impressions')->nullable()->change();

            $table->unsignedFloat('available_budget')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('inventory_check_history', function (Blueprint $table) {
            $table->bigInteger('available_impressions')->change();
            $table->bigInteger('min_daily_impressions')->change();

            $table->unsignedFloat('available_budget')->change();
        });
    }
}
