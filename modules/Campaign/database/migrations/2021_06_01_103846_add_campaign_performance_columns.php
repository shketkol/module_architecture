<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampaignPerformanceColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->float('cost')->after('budget')->nullable();
            $table->integer('delivered_impressions')->after('impressions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        if (Schema::hasColumn('campaigns', 'cost')) {
            Schema::table('campaigns', function (Blueprint $table) {
                $table->dropColumn('cost');
            });
        }

        if (Schema::hasColumn('campaigns', 'delivered_impressions')) {
            Schema::table('campaigns', function (Blueprint $table) {
                $table->dropColumn('delivered_impressions');
            });
        }
    }
}
