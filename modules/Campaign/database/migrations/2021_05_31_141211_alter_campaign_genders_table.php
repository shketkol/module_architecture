<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AlterCampaignGendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('campaign_genders', function (Blueprint $table) {
            // drop foreign key and indexes required before dropping primary key
            $table->dropForeign(['campaign_id']);
            $table->dropIndex('campaign_genders_campaign_id_foreign');

            $table->dropForeign(['gender_id']);
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('campaign_genders');

            if ($doctrineTable->hasIndex('campaign_genders_gender_id_index')) {
                $table->dropIndex('campaign_genders_gender_id_index');
            }

            $table->string('campaign_type')->nullable();

            $table->dropPrimary(['gender_id', 'campaign_id']);
        });

        $this->seedCampaignType();

        Schema::table('campaign_genders', function (Blueprint $table) {
            $table->primary(['gender_id', 'campaign_id', 'campaign_type'], 'campaign_genders_pk');

            $table->index(['gender_id']);
            $table->foreign('gender_id')
                ->references('id')
                ->on('genders')
                ->onDelete('cascade');
        });
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('campaign_genders')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('campaign_genders', function (Blueprint $table) {
            $table->dropForeign(['gender_id']);
            $table->dropIndex(['gender_id']);

            $table->dropPrimary(['gender_id', 'campaign_id', 'campaign_type']);
            $table->primary(['gender_id', 'campaign_id']);

            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');

            $table->index(['gender_id']);
            $table->foreign('gender_id')
                ->references('id')
                ->on('genders')
                ->onDelete('cascade');
        });
    }
}
