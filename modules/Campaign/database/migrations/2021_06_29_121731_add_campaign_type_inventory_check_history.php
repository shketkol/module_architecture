<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Campaign\Models\Campaign;

class AddCampaignTypeInventoryCheckHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('inventory_check_history', function (Blueprint $table): void {
            $table->dropForeign(['campaign_id']);

            $table->string('campaign_type')->nullable()->after('campaign_id');
        });

        $this->seedCampaignType();
    }

    /**
     * seed existing rows with campaign type
     */
    private function seedCampaignType(): void
    {
        DB::table('inventory_check_history')
            ->whereNull('campaign_type')
            ->update(['campaign_type' => Campaign::class]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('inventory_check_history', function (Blueprint $table): void {
            $table->dropColumn('campaign_type');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');
        });
    }
}
