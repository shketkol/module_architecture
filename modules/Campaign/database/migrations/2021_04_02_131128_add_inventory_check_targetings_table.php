<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventoryCheckTargetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('inventory_check_targetings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('history_id')->unsigned()->index();
            $table->foreign('history_id')->references('id')->on('inventory_check_history')->onDelete('cascade');

            $table->morphs('targetable');

            $table->boolean('excluded');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('inventory_check_targetings');
    }
}
