<?php

namespace Modules\Campaign\Database\Factories;

use App\Factories\Factory;
use App\Models\Timezone;
use Carbon\CarbonImmutable;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Payment\Models\PaymentMethod;
use Modules\User\Models\User;

class CampaignFactory extends Factory
{
    private const IMPRESSIONS_PER_DOLLAR = 31.25;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Campaign::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $dateStart = CarbonImmutable::now()->add(rand(4, 50), ' day');
        $budget = rand(2800, 3200);

        return [
            'name'              => $this->faker->catchPhrase,
            'budget'            => $budget,
            'cpm'               => (float)(rand(280, 350) / 10),
            'impressions'       => (int)ceil($budget * self::IMPRESSIONS_PER_DOLLAR),
            'submitted_at'      => $dateStart->sub(1, ' day'),
            'date_start'        => $dateStart,
            'date_end'          => $dateStart->add(rand(5, 50), ' day'),
            'status_id'         => CampaignStatus::ID_DRAFT,
            'status_changed_by' => function (array $attributes) {
                return Arr::get($attributes, 'user_id');
            },
            'user_id'           => User::factory(),
            'external_id'       => $this->faker->uuid,
            'order_id'          => $this->faker->unique()->numberBetween(999999, 9999999),
            'timezone_id'       => Timezone::ID_EASTERN,
            'payment_method_id' => function (array $attributes) {
                return PaymentMethod::factory()->create([
                    'user_id' => Arr::get($attributes, 'user_id'),
                ]);
            },
        ];
    }
}
