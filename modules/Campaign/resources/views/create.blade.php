@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <create-page />
    </div>
@endsection

@push('scripts')
    @include('common.includes.worldpay')
    <script src="{{ mix('js/campaign.js') }}"></script>
@endpush
