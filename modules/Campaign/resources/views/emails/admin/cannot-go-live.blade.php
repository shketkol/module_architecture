@extends('common.email.layout-admin')

@section('body')
    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('emails.hi', ['name' => $firstName]) }}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.admin.cannot_go_live.body.the_campaign_name_campaign_by', [
            'campaign_name' => e($campaignName),
            'campaign_id' => e($orderId),
            'advertiser_company' => view('common.email.part.link', [
                'link' => $advertiserDetails,
                'text' => $companyName,
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>
    @include('common.email.part.list', [
       'listItems' => [
           __('campaign::emails.admin.cannot_go_live.body.no_creative_was_submitted'),
           __('campaign::emails.admin.cannot_go_live.body.the_submitted_creative_didn’t_meet')
       ]
    ])

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.admin.cannot_go_live.body.for_more_information_about_the_campaign', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => __('campaign::labels.campaign_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!!  __('campaign::emails.admin.cannot_go_live.body.if_you_believe_there_has_been_an_error', [
            'support_email' => view('common.email.part.mailto', [
                'email' => $supportEmail,
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>
@stop
