@extends('common.email.layout')

@section('body')

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!!__('campaign::emails.advertiser.paused_by_advertiser.body.you_have_paused_the_campaign', [
            'campaign_name' => e($campaignName),
        ]) !!}
    </p>

    <p style="font-size: 16px; font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {!! __('campaign::emails.advertiser.paused_by_advertiser.body.for_more_information_about_the_campaign', [
            'campaign_details' => view('common.email.part.link', [
                'link' => $campaignDetails,
                'text' => __('campaign::labels.campaign_details'),
            ])->render(function ($content) {
                    return trim($content);
                }),
        ]) !!}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
