@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <edit-page
            :id="{{ $id }}"
            @if(isset($forceNavigateTo))
                force-navigate-step="{{ $forceNavigateTo }}"
            @endif
        />
    </div>
@endsection

@push('scripts')
    @include('common.includes.worldpay')
    <script src="{{ mix('js/campaign.js') }}"></script>
@endpush
