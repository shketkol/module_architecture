<?php

use Modules\Campaign\Models\CampaignRevisionStatus;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Targeting\Models\DeviceGroup;

return [
    'campaign_revision' => [
        'statuses' => [
            CampaignRevisionStatus::DRAFT      => 'Draft',
            CampaignRevisionStatus::SUBMITTED  => 'Submitted',
            CampaignRevisionStatus::LIVE       => 'Live',
            CampaignRevisionStatus::PROCESSING => 'Processing',
        ],
    ],
    'campaign'          => [
        /**
         * Campaign statuses
         */
        'statuses' => [
            CampaignStatus::DRAFT            => 'Draft',
            CampaignStatus::PENDING_APPROVAL => 'Pending Approval',
            CampaignStatus::READY            => 'Ready',
            CampaignStatus::LIVE             => 'Live',
            CampaignStatus::PAUSED           => 'Paused',
            CampaignStatus::COMPLETED        => 'Completed',
            CampaignStatus::CANCELED         => 'Cancelled',
            CampaignStatus::PROCESSING       => 'Processing',
            CampaignStatus::SUSPENDED        => 'Suspended',
        ],

        /**
         * Campaign cards titles
         */
        'cards'    => [
            'active_campaigns'            => 'Active Campaigns',
            'impressions_delivered'       => 'Impressions Delivered',
            'required_actions'            => 'Required Actions',
            'total_campaigns'             => 'Total Campaigns',
            'spend_to_date'               => 'Total Delivered Spend to Date',
            'ad_cost'                     => 'Ad Cost',
            'total_delivered_impressions' => 'Lifetime Impressions',
        ],
    ],

    'modal_inventory_info' => [
        'inventory_high_demand' => 'Inventory Availability Is Low',
        'daily_impressions_low' => 'Daily Impression Goal Is Too Low',
        'unable_guarantee'      => 'We may not have enough inventory for your request.',
        'forecasts_indicate'    => 'Our forecasts indicate that we can only meet up to a',
        'budget_lover_case'     => 'budget',
        'alt_expand_targeting'  => 'Alternately, expand your targeting',
        'change_budget'         => 'Change budget to',
        'expand_targeting'      => 'Expand targeting',
    ],
    'modal_inventory_fail' => [
        'something_went_wrong' => 'Oh no - something went wrong!',
        'dont_worry'           => 'Something\'s not right. We\'re working on it and have saved your campaign information. Please try again later.',
    ],
    'modal_progress'       => [
        'checking_inventory'        => 'We\'re checking our inventory.',
        'reviewing_campaign'        => 'Reviewing your campaign',
        'checking_date_range'       => 'Checking date range',
        'checking_parameters'       => 'Checking targeting parameters',
        'checking_inventory_demand' => 'Checking inventory demand',
        'optimizing'                => 'Optimizing',
    ],

    'titles' => [
        'campaign_name' => 'Campaign Name',
        'schedule'      => 'Campaign Date Range',
        'budget'        => 'Campaign Budget',
        'genders'       => 'Gender & Age Targeting',
        'locations'     => 'Location Targeting',
        'genres'        => 'Content Genre Targeting',
        'devices'       => 'Platform Targeting',
        'audiences'     => 'Audience Targeting',
        'creative'      => 'Ad Selection',
    ],

    'labels' => [
        'gender'        => 'Gender',
        'age_groups'    => 'Age Groups',
        'cpm_annotated' => ':cpm',
    ],

    'subtitles' => [
        'campaign_name' => 'Enter a recognizable name for your campaign. This will be used in reports, invoices, and more.',
        'schedule'      => '
            <p>To ensure we have enough time to review your Ad, please allow at least 3 business days before your campaign start date. <a href=":link" target="_blank">Learn more about our Ad approval process</a></p>
            <p>You can submit a campaign up to 3 months in advance, and it can run for up to 12 months.</p>
        ',
        'budget'        => '
            <p>Enter a :budget amount of at least $500, and we’ll estimate the number of :impressions that should be delivered based on this budget. While we can’t guarantee exact impression delivery, this budget will not be exceeded.</p>
            ',
        'genders'       => 'Select gender and age groups that you would like to target.',
        'locations'     => '
            <p>To include only specific locations, search by ZIP code, city, state, or :dma. To exclude a specific location, select it, and then click the <b>Exclude</b> option.</p>
            <p>If you skip this step, your Ad may be shown in any location.</p>
        ',
        'genres'        => '
            <p>To show your Ad only with specific content genres, select genres from the options below. To exclude a specific genre, select it and then click the Exclude option.</p>
            <p>If you skip this step, your Ad may be shown with any genre.</p>
        ',
        'devices'       => '
            <p>Select the streaming platforms on which to show your Ad. You may choose to select only the specific platforms whose audiences may find your Ad relevant.</p>
            <p>If you skip this step, your Ad may be shown on any platform.</p>
        ',
        'audiences'     => '
            <p>You can target specific audiences based on :behavior, :demographics, :interest, and :ownership. Each audience you select will be :targeted_independently, but some audiences may overlap.</p>
            <p>If you skip this step, your Ad will be shown to a general Hulu audience, including any of the specific audiences listed below.</p>
        ',
        'special_ads'   => [
            'genders'     => 'The <a target="_blank" href=":url">Hulu Ad Manager Guidelines for Special Ads Categories</a> prohibit targeting by gender or age group.',
            'audiences'   => 'The <a target="_blank" href=":url">Hulu Ad Manager Guidelines for Special Ads Categories</a> prohibit targeting audiences on the basis of their demographic characteristics.',
            'locations_1' => '
                <p>To include only specific locations, search by city, state, or :dma. To exclude a specific location, select it, and then click the <b>Exclude</b> option.</p>
                <p>If you skip this step, your Ad may be shown in any location.</p>
            ',
            'locations_2' => '
                <p>Note, the <a target="_blank" href=":url">Hulu Ad Manager Guidelines for Special Ads Categories</a> prohibit targeting by ZIP code.</p>
            ',
            'genres'      => 'The <a target="_blank" href=":url">Hulu Ad Manager Guidelines for Special Ads Categories</a> prohibit targeting audiences by show genre.',
        ],
        'live_mode'     => [
            'audiences' => 'Audience targeting cannot be added to a live campaign. To run this Ad with audience targeting, you can <a target="_blank" href=":url">duplicate</a> this campaign and re-use the rest of your settings and targeting selections.',
            'locations' => 'Location targeting cannot be added to a live campaign. To run this Ad with location targeting, you can <a target="_blank" href=":url">duplicate</a> this campaign and re-use the rest of your settings and targeting selections.',
        ],
    ],

    'annotations' => [
        'budget'                      => [
            'term'        => 'budget',
            'description' => '
                <p>The budget is the dollar amount that you wish to spend over the lifetime of your campaign. A minimum of $500 is required.</p>
                <p>We will attempt to deliver against your budget across the life of your campaign, but cannot guarantee delivery. We will not, however, exceed your budget limit.</p>
            ',
        ],
        'impressions'                 => [
            'term'        => 'impressions',
            'description' => '<p>An impression is a single display of your Ad.</p>',
        ],
        'cpm'                         => [
            'term'        => 'cpm',
            'description' => '<p>CPM refers to the <em>cost per mille</em> (thousand) impressions. It’s the amount of money you pay for every 1,000 times your Ad is displayed.</p>',
        ],
        'behavior'                    => [
            'term'        => 'behavior',
            'description' => '<p>Target people who are <em>in the market for</em> or who <em>have already purchased</em> particular kinds of goods.</p>',
        ],
        'demographics'                => [
            'term'        => 'demographics',
            'description' => '<p>Target people based on characteristics like employment or education.</p>',
        ],
        'interest'                    => [
            'term'        => 'interest',
            'description' => '<p>Target people based on activities, hobbies, or lifestyle choices.</p>',
        ],
        'ownership'                   => [
            'term'        => 'ownership',
            'description' => '<p>Target people who have particular kinds of cars, homes, or pets.</p>',
        ],
        'targeted_independently'      => [
            'term'        => 'targeted independently',
            'description' => '<p>For example, if you select “College Grads” and “Hip Hop Lovers,” you may reach people from either of these audiences, not necessarily <em>college grads who are also hip hop lovers.</em></p>',
        ],
        'dma'                         => [
            'term'        => 'DMA',
            'description' => '<p>A DMA (<em>designated market area</em>) is a region that constitutes an American television market. A DMA usually includes a metropolitan area and its surrounding suburbs.</p>',
        ],
        'estimated_daily_impressions' => [
            'description' => '<p>This is an estimated daily average. This may fluctuate over the course of the campaign.<p>',
        ]
    ],

    'infotips' => [
        'behavior'                => '<p>Target people who are <em>considering purchasing</em> or who <em>have already purchased</em> particular kinds of goods.</p>',
        'demographics'            => '<p>Target people based on their employment, education, income, or life stage.</p>',
        'demographics_milestones' => '<p>Target people in key life stages, such as New Parents or Brides and Grooms To Be.</p>',
        'interest'                => '<p>Target people based on activities, hobbies, or lifestyle choices.</p>',
        'ownership'               => '<p>Target people who have particular kinds of cars, homes, or pets.</p>',
        'devices'                 => [
            'living_room' => 'Includes streaming devices such as smart TVs, game consoles, set-top boxes, etc.',
            'computer'    => 'Includes laptops and desktops.',
            'mobile'      => 'Includes smartphones and tablets.',
        ],
    ],

    'selects' => [
        'placeholders' => [
            'ages'      => 'Select one or more age groups.',
            'locations' => 'Enter a location name',
            'genres'    => 'Select one or more genres.',
            'audiences' => 'Enter a keyword to search audiences',
            'zip'       => 'Enter multiple ZIP codes separated by commas',
        ],
        'select_all'   => [
            'genres' => 'All Genres',
        ],
    ],

    'campaign_details'            => 'Campaign Details',
    'advertiser_company'          => 'Advertiser company',
    'campaign_listing'            => 'Campaign Listing',
    'campaigns_page'              => 'Campaigns Page',
    'performance'                 => [
        'delivered_impressions_last_days' => 'Delivered Impressions (Last :days days)',
        'ad_exposure'                     => 'Ad Exposure',
        'ads_completed'                   => 'Ads Completed',
        'charts'                          => [
            'impressions' => [
                'title'    => 'Impressions over Time',
                'subtitle' => 'Campaign impressions delivered for the last :days days.',
            ],
            'genres'      => [
                'title'    => 'Impressions by Show Genre',
                'subtitle' => 'Percentage of total impressions by Show Genre.',
                'other'    => 'Other',
            ],
            'devices'     => [
                'title'       => 'Impressions by Platform',
                'subtitle'    => 'Percentage of total impressions by type of streaming device.',
                'living_room' => 'Living Room Devices',
                'computer'    => 'Computers',
                'mobile'      => 'Mobile devices',
                'phone'       => 'Phones',
                'tablet'      => 'Tablets',
            ],
        ],
    ],
    'budget'                      => 'Budget',
    'campaign_id'                 => 'ID',
    'campaign_name'               => 'Campaign Name',
    'campaign_start_end'          => 'Start - End Dates',
    'campaigns'                   => 'Campaigns',
    'creative_step'               => 'Ad',
    'date_range'                  => 'Date Range',
    'delivered'                   => 'Delivered',
    'targeted'                    => 'Targeted',
    'total_spend'                 => 'Total Spend',
    'max_budget'                  => 'Max Budget',
    'end_date'                    => 'End Date',
    'first_billing_date'          => 'Estimated First Billing Date',
    'id'                          => 'Campaign ID',
    'impressions'                 => 'Impressions',
    'target_impressions'          => 'Target Impressions',
    'targeted_impressions'        => 'Targeted :impressions',
    'name'                        => 'Campaign Name',
    'name_placeholder'            => 'Enter your campaign name',
    'search'                      => 'Search',
    'search_campaign_placeholder' => 'Search Campaigns',
    'order_confirmation'          => 'Order Confirmation',
    'campaign_summary'            => 'Campaign Summary',
    'order_id'                    => 'Order ID',
    'review_campaign'             => 'Review Campaign',
    'reach'                       => 'Reach',
    'frequency'                   => 'Frequency',
    'average'                     => 'Average',
    'start_date'                  => 'Start Date',
    'status'                      => 'Campaign Status',
    'targeting'                   => 'Targeting',
    'sales'                       => 'Sales',
    'campaign_title'              => 'Campaign',
    'selected'                    => ':amount selected',
    'excluded'                    => ':amount excluded',
    'code_applied'                => 'Code Applied',
    'setup'                       => 'Setup',
    'your_campaign'               => 'Your Campaign',
    'proceed_to_payment'          => 'Proceed To Payment',
    'view_campaign_page'          => 'View Campaign Page',
    'set_goal_by'                 => 'Set goal by',
    'dates'                       => 'Dates',
    'days'                        => '(:days days)',
    'steps'                       => [
        'details'          => [
            'name'     => 'Name',
            'schedule' => 'Dates',
        ],
        'targeting'        => [
            'ages'      => 'Gender and Age',
            'audiences' => 'Audiences',
            'locations' => 'Locations',
            'genres'    => 'Show Genres',
            'devices'   => 'Platforms',
        ],
        'budget'           => [
            'budget' => 'Budget',
        ],
        'creative'         => [
            'creative' => 'Ad',
        ],
        'reviewAndPayment' => [
            'review'  => 'Review',
            'payment' => 'Payment',
        ],
    ],
    'campaign_id_value'           => 'ID: :value',
    'buttons'                     => [
        'skip'         => 'Skip',
        'skip_for_now' => 'Skip for now',
    ],
    'inventory_steps'             => [
        'locations' => 'Location Targeting',
        'zip_codes' => 'Zip Targeting',
        'ages'      => 'Gender & Age Targeting',
        'audiences' => 'Audience Targeting',
        'devices'   => 'Platform Targeting',
        'genres'    => 'Genre Targeting',
        'schedule'  => 'Date Range',
    ],
    'try_the_following'           => 'To find available inventory, try the following:',
    'devices'                     => [
        DeviceGroup::NAME_COMPUTER    => 'Computer',
        DeviceGroup::NAME_MOBILE      => 'Mobile',
        DeviceGroup::NAME_LIVING_ROOM => 'Living room',
    ],
    'revision_date'               => 'Revision Date'
];
