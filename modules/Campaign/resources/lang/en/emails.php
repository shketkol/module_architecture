<?php

return [
    'advertiser' => [
        'paused_by_advertiser'   => [
            'subject' => 'Your campaign is paused',
            'title'   => 'Campaign Paused',
            'body'    => [
                'you_have_paused_the_campaign'            => 'You’ve successfully paused your <i>:campaign_name</i> campaign.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign or to resume it, see :campaign_details.',
            ],
        ],
        'paused_by_admin'        => [
            'subject' => 'Your campaign was paused by :publisher_company_name',
            'title'   => 'Campaign Paused By :publisher_company_name',
            'body'    => [
                'your_campaign_has_been_paused'           => 'We wanted to let you know that we’ve paused your <i>:campaign_name</i> campaign.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign, see :campaign_details.',
            ],
        ],
        'resumed_by_advertiser'  => [
            'subject' => 'Your campaign has resumed',
            'title'   => 'Campaign Resumed',
            'body'    => [
                'you_have_resumed_the_campaign'           => 'You’ve successfully resumed your <i>:campaign_name</i> campaign.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign, see :campaign_details.',
            ],
        ],
        'resumed_by_admin'       => [
            'subject' => 'Your campaign was resumed by :publisher_company_name',
            'title'   => 'Campaign Resumed By :publisher_company_name',
            'body'    => [
                'your_campaign_has_been_resumed'          => 'Good news! We’ve resumed your <i>:campaign_name</i> campaign.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign, see :campaign_details.',
            ],
        ],
        'canceled_by_admin'      => [
            'subject' => 'Your campaign was canceled by :publisher_company_name',
            'title'   => 'Campaign Canceled By :publisher_company_name',
            'body'    => [
                'something_is_not_quite_right'            => 'Something’s not right. We regret to inform you that your <i>:campaign_name</i> campaign has been cancelled.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign, see <i>:campaign_details.</i>',
            ],
        ],
        'canceled_by_advertiser' => [
            'subject' => 'Your :campaign_name campaign has been canceled',
            'title'   => 'Campaign Has Been Canceled',
            'body'    => [
                'you_have_successfully_canceled_your_campaign' => 'You’ve successfully canceled your <i>:campaign_name</i> campaign. You won’t be able to resume this campaign, but we hope you’ll think of us for your future needs.',
                'for_more_information_about_the_campaign'      => 'For more information about the campaign, see :campaign_details.',
                'to_book_a_new_campaign'                       => 'To submit a new campaign, click the link below:',
            ],
        ],
        'cannot_go_live'         => [
            'subject' => 'Your campaign can’t go live',
            'title'   => 'Campaign Can’t Go Live',
            'body'    => [
                'your_campaign_cannot_go_live'            => 'Your <i>:campaign_name</i> campaign can’t go live because there is no approved Ad attached.',
                'to_avoid_any_delays'                     => 'To avoid any delays, please upload or attach an approved Ad as soon as possible.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign, see :campaign_details.',
            ],
        ],
        'completed'              => [
            'subject' => 'Campaign complete',
            'title'   => 'Campaign Complete',
            'body'    => [
                'your_campaign_has_completed'         => 'Your <i>:campaign_name</i> campaign is now complete!',
                'for_more_information_about_campaign' => 'For more information about the campaign and its performance, see <i>:campaign_details.</i>',
                'thank_you_for_booking_your_campaign' => 'Thank you for using :publisher_company_full_name!',
                'to_create_a_new_campaign'            => 'To create a new campaign, click the link below:',
            ],
        ],
        'started'                => [
            'subject' => 'Campaign started',
            'title'   => 'Campaign Started',
            'body'    => [
                'your_campaign_is_up'                      => 'Your <i>:campaign_name</i> campaign is now up and running!',
                'for_more_information_about_your_campaign' => 'For more information about your campaign and its performance, see <i>:campaign_details.</i>',
            ],
        ],
        'ordered'                => [
            'subject' => 'Your campaign was submitted',
            'title'   => 'Campaign submitted',
            'body'    => [
                'thank_you_for_booking'                    => 'Thank you for submitting your campaign!',
                'at_the_end_of_each_billing_cycle'         => 'At the end of each billing period after your campaign starts, we’ll calculate the amount you owe based on delivered impressions and automatically charge that amount to your payment method.',
                'for_more_information_about_your_campaign' => 'For more information about your campaign, see <i>:campaign_details.</i>',
            ],
        ],
        'edit_confirmation'      => [
            'success' => [
                'subject' => 'Your campaign was successfully modified',
                'title'   => 'Campaign Modified',
                'body'    => [
                    'your_recent_campaign_modifications'       => 'Your recent campaign modifications have been accepted and the changes will be live within a few minutes.',
                    'for_more_information_about_your_campaign' => 'For more information about your campaign, see <i>:campaign_details.</i>',
                ],
            ],
            'failed'  => [
                'subject' => 'Your campaign modification was unsuccessful',
                'title'   => 'Campaign Modification Error',
                'body'    => [
                    'something_went_wrong' => 'Something went wrong while applying the recent campaign modifications you submitted. We have reverted the changes and your campaign is currently live with its original settings.',
                    'you_can_try_again'    => 'You can try again and resubmit your changes here: <i>:campaign_details.</i>',
                ],
            ]
        ]
    ],
    'admin'      => [
        'canceled_by_advertiser' => [
            'subject' => ':publisher_company_full_name Notification: Campaign canceled',
            'body'    => [
                'advertiser_has_canceled_their_campaign'  => ':advertiser_company has canceled their :campaign_name campaign.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign, see :campaign_details.',
            ],
        ],
        'cannot_go_live'         => [
            'subject' => ':publisher_company_full_name Notification: Campaign cannot go live',
            'body'    => [
                'the_campaign_name_campaign_by'           => 'The :campaign_name//:campaign_id campaign by :advertiser_company, can’t go live for one of the following reasons:',
                'no_creative_was_submitted'               => 'No Ad was submitted.',
                'the_submitted_creative_didn’t_meet'      => 'The submitted Ad didn’t meet Hulu quality/technical standards.',
                'for_more_information_about_the_campaign' => 'For more information about the campaign, see :campaign_details.',
                'if_you_believe_there_has_been_an_error'  => 'If you believe there has been an error regarding the status of this campaign, please contact us at (:support_email).',
            ],
        ],
        'ordered'                => [
            'subject' => ':publisher_company_full_name Notification: New campaign submitted',
            'body'    => [
                'great_news'             => ':company has submitted a new campaign. For more information about the campaign, see :campaign_details.',
                'at_the_time'            => 'At the time when the order was placed the Ad status was: :creative_status.',
                'to_review_the_creative' => 'To review the submitted Ad, go to :campaign_details.',
            ],
        ],
    ],
];
