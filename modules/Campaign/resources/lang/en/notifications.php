<?php

return [
    'advertiser' => [
        'started'           => 'Your :campaign_name campaign has started.',
        'completed'         => 'Your :campaign_name campaign has finished running. For impression and spend details see Campaign Details',
        'cannot_go_live'    => 'Your :campaign_name campaign cannot go live because it is missing an approved Ad. To avoid delays, upload your Ad as soon as possible.',
        'canceled_by_admin' => 'We’ve cancelled your :campaign_name campaign. If you have any questions, please use the Contact Us form.',
        'paused_by_admin'   => 'We\'ve paused your :campaign_name campaign. If you have any questions, please use the Contact Us form.',
        'resumed_by_admin'  => 'We\'ve resumed your :campaign_name campaign.',
        'edit_confirmation' => [
            'success' => 'Your :campaign_name campaign modifications have been accepted and will be live within a few minutes.',
            'failed'  => 'The modifications to your :campaign_name campaign were not successful. Please resubmit your changes again.'
        ]
    ],
];
