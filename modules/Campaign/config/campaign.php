<?php

use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Targeting\Models\DeviceGroup;

return [
    /*
    |--------------------------------------------------------------------------
    | Wizard
    |--------------------------------------------------------------------------
    |
    | These configs are used on campaign booking wizard.
    |
    */
    'wizard'                    => [
        'logo_default' => 'S',
        'steps'        => CampaignStep::getStepIds(),
        'date'         => [
            'start'        => [
                'min' => [
                    'value' => 0,
                    'type'  => 'day',
                ],
                'max' => [
                    'value' => 3,
                    'type'  => 'month',
                ],
            ],
            'max_duration' => [
                'value' => 12,
                'type'  => 'month',
            ],
            'timezone'     => 'America/New_York',
        ],
        'time'         => [
            'start' => [
                'hours'   => 0,
                'minutes' => 0,
                'seconds' => 0,
            ],
            'end'   => [
                'hours'   => 23,
                'minutes' => 59,
                'seconds' => 59,
            ],
        ],
        'budget'       => [
            'default' => 10000,
            'min'     => InventoryCheckHistory::SYSTEM_FLOOR_BUDGET,
        ],
        'targeting'    => [
            'genders' => [
                'default' => 'all',
            ],
        ],
        'live_edit_budget_buffer' => env('LIVE_EDIT_BUDGET_BUFFER', 105),
    ],
    'charts'                    => [
        'devices_map'    => [
            'living_room' => DeviceGroup::NAME_LIVING_ROOM,
            'computer'    => DeviceGroup::NAME_COMPUTER,
            'mobile'      => DeviceGroup::NAME_MOBILE,
            'phone'       => DeviceGroup::NAME_MOBILE,
            'tablet'      => 'tablet'
        ],
        'devices_config' => [
            'living_room' => [
                'color' => '#19e69b',
                'image' => [
                    'width'  => 100,
                    'height' => 145,
                ],
            ],
            'computer'    => [
                'color' => '#e6a219',
                'image' => [
                    'width'  => 100,
                    'height' => 145,
                ],
            ],
            'mobile'      => [
                'color' => '#1972e6',
                'image' => [
                    'width'  => 86,
                    'height' => 145,
                ],
            ],
            'tablet'      => [
                'color' => '#b887d4',
                'image' => [
                    'width'  => 86,
                    'height' => 145,
                ],
            ],
        ],
    ],
    'enable_timezone_select'    => env('CAMPAIGN_ENABLE_TIMEZONE_SELECT', false),
    'enable_budget_suggestions' => env('INVENTORY_ENABLE_BUDGET_SUGGESTIONS', false),
    'min_daily_impressions'     => env('MIN_DAILY_IMPRESSIONS', 1000),
    'inventory_expiration'      => env('INVENTORY_EXPIRATION', 60),
];
