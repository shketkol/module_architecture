<?php

use App\Helpers\EnvironmentHelper;
use Modules\Campaign\Actions\GetCampaignInventoryCheckAction;
use Modules\Haapi\Actions\Campaign\CampaignInventoryCheck;

return [
    'statuses'                => [
        GetCampaignInventoryCheckAction::TYPE_SUCCESS         => [
            CampaignInventoryCheck::HAAPI_TYPE_SUCCESS,
            CampaignInventoryCheck::HAAPI_TYPE_YIELDEX_DOWN,
        ],
        GetCampaignInventoryCheckAction::TYPE_WARNING         => [CampaignInventoryCheck::HAAPI_TYPE_WARNING],
        GetCampaignInventoryCheckAction::TYPE_DANGER_BUDGET   => [CampaignInventoryCheck::HAAPI_TYPE_DANGER_BUDGET],
        GetCampaignInventoryCheckAction::TYPE_DANGER_SCHEDULE => [CampaignInventoryCheck::HAAPI_TYPE_DANGER_SCHEDULE],
    ],
    'dynamic_inventory_check' => [
        'unavailable' => [
            'times_before_faq' => env('DYNAMIC_INVENTORY_CHECK_UNAVAILABLE_TIMES_BEFORE_FAQ', 5),
            'faq_link'         => env(
                'DYNAMIC_INVENTORY_CHECK_UNAVAILABLE_FAQ_LINK',
                'https://faq.admanager.hulu.com/Content/FAQ/faq-inventory.htm'
            ),
        ],
        'debug'       => [
            'enabled' => env('DYNAMIC_INVENTORY_CHECK_DEBUG_ENABLED', EnvironmentHelper::isOpenEnv()),
        ],
    ],
];
