<?php

namespace Modules\Campaign\Policies;

use App\Policies\Policy;
use App\Policies\Traits\Checks;
use Modules\Campaign\Actions\Validations\ValidateCampaignTargetingsAction;
use Modules\Campaign\Models\CampaignRevision;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Policies\Rules\TargetingsRules;
use Modules\Campaign\Policies\Traits\CampaignEditPolicy;
use Modules\User\Models\User;

class CampaignPolicy extends Policy
{
    use Checks, CampaignEditPolicy;

    /*** Permissions. */
    public const PERMISSION_CREATE_CAMPAIGN          = 'create_campaign';
    public const PERMISSION_DELETE_CAMPAIGN          = 'delete_campaign';
    public const PERMISSION_LIST_CAMPAIGN            = 'list_campaign';
    public const PERMISSION_LIST_ADVERTISER_CAMPAIGN = 'list_advertiser_campaign';
    public const PERMISSION_UPDATE_CAMPAIGN          = 'update_campaign';
    public const PERMISSION_VIEW_CAMPAIGN            = 'view_campaign';
    public const PERMISSION_CANCEL_CAMPAIGN          = 'cancel_campaign';
    public const PERMISSION_PAUSE_CAMPAIGN           = 'pause_campaign';
    public const PERMISSION_RESUME_CAMPAIGN          = 'resume_campaign';
    public const PERMISSION_MANAGE_CAMPAIGN_CREATIVE = 'manage_campaign_creative';
    public const PERMISSION_CLONE_CAMPAIGN           = 'clone_campaign';

    /**
     * @var ValidateCampaignTargetingsAction
     */
    private ValidateCampaignTargetingsAction $validateCampaignTargetingsAction;

    /**
     * @var TargetingsRules
     */
    private TargetingsRules $targetingRules;

    /**
     * @param TargetingsRules                  $targetingRules
     * @param ValidateCampaignTargetingsAction $validateCampaignTargetingsAction
     */
    public function __construct(
        TargetingsRules $targetingRules,
        ValidateCampaignTargetingsAction $validateCampaignTargetingsAction
    ) {
        $this->targetingRules = $targetingRules;
        $this->validateCampaignTargetingsAction = $validateCampaignTargetingsAction;
    }

    /**
     * Returns true if user can view list of all campaigns.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_CAMPAIGN);
    }

    /**
     * Returns true if user can view list campaigns of given advertiser.
     *
     * @param User      $user
     * @param User|null $advertiser
     *
     * @return bool
     */
    public function listAdvertiserCampaigns(User $user, ?User $advertiser): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_ADVERTISER_CAMPAIGN) &&
            $this->hasAccessToCampaigns($user, $advertiser);
    }

    /**
     * Returns true if user can store a new campaign draft.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_CAMPAIGN);
    }

    /**
     * Returns true if user can view the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function view(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_VIEW_CAMPAIGN)) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can clone the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function clone(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_CLONE_CAMPAIGN)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can order the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function order(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();
        return $this->update($user, $campaign) && $user->isActive();
    }

    /**
     * Returns true if user can delete the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function delete(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_DELETE_CAMPAIGN)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        if (!$campaign->isDeletable()) {
            return false;
        }

        if ($campaign->isProcessing()) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can cancel campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function cancel(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_CANCEL_CAMPAIGN)) {
            return false;
        }

        if ($campaign->user->isDeactivated()) {
            return false;
        }

        if ($campaign->isProcessing()) {
            return false;
        }

        if ($user->isSuperAdmin()) {
            return true;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can pause campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function pause(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_PAUSE_CAMPAIGN)) {
            return false;
        }

        if ($campaign->user->isDeactivated()) {
            return false;
        }

        if ($campaign->isProcessing()) {
            return false;
        }

        if (!$campaign->isLiveRange()) {
            return false;
        }

        if ($user->isSuperAdmin()) {
            return true;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can resume campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function resume(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_RESUME_CAMPAIGN)) {
            return false;
        }

        if ($campaign->user->isDeactivated()) {
            return false;
        }

        if (!$campaign->isLiveRange() || !$campaign->isPaused()) {
            return false;
        }

        if ($campaign->isProcessing()) {
            return false;
        }

        // if changed by advertiser and current user is same advertiser -> allow
        if ($user->is($campaign->statusChangedBy)) {
            return true;
        }

        // if changed by full rights admin and current user is also full rights admin -> allow
        if ($user->isSuperAdmin() && optional($campaign->statusChangedBy)->isSuperAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * Returns true if user can attach/detach creative to/from campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function manageCampaignCreative(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$this->baseManageCreative($user, $campaign)) {
            return false;
        }

        if (!$campaign->isEditableCreative()) {
            return false;
        }

        return true;
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function replaceCampaignCreative(User $user, Campaignable $campaign): bool
    {
        return $campaign->isOrigin() ?
            $this->replaceOriginCreative($user, $campaign->getOrigin()) :
            $this->replaceRevisionCreative($user, $campaign);
    }

    /**
     * @param User      $user
     * @param User|null $advertiser
     *
     * @return bool
     */
    private function hasAccessToCampaigns(User $user, ?User $advertiser): bool
    {
        return $this->isAdmin($user) || $user->is($advertiser);
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    private function baseManageCreative(User $user, Campaignable $campaign): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_MANAGE_CAMPAIGN_CREATIVE)) {
            return false;
        }

        if ($user->isDeactivated() && !$campaign->inState(CampaignStatus::ID_DRAFT)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        if ($campaign->inState(CampaignStatus::ID_PROCESSING)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can validate promocode in the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function validatePromocode(User $user, Campaignable $campaign): bool
    {
        return $campaign->getOrigin()->inState(CampaignStatus::ID_DRAFT);
    }

    /**
     * Check if user can send draft activity.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function accessActivity(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$this->update($user, $campaign)) {
            return false;
        }

        if ($user->isPendingManualReview() || $user->isInactive()) {
            return false;
        }

        return true;
    }

    /**
     * Check if order_id is empty before change status.
     *
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public static function changeStatusAfterSubmission(Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$campaign->order_id) {
            return false;
        }

        return true;
    }

    /**
     * @param User             $user
     * @param CampaignRevision $revision
     *
     * @return bool
     */
    public function showRevision(User $user, CampaignRevision $revision): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        if (!$this->isOwner($user, $revision->campaign)) {
            return false;
        }

        return true;
    }

    /**
     *  Check if user can download campaign report.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function downloadReport(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        // TODO Added check previous state
        // From Live -> Pause
        // From Live -> Canceled
        if ($campaign->inState([
                CampaignStatus::ID_LIVE,
                CampaignStatus::ID_COMPLETED,
                CampaignStatus::ID_PAUSED,
                CampaignStatus::ID_CANCELED,
                CampaignStatus::ID_PROCESSING,
                CampaignStatus::ID_SUSPENDED,
            ]) && !empty($campaign->order_id)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param User     $user
     * @param Campaign $campaign
     *
     * @return bool
     */
    protected function replaceOriginCreative(User $user, Campaign $campaign): bool
    {
        if (!$this->baseManageCreative($user, $campaign)) {
            return false;
        }

        if ($campaign->isOnlyReplaceableCreative()) {
            return true;
        }

        return false;
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    protected function replaceRevisionCreative(User $user, Campaignable $campaign): bool
    {
        if (!$this->baseManageCreative($user, $campaign->getOrigin())) {
            return false;
        }

        if (!$campaign->isOrigin()) {
            return true;
        }

        return false;
    }
}
