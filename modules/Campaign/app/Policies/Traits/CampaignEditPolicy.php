<?php

namespace Modules\Campaign\Policies\Traits;

use Carbon\Carbon;
use Modules\Campaign\Actions\Validations\Traits\CampaignTargetingValidation;
use Modules\Campaign\Actions\Validations\ValidateCampaignTargetingsAction;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Policies\Rules\TargetingsRules;
use Modules\User\Models\User;

/**
 * @property TargetingsRules targetingRules
 * @property ValidateCampaignTargetingsAction $validateCampaignTargetingsAction
 */
trait CampaignEditPolicy
{
    use CampaignTargetingValidation;

    /**
     * Returns true if user can update the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_CAMPAIGN)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        if (!$campaign->isEditable()) {
            return false;
        }

        /**
         * Regular advertiser created campaign with special ads targeting, then campaign went live.
         * Advertiser became special ads.
         * In this case we should not allow to edit such campaign.
         */
        if ($user->isSpecialAds()
            && $campaign->isLiveEditable()
            && !$this->validateCampaignTargetingsAction->validateSpecialAdTargetingAction($campaign)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can edit start date of the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function editStartDate(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_CAMPAIGN)) {
            return false;
        }

        if ($campaign->inState([
            CampaignStatus::ID_COMPLETED,
            CampaignStatus::ID_SUSPENDED,
            CampaignStatus::ID_PENDING_APPROVAL,
            CampaignStatus::ID_PROCESSING,
            CampaignStatus::ID_LIVE,
        ])
        ) {
            return false;
        }

        if (!$campaign->inState([CampaignStatus::ID_DRAFT, CampaignStatus::ID_READY, CampaignStatus::ID_PAUSED])) {
            return false;
        }

        if ($campaign->inState(CampaignStatus::ID_PAUSED) && !$campaign->date_start->gte(Carbon::today())) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can edit end date of the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function editEndDate(User $user, Campaignable $campaign): bool
    {
        return $this->update($user, $campaign);
    }

    /**
     * Returns true if user can edit budget of the campaign.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function editBudget(User $user, Campaignable $campaign): bool
    {
        if ($campaign->getOrigin()->hasPromocode()) {
            return false;
        }

        return $this->update($user, $campaign);
    }

    /**
     * Check if user can edit age targetings.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function editAgeTargetings(User $user, Campaignable $campaign): bool
    {
        return $this->targetingRules->checkEditAgeTargetings($user, $campaign);
    }

    /**
     * Check if user can access audience targetings.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function editAudienceTargetings(User $user, Campaignable $campaign): bool
    {
         return $this->targetingRules->checkEditAudienceTargetings($user, $campaign);
    }

    /**
     * Check if user can access location targetings.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function editLocationTargetings(User $user, Campaignable $campaign): bool
    {
        return $this->targetingRules->checkEditLocationTargeting($user, $campaign);
    }

    /**
     * Check if user can access zip targetings.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function editZipTargetings(User $user, Campaignable $campaign): bool
    {
        return $this->targetingRules->checkEditZipTargetings($user, $campaign);
    }

    /**
     * Check if user can access genre targetings.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function editGenreTargetings(User $user, Campaignable $campaign): bool
    {
        return $this->targetingRules->checkEditGenreTargetings($user, $campaign);
    }

    /**
     * Check if user can edit platform targetings.
     *
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function editPlatformTargetings(User $user, Campaignable $campaign): bool
    {
        return $this->targetingRules->checkEditPlatformTargetings($user, $campaign);
    }
}
