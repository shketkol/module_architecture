<?php

namespace Modules\Campaign\Policies\Rules;

use App\Policies\Traits\Checks;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Policies\CampaignPolicy;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Zipcode;
use Modules\User\Models\User;

class TargetingsRules
{
    use Checks;

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    protected function checkUpdate(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->hasPermissionTo(CampaignPolicy::PERMISSION_UPDATE_CAMPAIGN)) {
            return false;
        }

        if (!$this->isOwner($user, $campaign)) {
            return false;
        }

        if (!$campaign->isEditable()) {
            return false;
        }

        return true;
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function checkEditAgeTargetings(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->canAccessTargeting(AgeGroup::TYPE_NAME)) {
            return false;
        }

        return $this->checkUpdate($user, $campaign);
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function checkEditPlatformTargetings(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if ($campaign->inState(CampaignStatus::ID_DRAFT)) {
            return true;
        }

        if (!$this->checkUpdate($user, $campaign)) {
            return false;
        }

        return !($campaign->deviceGroups()->count() === DeviceGroup::where('visible', 1)->get()->count());
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function checkEditGenreTargetings(User $user, Campaignable $campaign)
    {
        $campaign = $campaign->getOrigin();

        if (!$user->canAccessTargeting(Genre::TYPE_NAME)) {
            return false;
        }

        return $this->checkUpdate($user, $campaign);
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function checkEditZipTargetings(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->canAccessTargeting(Zipcode::TYPE_NAME)) {
            return false;
        }

        if ($campaign->inState(CampaignStatus::ID_DRAFT)) {
            return true;
        }

        if (!$this->checkUpdate($user, $campaign)) {
            return false;
        }

        return $campaign->zipcodes()->exists() || $campaign->locations()->exists();
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function checkEditLocationTargeting(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->canAccessTargeting(Location::TYPE_NAME)) {
            return false;
        }

        if ($campaign->inState(CampaignStatus::ID_DRAFT)) {
            return true;
        }

        if (!$this->checkUpdate($user, $campaign)) {
            return false;
        }

        return $campaign->locations()->exists() || $campaign->zipcodes()->exists();
    }

    /**
     * @param User         $user
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function checkEditAudienceTargetings(User $user, Campaignable $campaign): bool
    {
        $campaign = $campaign->getOrigin();

        if (!$user->canAccessTargeting(Audience::TYPE_NAME)) {
            return false;
        }

        if ($campaign->inState(CampaignStatus::ID_DRAFT)) {
            return true;
        }

        if (!$this->checkUpdate($user, $campaign)) {
            return false;
        }

        return $campaign->audiences()->exists();
    }
}
