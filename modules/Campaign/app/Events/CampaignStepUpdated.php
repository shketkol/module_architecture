<?php

namespace Modules\Campaign\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Campaign\Models\Contracts\Campaignable;

class CampaignStepUpdated
{
    use SerializesModels;

    /**
     * @var Campaignable
     */
    public Campaignable $campaign;

    /**
     * Create a new event instance.
     *
     * @param Campaignable $campaign
     */
    public function __construct(Campaignable $campaign)
    {
        $this->campaign = $campaign;
    }
}
