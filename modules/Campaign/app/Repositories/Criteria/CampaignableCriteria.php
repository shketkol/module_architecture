<?php

namespace Modules\Campaign\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\Models\Contracts\Campaignable;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class CampaignableCriteria implements CriteriaInterface
{
    /**
     * @var Campaignable
     */
    private Campaignable $campaign;

    /**
     * @param Campaignable $campaign
     */
    public function __construct(Campaignable $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @param Builder             $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where('campaign_id', '=', $this->campaign->id)
            ->where('campaign_type', '=', get_class($this->campaign));
    }
}
