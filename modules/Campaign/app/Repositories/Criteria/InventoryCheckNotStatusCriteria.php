<?php

namespace Modules\Campaign\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class InventoryCheckNotStatusCriteria implements CriteriaInterface
{
    /**
     * @var int[]
     */
    private $statusIds;

    /**
     * @param int[] $statusIds
     */
    public function __construct(array $statusIds)
    {
        $this->statusIds = $statusIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder       $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->whereNotIn('status_id', $this->statusIds);
    }
}
