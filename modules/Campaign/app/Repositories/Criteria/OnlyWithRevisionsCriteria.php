<?php

namespace Modules\Campaign\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class OnlyWithRevisionsCriteria implements CriteriaInterface
{
    /**
     * @var int|null
     */
    private $status;

    /**
     * @param int|null $status
     */
    public function __construct(?int $status)
    {
        $this->status = $status;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        $model->join('campaign_revisions', 'campaign_revisions.campaign_id', '=', 'campaigns.id');
        if ($this->status) {
            $model->where('campaign_revisions.status_id', '=', $this->status);
        }

        return $model;
    }
}
