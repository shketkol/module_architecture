<?php

namespace Modules\Campaign\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\Models\CampaignStatus;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OngoingCampaignsCriteria implements CriteriaInterface
{
    /**
     * Get campaigns that has "live" or "ready" statuses
     *
     * @param Builder $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->whereIn('campaigns.status_id', [
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_READY,
            CampaignStatus::ID_PENDING_APPROVAL,
            CampaignStatus::ID_PAUSED,
        ]);
    }
}
