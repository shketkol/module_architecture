<?php

namespace Modules\Campaign\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ActiveCreativeCriteria implements CriteriaInterface
{
    /**
     * @var int
     */
    private $creativeId;

    /**
     * @param int $creativeId
     */
    public function __construct(int $creativeId)
    {
        $this->creativeId = $creativeId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->where('campaign_creative.creative_id', '=', $this->creativeId);
    }
}
