<?php

namespace Modules\Campaign\Repositories;

use App\Repositories\Repository;
use Modules\Campaign\Actions\Traits\CampaignTargetingAttach;
use Modules\Campaign\Actions\Traits\CampaignTargetingCollect;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignRevision;
use Modules\Campaign\Models\CampaignRevisionStatus;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Repositories\Criteria\CampaignCriteria;
use Modules\Campaign\Repositories\Criteria\StatusCriteria;

class CampaignRevisionRepository extends Repository
{
    use CampaignTargetingCollect,
        CampaignTargetingAttach;

    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return CampaignRevision::class;
    }

    /**
     * @param Campaignable $campaign
     * @param array        $attributes
     *
     * @return CampaignRevision
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function createRevision(Campaignable $campaign, array $attributes = []): CampaignRevision
    {
        $origin = $campaign->getOrigin();

        $revision = $this->create(array_merge([
            'campaign_id'  => $origin->id,
            'name'         => $origin->name,
            'date_start'   => $origin->date_start,
            'date_end'     => $origin->date_end,
            'budget'       => $origin->budget,
            'impressions'  => $origin->impressions,
        ], $attributes));

        $this->syncTargetings($revision, $campaign);
        $this->attachCreative($campaign, $revision);

        return $revision;
    }

    /**
     * @param Campaignable $instanceTo
     * @param Campaignable $instanceFrom
     *
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public function syncTargetings(Campaignable $instanceTo, Campaignable $instanceFrom): void
    {
        $this->detachRelations($instanceTo);
        $targetings = $this->collectCampaignTargetingsForDatabase($instanceFrom);

        $this->setTargetingValues($instanceTo, $targetings, ['campaign_type' => get_class($instanceTo)]);
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Campaignable
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function createLiveRevision(Campaignable $campaign): Campaignable
    {
        return $this->createRevision($campaign, [
            'status_id'    => CampaignRevisionStatus::ID_LIVE,
            'step_id'      => CampaignStep::ID_REVIEW,
            'submitted_at' => $campaign->submitted_at,
        ]);
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Campaignable
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function createDraftRevision(Campaignable $campaign): Campaignable
    {
        return $this->createRevision($campaign, [
            'status_id' => CampaignRevisionStatus::ID_DRAFT,
            'step_id'   => CampaignStep::ID_REVIEW,
        ]);
    }

    /**
     * @param Campaign $campaign
     *
     * @return CampaignRevision|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getLiveRevision(Campaign $campaign): ?CampaignRevision
    {
        return $this
            ->pushCriteria(new CampaignCriteria($campaign->id))
            ->pushCriteria(new StatusCriteria([CampaignRevisionStatus::ID_LIVE]))
            ->first();
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function issetDraftRevision(Campaign $campaign): bool
    {
        return $this->pushCriteria(new CampaignCriteria($campaign->id))
            ->pushCriteria(new StatusCriteria([CampaignRevisionStatus::ID_DRAFT]))
            ->exists();
    }
}
