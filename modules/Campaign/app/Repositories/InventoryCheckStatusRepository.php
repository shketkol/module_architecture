<?php

namespace Modules\Campaign\Repositories;

use App\Exceptions\ModelNotFoundException;
use App\Repositories\Repository;
use Modules\Campaign\Models\InventoryCheckStatus;

class InventoryCheckStatusRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return InventoryCheckStatus::class;
    }

    /**
     * @param string $status
     *
     * @return int
     * @throws ModelNotFoundException
     */
    public function findStatusId(string $status): int
    {
        $model = $this->findByField('name', $status)->first();

        if (!is_null($model)) {
            return $model->id;
        }

        throw new ModelNotFoundException(sprintf(
            'Inventory check status "%s" not found.',
            $status
        ));
    }
}
