<?php

namespace Modules\Campaign\Repositories;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Modules\Campaign\DataTable\Repositories\Criteria\AddCreativesCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\SelectCampaignsCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\WithoutDraftCriteria;
use Modules\Campaign\Repositories\Criteria\ActiveCreativeCriteria;
use Modules\Campaign\Repositories\Criteria\OngoingCampaignsCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\ActiveCampaignsCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\Contracts\CampaignRepository as CampaignRepositoryContract;
use Modules\Campaign\Repositories\Criteria\AddStatusCriteria;
use Modules\Campaign\Repositories\Criteria\ExcludeStatusCriteria;
use Modules\Campaign\Repositories\Criteria\OrderCriteria;
use Modules\Creative\Models\Creative;
use Modules\User\Models\User;
use Prettus\Repository\Exceptions\RepositoryException;

class CampaignRepository extends Repository implements CampaignRepositoryContract
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Campaign::class;
    }

    /**
     * @param User $user
     *
     * @return Collection
     * @throws RepositoryException
     */
    public function findActiveCampaignsByUser(User $user): Collection
    {
        return $this
            ->pushCriteria(new UserCriteria($user))
            ->pushCriteria(new ActiveCampaignsCriteria())
            ->get();
    }

    /**
     * @param User $user
     *
     * @return Collection
     * @throws RepositoryException
     */
    public function findOngoingCampaignsByUser(User $user): Collection
    {
        return $this
            ->pushCriteria(new UserCriteria($user))
            ->pushCriteria(new OngoingCampaignsCriteria())
            ->get();
    }

    /**
     * @param string $externalId
     *
     * @return Campaign
     * @throws CampaignNotFoundException
     */
    public function findCampaignByExternalId(string $externalId): Campaign
    {
        $campaign = $this->findByField('external_id', $externalId)->first();

        if (is_null($campaign)) {
            throw CampaignNotFoundException::create($externalId);
        }

        return $campaign;
    }

    /**
     * @param array $statusIds
     *
     * @return CampaignRepository
     * @throws RepositoryException
     */
    public function byStatus(array $statusIds): self
    {
        return $this->pushCriteria(new AddStatusCriteria($statusIds));
    }

    /**
     * @param array $statusIds
     *
     * @return CampaignRepository
     * @throws RepositoryException
     */
    public function excludeStatus(array $statusIds): self
    {
        return $this->pushCriteria(new ExcludeStatusCriteria($statusIds));
    }

    /**
     * @param User $user
     *
     * @return CampaignRepository
     * @throws RepositoryException
     */
    public function byUser(User $user): self
    {
        return $this->pushCriteria(new UserCriteria($user));
    }

    /**
     * @param int $id
     *
     * @return CampaignRepository
     * @throws CampaignNotFoundException
     */
    public function byId(int $id)
    {
        $campaign = $this->findByField('id', $id)->first();

        if (is_null($campaign)) {
            throw CampaignNotFoundException::create($id);
        }

        return $campaign;
    }

    /**
     * @param string $orderId
     *
     * @return Campaign
     * @throws RepositoryException
     * @throws CampaignNotFoundException
     */
    public function findCampaignByOrderId(string $orderId): Campaign
    {
        $campaign = $this->pushCriteria(new OrderCriteria($orderId))->first();

        if (is_null($campaign)) {
            throw CampaignNotFoundException::create($orderId);
        }

        $this->reset();

        return $campaign;
    }

    /**
     * @param Creative $creative
     *
     * @return $this
     * @throws RepositoryException
     */
    public function byActiveCreative(Creative $creative): self
    {
        return $this
            ->pushCriteria(new SelectCampaignsCriteria())
            ->pushCriteria(new AddCreativesCriteria())
            ->pushCriteria(new ActiveCreativeCriteria($creative->id));
    }

    /**
     * @return CampaignRepository
     * @throws RepositoryException
     */
    public function onlySubmitted(): self
    {
        return $this->pushCriteria(new WithoutDraftCriteria());
    }
}
