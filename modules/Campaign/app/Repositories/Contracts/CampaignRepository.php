<?php

namespace Modules\Campaign\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Campaign\Models\Campaign;
use Modules\User\Models\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CampaignRepository
 *
 * @package Modules\Campaign\Repositories\Contracts
 */
interface CampaignRepository extends RepositoryInterface
{
    /**
     * @param User $user
     *
     * @return Collection
     */
    public function findActiveCampaignsByUser(User $user): Collection;

    /**
     * @param string $externalId
     *
     * @return Campaign
     */
    public function findCampaignByExternalId(string $externalId): Campaign;

    /**
     * @param User $user
     *
     * @return Collection
     */
    public function findOngoingCampaignsByUser(User $user): Collection;
}
