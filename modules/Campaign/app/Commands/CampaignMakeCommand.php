<?php

namespace Modules\Campaign\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\Creative;
use Modules\User\Models\User;

class CampaignMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:campaign {user_id} {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate test campaigns for user.';

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * CampaignMakeCommand constructor.
     *
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        parent::__construct();
        $this->databaseManager = $databaseManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        $user = User::findOrFail($this->argument('user_id'));
        $count = $this->argument('count');

        $this->info(sprintf('Creating %d campaign(s) for User#%d', $count, $user->getKey()));
        $this->databaseManager->beginTransaction();

        try {
            for ($i = 1; $i <= $count; $i++) {
                $campaign = $this->createCampaign($user);
                $this->createCreative($campaign, $user);
            }

            $this->info('Done.');
        } catch (\Throwable $exception) {
            $this->warn(sprintf('Campaign creating failed. Reason: "%s".', $exception->getMessage()));
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * @param User $user
     *
     * @return Campaign
     */
    private function createCampaign(User $user): Campaign
    {
        $this->info('Creating campaign.');

        return Campaign::factory()->create([
            'user_id' => $user->id,
        ]);
    }

    /**
     * @param Campaign $campaign
     * @param User     $user
     */
    private function createCreative(Campaign $campaign, User $user): void
    {
        $doCreate = mt_rand(0, 1);

        if (!$doCreate) {
            $this->info('Creating creative skipped due random.');
            return;
        }

        $this->info('Creative creating.');

        $campaign->creatives()->attach(
            Creative::factory()->create(['user_id' => $user->id]),
            ['is_active' => true]
        );
    }
}
