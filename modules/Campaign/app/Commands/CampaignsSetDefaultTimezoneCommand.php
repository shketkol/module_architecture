<?php

namespace Modules\Campaign\Commands;

use App\Models\Timezone;
use Illuminate\Console\Command;
use Modules\Campaign\Models\Campaign;

class CampaignsSetDefaultTimezoneCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaigns:set-default-timezone';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set default timezone for all campaigns';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        $defaultTimezoneCode = config('date.default_timezone_code');
        $defaultTimezoneId   = Timezone
            ::where('code', $defaultTimezoneCode)
            ->first()
            ->id;

        $this->line(sprintf('Setting all campaigns dates to default timezone - %s', $defaultTimezoneCode));

        Campaign
            ::where('timezone_id', '!=', $defaultTimezoneId)
            ->update(['timezone_id' => $defaultTimezoneId]);

        $this->line('All campaigns were updated');
    }
}
