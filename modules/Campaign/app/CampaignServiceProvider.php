<?php

namespace Modules\Campaign;

use App\Providers\ModuleServiceProvider;
use Modules\Campaign\Commands\CampaignMakeCommand;
use Modules\Campaign\Commands\CampaignsSetDefaultTimezoneCommand;
use Modules\Campaign\DataTable\Repositories\CampaignsDataTableRepository;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository
    as CampaignsDataTableRepositoryInterface;
use Modules\Campaign\Policies\CampaignPolicy;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Campaign\Repositories\Contracts\CampaignRepository as CampaignRepositoryInterface;
use Modules\Campaign\States\CompletedState;
use Modules\Campaign\States\LiveState;
use Modules\Campaign\States\StatusChangedState;

class CampaignServiceProvider extends ModuleServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        'state-machine.campaign.states.completed'      => CompletedState::class,
        'state-machine.campaign.states.live'           => LiveState::class,
        'state-machine.campaign.states.status-changed' => StatusChangedState::class,
        CampaignRepositoryInterface::class             => CampaignRepository::class,
        CampaignsDataTableRepositoryInterface::class   => CampaignsDataTableRepository::class,
    ];

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'campaign' => CampaignPolicy::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        CampaignMakeCommand::class,
        CampaignsSetDefaultTimezoneCommand::class,
    ];

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'campaign';
    }

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();
        $this->loadRoutes();
        $this->loadConfigs([
            'campaign',
            'state-machine',
            'inventory-check'
        ]);
        $this->commands($this->commands);
    }
}
