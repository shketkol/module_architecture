<?php

namespace Modules\Campaign\States;

use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Notifications\Advertiser\Started;

class LiveState extends CampaignState
{
    /**
     * @param Campaign $campaign
     */
    public function before(Campaign $campaign): void
    {
        if ($campaign->isPaused()) {
            return;
        }

        // Send notification to advertiser
        $this->notification->send(
            $campaign->user,
            new Started($campaign)
        );
    }
}
