<?php

namespace Modules\Campaign\States;

use Carbon\Carbon;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Sebdesign\SM\Event\TransitionEvent;

class StatusChangedState extends CampaignState
{
    /**
     * Allow changing status from PROCESSING to another status:
     * - if campaign has empty previous_status_id
     * - if campaign previous_status_id was LIVE, READY, PAUSED
     * - if transition_id === previous_status_id
     * - if transition === SUSPENDED
     *
     * @param Campaign        $campaign
     * @param TransitionEvent $event
     *
     * @throws CanNotApplyStatusException
     * @throws \App\Exceptions\InvalidArgumentException
     */
    public function handleFromProcessingTransition(Campaign $campaign, TransitionEvent $event): void
    {
        if (is_null($campaign->previous_status_id)) {
            return;
        }

        if (!in_array($campaign->previous_status_id, CampaignStatus::ACTIVE_STATUSES)) {
            return;
        }

        if (!$campaign->isProcessing()) {
            return;
        }

        $transition = $event->getTransition();

        if (CampaignStatus::SUSPENDED === $transition) {
            return;
        }

        $transitionId = CampaignStatus::getIdByTransition($transition);

        if ($campaign->previous_status_id === $transitionId) {
            return;
        }

        throw CanNotApplyStatusException::create($campaign, $transition);
    }

    /**
     * @param Campaign $campaign
     */
    public function before(Campaign $campaign): void
    {
        $campaign->previous_status_id = $campaign->status_id;
        $campaign->save();
    }

    /**
     * @param Campaign $campaign
     */
    public function after(Campaign $campaign): void
    {
        $campaign->status_changed_at = Carbon::now();
        $campaign->save();
    }
}
