<?php

namespace Modules\Campaign\States;

use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Notifications\Advertiser\Completed;

class CompletedState extends CampaignState
{
    /**
     * @param Campaign $campaign
     */
    public function after(Campaign $campaign): void
    {
        // Send notification to advertiser
        $this->notification->send(
            $campaign->user,
            new Completed($campaign)
        );
    }
}
