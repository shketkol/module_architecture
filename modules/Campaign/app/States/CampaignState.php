<?php

namespace Modules\Campaign\States;

use Illuminate\Contracts\Notifications\Dispatcher;

class CampaignState
{
    /**
     * @var Dispatcher
     */
    protected $notification;

    /**
     * CampaignState constructor.
     * @param Dispatcher $notification
     */
    public function __construct(Dispatcher $notification)
    {
        $this->notification = $notification;
    }
}
