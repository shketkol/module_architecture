<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Controllers\Api\Traits\CampaignName;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Contracts\Campaignable;

class UpdateCampaignNameAction
{
    use CampaignName;

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var UpdateCampaignStepAction
     */
    private UpdateCampaignStepAction $stepAction;

    /**
     * @param Logger                   $log
     * @param UpdateCampaignStepAction $stepAction
     */
    public function __construct(Logger $log, UpdateCampaignStepAction $stepAction)
    {
        $this->log = $log;
        $this->stepAction = $stepAction;
    }

    /**
     * @param Campaignable $campaign
     * @param string|null  $name
     *
     * @return Campaignable
     * @throws CampaignNotUpdatedException
     */
    public function handle(Campaignable $campaign, ?string $name): Campaignable
    {
        $this->log->info('Updating campaign name.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'name'          => $name,
        ]);

        $campaign->name = $this->truncate($name);

        $this->stepAction->handle($campaign, CampaignStep::ID_NAME);

        if (!$campaign->save()) {
            $this->log->warning('Campaign name was not updated.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
            ]);

            throw CampaignNotUpdatedException::create();
        }

        $this->log->info('Campaign name was successfully saved.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        return $campaign;
    }
}
