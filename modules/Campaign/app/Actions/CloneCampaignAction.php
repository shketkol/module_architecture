<?php

namespace Modules\Campaign\Actions;

use App\Exceptions\BaseException;
use App\Helpers\Math\CalculationHelper;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Traits\CampaignTargetingAttach;
use Modules\Campaign\Actions\Traits\CampaignTargetingCollect;
use Modules\Campaign\Exceptions\CampaignNotCreatedException;
use Modules\Campaign\Http\Controllers\Api\Traits\StoreCampaign;
use Modules\Campaign\Models\Campaign;

class CloneCampaignAction
{
    use StoreCampaign,
        CampaignTargetingCollect,
        CampaignTargetingAttach;

    public const NAME_PREFIX = 'Copy - ';

    /**
     * @var Logger
     */
    protected Logger $log;

    /**
     * @var DatabaseManager
     */
    protected DatabaseManager $databaseManager;

    /**
     * @var GetCampaignPriceAction
     */
    protected GetCampaignPriceAction $getPriceAction;

    /**
     * @param DatabaseManager        $databaseManager
     * @param GetCampaignPriceAction $getPriceAction
     * @param Logger                 $log
     */
    public function __construct(
        DatabaseManager $databaseManager,
        GetCampaignPriceAction $getPriceAction,
        Logger $log
    ) {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->getPriceAction = $getPriceAction;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws BaseException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign): Campaign
    {
        $this->log->info('Start clone campaign.', ['origin_campaign_id' => $campaign->id]);

        $this->databaseManager->beginTransaction();

        $data = $this->getCampaignData($campaign);

        try {
            $clonedCampaign = $this->storeCampaign($data, $campaign->step_id);
            $targetingValues = $this->collectAllCampaignTargetings($campaign);

            $this->setTargetingValues($clonedCampaign, $targetingValues);
            if ($clonedCampaign->budget > 0) {
                $this->setPrice($clonedCampaign);
            }
            $this->attachCreative($campaign, $clonedCampaign);
        } catch (\Throwable $throwable) {
            $this->databaseManager->rollBack();

            $this->log->info('Fail clone campaign.', [
                'origin_campaign_id' => $campaign->id,
            ]);

            throw CampaignNotCreatedException::createFrom($throwable);
        }

        $this->databaseManager->commit();

        $this->log->info('Finish clone campaign.', [
            'origin_campaign_id' => $campaign->id,
            'cloned_campaign_id' => $clonedCampaign->id,
        ]);

        return $clonedCampaign;
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     */
    private function getCampaignData(Campaign $campaign): array
    {
        $attributesForClone = [
            'budget',
            'cpm',
            'date_start',
            'date_end',
            'name',
            'timezone_id',
        ];

        $tomorrow = Carbon::tomorrow(config('campaign.wizard.date.timezone'))
            ->format(config('date.format.db_date_time'));

        if ($campaign->date_start && $campaign->date_end && $campaign->date_start < $tomorrow) {
            $campaign = $this->createNewDates($campaign);
        }

        $attributes = $campaign->getAttributes();
        $attributes = Arr::only($attributes, $attributesForClone);
        Arr::set($attributes, 'name', self::NAME_PREFIX . Arr::get($attributes, 'name'));

        return $attributes;
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function setPrice(Campaign $campaign): void
    {
        $cpm = Arr::get($this->getPriceAction->handle($campaign), 'unitCost');
        $campaign->cpm = $cpm;
        $campaign->impressions = CalculationHelper::calculateImpressions($cpm, $campaign->budget);
        $campaign->save();
    }
}
