<?php

namespace Modules\Campaign\Actions;

use Illuminate\Support\Arr;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Haapi\Actions\Campaign\CampaignImpressionsDetail;
use Modules\Haapi\DataTransferObjects\Campaign\ImpressionsDetailParams;
use Modules\User\Models\User;

class GetCampaignImpressionsDetailAction
{
    /**
     * @var CampaignImpressionsDetail
     */
    private CampaignImpressionsDetail $action;

    /**
     * @param CampaignImpressionsDetail $action
     */
    public function __construct(CampaignImpressionsDetail $action)
    {
        $this->action = $action;
    }

    /**
     * @param Campaignable $campaign
     * @param User         $user
     * @param string       $rollup
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(
        Campaignable $campaign,
        User $user,
        string $rollup = ImpressionsDetailParams::ROLL_UP_DAILY
    ): array {
        return $campaign->getExternalId() ? $this->getImpressionsDetail($campaign, $user, $rollup) : [];
    }

    /**
     * Get all impressions detail for the campaign.
     *
     * @param Campaignable $campaign
     * @param User         $user
     * @param string       $rollup
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function getImpressionsDetail(Campaignable $campaign, User $user, string $rollup): array
    {
        $data = $this->action->handle(new ImpressionsDetailParams([
            'advertiserId' => $campaign->user->getExternalId(),
            'campaignIds'  => [$campaign->getExternalId()],
            'rollUpType'   => $rollup,
        ]), $user->getKey());

        $payload = Arr::first($data);

        return Arr::first($payload, null, []);
    }
}
