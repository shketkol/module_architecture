<?php

namespace Modules\Campaign\Actions;

use Modules\Campaign\Models\InventoryCheckHistory;

class CalculateDeliveryIndicatorArrowPositionAction
{
    /**
     * @param InventoryCheckHistory $history
     *
     * @return float
     */
    public function handle(InventoryCheckHistory $history): float
    {
        if ($history->available_impressions === 0) {
            return 0;
        }

        $minValue = $this->calculateMinimumImpressionsForCampaign($history);
        $maxValue = $history->ordered_impressions * 4; // approximately

        if ($maxValue > $history->available_impressions) {
            $maxValue = $history->available_impressions;
        }

        // Set arrow between 0% and 10% (red arc)
        $minPercentage = 10; // based on chartColor from InventoryDeliveryIndicator.vue and myself
        if ($minValue > $maxValue) {
            return $this->round($maxValue * $minPercentage / $minValue);
        }

        // Set arrow between 10% and 30% (yellow arc)
        // find arrow position if it's more then minimum but less then ordered
        // ordered_impressions = 10% - because it's part from red to yellow arc (30% - 10%)
        // maxValue percentage = available_impressions * 10% / ordered_impressions
        // and then +10% to start from red arc
        $minOrderedPercentage = 25; // based on chartColor from InventoryDeliveryIndicator.vue and myself
        if ($history->ordered_impressions > $maxValue) {
            return $this->round(($maxValue * ($minOrderedPercentage - $minPercentage) / $history->ordered_impressions)
                + $minPercentage);
        }

        // Set arrow between 30% and 100% (green arc)
        // according to graph in HD-1099 our delivery indicator split in 4 parts with step 25%:
        // 1 part: 0 - 25%
        // 2 part: 25 - 50%
        // 3 part: 50 - 75%
        // 4 part: 75 - 100%
        // so to place arrow in delivery indicator if we have more then 100% available impressions
        // we would use this formula: available_impressions / ordered_impressions * 25
        $arcSize = 25;
        $currentPercent = $this->round($maxValue / $history->ordered_impressions * $arcSize);

        if ($currentPercent > 100) {
            $currentPercent = 100;
        }

        return $currentPercent;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return int
     */
    private function calculateMinimumImpressionsForCampaign(InventoryCheckHistory $history): int
    {
        return $history->campaign->getDuration() * config('campaign.min_daily_impressions');
    }

    /**
     * @param float $value
     *
     * @return float
     */
    private function round(float $value): float
    {
        return number_format($value, 2);
    }
}
