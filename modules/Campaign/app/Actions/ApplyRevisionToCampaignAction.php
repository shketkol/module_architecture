<?php

namespace Modules\Campaign\Actions;

use App\Exceptions\InvalidArgumentException;
use Modules\Campaign\Actions\Traits\CampaignTargetingAttach;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignRevision;
use Modules\Campaign\Models\CampaignRevisionStatus;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRevisionRepository;
use Psr\Log\LoggerInterface;

class ApplyRevisionToCampaignAction
{
    use CampaignTargetingAttach;

    /**
     * @var Campaign
     */
    protected Campaign $campaign;

    /**
     * @var CampaignRevision
     */
    protected CampaignRevision $campaignRevision;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $log;

    /**
     * @var CampaignRevisionRepository
     */
    protected CampaignRevisionRepository $revisionRepository;

    /**
     * @param LoggerInterface            $log
     * @param CampaignRevisionRepository $revisionRepository
     */
    public function __construct(LoggerInterface $log, CampaignRevisionRepository $revisionRepository)
    {
        $this->log = $log;
        $this->revisionRepository = $revisionRepository;
    }

    /**
     * @param Campaign         $campaign
     * @param CampaignRevision $campaignRevision
     *
     * @return void
     * @throws InvalidArgumentException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \SM\SMException
     */
    public function handle(Campaign $campaign, CampaignRevision $campaignRevision): void
    {
        $this->campaign = $campaign;
        $this->campaignRevision = $campaignRevision;

        $this->checkRevision();
        $this->setCampaignAttributes();
        $this->revisionRepository->syncTargetings($this->campaign, $this->campaignRevision);
        $this->attachCreative($this->campaignRevision, $this->campaign);
        $this->setCampaignStatus();
        $this->updateRevisions();

        $this->campaign->save();
    }

    /**
     * @return void
     */
    protected function setCampaignAttributes(): void
    {
        $this->campaign->fill([
            'name'        => $this->campaignRevision->name,
            'date_start'  => $this->campaignRevision->date_start,
            'date_end'    => $this->campaignRevision->date_end,
            'budget'      => $this->campaignRevision->budget,
            'impressions' => $this->campaignRevision->impressions,
        ]);
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function setCampaignStatus(): void
    {
        $previousState = CampaignStatus::getTransitionById($this->campaign->previous_status_id);
        $this->campaign->applyInternalStatus($previousState);
    }

    /**
     * @return void
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \SM\SMException
     */
    protected function updateRevisions(): void
    {
        $liveRevision = $this->campaign->liveRevision();
        $liveRevision
            ? $liveRevision->applyInternalStatus(CampaignRevisionStatus::SUBMITTED)
            : $this->log->warning(sprintf("Live revision for campaign with ID %d wasn't found", $this->campaign->id));

        $this->campaignRevision->applyInternalStatus(CampaignRevisionStatus::LIVE);
    }

    /**
     * @return void
     * @throws InvalidArgumentException
     */
    protected function checkRevision(): void
    {
        if ($this->campaignRevision->campaign_id !== $this->campaign->id) {
            throw new InvalidArgumentException("Campaign's revision doesn't exists");
        }

        if (!$this->campaignRevision->isProcessing()) {
            throw new InvalidArgumentException(
                'Campaign revision should be in PROCESSING state to be applied to campaign'
            );
        }
    }
}
