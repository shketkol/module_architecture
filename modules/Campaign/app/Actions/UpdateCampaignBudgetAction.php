<?php

namespace Modules\Campaign\Actions;

use App\Helpers\Math\CalculationHelper;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Traits\FilterBudgetValues;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Contracts\Campaignable;

class UpdateCampaignBudgetAction
{
    use FilterBudgetValues;

    /**
     * @var GetCampaignPriceAction
     */
    private GetCampaignPriceAction $getCampaignPriceAction;

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var UpdateCampaignStepAction
     */
    private UpdateCampaignStepAction $stepAction;

    /**
     * @param GetCampaignPriceAction   $getCampaignPriceAction
     * @param Logger                   $log
     * @param UpdateCampaignStepAction $stepAction
     */
    public function __construct(
        GetCampaignPriceAction $getCampaignPriceAction,
        Logger $log,
        UpdateCampaignStepAction $stepAction
    ) {
        $this->getCampaignPriceAction = $getCampaignPriceAction;
        $this->log = $log;
        $this->stepAction = $stepAction;
    }

    /**
     * @param Campaignable $campaign
     * @param array        $data
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function handle(Campaignable $campaign, array $data = []): Campaignable
    {
        $this->log->info('Updating campaign budget.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'data'          => $data,
        ]);

        $budgetData = $this->getBudgetData($campaign, $data);
        $campaign->fill($budgetData);

        if (count($data)) {
            $this->stepAction->handle($campaign, CampaignStep::ID_BUDGET);
        }

        if (!$campaign->save()) {
            $this->log->warning('Campaign budget was not updated.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
            ]);
            throw CampaignNotUpdatedException::create();
        }

        $this->log->info('Campaign budget was successfully updated.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        return $campaign->refresh();
    }

    /**
     * Get budget data (all attributes to fill the model).
     *
     * @param Campaignable $campaign
     * @param array        $data
     *
     * @return array
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    protected function getBudgetData(Campaignable $campaign, array $data = []): array
    {
        $response = $this->getCampaignPriceAction->handle($campaign);
        $cpm = Arr::get($response, 'unitCost');
        (float)$cost = Arr::get($data, 'cost') !== null ? Arr::get($data, 'cost') : $campaign->budget;

        return [
            'cpm'             => $this->filterBudgetData('cpm', $cpm),
            'budget'          => $this->filterBudgetData('cost', $cost),
            'cost'            => $this->filterBudgetData('cost', $cost),
            'impressions'     => $this->filterBudgetData(
                'impressions',
                CalculationHelper::calculateImpressions($cpm, $cost)
            ),
        ];
    }
}
