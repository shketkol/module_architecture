<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Database\DatabaseManager;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Validations\ValidateCampaignAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Notifications\Admin\Ordered\Ordered as AdminOrderedNotification;
use Modules\Campaign\Notifications\Advertiser\Ordered\Ordered as AdvertiserOrderedNotification;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Creative\Models\CreativeStatus;
use Modules\Haapi\Actions\Campaign\SendCampaignAction;
use Modules\Payment\Actions\StoreOrderAction;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;

class OrderCampaignAction
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $log;

    /**
     * @var Dispatcher
     */
    protected Dispatcher $notification;

    /**
     * @var StoreOrderAction
     */
    protected StoreOrderAction $storePaymentAction;

    /**
     * @var DatabaseManager
     */
    protected DatabaseManager $databaseManager;

    /**
     * @var InventoryCheckHistoryRepository
     */
    protected InventoryCheckHistoryRepository $historyRepository;

    /**
     * @var SendCampaignAction
     */
    protected SendCampaignAction $sendCampaignAction;

    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * @var ValidateCampaignAction
     */
    private ValidateCampaignAction $validateCampaignAction;

    /**
     * @param DatabaseManager                 $databaseManager
     * @param Dispatcher                      $notification
     * @param LoggerInterface                 $log
     * @param SendCampaignAction              $sendCampaignAction
     * @param StoreOrderAction                $storePaymentAction
     * @param UserRepository                  $userRepository
     * @param InventoryCheckHistoryRepository $historyRepository
     * @param ValidateCampaignAction          $validateCampaignAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        Dispatcher $notification,
        LoggerInterface $log,
        SendCampaignAction $sendCampaignAction,
        StoreOrderAction $storePaymentAction,
        UserRepository $userRepository,
        InventoryCheckHistoryRepository $historyRepository,
        ValidateCampaignAction $validateCampaignAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->notification = $notification;
        $this->sendCampaignAction = $sendCampaignAction;
        $this->storePaymentAction = $storePaymentAction;
        $this->userRepository = $userRepository;
        $this->historyRepository = $historyRepository;
        $this->validateCampaignAction = $validateCampaignAction;
    }

    /**
     * Order the campaign.
     *
     * @param Campaign $campaign
     * @param int $paymentId
     *
     * @return Campaign
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign, int $paymentId): Campaign
    {
        $this->databaseManager->beginTransaction();

        try {
            $this->validateCampaignAction->handle($campaign);
            $this->checkCampaignCreative($campaign);
            $this->savePayment($campaign, $paymentId);
            $this->createHaapiCampaign($campaign);
            $this->updateCampaignStatus($campaign);
            $this->updateCreativeStatus($campaign);
            $this->sendNotification($campaign);
        } catch (\Throwable $exception) {
            $message = trans('campaign::messages.order.failed');
            $this->log->warning($message, [
                'campaign_id' => $campaign->id,
                'reason'      => $exception->getMessage() ?? get_class($exception),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        try {
            $this->historyRepository->deleteCampaignHistory($campaign);
        } catch (\Throwable $exception) {
            //mute exception since it is not critical flow operation
            $this->log->warning($exception->getMessage());
        }

        return $campaign->refresh();
    }

    /**
     * @param Campaign $campaign
     * @param int $paymentId
     *
     * @throws \Modules\Campaign\Exceptions\CampaignNotFoundException
     */
    private function savePayment(Campaign $campaign, int $paymentId): void
    {
        $this->storePaymentAction->handle($campaign, [
            'payment_method_id' => $paymentId,
        ]);
    }

    /**
     * @param Campaign $campaign
     *
     * @throws CampaignNotUpdatedException
     * @throws \SM\SMException
     */
    private function updateCampaignStatus(Campaign $campaign): void
    {
        $this->log->info('Ordering the campaign.', ['campaign_id' => $campaign->id]);

        $campaign->getState()->apply(CampaignStatus::PROCESSING);
        $campaign->submitted_at = Carbon::now();

        if (!$campaign->save()) {
            $message = trans('campaign::messages.order.failed');
            $this->log->warning($message, ['campaign_id' => $campaign->id]);
            throw CampaignNotUpdatedException::create($message);
        }

        $this->log->info('Campaign was successfully ordered.', ['campaign_id' => $campaign->id]);
    }

    /**
     * @param Campaign $campaign
     * @throws \SM\SMException
     */
    protected function updateCreativeStatus(Campaign $campaign): void
    {
        $creative = $campaign->getActiveCreative();

        if (!$creative || !$creative->inState(CreativeStatus::ID_DRAFT)) {
            return;
        }

        $creative->getState()->apply(CreativeStatus::PROCESSING);
        $creative->save();
    }

    /**
     * @param Campaign $campaign
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function createHaapiCampaign(Campaign $campaign): void
    {
        $this->sendCampaignAction->handle($campaign);
    }

    /**
     * Send email and on-site notifications.
     *
     * @param Campaign $campaign
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function sendNotification(Campaign $campaign): void
    {
        // Send notification to admin
        $this->notification->send(
            $this->userRepository->getAdmin(),
            new AdminOrderedNotification($campaign)
        );

        // Send notification to advertiser
        $this->notification->send(
            $campaign->user,
            new AdvertiserOrderedNotification($campaign)
        );
    }

    /**
     * Check campaign creative
     *
     * @param Campaign $campaign
     * @throws ValidationException
     */
    protected function checkCampaignCreative(Campaign $campaign): void
    {
        $creative = $campaign->getActiveCreative();

        if ($creative && $creative->inState(CreativeStatus::ID_PROCESSING)) {
            throw ValidationException::withMessages([__('creative::messages.replace_to_processing')]);
        }
    }
}
