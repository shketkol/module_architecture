<?php

namespace Modules\Campaign\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Haapi\Actions\Campaign\CampaignFrequencyDetail;
use Modules\User\Models\User;

class GetCampaignFrequencyDetailAction
{
    /**
     * @var CampaignFrequencyDetail
     */
    protected $action;

    /**
     * @param CampaignFrequencyDetail $action
     */
    public function __construct(CampaignFrequencyDetail $action)
    {
        $this->action = $action;
    }

    /**
     * @param Campaign             $campaign
     * @param User|Authenticatable $user
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaign $campaign, User $user): array
    {
        return $campaign->getExternalId() ? $this->getFrequencyDetails($campaign, $user) : [];
    }

    /**
     * Get frequency detail for the campaign.
     *
     * @param Campaign $campaign
     * @param User     $user
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function getFrequencyDetails(Campaign $campaign, User $user): array
    {
        $data = $this->action->handle($campaign->getExternalId(), $user->getKey());

        return Arr::first(Arr::get($data, 'lineItems'), null, []);
    }
}
