<?php

namespace Modules\Campaign\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Campaign\Actions\Traits\GetCampaignCost;
use Modules\Campaign\Actions\Traits\GetCampaignDeliveredImpressions;
use Modules\Campaign\Actions\Traits\GetCampaignPerformance;
use Modules\Campaign\Actions\Traits\GetGenericStructure;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\User\Models\User;

class GetCampaignStatisticsAction
{
    use GetCampaignDeliveredImpressions,
        GetCampaignCost,
        GetCampaignPerformance,
        GetGenericStructure;

    /**
     * @var Authenticatable|User
     */
    private $user;

    /**
     * @var GetCampaignImpressionsDetailAction
     */
    private GetCampaignImpressionsDetailAction $action;

    /**
     * @param Authenticatable                    $user
     * @param GetCampaignImpressionsDetailAction $action
     */
    public function __construct(Authenticatable $user, GetCampaignImpressionsDetailAction $action)
    {
        $this->user = $user;
        $this->action = $action;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(Campaignable $campaign): array
    {
        if (!$campaign->getExternalId()) {
            return $this->getGenericStructure();
        }

        $impressionsDetails = $this->action->handle($campaign, $this->user);

        if (empty($impressionsDetails)) {
            return $this->getGenericStructure();
        }

        return [
            'cost'                 => $this->getCost($impressionsDetails),
            'performance'          => $this->getPerformance($impressionsDetails),
            'deliveredImpressions' => $this->getDeliveredImpressions($impressionsDetails),
        ];
    }
}
