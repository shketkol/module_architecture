<?php

namespace Modules\Campaign\Actions;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Campaign\Repositories\Criteria\NameCampaignCriteria;
use Modules\Campaign\Repositories\Criteria\StatusCriteria;
use Modules\User\Repositories\Criteria\UserIdCriteria;

/**
 * Class SearchCampaignsAction
 * @package Modules\Campaign\Actions
 */
class SearchCampaignsAction
{
    /**
     * Default limit campaign
     */
    public const CAMPAIGNS_LIMIT = 30;

    /**
     * @var CampaignRepository
     */
    private CampaignRepository $campaignRepository;

    /**
     * @param CampaignRepository $campaignRepository
     */
    public function __construct(CampaignRepository $campaignRepository)
    {
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handler(array $data)
    {
        $queryCampaigns = $this->campaignRepository->pushCriteria(
            new UserIdCriteria(Auth::user()->id)
        );

        if (Arr::has($data, 'search') && Arr::get($data, 'search') !== null) {
            $queryCampaigns->pushCriteria(
                new NameCampaignCriteria(Arr::get($data, 'search'))
            );
        }

        if (Arr::has($data, 'statuses')) {
            $queryCampaigns->pushCriteria(
                new StatusCriteria(Arr::get($data, 'statuses'))
            );
        }

        return $queryCampaigns->take($this->getLimit($data))->get();
    }

    /**
     * @param array $data
     *
     * @return int
     */
    public function getLimit(array $data): int
    {
        return Arr::has($data, 'limit')
            ? Arr::get($data, 'limit')
            : self::CAMPAIGNS_LIMIT;
    }
}
