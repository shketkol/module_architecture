<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Traits\FilterBudgetValues;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\Contracts\Campaignable;

class UpdateCampaignPerformanceAction
{
    use FilterBudgetValues;

    /**
     * @var GetCampaignStatisticsAction
     */
    private GetCampaignStatisticsAction $action;

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @param GetCampaignStatisticsAction $action
     * @param Logger                      $log
     */
    public function __construct(GetCampaignStatisticsAction $action, Logger $log)
    {
        $this->action = $action;
        $this->log = $log;
    }

    /**
     * Update campaign performance data(delivered_impressions/cost).
     *
     * @param Campaign $campaign
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(Campaign $campaign): Campaign
    {
        $this->log->info('Updating campaign performance.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'external_id'   => $campaign->external_id,
        ]);

        $performanceData = $this->action->handle($campaign);

        $campaign->fill([
            'delivered_impressions' => $this->filterBudgetData(
                'impressions',
                Arr::get($performanceData, 'deliveredImpressions')
            ),
            'cost'                  => $this->filterBudgetData(
                'cost',
                Arr::get($performanceData, 'cost')
            ),
        ]);

        if (!$campaign->save()) {
            $this->log->warning('Campaign performance cannot be saved to DB.');
            throw CampaignNotUpdatedException::create();
        }

        $this->log->info('Campaign performance was successfully updated.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'external_id'   => $campaign->external_id,
        ]);

        return $campaign->refresh();
    }
}
