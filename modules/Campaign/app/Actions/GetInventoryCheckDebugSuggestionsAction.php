<?php

namespace Modules\Campaign\Actions;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Campaign\Helpers\InventoryCheckHistoryHelper;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Models\InventoryCheckStatus;
use Modules\Targeting\Models\TargetingValue;

class GetInventoryCheckDebugSuggestionsAction
{
    /**
     * @var InventoryCheckHistoryHelper
     */
    private $historyHelper;

    /**
     * @param InventoryCheckHistoryHelper $historyHelper
     */
    public function __construct(InventoryCheckHistoryHelper $historyHelper)
    {
        $this->historyHelper = $historyHelper;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    public function handle(InventoryCheckHistory $history): array
    {
        if ($history->status_id === InventoryCheckStatus::ID_AVAILABLE) {
            return ['👌'];
        }

        [
            'dates'      => $dates,
            'targetings' => $targetings,
        ] = $this->historyHelper->findDiffBetweenSuccessfulAndFailedInventoryCheck($history);

        $messages = ['Possible reasons that failed inventory check.'];

        if ($this->historyHelper->areDatesChanged($dates)) {
            $messages[] = $this->getDatesChangedMessage($dates);
        }

        if ($this->historyHelper->isTargetingsChanged($targetings)) {
            $messages[] = $this->getTargetingsChanged($targetings);
        }

        // if diff is empty but inventory failed -> add message that probably inventory changed size in YX
        if (!$this->historyHelper->areDatesChanged($dates) && !$this->historyHelper->isTargetingsChanged($targetings)) {
            $messages[] = 'Inventory check failed not because of changed choices.';
        }

        return $messages;
    }

    /**
     * @param Collection[][] $targetings
     *
     * @return array
     */
    private function getTargetingsChanged(array $targetings): array
    {
        $added = $this->historyHelper->getAddedTargetings($targetings);
        $removed = $this->historyHelper->getRemovedTargetings($targetings);

        // only removed
        if ($added->isEmpty() && $removed->isNotEmpty()) {
            return [
                'message'    => 'Removed targetings also removed most of impressions.',
                'targetings' => [
                    'added'   => $this->targetingsToString($added),
                    'removed' => $this->targetingsToString($removed),
                ],
            ];
        }

        // only added
        if ($removed->isEmpty() && $added->isNotEmpty()) {
            return [
                'message'    => 'Added targetings don\'t have enough of impressions.',
                'targetings' => [
                    'added'   => $this->targetingsToString($added),
                    'removed' => $this->targetingsToString($removed),
                ],
            ];
        }

        // added and removed
        return [
            'message'    => 'Inventory check was successful for removed targetings but failed for added.',
            'targetings' => [
                'added'   => $this->targetingsToString($added),
                'removed' => $this->targetingsToString($removed),
            ],
        ];
    }

    /**
     * @param Collection $targetings
     *
     * @return Collection
     */
    private function targetingsToString(Collection $targetings): Collection
    {
        $data = $targetings->map(function (TargetingValue $targeting): array {
            return [
                'name'     => $targeting->name,
                'type'     => $this->getTargetingNameFromClass(get_class($targeting)),
                'excluded' => $targeting->excluded, // dynamic attribute that not exists in original model
            ];
        });

        return $data->map(function (array $targeting): string {
            return sprintf(
                '%s: %s - %s',
                Arr::get($targeting, 'type'),
                Arr::get($targeting, 'name'),
                (bool)Arr::get($targeting, 'excluded') === true ? 'excluded' : 'included',
            );
        });
    }

    /**
     * @param string $class
     *
     * @return string
     */
    private function getTargetingNameFromClass(string $class): string
    {
        return Arr::last(explode('\\', $class));
    }

    /**
     * @param array $dates
     *
     * @return string
     */
    private function getDatesChangedMessage(array $dates): string
    {
        $succeededStart = $this->historyHelper->getSucceededStartDate($dates);
        $succeededEnd = $this->historyHelper->getSucceededEndDate($dates);

        $failedStart = $this->historyHelper->getFailedStartDate($dates);
        $failedEnd = $this->historyHelper->getFailedEndDate($dates);

        if (is_null($succeededStart) && is_null($succeededEnd)) {
            return sprintf(
                'Those dates (%s - %s) don\'t have enough of impressions.',
                $failedStart,
                $failedEnd
            );
        }

        // start date is changed only
        if ($succeededStart !== $failedStart && $succeededEnd === $failedEnd) {
            return sprintf(
                'Start dates (%s -> %s) were the reason of failing inventory check.',
                $succeededStart,
                $failedStart
            );
        }

        // end date is changed only
        if ($succeededEnd !== $failedEnd && $succeededStart !== $failedStart) {
            return sprintf(
                'End dates (%s -> %s) were the reason of failing inventory check.',
                $succeededEnd,
                $failedEnd
            );
        }

        // both dates is changed
        return sprintf(
            'Changed start and end dates (Start: %s -> %s, End: %s -> %s) don\'t have enough of impressions.',
            $succeededStart,
            $failedStart,
            $succeededEnd,
            $failedEnd
        );
    }
}
