<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Support\Arr;

class GetInventoryCheckConclusionErrorAction
{
    /**
     * @param array $messages
     *
     * @return array[]
     */
    public function handle(array $messages): array
    {
        return [
            'data'        => [
                'type'               => GetInventoryCheckConclusionAction::UNPROCESSABLE,
                'value'              => __('campaign::messages.inventory_check.availability.unavailable'),
                'message'            => __('campaign::messages.inventory_check.tooltips.unavailable'),
                'indicator_position' => 0,
            ],
            'suggestions' => $this->findSuggestions($messages),
            'updated'     => Carbon::now(),
        ];
    }

    /**
     * @param array $messages
     *
     * @return array
     */
    private function findSuggestions(array $messages): array
    {
        $message = Arr::first($messages);
        $key = Arr::first(array_keys($messages));

        if ($key === 'budget') {
            $action = [
                $key => __('campaign::messages.change_' . $key),
            ];
        } else {
            $action = [
                'steps' => [
                    $key => __('campaign::messages.change_' . $key),
                ],
            ];
        }

        return [
            'message' => $message,
            'action'  => $action,
        ];
    }
}
