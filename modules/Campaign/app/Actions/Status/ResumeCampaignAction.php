<?php

namespace Modules\Campaign\Actions\Status;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Actions\Traits\ValidateStatusEvaluation;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Notifications\Advertiser\Resumed\CampaignResumedByAdmin;
use Modules\Campaign\Notifications\Advertiser\Resumed\CampaignResumedByAdvertiser;
use Modules\Haapi\Actions\Campaign\CampaignGet;
use Modules\Haapi\Actions\Campaign\CampaignResume;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class ResumeCampaignAction
{
    use ValidateStatusEvaluation;

    /**
     * @var UpdateCampaignStatusAction
     */
    private $updateStatusAction;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @var CampaignResume
     */
    private $campaignResumeAction;

    /**
     * @var CampaignGet
     */
    private $campaignGetAction;

    /**
     * @param CampaignGet                $campaignGetAction
     * @param CampaignResume             $campaignResumeAction
     * @param Dispatcher                 $notification
     * @param LoggerInterface            $log
     * @param UpdateCampaignStatusAction $updateStatusAction
     */
    public function __construct(
        CampaignGet $campaignGetAction,
        CampaignResume $campaignResumeAction,
        Dispatcher $notification,
        LoggerInterface $log,
        UpdateCampaignStatusAction $updateStatusAction
    ) {
        $this->campaignGetAction = $campaignGetAction;
        $this->campaignResumeAction = $campaignResumeAction;
        $this->log = $log;
        $this->notification = $notification;
        $this->updateStatusAction = $updateStatusAction;
    }

    /**
     * Resume campaign.
     *
     * @param Campaign $campaign
     * @param User     $user
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(Campaign $campaign, User $user): Campaign
    {
        $this->log->info('Campaign resume started.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->canApply($campaign, $user, $campaign->getPreviousStatus());
        $this->campaignResumeAction->handle($campaign->external_id, $user->id);

        $status = $this->fetchStatus($campaign, $user->id);
        $campaign = $this->updateStatusAction->handle($campaign, $status, $user);

        $this->log->info('Campaign resumed.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->sendNotification($campaign, $user);

        return $campaign;
    }


    /**
     * @param Campaign $campaign
     * @param int      $userId
     *
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function fetchStatus(Campaign $campaign, int $userId): string
    {
        $campaignData = $this->campaignGetAction->handle($campaign->external_id, $userId);

        return $campaign->findTransition($campaignData->status);
    }

    /**
     * Send email and on-site notifications.
     *
     * @param Campaign $campaign
     * @param User     $user
     */
    protected function sendNotification(Campaign $campaign, User $user): void
    {
        // Advertiser has resumed the campaign
        if ($user->isAdvertiser()) {
            $this->notification->send($campaign->user, new CampaignResumedByAdvertiser($campaign, $user));

            return;
        }

        $this->notification->send($campaign->user, new CampaignResumedByAdmin($campaign, $user));
    }
}
