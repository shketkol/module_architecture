<?php

namespace Modules\Campaign\Actions\Status;

use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class UpdateCampaignStatusAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * UpdateCampaignStatusAction constructor.
     *
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    /**
     * Update campaign status.
     *
     * @param Campaign $campaign
     * @param string   $status
     * @param User     $user
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Throwable
     */
    public function handle(Campaign $campaign, string $status, User $user): Campaign
    {
        $campaign->load('status');

        $this->applyState($campaign, $status, $user);
        $campaign->status_changed_by = $user->id;

        if (!$campaign->save()) {
            $this->log->warning('Campaign status was not updated.', [
                'campaign_id' => $campaign->id,
                'from_status' => $campaign->status->name,
                'to_status'   => $status,
                'user_id'     => $user->id,
            ]);

            throw CampaignNotUpdatedException::create();
        }

        return $campaign->refresh();
    }

    /**
     * @param Campaign $campaign
     * @param string   $status
     * @param User     $user
     *
     * @throws \Throwable
     */
    private function applyState(Campaign $campaign, string $status, User $user): void
    {
        try {
            $campaign->applyInternalStatus($status);
        } catch (\Throwable $exception) {
            // not related to different user
            if ($campaign->statusChangedBy->is($user)) {
                throw $exception;
            }

            $message = trans('campaign::messages.resume.failed');

            $this->log->warning($message, [
                'campaign_id'       => $campaign->id,
                'status_changed_by' => $campaign->statusChangedBy->id,
                'user_id'           => $user->id,
            ]);

            throw CampaignNotUpdatedException::create($message, $exception);
        }
    }
}
