<?php

namespace Modules\Campaign\Actions\Status;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Actions\Traits\ValidateStatusEvaluation;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Haapi\Actions\Campaign\CampaignCancel;
use Modules\User\Models\User;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;

class CancelCampaignAction
{
    use ValidateStatusEvaluation;

    /**
     * @var UpdateCampaignStatusAction
     */
    private $updateStatusAction;

    /**
     * @var CampaignCancel
     */
    private $campaignCancelAction;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * CancelCampaignAction constructor.
     *
     * @param Dispatcher                 $notification
     * @param CampaignCancel             $campaignCancelAction
     * @param LoggerInterface            $log
     * @param UpdateCampaignStatusAction $updateStatusAction
     * @param UserRepository             $userRepository
     */
    public function __construct(
        Dispatcher $notification,
        CampaignCancel $campaignCancelAction,
        LoggerInterface $log,
        UpdateCampaignStatusAction $updateStatusAction,
        UserRepository $userRepository
    ) {
        $this->notification = $notification;
        $this->campaignCancelAction = $campaignCancelAction;
        $this->log = $log;
        $this->updateStatusAction = $updateStatusAction;
        $this->userRepository = $userRepository;
    }

    /**
     * Cancel campaign.
     *
     * @param Campaign $campaign
     * @param User     $user
     *
     * @return Campaign
     * @throws CampaignNotUpdatedException
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(Campaign $campaign, User $user): Campaign
    {
        $this->log->info('Campaign cancel started.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        $this->canApply($campaign, $user, CampaignStatus::CANCELED);
        $this->campaignCancelAction->handle($campaign->external_id ?? '', $user->id);

        $campaign = $this->updateStatusAction->handle($campaign, CampaignStatus::PROCESSING, $user);

        $this->log->info('Campaign cancel request was sent.', [
            'campaign_id' => $campaign->id,
            'user_id'     => $user->id,
        ]);

        return $campaign;
    }
}
