<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Validations\ValidateDynamicInventoryCheckAction;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetDynamicInventoryCheckAction
{
    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var StoreInventoryCheckResultAction
     */
    private StoreInventoryCheckResultAction $storeInventoryCheck;

    /**
     * @var GetInventoryCheckConclusionAction
     */
    private GetInventoryCheckConclusionAction $conclusion;

    /**
     * @var GetInventoryCheckConclusionErrorAction
     */
    private GetInventoryCheckConclusionErrorAction $conclusionError;

    /**
     * @var PerformInventoryCheckAction
     */
    private PerformInventoryCheckAction $performInventoryCheck;

    /**
     * @var ValidateDynamicInventoryCheckAction
     */
    private ValidateDynamicInventoryCheckAction $validateDynamicInventoryCheckAction;

    /**
     * @var UpdateInventoryCheckResultAction
     */
    private UpdateInventoryCheckResultAction $updateInventoryCheck;

    /**
     * @param Logger                                 $log
     * @param StoreInventoryCheckResultAction        $storeInventoryCheck
     * @param GetInventoryCheckConclusionAction      $conclusion
     * @param GetInventoryCheckConclusionErrorAction $conclusionError
     * @param PerformInventoryCheckAction            $performInventoryCheck
     * @param ValidateDynamicInventoryCheckAction    $validateDynamicInventoryCheckAction
     * @param UpdateInventoryCheckResultAction       $updateInventoryCheck
     */
    public function __construct(
        Logger $log,
        StoreInventoryCheckResultAction $storeInventoryCheck,
        GetInventoryCheckConclusionAction $conclusion,
        GetInventoryCheckConclusionErrorAction $conclusionError,
        PerformInventoryCheckAction $performInventoryCheck,
        ValidateDynamicInventoryCheckAction $validateDynamicInventoryCheckAction,
        UpdateInventoryCheckResultAction $updateInventoryCheck
    ) {
        $this->log = $log;
        $this->storeInventoryCheck = $storeInventoryCheck;
        $this->conclusion = $conclusion;
        $this->conclusionError = $conclusionError;
        $this->performInventoryCheck = $performInventoryCheck;
        $this->validateDynamicInventoryCheckAction = $validateDynamicInventoryCheckAction;
        $this->updateInventoryCheck = $updateInventoryCheck;
    }

    /**
     * Get expected delivery result.
     *
     * @param Campaignable $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     * @throws ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign): array
    {
        $this->log->info('[Dynamic Inventory check] Delivery indicator used.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        try {
            $errors = $this->validateDynamicInventoryCheckAction->handle($campaign);
            if (!empty($errors)) {
                return $this->conclusionError->handle($errors);
            }

            $history = $this->storeHistory($campaign);

            return $this->conclusion->handle($history);
        } catch (InternalErrorException $exception) {
            $this->log->warning('[Dynamic Inventory check] Fail inventory check', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'reason'        => $exception->getMessage(),
            ]);

            return $this->internalError();
        }
    }

    /**
     * @param Campaignable $campaign
     *
     * @return InventoryCheckHistory
     * @throws TypeNotFoundException
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Throwable
     */
    private function storeHistory(Campaignable $campaign): InventoryCheckHistory
    {
        $history = $this->storeInventoryCheck->handle($campaign);
        $response = $this->performInventoryCheck->handle($campaign);
        $history = $this->updateInventoryCheck->handle($history, $response->getPayload());

        $this->log->info('[Dynamic Inventory check] Delivery indicator result.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'budget'        => $response->get('availableBudget'),
            'impressions'   => $response->get('availableImpressions'),
            'floorBudget'   => $response->get('systemFloorBudget'),
            'history_id'    => $history->id,
        ]);

        return $history;
    }

    /**
     * @return array
     */
    private function internalError(): array
    {
        return [
            'title'   => __('campaign::labels.modal_inventory_fail.something_went_wrong'),
            'message' => trans('campaign::messages.inventory_check.fail.error'),
            'type'    => GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR,
            'payload' => [],
        ];
    }
}
