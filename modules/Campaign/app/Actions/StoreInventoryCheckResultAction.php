<?php

namespace Modules\Campaign\Actions;

use Illuminate\Database\DatabaseManager;
use Modules\Campaign\Actions\Traits\Targetings;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Models\InventoryCheckStatus;
use Modules\Campaign\Models\InventoryCheckTargeting;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Campaign\Repositories\InventoryCheckStatusRepository;
use Modules\Targeting\Models\TargetingValue;
use Psr\Log\LoggerInterface;

class StoreInventoryCheckResultAction
{
    use Targetings;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $log;

    /**
     * @var DatabaseManager
     */
    private DatabaseManager $databaseManager;

    /**
     * @var InventoryCheckHistoryRepository
     */
    private InventoryCheckHistoryRepository $historyRepository;

    /**
     * @var InventoryCheckStatusRepository
     */
    private InventoryCheckStatusRepository $statusRepository;

    /**
     * @param LoggerInterface                 $log
     * @param DatabaseManager                 $databaseManager
     * @param InventoryCheckHistoryRepository $historyRepository
     * @param InventoryCheckStatusRepository  $statusRepository
     */
    public function __construct(
        LoggerInterface $log,
        DatabaseManager $databaseManager,
        InventoryCheckHistoryRepository $historyRepository,
        InventoryCheckStatusRepository $statusRepository
    ) {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->historyRepository = $historyRepository;
        $this->statusRepository = $statusRepository;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return InventoryCheckHistory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign): InventoryCheckHistory
    {
        $this->log->info('[Inventory Check History] Started saving.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $attributes = $this->prepareAttributes($campaign);
            /** @var InventoryCheckHistory $history */
            $history = $this->historyRepository->create($attributes);
            $this->saveTargetings($history, $campaign);
        } catch (\Throwable $exception) {
            $this->log->info('[Inventory Check History] Failed saving.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'reason'        => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('[Inventory Check History] Finished saving.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'history_id'    => $history->id,
        ]);

        return $history;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    private function prepareAttributes(Campaignable $campaign): array
    {
        return [
            'campaign_id'         => $campaign->id,
            'campaign_type'       => get_class($campaign),
            'date_start'          => $campaign->date_start,
            'date_end'            => $campaign->date_end,
            'ordered_impressions' => $campaign->impressions,
            'campaign_budget'     => $campaign->budget,
            'status_id'           => InventoryCheckStatus::ID_INTERNAL_IN_PROGRESS,
        ];
    }

    /**
     * @param InventoryCheckHistory $history
     * @param Campaignable          $campaign
     */
    private function saveTargetings(InventoryCheckHistory $history, Campaignable $campaign): void
    {
        $targetings = $this->getTargetingsFromCampaign($campaign);
        $attributes = $targetings->map(function (TargetingValue $targeting) use ($history): array {
            return [
                'history_id'      => $history->id,
                'targetable_type' => get_class($targeting),
                'targetable_id'   => $targeting->id,
                'excluded'        => $targeting->getIsExcluded(),
            ];
        });

        InventoryCheckTargeting::query()->insert($attributes->toArray());
    }
}
