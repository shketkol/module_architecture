<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\InventoryCheckHistory;

class GetInventoryCheckConclusionAction
{
    private const AVAILABLE = 'available';
    private const UNAVAILABLE = 'unavailable';
    public const UNPROCESSABLE = 'unprocessable';

    private const PLENTY_AVAILABLE_COEFFICIENT = 2;

    /**
     * @var CalculateDeliveryIndicatorArrowPositionAction
     */
    private $calculateDeliveryIndicatorArrowPosition;

    /**
     * @var GetInventoryCheckSuggestionsAction
     */
    private $suggestions;

    /**
     * @var GetInventoryCheckDebugSuggestionsAction
     */
    private $debugSuggestions;

    /**
     * @param CalculateDeliveryIndicatorArrowPositionAction $calculateDeliveryIndicatorArrowPosition
     * @param GetInventoryCheckSuggestionsAction            $suggestions
     * @param GetInventoryCheckDebugSuggestionsAction       $debugSuggestions
     */
    public function __construct(
        CalculateDeliveryIndicatorArrowPositionAction $calculateDeliveryIndicatorArrowPosition,
        GetInventoryCheckSuggestionsAction $suggestions,
        GetInventoryCheckDebugSuggestionsAction $debugSuggestions
    ) {
        $this->calculateDeliveryIndicatorArrowPosition = $calculateDeliveryIndicatorArrowPosition;
        $this->suggestions = $suggestions;
        $this->debugSuggestions = $debugSuggestions;
    }

    /**
     * @param InventoryCheckHistory $history
     * @param Carbon|null           $updatedAt
     *
     * @return array[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(InventoryCheckHistory $history, ?Carbon $updatedAt = null): array
    {
        $conclusion = [
            'id'          => $history->id,
            'data'        => $this->getResultTypeData($history),
            'suggestions' => $this->suggestions->handle($history),
            'updated'     => $updatedAt ?: $history->updated_at,
        ];

        if (config('inventory-check.dynamic_inventory_check.debug.enabled')) {
            Arr::set($conclusion, 'debug', $this->debugSuggestions->handle($history));
        }

        return $conclusion;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    private function getResultTypeData(InventoryCheckHistory $history): array
    {
        $result = [
            'type'               => self::AVAILABLE,
            'value'              => __('campaign::messages.inventory_check.availability.' . self::AVAILABLE),
            'message'            => __('campaign::messages.inventory_check.tooltips.' . self::AVAILABLE),
            'indicator_position' => $this->calculateDeliveryIndicatorArrowPosition->handle($history),
        ];

        if ($history->ordered_impressions > $history->available_impressions) {
            Arr::set($result, 'type', self::UNAVAILABLE);
            Arr::set(
                $result,
                'message',
                __('campaign::messages.inventory_check.tooltips.' . self::UNAVAILABLE)
            );
            Arr::set(
                $result,
                'value',
                __('campaign::messages.inventory_check.availability.' . self::UNAVAILABLE)
            );
            return $result;
        }

        // at least twice as big inventory then ordered
        if (($history->ordered_impressions * self::PLENTY_AVAILABLE_COEFFICIENT) < $history->available_impressions) {
            return $result;
        }

        if ($history->ordered_impressions <= $history->available_impressions) {
            Arr::set(
                $result,
                'message',
                __('campaign::messages.inventory_check.tooltips.some_available')
            );
            Arr::set(
                $result,
                'value',
                __('campaign::messages.inventory_check.availability.some_available')
            );

            return $result;
        }

        return $result;
    }
}
