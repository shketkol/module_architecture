<?php

namespace Modules\Campaign\Actions\SpecialAds;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignAgeGroupsAction;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignGendersAction;
use Modules\Targeting\Actions\Audience\UpdateCampaignAudiencesAction;
use Modules\Targeting\Actions\Genre\UpdateCampaignGenresAction;
use Modules\Targeting\Actions\Location\Zipcode\UpdateCampaignZipcodesAction;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Repositories\AgeGroupRepository;
use Modules\Targeting\Repositories\GenderRepository;
use Modules\User\Models\AccountType;

class RemoveSpecialAdsTargetingsAction
{
    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var DatabaseManager
     */
    private DatabaseManager $databaseManager;

    /**
     * @var GetRelationByPermissionAction
     */
    private GetRelationByPermissionAction $relationByPermissionAction;

    /**
     * @var GenderRepository
     */
    private GenderRepository $genderRepository;

    /**
     * @var UpdateCampaignGendersAction
     */
    private UpdateCampaignGendersAction $updateGendersAction;

    /**
     * @var UpdateCampaignAgeGroupsAction
     */
    private UpdateCampaignAgeGroupsAction $updateAgeGroupsAction;

    /**
     * @var AgeGroupRepository
     */
    private AgeGroupRepository $ageGroupRepository;

    /**
     * @var UpdateCampaignAudiencesAction
     */
    private UpdateCampaignAudiencesAction $updateAudiencesAction;

    /**
     * @var UpdateCampaignGenresAction
     */
    private UpdateCampaignGenresAction $updateGenresAction;

    /**
     * @var UpdateCampaignZipcodesAction
     */
    private UpdateCampaignZipcodesAction $updateZipcodesAction;

    /**
     * @param Logger                        $log
     * @param DatabaseManager               $databaseManager
     * @param GetRelationByPermissionAction $relationByPermissionAction
     * @param GenderRepository              $genderRepository
     * @param UpdateCampaignGendersAction   $updateGendersAction
     * @param UpdateCampaignAgeGroupsAction $updateAgeGroupsAction
     * @param AgeGroupRepository            $ageGroupRepository
     * @param UpdateCampaignAudiencesAction $updateAudiencesAction
     * @param UpdateCampaignGenresAction    $updateGenresAction
     * @param UpdateCampaignZipcodesAction  $updateZipcodesAction
     */
    public function __construct(
        Logger $log,
        DatabaseManager $databaseManager,
        GetRelationByPermissionAction $relationByPermissionAction,
        GenderRepository $genderRepository,
        UpdateCampaignGendersAction $updateGendersAction,
        UpdateCampaignAgeGroupsAction $updateAgeGroupsAction,
        AgeGroupRepository $ageGroupRepository,
        UpdateCampaignAudiencesAction $updateAudiencesAction,
        UpdateCampaignGenresAction $updateGenresAction,
        UpdateCampaignZipcodesAction $updateZipcodesAction
    ) {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->relationByPermissionAction = $relationByPermissionAction;
        $this->genderRepository = $genderRepository;
        $this->updateGendersAction = $updateGendersAction;
        $this->updateAgeGroupsAction = $updateAgeGroupsAction;
        $this->ageGroupRepository = $ageGroupRepository;
        $this->updateAudiencesAction = $updateAudiencesAction;
        $this->updateGenresAction = $updateGenresAction;
        $this->updateZipcodesAction = $updateZipcodesAction;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return string|null
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign): ?string
    {
        if ($campaign->user->account_type_id === AccountType::ID_COMMON) {
            return null;
        }

        $this->log->info('Started removing targetings that forbidden to advertiser account type.', [
            'campaign_id'     => $campaign->id,
            'campaign_type'   => get_class($campaign),
            'account_type_id' => $campaign->user->account_type_id,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $changes = $this->removeForbiddenTargetings($campaign);
        } catch (\Throwable $exception) {
            $this->log->warning('Failed to remove targetings that forbidden to advertiser account type.', [
                'campaign_id'     => $campaign->id,
                'campaign_type'   => get_class($campaign),
                'account_type_id' => $campaign->user->account_type_id,
                'reason'          => $exception->getMessage(),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('Finished removing targetings that forbidden to advertiser account type.', [
            'campaign_id'     => $campaign->id,
            'campaign_type'   => get_class($campaign),
            'account_type_id' => $campaign->user->account_type_id,
        ]);

        if ($changes === 0) {
            return null;
        }

        return __(
            'campaign::messages.special_ads.targetings_removed',
            [
                'url' => config('general.links.faq.special_ads_categories'),
            ]
        );
    }

    /**
     * @param Campaignable $campaign
     *
     * @return int
     * @throws \App\Exceptions\InvalidArgumentException
     */
    private function removeForbiddenTargetings(Campaignable $campaign): int
    {
        $targetingPermissions = $this->getForbiddenTargetingPermissions($campaign);
        $changes = 0;

        if (empty($targetingPermissions)) {
            return $changes;
        }

        foreach ($targetingPermissions as $permission) {
            $relation = $this->relationByPermissionAction->handle($permission);
            $changes += $this->handleForbiddenTargetingRelation($campaign, $relation);
        }

        return $changes;
    }

    /**
     * @param Campaignable $campaign
     * @param string       $relation
     *
     * @return int
     * @throws InvalidArgumentException
     */
    private function handleForbiddenTargetingRelation(Campaignable $campaign, string $relation): int
    {
        $map = [
            Campaign::RELATION_GENDERS_NAME    => function (Campaignable $campaign): int {
                return $this->setGenderToAll($campaign);
            },
            Campaign::RELATION_AGE_GROUPS_NAME => function (Campaignable $campaign): int {
                return $this->setAgeGroupsToAll($campaign);
            },
            Campaign::RELATION_AUDIENCES_NAME  => function (Campaignable $campaign): int {
                return $this->setAudiencesToAll($campaign);
            },
            Campaign::RELATION_GENRES_NAME     => function (Campaignable $campaign): int {
                return $this->setGenresToAll($campaign);
            },
            Campaign::RELATION_ZIPCODES_NAME   => function (Campaignable $campaign): int {
                return $this->setZipcodesToAll($campaign);
            },
        ];

        $callable = Arr::get($map, $relation);

        if (is_null($callable)) {
            throw new InvalidArgumentException(sprintf('Campaign relation "%s" does not have handler.', $relation));
        }

        return $callable($campaign);
    }

    /**
     * @param Campaignable $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function setGenderToAll(Campaignable $campaign): int
    {
        $allGender = $this->getAllGenderModel();
        $campaignGendersIds = $campaign->targetingGenders->pluck('id')->toArray();
        $this->updateGendersAction->handle($campaign, $allGender->id);
        $count = count(array_diff($campaignGendersIds, [$allGender->id]));

        $this->log->info('Changes after setting gender to all.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'changes'       => $count,
        ]);

        return $count;
    }

    /**
     * @return Gender
     */
    private function getAllGenderModel(): Gender
    {
        return $this->genderRepository->getAllGenderModel();
    }

    /**
     * @param Campaignable $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function setAgeGroupsToAll(Campaignable $campaign): int
    {
        $gender = $this->getAllGenderModel();
        $allAgeGroupsIds = $this->ageGroupRepository->findWhere([['gender_id', '=', $gender->id]])
            ->pluck('id')
            ->toArray();

        $campaignAgeGroupsIds = $campaign->targetingAgeGroups->pluck('id')->toArray();
        $this->updateAgeGroupsAction->handle($campaign, $allAgeGroupsIds);
        $count = count(array_diff($campaignAgeGroupsIds, $allAgeGroupsIds));

        $this->log->info('Changes after setting age groups to all.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'changes'       => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return int
     * @throws \Throwable
     */
    private function setZipcodesToAll(Campaignable $campaign): int
    {
        $count = $campaign->zipcodes()->count();

        if ($count === 0) {
            return $count;
        }

        $this->updateZipcodesAction->handle($campaign, []);

        $this->log->info('Changes after setting zipcodes to all.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'changes'       => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     */
    private function setGenresToAll(Campaignable $campaign): int
    {
        $count = $campaign->genres()->count();

        if ($count === 0) {
            return $count;
        }

        $this->updateGenresAction->handle($campaign, []);

        $this->log->info('Changes after setting genres to all.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'changes'       => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return int
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     */
    private function setAudiencesToAll(Campaignable $campaign): int
    {
        $count = $campaign->audiences()->count();

        if ($count === 0) {
            return $count;
        }

        $this->updateAudiencesAction->handle($campaign, []);

        $this->log->info('Changes after setting audience to all.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'changes'       => $count,
        ]);

        return $count;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    private function getForbiddenTargetingPermissions(Campaignable $campaign): array
    {
        $permissions = array_keys($campaign->user->getTargetingPermissions());

        $targetings = array_filter($permissions, function (string $permission) use ($campaign): bool {
            return !$campaign->user->canAccessTargeting($permission);
        });

        if (empty($targetings)) {
            $this->log->info('Not found any targetings that forbidden to advertiser account type.', [
                'campaign_id'     => $campaign->id,
                'campaign_type'   => get_class($campaign),
                'account_type_id' => $campaign->user->account_type_id,
                'targetings'      => $targetings,
            ]);

            return $targetings;
        }

        $this->log->info('Found targetings that forbidden to advertiser account type.', [
            'campaign_id'     => $campaign->id,
            'campaign_type'   => get_class($campaign),
            'account_type_id' => $campaign->user->account_type_id,
            'targetings'      => $targetings,
        ]);

        return $targetings;
    }
}
