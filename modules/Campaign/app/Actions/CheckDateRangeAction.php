<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Modules\Campaign\Http\Controllers\Api\Traits\StoreCampaign;
use Modules\Campaign\Models\Contracts\Campaignable;

class CheckDateRangeAction
{
    use StoreCampaign;

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @param Logger $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }

    /**
     * Check campaign date range.
     *
     * @param Campaignable $campaign
     *
     * @return string|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaignable $campaign): ?string
    {
        if ($campaign->isLiveEdit()) {
            return null;
        }

        // Do not adjust dates if at least one of them is empty.
        if (!$campaign->date_start || !$campaign->date_end) {
            return null;
        }

        if ($campaign->date_start
                ->shiftTimezone(config('campaign.wizard.date.timezone'))
                ->gte($this->getTomorrowDate())
        ) {
            return null;
        }

        $campaign = $this->checkDates($campaign);
        $campaign->save();
        $campaign->refresh();

        return __('campaign::messages.update_start_date');
    }
}
