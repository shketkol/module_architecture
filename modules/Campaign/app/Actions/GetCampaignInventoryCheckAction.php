<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Validations\ValidateInventoryCheckAction;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\Haapi\Exceptions\InternalErrorException;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetCampaignInventoryCheckAction
{
    /** Types */
    public const TYPE_SUCCESS         = 'success';
    public const TYPE_WARNING         = 'warning';
    public const TYPE_DANGER_BUDGET   = 'dangerBudget';
    public const TYPE_DANGER_SCHEDULE = 'dangerSchedule';
    public const TYPE_INTERNAL_ERROR  = 'fail';

    /** Constants to use for identifier inventory check flow */
    public const TYPE_FLOW_SUCCESS = 'success';
    public const TYPE_FLOW_WARNING = 'warning';
    public const TYPE_FLOW_DANGER  = 'danger';

    /** Constant to use for showing message */
    private const FLOW_TYPES = [
        self::TYPE_FLOW_SUCCESS => [self::TYPE_SUCCESS],
        self::TYPE_FLOW_DANGER  => [
            self::TYPE_DANGER_BUDGET,
            self::TYPE_DANGER_SCHEDULE,
            self::TYPE_INTERNAL_ERROR,
        ],
        self::TYPE_FLOW_WARNING => [
            self::TYPE_WARNING,
        ],
    ];

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var StoreInventoryCheckResultAction
     */
    private StoreInventoryCheckResultAction $storeInventoryCheck;

    /**
     * @var PerformInventoryCheckAction
     */
    private PerformInventoryCheckAction $performInventoryCheck;

    /**
     * @var UpdateInventoryCheckResultAction
     */
    private UpdateInventoryCheckResultAction $updateInventoryCheck;

    /**
     * @var ValidateInventoryCheckAction
     */
    private ValidateInventoryCheckAction $validateInventoryCheckAction;

    /**
     * @param Logger                                 $log
     * @param StoreInventoryCheckResultAction        $storeInventoryCheck
     * @param UpdateInventoryCheckResultAction       $updateInventoryCheck
     * @param PerformInventoryCheckAction            $performInventoryCheck
     * @param ValidateInventoryCheckAction           $validateInventoryCheckAction
     */
    public function __construct(
        Logger $log,
        StoreInventoryCheckResultAction $storeInventoryCheck,
        UpdateInventoryCheckResultAction $updateInventoryCheck,
        PerformInventoryCheckAction $performInventoryCheck,
        ValidateInventoryCheckAction $validateInventoryCheckAction
    ) {
        $this->log = $log;
        $this->storeInventoryCheck = $storeInventoryCheck;
        $this->performInventoryCheck = $performInventoryCheck;
        $this->updateInventoryCheck = $updateInventoryCheck;
        $this->validateInventoryCheckAction = $validateInventoryCheckAction;
    }

    /**
     * Get expected delivery result.
     *
     * @param Campaignable $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     * @throws ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign): array
    {
        $this->log->info('Delivery indicator used.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        try {
            $this->validateInventoryCheckAction->handle($campaign);
            $history = $this->storeInventoryCheck->handle($campaign);
            $response = $this->performInventoryCheck->handle($campaign);
            $history = $this->updateInventoryCheck->handle($history, $response->getPayload());
            $type = $this->getType($response->get('statusCode'));

            $this->log->info('Delivery indicator result.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'type'          => $type,
                'budget'        => $response->get('availableBudget'),
                'impressions'   => $response->get('availableImpressions'),
                'floorBudget'   => $response->get('systemFloorBudget'),
                'history_id'    => $history->id,
            ]);

            return [
                'title'       => $this->getTitle($type),
                'message'     => $this->getMessage($type, $response),
                'messageType' => $this->getTypeFow($type),
                'payload'     => $this->getPayload($type, $response),
                'type'        => $type,
                'typeFlow'    => $this->getTypeFow($type),
            ];
        } catch (InternalErrorException $exception) {
            $this->log->warning('Fail inventory check', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'reason'        => $exception->getMessage(),
            ]);
            return $this->internalError();
        }
    }

    /**
     * @param string $statusCode
     *
     * @return string
     * @throws HaapiException
     */
    private function getType(string $statusCode): string
    {
        $type = Arr::where(config('inventory-check.statuses'), function (array $item) use ($statusCode): bool {
            return in_array($statusCode, $item);
        });

        if (empty($type)) {
            throw InternalErrorException::create("Not valid HAAPI inventory check status: $statusCode");
        }

        return Arr::first(array_keys($type));
    }

    /**
     * Get payload data.
     *
     * @param string        $type
     * @param HaapiResponse $response
     *
     * @return array
     */
    protected function getPayload(string $type, HaapiResponse $response): array
    {
        $data = [
            'budget'      => $response->get('availableBudget'),
            'impressions' => $response->get('availableImpressions'),
            'floorBudget' => $response->get('systemFloorBudget'),
        ];

        if ($type === self::TYPE_DANGER_SCHEDULE) {
            $data['minDailyImpressions'] = $response->get('minDailyImpressions');
        }

        return $data;
    }

    /**
     * Get message for the result type.
     *
     * @param string        $type
     * @param HaapiResponse $response
     *
     * @return string
     */
    protected function getMessage(string $type, HaapiResponse $response): string
    {
        $snakeType = Str::snake($type);

        switch ($type) {
            case self::TYPE_DANGER_SCHEDULE:
                return __('campaign::messages.inventory_check.result.danger_schedule', [
                    'impressions' => $response->get('minDailyImpressions'),
                ]);
            default:
                return __("campaign::messages.inventory_check.result.$snakeType");
        }
    }

    /**
     * @param string $type
     *
     * @return string
     */
    protected function getTitle(string $type): string
    {
        $typeMap = [
            self::TYPE_WARNING         => __('campaign::labels.modal_inventory_info.inventory_high_demand'),
            self::TYPE_DANGER_BUDGET   => __('campaign::labels.modal_inventory_info.inventory_high_demand'),
            self::TYPE_DANGER_SCHEDULE => __('campaign::labels.modal_inventory_info.daily_impressions_low'),
        ];

        return Arr::get($typeMap, $type, '');
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getTypeFow(string $type): string
    {
        $type = Arr::where(self::FLOW_TYPES, function (array $item) use ($type): bool {
            return in_array($type, $item);
        });

        return Arr::first(array_keys($type));
    }

    /**
     * @return array
     */
    private function internalError(): array
    {
        return [
            'title'       => __('campaign::labels.modal_inventory_fail.something_went_wrong'),
            'message'     => trans('campaign::messages.inventory_check.fail.error'),
            'messageType' => $this->getTypeFow(GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR),
            'type'        => GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR,
            'typeFlow'    => $this->getTypeFow(GetCampaignInventoryCheckAction::TYPE_INTERNAL_ERROR),
            'payload'     => [],
        ];
    }
}
