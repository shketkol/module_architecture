<?php

namespace Modules\Campaign\Actions;

use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignInventoryCheck;
use Modules\Haapi\DataTransferObjects\Campaign\InventoryCheckParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class PerformInventoryCheckAction
{
    /** Constants to use in HAAPI Inventory Check */
    private const PRODUCT_ID_KEY = 'daapi.campaign.productId';
    private const QUANTITY_TYPE  = 'IMPRESSIONS';

    /**
     * @var CampaignInventoryCheck
     */
    private CampaignInventoryCheck $campaignInventoryCheck;

    /**
     * @var GetCampaignTargetingsAction
     */
    private GetCampaignTargetingsAction $getCampaignTargetings;

    /**
     * @param CampaignInventoryCheck      $campaignInventoryCheck
     * @param GetCampaignTargetingsAction $getCampaignTargetings
     */
    public function __construct(
        CampaignInventoryCheck $campaignInventoryCheck,
        GetCampaignTargetingsAction $getCampaignTargetings
    ) {
        $this->campaignInventoryCheck = $campaignInventoryCheck;
        $this->getCampaignTargetings = $getCampaignTargetings;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return \Modules\Haapi\HttpClient\Responses\HaapiResponse
     * @throws TypeNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(Campaignable $campaign): HaapiResponse
    {
        $userId = Auth::user()->getAuthIdentifier();

        $inventoryCheckParams = new InventoryCheckParams([
            'campaignSourceId' => $campaign->getOrigin()->id,
            'startDate'        => $campaign->getStartDateForInventoryCheck()->format(config('date.format.haapi')),
            'endDate'          => $campaign->end_date_with_timezone->format(config('date.format.haapi')),
            'targets'          => $this->getCampaignTargetings->handle($campaign),
            'productId'        => config(self::PRODUCT_ID_KEY),
            'quantity'         => $campaign->getRemainedImpressions(),
            'quantityType'     => self::QUANTITY_TYPE,
        ]);

        return $this->campaignInventoryCheck->handle($inventoryCheckParams, $userId);
    }
}
