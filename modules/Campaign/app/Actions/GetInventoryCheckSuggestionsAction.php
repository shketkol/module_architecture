<?php

namespace Modules\Campaign\Actions;

use App\Helpers\Math\CalculationHelper;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Campaign\Actions\Traits\Targetings;
use Modules\Campaign\Helpers\InventoryCheckHistoryHelper;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Models\InventoryCheckStatus;
use Modules\Campaign\Models\InventoryCheckTargeting;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\TargetingValue;
use Modules\Targeting\Models\Zipcode;
use Modules\Targeting\Repositories\AgeGroupRepository;
use Modules\Targeting\Repositories\DeviceGroupRepository;
use Prettus\Repository\Exceptions\RepositoryException;

class GetInventoryCheckSuggestionsAction
{
    use Targetings;

    /**
     * Targeting values used on front-end same as __('campaign::labels.inventory_steps') keys
     */
    private const LOCATION     = 'locations';
    private const ZIPCODE      = 'locations';
    private const AGE_GROUP    = 'ages';
    private const AUDIENCE     = 'audiences';
    private const DEVICE_GROUP = 'devices';
    private const GENRE        = 'genres';
    private const DATE_RANGE   = 'schedule';

    /**
     * @var AgeGroupRepository
     */
    private AgeGroupRepository $ageGroupRepository;

    /**
     * @var InventoryCheckHistoryRepository
     */
    private InventoryCheckHistoryRepository $historyRepository;

    /**
     * @var DeviceGroupRepository
     */
    private DeviceGroupRepository $deviceRepository;

    /**
     * @var GetHelpSuggestionAction
     */
    private GetHelpSuggestionAction $getHelpSuggestion;

    /**
     * @var InventoryCheckHistoryHelper
     */
    private InventoryCheckHistoryHelper $historyHelper;

    /**
     * @param AgeGroupRepository              $ageGroupRepository
     * @param InventoryCheckHistoryRepository $historyRepository
     * @param DeviceGroupRepository           $deviceRepository
     * @param GetHelpSuggestionAction         $getHelpSuggestion
     * @param InventoryCheckHistoryHelper     $historyHelper
     */
    public function __construct(
        AgeGroupRepository $ageGroupRepository,
        InventoryCheckHistoryRepository $historyRepository,
        DeviceGroupRepository $deviceRepository,
        GetHelpSuggestionAction $getHelpSuggestion,
        InventoryCheckHistoryHelper $historyHelper
    ) {
        $this->ageGroupRepository = $ageGroupRepository;
        $this->historyRepository = $historyRepository;
        $this->deviceRepository = $deviceRepository;
        $this->getHelpSuggestion = $getHelpSuggestion;
        $this->historyHelper = $historyHelper;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws RepositoryException
     */
    public function handle(InventoryCheckHistory $history): array
    {
        if ($history->status_id === InventoryCheckStatus::ID_AVAILABILITY_UNDER_MIN_SPEND) {
            return $this->findSuggestionsForUnderMinSpend($history);
        }

        if ($history->status_id === InventoryCheckStatus::ID_YX_CONNECTION_FAILED) {
            return $this->findSuggestionsForError($history);
        }

        if ($history->status_id === InventoryCheckStatus::ID_UNAVAILABLE) {
            return $this->findSuggestionsForUnavailable($history);
        }

        if ($history->status_id === InventoryCheckStatus::ID_REQUEST_UNDER_MIN_PER_DAY) {
            return $this->findSuggestionsForUnderMinPerDay($history);
        }

        return [];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws RepositoryException
     */
    private function findSuggestionsForUnderMinPerDay(InventoryCheckHistory $history): array
    {
        $steps = $this->findShortReasonsForFail($history);
        $campaign = $history->campaign;

        $suggestions = [
            'message' => __('campaign::messages.min_budget_message', [
                'min_per_day'       => number_format((float)config('campaign.min_daily_impressions'), 0, '', ','),
                'current_average'   => floor($campaign->impressions / $campaign->getDuration()),
                'proposed_duration' => floor($campaign->impressions / config('campaign.min_daily_impressions')),
                'proposed_budget'   => CalculationHelper::calculateBudget(
                    $campaign->cpm,
                    $campaign->getDuration() * config('campaign.min_daily_impressions')
                ),
            ]),
            'action'  => [
                'steps'  => $this->sortActionSteps($steps),
                'budget' => __('campaign::messages.change_budget'),
            ],
        ];

        return $this->prepareSuggestion($suggestions, $history);
    }

    /**
     * @param array                 $suggestions
     * @param InventoryCheckHistory $history
     *
     * @return array
     * @throws RepositoryException
     */
    private function prepareSuggestion(array $suggestions, InventoryCheckHistory $history): array
    {
        return $this->getHelpSuggestion->handle($suggestions, $history);
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws RepositoryException
     */
    private function findSuggestionsForUnderMinSpend(InventoryCheckHistory $history): array
    {
        $steps = $this->findShortReasonsForFail($history);
        $suggestions = [
            'action' => [
                'steps' => $this->sortActionSteps($steps),
            ],
        ];

        return $this->prepareSuggestion($suggestions, $history);
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    private function findSuggestionsForError(InventoryCheckHistory $history): array
    {
        return [
            // @todo find better message
            'message' => 'An error has occurred. Could you please restart inventory check in a few moments?',
        ];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     * @throws RepositoryException
     */
    private function findSuggestionsForUnavailable(InventoryCheckHistory $history): array
    {
        if (!config('campaign.enable_budget_suggestions')) {
            return $this->findSuggestionsForUnderMinSpend($history);
        }

        $steps = $this->findShortReasonsForFail($history);
        $availableBudget = number_format($history->available_budget, 2);

        $suggestions = [
            'message' => __('campaign::messages.dynamic_inventory_check.error.unavailable', [
                'budget' => $availableBudget,
            ]),
            'action'  => [
                'budget' => sprintf('Change budget to $%s.', $availableBudget),
                'steps'  => $this->sortActionSteps($steps),
            ],
            'payload' => [
                'availableBudget' => round($history->available_budget, 2),
            ],
        ];

        return $this->prepareSuggestion($suggestions, $history);
    }

    /**
     * @param string[] $steps
     *
     * @return array
     */
    private function sortActionSteps(array $steps): array
    {
        $order = [
            self::LOCATION,
            self::ZIPCODE,
            self::AGE_GROUP,
            self::AUDIENCE,
            self::DEVICE_GROUP,
            self::GENRE,
            self::DATE_RANGE,
        ];

        // sort with pre-defined order
        $sorted = array_replace(array_flip($order), $steps);

        // remove empty steps
        $collection = collect($sorted)->filter(function ($value): bool {
            return is_string($value);
        });

        return $collection->toArray();
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return array
     */
    private function findShortReasonsForFail(InventoryCheckHistory $history): array
    {
        if ($history->isDrasticLack()) {
            $history = $this->historyRepository->findFirstDrasticFailedHistory($history);
        }

        [
            'dates'      => $dates,
            'targetings' => $targetings,
        ] = $this->historyHelper->findDiffBetweenSuccessfulAndFailedInventoryCheck($history);

        $messages = [];

        if ($this->historyHelper->areDatesChanged($dates)) {
            if ($this->areDatesBecomeWider($dates)) {
                $messages[self::DATE_RANGE] = __('campaign::messages.dynamic_inventory_check.schedule.narrow');
            } else {
                $messages[self::DATE_RANGE] = __('campaign::messages.dynamic_inventory_check.schedule.expand');
            }
        }

        if ($this->shouldShowTargetingsSuggestions($targetings, $messages, $history)) {
            $targetingsSuggestions = $this->prepareTargetingSuggestions($history);
            $messages = array_merge($messages, $targetingsSuggestions);
        }

        return $messages;
    }

    /**
     * @param array                 $targetings
     * @param array                 $messages
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function shouldShowTargetingsSuggestions(
        array $targetings,
        array $messages,
        InventoryCheckHistory $history
    ): bool {
        if ($this->historyHelper->isTargetingsChanged($targetings)) {
            return true;
        }

        if (empty($messages) && $history->status_id === InventoryCheckStatus::ID_UNAVAILABLE) {
            return true;
        }

        return false;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return string[]
     */
    private function prepareTargetingSuggestions(InventoryCheckHistory $history): array
    {
        return array_intersect_assoc(
            $this->getCampaignMessages($history->campaign),
            $this->getHistoryMessages($history)
        );
    }

    /**
     * @param Campaign $campaign
     *
     * @return array
     */
    private function getCampaignMessages(Campaign $campaign): array
    {
        $messages = $this->getTargetingsFromCampaign($campaign)
            ->mapWithKeys(
                fn(TargetingValue $targeting): array => $this->createTargetMoreMessage($targeting)
            )
            ->toArray();

        if (Arr::has($messages, self::AGE_GROUP) && $this->checkIfAllAgesChosenInCampaign($campaign)) {
            $messages = $this->removeTargetingFromMessages($messages, self::AGE_GROUP);
        }

        if (Arr::has($messages, self::DEVICE_GROUP) && $this->checkIfAllDevicesChosenInCampaign($campaign)) {
            $messages = $this->removeTargetingFromMessages($messages, self::DEVICE_GROUP);
        }

        return $messages;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return array
     */
    private function getHistoryMessages(InventoryCheckHistory $history): array
    {
        $messages = [];
        $historyTargetings = $this->getTargetingsFromHistory($history->targetings);

        foreach ($historyTargetings as $targeting) {
            $messages = array_merge($messages, $this->createTargetMoreMessage($targeting));
        }

        if (Arr::has($messages, self::AGE_GROUP) && $this->checkIfAllAgesChosen($history)) {
            $messages = $this->removeTargetingFromMessages($messages, self::AGE_GROUP);
        }

        if (Arr::has($messages, self::DEVICE_GROUP) && $this->checkIfAllDevicesChosen($history)) {
            $messages = $this->removeTargetingFromMessages($messages, self::DEVICE_GROUP);
        }

        return $messages;
    }

    /**
     * @param TargetingValue $targeting
     *
     * @return string[]
     */
    private function createTargetMoreMessage(TargetingValue $targeting): array
    {
        return [
            $this->formatTargetingClassToName(get_class($targeting)) =>
                __('campaign::messages.dynamic_inventory_check.expand'),
        ];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function checkIfAllDevicesChosen(InventoryCheckHistory $history): bool
    {
        return $this->getSelectedDevicesFromHistory($history)->count() === $this->deviceRepository->count();
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function checkIfAllDevicesChosenInCampaign(Campaign $campaign): bool
    {
        return $campaign->deviceGroups->count() === $this->deviceRepository->count();
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return Collection|DeviceGroup[]
     */
    private function getSelectedDevicesFromHistory(InventoryCheckHistory $history): Collection
    {
        $devices = $history->targetings->filter(function (InventoryCheckTargeting $targeting): bool {
            return $targeting->targetable_type === DeviceGroup::class;
        });

        return $devices->map(function (InventoryCheckTargeting $targeting): DeviceGroup {
            return $targeting->targetable;
        });
    }

    /**
     * @param array  $messages
     * @param string $targeting
     *
     * @return array
     */
    private function removeTargetingFromMessages(array $messages, string $targeting): array
    {
        Arr::forget($messages, $targeting);

        return $messages;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function checkIfAllAgesChosen(InventoryCheckHistory $history): bool
    {
        /** @var Collection|InventoryCheckHistory[] $ages */
        $ages = $history->targetings->filter(function (InventoryCheckTargeting $targeting): bool {
            return ($targeting->targetable instanceof AgeGroup);
        });

        $ages = $this->getTargetingsFromHistory($ages);

        return $this->areSelectedAllTargetings($ages);
    }

    /**
     * @param Campaign $campaign
     *
     * @return bool
     */
    private function checkIfAllAgesChosenInCampaign(Campaign $campaign): bool
    {
        return $this->areSelectedAllTargetings($campaign->targetingAgeGroups);
    }

    /**
     * @param Collection $selectedAges
     *
     * @return bool
     */
    private function areSelectedAllTargetings(Collection $selectedAges): bool
    {
        $all = $this->ageGroupRepository->all();
        $allGender = Gender::getAllGender();

        // at first check all
        $agesAll = $all->filter(function (AgeGroup $age) use ($allGender): bool {
            return $age->gender_id === $allGender->id;
        });

        if ($agesAll->diff($selectedAges)->isEmpty()) {
            return true;
        }

        // then check all by all males and all females
        $agesMaleAndFemale = $all->filter(function (AgeGroup $age) use ($allGender): bool {
            return $age->gender_id !== $allGender->id;
        });

        if ($agesMaleAndFemale->diff($selectedAges)->isEmpty()) {
            return true;
        }

        return false;
    }

    /**
     * @param string $targetingClassName
     *
     * @return string
     */
    private function formatTargetingClassToName(string $targetingClassName): string
    {
        $map = [
            Location::class    => self::LOCATION,
            Zipcode::class     => self::ZIPCODE,
            AgeGroup::class    => self::AGE_GROUP,
            Audience::class    => self::AUDIENCE,
            DeviceGroup::class => self::DEVICE_GROUP,
            Genre::class       => self::GENRE,
        ];

        return Arr::get($map, $targetingClassName);
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return bool
     */
    private function areDatesBecomeWider(array $dates): bool
    {
        $succeededStart = $this->historyHelper->getSucceededStartDate($dates);
        $failedStart = $this->historyHelper->getFailedStartDate($dates);
        $succeededEnd = $this->historyHelper->getSucceededEndDate($dates);
        $failedEnd = $this->historyHelper->getFailedEndDate($dates);

        if (is_null($succeededStart) || is_null($succeededEnd)) {
            return false;
        }

        $failedLength = $failedStart->diffInDays($failedEnd);
        $succeededLength = $succeededStart->diffInDays($succeededEnd);

        return $failedLength > $succeededLength;
    }
}
