<?php

namespace Modules\Campaign\Actions;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Log\Logger;
use Modules\Campaign\Actions\Traits\SavesStep;
use Modules\Campaign\Events\CampaignStepUpdated;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Contracts\Campaignable;

class UpdateCampaignStepAction
{
    use SavesStep;

    /**
     * @var Logger
     */
    protected Logger $log;

    /**
     * @var Dispatcher
     */
    protected Dispatcher $dispatcher;

    /**
     * @param Logger     $log
     * @param Dispatcher $dispatcher
     */
    public function __construct(Logger $log, Dispatcher $dispatcher)
    {
        $this->log = $log;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Campaignable $campaign
     * @param int          $stepId
     *
     * @return Campaignable
     * @throws CampaignNotUpdatedException
     */
    public function handle(Campaignable $campaign, int $stepId): Campaignable
    {
        $this->log->info('Updating campaign step.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'step_id'       => $stepId,
        ]);

        $this->saveStep($campaign, $stepId);
        $this->dispatcher->dispatch(new CampaignStepUpdated($campaign));

        $this->log->info('Campaign step was successfully updated.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        return $campaign;
    }
}
