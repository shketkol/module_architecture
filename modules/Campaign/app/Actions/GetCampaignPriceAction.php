<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGetPrice;
use Modules\Haapi\DataTransferObjects\Campaign\CampaignPriceParams;
use Modules\Targeting\Actions\GetCampaignTargetingsAction;
use Modules\Targeting\Exceptions\TypeNotFoundException;

class GetCampaignPriceAction
{
    /** Constants to use in HAAPI Inventory Check */
    private const PRODUCT_ID_KEY = 'daapi.campaign.productId';

    /**
     * @var CampaignGetPrice
     */
    private CampaignGetPrice $campaignGetPrice;

    /**
     * @var GetCampaignTargetingsAction
     */
    private GetCampaignTargetingsAction $getCampaignTargetings;

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @param CampaignGetPrice            $campaignGetPrice
     * @param Logger                      $log
     * @param GetCampaignTargetingsAction $getCampaignTargetings
     */
    public function __construct(
        CampaignGetPrice $campaignGetPrice,
        Logger $log,
        GetCampaignTargetingsAction $getCampaignTargetings
    ) {
        $this->campaignGetPrice = $campaignGetPrice;
        $this->log = $log;
        $this->getCampaignTargetings = $getCampaignTargetings;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     */
    public function handle(Campaignable $campaign): array
    {
        $this->log->info('Fetching campaign price.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        $payload = $this->fetchCampaignPrice($campaign);

        $this->log->info('Campaign price fetched.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'payload'       => $payload,
        ]);

        return $payload;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     * @throws TypeNotFoundException
     */
    private function fetchCampaignPrice(Campaignable $campaign): array
    {
        $userId = Auth::user()->getAuthIdentifier();

        $campaignPriceParams = new CampaignPriceParams([
            'targets'   => $this->getCampaignTargetings->handle($campaign),
            'productId' => config(self::PRODUCT_ID_KEY),
        ]);

        $response = $this->campaignGetPrice->handle($campaignPriceParams, $userId);

        return $response->getPayload();
    }
}
