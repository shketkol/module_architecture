<?php

namespace Modules\Campaign\Actions;

use App\Repositories\Contracts\TimezoneRepository;
use Carbon\Carbon;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\CampaignStep;
use Illuminate\Config\Repository as Config;
use Modules\Campaign\Models\Contracts\Campaignable;

class UpdateCampaignScheduleAction
{
    /**
     * @var Carbon
     */
    private Carbon $carbon;

    /**
     * @var string
     */
    private string $dateFormat;

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var TimezoneRepository
     */
    private TimezoneRepository $repository;

    /**
     * @var UpdateCampaignStepAction
     */
    private UpdateCampaignStepAction $stepAction;

    /**
     * @param Carbon                   $carbon
     * @param Config                   $config
     * @param Logger                   $log
     * @param TimezoneRepository       $repository
     * @param UpdateCampaignStepAction $stepAction
     */
    public function __construct(
        Carbon $carbon,
        Config $config,
        Logger $log,
        TimezoneRepository $repository,
        UpdateCampaignStepAction $stepAction
    ) {
        $this->carbon = $carbon;
        $this->dateFormat = $config->get('date.format.php');
        $this->log = $log;
        $this->repository = $repository;
        $this->stepAction = $stepAction;
    }

    /**
     * Update date range for the campaign.
     *
     * @param Campaignable $campaign
     * @param array        $data
     *
     * @return Campaignable
     * @throws CampaignNotUpdatedException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaignable $campaign, array $data = []): Campaignable
    {
        $this->log->info('Updating campaign schedule.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'data'          => $data,
        ]);

        $this->setStartDate($campaign, Arr::get($data, 'date.start'));
        $this->setEndDate($campaign, Arr::get($data, 'date.end'));
        $this->setTimezone($campaign, Arr::get($data, 'timezone_id'));
        $this->stepAction->handle($campaign, CampaignStep::ID_SCHEDULE);

        if (!$campaign->save()) {
            $this->log->warning('Campaign schedule was not updated.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
            ]);
            throw CampaignNotUpdatedException::create();
        }

        $this->log->info('Campaign schedule was successfully updated.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        return $campaign;
    }

    /**
     * @param Campaignable $campaign
     * @param int|null     $timezoneId
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function setTimezone(Campaignable $campaign, ?int $timezoneId = null): void
    {
        if (is_null($timezoneId)) {
            return;
        }

        /** @var \App\Models\Timezone $timezone */
        $timezone = $this->repository->findSoft($timezoneId);
        $campaign->timezone_id = $timezone ? $timezone->id : null;
    }

    /**
     * @param Campaignable $campaign
     * @param string|null  $start
     */
    protected function setStartDate(Campaignable $campaign, ?string $start = null): void
    {
        if (!is_null($start)) {
            $start = $this->carbon->createFromFormat($this->dateFormat, $start)
                ->setTime(
                    config('campaign.wizard.time.start.hours'),
                    config('campaign.wizard.time.start.minutes'),
                    config('campaign.wizard.time.start.seconds')
                );
        }

        $campaign->date_start = $start;
    }

    /**
     * @param Campaignable $campaign
     * @param string|null  $end
     */
    protected function setEndDate(Campaignable $campaign, ?string $end = null): void
    {
        if (!is_null($end)) {
            $end = $this->carbon->createFromFormat($this->dateFormat, $end)
                ->setTime(
                    config('campaign.wizard.time.end.hours'),
                    config('campaign.wizard.time.end.minutes'),
                    config('campaign.wizard.time.end.seconds')
                );
        }

        $campaign->date_end = $end;
    }
}
