<?php

namespace Modules\Campaign\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignRevisionStatus;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Daapi\Actions\BaseAction;
use Prettus\Repository\Exceptions\RepositoryException;

class RevertCampaignUpdateAttemptAction extends BaseAction
{
    /**
     * @var DatabaseManager
     */
    protected DatabaseManager $databaseManager;

    /**
     * @param DatabaseManager            $databaseManager
     * @param Logger                     $log
     */
    public function __construct(
        DatabaseManager $databaseManager,
        Logger $log
    ) {
        $this->databaseManager = $databaseManager;
        parent::__construct($log);
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws CampaignNotUpdatedException
     */
    public function handle(Campaign $campaign): void
    {
        $this->log->info('[Revert Campaign Update Attempt] Started', [
            'campaign_id'          => $campaign->id,
            'campaign_external_id' => $campaign->external_id,
        ]);

        $this->databaseManager->beginTransaction();
        try {
            $this->revertCampaign($campaign);
            $this->revertCampaignRevision($campaign);
        } catch (\Throwable $e) {
            $this->log->error('[Revert Campaign Update Attempt] Failed');
            $this->log->logException($e);

            $this->databaseManager->rollBack();

            throw CampaignNotUpdatedException::create('Failed to update campaign during reverting update attempt');
        }
        $this->databaseManager->commit();

        $this->log->info('[Revert Update Attempt] Successfully finished');
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function revertCampaign(Campaign $campaign): void
    {
        $this->log->info('[Revert Campaign Update Attempt] Reverting campaign');
        $previousStatus = CampaignStatus::getTransitionById($campaign->previous_status_id);
        $campaign->applyInternalStatus($previousStatus);
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function revertCampaignRevision(Campaign $campaign): void
    {
        $this->log->info('[Revert Campaign Update Attempt] Reverting campaign revision');

        $revision = $campaign->getProcessingRevision();
        $this->log->info('[Revert Campaign Update Attempt] Revision to be reverted', [
            'revision_id' => $revision->id,
        ]);

        $revision->applyInternalStatus(CampaignRevisionStatus::DRAFT);
        $revision->submitted_at = null;

        $revision->update(['submitted_at' => null]);
    }
}
