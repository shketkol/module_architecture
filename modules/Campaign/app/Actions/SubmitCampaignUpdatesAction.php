<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Validations\ValidateCampaignAction;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\CampaignRevisionStatus;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Creative\Models\CreativeStatus;
use Modules\Haapi\Actions\Campaign\UpdateCampaignAction;
use Psr\Log\LoggerInterface;

class SubmitCampaignUpdatesAction
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $log;

    /**
     * @var DatabaseManager
     */
    protected DatabaseManager $databaseManager;

    /**
     * @var InventoryCheckHistoryRepository
     */
    protected InventoryCheckHistoryRepository $historyRepository;

    /**
     * @var UpdateCampaignAction
     */
    protected UpdateCampaignAction $updateCampaignAction;

    /**
     * @var ValidateCampaignAction
     */
    private ValidateCampaignAction $validateCampaignAction;

    /**
     * @param DatabaseManager                 $databaseManager
     * @param LoggerInterface                 $log
     * @param UpdateCampaignAction            $updateCampaignAction
     * @param InventoryCheckHistoryRepository $historyRepository
     * @param ValidateCampaignAction          $validateCampaignAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        UpdateCampaignAction $updateCampaignAction,
        InventoryCheckHistoryRepository $historyRepository,
        ValidateCampaignAction $validateCampaignAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->updateCampaignAction = $updateCampaignAction;
        $this->historyRepository = $historyRepository;
        $this->validateCampaignAction = $validateCampaignAction;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Campaignable
     * @throws CampaignNotUpdatedException
     * @throws ValidationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \SM\SMException
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign): Campaignable
    {
        $this->databaseManager->beginTransaction();

        try {
            $this->validateCampaignAction->handle($campaign);
            $this->checkCampaignCreative($campaign);
            $this->updateCampaignState($campaign);
            $this->updateRevisionState($campaign);
            $this->updateHaapiCampaign($campaign);
        } catch (\Throwable $exception) {
            $this->log->warning('Failed to submit campaign updates.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'reason'        => $exception->getMessage() ?? get_class($exception),
            ]);
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        try {
            $this->historyRepository->deleteCampaignHistory($campaign);
        } catch (\Throwable $exception) {
            //mute exception since it is not critical flow operation
            $this->log->warning($exception->getMessage());
        }

        return $campaign->refresh();
    }

    /**
     * @param Campaignable $campaign
     *
     * @return void
     * @throws CampaignNotUpdatedException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function updateCampaignState(Campaignable $campaign): void
    {
        $campaign = $campaign->getOrigin();
        $this->log->info('Setting PROCESSING state to the campaign.', ['campaign_id' => $campaign->id]);

        $campaign->applyInternalStatus(CampaignStatus::PROCESSING);

        if (!$campaign->save()) {
            $this->log->warning("Failed to update campaign's state", ['campaign_id' => $campaign->id]);
            throw CampaignNotUpdatedException::create("Failed to update campaign's state");
        }

        $this->log->info("Campaign's state was successfully set to PROCESSING.", ['campaign_id' => $campaign->id]);
    }

    /**
     * @param Campaignable $revision
     *
     * @return void
     * @throws CampaignNotUpdatedException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function updateRevisionState(Campaignable $revision): void
    {
        if ($revision->isOrigin()) {
            $this->log->warning('Campaign is used instead of revision', ['campaign_id' => $revision->id]);
            throw CampaignNotUpdatedException::create('Campaign is used instead of revision');
        }

        $this->log->info('Setting PROCESSING state to the revision.', ['revision_id' => $revision->id]);

        $revision->applyInternalStatus(CampaignRevisionStatus::PROCESSING);
        $revision->submitted_at = Carbon::now();

        if (!$revision->save()) {
            $this->log->warning("Failed to update revision's state", ['revision_id' => $revision->id]);
            throw CampaignNotUpdatedException::create("Failed to update revision's state");
        }

        $this->log->info("Revision's state was successfully set to PROCESSING.", ['revision_id' => $revision->id]);
    }

    /**
     * @param Campaignable $campaign
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    private function updateHaapiCampaign(Campaignable $campaign): void
    {
        $this->updateCampaignAction->handle($campaign);
    }

    /**
     * @param Campaignable $campaign
     * @throws ValidationException
     */
    protected function checkCampaignCreative(Campaignable $campaign): void
    {
        $creative = $campaign->getActiveCreative();

        if (!$creative || !$creative->inState(CreativeStatus::ID_APPROVED)) {
            throw ValidationException::withMessages([__('creative::messages.replace_to_approved')]);
        }
    }
}
