<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Support\Arr;

trait FilterBudgetValues
{
    /**
     * Field limitations. It is used to store data in database even if it has invalid values.
     *
     * @var array
     */
    protected $fieldLimitations = [
        'cpm'             => [
            'min' => 0,
            'max' => 1000,
        ],
        'cost'            => [
            'min' => 0,
            'max' => 999999.99,
        ],
        'impressions'     => [
            'min' => 0,
            'max' => 99999999999,
        ],
    ];

    /**
     * Filter data to allow saving in database.
     *
     * @param string $field
     * @param mixed $value
     *
     * @return mixed
     */
    protected function filterBudgetData(string $field, $value)
    {
        $limitations = Arr::get($this->fieldLimitations, $field);

        // Set min value
        $min = Arr::get($limitations, 'min');
        $value = max($value, $min);

        // Set max value
        $max = Arr::get($limitations, 'max');
        $value = min($value, $max);

        return $value;
    }
}
