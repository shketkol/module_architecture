<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Support\Collection;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Models\InventoryCheckTargeting;
use Modules\Targeting\Models\TargetingValue;

trait Targetings
{
    use CampaignTargetingCollect;

    /**
     * @param Collection|InventoryCheckTargeting[] $targetings
     *
     * @return TargetingValue[]|Collection
     */
    private function getTargetingsFromHistory(Collection $targetings): Collection
    {
        return $targetings->map(function (InventoryCheckTargeting $targeting): TargetingValue {
            // we add dynamic attribute here so it would be possible to determine if targeting was or wasn't excluded
            $targeting->targetable->excluded = $targeting->excluded;

            return $targeting->targetable;
        });
    }

    /**
     * @param Campaignable $campaign
     *
     * @return TargetingValue[]|Collection
     */
    private function getTargetingsFromCampaign(Campaignable $campaign): Collection
    {
        $result = collect([]);
        foreach ($this->collectCampaignTargetingsForDatabase($campaign) as $collection) {
            foreach ($collection as $targeting) {
                $result->add($targeting);
            }
        }

        return $result;
    }
}
