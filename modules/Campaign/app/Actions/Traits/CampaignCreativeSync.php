<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\Alerts\SetCampaignAlertsAction;
use Modules\Campaign\Actions\UpdateCampaignStepAction;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Creative\Models\CreativeStatus;
use Modules\Haapi\Actions\Creative\Contracts\CampaignCreativeUpdate;
use Modules\Haapi\Actions\Creative\Contracts\CampaignCreativeUpload;
use Modules\Haapi\DataTransferObjects\Creative\CampaignCreativeUpdateParams;
use Modules\Haapi\DataTransferObjects\Creative\CampaignCreativeUploadParams;
use Modules\Notification\Actions\DestroyAlertsAction;

trait CampaignCreativeSync
{
    /**
     * @var Logger
     */
    protected Logger $log;

    /**
     * @var CampaignCreativeUpdate
     */
    protected CampaignCreativeUpdate $campaignCreativeUpdate;

    /**
     * @var DatabaseManager
     */
    protected DatabaseManager $databaseManager;

    /**
     * @var CampaignCreativeUpload
     */
    protected CampaignCreativeUpload $campaignCreativeUpload;

    /**
     * @var DestroyAlertsAction
     */
    protected DestroyAlertsAction $destroyAlertsAction;

    /**
     * @var SetCampaignAlertsAction
     */
    protected SetCampaignAlertsAction $setCampaignAlertsAction;

    /**
     * @var UpdateCampaignStepAction
     */
    protected UpdateCampaignStepAction $stepAction;

    /**
     * @param CampaignCreativeUpdate   $campaignCreativeUpdate
     * @param CampaignCreativeUpload   $campaignCreativeUpload
     * @param DatabaseManager          $databaseManager
     * @param DestroyAlertsAction      $destroyAlertsAction
     * @param Logger                   $log
     * @param SetCampaignAlertsAction  $setCampaignAlertsAction
     * @param UpdateCampaignStepAction $stepAction
     */
    public function __construct(
        CampaignCreativeUpdate $campaignCreativeUpdate,
        CampaignCreativeUpload $campaignCreativeUpload,
        DatabaseManager $databaseManager,
        DestroyAlertsAction $destroyAlertsAction,
        Logger $log,
        SetCampaignAlertsAction $setCampaignAlertsAction,
        UpdateCampaignStepAction $stepAction
    ) {
        $this->campaignCreativeUpdate = $campaignCreativeUpdate;
        $this->campaignCreativeUpload = $campaignCreativeUpload;
        $this->databaseManager = $databaseManager;
        $this->destroyAlertsAction = $destroyAlertsAction;
        $this->log = $log;
        $this->setCampaignAlertsAction = $setCampaignAlertsAction;
        $this->stepAction = $stepAction;
    }

    /**
     * Sync campaign creative with HAAPI
     *
     * @param Campaignable $campaign
     *
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function sync(Campaignable $campaign): void
    {
        if (!$campaign->isOrigin() && $campaign->isDraft()) {
            return;
        }

        $campaignId = $campaign->external_id;
        $userId = Auth::id();

        /**
         * If campaign has no ext id or in Draft mode then do nothing
         */
        if (!$campaignId || $campaign->getOrigin()->inState(CampaignStatus::ID_DRAFT)) {
            return;
        }

        $creative = $campaign->getActiveCreative();

        if (!$creative) {
            return;
        }

        if ($creative->external_id) {
            $this->campaignCreativeUpdate->handle(new CampaignCreativeUpdateParams([
                'campaignId' => $campaignId,
                'creativeId' => $creative->external_id,
            ]), $userId);
        } else {
            $this->campaignCreativeUpload->handle(new CampaignCreativeUploadParams([
                'campaignId'    => $campaign->external_id,
                'accountId'     => Auth::user()->account_external_id,
                'fileName'      => $creative->name . '.' . $creative->extension,
                'sourceUrl'     => $creative->source_url,
                'contentLength' => (int)$creative->filesize,
            ]), $userId);

            $creative->applyInternalStatus(CreativeStatus::PROCESSING);
            $creative->save();
        }

        $campaign->applyInternalStatus(CampaignStatus::PROCESSING);
        $campaign->save();
        $campaign->load(['creatives', 'status']);
    }
}
