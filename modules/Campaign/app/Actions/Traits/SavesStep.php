<?php

namespace Modules\Campaign\Actions\Traits;

use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Contracts\Campaignable;

trait SavesStep
{
    /**
     * @param Campaignable $campaign
     * @param int          $stepId
     *
     * @throws CampaignNotUpdatedException
     */
    protected function saveStep(Campaignable $campaign, int $stepId): void
    {
        $this->associateStep($campaign, $stepId);

        if (!$campaign->save()) {
            throw CampaignNotUpdatedException::create('Campaign save was not updated.');
        }
    }

    /**
     * Associate step with the campaign.
     *
     * @param Campaignable $campaign
     * @param int          $stepId
     */
    protected function associateStep(Campaignable $campaign, int $stepId): void
    {
        // We should not override already stored step
        if ($campaign->step && $campaign->step_id > $stepId) {
            return;
        }

        $campaign->step()->associate($stepId);
    }
}
