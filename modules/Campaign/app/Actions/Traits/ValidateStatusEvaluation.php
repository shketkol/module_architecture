<?php

namespace Modules\Campaign\Actions\Traits;

use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Exceptions\SameStatusException;
use Modules\Campaign\Models\Campaign;
use Modules\User\Models\User;

trait ValidateStatusEvaluation
{
    /**
     * @param Campaign $campaign
     * @param User     $user
     * @param string   $status
     *
     * @throws CampaignNotUpdatedException
     * @throws \SM\SMException
     * @throws SameStatusException
     */
    protected function canApply(Campaign $campaign, User $user, string $status): void
    {
        if ($campaign->canApply($status)) {
            return;
        }

        if ($campaign->status->name === $status) {
            throw SameStatusException::create(trans('campaign::messages.status.same', [
                'action' => trans("campaign::messages.action.{$status}"),
                'status' => $status,
                'actor'  => $campaign->status_change_actor,
            ]));
        }

        if (!$campaign->statusChangedBy->is($user)) {
            throw CampaignNotUpdatedException::create(trans('campaign::messages.resume.failed'));
        }

        throw CampaignNotUpdatedException::create(trans('campaign::messages.status_not_updated'));
    }
}
