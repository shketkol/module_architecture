<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignGendersAction;
use Modules\Targeting\Models\AgeGroup;

trait CampaignTargetingAttach
{
    /**
     * @param Campaignable $campaign
     * @param Collection[] $targetings
     * @param array        $override
     *
     * @return void
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function setTargetingValues(Campaignable $campaign, array $targetings, array $override = []): void
    {
        foreach ($targetings as $targetingName => $collection) {
            if (in_array($targetingName, CampaignStep::STEPS_RELATIONS[CampaignStep::ID_AGES])) {
                $this->setGender($campaign, $collection);
            }

            $data = Arr::pluck($collection->toArray(), 'pivot');
            $data = array_map(function (array $item) use ($override): array {
                $item = array_merge($item, $override);
                return Arr::except($item, 'campaign_id');
            }, $data);

            $campaign->$targetingName()->attach($data);
        }
    }

    /**
     * @param Campaignable          $campaign
     * @param Collection|AgeGroup[] $agesGroups
     *
     * @return void
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    private function setGender(Campaignable $campaign, Collection $agesGroups): void
    {
        $genderId = $agesGroups->first()->gender_id;

        /** @var UpdateCampaignGendersAction $updateGenders */
        $updateGenders = app(UpdateCampaignGendersAction::class);
        $updateGenders->handle($campaign, $genderId);
    }

    /**
     * @param Campaignable $campaignableFrom
     * @param Campaignable $campaignableTo
     *
     * @return void
     */
    private function attachCreative(Campaignable $campaignableFrom, Campaignable $campaignableTo): void
    {
        $creatives = $campaignableFrom->creatives;
        if ($creatives) {
            $campaignableTo->creatives()->detach();
            $campaignableTo->creatives()->attach($creatives);
        }
    }
}
