<?php

namespace Modules\Campaign\Actions\Traits;

use Carbon\Carbon;
use Illuminate\Support\Arr;

trait GetCampaignPerformance
{
    use GetGenericStructure;

    /**
     * Get performance for the campaign.
     *
     * Must be in the GetCampaignImpressionsDetailAction response format.
     *
     * @param array[] $impressionsDetails
     *
     * @return array
     */
    protected function getPerformance(array $impressionsDetails): array
    {
        $impressions = Arr::get($impressionsDetails, 'impressionCounts');
        $today = Carbon::now();

        $data = Arr::where($impressions, function (array $value) use ($today): bool {
            $dateStart = Carbon::createFromFormat(config('date.format.haapi_milliseconds'), $value['startTime']);
            return $dateStart->lte($today);
        });

        if (empty($data)) {
            return Arr::get($this->getGenericStructure(), 'performance');
        }

        $data = array_slice($data, -30);
        $nativeFormat = config('date.format.php');
        $startDate = Carbon::parse(Arr::first($data)['startTime'])->format($nativeFormat);
        $endDate = Carbon::parse(Arr::last($data)['startTime'])->format($nativeFormat);

        return [
            'total'       => Arr::get($impressionsDetails, 'impressions'),
            'startDate'   => $startDate,
            'endDate'     => $endDate,
            'performance' => $data,
        ];
    }
}
