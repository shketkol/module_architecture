<?php

namespace Modules\Campaign\Actions\Traits;

use Illuminate\Database\Eloquent\Collection;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Models\TargetingValue;

trait CampaignTargetingCollect
{
    /**
     * @var string[]
     */
    protected array $campaignTargetingRelations = [
        'genres',
        'zipcodes',
        'dma',
        'cities',
        'states',
        'targetingAgeGroups',
        'audiences',
        'deviceGroups',
    ];

    /**
     * @var string[]
     */
    protected array $dbRelations = [
        'targetingAgeGroups',
        'audiences',
        'locations',
        'zipcodes',
        'genres',
        'deviceGroups',
    ];

    /**
     * Collect campaign targetings into array
     *
     * @param Campaignable $campaign
     * @param bool         $visible
     *
     * @return array
     */
    public function collectCampaignTargetings(Campaignable $campaign, bool $visible = false): array
    {
        $targetingsArray = [];

        foreach ($this->campaignTargetingRelations as $relation) {
            $nonVisibleTargetings = $campaign->{$relation}()->where('visible', $visible)->get();
            if ($nonVisibleTargetings->isNotEmpty()) {
                $targetingsArray[$relation] = $nonVisibleTargetings;
            }
        }

        return $targetingsArray;
    }

    /**
     * Collect campaign all targetings into array
     *
     * @param Campaignable $campaign
     *
     * @return array
     */
    public function collectAllCampaignTargetings(Campaignable $campaign): array
    {
        $targetingsArray = [];

        foreach ($this->campaignTargetingRelations as $relation) {
            $nonVisibleTargetings = $campaign->{$relation}()->get();
            if ($nonVisibleTargetings->isNotEmpty()) {
                $targetingsArray[$relation] = $nonVisibleTargetings;
            }
        }

        return $targetingsArray;
    }

    /**
     * Collect campaign all targetings into array the way it saved in the database
     *
     * @param Campaignable $campaign
     *
     * @return Collection[]|TargetingValue[][]
     */
    public function collectCampaignTargetingsForDatabase(Campaignable $campaign): array
    {
        $collection = [];
        foreach ($this->dbRelations as $relation) {
            $collection[$relation] = $campaign->{$relation}()->get();
        }

        return $collection;
    }

    /**
     * @param Campaignable $campaignable
     *
     * @return void
     */
    private function detachRelations(Campaignable $campaignable): void
    {
        foreach ($this->dbRelations as $relation) {
            $campaignable->{$relation}()->detach();
        }
    }
}
