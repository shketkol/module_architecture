<?php

namespace Modules\Campaign\Actions\Alerts;

use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\AlertType;

class SetCantGoLiveAlertAction extends SetCampaignAlertAction
{
    /**
     * Set campaign 'Cannot Go Live' alert.
     *
     * @param Campaign $campaign
     * @return bool
     */
    public function handle(Campaign $campaign): bool
    {
        return $this->storeAlertAction->handle(
            $campaign,
            AlertType::ID_CANT_GO_LIVE,
            $campaign->start_date_with_timezone
        );
    }
}
