<?php

namespace Modules\Campaign\Actions\Alerts;

use Modules\Campaign\Models\Campaign;

class SetCampaignAlertsAction
{
    /**
     * @var SetCantGoLiveAlertAction
     */
    private SetCantGoLiveAlertAction $setCantGoLiveAlert;

    /**
     * @var SetCreativeMissingAlertAction
     */
    private SetCreativeMissingAlertAction $setCreativeMissingAlert;

    /**
     * @param SetCantGoLiveAlertAction      $setCantGoLiveAlert
     * @param SetCreativeMissingAlertAction $setCreativeMissingAlert
     */
    public function __construct(
        SetCantGoLiveAlertAction $setCantGoLiveAlert,
        SetCreativeMissingAlertAction $setCreativeMissingAlert
    ) {
        $this->setCantGoLiveAlert = $setCantGoLiveAlert;
        $this->setCreativeMissingAlert = $setCreativeMissingAlert;
    }

    /**
     * Set all campaign alerts.
     *
     * @param Campaign $campaign
     * @param bool     $force
     * @param bool     $receivedRejected
     *
     * @return bool
     */
    public function handle(Campaign $campaign, bool $force = false, bool $receivedRejected = false): bool
    {
        if (!$campaign->date_start) {
            return true;
        }

        $creative = $campaign->getActiveCreative();

        if (!$force && $creative && !($creative->isRejected() || $receivedRejected)) {
            return true;
        }

        return $this->setCreativeMissingAlert->handle($campaign) && $this->setCantGoLiveAlert->handle($campaign);
    }
}
