<?php

namespace Modules\Campaign\Actions;

use App\Helpers\EnvironmentHelper;
use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignExternalIdOverrideException;
use Modules\Campaign\Exceptions\CancelCampaignActivityRequestException;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\Haapi\Actions\Campaign\CampaignDraftSave;
use Modules\Haapi\Actions\Campaign\GetDraftActivityCampaignParamsAction;
use Modules\Haapi\DataTransferObjects\Campaign\DraftActivityCampaignParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

class SendDraftActivityCampaignAction
{
    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var GetDraftActivityCampaignParamsAction
     */
    private GetDraftActivityCampaignParamsAction $paramsAction;

    /**
     * @var CampaignDraftSave
     */
    private CampaignDraftSave $draftSave;

    /**
     * @var CampaignRepository
     */
    private CampaignRepository $campaignRepository;

    /**
     * @param Logger                               $log
     * @param GetDraftActivityCampaignParamsAction $paramsAction
     * @param CampaignDraftSave                    $draftSave
     * @param CampaignRepository                   $campaignRepository
     */
    public function __construct(
        Logger $log,
        GetDraftActivityCampaignParamsAction $paramsAction,
        CampaignDraftSave $draftSave,
        CampaignRepository $campaignRepository
    ) {
        $this->log = $log;
        $this->paramsAction = $paramsAction;
        $this->draftSave = $draftSave;
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @param Campaignable $campaign
     * @param array        $data
     *
     * @return HaapiResponse
     * @throws CampaignExternalIdOverrideException
     * @throws CancelCampaignActivityRequestException
     * @throws \App\Exceptions\InvalidArgumentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaignable $campaign, array $data): HaapiResponse
    {
        $this->checkIsRequestAllowed($campaign);

        $this->log->info('[Campaign][Draft Activity] Started sending activity to HAAPI.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'data'          => $data,
        ]);

        $params = new DraftActivityCampaignParams($this->paramsAction->handle($campaign, $data));
        $response = $this->draftSave->handle($params, $campaign->user_id);
        $this->storeExternalId($campaign->getOrigin(), Arr::get($response->getPayload(), 'campaign.id'));

        $this->log->info('[Campaign][Draft Activity] Finished sending activity to HAAPI.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'data'          => $data,
        ]);

        return $response;
    }

    /**
     * @param Campaignable $campaign
     * @param string       $externalId
     *
     * @return Campaignable
     * @throws CampaignExternalIdOverrideException
     */
    private function storeExternalId(Campaignable $campaign, string $externalId): Campaignable
    {
        $this->log->info('[Campaign][Draft Activity] Started updating campaign external id.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'external_id'   => $externalId,
        ]);

        if (!$campaign->exists) {
            $this->log->info('[Campaign][Draft Activity] Skipped updating campaign external id. Replica provided.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'external_id'   => $externalId,
            ]);

            return $campaign;
        }

        $this->checkExternalIdOverride($campaign, $externalId);

        if ($campaign->external_id) {
            $this->log->info('[Campaign][Draft Activity] Skipped updating campaign external id. Already exists.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'external_id'   => $externalId,
            ]);

            return $campaign;
        }

        return $this->saveExternalId($campaign, $externalId);
    }

    /**
     * @param Campaignable $campaign
     * @param string       $externalId
     *
     * @throws CampaignExternalIdOverrideException
     */
    private function checkExternalIdOverride(Campaignable $campaign, string $externalId): void
    {
        if ($campaign->external_id && $campaign->external_id !== $externalId) {
            $this->log->error('[Campaign][Draft Activity] HAAPI external id does not match with saved.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'external_id'   => $externalId,
            ]);

            throw CampaignExternalIdOverrideException::create($campaign, $externalId);
        }
    }

    /**
     * @param Campaignable $campaign
     * @param string       $externalId
     *
     * @return Campaignable
     */
    private function saveExternalId(Campaignable $campaign, string $externalId): Campaignable
    {
        $campaign = $this->campaignRepository->update(['external_id' => $externalId], $campaign->id);

        $this->log->info('[Campaign][Draft Activity] Finished updating campaign external id.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'external_id'   => $externalId,
        ]);

        return $campaign;
    }

    /**
     * @param Campaignable $campaign
     *
     * @throws CancelCampaignActivityRequestException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function checkIsRequestAllowed(Campaignable $campaign): void
    {
        if (EnvironmentHelper::isLocalEnv()) {
            $this->log->info('[Campaign][Draft Activity] Cancelled. Environment not allowed.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
            ]);

            throw CancelCampaignActivityRequestException::create($campaign);
        }

        if ($campaign->isLiveEdit()) {
            $this->log->info('[Campaign][Draft Activity] Cancelled. Campaign is in live edit mode.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
            ]);

            throw CancelCampaignActivityRequestException::create($campaign);
        }

        if ($campaign->user->isPendingManualReview()) {
            $this->log->info('[Campaign][Draft Activity] Cancelled. User is in pending status.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
            ]);

            throw CancelCampaignActivityRequestException::create($campaign);
        }
    }
}
