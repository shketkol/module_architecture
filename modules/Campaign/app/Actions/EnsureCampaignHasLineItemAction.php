<?php

namespace Modules\Campaign\Actions;

use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignHasNoLineItemException;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Haapi\Actions\Campaign\CampaignGet;

class EnsureCampaignHasLineItemAction
{
    /**
     * @var Logger
     */
    protected Logger $log;

    /**
     * @var CampaignGet
     */
    protected CampaignGet $campaignGetAction;

    /**
     * @param Logger      $log
     * @param CampaignGet $campaignGetAction
     */
    public function __construct(Logger $log, CampaignGet $campaignGetAction)
    {
        $this->log               = $log;
        $this->campaignGetAction = $campaignGetAction;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return void
     * @throws CampaignHasNoLineItemException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(Campaignable $campaign): void
    {
        if ($campaign->external_line_item_id) {
            return;
        }

        $this->log->info('Trying to get line-item from HAAPI');
        $campaignData = $this->campaignGetAction->handle($campaign->external_id, $campaign->user_id);
        if (!$campaignData->lineItems || !$campaignData->lineItems[0]->id) {
            $this->log->warning("Line-item wasn't found in HAAPI campaign details");
            throw new CampaignHasNoLineItemException("Line-item wasn't found in HAAPI campaign details");
        }

        $originCampaign = $campaign->getOrigin();
        $originCampaign->external_line_item_id = $campaignData->lineItems[0]->id;
        if (!$originCampaign->save()) {
            $this->log->warning("Campaign's external_line_item_id cannot be saved to DB.");
            throw new CampaignHasNoLineItemException("Campaign's external_line_item_id cannot be saved to DB.");
        }
    }
}
