<?php

namespace Modules\Campaign\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Campaign\Repositories\InventoryCheckStatusRepository;
use Psr\Log\LoggerInterface;

class UpdateInventoryCheckResultAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var InventoryCheckHistoryRepository
     */
    private $historyRepository;

    /**
     * @var InventoryCheckStatusRepository
     */
    private $statusRepository;

    /**
     * @param LoggerInterface                 $log
     * @param DatabaseManager                 $databaseManager
     * @param InventoryCheckHistoryRepository $historyRepository
     * @param InventoryCheckStatusRepository  $statusRepository
     */
    public function __construct(
        LoggerInterface $log,
        DatabaseManager $databaseManager,
        InventoryCheckHistoryRepository $historyRepository,
        InventoryCheckStatusRepository $statusRepository
    ) {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->historyRepository = $historyRepository;
        $this->statusRepository = $statusRepository;
    }

    /**
     * @param InventoryCheckHistory $history
     * @param array                 $payload
     *
     * @return InventoryCheckHistory
     * @throws InvalidArgumentException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Throwable
     */
    public function handle(InventoryCheckHistory $history, array $payload): InventoryCheckHistory
    {
        $this->log->info('[Inventory Check History] Started updating.', [
            'history_id'  => $history->id,
            'campaign_id' => $history->campaign_id,
            'payload'     => $payload,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $attributes = $this->prepareAttributes($payload);
            $history = $this->historyRepository->update($attributes, $history->id);
        } catch (\Throwable $exception) {
            $this->log->info('[Inventory Check History] Failed updating.', [
                'history_id'  => $history->id,
                'campaign_id' => $history->campaign_id,
                'payload'     => $payload,
                'reason'      => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();

            $this->historyRepository->markAsFailed($history);

            $this->log->info('[Inventory Check History] History marked as failed during update.', [
                'history_id'  => $history->id,
                'campaign_id' => $history->campaign_id,
            ]);

            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('[Inventory Check History] Finished updating.', [
            'history_id'  => $history->id,
            'campaign_id' => $history->campaign_id,
            'payload'     => $payload,
        ]);

        return $history->refresh();
    }

    /**
     * @param array $payload
     *
     * @return array
     * @throws \App\Exceptions\ModelNotFoundException
     */
    private function prepareAttributes(array $payload): array
    {
        return [
            'ordered_impressions'   => Arr::get($payload, 'quantity'),
            'available_impressions' => Arr::get($payload, 'availableImpressions'),
            'available_budget'      => Arr::get($payload, 'availableBudget'),
            'status_id'             => $this->statusRepository->findStatusId(Arr::get($payload, 'statusCode')),
        ];
    }
}
