<?php

namespace Modules\Campaign\Actions;

use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Traits\CampaignCreativeSync;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Creative\Models\Creative;
use Modules\Notification\Models\AlertType;

class ReplaceCampaignCreativeAction
{
    use CampaignCreativeSync;

    /**
     * Update campaign creative.
     *
     * @param Campaignable $campaign
     * @param int|null     $creativeId
     *
     * @return Campaignable
     * @throws CampaignNotUpdatedException
     * @throws ValidationException
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign, int $creativeId): Campaignable
    {
        if ($campaign->isOrigin() && $campaign->isReady()) {
            $this->checkForReadyCampaign($creativeId);
        }

        $activeCreative = $campaign->getActiveCreative();

        $this->log->info("Replacing campaign's creative.", [
            'campaign_id'          => $campaign->id,
            'campaign_type'        => get_class($campaign),
            'campaign_external_id' => $campaign->external_id,
            'old_creative_id'      => $activeCreative ? $activeCreative->getKey() : '',
            'new_creative_id'      => $creativeId,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $campaign->creatives()->sync([$creativeId]);
            $campaign->refresh();
            $this->sync($campaign);

            $this->destroyAlertsAction->handle($campaign->getOrigin(), [
                AlertType::ID_CANT_GO_LIVE,
                AlertType::ID_CREATIVE_MISSING,
            ]);

            $this->log->info("Campaign's creative was successfully replaced.", [
                'campaign_id'          => $campaign->id,
                'campaign_type'        => get_class($campaign),
                'campaign_external_id' => $campaign->external_id,
                'old_creative_id'      => $activeCreative ? $activeCreative->getKey() : '',
                'new_creative_id'      => $creativeId,
            ]);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();

            $this->log->warning('Campaign creative was not updated.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'reason'        => $exception->getMessage(),
            ]);

            throw CampaignNotUpdatedException::create($exception->getMessage(), $exception);
        }

        $this->databaseManager->commit();

        return $campaign;
    }

    /**
     * @param int $creativeId
     *
     * @return void
     *
     * @throws ValidationException
     */
    protected function checkForReadyCampaign(int $creativeId): void
    {
        $creative = Creative::where('id', $creativeId)->first();

        if (!$creative->isApproved()) {
            throw ValidationException::withMessages([__('creative::messages.replace_to_approved')]);
        }
    }
}
