<?php

namespace Modules\Campaign\Actions;

use Modules\Campaign\Actions\Traits\CampaignCreativeSync;
use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Notification\Models\AlertType;

class UpdateCampaignCreativeAction
{
    use CampaignCreativeSync;

    /**
     * Update campaign creative.
     *
     * @param Campaignable $campaign
     * @param int|null     $creativeId
     *
     * @return Campaignable
     * @throws CampaignNotUpdatedException
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign, ?int $creativeId = null): Campaignable
    {
        $this->databaseManager->beginTransaction();
        try {
            $creativeId ? $this->attach($campaign, $creativeId) : $this->detach($campaign);
            $this->stepAction->handle($campaign, CampaignStep::ID_CREATIVE);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            $this->log->warning('Campaign creative was not updated.', [
                'campaign_id' => $campaign->id,
                'reason'      => $exception->getMessage(),
            ]);

            throw CampaignNotUpdatedException::create($exception->getMessage(), $exception);
        }
        $this->databaseManager->commit();

        return $campaign;
    }

    /**
     * @param Campaignable $campaign
     * @param int          $creativeId
     *
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    protected function attach(Campaignable $campaign, int $creativeId): void
    {
        $this->log->info('Attach creative to campaign.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'creative_id'   => $creativeId,
        ]);

        $campaign->creatives()->attach($creativeId);
        $campaign->refresh();
        $this->sync($campaign);

        $this->log->info('Campaign creative was successfully attached.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
            'creative_id'   => $creativeId,
        ]);

        $this->destroyAlertsAction->handle($campaign->getOrigin(), [
            AlertType::ID_CANT_GO_LIVE,
            AlertType::ID_CREATIVE_MISSING,
        ]);
    }

    /**
     * @param Campaignable $campaign
     */
    protected function detach(Campaignable $campaign): void
    {
        $this->log->info('Detach all creatives from campaign.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);
        $campaign->creatives()->detach();

        $this->log->info('Campaign creative was successfully detached.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        $this->setCampaignAlertsAction->handle($campaign->getOrigin());
    }
}
