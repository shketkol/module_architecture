<?php

namespace Modules\Campaign\Actions;

use Illuminate\Support\Arr;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;

class GetHelpSuggestionAction
{
    /**
     * @var InventoryCheckHistoryRepository
     */
    private $historyRepository;

    /**
     * @param InventoryCheckHistoryRepository $historyRepository
     */
    public function __construct(InventoryCheckHistoryRepository $historyRepository)
    {
        $this->historyRepository = $historyRepository;
    }

    /**
     * @param array                 $suggestions
     * @param InventoryCheckHistory $history
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(array $suggestions, InventoryCheckHistory $history): array
    {
        if ($this->shouldAddHelpMessage($history)) {
            Arr::set($suggestions, 'help', $this->getHelpMessages());
        }

        return $suggestions;
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function shouldAddHelpMessage(InventoryCheckHistory $history): bool
    {
        $count = $this->historyRepository->countFailed($history->campaign);
        $needForHelp = config('inventory-check.dynamic_inventory_check.unavailable.times_before_faq');

        return $count >= $needForHelp;
    }

    /**
     * @return string[]
     */
    private function getHelpMessages(): array
    {
        return [
            'title'   => __('campaign::messages.dynamic_inventory_check.help.title'),
            'message' => __('campaign::messages.dynamic_inventory_check.help.message'),
            'link'    => config('inventory-check.dynamic_inventory_check.unavailable.faq_link'),
        ];
    }
}
