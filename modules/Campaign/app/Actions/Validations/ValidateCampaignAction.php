<?php

namespace Modules\Campaign\Actions\Validations;

use Illuminate\Support\Facades\Validator as FacadeValidator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Modules\Campaign\Actions\Validations\Abstracts\ValidateCampaignActionAbstract;
use Modules\Campaign\Models\Contracts\Campaignable;

final class ValidateCampaignAction extends ValidateCampaignActionAbstract
{
    /**
     * @param Campaignable $campaign
     *
     * @return void
     * @throws ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaignable $campaign): void
    {
        $validator = $this->validateCampaignData($campaign);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->validateCampaignVisibleTargetingHelper->validateVisibleTargetings($campaign);
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Validator
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function validateCampaignData(Campaignable $campaign): Validator
    {
        return FacadeValidator::make(
            $this->getCampaignDataToValidate($campaign),
            array_merge(
                $this->getNameValidation(),
                $this->getImpressionsValidation(),
                $this->getBudgetValidation($campaign),
                $this->getScheduleValidation($campaign),
                $this->getTargetingsValidation($campaign)
            ),
            $this->getMessages()
        );
    }
}
