<?php

namespace Modules\Campaign\Actions\Validations;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator as FacadeValidator;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Actions\Validations\Abstracts\ValidateCampaignActionAbstract;
use Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException;
use Modules\Campaign\Models\Contracts\Campaignable;

final class ValidateInventoryCheckAction extends ValidateCampaignActionAbstract
{
    /**
     * @param Campaignable $campaign
     *
     * @return array
     * @throws CampaignUsesNonVisibleTargetingException
     * @throws ValidationException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaignable $campaign): array
    {
        $validator = FacadeValidator::make(
            $this->getCampaignDataToValidate($campaign),
            array_merge(
                $this->getScheduleValidation($campaign),
                $this->getBudgetValidation($campaign),
            ),
            $this->getMessages()
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->validateCampaignVisibleTargetingHelper->validateVisibleTargetings($campaign);

        return [];
    }
}
