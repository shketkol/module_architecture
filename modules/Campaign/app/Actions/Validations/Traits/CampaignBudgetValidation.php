<?php

namespace Modules\Campaign\Actions\Validations\Traits;

use App\Helpers\NumberFormatHelper;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Modules\Campaign\Models\Contracts\Campaignable;

/**
 * @property ValidationRules $validationRules
 */
trait CampaignBudgetValidation
{
    /**
     * Get validation for budget step.
     *
     * @param Campaignable $campaign
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function getBudgetValidation(Campaignable $campaign): array
    {
        if ($campaign->isLiveEdit() && !$campaign->getOrigin()->hasPromocode()) {
            $this->validationRules->set('campaign.budget.cost.min', $campaign->getOrigin()->min_edit_budget);
        }

        if ($campaign->isLiveEdit() && $campaign->getOrigin()->hasPromocode()) {
            $this->validationRules->set('campaign.budget.cost.min', $campaign->getOrigin()->budget);
            $this->validationRules->set('campaign.budget.cost.max', $campaign->getOrigin()->budget);
        }

        return [
            'cost' => $this->validationRules->only(
                'campaign.budget.cost',
                ['required', 'numeric', 'min', 'max']
            ),
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    protected function getBudgetValidationMessages(): array
    {
        return [
            'cost.required' => __('validation.custom.campaign.cost.required'),
            'cost.min'      => __('validation.custom.campaign.cost.min'),
            'cost.max'      => __('validation.custom.campaign.cost.max', [
                'max' => NumberFormatHelper::floatCurrency(
                    $this->validationRules->get('campaign.budget.cost.max')
                ),
            ]),
        ];
    }

    /**
     * @return array
     */
    protected function getImpressionsValidation(): array
    {
        return [
            'impressions' => $this->validationRules->only(
                'campaign.budget.impressions',
                ['required', 'numeric', 'min', 'max']
            ),
        ];
    }
}
