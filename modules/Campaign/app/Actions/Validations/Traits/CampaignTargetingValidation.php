<?php

namespace Modules\Campaign\Actions\Validations\Traits;

use App\Rules\CampaignTargeting;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Audience;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Zipcode;

/**
 * @property ValidationRules $validationRules
 */
trait CampaignTargetingValidation
{
    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getTargetingsSpecialAdsValidation(Campaignable $campaign): array
    {
        return array_merge(
            $this->getAgeGroupsValidation($campaign),
            $this->getAudiencesValidation($campaign),
            $this->getZipcodesValidation($campaign),
            $this->getLocationValidation($campaign)
        );
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getTargetingsValidation(Campaignable $campaign): array
    {
        return array_merge(
            $this->getAgeGroupsValidation($campaign),
            $this->getAudiencesValidation($campaign),
            $this->getZipcodesValidation($campaign),
            $this->getGenresValidation($campaign),
            $this->getDeviceGroupsValidation($campaign),
            $this->getLocationValidation($campaign)
        );
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getAudiencesValidation(Campaignable $campaign): array
    {
        return [
            'audiences' => $this->validationRules->only(
                'targeting.audience.ids',
                ['array'],
                [new CampaignTargeting(Audience::TYPE_NAME, $campaign->getOrigin())]
            ),
        ];
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getZipcodesValidation(Campaignable $campaign): array
    {
        return [
            'zipcodes' => $this->validationRules->only(
                'targeting.zipcode.ids',
                ['array'],
                [
                    new CampaignTargeting(
                        Zipcode::TYPE_NAME,
                        $campaign->getOrigin(),
                        $campaign->locations()->pluck('id')->toArray()
                    ),
                ]
            ),
        ];
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getLocationValidation(Campaignable $campaign): array
    {
        return [
            'location' => $this->validationRules->only(
                'targeting.location.ids',
                ['array'],
                [
                    new CampaignTargeting(
                        Location::TYPE_NAME,
                        $campaign->getOrigin(),
                        $campaign->zipcodes()->pluck('id')->toArray()
                    )
                ]
            ),
        ];
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getGenresValidation(Campaignable $campaign): array
    {
        return [
            'genres' => $this->validationRules->only(
                'targeting.genre.ids',
                ['array'],
                [new CampaignTargeting(Genre::TYPE_NAME, $campaign->getOrigin())]
            ),
        ];
    }

    /**
     * Get validation for age groups step.
     *
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getAgeGroupsValidation(Campaignable $campaign): array
    {
        return [
            'age_groups' => $this->validationRules->only(
                'targeting.age_group',
                ['required', 'min', 'array'],
                [new CampaignTargeting(AgeGroup::TYPE_NAME, $campaign->getOrigin())]
            ),
        ];
    }

    /**
     * Get validation for device groups step.
     *
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getDeviceGroupsValidation(Campaignable $campaign): array
    {
        return [
            'device_groups' => $this->validationRules->only(
                'targeting.device_group.groups',
                ['required', 'min', 'array'],
                [new CampaignTargeting(DeviceGroup::TYPE_NAME, $campaign->getOrigin())]
            ),
        ];
    }

    /**
     * @return array
     */
    protected function getAgeGroupsValidationMessages(): array
    {
        return [
            'age_groups.required' => __('targeting::messages.empty_age_groups'),
        ];
    }

    /**
     * @return array
     */
    protected function getDeviceGroupsValidationMessages(): array
    {
        return [
            'device_groups.required' => __('targeting::messages.empty_device_groups'),
        ];
    }
}
