<?php

namespace Modules\Campaign\Actions\Validations\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\User\Models\User;

/**
 * @property ValidationRules $validationRules
 */
trait CampaignDetailsValidation
{
    /**
     * Get validation for name step.
     *
     * @return array
     */
    protected function getNameValidation(): array
    {
        return [
            'name' => $this->validationRules->only(
                'campaign.details.name',
                ['required', 'string', 'max', 'min', 'regex']
            ),
        ];
    }

    /**
     * Get validation for schedule step.
     *
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getScheduleValidation(Campaignable $campaign): array
    {
        $this->addDateEndValidationRule($campaign);

        return [
            'date.start'  => $this->validationRules->only(
                'campaign.details.date.start',
                $this->getStartDateRules($campaign)
            ),
            'date.end'    => $this->validationRules->only(
                'campaign.details.date.end',
                $this->getEndDateRules($campaign)
            ),
            'timezone_id' => $this->validationRules->only('campaign.details.timezone_id', ['required', 'exists']),
        ];
    }

    /**
     * @param Campaignable $campaign
     */
    private function addDateEndValidationRule(Campaignable $campaign): void
    {
        if (!$campaign->date_start || !$campaign->date_end) {
            return;
        }

        $dateFormat = config('date.format.php');
        $timestamp = strtotime(sprintf(
            '%s +%d %s',
            $campaign->date_start->format($dateFormat),
            config('campaign.wizard.date.max_duration.value'),
            config('campaign.wizard.date.max_duration.type')
        ));

        $this->validationRules->set('campaign.details.date.end.before', date($dateFormat, $timestamp));
    }

    /**
     * @param Campaignable $campaign
     *
     * @return string[]
     */
    private function getStartDateRules(Campaignable $campaign): array
    {
        $startDateRules = ['required', 'date', 'date_format'];
        $liveModeRules = $this->getLiveModeRules($campaign);

        if (empty($liveModeRules)) {
            $startDateRules = array_merge($startDateRules, ['after', 'before']);
        }

        return array_merge($startDateRules, $liveModeRules);
    }

    /**
     * @param Campaignable $campaign
     *
     * @return string[]
     */
    private function getEndDateRules(Campaignable $campaign): array
    {
        $endDateRules = ['required', 'date', 'date_format'];

        if ($campaign->date_start) {
            $endDateRules[] = 'before';
        }

        return $endDateRules;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getLiveModeRules(Campaignable $campaign): array
    {
        /** @var User $user */
        $user = Auth::user();
        $rules = [];

        if (!$user->can('campaign.editStartDate', $campaign->getOrigin())) {
            $this->validationRules->set(
                'campaign.details.date.start.date_equals',
                $campaign->getOrigin()->date_start
            );
            array_push($rules, 'date_equals');
        }

        return $rules;
    }

    /**
     * @return array
     */
    protected function getScheduleValidationMessages(): array
    {
        return [
            'date.start.after' => __('campaign::messages.validation.dates.invalid'),
        ];
    }
}
