<?php

namespace Modules\Campaign\Actions\Validations\Helpers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Modules\Campaign\Actions\Traits\CampaignTargetingCollect;
use Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\TargetingValue;

class ValidateCampaignVisibleTargetingHelper
{
    use CampaignTargetingCollect;

    /**
     * @param Campaignable $campaign
     *
     * @return void
     * @throws CampaignUsesNonVisibleTargetingException
     */
    public function validateVisibleTargetings(Campaignable $campaign): void
    {
        // Check if the original campaign has contained invisible targeting values.
        // Need to allow manipulation with chosen invisible values in the live edit flow
        if ($campaign->getOrigin()->isLiveEdit() &&
            count($this->collectCampaignTargetings($campaign->getOrigin(), false))) {
            return;
        }

        $nonVisible = $this->collectCampaignTargetings($campaign, false);

        if (count($nonVisible)) {
            throw new CampaignUsesNonVisibleTargetingException($campaign, $this->formMessage($nonVisible));
        }
    }

    /**
     * @param array|TargetingValue[][] $nonVisible
     *
     * @return string
     */
    protected function formMessage(array $nonVisible): string
    {
        $intro = e(trans('targeting::messages.targetings_no_available'));
        $message = "<p class='message-intro'>$intro</p>";
        foreach ($nonVisible as $targetingGroupName => $targetingCollection) {
            $targeting = e(trans('targeting::labels.labels.' . $targetingGroupName));
            $message .= "<b>$targeting:</b><ul>";
            if ($targetingGroupName === 'targetingAgeGroups') {
                $this->invisibleAgeGroupMessage($targetingCollection, $message);
                $message .= '</ul>';
                continue;
            }

            foreach ($targetingCollection as $targeting) {
                $name = e($targeting->name);
                $message .= "<li>$name</li>";
            }
            $message .= '</ul>';
        }

        return $message;
    }

    /**
     * Get invisible age groups full name
     *
     * @param AgeGroup[]|Collection $ageGroups
     * @param string                $message
     */
    private function invisibleAgeGroupMessage(Collection $ageGroups, string &$message)
    {
        foreach ($ageGroups as $ageGroup) {
            $genderName = e(Str::ucfirst($ageGroup->gender->name));
            $ageGroupName = e($ageGroup->name);
            $message .= "<li>$genderName $ageGroupName</li>";
        }
    }
}
