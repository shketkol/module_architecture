<?php

namespace Modules\Campaign\Actions\Validations;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator as FacadeValidator;
use Modules\Campaign\Actions\Validations\Abstracts\ValidateCampaignActionAbstract;
use Modules\Campaign\Exceptions\CampaignUsesNonVisibleTargetingException;
use Modules\Campaign\Models\Contracts\Campaignable;

final class ValidateDynamicInventoryCheckAction extends ValidateCampaignActionAbstract
{
    /**
     * Validation fields mapper
     */
    private const VALIDATION_MAP = [
        'date' => 'schedule',
        'cost' => 'budget',
    ];

    /**
     * @param Campaignable $campaign
     *
     * @return array
     * @throws CampaignUsesNonVisibleTargetingException
     */
    public function handle(Campaignable $campaign): array
    {
        $validator = FacadeValidator::make(
            $this->getCampaignDataToValidate($campaign),
            array_merge(
                $this->getScheduleValidation($campaign),
                $this->getBudgetValidation($campaign),
            ),
            $this->getMessages()
        );

        if ($validator->fails()) {
            return $this->formatDynamicInventoryValidationData($validator->errors()->toArray());
        }

        $this->validateCampaignVisibleTargetingHelper->validateVisibleTargetings($campaign);

        return [];
    }

    /**
     * @param array $errors
     *
     * @return array
     */
    private function formatDynamicInventoryValidationData(array $errors): array
    {
        return collect($errors)->mapWithKeys(function ($item, $mappedKey): array {
            $parts = explode('.', $mappedKey);
            $shortKey = count($parts) ? Arr::first($parts) : $mappedKey;

            return Arr::get(self::VALIDATION_MAP, $shortKey)
                ? [self::VALIDATION_MAP[$shortKey] => Arr::first($item)]
                : [];
        })->toArray();
    }
}
