<?php

namespace Modules\Campaign\Actions\Validations\Abstracts;

use App\Services\ValidationRulesService\ValidationRules;
use Modules\Campaign\Actions\Validations\Helpers\ValidateCampaignVisibleTargetingHelper;
use Modules\Campaign\Actions\Validations\Traits\CampaignBudgetValidation;
use Modules\Campaign\Actions\Validations\Traits\CampaignDetailsValidation;
use Modules\Campaign\Actions\Validations\Traits\CampaignTargetingValidation;
use Modules\Campaign\Models\Contracts\Campaignable;

abstract class ValidateCampaignActionAbstract
{
    use CampaignDetailsValidation,
        CampaignBudgetValidation,
        CampaignTargetingValidation;

    /**
     * @var ValidationRules
     */
    protected ValidationRules $validationRules;

    /**
     * @var ValidateCampaignVisibleTargetingHelper
     */
    protected ValidateCampaignVisibleTargetingHelper $validateCampaignVisibleTargetingHelper;

    /**
     * @param ValidationRules                        $validationRules
     * @param ValidateCampaignVisibleTargetingHelper $validateCampaignVisibleTargetingHelper
     */
    public function __construct(
        ValidationRules $validationRules,
        ValidateCampaignVisibleTargetingHelper $validateCampaignVisibleTargetingHelper
    ) {
        $this->validationRules = $validationRules;
        $this->validateCampaignVisibleTargetingHelper = $validateCampaignVisibleTargetingHelper;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return mixed
     */
    abstract public function handle(Campaignable $campaign);

    /**
     * @param Campaignable $campaign
     *
     * @return array
     */
    protected function getCampaignDataToValidate(Campaignable $campaign): array
    {
        $dateFormat = config('date.format.php');
        return [
            'name'          => $campaign->name,
            'timezone_id'   => $campaign->timezone_id,
            'date'          => [
                'start' => $campaign->date_start ? $campaign->date_start->format($dateFormat) : null,
                'end'   => $campaign->date_end ? $campaign->date_end->format($dateFormat) : null,
            ],
            'cost'          => $campaign->budget,
            'impressions'   => $campaign->impressions,
            'age_groups'    => $campaign->targetingAgeGroups->pluck('id')->toArray(),
            'audiences'     => $campaign->audiences->pluck('id')->toArray(),
            'zipcodes'      => $campaign->zipcodes->pluck('id')->toArray(),
            'genres'        => $campaign->genres->pluck('id')->toArray(),
            'device_groups' => $campaign->deviceGroups->pluck('id')->toArray(),
            'location'     => $campaign->locations->pluck('id')->toArray(),
        ];
    }

    /**
     * Get custom validation messages.
     *
     * @return array
     */
    protected function getMessages(): array
    {
        return array_merge(
            $this->getScheduleValidationMessages(),
            $this->getBudgetValidationMessages(),
            $this->getAgeGroupsValidationMessages(),
            $this->getDeviceGroupsValidationMessages()
        );
    }
}
