<?php

namespace Modules\Campaign\Actions\Validations;

use Illuminate\Support\Facades\Validator;
use Modules\Campaign\Actions\Validations\Abstracts\ValidateCampaignActionAbstract;
use Modules\Campaign\Models\Contracts\Campaignable;

final class ValidateCampaignTargetingsAction extends ValidateCampaignActionAbstract
{
    /**
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function handle(Campaignable $campaign): bool
    {
        $validator = Validator::make(
            $this->getCampaignDataToValidate($campaign),
            $this->getTargetingsValidation($campaign)
        );

        return !$validator->fails();
    }

    /**
     * @param Campaignable $campaign
     *
     * @return bool
     */
    public function validateSpecialAdTargetingAction(Campaignable $campaign): bool
    {
        $validator = Validator::make(
            $this->getCampaignDataToValidate($campaign),
            $this->getTargetingsSpecialAdsValidation($campaign)
        );

        return !$validator->fails();
    }
}
