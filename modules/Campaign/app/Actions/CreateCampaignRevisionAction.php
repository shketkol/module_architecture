<?php

namespace Modules\Campaign\Actions;

use Illuminate\Database\DatabaseManager;
use Illuminate\Log\Logger;
use Modules\Campaign\Exceptions\CampaignRevisionNotCreatedException;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Repositories\CampaignRevisionRepository;

class CreateCampaignRevisionAction
{
    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var DatabaseManager
     */
    private DatabaseManager $databaseManager;

    /**
     * @var CampaignRevisionRepository
     */
    private CampaignRevisionRepository $repository;

    /**
     * @param Logger                     $log
     * @param DatabaseManager            $databaseManager
     * @param CampaignRevisionRepository $repository
     */
    public function __construct(Logger $log, DatabaseManager $databaseManager, CampaignRevisionRepository $repository)
    {
        $this->log = $log;
        $this->databaseManager = $databaseManager;
        $this->repository = $repository;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Campaignable
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(Campaignable $campaign): Campaignable
    {
        $this->log->info('[Campaign Revision] Started creating campaign revisions.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        if (!$campaign->canHaveRevisions()) {
            $this->log->info('[Campaign Revision] Ignored creating campaign revisions. Campaign is in wrong state.', [
                'campaign_id'        => $campaign->id,
                'campaign_type'      => get_class($campaign),
                'previous_status_id' => $campaign->previous_status_id,
                'status_id'          => $campaign->status_id,
            ]);

            return $campaign;
        }

        $lastRevision = $campaign->lastRevision();
        if (!is_null($lastRevision) && $lastRevision->isDraft()) {
            $this->log->info('[Campaign Revision] Skipped creating campaign revisions. Campaign already has it.', [
                'campaign_id'       => $campaign->id,
                'campaign_type'     => get_class($campaign),
                'draft_revision_id' => $lastRevision->id,
            ]);

            return $lastRevision;
        }

        try {
            // create live revision only for the first edit
            if (is_null($lastRevision)) {
                $liveRevision = $this->createLiveRevision($campaign);
            }

            $draftRevision = $this->createDraftRevision($campaign);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();

            $this->log->info('[Campaign Revision] Failed creating campaign revisions.', [
                'campaign_id'   => $campaign->id,
                'campaign_type' => get_class($campaign),
                'reason'        => $exception->getMessage(),
            ]);

            throw CampaignRevisionNotCreatedException::createFrom($exception);
        }

        $this->databaseManager->commit();

        $this->log->info('[Campaign Revision] Finished creating campaign revisions.', [
            'campaign_id'       => $campaign->id,
            'campaign_type'     => get_class($campaign),
            'live_revision_id'  => isset($liveRevision) ? $liveRevision->id : null,
            'draft_revision_id' => $draftRevision->id,
        ]);

        return $draftRevision;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Campaignable
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function createLiveRevision(Campaignable $campaign): Campaignable
    {
        $revision = $this->repository->createLiveRevision($campaign);

        $this->log->info('[Campaign Revision] Created live campaign revision.', [
            'campaign_id'   => $campaign->getKey(),
            'campaign_type' => get_class($campaign),
            'revision_id'   => $revision->id,
        ]);

        return $revision;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Campaignable
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function createDraftRevision(Campaignable $campaign): Campaignable
    {
        $revision = $this->repository->createDraftRevision($campaign);

        $this->log->info('[Campaign Revision] Created draft campaign revision.', [
            'campaign_id'   => $campaign->getKey(),
            'campaign_type' => get_class($campaign),
            'revision_id'   => $revision->id,
        ]);

        return $revision;
    }
}
