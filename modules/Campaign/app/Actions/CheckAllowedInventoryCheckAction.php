<?php

namespace Modules\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Log\Logger;
use Illuminate\Support\Collection;
use Modules\Campaign\Actions\Traits\Targetings;
use Modules\Campaign\Helpers\DateHelper;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Targeting\Models\TargetingValue;

class CheckAllowedInventoryCheckAction
{
    use Targetings;

    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * @var InventoryCheckHistoryRepository
     */
    private InventoryCheckHistoryRepository $repository;

    /**
     * @var GetInventoryCheckConclusionAction
     */
    private GetInventoryCheckConclusionAction $conclusion;

    /**
     * @param Logger                            $log
     * @param InventoryCheckHistoryRepository   $repository
     * @param GetInventoryCheckConclusionAction $conclusion
     */
    public function __construct(
        Logger $log,
        InventoryCheckHistoryRepository $repository,
        GetInventoryCheckConclusionAction $conclusion
    ) {
        $this->log = $log;
        $this->repository = $repository;
        $this->conclusion = $conclusion;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function handle(Campaignable $campaign): array
    {
        $this->log->info('[Inventory check permission] Started checking.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        $history = $this->repository->findLast($campaign);

        if (is_null($history)) {
            return $this->getAllowResponse();
        }

        if ($this->hasBudgetChanged($campaign, $history)) {
            return $this->getAllowResponse();
        }

        if ($this->haveDatesChanged($campaign, $history)) {
            return $this->getAllowResponse();
        }

        if ($this->haveTargetingsChanged($campaign, $history)) {
            return $this->getAllowResponse();
        }

        if ($this->hasTimePassedFromLastUpdate($history)) {
            return $this->getAllowResponse();
        }

        $this->log->info('[Inventory check permission] Finished checking.', [
            'campaign_id' => $campaign->id,
            'history_id'  => $history->id,
        ]);

        return $this->getDenyResponse($history);
    }

    /**
     * In case advertiser returns to the campaign after some time
     * And in this time HAAPI inventory could also change
     *
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function hasTimePassedFromLastUpdate(InventoryCheckHistory $history): bool
    {
        $minutes = config('campaign.inventory_expiration');
        $now = Carbon::now()->subMinutes($minutes);

        return $now->gt($history->created_at);
    }

    /**
     * @param Campaignable          $campaign
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function hasBudgetChanged(Campaignable $campaign, InventoryCheckHistory $history): bool
    {
        return $campaign->budget !== $history->campaign_budget;
    }

    /**
     * @param Campaignable          $campaign
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function haveDatesChanged(Campaignable $campaign, InventoryCheckHistory $history): bool
    {
        return !DateHelper::areDatesEqual($campaign->date_start, $history->date_start) ||
           !DateHelper::areDatesEqual($campaign->date_end, $history->date_end);
    }

    /**
     * @param Campaignable          $campaign
     * @param InventoryCheckHistory $history
     *
     * @return bool
     */
    private function haveTargetingsChanged(Campaignable $campaign, InventoryCheckHistory $history): bool
    {
        $historyTargetings = $this->getTargetingsFromHistory($history->targetings);
        $campaignTargetings = $this->getTargetingsFromCampaign($campaign);
        $diffCampaign = $this->getCampaignHistoryTargetingsDiff($campaignTargetings, $historyTargetings);
        $diffHistory = $this->getHistoryCampaignTargetingsDiff($historyTargetings, $campaignTargetings);

        return $diffCampaign->isNotEmpty() || $diffHistory->isNotEmpty();
    }

    /**
     * @param Collection|TargetingValue[] $campaignTargetings
     * @param Collection|TargetingValue[] $historyTargetings
     *
     * @return Collection
     */
    private function getCampaignHistoryTargetingsDiff(
        Collection $campaignTargetings,
        Collection $historyTargetings
    ): Collection {
        return $campaignTargetings->filter(
            function (TargetingValue $campaignTargeting) use ($historyTargetings): bool {
                return is_null($historyTargetings->first(
                    function (TargetingValue $historyTargeting) use ($campaignTargeting): bool {
                        return $this->isSameTargetable($campaignTargeting, $historyTargeting);
                    }
                ));
            }
        );
    }

    /**
     * @param Collection|TargetingValue[] $campaignTargetings
     * @param Collection|TargetingValue[] $historyTargetings
     *
     * @return Collection
     */
    private function getHistoryCampaignTargetingsDiff(
        Collection $historyTargetings,
        Collection $campaignTargetings
    ): Collection {
        return $historyTargetings->filter(
            function (TargetingValue $historyTargeting) use ($campaignTargetings): bool {
                return is_null($campaignTargetings->first(
                    function (TargetingValue $campaignTargeting) use ($historyTargeting): bool {
                        return $this->isSameTargetable($campaignTargeting, $historyTargeting);
                    }
                ));
            }
        );
    }

    /**
     * @param TargetingValue $campaignTargeting
     * @param TargetingValue $historyTargeting
     *
     * @return bool
     */
    private function isSameTargetable(TargetingValue $campaignTargeting, TargetingValue $historyTargeting): bool
    {
        return get_class($campaignTargeting) === get_class($historyTargeting)
            && $campaignTargeting->id === $historyTargeting->id
            && $campaignTargeting->getIsExcluded() === (bool)$historyTargeting->excluded; // dynamic field
    }

    /**
     * @return array
     */
    private function getAllowResponse(): array
    {
        return [
            'allowed'  => true,
            'response' => [],
        ];
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return array
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getDenyResponse(InventoryCheckHistory $history): array
    {
        $conclusion = $this->conclusion->handle($history, Carbon::now());
        $default = ['message' => __('campaign::messages.dynamic_inventory_check.error.denied')];
        $response = array_merge($default, $conclusion);

        return [
            'allowed'  => false,
            'response' => $response,
        ];
    }
}
