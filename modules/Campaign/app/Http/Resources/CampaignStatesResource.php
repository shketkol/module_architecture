<?php

namespace Modules\Campaign\Http\Resources;

use App\Helpers\CurrentUser;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Campaign\Models\CampaignStatus;

class CampaignStatesResource extends JsonResource
{
    use CurrentUser;

    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $user = $this->getCurrentUser();
        $origin = $this->resource->getOrigin();

        return [
            'can_pause'                 => $user->can('campaign.pause', $this->resource)
                && $origin->canApply(CampaignStatus::PAUSED),
            'can_cancel'                => $user->can('campaign.cancel', $this->resource)
                && $origin->canApply(CampaignStatus::CANCELED),
            'can_resume'                => $user->can('campaign.resume', $this->resource)
                && ($origin->getPreviousStatus() ? $origin->canApply($origin->getPreviousStatus()) : false),
            'can_edit'                  => $user->can('campaign.update', $origin),
            'can_delete'                => $user->can('campaign.delete', $origin),
            'can_duplicate'             => $user->can('campaign.clone', $origin),
            'can_open_advertiser_page'  => $user->can('advertiser.view', $origin->user),
            'can_manage_creative'       => $user->can('campaign.manageCampaignCreative', $this->resource),
            'can_only_replace_creative' => $user->can('campaign.replaceCampaignCreative', $this->resource),
            'can_download_report'       => $user->can('campaign.downloadReport', $origin),
            'wizard'                    => [
                'can_edit_start_date'    => $user->can('campaign.editStartDate', $this->resource),
                'can_edit_end_date'      => $user->can('campaign.editEndDate', $this->resource),
                'can_edit_budget'        => $user->can('campaign.editBudget', $this->resource),
                'can_validate_promocode' => $user->can('campaign.validatePromocode', $this->resource),
                'targetings'             => [
                    'can_edit_age_groups' => $user->can('campaign.editAgeTargetings', $this->resource),
                    'can_edit_audiences'  => $user->can('campaign.editAudienceTargetings', $this->resource),
                    'can_edit_locations'  => $user->can('campaign.editLocationTargetings', $this->resource),
                    'can_edit_zipcodes'   => $user->can('campaign.editZipTargetings', $this->resource),
                    'can_edit_genres'     => $user->can('campaign.editGenreTargetings', $this->resource),
                    'can_edit_platforms'  => $user->can('campaign.editPlatformTargetings', $this->resource)
                ]
            ],
        ];
    }
}
