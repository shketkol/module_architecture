<?php

namespace Modules\Campaign\Http\Resources;

use App\Helpers\Math\CalculationHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class CampaignGenresMetricsResource extends JsonResource
{
    /**
     * Max items to return as general impression genres.
     */
    const MAX_ITEMS = 4;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'generalImpressionsByGenre' => $this->getGeneralImpressionsByTypes(
                Arr::get($this->resource, 'lineItemImpressionsByType.0.impressions', [])
            ),
        ];
    }

    /**
     * @param array $impressions
     *
     * @return array
     */
    private function getGeneralImpressionsByTypes(array $impressions): array
    {
        $generalImpressions = array_slice($impressions, 0, self::MAX_ITEMS);

        $otherImpressions = $this->getOtherImpressions($impressions);

        if (count($otherImpressions)) {
            $generalImpressions[] = $otherImpressions;
        }

        return CalculationHelper::largestReminderPercentage(
            $generalImpressions,
            ['precision' => 2, 'multiplier' => 100]
        );
    }

    /**
     * @param array $impressions
     *
     * @return array
     */
    private function getOtherImpressions(array $impressions): array
    {
        if (count($impressions) <= self::MAX_ITEMS) {
            return [];
        }

        $otherImpressions = array_slice($impressions, self::MAX_ITEMS);

        return [
            'name' => __('campaign::labels.performance.charts.genres.other'),
            'impressions' => array_sum(Arr::pluck($otherImpressions, 'impressions')),
            'percentage' => array_sum(Arr::pluck($otherImpressions, 'percentage')),
        ];
    }
}
