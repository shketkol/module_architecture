<?php

namespace Modules\Campaign\Http\Resources\traits;

use Carbon\Carbon;
use Modules\Campaign\Helpers\DateHelper;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\TargetingValue;

trait Dates
{
    /**
     * @param Campaign    $campaign
     * @param Carbon|null $new
     * @param Carbon|null $old
     *
     * @return array[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function getComparedDates(Campaign $campaign, ?Carbon $new, ?Carbon $old): array
    {
        if (!$campaign->isLiveEdit()) {
            return [[
                'value' => $old,
                'type'  => TargetingValue::TYPE_NEW,
            ]];
        }

        if (DateHelper::areDatesEqual($new, $old)) {
            return [[
                'value' => $new,
                'type'  => TargetingValue::TYPE_CURRENT,
            ]];
        }

        return [
            [
                'value' => $new,
                'type'  => TargetingValue::TYPE_NEW,
            ],
            [
                'value' => $old,
                'type'  => TargetingValue::TYPE_DELETED,
            ],
        ];
    }
}
