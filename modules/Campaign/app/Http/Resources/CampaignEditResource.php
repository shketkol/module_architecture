<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Support\Arr;
use Modules\Campaign\Models\Contracts\Campaignable;

class CampaignEditResource extends CampaignResource
{
    /**
     * @var array
     */
    private array $messages;

    /**
     * @param Campaignable $resource
     * @param array        $messages
     */
    public function __construct(Campaignable $resource, array $messages = [])
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->messages = array_filter($messages);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $data = parent::toArray($request);

        Arr::set($data, 'messages', $this->messages);
        Arr::set($data, 'step', $this->getStep());

        return $data;
    }
}
