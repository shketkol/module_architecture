<?php

namespace Modules\Campaign\Http\Resources;

use App\Helpers\CurrentUser;
use App\Helpers\DateFormatHelper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\CheckAllowedInventoryCheckAction;
use Modules\Campaign\Actions\Traits\CampaignTargetingCollect;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;
use Modules\Campaign\Http\Resources\traits\Dates;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignRevision;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Creative\DataTable\Repositories\Contracts\CreativesDataTableRepository;
use Modules\Creative\Http\Resources\CreativeResource;
use Modules\Promocode\Http\Resources\PromocodeResource;
use Modules\Targeting\Http\Resources\TargetingResource;
use Modules\Targeting\Http\Resources\Traits\Targetings;

/**
 * @mixin Campaign|CampaignRevision
 * @property Campaignable $resource
 */
class CampaignResource extends JsonResource
{
    use CurrentUser,
        CampaignTargetingCollect,
        Dates,
        Targetings;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function toArray($request): array
    {
        $targetsResource = new TargetingResource($this->resource);

        return [
            'id'                             => $this->getOrigin()->id,
            'name'                           => $this->name,
            'company_name'                   => $this->user->company_name,
            'advertiser_id'                  => $this->user->id,
            'date_start'                     => $this->getDateStart(),
            'date_end'                       => $this->getDateEnd(),
            'timezone_id'                    => $this->timezone_id,
            'timezone'                       => $this->timezone->code,
            'discounted_cost'                => $this->discounted_cost,
            'discount_money'                 => $this->getDiscountMoney(),
            'min_required_budget'            => $this->getMinBudget(),
            'cost'                           => $this->budget,
            'budget'                         => $this->budget,
            'impressions'                    => $this->impressions,
            'min_daily_impressions'          => config('campaign.min_daily_impressions'),
            'status_id'                      => $this->getOrigin()->status_id,
            'status'                         => $this->getOrigin()->status->name,
            'status_change_actor'            => $this->status_change_actor,
            'status_changed_at'              => $this->getStatusChangedAt(),
            'cpm'                            => $this->cpm,
            'discounted_cpm'                 => $this->discounted_cpm,
            'order_id'                       => $this->order_id,
            'creative'                       => $this->getCreativeResource(),
            'countUploadedAds'               => $this->getCountUploadedCreatives(),
            'targeting'                      => $targetsResource,
            'states'                         => new CampaignStatesResource($this->resource),
            'promocode'                      => new PromocodeResource($this->getOrigin()->promocode),
            'payment_method_id'              => $this->payment_method_id,
            'live_edit_mode'                 => $this->isLiveEditable(),
            'exist_draft_review'             => $this->isLiveEdit(),
            'revisions'                      => $this->getRevisions(),
            'successful_inventory_timestamp' => $this->getInventorySuccessTimestamp($targetsResource),
        ];
    }

    /**
     * @param TargetingResource $targetsResource
     *
     * @return Carbon|null
     * @throws \Exception
     */
    protected function getInventorySuccessTimestamp(TargetingResource $targetsResource): ?Carbon
    {
        /** @var InventoryCheckHistory $history */
        $history = app(InventoryCheckHistoryRepository::class)->findLast($this->resource);

        if (is_null($history) || !$history->isSuccessful()) {
            return null;
        }

        /** @var array $result */
        $result = app(CheckAllowedInventoryCheckAction::class)->handle($this->resource);

        if (Arr::get($result, 'allowed')) {
            return null;
        }

        if ($targetsResource->hasNullableRelation()) {
            return null;
        }

        return $this->budget && $this->date_start && $this->date_end ? $history->created_at : null;
    }

    /**
     * @return array
     */
    private function getRevisions(): array
    {
        if (!$this->resource->getOrigin()->existRevisions()) {
            return [
                'items'   => [],
                'current' => [],
            ];
        }

        $currentRevision = $this->getCurrentRevision();

        return [
            'items'   => RevisionsResource::collection(
                $this->getOrigin()
                    ->revisions()
                    ->orderBy('id', 'DESC')
                    ->get()
            ),
            'current' => [
                'id'     => $currentRevision->id,
                'status' => $currentRevision->status,
            ],
        ];
    }

    /**
     * @return Campaignable
     */
    private function getCurrentRevision(): Campaignable
    {
        if ($this->resource->isDraft()) {
            return $this->resource;
        }

        return $this->resource->getOrigin()->lastNonDraftRevision();
    }

    /**
     * The date is hardcoded (must be displayed) to ET at FE
     *
     * @return string|null
     */
    private function getStatusChangedAt(): ?string
    {
        return $this->status_changed_at
            ? DateFormatHelper::toTimezone($this->status_changed_at, config('date.default_timezone_full_code'))
            : null;
    }

    /**
     * @return array[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getDateStart(): array
    {
        return $this->getDateDiff($this->date_start, $this->getOrigin()->date_start);
    }

    /**
     * @return array[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getDateEnd(): array
    {
        return $this->getDateDiff($this->date_end, $this->getOrigin()->date_end);
    }

    /**
     * @param Carbon|null $revisionDate
     * @param Carbon|null $campaignDate
     *
     * @return array[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getDateDiff(?Carbon $revisionDate, ?Carbon $campaignDate): array
    {
        return $this->getComparedDates($this->getOrigin(), $revisionDate, $campaignDate);
    }

    /**
     * @return float
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getMinBudget(): float
    {
        return $this->isLiveEdit() ? $this->min_edit_budget : config('campaign.wizard.budget.min');
    }

    /**
     * @return int
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getCountUploadedCreatives(): int
    {
        /** @var CreativesDataTableRepository $repository */
        $repository = app(CreativesDataTableRepository::class);
        $user = $this->getCurrentUser();

        return $repository->getUserCreativesCount($user);
    }

    /**
     * @return CreativeResource|null
     */
    private function getCreativeResource(): ?CreativeResource
    {
        $creative = $this->getActiveCreative();
        return $creative ? new CreativeResource($creative) : null;
    }
}
