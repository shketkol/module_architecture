<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Support\Arr;

class CampaignDeleteResource extends CampaignResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'message' => Arr::get($this->resource, 'message'),
        ];
    }
}
