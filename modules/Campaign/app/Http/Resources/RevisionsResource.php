<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Campaign\Models\CampaignRevision;

/**
 * @mixin CampaignRevision
 * @property CampaignRevision $resource
 */
class RevisionsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'     => $this->id,
            'status' => $this->getStatus(),
            'date'   => $this->isDraft() ?
                $this->updated_at->format(config('date.format.php')) :
                $this->submitted_at->format(config('date.format.php'))
        ];
    }

    /**
     * @return \Modules\Campaign\Models\CampaignRevisionStatus|\Modules\Campaign\Models\CampaignStatus
     */
    public function getStatus(): Model
    {
        if (!$this->isLive()) {
            return $this->status;
        }

        return $this->getOrigin()->isProcessing()
            ? $this->getOrigin()->previousStatus
            : $this->getOrigin()->status;
    }
}
