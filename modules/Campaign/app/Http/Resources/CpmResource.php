<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CpmResource
 * @package Modules\Campaign\Http\Resources
 */
class CpmResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'min_daily_impressions' => config('campaign.min_daily_impressions'),
            'cpm'                   => $this['cpm'],
        ];
    }
}
