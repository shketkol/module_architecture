<?php

namespace Modules\Campaign\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryCheckResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return $this->resource;
    }
}
