<?php

namespace Modules\Campaign\Http\Resources;

class CampaignsListResource extends CampaignResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'label' => $this->resource->name,
            'value' => $this->resource->id,
        ];
    }
}
