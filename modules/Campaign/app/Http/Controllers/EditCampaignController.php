<?php

namespace Modules\Campaign\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;

class EditCampaignController extends Controller
{
    /**
     * @param Campaignable                    $campaign
     * @param InventoryCheckHistoryRepository $repository
     *
     * @return View
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        Campaignable $campaign,
        InventoryCheckHistoryRepository $repository
    ): View {
        return view('campaign::edit', [
            'id'              => $campaign->getOrigin()->id,
            'forceNavigateTo' => $this->getStep($campaign, $repository),
        ]);
    }

    /**
     * Force navigate to the budget step to cover migration scenario described in
     * modules/Campaign/database/migrations/2021_04_16_083005_update_step_id_in_draft_campaigns.php
     *
     * @param Campaignable                    $campaign
     * @param InventoryCheckHistoryRepository $repository
     *
     * @return string|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getStep(Campaignable $campaign, InventoryCheckHistoryRepository $repository): ?string
    {
        $forceStepNavigation = null;

        if ($campaign->isLiveEdit()) {
            return $forceStepNavigation;
        }

        if ($campaign->step_id > CampaignStep::ID_SCHEDULE &&
            $campaign->step_id < CampaignStep::ID_CREATIVE &&
            !$repository->findLast($campaign)
        ) {
            $forceStepNavigation = CampaignStep::BUDGET;
        }

        return $forceStepNavigation;
    }
}
