<?php

namespace Modules\Campaign\Http\Controllers;

class IndexController
{
    public function __invoke()
    {
        return view('campaign::index');
    }
}
