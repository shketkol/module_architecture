<?php

namespace Modules\Campaign\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class CreateCampaignController
 * @package Modules\Campaign\Http\Controllers
 */
class CreateCampaignController extends Controller
{
    /**
     * Display create wizard page.
     *
     * @return View
     */
    public function __invoke(): View
    {
        return view('campaign::create');
    }
}
