<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\Genre\SearchGenresAction;
use Modules\Targeting\Http\Requests\SearchRequest;
use Modules\Targeting\Http\Resources\GenreResource;

class SearchGenresController extends Controller
{
    /**
     * @param Campaignable       $campaign
     * @param SearchGenresAction $action
     * @param SearchRequest      $request
     *
     * @return AnonymousResourceCollection
     * @throws \Modules\Targeting\Exceptions\ClassNotMappedException
     */
    public function __invoke(
        Campaignable $campaign,
        SearchGenresAction $action,
        SearchRequest $request
    ): AnonymousResourceCollection {
        $query = $request->get('query') ?: '';
        return GenreResource::collection($action->handle($campaign, $query)->sortBy('name'));
    }
}
