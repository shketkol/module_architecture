<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\Genre\UpdateCampaignGenresAction;
use Modules\Targeting\Http\Requests\UpdateTargetingGenresRequest;

class UpdateCampaignGenresController extends Controller
{
    /**
     * Save targeting genres and update campaign step.
     *
     * @param Campaignable                 $campaign
     * @param UpdateTargetingGenresRequest $request
     * @param UpdateCampaignGenresAction   $action
     *
     * @return CampaignResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function __invoke(
        Campaignable $campaign,
        UpdateTargetingGenresRequest $request,
        UpdateCampaignGenresAction $action
    ): CampaignResource {
        $action->handle($campaign, $request->toData());
        return new CampaignResource($campaign);
    }
}
