<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\Location\Zipcode\SearchZipcodesAction;
use Modules\Targeting\Http\Requests\SearchZipcodesRequest;
use Modules\Targeting\Http\Resources\ZipcodeResource;

class SearchZipcodesController extends Controller
{
    /**
     * @param Campaignable          $campaign
     * @param SearchZipcodesAction  $action
     * @param SearchZipcodesRequest $request
     *
     * @return AnonymousResourceCollection
     * @throws \Modules\Targeting\Exceptions\ClassNotMappedException
     */
    public function __invoke(
        Campaignable $campaign,
        SearchZipcodesAction $action,
        SearchZipcodesRequest $request
    ): JsonResponse {
        $zipcodes = $request->get('zipcodes') ?: [];
        $collection = $action->handle($campaign, $zipcodes);

        $found = ZipcodeResource::collection($collection)->response($request)->getData()->data;
        $notFound = array_values(array_diff($zipcodes, array_column($found, 'name')));

        return response()->json(['data' => [
            'found'     => $found,
            'not_found' => $notFound,
        ]]);
    }
}
