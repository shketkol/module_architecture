<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use Modules\Campaign\Http\Controllers\Api\UpdateCampaignStepController;
use Modules\Campaign\Models\CampaignStep;

class UpdateCampaignAgesController extends UpdateCampaignStepController
{
    public static int $stepId = CampaignStep::ID_AGES;
}
