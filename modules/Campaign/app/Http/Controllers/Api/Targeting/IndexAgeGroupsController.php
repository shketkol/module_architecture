<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\AgeGroup\IndexAgeGroupsAction;
use Modules\Targeting\Http\Resources\AgesWithGenderResource;

class IndexAgeGroupsController extends Controller
{
    /**
     * @param Campaignable         $campaign
     * @param IndexAgeGroupsAction $action
     *
     * @return AnonymousResourceCollection
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __invoke(
        Campaignable $campaign,
        IndexAgeGroupsAction $action
    ): AnonymousResourceCollection {
        return AgesWithGenderResource::collection($action->handle($campaign));
    }
}
