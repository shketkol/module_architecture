<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\Device\IndexDevicesAction;
use Modules\Targeting\Http\Resources\DeviceGroupResource;

class IndexDevicesController extends Controller
{
    /**
     * @param Campaignable       $campaign
     * @param IndexDevicesAction $action
     *
     * @return AnonymousResourceCollection
     * @throws \Modules\Targeting\Exceptions\ClassNotMappedException
     */
    public function __invoke(
        Campaignable $campaign,
        IndexDevicesAction $action
    ): AnonymousResourceCollection {
        return DeviceGroupResource::collection($action->handle($campaign));
    }
}
