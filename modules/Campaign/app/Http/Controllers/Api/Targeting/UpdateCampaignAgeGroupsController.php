<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use Modules\Campaign\Exceptions\CampaignNotUpdatedException;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Daapi\Http\Controllers\Controller;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignAgeGroupsAction;
use Modules\Targeting\Actions\AgeGroup\UpdateCampaignGendersAction;
use Modules\Targeting\Http\Requests\UpdateTargetingAgeGroupsRequest;

class UpdateCampaignAgeGroupsController extends Controller
{
    /**
     * Save targeting age groups and update campaign step.
     *
     * @param Campaignable                    $campaign
     * @param UpdateTargetingAgeGroupsRequest $request
     * @param UpdateCampaignAgeGroupsAction   $actionAges
     * @param UpdateCampaignGendersAction     $actionGender
     *
     * @return CampaignResource
     * @throws CampaignNotUpdatedException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function __invoke(
        Campaignable $campaign,
        UpdateTargetingAgeGroupsRequest $request,
        UpdateCampaignAgeGroupsAction $actionAges,
        UpdateCampaignGendersAction $actionGender
    ): CampaignResource {
        $actionGender->handle($campaign, $request->genderId);
        $actionAges->handle($campaign, $request->ageGroups);

        return new CampaignResource($campaign);
    }
}
