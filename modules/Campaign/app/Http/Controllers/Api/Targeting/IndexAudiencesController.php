<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Helpers\ArrayHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\Audience\ShowAudienceTiersAction;

class IndexAudiencesController extends Controller
{
    use ArrayHelper;

    /**
     * Get list of all available audiences with tiers.
     *
     * @param Campaignable            $campaign
     * @param ShowAudienceTiersAction $action
     *
     * @return JsonResponse
     */
    public function __invoke(Campaignable $campaign, ShowAudienceTiersAction $action): JsonResponse
    {
        return response()->json(['data' => $this->sortByField($action->handle($campaign), 'name')]);
    }
}
