<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Actions\Device\UpdateCampaignDeviceGroupsAction;
use Modules\Targeting\Http\Requests\UpdateTargetingDeviceGroupsRequest;

class UpdateCampaignDevicesController extends Controller
{
    /**
     * Save targeting devices and update campaign step.
     *
     * @param Campaignable                       $campaign
     * @param UpdateTargetingDeviceGroupsRequest $request
     * @param UpdateCampaignDeviceGroupsAction   $action
     *
     * @return CampaignResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Modules\Targeting\Exceptions\TypeNotFoundException
     */
    public function __invoke(
        Campaignable $campaign,
        UpdateTargetingDeviceGroupsRequest $request,
        UpdateCampaignDeviceGroupsAction $action
    ): CampaignResource {
        $action->handle($campaign, $request->toData());

        return new CampaignResource($campaign);
    }
}
