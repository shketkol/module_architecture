<?php

namespace Modules\Campaign\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\Campaign\Actions\UnattachCampaignNonVisibleTargetingAction;
use Modules\Campaign\Models\Campaign;

class UnattachCampaignNonVisibleTargetingController extends Controller
{
    /**
     * @param Campaign                                  $campaign
     * @param UnattachCampaignNonVisibleTargetingAction $action
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     */
    public function __invoke(Campaign $campaign, UnattachCampaignNonVisibleTargetingAction $action): Response
    {
        $action->handle($campaign);
        return response([
            'message' => trans('targeting::messages.targeting_removed_success')
        ]);
    }
}
