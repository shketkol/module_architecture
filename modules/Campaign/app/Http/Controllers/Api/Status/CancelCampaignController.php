<?php

namespace Modules\Campaign\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\Status\CancelCampaignAction;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\Contracts\Campaignable;

class CancelCampaignController extends Controller
{
    /**
     * Cancel campaign.
     *
     * @param Campaignable         $campaign
     * @param CancelCampaignAction $action
     *
     * @return CampaignResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(Campaignable $campaign, CancelCampaignAction $action): CampaignResource
    {
        $campaign = $action->handle($campaign->getOrigin(), Auth::user());
        return new CampaignResource($campaign);
    }
}
