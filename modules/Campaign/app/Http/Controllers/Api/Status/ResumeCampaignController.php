<?php

namespace Modules\Campaign\Http\Controllers\Api\Status;

use App\Helpers\CurrentUser;
use App\Http\Controllers\Controller;
use Modules\Campaign\Actions\Status\ResumeCampaignAction;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\Contracts\Campaignable;

class ResumeCampaignController extends Controller
{
    use CurrentUser;

    /**
     * Resume campaign.
     *
     * @param Campaign             $campaign
     * @param ResumeCampaignAction $action
     *
     * @return CampaignResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \Throwable
     */
    public function __invoke(Campaignable $campaign, ResumeCampaignAction $action): CampaignResource
    {
        $campaign = $action->handle($campaign->getOrigin(), $this->getCurrentUser());
        return new CampaignResource($campaign);
    }
}
