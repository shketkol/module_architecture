<?php

namespace Modules\Campaign\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Actions\Status\PauseCampaignAction;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\Contracts\Campaignable;

class PauseCampaignController extends Controller
{
    /**
     * Pause campaign.
     *
     * @param Campaign            $campaign
     * @param PauseCampaignAction $action
     *
     * @return CampaignResource
     * @throws \Modules\Campaign\Exceptions\CampaignNotUpdatedException
     * @throws \SM\SMException
     * @throws \Throwable
     */
    public function __invoke(Campaignable $campaign, PauseCampaignAction $action): CampaignResource
    {
        $campaign = $action->handle($campaign->getOrigin(), Auth::user());
        return new CampaignResource($campaign);
    }
}
