<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\Campaign\Actions\GetCampaignFrequencyDetailAction;
use Modules\Campaign\Http\Resources\CampaignFrequencyResource;
use Modules\Campaign\Models\Contracts\Campaignable;

/**
 * @package Modules\Campaign\Http\Controllers\Api
 */
class CampaignFrequencyController extends Controller
{
    /**
     * @param Campaignable                     $campaign
     * @param Authenticatable                  $user
     * @param GetCampaignFrequencyDetailAction $action
     *
     * @return CampaignFrequencyResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function __invoke(
        Campaignable $campaign,
        Authenticatable $user,
        GetCampaignFrequencyDetailAction $action
    ): CampaignFrequencyResource {
        return new CampaignFrequencyResource($action->handle($campaign->getOrigin(), $user));
    }
}
