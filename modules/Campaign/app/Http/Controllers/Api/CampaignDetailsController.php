<?php

namespace Modules\Campaign\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Campaign\Http\Controllers\Api\Traits\StatusMessage;
use Modules\Campaign\Http\Resources\CampaignResource;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Promocode\Actions\DiscardPromocodeAction;

class CampaignDetailsController extends Controller
{
    use StatusMessage;

    /**
     * @param Campaignable           $campaign
     * @param DiscardPromocodeAction $discardPromocode
     *
     * @return CampaignResource
     * @throws \Throwable
     */
    public function __invoke(Campaignable $campaign, DiscardPromocodeAction $discardPromocode): CampaignResource
    {
        $discardPromocode->handle($campaign->getOrigin());
        $campaign->load('creatives');

        return (new CampaignResource($campaign->getOrigin()))->additional([
            'data' => [
                'targeting_message' => $this->getMessage(
                    $campaign->campaign ?
                        $campaign->campaign->liveRevision() :
                        null
                )
            ]
        ]);
    }
}
