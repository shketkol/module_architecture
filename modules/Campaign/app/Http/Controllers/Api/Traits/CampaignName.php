<?php

namespace Modules\Campaign\Http\Controllers\Api\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;

trait CampaignName
{
    /**
     * @param string|null $name
     *
     * @return string
     */
    private function truncate(?string $name): string
    {
        /** @var ValidationRules $validationRules */
        $validationRules = app(ValidationRules::class);

        return substr($name, 0, $validationRules->get('campaign.details.name.max'));
    }
}
