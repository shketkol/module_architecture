<?php

namespace Modules\Campaign\Http\Controllers\Api\Traits;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Arr;
use Modules\Campaign\Exceptions\CampaignNotCreatedException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\CampaignStep;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Repositories\CampaignRepository;

trait StoreCampaign
{
    use CampaignName;

    /**
     * Store new campaign instance.
     *
     * @param array $data
     * @param int   $step
     *
     * @return Campaign
     * @throws CampaignNotCreatedException
     */
    protected function storeCampaign(array $data, int $step = CampaignStep::ID_NAME): Campaign
    {
        /** @var CampaignRepository $repository */
        $repository = app(CampaignRepository::class);

        /** @var Guard $auth */
        $auth = app(Guard::class);

        $campaign = $repository->newInstance([]);

        Arr::set($data, 'name', $this->truncate(Arr::get($data, 'name')));
        Arr::set($data, 'submitted_at', Carbon::now());

        $campaign->fill($data);

        $campaign->user()->associate($auth->user());
        $campaign->status()->associate(CampaignStatus::ID_DRAFT);
        $campaign->step()->associate($step);

        if (!$campaign->save()) {
            throw new CampaignNotCreatedException('Campaign was not created');
        }

        $this->log->info('Campaign was successfully saved.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        return $campaign;
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Campaignable
     */
    protected function checkDates(Campaignable $campaign): Campaignable
    {
        $this->log->info('Update campaign start/end dates.', [
            'campaign_id'   => $campaign->id,
            'campaign_type' => get_class($campaign),
        ]);

        $tomorrow = $this->getTomorrowDate();

        if ($campaign->date_end <= $tomorrow) {
            $campaign->date_end = $this->getEndDateUpdated($campaign);
        }

        $campaign->date_start = $tomorrow;

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     *
     * @return Campaign
     */
    protected function createNewDates(Campaign $campaign): Campaign
    {
        $campaign->date_end = $this->getEndDateUpdated($campaign);
        $campaign->date_start = $this->getTomorrowDate();

        return $campaign;
    }

    /**
     * @return Carbon
     */
    private function getTomorrowDate(): CarbonImmutable
    {
        return CarbonImmutable::tomorrow(config('campaign.wizard.date.timezone'));
    }

    /**
     * @param Campaignable $campaign
     *
     * @return Carbon
     */
    private function getEndDateUpdated(Campaignable $campaign): CarbonImmutable
    {
        $startDate = Carbon::parse($campaign->date_start);
        $endDate = Carbon::parse($campaign->date_end);
        $diff = $startDate->diffInDays($endDate);

        return $this->getTomorrowDate()->addDays($diff)->endOfDay();
    }
}
