<?php

namespace Modules\Campaign\Http\Controllers\Api\Traits;

use Modules\Campaign\Models\CampaignRevision;

trait StatusMessage
{
    /**
     * @param CampaignRevision|null $revision
     *
     * @return string|null
     */
    private function getMessage(?CampaignRevision $revision): ?string
    {
        if (is_null($revision)) {
            return null;
        }

        if ($revision->isLive()) {
            return __('campaign::messages.campaign_live_status');
        }

        if ($revision->isDraft()) {
            return __('campaign::messages.campaign_draft_status', [
                'link' => route('campaigns.edit', ['campaign' => $revision->campaign_id])
            ]);
        }

        if ($revision->isProcessing()) {
            return __('campaign::messages.campaign_processing_status');
        }

        return null;
    }
}
