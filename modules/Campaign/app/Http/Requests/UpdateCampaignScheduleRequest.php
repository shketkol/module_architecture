<?php

namespace Modules\Campaign\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\ValidationRules;
use Modules\Campaign\Actions\Validations\Traits\CampaignDetailsValidation;
use Modules\Campaign\Models\Contracts\Campaignable;

/**
 * @property Campaignable $campaign
 */
class UpdateCampaignScheduleRequest extends Request
{
    use CampaignDetailsValidation;

    /**
     * @var ValidationRules
     */
    protected ValidationRules $validationRules;

    /**
     * @param ValidationRules $rules
     *
     * @return array
     */
    public function rules(ValidationRules $rules): array
    {
        $this->validationRules = $rules;

        return [
            'date.start' => $this->validationRules->only(
                'campaign.details.date.start',
                $this->getLiveModeRules($this->campaign)
            ),
        ];
    }
}
