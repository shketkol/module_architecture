<?php

namespace Modules\Campaign\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class SearchCampaignsRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'name' => $validationRules->only(
                'campaign.details.name',
                ['nullable', 'string', 'min', 'max', 'regex']
            ),
            'statuses' => $validationRules->only(
                'campaign.search.statuses',
                ['array', 'nullable', 'min', 'max']
            ),
            'statuses.*' => $validationRules->only(
                'campaign.search.statuses',
                ['integer', 'min', 'max']
            ),
            'limit' => $validationRules->only(
                'campaign.search.limit',
                ['integer', 'min', 'max', 'nullable']
            ),
        ];
    }
}
