<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\Pivot\CampaignCreative;

/**
 * Trait BelongsToManyCampaigns
 *
 * @package Modules\Campaign\Models\Traits
 * @property Campaign[]|\Illuminate\Database\Eloquent\Collection $campaigns
 */
trait BelongsToManyCampaigns
{
    /**
     * @return BelongsToMany
     */
    public function campaigns(): BelongsToMany
    {
        return $this->morphedByMany(Campaign::class, 'campaign', 'campaign_creative')
            ->withTimestamps()
            ->using(CampaignCreative::class);
    }
}
