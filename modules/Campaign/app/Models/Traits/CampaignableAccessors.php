<?php

namespace Modules\Campaign\Models\Traits;

use Modules\Campaign\Models\Campaign;

/**
 * @mixin Campaign
 */
trait CampaignableAccessors
{
    /**
     * @return int
     */
    public function getOriginId(): int
    {
        return $this->getOrigin()->id;
    }
}
