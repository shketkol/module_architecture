<?php

namespace Modules\Campaign\Models\Traits;

trait ChosenTargetingValues
{
    /**
     * @param string $relation
     * @param bool   $visible
     *
     * @return bool
     */
    public function containsInvisibleTargetingValues(string $relation, bool $visible = false): bool
    {
        if (!$this->isLiveEdit()) {
            return false;
        }

        $nonVisibleTargetings = $this->{$relation}()->where('visible', $visible)->get();
        if ($nonVisibleTargetings->isNotEmpty()) {
            return true;
        }

        return false;
    }
}
