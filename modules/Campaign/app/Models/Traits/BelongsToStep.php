<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\CampaignStep;

/**
 * @property CampaignStep $step
 */
trait BelongsToStep
{
    /**
     * @return BelongsTo
     */
    public function step(): BelongsTo
    {
        return $this->belongsTo(CampaignStep::class, 'step_id');
    }

    /**
     * Get current step for the campaign.
     *
     * @return string
     */
    public function getStep(): string
    {
        if ($this->step) {
            return $this->step->name;
        }

        if ($this->getActiveCreative()) {
            return CampaignStep::CREATIVE;
        }

        if ($this->budget) {
            return CampaignStep::BUDGET;
        }

        if ($this->timezone_id || $this->date_start || $this->date_end) {
            return CampaignStep::SCHEDULE;
        }

        return CampaignStep::NAME;
    }
}
