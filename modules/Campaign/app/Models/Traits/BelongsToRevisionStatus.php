<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Campaign\Models\CampaignRevisionStatus;

/**
 * @property CampaignRevisionStatus $status
 */
trait BelongsToRevisionStatus
{
    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(CampaignRevisionStatus::class, 'status_id');
    }

    /**
     * @return bool
     */
    public function isSubmitted(): bool
    {
        return $this->inState(CampaignRevisionStatus::ID_SUBMITTED);
    }

    /**
     * @return bool
     */
    public function isProcessing(): bool
    {
        return $this->inState(CampaignRevisionStatus::ID_PROCESSING);
    }

    /**
     * CampaignRevisions could not be paused
     *
     * @return bool
     */
    public function isPaused(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->inState(CampaignRevisionStatus::ID_DRAFT);
    }

    /**
     * @return bool
     */
    public function isLive(): bool
    {
        return $this->inState(CampaignRevisionStatus::ID_LIVE);
    }
}
