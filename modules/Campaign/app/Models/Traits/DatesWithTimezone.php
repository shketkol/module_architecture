<?php

namespace Modules\Campaign\Models\Traits;

use Carbon\Carbon;

/**
 * @property Carbon $start_date_with_timezone
 * @see Campaign::getStartDateWithTimezoneAttribute()
 * @property Carbon $end_date_with_timezone
 * @see Campaign::getEndDateWithTimezoneAttribute()
 */
trait DatesWithTimezone
{
    /**
     * Get campaign start date with timezone offset
     *
     * @return Carbon|null
     */
    public function getStartDateWithTimezoneAttribute(): ?Carbon
    {
        if (!$this->date_start) {
            return null;
        }

        return $this->convertDateToCampaignTimezone($this->date_start);
    }

    /**
     * @return Carbon|null
     */
    public function getStartDateForInventoryCheck(): ?Carbon
    {
        return $this->start_date_with_timezone;
    }

    /**
     * Get campaign end date with timezone offset
     *
     * @return Carbon|null
     */
    public function getEndDateWithTimezoneAttribute(): ?Carbon
    {
        if (!$this->date_end) {
            return null;
        }

        return $this->convertDateToCampaignTimezone($this->date_end);
    }

    /**
     * @param Carbon $date
     *
     * @return Carbon
     */
    private function convertDateToCampaignTimezone(Carbon $date): Carbon
    {
        return Carbon::createFromFormat(config('date.format.db_date_time'), $date, $this->timezone->full_code);
    }
}
