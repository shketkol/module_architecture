<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Modules\Campaign\Models\CampaignRevision;
use Modules\Campaign\Models\CampaignRevisionStatus;
use Modules\Creative\Models\Pivot\CampaignCreative;

/**
 * @property CampaignRevision[]|Collection $revisions
 */
trait BelongsToManyRevisions
{
    /**
     * @return BelongsToMany
     */
    public function revisions(): BelongsToMany
    {
        return $this->morphedByMany(CampaignRevision::class, 'campaign', 'campaign_creative')
            ->withTimestamps()
            ->using(CampaignCreative::class);
    }

    /**
     * @return  CampaignRevision[]|Collection
     */
    public function draftRevisions(): Collection
    {
        return $this->revisions()->where('status_id', '=', CampaignRevisionStatus::ID_DRAFT)->get();
    }
}
