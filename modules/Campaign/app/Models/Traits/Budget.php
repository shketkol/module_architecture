<?php

namespace Modules\Campaign\Models\Traits;

/**
 * @property float $remaining_budget
 * @property float $min_edit_budget
 */
trait Budget
{
    /**
     * @return float|null
     */
    public function getLowestCpm(): ?float
    {
        return $this->getOrigin()->discounted_cpm ?: $this->getOrigin()->cpm;
    }

    /**
     * @return float|null
     */
    public function getLowestBudget(): ?float
    {
        return $this->getOrigin()->discounted_cost ?: $this->getOrigin()->budget;
    }

    /**
     * @return float
     */
    public function getRemainingBudgetAttribute(): float
    {
        return $this->getOrigin()->budget - $this->getOrigin()->cost;
    }

    /**
     * @return float
     */
    public function getMinEditBudgetAttribute(): float
    {
        return !empty($this->getOrigin()->cost) ?
            (round($this->getOrigin()->cost * config('campaign.wizard.live_edit_budget_buffer')) / 100) :
            config('campaign.wizard.budget.min');
    }
}
