<?php

namespace Modules\Campaign\Models\Traits;

use Carbon\Carbon;

/**
 * @property \Carbon\Carbon|null $date_start
 * @property \Carbon\Carbon|null $date_end
 */
trait CampaignLifetime
{
    /**
     * Is campaign in live range?
     *
     * @return bool
     */
    public function isLiveRange(): bool
    {
        if (is_null($this->date_start) || is_null($this->date_end)) {
            return false;
        }

        return $this->date_end->gt(Carbon::now());
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        if (!$this->date_end || !$this->date_start) {
            return 0;
        }

        return ceil($this->date_end->floatDiffInRealDays($this->date_start));
    }
}
