<?php

namespace Modules\Campaign\Models\Traits;

use App\Models\Timezone;
use Carbon\Carbon;
use Modules\User\Models\User;

/**
 * @property int      $timezone_id
 * @property Timezone $timezone
 * @property User     $user
 * @property int      $user_id
 * @property int      $min_impressions
 * @property float    $cpm
 * @property Carbon   $start_date_with_timezone
 * @see Campaign::getStartDateWithTimezoneAttribute()
 * @property Carbon   $end_date_with_timezone
 * @see Campaign::getEndDateWithTimezoneAttribute()
 */
trait CampaignRevisionAccessors
{
    /**
     * CampaignRevision does not support promocode for now.
     *
     * @return bool
     */
    public function hasPromocode(): bool
    {
        return false;
    }

    /**
     * @return int
     */
    public function getDiscountMoney(): int
    {
        return $this->getOrigin()->getDiscountMoney();
    }

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->getOrigin()->getExternalId();
    }

    /**
     * @return string
     */
    public function getExternalIdAttribute(): string
    {
        return $this->getExternalId();
    }

    /**
     * CampaignRevision is available to edit only in submitted campaigns.
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function isLiveEdit(): bool
    {
        return $this->getOrigin()->isLiveEdit();
    }

    /**
     * CampaignRevision is available to edit only in submitted campaigns.
     *
     * @return bool
     */
    public function isLiveEditable(): bool
    {
        return $this->getOrigin()->isLiveEditable();
    }

    /**
     * CPM could not be changed
     *
     * @return float
     */
    public function getCpmAttribute(): float
    {
        return $this->getOrigin()->cpm;
    }

    /**
     * @return int
     */
    public function getTimezoneIdAttribute(): int
    {
        return $this->getOrigin()->timezone_id;
    }

    /**
     * @param int $id
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTimezoneIdAttribute(int $id): void
    {
        // we don't change timezone in revisions
    }

    /**
     * @return Timezone
     */
    public function getTimezoneAttribute(): Timezone
    {
        return $this->getOrigin()->timezone;
    }

    /**
     * @return User
     */
    public function getUserAttribute(): User
    {
        return $this->getOrigin()->user;
    }

    /**
     * @return int
     */
    public function getUserIdAttribute(): int
    {
        return $this->getOrigin()->user_id;
    }

    /**
     * @return string|null
     */
    public function getExternalLineItemIdAttribute(): ?string
    {
        return $this->getOrigin()->external_line_item_id;
    }

    /**
     * @return string|null
     */
    public function getStatusChangeActorAttribute(): ?string
    {
        return $this->getOrigin()->status_change_actor;
    }

    /**
     * @return Carbon|null
     */
    public function getStartDateForInventoryCheck(): ?Carbon
    {
        if (!$this->date_start) {
            return null;
        }

        $now = Carbon::now()->startOfDay();
        $startDate = $this->date_start->lessThan($now) ? $now : $this->date_start;

        return $this->convertDateToCampaignTimezone($startDate);
    }
}
