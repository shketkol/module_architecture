<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Campaign\Models\Campaign;

/**
 * @property Campaign $campaign
 */
trait BelongsToCampaign
{
    /**
     * @return BelongsTo
     */
    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * @return Campaign
     */
    public function getOrigin(): Campaign
    {
        return $this->campaign;
    }

    /**
     * @return bool
     */
    public function isOrigin(): bool
    {
        return false;
    }
}
