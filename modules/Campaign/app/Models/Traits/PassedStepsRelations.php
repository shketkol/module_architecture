<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStep;

trait PassedStepsRelations
{
    /**
     * @return $this
     */
    public function loadPassedStepsRelations(): self
    {
        return $this->load($this->getPassedStepsRelations());
    }

    /**
     * Get the relation names of the steps that have been passed.
     *
     * @return array
     */
    protected function getPassedStepsRelations(): array
    {
        $step = $this->isOrigin() ? $this->step_id : CampaignStep::ID_PAYMENT;

        return Arr::flatten(
            array_filter(CampaignStep::STEPS_RELATIONS, function (int $key) use ($step): bool {
                return $key <= $step;
            }, ARRAY_FILTER_USE_KEY)
        );
    }
}
