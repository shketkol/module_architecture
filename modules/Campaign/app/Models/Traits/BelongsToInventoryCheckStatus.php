<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Campaign\Models\InventoryCheckStatus;

/**
 * @property InventoryCheckStatus $status
 */
trait BelongsToInventoryCheckStatus
{
    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(InventoryCheckStatus::class, 'status_id');
    }

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->status_id === InventoryCheckStatus::ID_AVAILABLE;
    }

    /**
     * @return bool
     */
    public function isFailed(): bool
    {
        return $this->status_id === InventoryCheckStatus::ID_INTERNAL_FAILED;
    }
}
