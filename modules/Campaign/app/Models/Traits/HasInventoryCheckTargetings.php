<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Modules\Campaign\Models\InventoryCheckTargeting;

/**
 * @property Collection|InventoryCheckTargeting[] $targetings
 */
trait HasInventoryCheckTargetings
{
    /**
     * @return HasMany
     */
    public function targetings(): HasMany
    {
        return $this->hasMany(InventoryCheckTargeting::class, 'history_id');
    }
}
