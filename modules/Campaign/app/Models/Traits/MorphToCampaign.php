<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Modules\Campaign\Models\Campaign;

/**
 * @property Campaign $campaign
 */
trait MorphToCampaign
{
    /**
     * @return MorphTo
     */
    public function campaign(): MorphTo
    {
        return $this->morphTo(__FUNCTION__, 'campaign_type', 'campaign_id');
    }
}
