<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Modules\Targeting\Models\TargetingValue;

/**
 * @property TargetingValue $targetable
 */
trait MorphToTargeting
{
    /**
     * Get the parent targeting model (age groups, audiences, etc.).
     *
     * @return MorphTo
     */
    public function targetable(): MorphTo
    {
        return $this->morphTo();
    }
}
