<?php

namespace Modules\Campaign\Models\Traits;

use Modules\Campaign\Models\CampaignStatus;

trait Deletable
{
    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->status_id === CampaignStatus::ID_DRAFT;
    }
}
