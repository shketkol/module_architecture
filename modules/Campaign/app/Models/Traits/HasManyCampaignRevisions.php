<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignRevision;
use Modules\Campaign\Models\CampaignRevisionStatus;

/**
 * @property CampaignRevision[]|\Illuminate\Database\Eloquent\Collection $revisions
 */
trait HasManyCampaignRevisions
{
    /**
     * @return HasMany
     */
    public function revisions(): HasMany
    {
        return $this->hasMany(CampaignRevision::class, 'campaign_id');
    }

    /**
     * @return bool
     */
    public function existRevisions(): bool
    {
        return $this->revisions()->exists();
    }

    /**
     * @return CampaignRevision|null
     */
    public function lastRevision(): ?CampaignRevision
    {
        return $this->revisions()->orderBy('id', 'desc')->first();
    }

    /**
     * @return CampaignRevision|null
     */
    public function liveRevision(): ?CampaignRevision
    {
        return $this->revisions()
            ->where('status_id', '=', CampaignRevisionStatus::ID_LIVE)
            ->first();
    }

    /**
     * @return CampaignRevision|null
     */
    public function lastNonDraftRevision(): ?CampaignRevision
    {
        return $this->revisions()
            ->where('status_id', '!=', CampaignRevisionStatus::ID_DRAFT)
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * @return CampaignRevision|null
     */
    public function getProcessingRevision(): ?CampaignRevision
    {
        return $this->revisions()
            ->where('status_id', '=', CampaignRevisionStatus::ID_PROCESSING)
            ->first();
    }

    /**
     * @return bool
     */
    public function canHaveRevisions(): bool
    {
        return $this->isLiveEditable();
    }

    /**
     * @return Campaign
     */
    public function getOrigin(): Campaign
    {
        return $this;
    }

    /**
     * @return bool
     */
    public function isOrigin(): bool
    {
        return true;
    }
}
