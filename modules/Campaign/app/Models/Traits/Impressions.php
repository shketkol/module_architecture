<?php

namespace Modules\Campaign\Models\Traits;

trait Impressions
{
    /**
     * @return int
     */
    public function getRemainedImpressions(): int
    {
        return $this->impressions - (int)$this->getOrigin()->delivered_impressions;
    }
}
