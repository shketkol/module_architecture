<?php

namespace Modules\Campaign\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Campaign\Models\CampaignStatus;
use Modules\User\Models\User;

/**
 * @property CampaignStatus      $status
 * @property CampaignStatus|null $previousStatus
 * @property User|null           $statusChangedBy
 * @property string|null         $status_change_actor
 */
trait BelongsToStatus
{
    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(CampaignStatus::class, 'status_id');
    }

    /**
     * @return BelongsTo
     */
    public function previousStatus(): BelongsTo
    {
        return $this->belongsTo(CampaignStatus::class, 'previous_status_id');
    }

    /**
     * @return BelongsTo
     */
    public function statusChangedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'status_changed_by');
    }

    /**
     * Returns label to display who changed the campaign status.
     *
     * @return null|string
     */
    public function getStatusChangeActorAttribute(): ?string
    {
        if (!$this->statusChangedBy) {
            return null;
        }

        return $this->statusChangedBy->isAdvertiser() ?
            __('labels.advertiser') :
            __('labels.administrator_hulu');
    }

    /**
     * Returns true if the campaign is paused.
     *
     * @return bool
     */
    public function isPaused(): bool
    {
        return $this->status_id === CampaignStatus::ID_PAUSED;
    }

    /**
     * Returns true if the campaign is draft.
     *
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->status_id === CampaignStatus::ID_DRAFT;
    }

    /**
     * Returns true if the campaign is in ready state.
     *
     * @return bool
     */
    public function isReady(): bool
    {
        return $this->status_id === CampaignStatus::ID_READY;
    }

    /**
     * Returns true if the campaign is in processing state.
     *
     * @return bool
     */
    public function isProcessing(): bool
    {
        return $this->status_id === CampaignStatus::ID_PROCESSING;
    }

    /**
     * Returns true if the campaign is in pending approval state.
     *
     * @return bool
     */
    public function isPending(): bool
    {
        return $this->status_id === CampaignStatus::ID_PENDING_APPROVAL;
    }

    /**
     * Returns true if the campaign is in suspended state.
     *
     * @return bool
     */
    public function isSuspended(): bool
    {
        return $this->status_id === CampaignStatus::ID_SUSPENDED;
    }

    /**
     * Returns true if the campaign is in live state.
     *
     * @return bool
     */
    public function isLive(): bool
    {
        return $this->status_id === CampaignStatus::ID_LIVE;
    }

    /**
     * Returns true if alert notifications should be skipped.
     *
     * @return bool
     */
    public function skipAlerts(): bool
    {
        return $this->inState([
            CampaignStatus::ID_CANCELED,
            CampaignStatus::ID_COMPLETED,
        ]);
    }
}
