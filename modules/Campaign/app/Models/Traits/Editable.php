<?php

namespace Modules\Campaign\Models\Traits;

use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRevisionRepository;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeStatus;
use Modules\User\Models\User;

trait Editable
{
    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->isLiveEditable() ?: $this->inState(CampaignStatus::ID_DRAFT);
    }

    /**
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function isLiveEdit(): bool
    {
        /** @var CampaignRevisionRepository $repository */
        $repository = app(CampaignRevisionRepository::class);

        return $this->isLiveEditable() && $repository->issetDraftRevision($this);
    }

    /**
     * @return bool
     */
    public function isLiveEditable(): bool
    {
        return $this->inState(CampaignStatus::ACTIVE_STATUSES);
    }

    /**
     * @return bool
     */
    public function isEditableCreative(): bool
    {
        if ($this->inState([CampaignStatus::ID_CANCELED, CampaignStatus::ID_COMPLETED])) {
            return false;
        }

        /** @var Creative $creative */
        $creative = $this->getActiveCreative();

        if ($this->isProcessing() && !is_null($creative) && $creative->isProcessing()) {
            return false;
        }

        if ($this->isPending() && !is_null($creative) && !$creative->isRejected()) {
            return false;
        }

        if ($this->isReady() && !is_null($creative) && $this->creativeOnProcessing($creative)) {
            return false;
        }

        /** @var User $user */
        $user = $this->user;
        $approvedCreatives = $user->creatives()->approved()->count();

        if ($this->isReady() && $approvedCreatives < 2) {
            return false;
        }

        if ($this->isLive() && !is_null($creative) && $creative->isApproved()) {
            return false;
        }

        if ($this->isPaused() && !is_null($creative) && $creative->inState([
                CreativeStatus::ID_PENDING_APPROVAL,
                CreativeStatus::ID_APPROVED,
                CreativeStatus::ID_PROCESSING,
            ])
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isOnlyReplaceableCreative(): bool
    {
        /** @var Creative $creative */
        $creative = $this->getActiveCreative();

        if (!is_null($creative) && $this->isPending() && $creative->isRejected()) {
            return true;
        }

        if (!is_null($creative) && $this->isReady() && $creative->isApproved()) {
            return true;
        }

        if (!is_null($creative) && $this->isPaused() && $creative->isRejected()) {
            return true;
        }

        return false;
    }

    /**
     * @param Creative $creative
     *
     * @return bool
     */
    protected function creativeOnProcessing(Creative $creative): bool
    {
        return $creative->inState([CreativeStatus::ID_PENDING_APPROVAL, CreativeStatus::ID_PROCESSING]);
    }
}
