<?php

namespace Modules\Campaign\Models;

use App\Traits\State;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Models\Traits\BelongsToCampaign;
use Modules\Campaign\Models\Traits\BelongsToRevisionStatus;
use Modules\Campaign\Models\Traits\BelongsToStep;
use Modules\Campaign\Models\Traits\Budget;
use Modules\Campaign\Models\Traits\CampaignableAccessors;
use Modules\Campaign\Models\Traits\CampaignLifetime;
use Modules\Campaign\Models\Traits\CampaignRevisionAccessors;
use Modules\Campaign\Models\Traits\DatesWithTimezone;
use Modules\Campaign\Models\Traits\Impressions;
use Modules\Campaign\Models\Traits\PassedStepsRelations;
use Modules\Creative\Models\Traits\MorphToManyCreatives;
use Modules\Targeting\Models\Traits\MorphToManyAudiences;
use Modules\Targeting\Models\Traits\MorphToManyDeviceGroups;
use Modules\Targeting\Models\Traits\MorphToManyGenres;
use Modules\Targeting\Models\Traits\MorphToManyLocations;
use Modules\Targeting\Models\Traits\MorphToManyTargetingAgeGroups;
use Modules\Targeting\Models\Traits\MorphToManyTargetingGenders;
use Modules\Targeting\Models\Traits\MorphToManyZipcodes;

/**
 * @property int    $id
 * @property int    $campaign_id
 * @property string $name
 * @property Carbon $date_start
 * @property Carbon $date_end
 * @property float  $budget
 * @property int    $impressions
 * @property int    $step_id
 * @property int    $status_id
 * @property Carbon $submitted_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class CampaignRevision extends Model implements Campaignable
{
    use BelongsToCampaign,
        BelongsToStep,
        BelongsToRevisionStatus,
        MorphToManyTargetingAgeGroups,
        MorphToManyAudiences,
        MorphToManyDeviceGroups,
        MorphToManyTargetingGenders,
        MorphToManyGenres,
        MorphToManyLocations,
        MorphToManyZipcodes,
        MorphToManyCreatives,
        CampaignRevisionAccessors,
        CampaignableAccessors,
        CampaignLifetime,
        PassedStepsRelations,
        Budget,
        State,
        Impressions,
        DatesWithTimezone {
        CampaignRevisionAccessors::getStartDateForInventoryCheck insteadof DatesWithTimezone;
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_start',
        'date_end',
        'submitted_at',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'name',
        'date_start',
        'date_end',
        'budget',
        'impressions',
        'step_id',
        'status_id',
        'submitted_at',
    ];

    /**
     * Campaign state's flow.
     *
     * @var string
     */
    protected string $flow = 'campaign_revision';
}
