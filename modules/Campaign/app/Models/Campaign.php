<?php

namespace Modules\Campaign\Models;

use App\Models\Traits\BelongsToTimezone;
use App\Traits\HasFactory;
use App\Traits\State;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Models\Traits\BelongsToStatus;
use Modules\Campaign\Models\Traits\BelongsToStep;
use Modules\Campaign\Models\Traits\Budget;
use Modules\Campaign\Models\Traits\CampaignableAccessors;
use Modules\Campaign\Models\Traits\CampaignLifetime;
use Modules\Campaign\Models\Traits\ChosenTargetingValues;
use Modules\Campaign\Models\Traits\DatesWithTimezone;
use Modules\Campaign\Models\Traits\Deletable;
use Modules\Campaign\Models\Traits\Editable;
use Modules\Campaign\Models\Traits\HasManyCampaignRevisions;
use Modules\Campaign\Models\Traits\Impressions;
use Modules\Campaign\Models\Traits\PassedStepsRelations;
use Modules\Creative\Models\Traits\MorphToManyCreatives;
use Modules\Notification\Models\Traits\MorphAlerts;
use Modules\Promocode\Models\Traits\BelongsToPromocode;
use Modules\Targeting\Models\Traits\MorphToManyAudiences;
use Modules\Targeting\Models\Traits\MorphToManyDeviceGroups;
use Modules\Targeting\Models\Traits\MorphToManyLocations;
use Modules\Targeting\Models\Traits\MorphToManyGenres;
use Modules\Targeting\Models\Traits\MorphToManyTargetingAgeGroups;
use Modules\Targeting\Models\Traits\MorphToManyTargetingGenders;
use Modules\Targeting\Models\Traits\MorphToManyZipcodes;
use Modules\User\Models\Traits\BelongsToUser;
use Modules\Payment\Models\Traits\BelongsToPaymentMethod;

/**
 * @property int      $id
 * @property int      $user_id
 * @property int      $previous_status_id
 * @property integer  $status_id
 * @property integer  $status_changed_by
 * @property Carbon   $status_changed_at
 * @property integer  $timezone_id
 * @property string   $external_id
 * @property string   $external_line_item_id
 * @property float    $budget
 * @property float    $cost
 * @property float    $cpm
 * @property float    $discounted_cpm
 * @property int      $impressions
 * @property int|null $delivered_impressions
 * @property int      $min_impressions
 * @property float    $discounted_cost
 * @property string   $name
 * @property Carbon   $created_at
 * @property Carbon   $updated_at
 * @property Carbon   $submitted_at
 * @property int      $step_id
 * @property int      $order_id
 * @property int      $promocode_id
 * @property int      $payment_method_id
 * @property Carbon   $date_start
 * @property Carbon   $date_end
 */
class Campaign extends Model implements Campaignable
{
    use MorphToManyCreatives,
        MorphToManyGenres,
        MorphToManyDeviceGroups,
        MorphToManyLocations,
        MorphToManyZipcodes,
        MorphToManyTargetingAgeGroups,
        MorphToManyTargetingGenders,
        MorphToManyAudiences,
        BelongsToPaymentMethod,
        BelongsToStatus,
        BelongsToStep,
        BelongsToTimezone,
        BelongsToUser,
        CampaignableAccessors,
        CampaignLifetime,
        Deletable,
        Editable,
        BelongsToPromocode,
        MorphAlerts,
        State,
        Budget,
        PassedStepsRelations,
        HasFactory,
        HasManyCampaignRevisions,
        DatesWithTimezone,
        Impressions,
        ChosenTargetingValues;

    /**
     * @see targetingAgeGroups
     */
    public const RELATION_AGE_GROUPS_NAME = 'targetingAgeGroups';

    /**
     * @see targetingGenders
     */
    public const RELATION_GENDERS_NAME = 'targetingGenders';

    /**
     * @see audiences
     */
    public const RELATION_AUDIENCES_NAME = 'audiences';

    /**
     * @see locations
     */
    public const RELATION_LOCATIONS_NAME = 'locations';

    /**
     * @see zipcodes
     */
    public const RELATION_ZIPCODES_NAME = 'zipcodes';

    /**
     * @see genres
     */
    public const RELATION_GENRES_NAME = 'genres';

    /**
     * @var array
     */
    protected $fillable = [
        'budget',
        'cost',
        'cpm',
        'impressions',
        'delivered_impressions',
        'name',
        'date_start',
        'date_end',
        'user_id',
        'payment_method_id',
        'previous_status_id',
        'status_id',
        'timezone_id',
        'external_id',
        'order_id',
        'discounted_cost',
        'submitted_at',
        'status_changed_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_start',
        'date_end',
        'submitted_at',
        'status_changed_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'budget' => 'float',
        'cost'   => 'float',
    ];

    /**
     * Campaign state's flow.
     *
     * @var string
     */
    protected string $flow = 'campaign';

    /**
     * HAAPI external id.
     *
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->external_id;
    }

    /**
     * @return string|null
     */
    public function getPreviousStatus(): ?string
    {
        if (!$this->previous_status_id) {
            return null;
        }

        return Arr::first(array_keys(
            CampaignStatus::TRANSITIONS,
            $this->previous_status_id
        ));
    }
}
