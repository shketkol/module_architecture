<?php

namespace Modules\Campaign\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\Traits\BelongsToCampaign;
use Modules\Campaign\Models\Traits\BelongsToInventoryCheckStatus;
use Modules\Campaign\Models\Traits\DrasticLack;
use Modules\Campaign\Models\Traits\HasInventoryCheckTargetings;
use Modules\Campaign\Models\Traits\MorphToCampaign;

/**
 * @property int    $id
 * @property int    $campaign_id
 * @property Carbon $date_start
 * @property Carbon $date_end
 * @property int    $ordered_impressions
 * @property int    $available_impressions
 * @property float  $campaign_budget
 * @property float  $available_budget
 * @property int    $status_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class InventoryCheckHistory extends Model
{
    use HasInventoryCheckTargetings,
        BelongsToInventoryCheckStatus,
        MorphToCampaign,
        DrasticLack;

    /**
     * Minimum allowed campaign budget
     */
    public const SYSTEM_FLOOR_BUDGET = 500;

    /**
     * @inheritdoc
     */
    protected $table = 'inventory_check_history';

    /**
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'campaign_type',
        'date_start',
        'date_end',
        'ordered_impressions',
        'available_impressions',
        'campaign_budget',
        'available_budget',
        'status_id',
    ];

    /**
     * @inheritdoc
     */
    protected $dates = [
        'date_start',
        'date_end',
    ];
}
