<?php

namespace Modules\Campaign\Models\Contracts;

use Modules\Campaign\Models\Campaign;

/**
 * @mixin Campaign
 * @mixin \Modules\Campaign\Models\CampaignRevision
 */
interface Campaignable
{
    /**
     * @return Campaign
     */
    public function getOrigin(): Campaign;

    /**
     * @return bool
     */
    public function isOrigin(): bool;
}
