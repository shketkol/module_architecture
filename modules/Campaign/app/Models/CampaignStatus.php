<?php

namespace Modules\Campaign\Models;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\Traits\HasCampaigns;

/**
 * @property int    $id
 * @property string $name
 * @property string $translated_name
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class CampaignStatus extends Model
{
    use HasCampaigns;

    /**
     * Campaign just created and in draft state.
     * Does not exists in HAAPI.
     */
    public const DRAFT = 'draft';

    /**
     * This is the initial, typically short-term, status of a campaign.
     * It indicates that the campaign has been successfully created and is under review because
     * a creative is missing or pending approval.
     */
    public const PENDING_APPROVAL = 'pending approval';

    /**
     * This status indicates that a campaign has been reviewed and its creative has been approved.
     * The campaign is now ready to go live on the start date specified for its line item.
     */
    public const READY = 'ready';

    /**
     * This status indicates that a campaign is running.
     * An approved campaign automatically goes live on the start date of its line item.
     */
    public const LIVE = 'live';

    /**
     * This status shall be set when a user pauses a campaign.
     * After the campaign is resumed, its status is set back to the one before the campaign was paused,
     * which is either READY or LIVE.
     *
     * A campaign may be also paused due to payment issues, in which case its status shall be SUSPENDED.
     */
    public const PAUSED = 'paused';

    /**
     * This status indicates that a campaign has passed the end date of its line item and is no longer running.
     */
    public const COMPLETED = 'completed';

    /**
     * This status shall be set when a user cancels a campaign.
     * The user may not resume a cancelled campaign and shall have to create a new one.
     */
    public const CANCELED = 'canceled';

    /**
     * This status shall be set when a request sent to campaign change
     */
    public const PROCESSING = 'processing';

    /**
     * This status indicates that a campaign payment has failed.
     * Regardless of which campaign payment fails for a given advertiser,
     * all of the advertiser’s campaigns get suspended.
     * The campaigns shall resume automatically after the advertiser resolves the payment issue.
     */
    public const SUSPENDED = 'suspended';

    public const ID_DRAFT            = 1;
    public const ID_PENDING_APPROVAL = 2;
    public const ID_READY            = 3;
    public const ID_LIVE             = 4;
    public const ID_PAUSED           = 5;
    public const ID_COMPLETED        = 6;
    public const ID_CANCELED         = 7;
    public const ID_PROCESSING       = 8;
    public const ID_SUSPENDED        = 9;

    public const TRANSITIONS = [
        self::DRAFT            => self::ID_DRAFT,
        self::PENDING_APPROVAL => self::ID_PENDING_APPROVAL,
        self::READY            => self::ID_READY,
        self::LIVE             => self::ID_LIVE,
        self::PAUSED           => self::ID_PAUSED,
        self::COMPLETED        => self::ID_COMPLETED,
        self::CANCELED         => self::ID_CANCELED,
        self::PROCESSING       => self::ID_PROCESSING,
        self::SUSPENDED        => self::ID_SUSPENDED,
    ];

    /**
     * Statuses indicating that campaign is ready to be used or in live.
     */
    public const ACTIVE_STATUSES = [
        self::ID_READY,
        self::ID_LIVE,
        self::ID_PAUSED,
    ];

    /**
     * Array campaigns statuses are used in the admin campaign sorting by default.
     */
    public static array $defaultAdminCampaignsSortBy = [
        self::LIVE,
        self::READY,
        self::DRAFT,
        self::CANCELED,
        self::COMPLETED,
        self::PAUSED,
        self::PENDING_APPROVAL,
        self::PROCESSING,
        self::SUSPENDED,
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get translated status name.
     *
     * @return string
     */
    public function getTranslatedNameAttribute(): string
    {
        return __("campaign::labels.campaign.statuses.$this->name");
    }

    /**
     * @param string $transition
     *
     * @return int
     * @throws InvalidArgumentException
     */
    public static function getIdByTransition(string $transition): int
    {
        $id = Arr::get(static::TRANSITIONS, $transition);

        if (is_null($id)) {
            throw new InvalidArgumentException(sprintf('Invalid transition: "%s".', $transition));
        }

        return $id;
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public static function getTransitionById(int $id): string
    {
        $transition = array_search($id, static::TRANSITIONS, true);
        if ($transition === false) {
            throw new InvalidArgumentException(sprintf('Invalid transition id: "%d".', $id));
        }

        return $transition;
    }
}
