<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 * @property string $translated_name
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class CampaignRevisionStatus extends Model
{
    public const DRAFT = 'draft';
    public const LIVE = 'live';
    public const SUBMITTED = 'submitted';
    public const PROCESSING = 'processing';

    public const ID_DRAFT = 1;
    public const ID_LIVE = 2;
    public const ID_SUBMITTED = 3;
    public const ID_PROCESSING = 4;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get translated status name.
     *
     * @return string
     */
    public function getTranslatedNameAttribute(): string
    {
        return __("campaign::labels.campaign_revision.statuses.{$this->name}");
    }
}
