<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Models\Traits\HasCampaigns;

/**
 * @property integer $id
 * @property string  $name
 */
class CampaignStep extends Model
{
    use HasCampaigns;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Keys of steps placed in correct order.
     */
    public const NAME      = 'details.name';
    public const SCHEDULE  = 'details.schedule';
    public const AGES      = 'targeting.ages';
    public const AUDIENCES = 'targeting.audiences';
    public const LOCATIONS = 'targeting.locations';
    public const GENRES    = 'targeting.genres';
    public const DEVICES   = 'targeting.devices';
    public const BUDGET    = 'budget.budget';
    public const CREATIVE  = 'creative.creative';
    public const REVIEW    = 'reviewAndPayment.review';
    public const PAYMENT   = 'reviewAndPayment.payment';

    /**
     * Id constants.
     */
    public const ID_NAME      = 1;
    public const ID_SCHEDULE  = 2;
    public const ID_BUDGET    = 3;
    public const ID_LOCATIONS = 4;
    public const ID_AGES      = 5;
    public const ID_AUDIENCES = 6;
    public const ID_DEVICES   = 7;
    public const ID_GENRES    = 8;
    public const ID_CREATIVE  = 9;
    public const ID_REVIEW    = 10;
    public const ID_PAYMENT   = 11;

    /**
     * Campaign's model relations that are associated with particular steps wizard.
     */
    public const STEPS_RELATIONS = [
        self::ID_AGES      => ['targetingGenders', 'targetingAgeGroups'],
        self::ID_LOCATIONS => ['locations', 'zipcodes'],
        self::ID_GENRES    => ['genres'],
        self::ID_AUDIENCES => ['audiences'],
        self::ID_DEVICES   => ['deviceGroups'],
    ];

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return array
     */
    public static function getStepIds(): array
    {
        return [
            CampaignStep::NAME      => CampaignStep::ID_NAME,
            CampaignStep::SCHEDULE  => CampaignStep::ID_SCHEDULE,
            CampaignStep::AGES      => CampaignStep::ID_AGES,
            CampaignStep::LOCATIONS => CampaignStep::ID_LOCATIONS,
            CampaignStep::GENRES    => CampaignStep::ID_GENRES,
            CampaignStep::DEVICES   => CampaignStep::ID_DEVICES,
            CampaignStep::AUDIENCES => CampaignStep::ID_AUDIENCES,
            CampaignStep::BUDGET    => CampaignStep::ID_BUDGET,
            CampaignStep::CREATIVE  => CampaignStep::ID_CREATIVE,
            CampaignStep::REVIEW    => CampaignStep::ID_REVIEW,
            CampaignStep::PAYMENT   => CampaignStep::ID_PAYMENT,
        ];
    }

    /**
     * @return array
     */
    public static function getStepNames(): array
    {
        return array_keys(self::getStepIds());
    }
}
