<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property string $name
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class InventoryCheckStatus extends Model
{
    /**
     * @see http://huluaddocs-haapi.prod.hulu.com.s3-website-us-west-2.amazonaws.com/Content/API-reference/campaign-inventory-check.htm
     */
    public const AVAILABLE = 'AVAILABLE';
    public const YX_CONNECTION_FAILED = 'YX CONNECTION FAILED - BUY ALLOWED';
    public const UNAVAILABLE = 'UNAVAILABLE';
    public const AVAILABILITY_UNDER_MIN_SPEND = 'AVAILABILITY_UNDER_MIN_SPEND';
    public const REQUEST_UNDER_MIN_PER_DAY = 'REQUEST_UNDER_MIN_PER_DAY';
    public const INTERNAL_IN_PROGRESS = 'INTERNAL_IN_PROGRESS';
    public const INTERNAL_FAILED = 'INTERNAL_FAILED';

    public const ID_AVAILABLE = 1;
    public const ID_YX_CONNECTION_FAILED = 2;
    public const ID_UNAVAILABLE = 3;
    public const ID_AVAILABILITY_UNDER_MIN_SPEND = 4;
    public const ID_REQUEST_UNDER_MIN_PER_DAY = 5;
    public const ID_INTERNAL_IN_PROGRESS = 6;
    public const ID_INTERNAL_FAILED = 7;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['name'];
}
