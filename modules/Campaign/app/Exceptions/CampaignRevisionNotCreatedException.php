<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class CampaignRevisionNotCreatedException extends ModelNotCreatedException
{
    //
}
