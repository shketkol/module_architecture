<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ValidationWithActionException;
use Illuminate\Http\Request;
use Modules\Campaign\Models\Contracts\Campaignable;

class CampaignUsesNonVisibleTargetingException extends ValidationWithActionException
{
    /**
     * @var Campaignable
     */
    protected Campaignable $campaign;

    /**
     * @param Campaignable $campaign
     * @param string       $message
     */
    public function __construct(Campaignable $campaign, string $message)
    {
        $this->campaign = $campaign;
        parent::__construct($message);
    }

    /**
     * @return string
     */
    public function getActionRouteName(): string
    {
        return 'api.campaigns.targeting.nonVisible.update';
    }

    /**
     * @return string
     */
    public function getActionHttpMethod(): string
    {
        return Request::METHOD_PATCH;
    }

    /**
     * @return array
     */
    public function getRequestParams(): array
    {
        return [
            'campaign' => $this->campaign->getKey(),
        ];
    }
}
