<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SameStatusException extends BaseException
{
    /**
     * @inheritdoc
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @param string $message
     *
     * @return self
     */
    public static function create(string $message): self
    {
        return new self($message);
    }

    /**
     * Report the exception.
     *
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'message' => $this->message,
        ], self::STATUS_CODE);
    }
}
