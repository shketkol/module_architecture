<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\BaseException;

class CampaignHasNoLineItemException extends BaseException
{
    //
}
