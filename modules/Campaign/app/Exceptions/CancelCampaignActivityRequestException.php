<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\Response;
use Modules\Campaign\Models\Contracts\Campaignable;

class CancelCampaignActivityRequestException extends BaseException
{
    /**
     * @inheritdoc
     */
    public const STATUS_CODE = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @param Campaignable $campaign
     *
     * @return self
     */
    public static function create(Campaignable $campaign): self
    {
        return new self(sprintf('"%s" #%d activity request was cancelled.', get_class($campaign), $campaign->id));
    }
}
