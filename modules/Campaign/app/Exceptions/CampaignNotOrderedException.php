<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ModelNotUpdatedException;
use Throwable;

class CampaignNotOrderedException extends ModelNotUpdatedException
{
    /**
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     *
     * @return CampaignNotOrderedException
     */
    public static function create(string $message, int $code = 0, Throwable $previous = null): self
    {
        return new self($message, $code, $previous);
    }
}
