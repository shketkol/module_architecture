<?php

namespace Modules\Campaign\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class CampaignNotEditedException extends ModelNotCreatedException
{

}
