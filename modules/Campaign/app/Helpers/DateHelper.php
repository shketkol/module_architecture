<?php

namespace Modules\Campaign\Helpers;

use Carbon\Carbon;

/**
 * Class DateHelper
 *
 * @package Modules\Campaign\Helpers
 */
class DateHelper
{
    /**
     * Get minimal start date of campaign.
     *
     * @return Carbon
     */
    public static function getMinStartDate()
    {
        return self::addBusinessDays(Carbon::today(), config('campaign.wizard.date.start.min.value'));
    }

    /**
     * Add N business days to the provided date.
     *
     * @param Carbon $date
     * @param int    $days
     *
     * @return Carbon
     */
    public static function addBusinessDays(Carbon $date, int $days): Carbon
    {
        return $date->addWeekdays($days);
    }

    /**
     * Returns `true` if dates are equal, `false` - otherwise.
     *
     * @param Carbon|null $date1
     * @param Carbon|null $date2
     *
     * @return bool
     */
    public static function areDatesEqual(?Carbon $date1, ?Carbon $date2): bool
    {
        if ($date1 instanceof Carbon && !($date2 instanceof Carbon) ||
            $date2 instanceof Carbon && !($date1 instanceof Carbon)
        ) {
            return false;
        }

        // If both dates are nulls
        if ($date1 === $date2) {
            return true;
        }

        return $date1->equalTo($date2);
    }
}
