<?php

namespace Modules\Campaign\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Modules\Campaign\Actions\Traits\Targetings;
use Modules\Campaign\Models\InventoryCheckHistory;
use Modules\Campaign\Models\InventoryCheckTargeting;
use Modules\Campaign\Repositories\InventoryCheckHistoryRepository;

class InventoryCheckHistoryHelper
{
    use Targetings;

    /**
     * @var InventoryCheckHistoryRepository
     */
    private $historyRepository;

    /**
     * @param InventoryCheckHistoryRepository $historyRepository
     */
    public function __construct(InventoryCheckHistoryRepository $historyRepository)
    {
        $this->historyRepository = $historyRepository;
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon|null
     */
    public function getSucceededStartDate(array $dates): ?Carbon
    {
        return Arr::get($dates, 'succeeded.date_start');
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon|null
     */
    public function getSucceededEndDate(array $dates): ?Carbon
    {
        return Arr::get($dates, 'succeeded.date_end');
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon
     */
    public function getFailedStartDate(array $dates): Carbon
    {
        return Arr::get($dates, 'failed.date_start');
    }

    /**
     * @param Carbon[][] $dates
     *
     * @return Carbon
     */
    public function getFailedEndDate(array $dates): Carbon
    {
        return Arr::get($dates, 'failed.date_end');
    }

    /**
     * @param Collection[][] $targetings
     *
     * @return Collection
     */
    public function getAddedTargetings(array $targetings): Collection
    {
        return Arr::get($targetings, 'added');
    }

    /**
     * @param Collection[][] $targetings
     *
     * @return Collection
     */
    public function getRemovedTargetings(array $targetings): Collection
    {
        return Arr::get($targetings, 'removed');
    }

    /**
     * @param InventoryCheckHistory $history
     *
     * @return array[]
     */
    public function findDiffBetweenSuccessfulAndFailedInventoryCheck(InventoryCheckHistory $history): array
    {
        $latestSucceededId = $this->historyRepository->findLastSucceededHistoryId($history);

        if (is_null($latestSucceededId)) {
            return [
                'dates'      => [
                    'succeeded' => [],
                    'failed'    => [
                        'date_start' => $history->date_start,
                        'date_end'   => $history->date_end,
                    ],
                ],
                'targetings' => [
                    'added'   => $this->getTargetingsFromHistory($history->targetings),
                    'removed' => collect([]),
                ],
            ];
        }

        $latestSucceeded = $this->historyRepository->findById($latestSucceededId);

        $targetingsDiff = $this->findAddedAndRemovedTargetings($latestSucceeded->targetings, $history->targetings);
        $datesDiff = $this->findChangedDates($latestSucceeded, $history);

        return [
            'dates'      => $datesDiff,
            'targetings' => $targetingsDiff,
        ];
    }

    /**
     * @param InventoryCheckHistory $succeeded
     * @param InventoryCheckHistory $failed
     *
     * @return array[]
     */
    private function findChangedDates(InventoryCheckHistory $succeeded, InventoryCheckHistory $failed): array
    {
        if ($this->isSameDate($succeeded, $failed)) {
            return [];
        }

        return [
            'succeeded' => [
                'date_start' => $succeeded->date_start,
                'date_end'   => $succeeded->date_end,
            ],
            'failed'    => [
                'date_start' => $failed->date_start,
                'date_end'   => $failed->date_end,
            ],
        ];
    }

    /**
     * @param InventoryCheckHistory $history1
     * @param InventoryCheckHistory $history2
     *
     * @return bool
     */
    private function isSameDate(InventoryCheckHistory $history1, InventoryCheckHistory $history2): bool
    {
        return $history1->date_start->eq($history2->date_start) && $history1->date_end->eq($history2->date_end);
    }

    /**
     * @param Collection|InventoryCheckTargeting[] $succeeded
     * @param Collection|InventoryCheckTargeting[] $failed
     *
     * @return array
     */
    private function findAddedAndRemovedTargetings(Collection $succeeded, Collection $failed): array
    {
        $removedHistory = $this->getTargetingsDiff($succeeded, $failed);
        $addedHistory = $this->getTargetingsDiff($failed, $succeeded);

        return [
            'added'   => $this->getTargetingsFromHistory($addedHistory),
            'removed' => $this->getTargetingsFromHistory($removedHistory),
        ];
    }

    /**
     * @param Collection|InventoryCheckTargeting[] $collection1
     * @param Collection|InventoryCheckTargeting[] $collection2
     *
     * @return Collection
     */
    private function getTargetingsDiff(Collection $collection1, Collection $collection2): Collection
    {
        return $collection1->filter(function (InventoryCheckTargeting $targeting1) use ($collection2): bool {
            return is_null($collection2->first(function (InventoryCheckTargeting $targeting2) use ($targeting1): bool {
                return $this->isSameTargetable($targeting1, $targeting2);
            }));
        });
    }

    /**
     * @param InventoryCheckTargeting $targeting1
     * @param InventoryCheckTargeting $targeting2
     *
     * @return bool
     */
    private function isSameTargetable(InventoryCheckTargeting $targeting1, InventoryCheckTargeting $targeting2): bool
    {
        return $targeting1->targetable_type === $targeting2->targetable_type
            && $targeting1->targetable_id === $targeting2->targetable_id
            && $targeting1->excluded === $targeting2->excluded;
    }

    /**
     * @param array $dates
     *
     * @return bool
     */
    public function areDatesChanged(array $dates): bool
    {
        return !empty($dates);
    }

    /**
     * @param array $targetings
     *
     * @return bool
     */
    public function isTargetingsChanged(array $targetings): bool
    {
        return $this->getAddedTargetings($targetings)->isNotEmpty()
            || $this->getRemovedTargetings($targetings)->isNotEmpty();
    }
}
