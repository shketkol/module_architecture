<?php

namespace Modules\Campaign\DataTable;

use App\DataTable\Cards\PlaceholderCard;
use App\DataTable\DataTable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Modules\Campaign\DataTable\Filters\CampaignStatusFilter;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;
use Modules\Campaign\DataTable\Repositories\Criteria\AddCampaignCompanyCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\AddCampaignStatusCriteria;
use Modules\Campaign\DataTable\Transformers\CampaignsTransformer;
use Modules\Campaign\Models\CampaignStatus;

class CampaignAdminDataTable extends DataTable
{
    const LIVE_COLUMN_NUMBER = 9;

    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'admin-campaigns';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = CampaignsTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = CampaignsDataTableRepository::class;

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [
        PlaceholderCard::class,
        PlaceholderCard::class,
        PlaceholderCard::class,
        PlaceholderCard::class,
    ];

    /**
     * Add aliases according model relation.
     * Required for search
     *
     * @var array
     */
    protected $relationAliases = [
        'creative_status' => 'creative_statuses.name',
        'company_name'    => 'users.company_name',
        'order_date'      => 'campaigns.submitted_at',
    ];

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        CampaignStatusFilter::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        $orderColumn = $this->request->get('order');

        /** @var CampaignsDataTableRepository $repository */
        $repository = $this->getRepository();

        if ((int) Arr::get($orderColumn, '0.column') === self::LIVE_COLUMN_NUMBER) {
            $order = 'FIELD(campaign_status,';
            foreach (CampaignStatus::$defaultAdminCampaignsSortBy as $key => $status) {
                $order .= "'$status'";
                if ($key !== count(CampaignStatus::$defaultAdminCampaignsSortBy) - 1) {
                    $order .= ',';
                }
            }
            $order .= ') asc, campaigns.submitted_at desc';

            return $repository->pushCriteria(new AddCampaignStatusCriteria())
               ->pushCriteria(new AddCampaignCompanyCriteria())
               ->campaigns()
               ->orderByRaw($order);
        }

        return $repository->pushCriteria(new AddCampaignStatusCriteria())
            ->pushCriteria(new AddCampaignCompanyCriteria())
            ->campaigns();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('campaign.list') && $this->getUser()->isAdmin();
    }
}
