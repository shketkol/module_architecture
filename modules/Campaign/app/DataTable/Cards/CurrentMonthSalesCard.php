<?php

namespace Modules\Campaign\DataTable\Cards;

use App\DataTable\Cards\Card;
use Carbon\Carbon;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;
use Modules\Campaign\DataTable\Repositories\Criteria\CurrentMonthCriteria;

class CurrentMonthSalesCard extends Card
{
    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var \Modules\Campaign\DataTable\Repositories\CampaignsDataTableRepository
     */
    private $repository;

    /**
     * CurrentMonthSalesCard constructor.
     *
     * @param CampaignsDataTableRepository $repository
     */
    public function __construct(CampaignsDataTableRepository $repository)
    {
        $now = Carbon::now();
        $this->title = __('months.' . strtolower($now->format('F'))) . ' ' . __('campaign::labels.sales');
        $this->repository = $repository;
    }

    /**
     * Collect data
     *
     * @return float
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new CurrentMonthCriteria())
            ->sum('budget');
    }
}
