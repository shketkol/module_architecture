<?php

namespace Modules\Campaign\DataTable\Cards;

use App\DataTable\Cards\Card;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;
use Modules\Campaign\DataTable\Repositories\Criteria\UserCriteria;

class TotalCampaignsCard extends Card
{
    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var CampaignsDataTableRepository
     */
    private $repository;

    /**
     * TotalCampaignsCard constructor.
     *
     * @param CampaignsDataTableRepository $repository
     */
    public function __construct(CampaignsDataTableRepository $repository)
    {
        $this->title = trans('campaign::labels.campaign.cards.total_campaigns');
        $this->repository = $repository;
    }

    /**
     * Collect data
     *
     * @return float
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new UserCriteria($this->getAdvertiser()))
            ->campaigns()
            ->count('campaigns.id');
    }
}
