<?php

namespace Modules\Campaign\DataTable\Cards;

use Modules\Campaign\DataTable\Repositories\Criteria\ActiveCampaignsCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\UserCriteria;

class ActiveAdvertiserCampaignsCard extends ActiveCampaignsCard
{
    /**
     * Collect data
     *
     * @return float
     */
    public function collect(): float
    {
        return $this->repository
            ->pushCriteria(new UserCriteria($this->getAdvertiser()))
            ->pushCriteria(new ActiveCampaignsCriteria())
            ->campaigns()
            ->count('campaigns.id');
    }
}
