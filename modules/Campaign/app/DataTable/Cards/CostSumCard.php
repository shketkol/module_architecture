<?php

namespace Modules\Campaign\DataTable\Cards;

use App\DataTable\Cards\Card;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;

class CostSumCard extends Card
{
    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var \Modules\Campaign\DataTable\Repositories\CampaignsDataTableRepository
     */
    private $repository;

    /**
     * CostSumCard constructor.
     *
     * @param \Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository $repository
     */
    public function __construct(CampaignsDataTableRepository $repository)
    {
        $this->title = trans('campaign::labels.campaign.cards.ad_cost');
        $this->repository = $repository;
    }

    /**
     * Collect data
     *
     * @return float
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function collect(): float
    {
        return $this->repository->sum('budget');
    }
}
