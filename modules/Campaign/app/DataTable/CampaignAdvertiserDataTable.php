<?php

namespace Modules\Campaign\DataTable;

use App\DataTable\Cards\PlaceholderCard;
use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\DataTable\Repositories\Contracts\CampaignsDataTableRepository;
use Modules\Campaign\DataTable\Repositories\Criteria\AddCampaignStatusCriteria;
use Modules\Campaign\DataTable\Repositories\Criteria\UserCriteria;
use Modules\Campaign\DataTable\Transformers\CampaignsTransformer;
use Modules\Campaign\DataTable\Repositories\Criteria\AddCreativesCriteria;
use Modules\Creative\DataTable\Repositories\Criteria\AddCreativeStatusCriteria;

/**
 * Class CampaignAdvertiserDataTable
 *
 * @package App\Modules\Campaign\DataTable
 */
class CampaignAdvertiserDataTable extends CampaignDataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'advertiser-campaigns';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = CampaignsTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = CampaignsDataTableRepository::class;

    /**
     * @var string
     */
    protected $sortNullableLastColumn = 'order_id';

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [
        PlaceholderCard::class,
        PlaceholderCard::class,
        PlaceholderCard::class,
        PlaceholderCard::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var CampaignsDataTableRepository $repository */
        $repository = $this->getRepository();

        return $this->applySortCriteria($repository)
            ->pushCriteria(new UserCriteria($this->getAdvertiser()))
            ->pushCriteria(new AddCampaignStatusCriteria())
            ->pushCriteria(new AddCreativesCriteria())
            ->pushCriteria(new AddCreativeStatusCriteria())
            ->campaigns();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('campaign.listAdvertiserCampaigns', $this->getAdvertiser());
    }
}
