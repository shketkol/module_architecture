<?php

namespace Modules\Campaign\DataTable\Filters;

use App\DataTable\Filters\DataTableFilter;
use Illuminate\Database\Eloquent\Builder;
use Modules\Campaign\Models\CampaignStatus;
use Yajra\DataTables\DataTableAbstract;

class CampaignStatusFilter extends DataTableFilter
{
    /**
     * Filter name
     *
     * @var string
     */
    public static $name = 'campaign-status';

    /**
     * DataTableFilter constructor.
     *
     * @param \Yajra\DataTables\DataTableAbstract $dataTable
     */
    public function filter(DataTableAbstract $dataTable): void
    {
        $this->makeFilter($dataTable, 'campaign_status', function (Builder $query, $values) {
            $query->orWhereIn('campaign_statuses.id', $values);
        });
    }

    /**
     * Get filter options
     *
     * @return array
     */
    public function options(): array
    {
        return CampaignStatus::all()
            ->pluck('name', 'id')
            ->mapWithKeys(function (string $value, int $key) {
                return [$key => __("campaign::labels.campaign.statuses.$value")];
            })
            ->toArray();
    }
}
