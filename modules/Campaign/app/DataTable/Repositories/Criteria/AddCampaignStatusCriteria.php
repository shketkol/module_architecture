<?php

namespace Modules\Campaign\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddCampaignStatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model
            ->addSelect('campaign_statuses.name as campaign_status')
            ->addSelect('campaign_statuses.name as live_status')
            ->leftJoin('campaign_statuses', 'campaigns.status_id', '=', 'campaign_statuses.id');
    }
}
