<?php

namespace Modules\Campaign\DataTable\Repositories\Criteria;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class CurrentMonthCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        $now = Carbon::now();
        return $model->orWhereBetween(
            'date_end',
            [$now->clone()->startOfMonth()->toISOString(), $now->clone()->endOfMonth()->toISOString()]
        );
    }
}
