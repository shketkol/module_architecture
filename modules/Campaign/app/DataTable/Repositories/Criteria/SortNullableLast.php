<?php

namespace Modules\Campaign\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class SortNullableLast implements CriteriaInterface
{
    /**
     * @var string
     */
    public $field;

    /**
     * @var string
     */
    public $order;

    /**
     * @var string[]
     */
    private $allowedFields = ['order_id'];

    /**
     * @var string[]
     */
    private $allowedOrders = ['asc', 'desc'];

    /**
     * @param string $field
     * @param string $order
     */
    public function __construct(string $field, string $order)
    {
        $this->field = in_array($field, $this->allowedFields) ? $field : 'NULL';
        $this->order = in_array($order, $this->allowedOrders) ? $order : 'asc';
    }

    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface                         $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->orderByRaw($this->field . ' IS NULL ' . $this->order . ', ' . $this->field . ' ' . $this->order)
            ->orderBy('id', $this->order);
    }
}
