<?php

namespace Modules\Campaign\DataTable\Repositories\Criteria;

use Modules\Campaign\Models\CampaignStatus;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithoutDraftCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Prettus\Repository\Contracts\RepositoryInterface $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('campaigns.status_id', '!=', CampaignStatus::ID_DRAFT)
            ->whereNotNull('campaigns.external_id');
    }
}
