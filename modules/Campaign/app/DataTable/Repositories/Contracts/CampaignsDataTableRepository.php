<?php

namespace Modules\Campaign\DataTable\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Illuminate\Database\Eloquent\Builder;

interface CampaignsDataTableRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string;

    /**
     * Get user campaigns
     *
     * @return Builder
     */
    public function campaigns(): Builder;

    /**
     * Required actions query
     *
     * @return mixed
     */
    public function requiredActions(): Builder;
}
