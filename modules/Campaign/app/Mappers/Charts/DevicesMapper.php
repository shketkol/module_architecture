<?php

namespace Modules\Campaign\Mappers\Charts;

use Illuminate\Support\Arr;

class DevicesMapper
{
    /**
     * @param array $item
     * @return array
     */
    public static function map(array $item): array
    {
        $map = config('campaign.charts.devices_config');
        $rawKey = strtolower(preg_replace('/[^a-z]/i', '_', Arr::get($item, 'name')));
        $key = self::getDeviceKey($rawKey);

        if (empty($map[$key])) {
            return [];
        }
        $mapItem = $map[$key];

        return [
            'key'        => $key,
            'label'      => __('campaign::labels.performance.charts.devices.' . $rawKey),
            'color'      => Arr::get($mapItem, 'color'),
            'image'      => Arr::get($mapItem, 'image'),
            'percentage' => Arr::get($item, 'percentage'),
        ];
    }

    /**
     * @param string $deviceName
     *
     * @return string
     */
    public static function getDeviceKey(string $deviceName): string
    {
        return config('campaign.charts.devices_map.' . $deviceName, $deviceName);
    }
}
