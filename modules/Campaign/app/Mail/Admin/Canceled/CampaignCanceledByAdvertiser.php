<?php

namespace Modules\Campaign\Mail\Admin\Canceled;

use App\Mail\Mail;

class CampaignCanceledByAdvertiser extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.admin.canceled_by_advertiser.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('campaign::emails.admin.canceled-by-advertiser')
            ->with($this->payload);
    }
}
