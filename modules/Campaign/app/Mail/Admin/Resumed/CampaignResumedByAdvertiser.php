<?php

namespace Modules\Campaign\Mail\Admin\Resumed;

use App\Mail\Mail;

class CampaignResumedByAdvertiser extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.admin.resumed_by_advertiser.subject'))
            ->view('campaign::emails.admin.resumed-by-advertiser')
            ->with($this->payload);
    }
}
