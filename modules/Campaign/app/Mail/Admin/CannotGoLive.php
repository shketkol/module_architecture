<?php

namespace Modules\Campaign\Mail\Admin;

use App\Mail\Mail;

class CannotGoLive extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.admin.cannot_go_live.subject', [
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('campaign::emails.admin.cannot-go-live')
            ->with($this->payload);
    }
}
