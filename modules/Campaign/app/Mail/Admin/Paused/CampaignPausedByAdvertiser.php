<?php

namespace Modules\Campaign\Mail\Admin\Paused;

use App\Mail\Mail;

class CampaignPausedByAdvertiser extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.admin.paused_by_advertiser.subject'))
            ->view('campaign::emails.admin.paused-by-advertiser')
            ->with($this->payload);
    }
}
