<?php

namespace Modules\Campaign\Mail\Advertiser\Paused;

use App\Mail\Mail;

class CampaignPausedByAdmin extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.paused_by_admin.subject', [
                'publisher_company_name' => config('general.company_name')
            ]))
            ->view('campaign::emails.advertiser.paused-by-admin')
            ->with($this->payload);
    }
}
