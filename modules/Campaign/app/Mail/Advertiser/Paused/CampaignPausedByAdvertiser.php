<?php

namespace Modules\Campaign\Mail\Advertiser\Paused;

use App\Mail\Mail;

class CampaignPausedByAdvertiser extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->subject(__('campaign::emails.advertiser.paused_by_advertiser.subject'))
            ->view('campaign::emails.advertiser.paused-by-advertiser')
            ->with($this->payload);
    }
}
