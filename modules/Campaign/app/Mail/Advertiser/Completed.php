<?php

namespace Modules\Campaign\Mail\Advertiser;

use App\Mail\Mail;

class Completed extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.completed.subject'))
            ->view('campaign::emails.advertiser.completed')
            ->with($this->payload);
    }
}
