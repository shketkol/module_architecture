<?php

namespace Modules\Campaign\Mail\Advertiser\EditConfirmation;

use App\Mail\Mail;

class Failed extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.edit_confirmation.failed.subject'))
            ->view('campaign::emails.advertiser.edit-confirmation-failed')
            ->with($this->payload);
    }
}
