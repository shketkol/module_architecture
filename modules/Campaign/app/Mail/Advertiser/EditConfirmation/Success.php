<?php

namespace Modules\Campaign\Mail\Advertiser\EditConfirmation;

use App\Mail\Mail;

class Success extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.edit_confirmation.success.subject'))
            ->view('campaign::emails.advertiser.edit-confirmation-success')
            ->with($this->payload);
    }
}
