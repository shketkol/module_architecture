<?php

namespace Modules\Campaign\Mail\Advertiser\Canceled;

use App\Mail\Mail;

class CampaignCanceledByAdmin extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.canceled_by_admin.subject', [
                'publisher_company_name' => config('general.company_name')
            ]))
            ->view('campaign::emails.advertiser.canceled-by-admin')
            ->with($this->payload);
    }
}
