<?php

namespace Modules\Campaign\Mail\Advertiser;

use App\Mail\Mail;

class CannotGoLive extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('campaign::emails.advertiser.cannot_go_live.subject'))
            ->view('campaign::emails.advertiser.cannot-go-live')
            ->with($this->payload);
    }
}
