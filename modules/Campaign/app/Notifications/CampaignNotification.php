<?php

namespace Modules\Campaign\Notifications;

use App\Notifications\Notification;
use Modules\Campaign\Models\Campaign;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Models\User;

abstract class CampaignNotification extends Notification
{
    /**
     * @var User|null
     */
    protected $actor;

    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * CampaignNotification constructor.
     *
     * @param Campaign  $campaign
     * @param User|null $actor
     */
    public function __construct(Campaign $campaign, ?User $actor = null)
    {
        $this->actor = $actor;
        $this->campaign = $campaign;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_CAMPAIGN;
    }

    /**
     * @return string
     */
    public function getDetailsUrl(): string
    {
        return route('campaigns.show', ['campaign' => $this->campaign->id]);
    }

    /**
     * @return string
     */
    public function getCompanyDetailsUrl(): string
    {
        return route('advertisers.show', ['advertiser' => $this->campaign->user_id]);
    }
}
