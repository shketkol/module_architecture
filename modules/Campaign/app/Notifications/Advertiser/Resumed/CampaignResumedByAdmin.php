<?php

namespace Modules\Campaign\Notifications\Advertiser\Resumed;

use App\Helpers\HtmlHelper;
use App\Notifications\Traits\AdvertiserNotification;
use Illuminate\Support\Arr;
use Modules\Campaign\Mail\Advertiser\Resumed\CampaignResumedByAdmin as Mail;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

class CampaignResumedByAdmin extends CampaignNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'campaignName'    => $this->campaign->name,
            'campaignDetails' => $this->getDetailsUrl(),
            'title'           => __('campaign::emails.advertiser.resumed_by_admin.title', [
                'publisher_company_name' => config('general.company_name')
            ]),
            'titleIcon'       => 'play',
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('campaign::notifications.advertiser.resumed_by_admin', [
            'campaign_name' => $html->createAnchorElement(Arr::get($data, 'campaignDetails'), [
                'title' => Arr::get($data, 'campaignName'),
            ]),
        ]);
    }
}
