<?php

namespace Modules\Campaign\Notifications\Advertiser\Ordered;

use App\Notifications\Traits\AdvertiserNotification;
use Modules\Campaign\Mail\Advertiser\Ordered\Ordered as Mail;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

class Ordered extends CampaignNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'first_name'       => $notifiable->first_name,
            'campaign_details' => $this->getDetailsUrl(),
            'campaign_name'    => $this->campaign->name,
            'title'            => __('campaign::emails.advertiser.ordered.title'),
            'titleIcon'        => 'mail',
        ];
    }
}
