<?php

namespace Modules\Campaign\Notifications\Advertiser\Canceled;

use App\Notifications\Traits\AdvertiserNotification;
use Modules\Campaign\Mail\Advertiser\Canceled\CampaignCanceledByAdvertiser as Mail;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

class CampaignCanceledByAdvertiser extends CampaignNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'campaignName'    => $this->campaign->name,
            'campaignDetails' => $this->getDetailsUrl(),
            'title'           => __('campaign::emails.advertiser.canceled_by_advertiser.title', [
                'publisher_company_name' => config('general.company_name'),
                'campaign_name'          => $this->campaign->name,
            ]),
            'titleIcon'       => 'cancel_window',
            'bookNewCampaign' => route('campaigns.create'),
        ];
    }
}
