<?php

namespace Modules\Campaign\Notifications\Advertiser;

use App\Helpers\HtmlHelper;
use App\Notifications\Traits\AdvertiserNotification;
use Illuminate\Support\Arr;
use Modules\Campaign\Mail\Advertiser\Started as Mail;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

class Started extends CampaignNotification
{
    use AdvertiserNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'campaignName'    => $this->campaign->name,
            'campaignDetails' => $this->getDetailsUrl(),
            'title'           => __('campaign::emails.advertiser.started.title'),
            'titleIcon'       => 'marked',
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        /** @var HtmlHelper $html */
        $html = app(HtmlHelper::class);

        return __('campaign::notifications.advertiser.started', [
            'campaign_name' => $html->createAnchorElement(Arr::get($data, 'campaignDetails'), [
                'title' => Arr::get($data, 'campaignName'),
            ]),
        ]);
    }
}
