<?php

namespace Modules\Campaign\Notifications\Admin\Ordered;

use App\Notifications\Traits\AdminNotification;
use Modules\Campaign\Mail\Admin\Ordered\Ordered as Mail;
use Modules\Campaign\Notifications\CampaignNotification;
use Modules\User\Models\User;

class Ordered extends CampaignNotification
{
    use AdminNotification;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'first_name'       => $notifiable->first_name,
            'company'          => $this->campaign->user->company_name,
            'campaign_details' => $this->getDetailsUrl(),
            'creative_status'  => $this->campaign->creative_status_name,
        ];
    }
}
