<?php

namespace Modules\Campaign\Notifications\Admin\Canceled;

use Modules\Campaign\Notifications\Admin\CampaignStatusChangedNotification;
use Modules\Campaign\Mail\Admin\Canceled\CampaignCanceledByAdvertiser as Mail;
use Modules\User\Models\User;

class CampaignCanceledByAdvertiser extends CampaignStatusChangedNotification
{
    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'companyName'     => $this->actor->company_name,
            'campaignName'    => $this->campaign->name,
            'campaignDetails' => $this->getDetailsUrl(),
            'companyDetails'  => $this->getCompanyDetailsUrl(),
        ];
    }
}
