<?php

namespace Modules\Campaign\Listeners;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Notifications\Admin\Canceled\CampaignCanceledByAdvertiser as AdminNotification;
use Modules\Campaign\Notifications\Advertiser\Canceled\CampaignCanceledByAdmin;
use Modules\Campaign\Notifications\Advertiser\Canceled\CampaignCanceledByAdvertiser;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\Daapi\Events\Campaign\CampaignCanceled;
use Modules\User\Repositories\Contracts\UserRepository;
use Psr\Log\LoggerInterface;

class CampaignCanceledListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var CampaignRepository
     */
    private $repository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @param LoggerInterface    $log
     * @param CampaignRepository $repository
     * @param UserRepository     $userRepository
     * @param Dispatcher         $notification
     */
    public function __construct(
        LoggerInterface $log,
        CampaignRepository $repository,
        UserRepository $userRepository,
        Dispatcher $notification
    ) {
        $this->log = $log;
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param CampaignCanceled $event
     *
     * @throws \Throwable
     */
    public function handle(CampaignCanceled $event): void
    {
        $externalId = $event->getCampaignId();
        $this->log->info('Started update campaign at cancel callback.', [
            'external_id' => $externalId,
        ]);

        try {
            /** @var Campaign $campaign */
            $campaign = $this->repository->findCampaignByExternalId($externalId);
            $campaign->applyInternalStatus(CampaignStatus::CANCELED);

            $this->sendNotification($campaign);
        } catch (\Throwable $exception) {
            $this->log->warning('Failed update campaign at cancel callback.', [
                'external_id' => $externalId,
                'reason'      => $exception->getMessage(),
            ]);

            throw $exception;
        }

        $this->log->info('Finished update campaign at cancel callback.', [
            'external_id' => $externalId,
            'campaign_id' => $campaign->id,
        ]);
    }

    /**
     * Send email and on-site notifications.
     *
     * @param Campaign $campaign
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Modules\User\Exceptions\UserNotFoundException
     */
    private function sendNotification(Campaign $campaign): void
    {
        $admin = $this->userRepository->getAdmin();
        $user = $this->userRepository->getById($campaign->status_changed_by);

        // Advertiser has canceled the campaign
        if ($user->isAdvertiser()) {
            $this->notification->send($user, new CampaignCanceledByAdvertiser($campaign, $user));
            $this->notification->send($admin, new AdminNotification($campaign, $user));

            return;
        }

        $this->notification->send($campaign->user, new CampaignCanceledByAdmin($campaign, $user));
    }
}
