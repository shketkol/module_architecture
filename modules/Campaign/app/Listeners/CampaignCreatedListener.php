<?php

namespace Modules\Campaign\Listeners;

use App\Exceptions\ModelNotUpdatedException;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Alerts\SetCampaignAlertsAction;
use Modules\Campaign\Exceptions\DuplicatedCallbackException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\Creative\Models\Creative;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Repositories\Contracts\CreativeRepository;
use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Modules\Daapi\DataTransferObjects\Types\CreativeData;
use Modules\Daapi\Events\Campaign\CampaignCreated;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;
use Modules\Creative\Actions\Notifications\UpdateCreativeStatusAction;
use Modules\Promocode\Repositories\PromocodeRepository;
use Psr\Log\LoggerInterface;

class CampaignCreatedListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @var CreativeRepository
     */
    private $creativeRepository;

    /**
     * @var PromocodeRepository
     */
    private $promocodeRepository;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var UpdateCreativeStatusAction
     */
    private $updateCreativeAction;

    /**
     * @var SetCampaignAlertsAction
     */
    protected $setCampaignAlertsAction;

    /**
     * @param LoggerInterface $log
     * @param CampaignRepository $campaignRepository
     * @param CreativeRepository $creativeRepository
     * @param DatabaseManager $databaseManager
     * @param UpdateCreativeStatusAction $updateCreativeAction
     * @param SetCampaignAlertsAction $setCampaignAlertsAction
     * @param PromocodeRepository $promocodeRepository
     */
    public function __construct(
        LoggerInterface $log,
        CampaignRepository $campaignRepository,
        CreativeRepository $creativeRepository,
        DatabaseManager $databaseManager,
        UpdateCreativeStatusAction $updateCreativeAction,
        SetCampaignAlertsAction $setCampaignAlertsAction,
        PromocodeRepository $promocodeRepository
    ) {
        $this->log = $log;
        $this->campaignRepository = $campaignRepository;
        $this->creativeRepository = $creativeRepository;
        $this->databaseManager = $databaseManager;
        $this->updateCreativeAction = $updateCreativeAction;
        $this->setCampaignAlertsAction = $setCampaignAlertsAction;
        $this->promocodeRepository = $promocodeRepository;
    }

    /**
     * Handle the event.
     *
     * @param CampaignCreated $event
     *
     * @return void
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(CampaignCreated $event): void
    {
        $data = $event->getCampaign();
        $this->log->info('Started update campaign at create callback.', [
            'campaign_id' => $data->sourceId,
            'external_id' => $data->id,
            'order_id'    => $data->operativeOrderId,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $campaign = $this->handleCampaign($data);
            $this->handleCreative($campaign, $data);

            $campaign->order_id = $data->operativeOrderId;
            $campaign->save();
        } catch (\Throwable $exception) {
            $this->log->warning('Failed update campaign at create callback.', [
                'campaign_id' => $data->sourceId,
                'external_id' => $data->id,
                'order_id'    => $data->operativeOrderId,
                'reason'      => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();

            if ($exception instanceof DuplicatedCallbackException) {
                throw $exception;
            }

            throw ModelNotUpdatedException::createFrom($exception);
        }

        $this->databaseManager->commit();

        $this->log->info('Finished update campaign at create callback.', [
            'campaign_id' => $data->sourceId,
            'external_id' => $data->id,
            'order_id'    => $data->operativeOrderId,
        ]);
    }

    /**
     * @param Campaign $campaign
     * @param CampaignData $data
     *
     * @throws CanNotApplyStatusException
     * @throws \SM\SMException
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function handleCreative(Campaign $campaign, CampaignData $data): void
    {
        $creativeData =  $this->getCreativeData($data);

        if (is_null($creativeData)) {
            $this->log->info('Creative update ignored. No creative data in callback.', [
                'campaign_id' => $campaign->id,
            ]);
            return;
        }

        $creative = $campaign->getActiveCreative();

        if (is_null($creative)) {
            $this->log->warning('Creative update failed. Campaign has no creative, but callback has one.', [
                'campaign_id'          => $campaign->id,
                'creative_external_id' => $creativeData->id,
            ]);

            return;
        }

        $this->creativeRepository->update(['external_id' => $creativeData->id], $creative->id);

        $this->updateCreativeAction->handle(
            $creativeData->id,
            $creativeData->status,
            ['comments' => $creativeData->comment, 'reason' => $creativeData->reason]
        );

        $this->log->info('Creative update succeed.', [
            'campaign_id'          => $campaign->id,
            'creative_id'          => $creative->id,
            'creative_external_id' => $creativeData->id,
        ]);
    }

    /**
     * Update campaign
     * @param Campaign     $campaign
     * @param CampaignData $data
     */
    private function updateCampaign(Campaign $campaign, CampaignData $data): void
    {
        $attributes = [
            'external_id'     => $data->id,
            'order_id'        => $data->operativeOrderId,
            'budget'          => $data->displayBudget,
            'cost'            => $data->displayBudget,
            'discounted_cost' => $data->displayDiscountedBudget,
        ];

        $this->campaignRepository->update($attributes, $campaign->id);
    }

    /**
     * Update promodate
     * @param CampaignData $data
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function updatePromocode(CampaignData $data): void
    {
        if (!Arr::has($data->promoCode, 'promoCodeName')) {
            return;
        }

        $promocodeName = Arr::get($data->promoCode, 'promoCodeName');
        $promocode = $this->promocodeRepository->byCode($promocodeName)->first();

        if (!$promocode || !empty($promocode->external_id)) {
            return;
        }

        $attributes = [
            'external_id' => Arr::get($data->promoCode, 'promoCodeExternalId'),
        ];

        $this->promocodeRepository->update($attributes, $promocode->id);
    }

    /**
     * @param CampaignData $data
     *
     * @return Campaign
     * @throws CanNotApplyStatusException
     * @throws DuplicatedCallbackException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \SM\SMException
     */
    private function handleCampaign(CampaignData $data): Campaign
    {
        $this->log->info('Campaign update started.', [
            'campaign_id' => $data->sourceId,
        ]);

        /** @var Campaign $campaign */
        $campaign = $this->campaignRepository->find($data->sourceId);

        $this->checkCallbackDuplicate($campaign);
        $campaign->applyHaapiStatus($data->status);
        $this->updateCampaign($campaign, $data);
        $this->updatePromocode($data);

        $receivedRejected = $this->isCreativeReceivedRejected($data);
        $this->setCampaignAlertsAction->handle($campaign->refresh(), false, $receivedRejected);

        $this->log->info('Campaign update finished.', [
            'campaign_id' => $campaign->id,
            'external_id' => $data->id,
        ]);

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws DuplicatedCallbackException
     */
    private function checkCallbackDuplicate(Campaign $campaign)
    {
        if (!$campaign->inState([CampaignStatus::ID_DRAFT, CampaignStatus::ID_PROCESSING])) {
            throw new DuplicatedCallbackException('Duplicated callback "campaign/create"');
        }
    }

    /**
     * @param CampaignData $data
     *
     * @return CreativeData|null
     */
    private function getCreativeData(CampaignData $data)
    {
        $item = Arr::first($data->lineItems);

        return $item->creative;
    }

    /**
     * @param CampaignData $data
     *
     * @return bool
     */
    private function isCreativeReceivedRejected(CampaignData $data): bool
    {
        if (!$creativeData = $this->getCreativeData($data)) {
            return false;
        }

        $creativeStatus = Creative::getStatusIdByTransactionFlow($creativeData->status, 'creative');

        return $creativeStatus === CreativeStatus::ID_REJECTED;
    }
}
