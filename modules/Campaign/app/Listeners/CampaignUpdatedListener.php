<?php

namespace Modules\Campaign\Listeners;

use App\Exceptions\InvalidArgumentException;
use App\Exceptions\ModelNotUpdatedException;
use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Database\DatabaseManager;
use Modules\Campaign\Actions\ApplyRevisionToCampaignAction;
use Modules\Campaign\Actions\RevertCampaignUpdateAttemptAction;
use Modules\Campaign\Exceptions\DuplicatedCallbackException;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Notifications\Advertiser\EditConfirmation\Failed;
use Modules\Campaign\Notifications\Advertiser\EditConfirmation\Success;
use Modules\Campaign\Repositories\CampaignRevisionRepository;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\Daapi\Events\Campaign\CampaignUpdated;
use Psr\Log\LoggerInterface;

class CampaignUpdatedListener
{
    protected const STATUS_SUCCESS = 'SUCCESS';
    protected const STATUS_REVERTED = 'REVERTED';
    protected const STATUS_PARTIAL_SUCCESS = 'PARTIAL_SUCCESS';

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $log;

    /**
     * @var CampaignRepository
     */
    protected CampaignRepository $campaignRepository;

    /**
     * @var CampaignRevisionRepository
     */
    protected CampaignRevisionRepository $revisionRepository;

    /**
     * @var DatabaseManager
     */
    protected DatabaseManager $databaseManager;

    /**
     * @var ApplyRevisionToCampaignAction
     */
    protected ApplyRevisionToCampaignAction $applyRevisionAction;

    /**
     * @var RevertCampaignUpdateAttemptAction
     */
    protected RevertCampaignUpdateAttemptAction $revertAction;

    /**
     * @var Dispatcher
     */
    protected Dispatcher $notification;

    /**
     * @param LoggerInterface               $log
     * @param CampaignRepository            $campaignRepository
     * @param CampaignRevisionRepository    $revisionRepository
     * @param DatabaseManager               $databaseManager
     * @param ApplyRevisionToCampaignAction $applyRevisionAction
     * @param Dispatcher                    $notification
     */
    public function __construct(
        LoggerInterface $log,
        CampaignRepository $campaignRepository,
        CampaignRevisionRepository $revisionRepository,
        DatabaseManager $databaseManager,
        ApplyRevisionToCampaignAction $applyRevisionAction,
        RevertCampaignUpdateAttemptAction $revertAction,
        Dispatcher $notification
    ) {
        $this->log = $log;
        $this->campaignRepository = $campaignRepository;
        $this->revisionRepository = $revisionRepository;
        $this->databaseManager = $databaseManager;
        $this->applyRevisionAction = $applyRevisionAction;
        $this->revertAction = $revertAction;
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param CampaignUpdated $event
     *
     * @return void
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(CampaignUpdated $event): void
    {
        $data = $event->getCampaign();
        $this->log->info('[Update Campaign Callback] Started precessing callback.', [
            'campaign_id' => $data->sourceId,
            'external_id' => $data->id,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $campaign = $this->campaignRepository->find($data->sourceId);

            $this->checkCallbackDuplicate($campaign);
            $this->checkUpdateStatus($event->getUpdateStatus());

            $this->processFlow($campaign, $event->getUpdateStatus());
        } catch (\Throwable $exception) {
            $this->log->warning('[Update Campaign Callback] Failed precessing callback.', [
                'reason'      => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();

            if ($exception instanceof DuplicatedCallbackException) {
                throw $exception;
            }

            throw ModelNotUpdatedException::createFrom($exception);
        }

        $this->databaseManager->commit();

        $this->log->info('[Update Campaign Callback] Finished processing callback.');
    }

    /**
     * @param string $status
     *
     * @return void
     * @throws InvalidArgumentException
     */
    protected function checkUpdateStatus(string $status): void
    {
        if (!in_array($status, [self::STATUS_SUCCESS, self::STATUS_REVERTED, self::STATUS_PARTIAL_SUCCESS])) {
            throw new InvalidArgumentException("Bad updateStatus value: '${$status}'");
        }
    }

    /**
     * @param Campaign $campaign
     * @param string   $status
     *
     * @return void
     * @throws \Exception
     */
    protected function processFlow(Campaign $campaign, string $status): void
    {
        switch ($status) {
            case self::STATUS_SUCCESS:
                $this->processSuccessFlow($campaign);
                break;
            case self::STATUS_REVERTED:
                $this->processRevertedFlow($campaign);
                break;
            default:
                $this->processPartialSuccessFlow();
        }
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function processSuccessFlow(Campaign $campaign): void
    {
        $revision = $campaign->getProcessingRevision();
        $this->applyRevisionAction->handle($campaign, $revision);

        // Send notification to advertiser
        $this->notification->send(
            $campaign->user,
            new Success($campaign)
        );
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws \Exception
     */
    protected function processRevertedFlow(Campaign $campaign): void
    {
        $this->revertAction->handle($campaign);

        // Send notification to advertiser
        $this->notification->send(
            $campaign->user,
            new Failed($campaign)
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function processPartialSuccessFlow(): void
    {
        $this->log->info('[Update Campaign Callback] Received PARTIAL_SUCCESS. Do nothing.');
    }

    /**
     * @param Campaign $campaign
     *
     * @return void
     * @throws DuplicatedCallbackException
     */
    private function checkCallbackDuplicate(Campaign $campaign): void
    {
        if (!$campaign->inState([CampaignStatus::ID_PROCESSING])) {
            throw new DuplicatedCallbackException('Duplicated callback: campaign/update');
        }
    }
}
