<?php

Route::group([
    'middleware' => 'auth:web,admin',
], function () {
    // Store
    Route::post('/', 'StoreCampaignController')->name('store')->middleware('can:campaign.create');

    // Specified resource
    Route::group([
        'prefix' => '/{campaign}',
        'where'  => [
            'campaign' => '[0-9]+',
        ],
    ], function () {
        // Details
        Route::group(['prefix' => '/details', 'as' => 'details.'], function () {
            Route::group(['middleware' => 'can:campaign.view,campaign'], function () {
                Route::get('', 'CampaignDetailsController')->name('show');
                Route::get('statistic', 'CampaignStatisticController')->name('statistic.show');
            });

            Route::patch('/name', 'UpdateCampaignNameController')
                ->name('name.update')
                ->middleware('can:campaign.update,campaign');
            Route::patch('/schedule', 'UpdateCampaignScheduleController')
                ->name('schedule.update')
                ->middleware('can:campaign.update,campaign');
        });

        // Metrics
        Route::group(['prefix' => '/metrics', 'as' => 'metrics.'], function () {
            Route::group(['middleware' => 'can:campaign.view,campaign'], function () {
                Route::get('genres', 'CampaignMetricsByGenreController')->name('genres');
                Route::get('devices', 'CampaignMetricsByDeviceController')->name('devices');
            });
        });

        // Frequency
        Route::get('frequency', 'CampaignFrequencyController')
            ->name('frequency')
            ->middleware('can:campaign.view,campaign');

        // Permissions
        Route::get('/permissions', 'GetCampaignPermissionsController')
            ->name('permissions')
            ->middleware('can:campaign.create,campaign');

        // Creative
        Route::group(['prefix' => '/creative', 'as' => 'creative.'], function () {
            Route::patch('/', 'UpdateCampaignCreativeController')
                ->name('update')
                ->middleware('can:campaign.manageCampaignCreative,campaign');
            Route::patch('/replace', 'ReplaceCampaignCreativeController')
                ->name('replace')
                ->middleware('can:campaign.replaceCampaignCreative,campaign');
            Route::patch('/step', 'UpdateCampaignCreativeStepController')
                ->name('step.update')
                ->middleware('can:campaign.update,campaign');
        });

        // Targeting
        Route::group([
            'prefix'     => '/targeting',
            'as'         => 'targeting.',
            'namespace'  => 'Targeting',
            'middleware' => 'can:campaign.update,campaign',
        ], function () {
            // show
            Route::get('/ages', 'IndexAgeGroupsController')->name('ages');
            Route::get('/locations', 'SearchLocationsController')->name('locations');
            Route::post('/zipcodes', 'SearchZipcodesController')
                ->name('zipcodes')
                ->middleware('can:campaign.editZipTargetings,campaign');
            Route::post('/csv', 'UploadCsvController')->name('upload.csv');
            Route::get('/audiences', 'IndexAudiencesController')
                ->name('audiences')
                ->middleware('can:campaign.editAudienceTargetings,campaign');
            Route::get('/genres', 'SearchGenresController')
                ->name('genres')
                ->middleware('can:campaign.editGenreTargetings,campaign');
            Route::get('/devices', 'IndexDevicesController')->name('devices');

            // update
            Route::patch('/ages', 'UpdateCampaignAgeGroupsController')->name('ages.update');
            Route::patch('/locations', 'UpdateCampaignLocationsController')->name('locations.update');
            Route::patch('/genres', 'UpdateCampaignGenresController')->name('genres.update');
            Route::patch('/devices', 'UpdateCampaignDevicesController')->name('devices.update');
            Route::patch('/audiences', 'UpdateCampaignAudiencesController')->name('audiences.update');

            // unattach non-visible targetings
            Route::patch('/update-non-visible', 'UnattachCampaignNonVisibleTargetingController')
                ->name('nonVisible.update');
        });

        // Budget
        Route::group([
            'prefix'     => '/budget',
            'as'         => 'budget.',
            'middleware' => 'can:campaign.update,campaign',
        ], function () {
            Route::get('/cpm', 'ShowCpmController')->name('cpm.show');
            Route::patch('/', 'UpdateCampaignBudgetController')->name('update');
        });

        // Final Inventory Check before the campaign order
        Route::get('/inventory-check', 'GetCampaignInventoryCheckController')
            ->name('inventoryCheck.get')
            ->middleware('can:campaign.view,campaign');

        // Dynamic Inventory Check between the campaign wizard steps
        Route::get('/dynamic-inventory-check', 'GetDynamicInventoryCheckController')
            ->name('dynamicInventoryCheck.get')
            ->middleware('can:campaign.view,campaign');

        // Billing Date
        Route::get('/billing-date', 'GetCampaignBillingDateController')
            ->name('billingDate.get')
            ->middleware('can:campaign.view,campaign');

        // Validation
        Route::get('/validate', 'ValidateCampaignController')
            ->name('validate')
            ->middleware('can:campaign.view,campaign');

        // Order Campaign
        Route::post('/order', 'OrderCampaignController')
             ->name('order')
             ->middleware('can:campaign.order,campaign');

        // Update Campaign
        Route::post('/update', 'UpdateCampaignController')
             ->name('update')
             ->middleware('can:campaign.order,campaign');

        // Edit
        Route::get('/edit', 'EditCampaignController')->name('edit')->middleware('can:campaign.update,campaign');

        // Clone
        Route::get('/clone', 'CloneCampaignController')->name('clone')->middleware('can:campaign.clone,campaign');

        Route::delete('', 'DeleteCampaignController')->name('delete')->middleware('can:campaign.delete,campaign');

        // Status
        Route::group(['namespace' => 'Status'], function () {
            Route::patch('/cancel', 'CancelCampaignController')
                ->name('cancel')->middleware('can:campaign.cancel,campaign');
            Route::patch('/pause', 'PauseCampaignController')
                ->name('pause')->middleware('can:campaign.pause,campaign');
            Route::patch('/resume', 'ResumeCampaignController')
                ->name('resume')->middleware('can:campaign.resume,campaign');
        });

        // Promocode
        Route::group([
            'prefix'     => '/promocode',
            'as'         => 'promocode.',
            'middleware' => 'can:campaign.validatePromocode,campaign',
        ], function () {
            Route::post('/validate', '\Modules\Promocode\Http\Controllers\Api\PromocodeValidateController')
                ->name('validate');

            Route::post('/delete', 'DeletePromocodeController')
                ->name('delete');
        });

        // Report
        Route::group([
            'prefix'     => '/report',
            'as'         => 'report.',
            'middleware' => 'can:campaign.view,campaign',
        ], function () {
            Route::get('/download', 'DownloadCampaignReportController')
                ->name('download');
        });

        // Draft Activity
        Route::post('/activity', 'StoreDraftActivityCampaignController')
            ->name('activity')
            ->middleware('can:campaign.accessActivity,campaign');
    });

    // Revision
    Route::group([
        'prefix' => '/revisions/{revision}',
        'where'  => [
            'campaign' => '[0-9]+',
        ],
    ], function () {
        Route::get('', 'ShowRevisionController')
            ->name('revision.show')
            ->middleware('can:campaign.showRevision,revision');
    });

    Route::get('/campaigns-search-by-user', 'SearchCampaignsListController')
        ->name('search')
        ->middleware('can:campaign.list');
});
