@extends('common.email.layout')

@section('body')

    <p style="font-family: 'Graphik-regular',  Helvetica-Light, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">{{ __('emails.hi', ['name' => $firstName]) }}</p>

    <p style="font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.5; margin-top: 0; margin-bottom: 15px;">
        {{ __('advertiser::emails.advertiser.deactivated.body.your_publisher_account_suspended', [
            'publisher_company_full_name' => $publisherCompanyFullName,
            'company_name'                => $companyName,
        ]) }}
    </p>

    @include('common.email.part.if-you-have-any-questions')
@stop
