@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <index-page></index-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/resitrevda.js') }}"></script>
@endpush

