<?php

return [
    'get'          => [
        'failed' => 'Can\'t obtain user data.',
    ],
    'deactivate'   => [
        'success'                          => 'Account deactivated',
        'fail'                             => 'User ID #:user_id (:company_name) cannot be deactivated. Please, try again later.',
        'help'                             => 'Something\'s not right! It looks like you need to update your payment method. Please go to your account profile and update your credit card.',
        'has_live_campaigns'               => 'User you trying to deactivate has Live campaigns. Please, pause all campaigns and try again.',
        'has_active_campaigns'             => 'This Account has active Campaigns. Please pause or cancel them first.',
        'account_deactivated'              => 'It looks like your account has been deactivated, and you can no longer create campaigns. If you have any questions, please contact us at (:email)',
        'pending_status'                   => 'It looks like your account requires additional review and validation. For details, please fill out the Contact Us form.',
        'inactive_status'                  => 'It looks like your account is no longer active. For details, please fill out the Contact Us form.',
        'bad_payment_status'               => 'It looks like you need to update your payment method.  Please go to your account profile and update your credit card.',
        'bad_payment_status_pending'       => 'We\'ll update your transaction status within 24 hours after we process the payment.',
        'bad_payment_status_pending_title' => 'Thank you for updating your credit card!',
    ],
    'activate'     => [
        'success' => 'The Advertiser has been activated. You can now resume any of their paused campaigns.',
        'fail'    => 'User ID #:user_id cannot be activated. Please, try again later.',
    ],
    'account_type' => [
        'changed' => 'Account type changed to :account_type.',
        'failed'  => 'Failed to change Advertiser account type to :account_type.',
    ],
    'change_special_category_content' => 'Do you want to enable Special Ads Category for this Advertiser? Once it is enabled, a part of the campaign targeting will be disabled at the Advertiser’s booking flow. Disabled targeting steps will be re-set to “All” at the Advertiser’s Draft campaigns with no ability to see which options were selected previously.',
    'setting_limit'        => [
        'changed' => 'This advertiser’s monthly ad upload limit has been successfully changed, effective immediately.',
        'failed'  => 'Failed to change Advertiser limit.',
    ],

];
