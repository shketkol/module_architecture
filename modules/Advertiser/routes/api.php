<?php

Route::middleware(['auth:admin'])->group(function () {
    Route::get('/search', 'SearchAdvertiserController')->name('search')->middleware('can:advertiser.list');

    Route::group([
        'prefix' => '/{advertiser}',
        'where'  => ['advertiser' => '[0-9]+'],
    ], function () {
        Route::get('', 'ShowAdvertiserController')
            ->name('show')
            ->middleware('can:advertiser.view,advertiser');

        Route::post('/deactivate', 'DeactivateAdvertiserController')
            ->name('deactivate')
            ->middleware('can:advertiser.deactivate,advertiser');

        Route::post('/activate', 'ActivateAdvertiserController')
            ->name('activate')
            ->middleware('can:advertiser.activate,advertiser');

        // Account Type
        Route::post('/account-type', 'UpdateAccountTypeController')
            ->name('account-type')
            ->middleware('can:advertiser.updateAccountType,advertiser');

        Route::get('/settings', 'ShowSettingUserController')
            ->name('setting.show')
            ->middleware('can:setting.show,advertiser');
        Route::post('/settings', 'StoreUserSettingController')
            ->name('setting.store')
            ->middleware('can:setting.store,advertiser');
    });
});
