<?php

namespace Modules\Advertiser\Policies;

use App\Policies\Policy;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\User\Models\User;

class AdvertiserPolicy extends Policy
{
    use HandlesAuthorization;

    /**
     * Permissions.
     */
    public const PERMISSION_LIST = 'list_advertiser';
    public const PERMISSION_VIEW = 'view_advertiser';
    public const PERMISSION_ACTIVATE = 'activate_advertiser';
    public const PERMISSION_DEACTIVATE = 'deactivate_advertiser';
    public const PERMISSION_UPDATE_ACCOUNT_TYPE = 'update_account_type';

    /**
     * Model class.
     *
     * @var string
     */
    protected string $model = User::class;

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @param CampaignRepository $repository
     */
    public function __construct(CampaignRepository $repository)
    {
        $this->campaignRepository = $repository;
    }

    /**
     * Returns true if user can list advertiser.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST);
    }

    /**
     * Returns true if user can view advertiser.
     *
     * @param User $user
     * @param User $advertiser
     *
     * @return bool
     */
    public function view(User $user, User $advertiser): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_VIEW)) {
            return false;
        }

        return $advertiser->isAdvertiser();
    }

    /**
     * Returns true if user can activate advertiser.
     *
     * @param User $user
     * @param User $advertiser
     *
     * @return bool
     */
    public function activate(User $user, User $advertiser): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_ACTIVATE)) {
            return false;
        }

        if (!$advertiser->isAdvertiser()) {
            return false;
        }

        if (!$advertiser->isDeactivated()) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can deactivate advertiser.
     *
     * @param User $user
     * @param User $advertiser
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function deactivate(User $user, User $advertiser): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_DEACTIVATE)) {
            return false;
        }

        if (!$advertiser->isAdvertiser()) {
            return false;
        }

        if (!$advertiser->isActive()) {
            return false;
        }

        if (!$this->activeCampaigns($user, $advertiser)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can deactivate advertiser.
     *
     * @param User $user
     * @param User $advertiser
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function activeCampaigns(User $user, User $advertiser): bool
    {
        $countLive = $this->campaignRepository
            ->byStatus([
                CampaignStatus::ID_LIVE,
                CampaignStatus::ID_READY,
                CampaignStatus::ID_PROCESSING,
                CampaignStatus::ID_PENDING_APPROVAL,
            ])
            ->byUser($advertiser)
            ->count();

        if ($countLive) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update account type for the advertiser.
     *
     * @param User $user
     * @param User $advertiser
     *
     * @return bool
     */
    public function updateAccountType(User $user, User $advertiser): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_ACCOUNT_TYPE)) {
            return false;
        }

        if (!$advertiser->isAdvertiser()) {
            return false;
        }

        return true;
    }
}
