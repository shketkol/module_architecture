<?php

namespace Modules\Advertiser\Events;

use Illuminate\Queue\SerializesModels;
use Modules\User\Models\User;

class AdvertiserActivated
{
    use SerializesModels;

    /**
     * The activated advertiser.
     *
     * @var User
     */
    public $advertiser;

    /**
     * Admin who performs the activation.
     *
     * @var User
     */
    public $admin;

    /**
     * Create a new event instance.
     *
     * @param User $advertiser
     * @param User $admin
     */
    public function __construct(User $advertiser, User $admin)
    {
        $this->advertiser = $advertiser;
        $this->admin = $admin;
    }
}
