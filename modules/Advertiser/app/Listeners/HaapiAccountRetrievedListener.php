<?php

namespace Modules\Advertiser\Listeners;

use Illuminate\Support\Arr;
use Modules\Haapi\Events\HaapiAccountRetrieved;
use Modules\User\Models\AccountType;
use Modules\User\Models\User;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class HaapiAccountRetrievedListener
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var HaapiAccountRetrieved
     */
    private $event;

    /**
     * @param LoggerInterface $log
     * @param UserRepository  $repository
     */
    public function __construct(LoggerInterface $log, UserRepository $repository)
    {
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param HaapiAccountRetrieved $event
     */
    public function handle(HaapiAccountRetrieved $event): void
    {
        $this->log->info('[Special Ads Update] Received HAAPI response with advertiser data.');
        $this->event = $event;

        $payload = $event->response->getPayload();

        $accountId = $this->getAccountId($payload);
        if (is_null($accountId)) {
            return;
        }

        $advertiser = $this->findAdvertiser($accountId);
        if (is_null($advertiser)) {
            return;
        }

        $haapiSpecialAds = $this->getHaapiSpecialAds($payload);
        if (is_null($haapiSpecialAds)) {
            return;
        }

        $this->updateSpecialAds($advertiser, $haapiSpecialAds);
    }

    /**
     * @param array $payload
     *
     * @return string|null
     */
    private function getAccountId(array $payload): ?string
    {
        $accountId = Arr::get($payload, 'account.externalId');

        if (!is_null($accountId)) {
            return $accountId;
        }

        $this->log->info('[Special Ads Update] Retry. Account external ID not found.', [
            'response_id' => $this->event->response->getId(),
            'account_id'  => $accountId,
        ]);

        $accountId = Arr::get($payload, 'account.id');

        if (is_null($accountId)) {
            $this->log->error('[Special Ads Update] Cancelled. Account ID not found.', [
                'response_id' => $this->event->response->getId(),
                'account_id'  => $accountId,
            ]);

            return null;
        }

        return $accountId;
    }

    /**
     * @param string $accountId
     *
     * @return User|null
     */
    private function findAdvertiser(string $accountId): ?User
    {
        /** @var User $user */
        $user = $this->repository->findByField('account_external_id', $accountId)->first();

        if (is_null($user)) {
            $this->log->error('[Special Ads Update] Cancelled. Account not found.', [
                'response_id' => $this->event->response->getId(),
                'account_id'  => $accountId,
            ]);

            return null;
        }

        if (!$user->isAdvertiser()) {
            $this->log->error('[Special Ads Update] Cancelled. User is not an advertiser.', [
                'response_id' => $this->event->response->getId(),
                'account_id'  => $user->getExternalId(),
                'user_id'     => $user->id,
            ]);

            return null;
        }

        return $user;
    }

    /**
     * @param array $payload
     *
     * @return bool|null
     */
    private function getHaapiSpecialAds(array $payload): ?bool
    {
        $haapiSpecialAds = Arr::get($payload, 'account.specialAdsCategory');

        if (is_null($haapiSpecialAds)) {
            $this->log->error('[Special Ads Update] Cancelled. Special Ads Category not found.', [
                'response_id' => $this->event->response->getId(),
            ]);

            return null;
        }

        return $haapiSpecialAds;
    }

    /**
     * @param User $advertiser
     * @param bool $haapiSpecialAds
     */
    private function updateSpecialAds(User $advertiser, bool $haapiSpecialAds): void
    {
        if ($haapiSpecialAds === $advertiser->isSpecialAds()) {
            $this->log->info('[Special Ads Update] Cancelled. HAAPI data same as platform.', [
                'response_id' => $this->event->response->getId(),
                'account_id'  => $advertiser->getExternalId(),
                'user_id'     => $advertiser->id,
            ]);

            return;
        }

        $advertiser->account_type_id = $haapiSpecialAds ? AccountType::ID_SPECIAL_ADS : AccountType::ID_COMMON;
        $advertiser->save();

        $this->log->info('[Special Ads Update] Finished. Advertiser Special Ads changed.', [
            'response_id'             => $this->event->response->getId(),
            'account_id'              => $advertiser->getExternalId(),
            'user_id'                 => $advertiser->id,
            'is_special_ads_category' => $haapiSpecialAds,
        ]);
    }
}
