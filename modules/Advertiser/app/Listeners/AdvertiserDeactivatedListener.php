<?php

namespace Modules\Advertiser\Listeners;

use Illuminate\Contracts\Notifications\Dispatcher;
use Modules\Advertiser\Events\AdvertiserDeactivated;
use Modules\Advertiser\Notifications\Advertiser\AdvertiserDeactivated as AdvertiserDeactivatedNotification;
use Modules\Campaign\Actions\Status\PauseCampaignAction;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class AdvertiserDeactivatedListener
{
    /**
     * @var Dispatcher
     */
    private $notification;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var PauseCampaignAction
     */
    private $pauseCampaignAction;

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * UserDeactivatedListener constructor.
     *
     * @param Dispatcher          $notification
     * @param LoggerInterface     $log
     * @param CampaignRepository  $campaignRepository
     * @param PauseCampaignAction $pauseCampaignAction
     */
    public function __construct(
        Dispatcher $notification,
        LoggerInterface $log,
        CampaignRepository $campaignRepository,
        PauseCampaignAction $pauseCampaignAction
    ) {
        $this->notification = $notification;
        $this->log = $log;
        $this->campaignRepository = $campaignRepository;
        $this->pauseCampaignAction = $pauseCampaignAction;
    }

    /**
     * Handle the event.
     *
     * @param AdvertiserDeactivated $event
     */
    public function handle(AdvertiserDeactivated $event): void
    {
        $this->pauseCampaigns($event->advertiser);
        $this->sendAdvertiserNotification($event->advertiser, $event->admin);
    }

    /**
     * Pause as advertiser so after re-activation advertiser can resume campaigns.
     *
     * @param User $advertiser
     */
    private function pauseCampaigns(User $advertiser): void
    {
        $this->campaignRepository
            ->findActiveCampaignsByUser($advertiser)
            ->each(function (Campaign $campaign) use ($advertiser) {
                $this->pauseCampaignAction->handle($campaign, $advertiser);
            });
    }

    /**
     * @param User $advertiser
     * @param User $admin
     */
    private function sendAdvertiserNotification(User $advertiser, User $admin): void
    {
        $this->notification->send($advertiser, new AdvertiserDeactivatedNotification($advertiser));

        $this->log->info('Notification "AdvertiserDeactivated" sent for advertiser.', [
            'advertiser' => $advertiser->getKey(),
            'admin'      => $admin->getKey(),
        ]);
    }
}
