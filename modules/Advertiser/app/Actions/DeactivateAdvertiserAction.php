<?php

namespace Modules\Advertiser\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Auth;
use Modules\Advertiser\Events\AdvertiserDeactivated;
use Modules\Haapi\Actions\Admin\Account\AdminAccountSetStatus;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;
use Psr\Log\LoggerInterface;

class DeactivateAdvertiserAction
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var AdminAccountSetStatus
     */
    private $accountSetStatusAction;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param Dispatcher            $dispatcher
     * @param AdminAccountSetStatus $accountSetStatusAction
     * @param DatabaseManager       $databaseManager
     * @param LoggerInterface       $log
     */
    public function __construct(
        Dispatcher $dispatcher,
        AdminAccountSetStatus $accountSetStatusAction,
        DatabaseManager $databaseManager,
        LoggerInterface $log
    ) {
        $this->dispatcher = $dispatcher;
        $this->accountSetStatusAction = $accountSetStatusAction;
        $this->log = $log;
        $this->databaseManager = $databaseManager;
    }

    /**
     * Deactivated advertiser.
     *
     * @param User                 $advertiser
     * @param User|Authenticatable $admin
     *
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(User $advertiser, User $admin): void
    {
        $this->log->info('Started deactivating advertiser.', [
            'advertiser' => $advertiser->getKey(),
            'admin'      => $admin->getKey(),
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $this->accountSetStatusAction->handle($advertiser->account_external_id, false, Auth::id());

            $advertiser->getState()->apply(UserStatus::INACTIVE);
            $advertiser->save();

            $this->log->info('Advertiser status changed to deactivated.', [
                'advertiser' => $advertiser->getKey(),
                'admin'      => $admin->getKey(),
            ]);

            $this->dispatcher->dispatch(new AdvertiserDeactivated($advertiser, $admin));
        } catch (\Throwable $exception) {
            $this->log->warning('Failed to deactivate advertiser.', [
                'advertiser' => $advertiser->getKey(),
                'admin'      => $admin->getKey(),
                'reason'     => $exception->getMessage(),
            ]);
            $this->databaseManager->rollBack();
            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('Finished advertiser deactivation.', [
            'advertiser' => $advertiser->getKey(),
            'admin'      => $admin->getKey(),
        ]);
    }
}
