<?php

namespace Modules\Advertiser\Actions;

use Illuminate\Support\Collection;
use Modules\User\Models\Role;
use Modules\User\Repositories\Contracts\UserRepository;
use Modules\User\Repositories\Criteria\BusinessNameCriteria;
use Modules\User\Repositories\Criteria\RoleCriteria;

class SearchAdvertisersAction
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $needle
     * @return mixed
     */
    public function handle(string $needle): Collection
    {
        return $this->repository->pushCriteria(new RoleCriteria(Role::ID_ADVERTISER))
            ->pushCriteria(new BusinessNameCriteria($needle))->orderBy('company_name')->limit(20);
    }
}
