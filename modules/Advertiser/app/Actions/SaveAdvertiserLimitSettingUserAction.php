<?php

namespace Modules\Advertiser\Actions;

use Illuminate\Log\Logger;
use Illuminate\Support\Arr;
use Modules\Advertiser\Exceptions\SettingNotSaveException;
use Modules\User\Helpers\UserSettingsHelper;
use Modules\User\Models\UserSetting;
use Modules\User\Models\User;
use Modules\User\Repositories\UserSettingsRepository;

class SaveAdvertiserLimitSettingUserAction
{
    /**
     * @var Logger
     */
    protected Logger $log;

    /**
     * @var UserSettingsRepository
     */
    protected UserSettingsRepository $repository;

    /**
     * SaveAdvertiserLimitSettingUserAction constructor.
     * @param Logger $log
     * @param UserSettingsRepository $repository
     */
    public function __construct(Logger $log, UserSettingsRepository $repository)
    {
        $this->log = $log;
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @param User $advertiser
     * @return User
     * @throws \App\Exceptions\BaseException
     */
    public function handle(array $data, User $advertiser): User
    {
        $this->log->info('Updating settings.', [
            'data'          => $data,
            'advertiser_id' => $advertiser->id
        ]);

        try {
            return $this->deleteOrUpdateLimitSetting($advertiser, $data);
        } catch (\Throwable $throwable) {
            throw SettingNotSaveException::createFrom($throwable);
        }
    }

    /**
     * @param User $advertiser
     * @param array $data
     * @return User
     */
    protected function saveOrUpdate(User $advertiser, array $data): User
    {
        foreach ($data as $key => $item) {
            UserSetting::updateOrCreate(
                [
                    'user_id'    => $advertiser->id,
                    'setting_id' => UserSettingsHelper::getSettingByName($key)
                ],
                ['value' => $item]
            );
        }

        return $advertiser;
    }

    /**
     * @param User $advertiser
     * @param array $data
     * @return User
     */
    private function deleteOrUpdateLimitSetting(User $advertiser, array $data): User
    {
        return Arr::get($data, 'creative_monthly_upload_max_submits') === null
            ? $this->deleteSettings($advertiser)
            : $this->saveOrUpdate($advertiser, $data) ;
    }

    /**
     * @param User $advertiser
     * @return User
     */
    private function deleteSettings(User $advertiser): User
    {
        $settings = [UserSetting::CREATIVE_MAX_SUBMITS_LIMIT];
        $this->repository->deleteSettingUser($advertiser->id, $settings);

        return $advertiser;
    }
}
