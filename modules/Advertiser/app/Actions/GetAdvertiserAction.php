<?php

namespace Modules\Advertiser\Actions;

use Modules\Advertiser\Http\Resources\AdvertiserDataResource;
use Modules\Advertiser\Http\Resources\AdvertiserResource;
use Modules\Haapi\Actions\Admin\User\AdminUserGet;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Modules\User\Models\User;
use Modules\User\Repositories\Contracts\UserRepository;

class GetAdvertiserAction
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var AdminUserGet
     */
    private $adminUserGetAction;

    /**
     * @param UserRepository $repository
     * @param AdminUserGet   $adminUserGetAction
     */
    public function __construct(UserRepository $repository, AdminUserGet $adminUserGetAction)
    {
        $this->repository = $repository;
        $this->adminUserGetAction = $adminUserGetAction;
    }

    /**
     * Fetch advertiser data.
     *
     * @param User $advertiser
     *
     * @return AdvertiserResource|AdvertiserDataResource
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws UnauthorizedException
     */
    public function handle(User $advertiser)
    {
        try {
            $advertiserData = $this->adminUserGetAction->handle($advertiser);
        } catch (UnauthorizedException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            return new AdvertiserDataResource($advertiser);
        }

        return new AdvertiserResource($advertiserData);
    }
}
