<?php

namespace Modules\Advertiser\Actions;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\DatabaseManager;
use Modules\Advertiser\Exceptions\UpdateAccountTypeException;
use Modules\Haapi\Actions\Account\UpdateSpecialAdsCategory;
use Modules\Notification\Actions\DestroyReminderAction;
use Modules\Notification\Actions\StoreReminderAction;
use Modules\Notification\Models\ReminderType;
use Modules\User\Models\AccountType;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class UpdateAccountTypeAction
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var StoreReminderAction
     */
    private $storeReminderAction;

    /**
     * @var DestroyReminderAction
     */
    private $destroyReminderAction;

    /**
     * @var UpdateSpecialAdsCategory
     */
    private $updateSpecialAdsCategory;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @param LoggerInterface          $log
     * @param StoreReminderAction      $storeReminderAction
     * @param DestroyReminderAction    $destroyReminderAction
     * @param UpdateSpecialAdsCategory $updateSpecialAdsCategory
     * @param DatabaseManager          $databaseManager
     */
    public function __construct(
        LoggerInterface $log,
        StoreReminderAction $storeReminderAction,
        DestroyReminderAction $destroyReminderAction,
        UpdateSpecialAdsCategory $updateSpecialAdsCategory,
        DatabaseManager $databaseManager
    ) {
        $this->log = $log;
        $this->storeReminderAction = $storeReminderAction;
        $this->destroyReminderAction = $destroyReminderAction;
        $this->updateSpecialAdsCategory = $updateSpecialAdsCategory;
        $this->databaseManager = $databaseManager;
    }

    /**
     * @param User                 $advertiser
     * @param User|Authenticatable $admin
     * @param int                  $accountTypeId
     *
     * @throws UpdateAccountTypeException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Throwable
     */
    public function handle(User $advertiser, User $admin, int $accountTypeId): void
    {
        $this->log->info('Started updating advertiser account type.', [
            'advertiser_id'        => $advertiser->id,
            'admin_id'             => $admin->id,
            'from_account_type_id' => $advertiser->account_type_id,
            'to_account_type_id'   => $accountTypeId,
        ]);

        $this->databaseManager->beginTransaction();

        try {
            $this->updateAdvertiser($advertiser, $admin, $accountTypeId);
            $this->updateReminders($advertiser, $accountTypeId);
            $this->updateSpecialAdsCategory->handle($advertiser, $admin->id);
        } catch (\Throwable $exception) {
            $this->log->error('Failed updating advertiser account type.', [
                'advertiser_id'        => $advertiser->id,
                'admin_id'             => $admin->id,
                'from_account_type_id' => $advertiser->account_type_id,
                'to_account_type_id'   => $accountTypeId,
                'reason'               => $exception->getMessage(),
            ]);

            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        $this->log->info('Finished updating advertiser account type.', [
            'advertiser_id'        => $advertiser->id,
            'admin_id'             => $admin->id,
            'from_account_type_id' => $advertiser->account_type_id,
            'to_account_type_id'   => $accountTypeId,
        ]);

        $advertiser->refresh();
    }

    /**
     * @param User $advertiser
     * @param User $admin
     * @param int  $accountTypeId
     *
     * @return User
     * @throws UpdateAccountTypeException
     */
    private function updateAdvertiser(User $advertiser, User $admin, int $accountTypeId): User
    {
        $advertiser->account_type_id = $accountTypeId;

        if (!$advertiser->save()) {
            $this->log->info('Failed to update advertiser account type.', [
                'advertiser_id'        => $advertiser->id,
                'admin_id'             => $admin->id,
                'from_account_type_id' => $advertiser->account_type_id,
                'to_account_type_id'   => $accountTypeId,
            ]);

            $message = __('advertiser::messages.account_type.failed', [
                'account_type' => __('user::messages.account_type.' . $accountTypeId),
            ]);

            throw UpdateAccountTypeException::create($message);
        }

        return $advertiser;
    }

    /**
     * @param User $advertiser
     * @param int  $accountTypeId
     */
    private function updateReminders(User $advertiser, int $accountTypeId): void
    {
        if ($accountTypeId === AccountType::ID_SPECIAL_ADS) {
            $this->storeReminderAction->handle($advertiser, ReminderType::ID_SPECIAL_ADS);
            return;
        }

        $this->destroyReminderAction->handle($advertiser, ReminderType::ID_SPECIAL_ADS);
    }
}
