<?php

namespace Modules\Advertiser\Exceptions;

use App\Exceptions\BaseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateAccountTypeException extends BaseException
{
    /**
     * @param string $message
     *
     * @return self
     */
    public static function create(string $message): self
    {
        return new self($message);
    }

    /**
     * Report the exception.
     *
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'message' => $this->getMessage(),
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
