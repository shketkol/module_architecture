<?php

namespace Modules\Advertiser\Exceptions;

use App\Exceptions\ModelNotCreatedException;

class SettingNotSaveException extends ModelNotCreatedException
{
    //
}
