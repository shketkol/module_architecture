<?php

namespace Modules\Advertiser\DataTable;

use App\DataTable\Exceptions\RepositoryNotSetException;
use App\DataTable\Cards\PlaceholderCard;
use App\DataTable\DataTable;
use Illuminate\Database\Eloquent\Builder;
use Modules\Advertiser\DataTable\Repositories\Criteria\AdvertiserRoleCriteria;
use Modules\Advertiser\DataTable\Repositories\Criteria\SortByLastLoginCriteria;
use Modules\Advertiser\DataTable\Transformers\AdvertisersTransformer;
use Modules\User\DataTable\Filters\UserStatusFilter;
use Modules\User\DataTable\Repositories\Criteria\AddCampaignsCriteria;
use Modules\User\DataTable\Repositories\Criteria\AddRolesCriteria;
use Modules\User\DataTable\Repositories\Criteria\AddUserStatusesCriteria;
use Modules\Advertiser\DataTable\Repositories\Contracts\AdvertiserDataTableRepository;
use Modules\User\DataTable\Repositories\Criteria\NumberOfCampaignsCriteria;
use Prettus\Repository\Exceptions\RepositoryException;

class AdvertiserDataTable extends DataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'advertisers';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = AdvertisersTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = AdvertiserDataTableRepository::class;

    /**
     * Available cards
     *
     * @var array
     */
    protected $cards = [
        PlaceholderCard::class,
        PlaceholderCard::class,
        PlaceholderCard::class,
        PlaceholderCard::class,
    ];

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        UserStatusFilter::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws RepositoryNotSetException
     * @throws RepositoryException
     */
    public function createQuery(): Builder
    {
        /** @var AdvertiserDataTableRepository $repository */
        $repository = $this->getRepository();

        $repository->pushCriteria(new AddRolesCriteria())
            ->pushCriteria(new AdvertiserRoleCriteria())
            ->pushCriteria(new AddUserStatusesCriteria())
            ->pushCriteria(new AddCampaignsCriteria())
            ->pushCriteria(new NumberOfCampaignsCriteria());

        //Additional sorting for fake "incomplete" status;
        if ($this->getSortColumnName() === 'user_status') {
            $repository->pushCriteria(new SortByLastLoginCriteria($this->getSortColumnDirection()));
        }

        return $repository->users();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('advertiser.list');
    }
}
