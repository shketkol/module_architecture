<?php

namespace Modules\Advertiser\DataTable\Transformers;

use App\DataTable\Transformers\DataTableTransformer;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\UserStatus;

class AdvertisersTransformer extends DataTableTransformer
{
    /**
     * Mapping fields.
     *
     * @var array
     */
    protected array $mapMap = [
        'id'                  => [
            'name'    => 'id',
            'default' => '-',
        ],
        'company_name'        => [
            'name'    => 'company_name',
            'default' => '-',
        ],
        'number_of_campaigns' => [
            'name'    => 'number_of_campaigns',
            'default' => '-',
        ],
        'user_status'         => [
            'name'    => 'user_status',
            'default' => '-',
        ],
        'external_id'         => [
            'name'    => 'external_id',
            'default' => '',
        ],
        'account_external_id' => [
            'name'    => 'external_id',
            'default' => '',
        ],
        'created_at' => [
            'name'    => 'created_at',
            'default' => '-',
        ],
        'last_login' => [
            'name'    => 'last_login',
            'default' => '-',
        ],
    ];

    /**
     * Do transform.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array
     */
    public function transform(Model $model): array
    {
        $mapped = parent::transform($model);

        if ($model->isIncomplete()) {
            $mapped['user_status'] = UserStatus::INCOMPLETE;
        }

        return $mapped;
    }
}
