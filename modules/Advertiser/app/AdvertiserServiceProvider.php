<?php

namespace Modules\Advertiser;

use App\Providers\ModuleServiceProvider;
use Modules\Advertiser\Console\Commands\AdvertiserImportCommand;
use Modules\Advertiser\Console\Commands\AdvertiserMakeCommand;
use Modules\Advertiser\Console\Commands\SpecialAdsUploadCommand;
use Modules\Advertiser\DataTable\Repositories\AdvertiserDataTableRepository;
use Modules\Advertiser\DataTable\Repositories\Contracts\AdvertiserDataTableRepository
    as AdvertiserDataTableRepositoryInterface;
use Modules\Advertiser\Policies\AdvertiserPolicy;

class AdvertiserServiceProvider extends ModuleServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        AdvertiserDataTableRepositoryInterface::class => AdvertiserDataTableRepository::class,
    ];

    /**
     * Adblock bypass.
     *
     * @var string
     */
    protected $routePrefix = 'users';

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [
        'advertiser' => AdvertiserPolicy::class,
    ];

    /**
     * List of module console commands
     *
     * @var array
     */
    protected $commands = [
        AdvertiserMakeCommand::class,
        AdvertiserImportCommand::class,
        SpecialAdsUploadCommand::class,
    ];

    /**
     * Get module prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return 'advertiser';
    }

    /**
     * Boot provider.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        parent::boot();

        $this->commands($this->commands);
        $this->loadRoutes();
        $this->loadConfigs(['advertiser']);
    }
}
