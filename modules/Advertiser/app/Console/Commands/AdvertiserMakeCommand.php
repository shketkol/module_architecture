<?php

namespace Modules\Advertiser\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;

class AdvertiserMakeCommand extends Command
{
    use CreateUser;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:advertiser {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test advertiser.';

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * CampaignMakeCommand constructor.
     *
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        parent::__construct();
        $this->databaseManager = $databaseManager;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $count = (int)$this->argument('count');
        $batchSize = 100;
        $this->info(sprintf('Creating %d advertiser(s).', $count));
        $this->databaseManager->beginTransaction();

        try {
            for ($i = 1; $i <= $count; $i++) {
                $this->createAdvertiser();
                if (($i % $batchSize) === 0) {
                    $this->databaseManager->commit();
                    $this->info(sprintf('%d advertiser(s) created so far...', $i));
                }
            }

            $this->info('Done.');
        } catch (\Throwable $exception) {
            $this->warn(sprintf('Creating advertiser(s) failed. Reason: "%s".', $exception->getMessage()));
            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();
    }

    /**
     * @return User
     */
    private function createAdvertiser(): User
    {
        $statusId = rand(UserStatus::ID_CREATE_IN_PROGRESS, UserStatus::ID_INACTIVE);
        $approvable = in_array($statusId, [UserStatus::ID_ACTIVE, UserStatus::ID_INACTIVE]);
        $rejectable = in_array($statusId, [UserStatus::ID_CREATE_IN_PROGRESS, UserStatus::ID_INACTIVE]);

        $advertiser = $this->createTestAdvertiser([
            'status_id'   => $statusId,
            'approved_at' => $approvable ? Carbon::now() : null,
            'rejected_at' => $rejectable ? Carbon::now() : null,
        ]);

        return $advertiser;
    }
}
