<?php

namespace Modules\Advertiser\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Str;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Admin\User\Contracts\AdminUserGet;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Haapi\Mappers\Models\UserModelMapper;
use Modules\User\Models\User;
use Modules\User\Repositories\Contracts\UserRepository;

class AdvertiserImportCommand extends Command
{
    use ActAsAdmin;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'advertiser:import
                            {--account_external_id= : Advertiser account external id to import from HAAPI.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import advertiser from HAAPI by account external id.Examples:
                > php artisan advertiser:import --account_external_id="9041ab2d-c02e-4c24-bd49-121066de9433"';

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var AdminUserGet
     */
    private $userGetAction;

    /**
     * @param DatabaseManager $databaseManager
     * @param UserRepository $repository
     * @param AdminUserGet $userGetAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        UserRepository $repository,
        AdminUserGet $userGetAction
    ) {
        parent::__construct();
        $this->databaseManager = $databaseManager;
        $this->repository = $repository;
        $this->userGetAction = $userGetAction;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->info('Started import advertiser.');
        $this->databaseManager->beginTransaction();

        try {
            $accountId = $this->getAccountId();
            $user = $this->import($accountId);
        } catch (\Throwable $exception) {
            $this->warn(sprintf(
                'Failed import advertiser with account_external_id "%s". Reason: "%s".',
                $accountId,
                $exception->getMessage()
            ));

            $this->databaseManager->rollBack();

            throw $exception;
        }

        $this->databaseManager->commit();

        $this->info(sprintf(
            'Successfully imported advertiser with id #%d with account_external_id "%s"',
            $user->id,
            $accountId
        ));
    }

    /**
     * @return string
     */
    private function getAccountId(): string
    {
        $id = $this->option('account_external_id');

        if (!is_null($id)) {
            return $id;
        }

        return $this->askAccountid();
    }

    /**
     * @return string
     */
    private function askAccountId(): string
    {
        return $this->ask('Please enter account external id');
    }

    /**
     * @param string $accountId
     * @return User
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    private function import(string $accountId): User
    {
        $user = $this->retrieveUser($accountId);
        $userData = $this->userGet($user);
        $user = $this->updateUser($userData, $user);

        return $user;
    }

    /**
     * @param UserData $userData
     * @param User     $user
     *
     * @return User
     * @throws \Throwable
     */
    private function updateUser(UserData $userData, User $user): User
    {
        $this->info('Started updating user in DB.');

        try {
            $data = UserModelMapper::map($userData);
            /** @var User $user */
            $user = $this->repository->update($data, $user->id);
            $this->syncRoles($userData, $user);
        } catch (\Throwable $exception) {
            $this->warn(sprintf('Failed updating user in DB. Reason: "%s".', $exception->getMessage()));
            throw $exception;
        }

        $this->info('Successfully updated user in DB.');

        return $user;
    }

    /**
     * @param UserData $userData
     * @param User     $user
     *
     * @throws \Throwable
     */
    private function syncRoles(UserData $userData, User $user): void
    {
        $this->info('Sync roles for user.');

        try {
            $role = $this->findRole($userData);
            $user->syncRoles([$role]);

            $this->info(sprintf('Role "%s" synced.', $role));
        } catch (\Throwable $exception) {
            $this->warn(sprintf('Failed to sync user role. Reason: "%s".', $exception->getMessage()));
            throw $exception;
        }

        $this->info('Successfully synced user role.');
    }

    /**
     * @param UserData $userData
     *
     * @return string
     */
    private function findRole(UserData $userData): string
    {
        return config('daapi.roles_mapping.' . Str::lower($userData->company->accountType));
    }

    /**
     * @param User $user
     *
     * @return UserData
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    private function userGet(User $user): UserData
    {
        $this->info('Trying to get user data from HAAPI.');

        try {
            $userData = $this->userGetAction->handle($user, $this->authAsAdmin());
        } catch (\Throwable $exception) {
            $this->warn(sprintf('Failed to get user data from HAAPI. Reason: "%s".', $exception->getMessage()));
            throw $exception;
        }

        $this->info('Successfully get user data from HAAPI.');

        return $userData;
    }

    /**
     * @param string $accountId
     * @return User
     * @throws \Throwable
     */
    private function retrieveUser(string $accountId): User
    {
        try {
            /** @var User $user */
            $user = $this->repository->findWhere(['account_external_id' => $accountId])->first();

            if (!$user) {
                $user = $this->repository->create(['account_external_id' => $accountId]);
            }
        } catch (\Throwable $exception) {
            $this->warn(sprintf('User not created. Reason "%s".', $exception->getMessage()));

            throw $exception;
        }

        $this->info(sprintf('User with id #%d created', $user->id));

        return $user;
    }
}
