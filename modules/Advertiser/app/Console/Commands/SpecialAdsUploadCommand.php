<?php

namespace Modules\Advertiser\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Account\UpdateSpecialAdsCategory;
use Modules\User\DataTable\Repositories\Criteria\AddRolesCriteria;
use Modules\User\Models\Role;
use Modules\User\Models\User;
use Modules\User\Repositories\Criteria\RoleCriteria;
use Modules\User\Repositories\Criteria\SpecialAdsCriteria;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class SpecialAdsUploadCommand extends Command
{
    use ActAsAdmin;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'special-ads:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload stored on the platform info about special ads category to the HAAPI.';

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var UpdateSpecialAdsCategory
     */
    private $updateSpecialAds;

    /**
     * @param UserRepository           $repository
     * @param LoggerInterface          $log
     * @param UpdateSpecialAdsCategory $updateSpecialAds
     */
    public function __construct(
        UserRepository $repository,
        LoggerInterface $log,
        UpdateSpecialAdsCategory $updateSpecialAds
    ) {
        parent::__construct();
        $this->repository = $repository;
        $this->log = $log;
        $this->updateSpecialAds = $updateSpecialAds;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->logStarted();

        $advertisers = $this->findSpecialAdsAdvertisers();
        $this->sendToHaapi($advertisers);

        $this->logFinished($advertisers);
    }

    /**
     * @return Collection|User[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findSpecialAdsAdvertisers(): Collection
    {
        $advertisers = $this->repository
            ->pushCriteria(new AddRolesCriteria())
            ->pushCriteria(new RoleCriteria(Role::ID_ADVERTISER))
            ->pushCriteria(new SpecialAdsCriteria())
            ->all();

        if ($advertisers->isEmpty()) {
            $this->logAdvertisersNotFound();
            return $advertisers;
        }

        $this->logAdvertisersFound($advertisers);
        return $advertisers;
    }

    /**
     * @param Collection $advertisers
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function sendToHaapi(Collection $advertisers): void
    {
        if ($advertisers->isEmpty()) {
            return;
        }

        $adminId = $this->authAsAdmin();

        $advertisers->each(function (User $advertiser) use ($adminId): void {
            $this->logAdvertiserSent($advertiser);
            try {
                $this->updateSpecialAds->handle($advertiser, $adminId);
            } catch (\Throwable $exception) {
                $this->logFailed($exception, $advertiser);
            }
        });
    }

    /**
     * Console output and logs
     */
    private function logStarted(): void
    {
        $message = '[Special Ads] Started upload advertiser info to the HAAPI.';
        $this->info($message);
        $this->log->info($message);
    }

    /**
     * Console output and logs
     *
     * @param Collection $advertisers
     */
    private function logFinished(Collection $advertisers): void
    {
        $message = '[Special Ads] Finished upload advertiser info to the HAAPI.';
        $this->info($message);
        $this->log->info($message, [
            'advertiser_ids' => $advertisers->pluck('id')->toArray(),
            'count'          => $advertisers->count(),
        ]);
    }

    /**
     * @param \Throwable $exception
     * @param User $advertiser
     */
    private function logFailed(\Throwable $exception, User $advertiser): void
    {
        $message = '[Special Ads] Failed upload advertiser info to the HAAPI.';
        $this->warn(
            sprintf(
                $message . ' Reason "%s". Advertiser internal id - #{%d}',
                $exception->getMessage(),
                $advertiser->id
            )
        );

        $this->log->error($message, [
            'reason'        => $exception->getMessage(),
            'advertiser_id' => $advertiser->id,
        ]);
    }

    /**
     * Console output and logs
     */
    private function logAdvertisersNotFound(): void
    {
        $message = '[Special Ads] Advertisers not found.';
        $this->info($message);
        $this->log->info($message);
    }

    /**
     * @param Collection $advertisers
     */
    private function logAdvertisersFound(Collection $advertisers): void
    {
        $message = '[Special Ads] Advertisers found.';
        $this->info($message);
        $this->log->info($message, [
            'advertiser_ids' => $advertisers->pluck('id')->toArray(),
            'count'          => $advertisers->count(),
        ]);
    }

    /**
     * @param User $advertiser
     */
    private function logAdvertiserSent(User $advertiser): void
    {
        $message = '[Special Ads] Advertiser to be send to the HAAPI.';
        $this->info($message . " #{$advertiser->id}");
        $this->log->info($message, [
            'advertiser_id' => $advertiser->id,
        ]);
    }
}
