<?php

namespace Modules\Advertiser\Notifications\Advertiser;

use App\Notifications\Notification;
use App\Notifications\Traits\AdvertiserNotification as AdvertiserNotificationTrait;
use Modules\Notification\Models\NotificationCategory;
use Modules\User\Models\User;

abstract class AdvertiserNotification extends Notification
{
    use AdvertiserNotificationTrait;

    /**
     * @var User
     */
    public $advertiser;

    /**
     * AdvertiserActivated constructor.
     *
     * @param User $advertiser
     */
    public function __construct(User $advertiser)
    {
        $this->advertiser = $advertiser;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return NotificationCategory::ID_ACCOUNT;
    }
}
