<?php

namespace Modules\Advertiser\Notifications\Advertiser;

use Illuminate\Support\Arr;
use Modules\Advertiser\Mail\Advertiser\AdvertiserActivated as Mail;
use Modules\User\Models\User;

class AdvertiserActivated extends AdvertiserNotification
{
    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'       => $notifiable->first_name,
            'companyName'     => $notifiable->company_name,
            'bookNewCampaign' => route('campaigns.create'),
            'titleIcon'       => 'marked',
            'title'           => __('advertiser::emails.advertiser.activated.title'),
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        return __('advertiser::notifications.advertiser.activated', [
            'company_name' => e(Arr::get($data, 'companyName')),
        ]);
    }
}
