<?php

namespace Modules\Advertiser\Notifications\Advertiser;

use Illuminate\Support\Arr;
use Modules\Advertiser\Mail\Advertiser\AdvertiserDeactivated as Mail;
use Modules\User\Models\User;

class AdvertiserDeactivated extends AdvertiserNotification
{
    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass = Mail::class;

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getPayload(User $notifiable): array
    {
        return [
            'firstName'   => $notifiable->first_name,
            'companyName' => $notifiable->company_name,
            'titleIcon'   => 'cancel_window',
            'title'       => __('advertiser::emails.advertiser.deactivated.title'),
        ];
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function getContent(array $data): string
    {
        return __('advertiser::notifications.advertiser.deactivated', [
            'company_name' => e(Arr::get($data, 'companyName')),
        ]);
    }
}
