<?php

namespace Modules\Advertiser\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;

class SaveUserSettingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'creative_monthly_upload_max_submits' => $validationRules->only(
                'setting.creative_monthly_upload_max_submits',
                ['nullable', 'integer', 'min', 'max']
            ),
        ];
    }
}
