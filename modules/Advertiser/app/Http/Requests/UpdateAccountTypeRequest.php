<?php

namespace Modules\Advertiser\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class UpdateAccountTypeRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'account_type_id' => $validationRules->only(
                'user.account_type_id',
                ['required', 'integer', 'min', 'max', 'exists']
            ),
        ];
    }
}
