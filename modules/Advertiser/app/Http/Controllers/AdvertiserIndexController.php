<?php

namespace Modules\Advertiser\Http\Controllers;

use App\Http\Controllers\Controller;

class AdvertiserIndexController extends Controller
{
    /**
     * Get view for listing.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        return view('advertiser::index');
    }
}
