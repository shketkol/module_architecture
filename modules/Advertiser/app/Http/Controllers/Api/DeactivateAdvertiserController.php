<?php

namespace Modules\Advertiser\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Advertiser\Actions\DeactivateAdvertiserAction;
use Modules\Advertiser\Http\Resources\AdvertiserBriefResource;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\User\Models\User;

class DeactivateAdvertiserController extends Controller
{
    /**
     * Deactivate the advertiser.
     *
     * @param DeactivateAdvertiserAction $action
     * @param User                       $advertiser
     *
     * @return AdvertiserBriefResource|JsonResponse
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(DeactivateAdvertiserAction $action, User $advertiser)
    {
        try {
            $action->handle($advertiser, Auth::user());
        } catch (HaapiException $exception) {
            return response()->json([
                'message' => trans('advertiser::messages.deactivate.fail', [
                    'user_id' => $advertiser->id,
                    'company_name' => $advertiser->company_name
                ]),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $advertiser->load('status');

        return (new AdvertiserBriefResource($advertiser))->additional([
            'data' => [
                'message' => __('advertiser::messages.deactivate.success'),
            ],
        ]);
    }
}
