<?php

namespace Modules\Advertiser\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Advertiser\Actions\SaveAdvertiserLimitSettingUserAction;
use Modules\Advertiser\Http\Requests\SaveUserSettingRequest;
use Modules\User\Http\Resources\UserFetchedResource;
use Modules\User\Models\User;

class StoreUserSettingController extends Controller
{
    /**
     * @param SaveUserSettingRequest $request
     * @param SaveAdvertiserLimitSettingUserAction $action
     * @param User $advertiser
     * @return UserFetchedResource
     * @throws \App\Exceptions\BaseException
     */
    public function __invoke(
        SaveUserSettingRequest $request,
        SaveAdvertiserLimitSettingUserAction $action,
        User $advertiser
    ): UserFetchedResource {
        $settings = $action->handle($request->validated(), $advertiser);

        return (new UserFetchedResource($settings))->additional([
            'data' => [
                'title'   => __('messages.success'),
                'message' => __('advertiser::messages.setting_limit.changed'),
            ],
        ]);
    }
}
