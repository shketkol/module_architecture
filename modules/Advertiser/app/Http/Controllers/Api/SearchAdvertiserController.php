<?php

namespace Modules\Advertiser\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Advertiser\Actions\SearchAdvertisersAction;
use Modules\Advertiser\Http\Requests\SearchAdvertiserRequest;
use Modules\Advertiser\Http\Resources\AdvertiserSelectResource;

class SearchAdvertiserController extends Controller
{
    /**
     * Search advertiser
     *
     * @param SearchAdvertisersAction $action
     * @param SearchAdvertiserRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __invoke(
        SearchAdvertisersAction $action,
        SearchAdvertiserRequest $request
    ): AnonymousResourceCollection {
        $query = $request->get('query') ?: '';
        return AdvertiserSelectResource::collection($action->handle($query));
    }
}
