<?php

namespace Modules\Advertiser\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Modules\Advertiser\Actions\UpdateAccountTypeAction;
use Modules\Advertiser\Http\Requests\UpdateAccountTypeRequest;
use Modules\Advertiser\Http\Resources\AdvertiserBriefResource;
use Modules\User\Models\User;

class UpdateAccountTypeController extends Controller
{
    /**
     * @param UpdateAccountTypeRequest $request
     * @param UpdateAccountTypeAction  $action
     * @param User                     $advertiser
     *
     * @return JsonResponse
     * @throws \Throwable
     */
    public function __invoke(
        UpdateAccountTypeRequest $request,
        UpdateAccountTypeAction $action,
        User $advertiser
    ): JsonResponse {
        $action->handle($advertiser, Auth::user(), $request->get('account_type_id'));

        return (new AdvertiserBriefResource($advertiser))->additional([
            'data' => [
                'message' => __('advertiser::messages.account_type.changed', [
                    'account_type' => __('user::messages.account_type.' . $advertiser->account_type_id),
                ]),
            ],
        ])->response();
    }
}
