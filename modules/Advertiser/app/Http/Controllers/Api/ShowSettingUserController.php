<?php

namespace Modules\Advertiser\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Advertiser\Http\Resources\SettingsResource;
use Modules\User\Models\User;

class ShowSettingUserController extends Controller
{
    /**
     * @param User $advertiser
     * @return SettingsResource
     */
    public function __invoke(User $advertiser): SettingsResource
    {
        return new SettingsResource($advertiser);
    }
}
