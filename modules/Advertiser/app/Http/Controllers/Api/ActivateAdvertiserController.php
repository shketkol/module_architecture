<?php

namespace Modules\Advertiser\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Advertiser\Actions\ActivateAdvertiserAction;
use Modules\Advertiser\Http\Resources\AdvertiserBriefResource;
use Modules\Haapi\Exceptions\HaapiException;
use Modules\User\Models\User;

class ActivateAdvertiserController extends Controller
{
    /**
     * Activate the advertiser.
     *
     * @param ActivateAdvertiserAction $action
     * @param User                     $advertiser
     *
     * @return AdvertiserBriefResource|JsonResponse
     * @throws \Throwable
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(ActivateAdvertiserAction $action, User $advertiser)
    {
        try {
            $action->handle($advertiser, Auth::guard('admin')->user());
        } catch (HaapiException $exception) {
            return response()->json([
                'message' => trans('advertiser::messages.activate.fail')
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $advertiser->load('status');

        return (new AdvertiserBriefResource($advertiser))->additional([
            'data' => [
                'message' => __('advertiser::messages.activate.success'),
            ],
        ]);
    }
}
