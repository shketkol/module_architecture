<?php

namespace Modules\Advertiser\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\User\Models\User;

class AdvertiserShowController extends Controller
{
    /**
     * Get view for the resource displaying.
     *
     * @param User $advertiser
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(User $advertiser)
    {
        return view('advertiser::show', ['id' => $advertiser->id]);
    }
}
