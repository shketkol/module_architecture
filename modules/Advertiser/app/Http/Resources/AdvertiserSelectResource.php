<?php

namespace Modules\Advertiser\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdvertiserSelectResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'value' => $this->id,
            'label' => $this->company_name,
        ];
    }
}
