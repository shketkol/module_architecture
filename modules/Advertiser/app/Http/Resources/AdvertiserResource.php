<?php

namespace Modules\Advertiser\Http\Resources;

use App\Helpers\CurrentUser;
use Modules\User\Http\Resources\UserResource;
use Modules\User\Http\Resources\UserStatusResource;
use Modules\User\Models\AccountType;
use Modules\User\Models\User;

class AdvertiserResource extends UserResource
{
    use CurrentUser;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        $user = User::find($this->id);
        return array_merge(parent::toArray($request), [
            'advertiserSfdcUrl'  => $this->advertiserSfdcUrl ?? null,
            'brandSfdcUrl'       => $this->brandSfdcUrl ?? null,
            'contactSfdcUrl'     => $this->contactSfdcUrl ?? null,
            'states'             => $this->getStates(),
            'status'             => new UserStatusResource($user->status),
            'specialAccountType' => $user->account_type_id === AccountType::ID_SPECIAL_ADS,
            'settings'           => UserSettingResource::collection($user->settings)
        ]);
    }

    /**
     * Returns list of all states.
     *
     * @return array
     */
    protected function getStates(): array
    {
        $user = $this->getCurrentUser();
        $queriedUser = User::find($this->id);

        return [
            'can_deactivate'                   => $user->can('advertiser.deactivate', $queriedUser),
            'can_deactivate_active_campaigns'  => $user->can('advertiser.activeCampaigns', $queriedUser),
            'can_activate'                     => $user->can('advertiser.activate', $queriedUser),
            'can_update_account_type'          => $user->can('advertiser.updateAccountType', $queriedUser),
            'can_show_settings'                => $user->can('setting.show', $queriedUser),
            'can_update_creative_upload_limit' => $user->can('setting.store', $queriedUser),
        ];
    }
}
