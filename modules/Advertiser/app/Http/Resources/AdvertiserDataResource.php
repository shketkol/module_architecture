<?php

namespace Modules\Advertiser\Http\Resources;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\User\Http\Resources\UserStatusResource;

/**
 * @mixin \Modules\User\Models\User
 */
class AdvertiserDataResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'         => $this->id,
            'first_name' => $this->first_name,
            'last_name'  => $this->last_name,
            'email'      => $this->email,
            'company'    => [
                'company_name' => $this->company_name,
            ],
            'states'     => $this->getStates(),
            'status'     => new UserStatusResource($this->status),
        ];
    }

    /**
     * Returns list of all states.
     *
     * @return array
     */
    protected function getStates(): array
    {
        /** @var \Modules\User\Models\User $user */
        $user = app(Guard::class)->user();

        return [
            'can_deactivate'                  => $user->can('advertiser.deactivate', $this->getModel()),
            'can_deactivate_active_campaigns' => $user->can('advertiser.activeCampaigns', $this->getModel()),
            'can_activate'                    => $user->can('advertiser.activate', $this->getModel()),
        ];
    }
}
