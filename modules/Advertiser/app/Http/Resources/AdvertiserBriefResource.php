<?php

namespace Modules\Advertiser\Http\Resources;

use App\Helpers\CurrentUser;
use Modules\User\Http\Resources\UserFetchedResource;
use Modules\User\Http\Resources\UserStatusResource;
use Modules\User\Models\AccountType;

class AdvertiserBriefResource extends UserFetchedResource
{
    use CurrentUser;

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge(parent::toArray($request), [
            'states'             => $this->getStates(),
            'specialAccountType' => $this->resource->account_type_id === AccountType::ID_SPECIAL_ADS,
            'status'             => new UserStatusResource($this->whenLoaded('status')),
            'user_id'            => $this->resource->id
        ]);
    }

    /**
     * Returns list of all states.
     *
     * @return array
     */
    protected function getStates(): array
    {
        $user = $this->getCurrentUser();

        return [
            'can_deactivate'          => $user->can('advertiser.deactivate', $this->getModel()),
            'can_activate'            => $user->can('advertiser.activate', $this->getModel()),
            'can_update_account_type' => $user->can('advertiser.updateAccountType', $this->getModel()),
        ];
    }
}
