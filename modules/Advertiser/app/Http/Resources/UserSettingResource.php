<?php

namespace Modules\Advertiser\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserSettingResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'value' => $this->whenPivotLoaded('setting_users', function () {
                return $this->pivot->value;
            }),
        ];
    }
}
