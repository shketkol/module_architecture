<?php

namespace Modules\Advertiser\Http\Resources;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\User\Models\AccountType;

class SettingsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return int[]
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function toArray($request): array
    {
        return [
            'count_creative_submitted' => $this->getCountSubmittedCreative(),
            'account_types'           => [
                'common'  => AccountType::ID_COMMON,
                'special' => AccountType::ID_SPECIAL_ADS,
            ],
        ];
    }

    /**
     * @return int
     * @throws BindingResolutionException
     */
    public function getCountSubmittedCreative(): int
    {
        return app()->make(CreativeRepository::class)
            ->reset()
            ->bySubmittedCurrentMonth()
            ->byUser($this->resource)
            ->count();
    }
}
