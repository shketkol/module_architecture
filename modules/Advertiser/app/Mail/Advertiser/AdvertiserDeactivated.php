<?php

namespace Modules\Advertiser\Mail\Advertiser;

use App\Mail\Mail;

class AdvertiserDeactivated extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(__('advertiser::emails.advertiser.deactivated.subject', [
                'company_name' => $this->getPayloadValue('companyName'),
                'publisher_company_full_name' => config('general.company_full_name'),
            ]))
            ->view('advertiser::emails.advertiser.deactivated')
            ->with($this->payload);
    }
}
