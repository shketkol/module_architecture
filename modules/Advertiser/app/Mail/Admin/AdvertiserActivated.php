<?php

namespace Modules\Advertiser\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\User\Models\User;

class AdvertiserActivated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    public $advertiser;

    /**
     * @var User
     */
    public $admin;

    /**
     * Create a new message instance.
     *
     * @param User $admin
     * @param User $advertiser
     */
    public function __construct(User $admin, User $advertiser)
    {
        $this->advertiser = $advertiser;
        $this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->to(config('general.company_admin_emails') ?? $this->admin->email)
            ->subject(trans('advertiser::emails.admin.advertiser.activated.subject'))
            ->view('advertiser::emails.admin.activated')
            ->with([
                'advertiser'            => $this->advertiser,
                'admin'                 => $this->admin,
                'publisherCompanyName'  => config('general.company_name'),
                'publisherCompanyEmail' => config('general.company_support_email'),
            ]);
    }
}
