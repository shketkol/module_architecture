<?php

namespace Modules\Auth\Drivers;

use Carbon\Carbon;

class CognitoTokenCacheDriver extends TokenCacheDriver
{
    /**
     * @return string
     */
    protected function getKeyPrefix(): string
    {
        return 'cognito_';
    }

    /**
     * @return int
     */
    protected function getTokenCacheLifeTime(): int
    {
        return (int) config('session.lifetime') * Carbon::SECONDS_PER_MINUTE;
    }
}
