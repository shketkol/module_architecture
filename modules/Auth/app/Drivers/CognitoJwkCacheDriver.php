<?php

namespace Modules\Auth\Drivers;

use Illuminate\Support\Facades\Cache;

class CognitoJwkCacheDriver
{
    /**
     * @param  string $key
     *
     * @return array|null
     */
    public function getJwk(string $key): ?array
    {
        return Cache::get($this->getKey($key));
    }

    /**
     * @param string $key
     * @param array $data
     *
     * @return void
     */
    public function setJwk(string $key, array $data): void
    {
        Cache::put(
            $this->getKey($key),
            $data,
            $this->getCacheLifeTime()
        );
    }

    /**
     * @param string $key
     *
     * @return string
     */
    protected function getKey(string $key): string
    {
        return hash('sha256', $this->getKeyPrefix() . $key);
    }

    /**
     * @return string
     */
    protected function getKeyPrefix(): string
    {
        return 'cognito_jwk';
    }

    /**
     * @return int
     */
    protected function getCacheLifeTime(): int
    {
        return config('cognito.jwk_cache_ttl');
    }
}
