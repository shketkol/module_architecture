<?php

namespace Modules\Auth\Drivers;

use App\Helpers\SessionIdentifierHelper;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Modules\Auth\Drivers\Contracts\TokenCacheDriver as TokenCacheDriverContract;

abstract class TokenCacheDriver implements TokenCacheDriverContract
{
    /**
     * @var SessionIdentifierHelper
     */
    private $identifierHelper;

    /**
     * @param SessionIdentifierHelper $identifierHelper
     */
    public function __construct(SessionIdentifierHelper $identifierHelper)
    {
        $this->identifierHelper = $identifierHelper;
    }

    /**
     * Generate token key.
     *
     * @param  mixed $key
     *
     * @return string
     */
    protected function getKey($key): string
    {
        return hash('sha256', $this->getKeyPrefix() . $key);
    }

    /**
     * @return string
     */
    abstract protected function getKeyPrefix(): string;

    /**
     * @param  int   $key
     * @param  mixed $default
     *
     * @return array
     */
    public function get(int $key, $default = []): array
    {
        return Cache::get($this->getKey($key), $default);
    }

    /**
     * @param int   $key
     * @param array $tokens
     *
     * @return void
     */
    public function setTokens(int $key, array $tokens): void
    {
        $tokens = Arr::only($tokens, ['token', 'refreshToken']);
        Cache::put(
            $this->getKey($key),
            $tokens,
            $this->getTokenCacheLifeTime()
        );

        $this->identifierHelper->setSessionIdentifier($key);
    }

    /**
     * @return int
     */
    abstract protected function getTokenCacheLifeTime(): int;

    /**
     * Remove existing token.
     *
     * @param $key
     */
    public function unset($key): void
    {
        Cache::forget($this->getKey($key));
    }

    /**
     * @param int $userId
     *
     * @return null|string
     */
    public function getAccessToken(int $userId): ?string
    {
        if ($userId) {
            return Arr::get($this->get($userId), 'token');
        }

        return '';
    }

    /**
     * @param int $userId
     *
     * @return null|string
     */
    public function getRefreshToken(int $userId): ?string
    {
        if ($userId) {
            return Arr::get($this->get($userId), 'refreshToken');
        }

        return '';
    }
}
