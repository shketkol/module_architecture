<?php

namespace Modules\Auth\Guards;

use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Session\Session;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Exceptions\UnauthorizedException;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Modules\User\Models\User;
use Symfony\Component\HttpFoundation\Request;

class CognitoGuard extends SessionGuard
{
    /**
     * @var AuthService
     */
    protected $authService;

    /**
     * CognitoGuard constructor.
     *
     * @param string       $name
     * @param UserProvider $provider
     * @param Session      $session
     * @param null|Request $request
     * @param AuthService  $authService
     */
    public function __construct(
        string $name,
        UserProvider $provider,
        Session $session,
        ?Request $request = null,
        AuthService $authService
    ) {
        $this->authService = $authService;
        parent::__construct($name, $provider, $session, $request);
    }


    /**
     * Get the currently authenticated user.
     *
     * @return Authenticatable|null
     * @throws UnauthorizedException
     */
    public function user(): ?Authenticatable
    {
        $user = parent::user();

        if (!$user) {
            return null;
        }

        if ($this->authService->isTokenSpoiled()) {
            return $user;
        }

        // If access token isn't accessible then logout user.
        if (!$this->authService->checkToken($user->getKey())) {
            return null;
        }

        return $user;
    }
}
