<?php

namespace Modules\Auth\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Modules\Auth\Http\Requests\RegisterUserRequest;
use Modules\Haapi\Actions\Account\AccountSignUp;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\User\DataTransferObjects\UserData;
use Psr\Log\LoggerInterface;

class RegisterController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var InvitationRepository
     */
    private $invitationRepository;

    /**
     * @param LoggerInterface      $log
     * @param InvitationRepository $invitationRepository
     */
    public function __construct(LoggerInterface $log, InvitationRepository $invitationRepository)
    {
        $this->log = $log;
        $this->invitationRepository = $invitationRepository;
    }

    /**
     * @param RegisterUserRequest  $request
     *
     * @param AccountSignUp        $accountSignUpAction
     * @param InvitationRepository $invitationRepository
     *
     * @return JsonResponse
     * @throws ConflictException
     * @throws InvalidRequestException
     */
    public function __invoke(
        RegisterUserRequest $request,
        AccountSignUp $accountSignUpAction,
        InvitationRepository $invitationRepository
    ): JsonResponse {
        $this->log->info('Registration started.');

        try {
            $identificationKey = $this->findIdentificationKey($request->get('invite_code'));
            $accountSignUpAction->handle(UserData::fromRequest($request, $identificationKey));
            $invitationRepository->applyProcessingStatus($request->get('invite_code'));
        } catch (InvalidRequestException | ConflictException $exception) {
            throw $exception;
        } catch (\Throwable $exception) {
            Log::logException($exception);

            return $this->sendFailedResponse();
        }

        return $this->sendSuccessResponse();
    }

    /**
     * @param string $inviteCode
     *
     * @return string|null
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function findIdentificationKey(string $inviteCode): ?string
    {
        $invitation = $this->invitationRepository->byCode($inviteCode)->first();

        return optional($invitation)->identification_key;
    }

    /**
     * @return JsonResponse
     */
    private function sendSuccessResponse(): JsonResponse
    {
        $this->log->info('Registration completed.');

        return response()->json(
            ['data' => ['message' => trans('auth::messages.success')]],
            Response::HTTP_CREATED
        );
    }

    /**
     * @return JsonResponse
     */
    private function sendFailedResponse(): JsonResponse
    {
        $this->log->warning('Registration failed.', ['reason' => trans('auth::messages.failed')]);

        return response()->json(
            ['data' => ['message' => trans('auth::messages.failed')]],
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }
}
