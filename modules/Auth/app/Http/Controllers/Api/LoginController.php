<?php

namespace Modules\Auth\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Auth\Traits\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;
}
