<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class RegisterController extends Controller
{
    /**
     * Show the application registration form.
     *
     * @param string|null $step
     *
     * @return View|RedirectResponse
     */
    public function __invoke(?string $step = null)
    {
        if (!is_null($step)) {
            return redirect()->route('register');
        }

        return view('auth::register.form');
    }
}
