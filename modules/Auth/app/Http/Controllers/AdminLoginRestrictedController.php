<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AdminLoginRestrictedController extends Controller
{
    /**
     * Show the application registration form.
     *
     * @return View
     */
    public function show()
    {
        return view('auth::admin-login-restricted');
    }
}
