<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class LoginFailedController extends Controller
{
    /**
     * Show the application registration form.
     *
     * @return View
     */
    public function __invoke()
    {
        return view('auth::login-failed');
    }
}
