<?php

namespace Modules\Auth\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterUserRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        $issetInvitationCodeByEmail = Rule::exists('invitations', 'email')
            ->where('code', request()->get('invite_code', ''));

        return [
            'email'                   => array_merge($validationRules->only(
                'user.email',
                ['required', 'string', 'email', 'max', 'valid_email']
            ), [$issetInvitationCodeByEmail]),
            'invite_code'             => $validationRules->only(
                'user.invite_code',
                ['required', 'string', 'max', 'valid_invite_code']
            ),
            'first_name'              => $validationRules->only(
                'user.first_name',
                ['required', 'string', 'max']
            ),
            'last_name'               => $validationRules->only(
                'user.last_name',
                ['required', 'string', 'max']
            ),
            'phone_number'            => $validationRules->only(
                'user.phone',
                ['required', 'string', 'max', 'regex']
            ),
            'company_name'            => $validationRules->only(
                'user.company_name',
                ['required', 'string', 'max']
            ),
            'company_address'         => $validationRules->only(
                'user.company_address',
                ['required', 'array']
            ),
            'company_address.line1'   => $validationRules->only(
                'user.address.line1',
                ['required', 'string', 'max']
            ),
            'company_address.line2'   => $validationRules->only(
                'user.address.line2',
                ['string', 'max'],
                ['nullable']
            ),
            'company_address.city'    => $validationRules->only(
                'user.address.city',
                ['required', 'string', 'max']
            ),
            'company_address.state'   => $validationRules->only(
                'user.address.state',
                ['required', 'string', 'max', 'exists']
            ),
            'company_address.country' => $validationRules->only(
                'user.address.country',
                ['required', 'string', 'max']
            ),
            'company_address.zipcode' => $validationRules->only(
                'user.address.zip',
                ['required', 'string', 'max', 'regex', 'exists', 'has_state']
            ),
        ];
    }
}
