<?php

namespace Modules\Auth\HttpClient;

use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Modules\Auth\HttpClient\Traits\RefreshToken;
use Modules\Auth\HttpClient\Traits\SignOut;
use Psr\Log\LoggerInterface;

class CognitoClient
{
    use RefreshToken,
        SignOut;

    /**
     * @var CognitoIdentityProviderClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $poolId;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * CognitoClient constructor.
     *
     * @param CognitoIdentityProviderClient $client
     * @param string                        $clientId
     * @param string                        $poolId
     */
    public function __construct(
        CognitoIdentityProviderClient $client,
        string $clientId,
        string $poolId
    ) {
        $this->client = $client;
        $this->clientId = $clientId;
        $this->poolId = $poolId;
        $this->setDependencies();
    }

    /**
     * @return void
     */
    private function setDependencies(): void
    {
        $this->log = app(LoggerInterface::class);
    }
}
