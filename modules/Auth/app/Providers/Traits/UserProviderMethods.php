<?php

namespace Modules\Auth\Providers\Traits;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\User\Models\User;

trait UserProviderMethods
{
    /**
     * Retrieve a user by their unique identifier (id).
     *
     * @param int $identifier
     *
     * @return Authenticatable|null
     */
    public function retrieveById($identifier): ?Authenticatable
    {
        return User::find($identifier);
    }

    /**
     * Remember me functionality is disabled
     *
     * @param mixed  $identifier
     * @param string $token
     *
     * @return Authenticatable|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function retrieveByToken($identifier, $token): ?Authenticatable
    {
        return null;
    }

    /**
     * Remember me functionality is disabled
     *
     * @param Authenticatable|User $user
     * @param string               $token
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function updateRememberToken(Authenticatable $user, $token): void
    {
        // not supported
    }
}
