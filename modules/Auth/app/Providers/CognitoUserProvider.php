<?php

namespace Modules\Auth\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use Modules\Auth\Providers\Contracts\UserProvider as UserProviderContract;

class CognitoUserProvider extends UserProvider implements UserProviderContract
{
    /**
     * @param Authenticatable $user
     * @param array           $credentials
     *
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        if (!parent::validateCredentials($user, $credentials)) {
            return false;
        }

        $this->authService->setTokens($user->getKey(), $credentials);
        $this->setTokenCookieAction->handle($credentials['token']);

        return true;
    }

    /**
     * @param array $credentials
     *
     * @return string
     */
    protected function getSessionToken(array $credentials): string
    {
        return Arr::get($credentials, 'token', '');
    }
}
