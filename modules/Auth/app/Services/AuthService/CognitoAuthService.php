<?php

namespace Modules\Auth\Services\AuthService;

use Modules\Auth\Exceptions\UnauthorizedException;
use Modules\Auth\Services\AuthService\Actions\Cognito\GlobalSignOutAction;
use Modules\Auth\Services\AuthService\Actions\Cognito\RefreshTokenAction;
use Modules\Auth\Services\AuthService\Actions\ValidateCognitoJwtAction;
use Modules\User\Models\User;

class CognitoAuthService extends AuthService
{
    /**
     * @var RefreshTokenAction
     */
    protected $actionRefreshToken;

    /**
     * @var GlobalSignOutAction
     */
    protected $actionSignOut;

    /**
     * @var ValidateCognitoJwtAction
     */
    protected $actionValidateJwt;

    /**
     * @return void
     */
    protected function setDependencies(): void
    {
        parent::setDependencies();

        $this->actionRefreshToken = app(RefreshTokenAction::class);
        $this->actionSignOut = app(GlobalSignOutAction::class);
        $this->actionValidateJwt = app(ValidateCognitoJwtAction::class);
    }

    /**
     * @param int|null $userId
     *
     * @return null|string
     */
    public function getToken(?int $userId): ?string
    {
        if (!$userId) {
            return null;
        }

        $token = parent::getToken($userId);

        if (!$token) {
            return null;
        }

        if ($this->validateToken($token)) {
            return $token;
        }

        return $this->tryRefreshToken($userId);
    }

    /**
     * @param int $userId
     *
     * @return null|string
     */
    public function tryRefreshToken(int $userId): ?string
    {
        $refreshToken = $this->getRefreshToken($userId);
        if (!$refreshToken) {
            return null;
        }

        $attempts = 0;
        while (true) {
            $attempts++;

            $accessToken = $this->actionRefreshToken->handle($refreshToken);

            if ($accessToken) {
                $this->setTokens($userId, [
                    'token'        => $accessToken,
                    'refreshToken' => $refreshToken,
                ]);

                return $accessToken;
            }

            if ($attempts === config('cognito.refresh_token_attempts')) {
                $this->log->info('[COGNITO] Reached max refresh token attempts. Flushing cognito tokens.');
                $this->flushTokens($userId);
                return null;
            }
        }
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    private function validateToken(string $token): bool
    {
        return $this->actionValidateJwt->handle($token);
    }

    /**
     * @param int $userId
     *
     * @return void
     */
    public function flushTokens(int $userId): void
    {
        parent::flushTokens($userId);
        $this->setTokenSpoiled();
    }

    /**
     * @param User $user
     *
     * @return void
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function logout(User $user): void
    {
        $userId = $user->getKey();
        $token = $this->getToken($userId);
        if (!$token) {
            throw new UnauthorizedException('[COGNITO] Failed to sign out user -  missing Access Token.');
        }

        try {
            $this->actionSignOut->handle($userId, $token);
        } catch (\Throwable $e) {
            $token = $this->tryRefreshToken($userId);
            if (!$token) {
                throw new UnauthorizedException('[COGNITO] Failed to sign out user - Access Token failed to refresh.');
            }

            try {
                $this->actionSignOut->handle($userId, $token);
            } catch (\Throwable $e) {
                throw new UnauthorizedException('[COGNITO] Failed to sign out user during second attempt.');
            }
        }

        $this->log->info('User successfully logged out from Cognito', ['user_id' => $user->getKey()]);
    }
}
