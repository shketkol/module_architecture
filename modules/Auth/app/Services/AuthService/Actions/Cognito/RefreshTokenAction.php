<?php

namespace Modules\Auth\Services\AuthService\Actions\Cognito;

use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Modules\Auth\HttpClient\CognitoClient;
use Modules\Daapi\Actions\Cookie\SetTokenCookieAction;
use Psr\Log\LoggerInterface;
use Throwable;

class RefreshTokenAction extends BaseAction
{
    protected $setTokenCookieAction;

    /**
     * RefreshTokenAction constructor.
     * @param CognitoClient $client
     * @param LoggerInterface $log
     * @param SetTokenCookieAction $setTokenCookieAction
     */
    public function __construct(
        CognitoClient $client,
        LoggerInterface $log,
        SetTokenCookieAction $setTokenCookieAction
    ) {
        parent::__construct($client, $log);
        $this->setTokenCookieAction = $setTokenCookieAction;
    }

    /**
     * Try to get the access-token using refreshToken.
     *
     * @param string $refreshToken
     *
     * @return null|string
     */
    public function handle(string $refreshToken): ?string
    {
        try {
            $result = $this->client->refreshToken($refreshToken);
            $this->setTokenCookieAction->handle($result['AccessToken']);

            return $result['AccessToken'];
        } catch (Throwable $exception) {
            $this->logException($exception);

            return null;
        }
    }

    /**
     * @param Throwable $exception
     *
     * @return void
     */
    private function logException(Throwable $exception): void
    {
        $context = [
            'message' => $exception->getMessage(),
        ];
        if ($exception instanceof CognitoIdentityProviderException) {
            $context['message']        = '(AWS Exception) ' . $exception->getAwsErrorMessage();
            $context['aws_error_code'] = $exception->getAwsErrorCode();
        }

        $this->log->info('[COGNITO] Refresh token operation - FAILED', $context);
    }
}
