<?php

namespace Modules\Auth\Services\AuthService\Actions\Cognito;

use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Throwable;

class GlobalSignOutAction extends BaseAction
{
    /**
     * @param int    $userId
     * @param string $token
     *
     * @return void
     * @throws Throwable
     */
    public function handle(int $userId, string $token): void
    {
        try {
            $this->client->signOut($token);
        } catch (Throwable $e) {
            $this->logException($e, $userId);
            throw $e;
        }
    }

    /**
     * @param Throwable $exception
     * @param int       $userId
     *
     * @return void
     */
    private function logException(Throwable $exception, int $userId): void
    {
        $context = [
            'message' => $exception->getMessage(),
            'user_id'  => $userId,
        ];
        if ($exception instanceof CognitoIdentityProviderException) {
            $context['message']        = '(AWS Exception) ' . $exception->getAwsErrorMessage();
            $context['aws_error_code'] = $exception->getAwsErrorCode();
        }
        $this->log->info('[COGNITO] Sign out user operation - FAILED.', $context);
    }
}
