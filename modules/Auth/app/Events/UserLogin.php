<?php

namespace Modules\Auth\Events;

use Illuminate\Queue\SerializesModels;
use Modules\User\Models\User;

class UserLogin
{
    use SerializesModels;

    /**
     * The logged in user.
     *
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
