<?php

namespace Modules\Auth\Traits;

use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Auth\AuthenticatesUsers as BaseAuthenticate;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Events\UserLogin;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

trait AuthenticatesAdmin
{
    use BaseAuthenticate;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var SessionManager
     */
    private $session;

    /**
     * @var ValidationRules
     */
    private $validationRules;

    /**
     * @param LoggerInterface $log
     * @param Dispatcher      $dispatcher
     * @param SessionManager  $session
     * @param ValidationRules $validationRules
     */
    public function __construct(
        LoggerInterface $log,
        Dispatcher $dispatcher,
        SessionManager $session,
        ValidationRules $validationRules
    ) {
        $this->log = $log;
        $this->dispatcher = $dispatcher;
        $this->session = $session;
        $this->validationRules = $validationRules;
    }

    /**
     * Define a guard to be used
     *
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * Validate the admin login request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            'code' => $this->validationRules->only('user.token', ['required', 'string', 'max']),
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function credentials(Request $request): array
    {
        return $request->only('code');
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param User    $user
     *
     * @return RedirectResponse
     */
    protected function authenticated(Request $request, User $user): RedirectResponse
    {
        $this->log->info('Admin logged in.', ['user' => $user->getKey()]);

        $this->dispatcher->dispatch(new UserLogin($user));

        return redirect($this->getRedirectTo($user));
    }

    /**
     * Get url user should be redirected to after login.
     *
     * @param User $user
     *
     * @return string
     */
    protected function getRedirectTo(User $user): string
    {
        return $this->session->get('target_url') ?: route('landing');
    }
}
