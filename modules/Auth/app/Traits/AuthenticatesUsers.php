<?php

namespace Modules\Auth\Traits;

use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Auth\AuthenticatesUsers as BaseAuthenticate;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Modules\Auth\Events\UserLogin;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

trait AuthenticatesUsers
{
    use BaseAuthenticate;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var SessionManager
     */
    private $session;

    /**
     * @var ValidationRules
     */
    private $validationRules;

    /**
     * @param LoggerInterface $log
     * @param Dispatcher $dispatcher
     * @param SessionManager $session
     * @param ValidationRules $validationRules
     */
    public function __construct(
        LoggerInterface $log,
        Dispatcher $dispatcher,
        SessionManager $session,
        ValidationRules $validationRules
    ) {
        $this->log = $log;
        $this->dispatcher = $dispatcher;
        $this->session = $session;
        $this->validationRules = $validationRules;
    }

    /**
     * Validate the user login request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            'token' => $this->validationRules->only('user.token', ['required', 'string', 'max']),
            'refreshToken' => $this->validationRules->only('user.token', ['required', 'string', 'max']),
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request): array
    {
        return $request->only('token', 'refreshToken');
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function authenticated(Request $request, User $user): JsonResponse
    {
        $this->log->info('User logged in.', ['user' => $user->getKey()]);

        $this->dispatcher->dispatch(new UserLogin($user));

        return response()->json([
            'redirect' => $this->getRedirectTo($user),
        ]);
    }

    /**
     * Get url user should be redirected to after login.
     *
     * @param User $user
     *
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function getRedirectTo(User $user): string
    {
        return $this->session->get('target_url') ?: route('landing');
    }
}
