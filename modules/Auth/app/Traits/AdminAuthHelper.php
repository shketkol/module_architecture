<?php

namespace Modules\Auth\Traits;

trait AdminAuthHelper
{
    /**
     * Returns OKTA login url with parameters
     *
     * @return string
     */
    private function getAdminLoginRedirectUrl(): string
    {
        $url = config('cognito.admin.okta_login_url');
        $params = urldecode(http_build_query(config('cognito.admin.login_params'), '', '&'));

        return "{$url}?{$params}";
    }

    /**
     * Get url for redirection on successful admin logout.
     *
     * @return string
     */
    private function getAdminLogoutRedirectUrl(): string
    {
        return $this->getOktaLogoutUrl();
    }

    /**
     * Returns OKTA logout url with parameters.
     *
     * @return string
     */
    private function getOktaLogoutUrl(): string
    {
        $url = config('cognito.admin.okta_logout_url');
        $params = urldecode(http_build_query(config('cognito.admin.logout_params'), '', '&'));

        return "{$url}?{$params}";
    }
}
