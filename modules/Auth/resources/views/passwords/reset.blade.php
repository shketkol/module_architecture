@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <reset-password-page
            token="{{ $token }}"
            user-id="{{ $id }}"
        ></reset-password-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
