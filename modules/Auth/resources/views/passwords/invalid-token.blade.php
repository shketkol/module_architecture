@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <invalid-reset-password-token-page></invalid-reset-password-token-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
