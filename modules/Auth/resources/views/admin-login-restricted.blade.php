@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <admin-login-restricted-page></admin-login-restricted-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
