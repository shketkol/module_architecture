@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <login-failed-page></login-failed-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
