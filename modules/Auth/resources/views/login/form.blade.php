@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <login-form-page></login-form-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
