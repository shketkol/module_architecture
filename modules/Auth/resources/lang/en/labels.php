<?php

return [
    'user_name'           => 'User name',
    'user_email'          => 'User email',
    'business_name'       => 'Business name',
    'business_address'    => 'Business address',
    'get_started'         => 'Get Started',
    'welcome'             => 'Welcome to Hulu Ad Manager!',
    'to_finish'           => 'To finish setting up your account, create a password below. You can edit your account details later in Settings.',
    'by_creating_account' => 'By creating an account, you represent that you have the authority to act on the business\' behalf.',
    'by_clicking'         => 'By clicking “GET STARTED” you agree to the Hulu Ad Manager <a href=":link1" target="_blank">Terms of Use</a> and <a href=":link2" target="_blank">Privacy Policy</a>.',
    'password_must_meet'  => 'Password must meet requirements below',
];
