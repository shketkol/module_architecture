<?php

/**
 * Authorized users.
 */
Route::middleware(['auth:admin'])->group(function () {
    /**
     * Broadcast index page
     */
    Route::get('/', 'IndexController')->name('index')->middleware('can:broadcast.list');

    /**
     * Broadcast details page
     */
    Route::get('/{broadcast}', 'IndexController')
        ->name('show')
        ->middleware('can:broadcast.view');
});
