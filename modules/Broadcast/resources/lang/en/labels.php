<?php

use Modules\Broadcast\Models\BroadcastChannel;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;

return [
    'broadcasts'                    => 'Broadcasts',
    'create_broadcast'              => 'Create Broadcast',
    'update_broadcast'              => 'Update Broadcast',
    'schedule'                      => 'Schedule',
    'search_broadcasts_placeholder' => 'Search Broadcasts',
    'scheduled_date'                => 'scheduled date',
    'unschedule'                    => 'Reschedule',
    'saved'                         => 'Saved',
    'blocks'                        => [
        'general' => 'General information',
        'email'   => 'Email fields',
        'in-app'  => 'In-app fields',
    ],
    'field_labels'                  => [
        'name'           => 'Broadcast name',
        'date'           => 'Broadcast date/time',
        'channel'        => 'Broadcast channel',
        'segment'        => 'Broadcast segment',
        'advertiser'     => 'Advertiser',
        'affected_dates' => 'Affected Dates',
    ],
    'field_placeholders'            => [
        'name'       => 'Enter your broadcast name',
        'date'       => 'MM/DD/YY',
        'advertiser' => 'Select Advertiser (business name)',
    ],
    'channel_notification'          => [
        BroadcastChannel::ALL                 => 'All',
        BroadcastChannel::EMAIL               => 'Email notification',
        BroadcastChannel::IN_APP_NOTIFICATION => 'In-app notification',
    ],
    'email_channels_fields'         => [
        'labels'       => [
            'subject' => 'Email subject',
            'header'  => 'Email header',
            'icon'    => 'Icon (optional)',
            'body'    => 'Email body',
        ],
        'placeholders' => [
            'subject' => 'Enter your broadcast email subject',
            'header'  => 'Enter your broadcast email header',
            'icon'    => 'Select an icon',
        ],
    ],
    'in-app_channels_fields'        => [
        'labels'       => [
            'category' => 'Notification category',
            'body'     => 'Notification message',
        ],
        'placeholders' => [
            'category' => 'Select a category',
        ],
    ],
    'segments'                      => [
        BroadcastSegmentGroup::ID_ALL_ADVERTISERS      => 'All advertisers',
        BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS => 'Select advertiser(s)',
        BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS => 'Affected advertiser(s)',
    ],
    'datatable_labels'              => [
        'name'            => 'Name',
        'date'            => 'Scheduled Date',
        'channel'         => 'Channel Type',
        'status'          => 'Notification status',
        'segment'         => 'Segment Type',
        'broadcaster'     => 'Broadcaster',
        'segments_values' => [
            BroadcastSegmentGroup::ID_ALL_ADVERTISERS      => 'All advertisers',
            BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS => 'Selected advertiser(s)',
            BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS => 'Affected advertiser(s)',
        ],
    ],
    'statuses'                      => [
        'draft'      => 'Draft',
        'scheduled'  => 'Scheduled',
        'processing' => 'Processing',
        'sent'       => 'Sent',
        'failed'     => 'Failed',
    ],
    'date_description'              => 'The Broadcast will be sent at the specified time (ET time zone) on the selected date.',
    'test_email'                    => [
        'label'       => 'Test email address',
        'placeholder' => 'Enter an email to test the broadcast',
    ],
    'enter_test_email'              => 'Enter test email',
    'send_test_email_title'         => 'Test Email Broadcast',
    'send_now_modal_title'          => 'Send the Broadcast now?',
    'close_modal_title'             => 'Close Broadcast Edit Form',
    'send_email_button'             => 'Send Email',
    'count_affected_advertisers'    => 'Number of affected advertisers: :count',
];
