<?php

return [
    'clone'             => 'Duplicate',
    'send_email_button' => 'Send test email',
    'send_now_button'   => 'Send Now',
];
