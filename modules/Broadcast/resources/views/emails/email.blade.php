@extends('common.email.layout')

@section('title')
    @include('common.email.title', [
       'title' => \Illuminate\Support\Arr::get($email, 'header'),
       'titleIcon' => \Illuminate\Support\Arr::get($email, 'icon'),
   ])
@stop

@section('body')
    {!! \Illuminate\Support\Arr::get($email, 'body') !!}

    @include('common.email.part.if-you-have-any-questions')
@stop
