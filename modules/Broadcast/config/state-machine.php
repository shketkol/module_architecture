<?php

use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;

return [
    /**
     * Broadcast workflow graph
     */
    'broadcast' => [
        // class of your domain object
        'class'         => Broadcast::class,

        // name of the graph (default is "default")
        'graph'         => 'broadcast',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status_id',

        // list of all possible states
        'states'        => [
            [
                'name' => BroadcastStatus::ID_DRAFT,
            ],
            [
                'name' => BroadcastStatus::ID_SCHEDULED,
            ],
            [
                'name' => BroadcastStatus::ID_PROCESSING,
            ],
            [
                'name' => BroadcastStatus::ID_SENT,
            ],
            [
                'name' => BroadcastStatus::ID_FAILED,
            ],
        ],

        // list of all possible transitions
        'transitions'   => [
            BroadcastStatus::DRAFT      => [
                'from' => [
                    BroadcastStatus::ID_SCHEDULED,
                ],
                'to'   => BroadcastStatus::ID_DRAFT,
            ],
            BroadcastStatus::SCHEDULED  => [
                'from' => [
                    BroadcastStatus::ID_DRAFT,
                ],
                'to'   => BroadcastStatus::ID_SCHEDULED,
            ],
            BroadcastStatus::SENT       => [
                'from' => [
                    BroadcastStatus::ID_PROCESSING,
                ],
                'to'   => BroadcastStatus::ID_SENT,
            ],
            BroadcastStatus::PROCESSING => [
                'from' => [
                    BroadcastStatus::ID_SCHEDULED,
                    BroadcastStatus::ID_FAILED,
                ],
                'to'   => BroadcastStatus::ID_PROCESSING,
            ],
            BroadcastStatus::FAILED     => [
                'from' => [
                    BroadcastStatus::ID_PROCESSING,
                ],
                'to'   => BroadcastStatus::ID_FAILED,
            ],
        ],

        // list of all callbacks
        'callbacks'     => [
            // will be called when testing a transition
            'guard'  => [],

            // will be called before applying a transition
            'before' => [],

            // will be called after applying a transition
            'after'  => [],
        ],
    ],
];
