<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeToNullableBroadcastEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('broadcast_emails', function (Blueprint $table) {
            $table->string('subject')->nullable()->change();
            $table->string('header')->nullable()->change();
            $table->longText('body')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('broadcast_emails', function (Blueprint $table) {
            $table->string('subject')->change();
            $table->string('header')->change();
            $table->longText('body')->change();
        });
    }
}
