<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBroadcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('broadcasts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');

            $table->foreignId('status_id')->constrained('broadcast_statuses');
            $table->foreignId('user_id')->constrained();

            $table->foreignId('email_id')
                ->nullable()
                ->constrained('broadcast_emails')
                ->onDelete('cascade');

            $table->foreignId('notification_id')
                ->nullable()
                ->constrained('broadcast_notifications')
                ->onDelete('cascade');

            $table->timestamp('schedule_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('broadcasts');
    }
}
