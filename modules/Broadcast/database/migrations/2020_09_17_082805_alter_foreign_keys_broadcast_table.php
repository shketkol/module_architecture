<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterForeignKeysBroadcastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('broadcasts', function (Blueprint $table) {
            $table->dropForeign(['email_id']);
            $table->foreign('email_id')
                ->references('id')
                ->on('broadcast_emails')
                ->onDelete('set null');

            $table->dropForeign(['notification_id']);
            $table->foreign('notification_id')
                ->references('id')
                ->on('broadcast_notifications')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('broadcasts', function (Blueprint $table) {
            $table->dropForeign(['email_id']);
            $table->foreign('email_id')
                ->references('id')
                ->on('broadcast_emails')
                ->onDelete('cascade');

            $table->dropForeign(['notification_id']);
            $table->foreign('notification_id')
                ->references('id')
                ->on('broadcast_notifications')
                ->onDelete('cascade');
        });
    }
}
