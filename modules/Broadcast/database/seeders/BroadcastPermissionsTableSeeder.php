<?php

namespace Modules\Broadcast\Database\Seeders;

use Database\Seeders\BasePermissionsTableSeeder;
use Modules\Broadcast\Policies\BroadcastPolicy;
use Modules\User\Models\Role;

class BroadcastPermissionsTableSeeder extends BasePermissionsTableSeeder
{
    /**
     * This property should be modified in module seeder.
     *
     * @var array
     */
    protected $permissions = [
        Role::ID_ADMIN           => [
            BroadcastPolicy::PERMISSION_CREATE_BROADCAST,
            BroadcastPolicy::PERMISSION_LIST_BROADCAST,
            BroadcastPolicy::PERMISSION_VIEW_BROADCAST,
            BroadcastPolicy::PERMISSION_UPDATE_BROADCAST,
            BroadcastPolicy::PERMISSION_CLONE_BROADCAST,
            BroadcastPolicy::PERMISSION_SEND_TEST_BROADCAST,
            BroadcastPolicy::PERMISSION_SEND_NOW_BROADCAST,
        ],
        Role::ID_ADMIN_READ_ONLY => [
            BroadcastPolicy::PERMISSION_LIST_BROADCAST,
            BroadcastPolicy::PERMISSION_VIEW_BROADCAST,
        ],
        Role::ID_ADVERTISER      => [],
    ];
}
