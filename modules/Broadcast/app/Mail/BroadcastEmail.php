<?php

namespace Modules\Broadcast\Mail;

use App\Mail\Mail;
use Illuminate\Support\Arr;

class BroadcastEmail extends Mail
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->subject(Arr::get($this->payload, 'email.subject'))
            ->view('broadcast::emails.email')
            ->with($this->payload);
    }
}
