<?php

namespace Modules\Broadcast\Actions\Template;

use Modules\User\Models\User;

class ReplaceTemplateVariablesAction
{
    /**
     * @param User $recipient
     *
     * @return array
     */
    private function createVariables(User $recipient): array
    {
        return [
            '${first_name}'   => $recipient->first_name,
            '${last_name}'    => $recipient->last_name,
            '${full_name}'    => $recipient->full_name,
            '${email}'        => $recipient->email,
            '${company_name}' => $recipient->company_name,
        ];
    }

    /**
     * @param string $string
     * @param User   $recipient
     *
     * @return string
     */
    public function handle(string $string, User $recipient): string
    {
        return strtr($string, $this->createVariables($recipient));
    }
}
