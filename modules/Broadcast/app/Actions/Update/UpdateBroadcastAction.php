<?php

namespace Modules\Broadcast\Actions\Update;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Arr;
use Modules\Broadcast\Actions\Store\StoreBroadcastSegmentGroupAction;
use Modules\Broadcast\Exceptions\BroadcastNotUpdatedException;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastChannel;
use Modules\Broadcast\Repositories\BroadcastRepository;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class UpdateBroadcastAction
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var BroadcastRepository
     */
    private $broadcastRepository;

    /**
     * @var UpdateBroadcastEmailAction
     */
    private $updateBroadcastEmailAction;

    /**
     * @var UpdateBroadcastNotificationAction
     */
    private $updateBroadcastNotificationAction;

    /**
     * @var StoreBroadcastSegmentGroupAction
     */
    private $storeBroadcastSegmentGroupAction;

    /**
     * @param DatabaseManager                   $databaseManager
     * @param LoggerInterface                   $log
     * @param BroadcastRepository               $broadcastRepository
     * @param UpdateBroadcastEmailAction        $updateBroadcastEmailAction
     * @param UpdateBroadcastNotificationAction $updateBroadcastNotificationAction
     * @param StoreBroadcastSegmentGroupAction  $storeBroadcastSegmentGroupAction
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        BroadcastRepository $broadcastRepository,
        UpdateBroadcastEmailAction $updateBroadcastEmailAction,
        UpdateBroadcastNotificationAction $updateBroadcastNotificationAction,
        StoreBroadcastSegmentGroupAction $storeBroadcastSegmentGroupAction
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->broadcastRepository = $broadcastRepository;
        $this->updateBroadcastEmailAction = $updateBroadcastEmailAction;
        $this->updateBroadcastNotificationAction = $updateBroadcastNotificationAction;
        $this->storeBroadcastSegmentGroupAction = $storeBroadcastSegmentGroupAction;
    }

    /**
     * @param User|Authenticatable $admin
     * @param Broadcast            $broadcast
     * @param array                $data
     *
     * @return Broadcast
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function handle(User $admin, Broadcast $broadcast, array $data): Broadcast
    {
        $this->log->info('Updating broadcast with relations.', ['data' => $data]);
        $this->databaseManager->beginTransaction();

        try {
            $broadcast = $this->update($admin, $broadcast, $data);
        } catch (\Throwable $exception) {
            $this->databaseManager->rollBack();
            $this->log->warning('Broadcast not updated.', ['reason' => $exception->getMessage()]);

            throw BroadcastNotUpdatedException::createFrom($exception);
        }

        $this->databaseManager->commit();
        $this->log->info('Broadcast with relations updated.', ['broadcast_id' => $broadcast->id]);

        return $broadcast;
    }

    /**
     * @param User      $admin
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return Broadcast
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function update(User $admin, Broadcast $broadcast, array $data): Broadcast
    {
        $channel = Arr::get($data, 'channel');

        $this->updateBroadcastEmailAction->handle($broadcast, Arr::get($data, 'email', []));
        $this->updateBroadcastNotificationAction->handle($broadcast, Arr::get($data, 'notification', []));

        if ($channel === BroadcastChannel::EMAIL) {
            $this->updateBroadcastNotificationAction->deleteBroadcastNotification($broadcast);
        }

        if ($channel === BroadcastChannel::IN_APP_NOTIFICATION) {
            $this->updateBroadcastEmailAction->deleteBroadcastEmail($broadcast);
        }

        $broadcastData = array_merge(
            Arr::except($data, ['email', 'notification', 'segment_group']),
            ['user_id' => $admin->id]
        );

        $broadcast = $this->updateBroadcast($broadcast, $broadcastData);
        $this->storeBroadcastSegmentGroupAction->handle($broadcast, Arr::get($data, 'segment_group', []));

        return $broadcast;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return Broadcast
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function updateBroadcast(Broadcast $broadcast, array $data): Broadcast
    {
        $this->log->info('Updating broadcast.', [
            'broadcast_id' => $broadcast->id,
            'data'         => $data,
        ]);

        if (Arr::get($data, 'user_id') !== $broadcast->user->id) {
            $this->log->info('Broadcast owner has changed.', [
                'broadcast_id' => $broadcast->id,
                'old_user_id'  => $broadcast->user->id,
                'new_user_id'  => Arr::get($data, 'user_id'),
            ]);
        }

        $broadcast = $this->broadcastRepository->update($data, $broadcast->id);
        $this->log->info('Broadcast updated.', ['broadcast_id' => $broadcast->id]);

        return $broadcast;
    }
}
