<?php

namespace Modules\Broadcast\Actions\Update;

use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;
use Modules\Broadcast\Repositories\BroadcastSegmentDateRepository;
use Psr\Log\LoggerInterface;

class UpdateBroadcastSegmentDateAction
{
    /**
     * @var BroadcastSegmentDateRepository
     */
    private $segmentDateRepository;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param LoggerInterface                $log
     * @param BroadcastSegmentDateRepository $segmentDateRepository
     */
    public function __construct(LoggerInterface $log, BroadcastSegmentDateRepository $segmentDateRepository)
    {
        $this->log = $log;
        $this->segmentDateRepository = $segmentDateRepository;
    }

    /**
     * @param BroadcastSegmentGroup $segmentGroup
     * @param array                 $data
     *
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(BroadcastSegmentGroup $segmentGroup, array $data): void
    {
        $this->log->info('[Broadcast Segment Group Date] Update started.', [
            'group_id' => $segmentGroup->id,
            'data'     => $data,
        ]);

        $this->segmentDateRepository->update($data, $segmentGroup->date->id);

        $this->log->info('[Broadcast Segment Group Date] Update finished.', [
            'data'     => $data,
            'group_id' => $segmentGroup->id,
            'date_id'  => $segmentGroup->date->id,
        ]);
    }
}
