<?php

namespace Modules\Broadcast\Actions\Store;

use Modules\Broadcast\Models\BroadcastEmail;
use Modules\Broadcast\Repositories\BroadcastEmailRepository;
use Psr\Log\LoggerInterface;

class StoreBroadcastEmailAction
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var BroadcastEmailRepository
     */
    private $emailRepository;

    /**
     * @param LoggerInterface          $log
     * @param BroadcastEmailRepository $emailRepository
     */
    public function __construct(LoggerInterface $log, BroadcastEmailRepository $emailRepository)
    {
        $this->log = $log;
        $this->emailRepository = $emailRepository;
    }

    /**
     * @param array $data
     *
     * @return BroadcastEmail|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(array $data): ?BroadcastEmail
    {
        $this->log->info('Storing broadcast email.', ['data' => $data]);

        if (empty($data)) {
            $this->log->info('Skipped broadcast email. No data was given.', ['data' => $data]);
            return null;
        }

        $email = $this->emailRepository->create($data);

        $this->log->info('Broadcast email created.', ['email_id' => $email->id]);

        return $email;
    }
}
