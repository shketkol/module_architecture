<?php

namespace Modules\Broadcast\Actions\Store;

use Illuminate\Database\DatabaseManager;
use Modules\Broadcast\Models\BroadcastNotification;
use Modules\Broadcast\Repositories\BroadcastNotificationRepository;
use Psr\Log\LoggerInterface;

class StoreBroadcastNotificationAction
{
    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var BroadcastNotificationRepository
     */
    private $notificationRepository;

    /**
     * @param DatabaseManager                 $databaseManager
     * @param LoggerInterface                 $log
     * @param BroadcastNotificationRepository $notificationRepository
     */
    public function __construct(
        DatabaseManager $databaseManager,
        LoggerInterface $log,
        BroadcastNotificationRepository $notificationRepository
    ) {
        $this->databaseManager = $databaseManager;
        $this->log = $log;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * @param array $data
     *
     * @return BroadcastNotification|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle(array $data): ?BroadcastNotification
    {
        $this->log->info('Storing broadcast notification.', ['data' => $data]);

        if (empty($data)) {
            $this->log->info('Skipped broadcast notification. No data was given.', ['data' => $data]);
            return null;
        }

        $notification = $this->notificationRepository->create($data);

        $this->log->info('Broadcast notification created.', ['notification_id' => $notification->id]);

        return $notification;
    }
}
