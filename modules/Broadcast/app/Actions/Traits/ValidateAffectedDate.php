<?php

namespace Modules\Broadcast\Actions\Traits;

use App\Services\ValidationRulesService\ValidationRules;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\Validator;

trait ValidateAffectedDate
{
    /**
     * @param array $dates
     *
     * @return bool
     */
    private function isValidAffectedDate(array $dates): bool
    {
        $validator = Validator::make($dates, $this->getAffectedDateRules());

        return !$validator->fails();
    }

    /**
     * Get rules for validation.
     *
     * @return array
     */
    protected function getAffectedDateRules(): array
    {
        $validationRules = $this->validationRules();

        return [
            'date_start' => $validationRules->only(
                'broadcast.affected_advertisers.date_start',
                ['date', 'before', 'after_or_equal']
            ),
            'date_end'   => $validationRules->only(
                'broadcast.affected_advertisers.date_end',
                ['date', 'before', 'after_or_equal']
            ),
        ];
    }

    /**
     * @return ValidationRules
     */
    private function validationRules(): ValidationRules
    {
        return app(ValidationRules::class);
    }

    /**
     * @return array
     */
    private function getValidAffectedDate(): array
    {
        $end = CarbonImmutable::now();
        $start = $end->subMonths(config('activities.database.store_months'));

        return [
            'date_start' => $start,
            'date_end'   => $end,
        ];
    }
}
