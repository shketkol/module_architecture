<?php

namespace Modules\Broadcast\Actions;

use App\Services\ValidationRulesService\ValidationRules;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Modules\Broadcast\Http\Requests\Traits\BroadcastValidationRules;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;
use Psr\Log\LoggerInterface;

class ValidateBroadcastAction
{
    use BroadcastValidationRules;

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var ValidationRules
     */
    private $validationRules;

    /**
     * @param LoggerInterface $log
     * @param ValidationRules $validationRules
     */
    public function __construct(LoggerInterface $log, ValidationRules $validationRules)
    {
        $this->log = $log;
        $this->validationRules = $validationRules;
    }

    /**
     * @param Broadcast $broadcast
     *
     * @return void
     * @throws ValidationException
     */
    public function handle(Broadcast $broadcast): void
    {
        $this->log->info('Started validating broadcast.', ['broadcast_id' => $broadcast->id]);

        $validator = Validator::make($this->getData($broadcast), $this->getRules());

        if ($validator->fails()) {
            $this->log->warning('Broadcast is not valid.', [
                'broadcast_id' => $broadcast->id,
                'errors'       => $validator->errors(),
            ]);

            throw new ValidationException($validator);
        }

        $this->log->info('Finished broadcast validation.', ['broadcast_id' => $broadcast->id]);
    }

    /**
     * Get data for validation.
     *
     * @param Broadcast $broadcast
     *
     * @return array
     */
    protected function getData(Broadcast $broadcast): array
    {
        $data = [
            'name'          => $broadcast->name,
            'schedule_date' => $broadcast->schedule_date,
        ];

        $data = $this->addSegmentGroupData($broadcast, $data);

        if ($broadcast->hasEmail()) {
            Arr::set($data, 'email', [
                'subject' => $broadcast->email->subject,
                'header'  => $broadcast->email->header,
                'icon'    => $broadcast->email->icon,
                'body'    => $broadcast->email->body,
            ]);
        }

        if ($broadcast->hasNotification()) {
            Arr::set($data, 'notification', [
                'notification_category_id' => $broadcast->notification->notification_category_id,
                'body'                     => $broadcast->notification->body,
            ]);
        }

        return $data;
    }

    /**
     * @param Broadcast $broadcast
     * @param array     $data
     *
     * @return array
     */
    private function addSegmentGroupData(Broadcast $broadcast, array $data): array
    {
        /** @var BroadcastSegmentGroup|null $segmentGroup */
        $segmentGroup = $broadcast->segmentGroups()->first();

        if (is_null($segmentGroup)) {
            return $data;
        }

        if ($segmentGroup->isSelectedAdvertisers()) {
            Arr::set($data, 'segment_group', [
                'id'          => $segmentGroup->id,
                'advertisers' => $segmentGroup->recipients()->pluck('id')->toArray(),
            ]);

            return $data;
        }

        if ($segmentGroup->isAffectedAdvertisers()) {
            Arr::set($data, 'segment_group', [
                'id'    => $segmentGroup->id,
                'dates' => $segmentGroup->date->only(['date_start', 'date_end']),
            ]);

            return $data;
        }

        return $data;
    }

    /**
     * Get rules for validation.
     *
     * @return array
     */
    protected function getRules(): array
    {
        $rules = [
            'schedule_date'                         => $this->validationRules->only(
                'broadcast.schedule_date',
                ['required', 'date', 'valid_broadcast_date']
            ),

            // segment group
            'segment_group'                         => $this->validationRules->only(
                'broadcast.segment_group.payload',
                ['required', 'array']
            ),
            'segment_group.id'                      => $this->validationRules->only(
                'broadcast.segment_group.id',
                ['required', 'integer', 'min', 'max', 'exists']
            ),

            // email
            'email'                                 => $this->validationRules->only(
                'broadcast.email.payload',
                ['required_without_all', 'array']
            ),
            'email.subject'                         => $this->validationRules->only(
                'broadcast.email.subject',
                ['required_with', 'string', 'max', 'regex']
            ),
            'email.header'                          => $this->validationRules->only(
                'broadcast.email.header',
                ['required_with', 'string', 'max', 'regex']
            ),
            'email.icon'                            => $this->validationRules->only(
                'broadcast.email.icon',
                ['string', 'exists', 'nullable']
            ),
            'email.body'                            => $this->validationRules->only(
                'broadcast.email.body',
                ['required_with', 'string', 'max']
            ),

            // notification
            'notification'                          => $this->validationRules->only(
                'broadcast.notification.payload',
                ['required_without_all', 'array']
            ),
            'notification.notification_category_id' => $this->validationRules->only(
                'broadcast.notification.category.id',
                ['required_with', 'integer', 'min', 'max', 'exists']
            ),
            'notification.body'                     => $this->validationRules->only(
                'broadcast.notification.body',
                ['required_with', 'string', 'max']
            ),
        ];

        $rules = array_merge($rules, $this->getRequiredRules($this->validationRules));
        $rules = array_merge($rules, $this->getRequiredSegmentGroupRules($this->validationRules));
        $rules = array_merge($rules, $this->getOptionalRules($this->validationRules));

        return $rules;
    }
}
