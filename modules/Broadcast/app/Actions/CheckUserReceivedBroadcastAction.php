<?php

namespace Modules\Broadcast\Actions;

use Illuminate\Support\Arr;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Notifications\BroadcastNotification;
use Modules\Notification\Repositories\EmailHistoryRepository;
use Modules\Notification\Repositories\NotificationRepository;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class CheckUserReceivedBroadcastAction
{
    /**
     * @var EmailHistoryRepository
     */
    private $emailHistoryRepository;

    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param EmailHistoryRepository $emailHistoryRepository
     * @param NotificationRepository $notificationRepository
     * @param LoggerInterface        $log
     */
    public function __construct(
        EmailHistoryRepository $emailHistoryRepository,
        NotificationRepository $notificationRepository,
        LoggerInterface $log
    ) {
        $this->emailHistoryRepository = $emailHistoryRepository;
        $this->notificationRepository = $notificationRepository;
        $this->log = $log;
    }

    /**
     * @param User      $user
     * @param Broadcast $broadcast
     * @param array     $options ['check_only_email' => bool, 'check_only_notification' => bool]
     *
     * @return bool
     */
    public function handle(User $user, Broadcast $broadcast, array $options = []): bool
    {
        $this->log->info('[Check User Received Broadcast] Checking if user has already received the notification.', [
            'broadcast_id' => $broadcast->id,
            'recipient_id' => $user->id,
        ]);

        // @todo in case we would add more channels, refactor with strategy

        if (Arr::get($options, 'check_only_email', false) && $broadcast->hasEmail()) {
            return $this->checkEmail($user, $broadcast);
        }

        if (Arr::get($options, 'check_only_notification', false) && $broadcast->hasNotification()) {
            return $this->checkNotification($user, $broadcast);
        }

        return $this->checkAll($user, $broadcast);
    }

    /**
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     */
    private function checkEmail(User $user, Broadcast $broadcast): bool
    {
        $this->log->info('[Check User Received Broadcast] Checking only email notification.', [
            'broadcast_id' => $broadcast->id,
            'recipient_id' => $user->id,
        ]);

        return $this->hasReceivedEmailNotification($user, $broadcast);
    }

    /**
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     */
    private function checkNotification(User $user, Broadcast $broadcast): bool
    {
        $this->log->info('[Check User Received Broadcast] Checking only database notification.', [
            'broadcast_id' => $broadcast->id,
            'recipient_id' => $user->id,
        ]);

        return $this->hasReceivedDatabaseNotification($user, $broadcast);
    }

    /**
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     */
    private function checkAll(User $user, Broadcast $broadcast): bool
    {
        $result = true;
        if ($broadcast->hasEmail()) {
            $result = $this->hasReceivedEmailNotification($user, $broadcast) && $result;
        }

        if ($broadcast->hasNotification()) {
            $result = $this->hasReceivedDatabaseNotification($user, $broadcast) && $result;
        }

        return $result;
    }

    /**
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     */
    private function hasReceivedEmailNotification(User $user, Broadcast $broadcast): bool
    {
        $notification = $this->emailHistoryRepository->findWhere([
            'mailable_type'   => BroadcastNotification::class,
            'mailable_id'     => $broadcast->id,
            'notifiable_type' => get_class($user),
            'notifiable_id'   => $user->id,
        ])->first();

        if (is_null($notification)) {
            $this->log->info('[Check User Received Broadcast] User has not received the email notification.', [
                'broadcast_id' => $broadcast->id,
                'recipient_id' => $user->id,
            ]);

            return false;
        }

        $this->log->info('[Check User Received Broadcast] User has already received the email notification.', [
            'broadcast_id'    => $broadcast->id,
            'recipient_id'    => $user->id,
            'notification_id' => $notification->id,
        ]);

        return true;
    }

    /**
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     */
    private function hasReceivedDatabaseNotification(User $user, Broadcast $broadcast): bool
    {
        $notification = $this->notificationRepository->findWhere([
            'type'            => BroadcastNotification::class,
            'type_id'         => $broadcast->id,
            'notifiable_type' => get_class($user),
            'notifiable_id'   => $user->id,
        ])->first();

        if (is_null($notification)) {
            $this->log->info('[Check User Received Broadcast] User has not received the database notification.', [
                'broadcast_id' => $broadcast->id,
                'recipient_id' => $user->id,
            ]);

            return false;
        }

        $this->log->info('[Check User Received Broadcast] User has already received the database notification.', [
            'broadcast_id'    => $broadcast->id,
            'recipient_id'    => $user->id,
            'notification_id' => $notification->id,
        ]);

        return true;
    }
}
