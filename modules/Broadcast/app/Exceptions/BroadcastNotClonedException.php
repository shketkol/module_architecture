<?php

namespace Modules\Broadcast\Exceptions;

use App\Exceptions\ModelNotUpdatedException;

class BroadcastNotClonedException extends ModelNotUpdatedException
{
    //
}
