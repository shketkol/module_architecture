<?php

namespace Modules\Broadcast\Jobs;

use App\Jobs\Job;
use App\Models\QueuePriority;
use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Support\Collection;
use Modules\Broadcast\Actions\CheckUserReceivedBroadcastAction;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;
use Modules\Broadcast\Notifications\BroadcastNotification;
use Modules\User\Models\User;
use Psr\Log\LoggerInterface;

class ProcessBroadcast extends Job
{
    /**
     * @var Broadcast
     */
    public $broadcast;

    /**
     * @param Broadcast $broadcast
     */
    public function __construct(Broadcast $broadcast)
    {
        $this->broadcast = $broadcast;
        $this->onQueue(QueuePriority::low());
    }

    /**
     * Process broadcast job
     */
    public function handle(): void
    {
        $this->log()->info('[Broadcast job] Started processing.', [
            'job_id'       => $this->job->getJobId(),
            'broadcast_id' => $this->broadcast->id,
        ]);

        try {
            $this->broadcast->applyInternalStatus(BroadcastStatus::PROCESSING);
            $this->handleRecipients();
            $this->broadcast->applyInternalStatus(BroadcastStatus::SENT);
        } catch (\Throwable $exception) {
            $this->log()->warning('[Broadcast job] Failed processing.', [
                'reason' => $exception->getMessage(),
            ]);

            $this->fail($exception);
        }

        $this->log()->info('[Broadcast job] Finished processing.', [
            'job_id'       => $this->job->getJobId(),
            'broadcast_id' => $this->broadcast->id,
        ]);
    }

    /**
     * Get all broadcast recipients and send them notifications
     */
    private function handleRecipients(): void
    {
        foreach ($this->broadcast->segmentGroups as $segmentGroup) {
            $offset = 0;
            $limit = $this->getBatchSize();
            $recipients = $segmentGroup->recipients($limit, $offset);
            while (!$recipients->isEmpty()) {
                $this->sendNotifications($recipients);

                $offset += $limit;
                $recipients = $segmentGroup->recipients($limit, $offset);
            }
        }
    }

    /**
     * @param Collection $recipients
     */
    private function sendNotifications(Collection $recipients): void
    {
        $recipients->each(function (User $recipient): void {
            if ($this->checkUserReceivedBroadcast()->handle($recipient, $this->broadcast)) {
                return;
            }

            $this->sendNotification($recipient);
        });
    }

    /**
     * @param User $recipient
     */
    private function sendNotification(User $recipient): void
    {
        $this->dispatcher()->send($recipient, new BroadcastNotification($this->broadcast));

        $this->log()->info('[Broadcast job] Notification was put in the queue.', [
            'job_id'       => $this->job->getJobId(),
            'broadcast_id' => $this->broadcast->id,
            'recipient_id' => $recipient->id,
        ]);
    }

    /**
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function failed(): void
    {
        if ($this->broadcast) {
            $this->broadcast->applyInternalStatus(BroadcastStatus::FAILED);
        }
    }

    /**
     * @return LoggerInterface
     */
    private function log(): LoggerInterface
    {
        return app(LoggerInterface::class);
    }

    /**
     * @return int
     */
    private function getBatchSize(): int
    {
        return config('broadcast.job.batch_size');
    }

    /**
     * @return Dispatcher
     */
    private function dispatcher(): Dispatcher
    {
        return app(Dispatcher::class);
    }

    /**
     * @return CheckUserReceivedBroadcastAction
     */
    private function checkUserReceivedBroadcast(): CheckUserReceivedBroadcastAction
    {
        return app(CheckUserReceivedBroadcastAction::class);
    }
}
