<?php

namespace Modules\Broadcast\DataTable;

use App\DataTable\DataTable;
use Illuminate\Database\Eloquent\Builder;
use Modules\Broadcast\DataTable\Repositories\BroadcastsDataTableRepository;
use Modules\Broadcast\DataTable\Repositories\Criteria\BroadcasterCriteria;
use Modules\Broadcast\DataTable\Repositories\Criteria\ChannelCriteria;
use Modules\Broadcast\DataTable\Repositories\Criteria\SegmentCriteria;
use Modules\Broadcast\DataTable\Repositories\Filters\BroadcastStatusFilter;
use Modules\Broadcast\DataTable\Transformers\BroadcastsTransformer;
use Modules\Broadcast\DataTable\Repositories\Criteria\StatusCriteria;
use Modules\Report\DataTable\Repositories\Contracts\ReportsDataTableRepository;

class BroadcastDataTable extends DataTable
{
    /**
     * Data table name
     *
     * @var string
     */
    public static $name = 'broadcasts';

    /**
     * Transformer class
     *
     * @var string
     */
    protected $transformer = BroadcastsTransformer::class;

    /**
     * Repository
     *
     * @var string
     */
    protected $repository = BroadcastsDataTableRepository::class;

    /**
     * Filters
     *
     * @var array
     */
    protected $filters = [
        BroadcastStatusFilter::class,
    ];

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
        /** @var ReportsDataTableRepository $repository */
        $repository = $this->getRepository();

        return $this->applySortCriteria($repository)
            ->pushCriteria(new StatusCriteria())
            ->pushCriteria(new SegmentCriteria())
            ->pushCriteria(new BroadcasterCriteria())
            ->pushCriteria(new ChannelCriteria())
            ->broadcasts();
    }

    /**
     * Check if user can view table
     *
     * @return bool
     */
    protected function can(): bool
    {
        return $this->getUser()->can('broadcast.list');
    }
}
