<?php

namespace Modules\Broadcast\DataTable\Repositories\Contracts;

use App\Repositories\Contracts\Repository;
use Illuminate\Database\Eloquent\Builder;

interface BroadcastsDataTableRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string;

    /**
     * Get broadcasts list
     *
     * @return Builder
     */
    public function broadcasts(): Builder;
}
