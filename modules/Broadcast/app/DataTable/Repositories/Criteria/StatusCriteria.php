<?php

namespace Modules\Broadcast\DataTable\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class StatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param \Illuminate\Database\Eloquent\Model|Builder $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->addSelect(
            'broadcast_statuses.name AS broadcast_status'
        )->leftJoin(
            'broadcast_statuses',
            'broadcasts.status_id',
            '=',
            'broadcast_statuses.id'
        );
    }
}
