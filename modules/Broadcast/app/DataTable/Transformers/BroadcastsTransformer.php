<?php

namespace Modules\Broadcast\DataTable\Transformers;

use App\DataTable\Transformers\DataTableTransformer;
use App\Helpers\DateFormatHelper;
use Illuminate\Database\Eloquent\Model;
use Modules\Broadcast\Models\BroadcastChannel;

class BroadcastsTransformer extends DataTableTransformer
{
    /**
     * Mapping fields.
     *
     * @var array
     */
    protected array $mapMap = [
        'id'               => [
            'name'    => 'id',
            'default' => '-',
        ],
        'name'             => [
            'name'    => 'name',
            'default' => '-',
        ],
        'schedule_date'    => [
            'name'    => 'schedule_date',
            'default' => '-',
        ],
        'broadcast_status' => [
            'name'    => 'status.name',
            'default' => '-',
        ],
        'segment_id'       => [
            'name'    => 'segment_id',
            'default' => '-',
        ],
        'broadcaster_name' => [
            'name'    => 'broadcaster_name',
            'default' => '-',
        ],
        'status_id' => [
            'name'    => 'status_id',
            'default' => '-',
        ],
        'channel' => [
            'name'    => 'channel',
            'default' => '-',
        ],
    ];

    /**
     * Do transform.
     *
     * @param Model|\Modules\Broadcast\Models\Broadcast $model
     *
     * @return array
     */
    public function transform(Model $model): array
    {
        $mapped = parent::transform($model);

        // Add channel
        $mapped['channel'] = __("broadcast::labels.channel_notification.{$mapped['channel']}");

        $mapped['schedule_date'] = $mapped['schedule_date'] !== '-' ?
            DateFormatHelper::toTimezone(
                $mapped['schedule_date']->format(config('date.format.db_date_time')),
                config('date.default_timezone_full_code')
            ) :
            '-';

        return $mapped;
    }
}
