<?php

namespace Modules\Broadcast\Policies;

use App\Policies\Policy;
use Modules\Broadcast\Models\Broadcast;
use Modules\Broadcast\Models\BroadcastStatus;
use Modules\User\Models\User;

class BroadcastPolicy extends Policy
{
    /**
     * @var string
     */
    protected string $model = Broadcast::class;

    /** Permissions. */
    public const PERMISSION_CREATE_BROADCAST = 'create_broadcast';
    public const PERMISSION_LIST_BROADCAST = 'list_broadcast';
    public const PERMISSION_VIEW_BROADCAST = 'view_broadcast';
    public const PERMISSION_UPDATE_BROADCAST = 'update_broadcast';
    public const PERMISSION_CLONE_BROADCAST = 'clone_broadcast';
    public const PERMISSION_SEND_TEST_BROADCAST = 'send_test_broadcast';
    public const PERMISSION_SEND_NOW_BROADCAST = 'send_now_broadcast';

    /**
     * Returns true if user can view list of all broadcasts.
     *
     * @param User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_LIST_BROADCAST);
    }

    /**
     * Returns true if user can store a new broadcast draft.
     *
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CREATE_BROADCAST);
    }

    /**
     * Returns true if user can view the broadcast.
     *
     * @param User $user
     *
     * @return bool
     */
    public function view(User $user): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_VIEW_BROADCAST)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update the broadcast.
     *
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     */
    public function update(User $user, Broadcast $broadcast): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_BROADCAST)) {
            return false;
        }

        if (!$broadcast->isEditable()) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can update the broadcast.
     *
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     * @throws \SM\SMException
     */
    public function schedule(User $user, Broadcast $broadcast): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_BROADCAST)) {
            return false;
        }

        if (!$broadcast->canApply(BroadcastStatus::SCHEDULED)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can unschedule the broadcast.
     *
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     * @throws \SM\SMException
     */
    public function unschedule(User $user, Broadcast $broadcast): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_UPDATE_BROADCAST)) {
            return false;
        }

        if (!$broadcast->canApply(BroadcastStatus::DRAFT)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can clone broadcast.
     *
     * @param User $user
     *
     * @return bool
     */
    public function clone(User $user): bool
    {
        return $user->hasPermissionTo(self::PERMISSION_CLONE_BROADCAST);
    }

    /**
     * Returns true if user can send test email.
     *
     * @param User      $user
     * @param Broadcast $broadcast
     *
     * @return bool
     * @throws \SM\SMException
     */
    public function sendTest(User $user, Broadcast $broadcast): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_SEND_TEST_BROADCAST)) {
            return false;
        }

        if (!$broadcast->inState(BroadcastStatus::ID_SCHEDULED)) {
            return false;
        }

        if (!$broadcast->hasEmail()) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if user can send now broadcast.
     *
     * @param User $user
     * @param Broadcast $broadcast
     *
     * @return bool
     */
    public function sendNow(User $user, Broadcast $broadcast): bool
    {
        if (!$user->hasPermissionTo(self::PERMISSION_SEND_NOW_BROADCAST)) {
            return false;
        }

        if (!$broadcast->inState(BroadcastStatus::ID_SCHEDULED)) {
            return false;
        }

        return true;
    }
}
