<?php

namespace Modules\Broadcast\Http\Requests;

use App\Helpers\HtmlHelper;
use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Support\Arr;
use Modules\Broadcast\Http\Requests\Traits\BroadcastValidationRules;

class StoreBroadcastRequest extends Request
{
    use BroadcastValidationRules;

    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        $rules = [
            'schedule_date'                         => $validationRules->only(
                'broadcast.schedule_date',
                ['date', 'nullable']
            ),
            'channel'                               => $validationRules->only(
                'broadcast.channel',
                ['required', 'string', 'in']
            ),

            // segment group - selected advertisers
            'segment_group.advertisers'             => $validationRules->only(
                'broadcast.segment_group.advertisers',
                ['array', 'is_advertisers', 'max']
            ),

            // segment group - affected advertisers
            'segment_group.dates'                   => $validationRules->only(
                'broadcast.affected_advertisers.dates',
                ['array']
            ),
            'segment_group.dates.date_start'        => $validationRules->only(
                'broadcast.affected_advertisers.date_start',
                ['date', 'before', 'after_or_equal', 'nullable']
            ),
            'segment_group.dates.date_end'          => $validationRules->only(
                'broadcast.affected_advertisers.date_end',
                ['date', 'before', 'after_or_equal', 'nullable']
            ),

            // email
            'email'                                 => $validationRules->only(
                'broadcast.email.payload',
                ['array']
            ),
            'email.subject'                         => $validationRules->only(
                'broadcast.email.subject',
                ['string', 'max', 'regex', 'nullable']
            ),
            'email.header'                          => $validationRules->only(
                'broadcast.email.header',
                ['string', 'max', 'regex', 'nullable']
            ),
            'email.body'                            => $validationRules->only(
                'broadcast.email.body',
                ['string', 'max', 'nullable']
            ),

            // notification
            'notification'                          => $validationRules->only(
                'broadcast.notification.payload',
                ['array']
            ),
            'notification.notification_category_id' => $validationRules->only(
                'broadcast.notification.category.id',
                ['integer', 'min', 'max', 'exists', 'nullable']
            ),
            'notification.body'                     => $validationRules->only(
                'broadcast.notification.body',
                ['string', 'max', 'nullable']
            ),
        ];

        $rules = array_merge($rules, $this->getRequiredRules($validationRules));
        $rules = array_merge($rules, $this->getOptionalRules($validationRules));

        return $rules;
    }

    /**
     * Filter malicious html
     */
    protected function prepareForValidation(): void
    {
        $input = $this->all();

        Arr::set($input, 'email.body', $this->html()->purify(Arr::get($input, 'email.body')));
        Arr::set($input, 'notification.body', $this->html()->purify(Arr::get($input, 'notification.body')));

        $this->replace($input);
    }

    /**
     * @return HtmlHelper
     */
    private function html(): HtmlHelper
    {
        return app(HtmlHelper::class);
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'schedule_date' => __('broadcast::labels.scheduled_date'),
        ];
    }
}
