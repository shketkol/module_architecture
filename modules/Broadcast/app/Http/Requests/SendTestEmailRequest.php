<?php

namespace Modules\Broadcast\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class SendTestEmailRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'test_email'    => $validationRules->only(
                'broadcast.test_email',
                ['required', 'string', 'email']
            )
        ];
    }
}
