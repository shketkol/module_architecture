<?php

namespace Modules\Broadcast\Http\Requests;

use App\Http\Requests\Request;
use App\Services\ValidationRulesService\Contracts\ValidationRules;

class CountAffectedAdvertisersRequest extends Request
{
    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            'date_start' => $validationRules->only(
                'broadcast.affected_advertisers.date_start',
                ['required', 'date', 'before', 'after_or_equal']
            ),
            'date_end'   => $validationRules->only(
                'broadcast.affected_advertisers.date_end',
                ['required', 'date', 'before', 'after_or_equal']
            ),
        ];
    }
}
