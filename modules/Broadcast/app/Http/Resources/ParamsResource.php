<?php

namespace Modules\Broadcast\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Modules\Broadcast\Models\BroadcastChannel;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;

class ParamsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'emailIcons' => EmailIconResource::collection(Arr::get($this->resource, 'emailIcons')),
            'notificationCategories' => NotificationCategoryResource::collection(
                Arr::get($this->resource, 'notificationCategories'),
            ),
            'channels' => BroadcastChannel::CHANNELS,
            'segments' => BroadcastSegmentGroup::SEGMENTS,
        ];
    }
}
