<?php

namespace Modules\Broadcast\Http\Controllers;

class IndexController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        return view('broadcast::index');
    }
}
