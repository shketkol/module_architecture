<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Modules\Broadcast\Actions\Update\UpdateBroadcastAction;
use Modules\Broadcast\Http\Requests\StoreBroadcastRequest;
use Modules\Broadcast\Http\Resources\BroadcastResource;
use Modules\Broadcast\Models\Broadcast;

class UpdateBroadcastController
{
    /**
     * @param Broadcast             $broadcast
     * @param UpdateBroadcastAction $action
     * @param StoreBroadcastRequest $request
     * @param Authenticatable       $admin
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\BaseException
     * @throws \Throwable
     */
    public function __invoke(
        Broadcast $broadcast,
        UpdateBroadcastAction $action,
        StoreBroadcastRequest $request,
        Authenticatable $admin
    ): JsonResponse {
        $broadcast = $action->handle($admin, $broadcast, $request->all());
        return (new BroadcastResource($broadcast))->response();
    }
}
