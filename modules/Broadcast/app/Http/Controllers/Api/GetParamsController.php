<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Modules\Broadcast\Http\Resources\ParamsResource;
use Modules\Notification\Repositories\EmailIconRepository;
use Modules\Notification\Repositories\NotificationCategoryRepository;

class GetParamsController extends Controller
{
    /**
     * Get broadcast params.
     *
     * @param EmailIconRepository $emailIconRepository
     * @param NotificationCategoryRepository $notificationCategoryRepository
     * @return ParamsResource
     */
    public function __invoke(
        EmailIconRepository $emailIconRepository,
        NotificationCategoryRepository $notificationCategoryRepository
    ): ParamsResource {
        return new ParamsResource([
            'emailIcons'             => $emailIconRepository->all(),
            'notificationCategories' => $notificationCategoryRepository->all(),
        ]);
    }
}
