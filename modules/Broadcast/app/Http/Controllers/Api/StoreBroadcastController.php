<?php

namespace Modules\Broadcast\Http\Controllers\Api;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Broadcast\Actions\Store\StoreBroadcastAction;
use Modules\Broadcast\Http\Requests\StoreBroadcastRequest;
use Modules\Broadcast\Http\Resources\BroadcastResource;

class StoreBroadcastController
{
    /**
     * @param StoreBroadcastAction  $action
     * @param StoreBroadcastRequest $request
     * @param Authenticatable       $admin
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function __invoke(
        StoreBroadcastAction $action,
        StoreBroadcastRequest $request,
        Authenticatable $admin
    ): JsonResponse {
        return (new BroadcastResource($action->handle($admin, $request->all())))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
}
