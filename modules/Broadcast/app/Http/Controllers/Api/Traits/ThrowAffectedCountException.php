<?php

namespace Modules\Broadcast\Http\Controllers\Api\Traits;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

trait ThrowAffectedCountException
{
    /**
     * @param int $count
     *
     * @return ValidationException
     */
    private function createException(int $count): ValidationException
    {
        /** @var ValidationRules $validationRules */
        $validationRules = app(ValidationRules::class);

        $validator = Validator::make(
            ['count' => $count],
            ['count' => $validationRules->only('broadcast.affected_advertisers.count', [
                'required', 'min', 'integer',
            ])],
            ['count.min' => __('validation.custom.broadcast.segment_group.affected_advertisers.count.min')],
        );

        return new ValidationException($validator);
    }

    /**
     * @param int $count
     *
     * @return bool
     */
    private function isNotEnough(int $count): bool
    {
        return $count === 0;
    }
}
