<?php

namespace Modules\Broadcast\Models;

class BroadcastChannel
{
    public const ALL = 'all';
    public const EMAIL = 'email';
    public const IN_APP_NOTIFICATION = 'in_app_notification';
    public const NONE = 'none';

    public const CHANNELS = [
        'all'    => self::ALL,
        'email'  => self::EMAIL,
        'in_app' => self::IN_APP_NOTIFICATION,
    ];

    /**
     * @param Broadcast $broadcast
     *
     * @return string
     */
    public static function getChannel(Broadcast $broadcast): string
    {
        if ($broadcast->hasEmail() && $broadcast->hasNotification()) {
            return BroadcastChannel::ALL;
        }

        if ($broadcast->hasEmail()) {
            return BroadcastChannel::EMAIL;
        }

        if ($broadcast->hasNotification()) {
            return BroadcastChannel::IN_APP_NOTIFICATION;
        }

        return BroadcastChannel::ALL;
    }
}
