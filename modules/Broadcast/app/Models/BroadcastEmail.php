<?php

namespace Modules\Broadcast\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Broadcast\Models\Traits\HtmlStyles;

/**
 * @property integer        $id
 * @property string         $subject
 * @property string         $header
 * @property string|null    $icon
 * @property string         $body
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class BroadcastEmail extends Model
{
    use HtmlStyles;

    /**
     * @var array
     */
    protected $fillable = [
        'subject',
        'header',
        'icon',
        'body',
    ];
}
