<?php

namespace Modules\Broadcast\Models\Segment;

use Illuminate\Database\Eloquent\Model;
use Modules\Broadcast\Models\Traits\HasDate;
use Modules\Broadcast\Models\Traits\BelongsToManyRecipients;

/**
 * @property integer        $id
 * @property string         $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class BroadcastSegmentGroup extends Model
{
    use BelongsToManyRecipients,
        HasDate;

    /**
     * All advertisers on the platform
     */
    public const ALL_ADVERTISERS = 'all_advertisers';

    /**
     * Selected advertisers on the platform
     */
    public const SELECTED_ADVERTISERS = 'selected_advertisers';

    /**
     * Affected advertisers on the platform based on time period
     */
    public const AFFECTED_ADVERTISERS = 'affected_advertisers';

    public const ID_ALL_ADVERTISERS = 1;
    public const ID_SELECTED_ADVERTISERS = 2;
    public const ID_AFFECTED_ADVERTISERS = 3;

    public const SEGMENTS = [
        self::ID_ALL_ADVERTISERS,
        self::ID_SELECTED_ADVERTISERS,
        self::ID_AFFECTED_ADVERTISERS,
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @return bool
     */
    public function isSelectedAdvertisers(): bool
    {
        return $this->id === self::ID_SELECTED_ADVERTISERS;
    }

    /**
     * @return bool
     */
    public function isAffectedAdvertisers(): bool
    {
        return $this->id === self::ID_AFFECTED_ADVERTISERS;
    }
}
