<?php

namespace Modules\Broadcast\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Broadcast\Models\Traits\HasBroadcasts;

/**
 * @property integer $id
 * @property string  $name
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class BroadcastStatus extends Model
{
    use HasBroadcasts;

    /**
     * Broadcast just created and in draft state.
     */
    public const DRAFT = 'draft';

    /**
     * This status indicates that a broadcast has scheduled to be send to advertisers.
     */
    public const SCHEDULED = 'scheduled';

    /**
     * This status indicates that a broadcast has sent all notifications.
     */
    public const SENT = 'sent';

    /**
     * This status shall be set when a broadcast starts to sent notifications.
     */
    public const PROCESSING = 'processing';

    /**
     * This status indicates that a broadcast job has failed.
     */
    public const FAILED = 'failed';

    public const ID_DRAFT = 1;
    public const ID_SCHEDULED = 2;
    public const ID_PROCESSING = 3;
    public const ID_SENT = 4;
    public const ID_FAILED = 5;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['name'];
}
