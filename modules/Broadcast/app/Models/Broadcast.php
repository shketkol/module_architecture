<?php

namespace Modules\Broadcast\Models;

use App\Traits\State;
use Illuminate\Database\Eloquent\Model;
use Modules\Broadcast\Models\Traits\BelongsToEmail;
use Modules\Broadcast\Models\Traits\BelongsToManyGroups;
use Modules\Broadcast\Models\Traits\BelongsToNotification;
use Modules\Broadcast\Models\Traits\BelongsToStatus;
use Modules\Broadcast\Models\Traits\Channel;
use Modules\Broadcast\Models\Traits\Editable;
use Modules\User\Models\Traits\BelongsToUser;

/**
 * @property integer        $id
 * @property string         $name
 * @property integer        $user_id
 * @property integer        $status_id
 * @property integer        $email_id
 * @property integer        $notification_id
 * @property \Carbon\Carbon $schedule_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Broadcast extends Model
{
    use BelongsToStatus,
        BelongsToUser,
        BelongsToNotification,
        BelongsToEmail,
        BelongsToManyGroups,
        Channel,
        Editable,
        State;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'status_id',
        'email_id',
        'notification_id',
        'schedule_date',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'schedule_date' => 'datetime',
    ];

    /**
     * Broadcast state's flow.
     *
     * @var string
     */
    protected $flow = 'broadcast';
}
