<?php

namespace Modules\Broadcast\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\Broadcast\Models\Segment\BroadcastSegmentDate;

/**
 * @property BroadcastSegmentDate $date
 */
trait HasDate
{
    /**
     * @return HasOne
     */
    public function date(): HasOne
    {
        return $this
            ->hasOne(BroadcastSegmentDate::class, 'group_id')
            ->where('broadcast_id', '=', $this->getOriginal('pivot_broadcast_id'));
    }
}
