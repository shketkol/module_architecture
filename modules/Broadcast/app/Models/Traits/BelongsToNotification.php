<?php

namespace Modules\Broadcast\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Broadcast\Models\BroadcastNotification;

/**
 * @property BroadcastNotification $notification
 */
trait BelongsToNotification
{
    /**
     * @return BelongsTo
     */
    public function notification(): BelongsTo
    {
        return $this->belongsTo(BroadcastNotification::class, 'notification_id');
    }

    /**
     * @return bool
     */
    public function hasNotification(): bool
    {
        return !is_null($this->notification_id);
    }
}
