<?php

namespace Modules\Broadcast\Models\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Broadcast\Models\Broadcast;

/**
 * @property Broadcast[] $broadcasts
 */
trait HasBroadcasts
{
    /**
     * @return HasMany
     */
    public function broadcasts(): HasMany
    {
        return $this->hasMany(Broadcast::class, 'status_id');
    }
}
