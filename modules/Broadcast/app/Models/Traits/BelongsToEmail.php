<?php

namespace Modules\Broadcast\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Broadcast\Models\BroadcastEmail;

/**
 * @property BroadcastEmail $email
 */
trait BelongsToEmail
{
    /**
     * @return BelongsTo
     */
    public function email(): BelongsTo
    {
        return $this->belongsTo(BroadcastEmail::class, 'email_id');
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !is_null($this->email_id);
    }
}
