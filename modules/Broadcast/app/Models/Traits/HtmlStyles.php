<?php

namespace Modules\Broadcast\Models\Traits;

use App\Helpers\HtmlHelper;
use KubAT\PhpSimple\HtmlDomParser;

trait HtmlStyles
{
    /**
     * @return string
     */
    public function getMailHtmlMarkDown(): string
    {
        $dom = HtmlDomParser::str_get_html($this->body);
        $tagsToStyle = ['a', 'p'];

        foreach ($tagsToStyle as $tagToStyle) {
            $tags = $dom->find($tagToStyle);
            foreach ($tags as $tag) {
                $tag->setAttribute('style', HtmlHelper::getMailTagCommonStyles($tagToStyle));
            }
        }

        return $dom->outertext;
    }
}
