<?php

namespace Modules\Broadcast\Repositories;

use App\Repositories\Repository;
use Modules\Broadcast\Models\Broadcast;

class BroadcastRepository extends Repository
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * @return string
     */
    public function model(): string
    {
        return Broadcast::class;
    }
}
