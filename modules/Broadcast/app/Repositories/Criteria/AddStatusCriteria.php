<?php

namespace Modules\Broadcast\Repositories\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddStatusCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private $statusIds;

    /**
     * @param array $statusIds
     */
    public function __construct(array $statusIds)
    {
        $this->statusIds = $statusIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Model|Builder       $model
     * @param RepositoryInterface $repository
     *
     * @return Builder
     */
    public function apply($model, RepositoryInterface $repository): Builder
    {
        return $model->whereIn('broadcasts.status_id', $this->statusIds);
    }
}
