# syntax = docker/dockerfile:1.0-experimental

FROM php:7.4-fpm-alpine

ARG DA_DEBUG

RUN apk add --no-cache git unzip libxml2 libxml2-dev libpng-dev libzip-dev readline-dev gettext-dev mediainfo ffmpeg groff freetype-dev libpng-dev bash libjpeg-turbo-dev icu-dev php7-pecl-apcu \
    && docker-php-ext-install -j$(nproc) bcmath calendar fileinfo gd gettext iconv json pcntl pdo pdo_mysql mysqli soap tokenizer zip \
    && docker-php-ext-configure gd --with-jpeg=/usr/lib --with-freetype=/usr/include/freetype2 \
    && docker-php-ext-install gd intl \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=1.10.15 \
    && if [ "$DA_DEBUG" = "true" ]; then apk add --no-cache $PHPIZE_DEPS && pecl install xdebug-2.9.8 && docker-php-ext-enable xdebug; fi

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php
ENV GIT_SSL_NO_VERIFY=true
