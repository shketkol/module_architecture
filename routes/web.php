<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\URL;

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', 'ShowLandingController')->name('landing');
    Route::get('/login', '\Modules\Auth\Http\Controllers\LoginController')->name('login.form');
    Route::get('/admin-auth/sign-in', '\Modules\Auth\Http\Controllers\AdminLoginController@login')->name('admin-sign-in');
    Route::get('/login-failed', '\Modules\Auth\Http\Controllers\LoginFailedController')->name('login-failed');
    Route::get('/signup', '\Modules\Auth\Http\Controllers\RegisterController')->name('register.form');
    Route::get('/signup/{step?}', '\Modules\Auth\Http\Controllers\RegisterController')->name('register');
    Route::get('/lock', '\Modules\Auth\Http\Controllers\LockController')->name('lock');
    Route::get('/health-check', 'HealthcheckController')->name('healthcheck')->middleware('healthcheck');
});

Route::get('/admin-login', '\Modules\Auth\Http\Controllers\AdminLoginController@show')
    ->middleware('can_open_admin_login')
    ->name('admin-login');

Route::middleware(['auth:web'])->group(function () {
    Route::get('/admin-login-restricted', '\Modules\Auth\Http\Controllers\AdminLoginRestrictedController@show')->name('admin-login-restricted');

    Route::get('/admin-attempt-login', '\Modules\Auth\Http\Controllers\AdminAttemptLoginController@show')->name('admin-attempt-login');
});

Route::get('stylemap', 'FrontendController@stylemap')->name('stylemap')->middleware('open_env');

Route::get('mockups', 'MockupsController@index')->name('mockups')->middleware('open_env');

Route::get('/admin-auth/sign-out', '\Modules\Auth\Http\Controllers\AdminLogoutController')->name('admin-logout')->middleware('guest');
