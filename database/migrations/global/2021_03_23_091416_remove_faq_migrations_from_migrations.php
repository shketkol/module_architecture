<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFaqMigrationsFromMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $faqMigrations = [
            '2020_07_28_151546_change_answer_in_default_faq_item',
            '2019_06_06_105632_add_new_column_order_to_faq_item',
            '2019_06_06_105532_add_new_column_order_to_faq_section',
            '2019_05_27_102210_update_faq_item_add_category',
            '2019_05_27_101807_create_faq_section_table',
            '2019_05_27_101800_create_faq_item_table',
            '2019_05_27_101226_drop_old_faq_schema',
        ];

        DB::table('migrations')->whereIn('migration' , $faqMigrations)->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // do nothing
    }
}
