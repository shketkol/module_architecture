<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Modules\Notification\Models\EmailIcon;

class EmailIconTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $icons = [
            [
                'name' => EmailIcon::TYPE_CANCEL_WINDOW,
            ],
            [
                'name' => EmailIcon::TYPE_CASH,
            ],
            [
                'name' => EmailIcon::TYPE_FINISH,
            ],
            [
                'name' => EmailIcon::TYPE_LOCK,
            ],
            [
                'name' => EmailIcon::TYPE_MAIL,
            ],
            [
                'name' => EmailIcon::TYPE_MARKED,
            ],
            [
                'name' => EmailIcon::TYPE_NOTICE,
            ],
            [
                'name' => EmailIcon::TYPE_NOTICE_WINDOW,
            ],
            [
                'name' => EmailIcon::TYPE_PAUSE,
            ],
            [
                'name' => EmailIcon::TYPE_PLAY,
            ],
            [
                'name' => EmailIcon::TYPE_QUESTION_WINDOW,
            ],
            [
                'name' => EmailIcon::TYPE_REJECTED,
            ],
            [
                'name' => EmailIcon::TYPE_SCHEDULE,
            ],
            [
                'name' => EmailIcon::TYPE_THUMBS_UP,
            ],
        ];

        foreach ($icons as $icon) {
            EmailIcon::updateOrCreate(
                ['name' => Arr::get($icon, 'name')],
                $icon
            );
        }
    }
}
