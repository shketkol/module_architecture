<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Broadcast\Database\Seeders\BroadcastSegmentGroupsTableSeeder;
use Modules\Broadcast\Database\Seeders\BroadcastStatusesTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignRevisionStatusesTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignStepsTableSeeder;
use Modules\Campaign\Database\Seeders\InventoryCheckStatusesTableSeeder;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Invitation\Database\Seeders\InvitationPermissionsTableSeeder;
use Modules\Invitation\Database\Seeders\InvitationStatusesTableSeeder;
use Modules\Location\Database\Seeders\UsStatesTableSeeder;
use Modules\Notification\Database\Seeders\AlertTypesTableSeeder;
use Modules\Notification\Database\Seeders\ReminderTypesTableSeeder;
use Modules\Notification\Database\Seeders\NotificationCategoryTableSeeder;
use Modules\Notification\Database\Seeders\NotificationPermissionsTableSeeder;
use Modules\Notification\Database\Seeders\ReminderPermissionsTableSeeder;
use Modules\Payment\Database\Seeders\OrderPermissionsTableSeeder;
use Modules\Payment\Database\Seeders\TransactionStatusesTableSeeder;
use Modules\Report\Database\Seeders\ReportDeliveryFrequenciesTableSeeder;
use Modules\Report\Database\Seeders\ReportDeliveryTypeTableSeeder;
use Modules\Report\Database\Seeders\ReportStatusesTableSeeder;
use Modules\Report\Database\Seeders\ReportTargetingTypesTableSeeder;
use Modules\Report\Database\Seeders\ReportTypesTableSeeder;
use Modules\User\Database\Seeders\AccountTypesSeeder;
use Modules\User\Database\Seeders\ActivityTypesTableSeeder;
use Modules\User\Database\Seeders\AttachAdvertiserRoleToCreatedUsersSeeder;
use Modules\User\Database\Seeders\CreateAdminsSeeder;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserSettingsTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(UserStatusesTableSeeder::class);
        $this->call(CampaignStatusesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(AttachAdvertiserRoleToCreatedUsersSeeder::class);
        $this->call(CreativeStatusesTableSeeder::class);
        $this->call(UsStatesTableSeeder::class);
        $this->call(TimezoneTableSeeder::class);
        $this->call(CampaignStepsTableSeeder::class);
        $this->call(CreateAdminsSeeder::class);
        $this->call(TransactionStatusesTableSeeder::class);
        $this->call(AlertTypesTableSeeder::class);
        $this->call(ReminderTypesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ReportDeliveryFrequenciesTableSeeder::class);
        $this->call(ReportStatusesTableSeeder::class);
        $this->call(ReportTypesTableSeeder::class);
        $this->call(NotificationPermissionsTableSeeder::class);
        $this->call(ReminderPermissionsTableSeeder::class);
        $this->call(NotificationCategoryTableSeeder::class);
        $this->call(OrderPermissionsTableSeeder::class);
        $this->call(InvitationPermissionsTableSeeder::class);
        $this->call(InvitationStatusesTableSeeder::class);
        $this->call(AccountTypesSeeder::class);
        $this->call(BroadcastStatusesTableSeeder::class);
        $this->call(BroadcastSegmentGroupsTableSeeder::class);
        $this->call(EmailIconTableSeeder::class);
        $this->call(ActivityTypesTableSeeder::class);
        $this->call(InventoryCheckStatusesTableSeeder::class);
        $this->call(UserSettingsTableSeeder::class);
        $this->call(CampaignRevisionStatusesTableSeeder::class);
        $this->call(ReportTargetingTypesTableSeeder::class);
        $this->call(ReportDeliveryTypeTableSeeder::class);
    }
}
