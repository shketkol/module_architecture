<?php

namespace Database\Seeders\Tests;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Modules\Targeting\Models\DeviceGroup;
use Modules\Targeting\Models\Type;

class TargetingDevicesSeeder extends Seeder
{
    /**
     * Targeting devices array.
     */
    protected $devices = [
        'living_room',
        'computer',
        'mobile',
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->devices as $device) {
            DeviceGroup::updateOrCreate(['name' => $device],['name' => $device]);
        }
    }
}
