<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use App\Models\Timezone;

class TimezoneTableSeeder extends Seeder
{
    /**
     * Run seeder
     */
    public function run(): void
    {
        $timezones = [
            [
                'id'        => Timezone::ID_EASTERN,
                'name'      => 'Eastern',
                'code'      => 'ET',
                'full_code' => 'America/New_York',
            ],
            [
                'id'        => Timezone::ID_CENTRAL,
                'name'      => 'Central',
                'code'      => 'CST',
                'full_code' => 'America/Chicago',
            ],
            [
                'id'        => Timezone::ID_MOUNTAIN,
                'name'      => 'Mountain',
                'code'      => 'MST',
                'full_code' => 'America/Denver',
            ],
            [
                'id'        => Timezone::ID_PACIFIC,
                'name'      => 'Pacific',
                'code'      => 'PST',
                'full_code' => 'America/Los_Angeles',
            ],
            [
                'id'        => Timezone::ID_ALASKA,
                'name'      => 'Alaska',
                'code'      => 'AKST',
                'full_code' => 'America/Anchorage',
            ],
            [
                'id'        => Timezone::ID_HAWAII_ALEUTIAN,
                'name'      => 'Hawaii‑Aleutian',
                'code'      => 'HAST',
                'full_code' => 'America/Adak',
            ],
        ];

        foreach ($timezones as $timezone) {
            Timezone::updateOrCreate(
                ['id' => Arr::get($timezone, 'id')],
                $timezone
            );
        }
    }
}
