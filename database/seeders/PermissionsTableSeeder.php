<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Advertiser\Database\Seeders\AdvertiserPermissionsTableSeeder;
use Modules\Broadcast\Database\Seeders\BroadcastPermissionsTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignPermissionsTableSeeder;
use Modules\Creative\Database\Seeders\CreativePermissionsTableSeeder;
use Modules\Payment\Database\Seeders\PaymentPermissionsTableSeeder;
use Modules\Report\Database\Seeders\ReportPermissionsTableSeeder;
use Modules\Support\Database\Seeders\SupportPermissionsTableSeeder;
use Modules\User\Database\Seeders\SettingPermissionsTableSeeder;
use Modules\User\Database\Seeders\UserPermissionsTableSeeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        // Truncate all relations between existing permissions and roles
        $this->call(TruncatePermissionsTableSeeder::class);

        // Create new permissions
        $this->call(UserPermissionsTableSeeder::class);
        $this->call(CampaignPermissionsTableSeeder::class);
        $this->call(CreativePermissionsTableSeeder::class);
        $this->call(PaymentPermissionsTableSeeder::class);
        $this->call(AdvertiserPermissionsTableSeeder::class);
        $this->call(SupportPermissionsTableSeeder::class);
        $this->call(ReportPermissionsTableSeeder::class);
        $this->call(BroadcastPermissionsTableSeeder::class);
        $this->call(FaqSectionPermissionsTableSeeder::class);
        $this->call(SettingPermissionsTableSeeder::class);
    }
}
