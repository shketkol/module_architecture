<?php

namespace App\Actions\UpdateStatus;

use App\Actions\UpdateStatus\Iterators\CampaignIterator;
use App\Actions\UpdateStatus\Traits\GetAdvertisers;
use Illuminate\Support\Arr;
use Modules\Campaign\Actions\Notifications\UpdateCampaignStatusAction;
use Modules\Campaign\Exceptions\CampaignNotFoundException;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\User\Repositories\UserRepository;
use Psr\Log\LoggerInterface;

class CampaignUpdateAction
{
    use GetAdvertisers;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @var UpdateCampaignStatusAction
     */
    private $updateStatusAction;

    /**
     * @var CampaignIterator
     */
    private $campaignIterator;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @param UserRepository             $userRepository
     * @param CampaignRepository         $campaignRepository
     * @param LoggerInterface            $log
     * @param UpdateCampaignStatusAction $updateStatusAction
     */
    public function __construct(
        UserRepository $userRepository,
        CampaignRepository $campaignRepository,
        LoggerInterface $log,
        UpdateCampaignStatusAction $updateStatusAction
    ) {
        $this->campaignRepository = $campaignRepository;
        $this->log = $log;
        $this->updateStatusAction = $updateStatusAction;
        $this->userRepository = $userRepository;
    }

    /**
     * @return void
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \Modules\Daapi\Exceptions\StatusNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \SM\SMException
     */
    public function handle(): void
    {
        $this->campaignIterator = app(CampaignIterator::class);
        $advertisers = $this->getAdvertiserList();

        foreach ($advertisers as $advertiser) {
            if (empty($advertiser->campaigns)) {
                continue;
            }

            $campaigns = $this->campaignIterator->getCampaigns($advertiser->account_external_id);

            foreach ($campaigns as $campaign) {
                try {
                    $this->updateStatusAction->handle(
                        Arr::get($campaign, 'campaignId'),
                        Arr::get($campaign, 'campaignStatus'),
                        []
                    );
                } catch (CampaignNotFoundException $exception) {
                    $this->log->warning('Campaign not found', [
                        'externail_id' => Arr::get($campaign, 'campaignId'),
                    ]);
                }
            }
        }
    }
}
