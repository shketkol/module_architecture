<?php

namespace App\Actions\UpdateStatus\Iterators;

use Iterator;
use Modules\Daapi\Actions\ActAsAdmin;
use Modules\Haapi\Actions\Payment\OrderList;
use Modules\Haapi\DataTransferObjects\Payment\OrderListParams;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Iterators\PaginationIterator;

class OrderIterator extends PaginationIterator implements Iterator
{
    use ActAsAdmin;

    /**
     * @var OrderList
     */
    private $orderList;

    /**
     * @var int
     */
    private $adminId;

    /**
     * @var string
     */
    private $accountId;

    /**
     * @param OrderList $orderList
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function __construct(OrderList $orderList)
    {
        $this->orderList = $orderList;
        $this->adminId = $this->authAsAdmin();
    }

    /**
     * @param string $accountId
     * @return OrderIterator
     */
    public function getOrders(string $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Send request to load data.
     *
     * @return HaapiResponse
     * @throws \App\Exceptions\BaseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function load(): HaapiResponse
    {
        return $this->orderList->handle(
            $this->createParam(OrderListParams::class, [
                'accountId' => $this->accountId,
            ]),
            $this->adminId
        );
    }

    /**
     * Get key of data that should be iterated.
     *
     * @return string
     */
    protected function getResponseKey(): string
    {
        return 'orders';
    }
}
