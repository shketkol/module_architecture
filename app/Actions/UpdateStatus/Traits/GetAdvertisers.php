<?php

namespace App\Actions\UpdateStatus\Traits;

use Illuminate\Database\Eloquent\Collection;
use Modules\User\Repositories\UserRepository;

/**
 * @property UserRepository $userRepository
 */
trait GetAdvertisers
{
    /**
     * @return Collection
     */
    private function getAdvertiserList(): Collection
    {
        return $this->userRepository
            ->onlyHasExternalId()
            ->all();
    }
}
