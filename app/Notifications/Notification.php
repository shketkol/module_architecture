<?php

namespace App\Notifications;

use App\Exceptions\MethodMustBeOverriddenException;
use App\Mail\Mail;
use App\Notifications\Traits\MailCarbonCopies;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Log\Logger;
use Illuminate\Notifications\Notification as BaseNotification;
use Illuminate\Queue\SerializesModels;
use Modules\User\Models\User;

abstract class Notification extends BaseNotification implements ShouldQueue
{
    use Queueable, SerializesModels, MailCarbonCopies;

    /**
     * Name of mail class.
     *
     * @var string
     */
    protected $mailClass;

    /**
     * Build the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return Mailable
     */
    public function toMail(User $notifiable): Mailable
    {
        app(Logger::class)->info('Sending email notification.', [
            'notification_class' => get_class($this),
            'notification_id'    => $this->id,
            'user_id'            => $notifiable->id,
        ]);

        /** @var Mail $mail */
        $mail = new $this->mailClass($this->getPayload($notifiable));

        if ($this->hasBcc()) {
            $mail->bcc($this->getBcc());
        }

        if ($this->hasCc()) {
            $mail->cc($this->getCc());
        }

        $mail->to($notifiable);

        return $mail;
    }

    /**
     * Build the database notification representation of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     * @throws MethodMustBeOverriddenException
     */
    public function toArray(User $notifiable): array
    {
        app(Logger::class)->info('Sending database notification.', [
            'notification_class' => get_class($this),
            'notification_id'    => $this->id,
            'user_id'            => $notifiable->id,
        ]);

        app(Logger::class)->debug(static::getContent($this->getPayload($notifiable)));

        return $this->getPayload($notifiable);
    }

    /**
     * @param array $data
     *
     * @return string
     * @throws MethodMustBeOverriddenException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public static function getContent(array $data): string
    {
        throw MethodMustBeOverriddenException::create(__METHOD__, static::class);
    }

    /**
     * @return int
     * @throws MethodMustBeOverriddenException
     */
    public function getCategoryId(): int
    {
        throw MethodMustBeOverriddenException::create(__METHOD__, static::class);
    }

    /**
     * Get notification payload.
     *
     * @param User $notifiable
     *
     * @return array
     */
    abstract protected function getPayload(User $notifiable): array;
}
