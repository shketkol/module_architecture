<?php

namespace App\Notifications\Traits;

use Modules\User\Models\User;

trait AdvertiserNotification
{
    /**
     * Get the notification's channels.
     *
     * @param User $notifiable
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function via(User $notifiable): array
    {
        return ['mail', 'database'];
    }
}
