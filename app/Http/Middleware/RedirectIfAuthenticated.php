<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /*** User and Admin guards to check by default */
    private const USER_GUARD = 'web';
    private const ADMIN_GUARD = 'admin';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!$this->isAuthorized($guard)) {
            return $next($request);
        }

        $redirectPath = route('campaigns.index');

        $user = $this->retrieveAuthorizedUser($guard);
        if ($user && $user->isAdmin()) {
            $redirectPath = route('advertisers.index');
        }

        if ($request->expectsJson()) {
            return response()->json(['data' => [
                'redirect' => $redirectPath,
                'message'  => trans('auth::messages.not_guest'),
            ]]);
        }

        return redirect($redirectPath);
    }

    /**
     * @param $guard
     * @return bool
     */
    private function isAuthorized($guard): bool
    {
        if ($guard) {
            return Auth::guard($guard)->check();
        }

        return Auth::guard(self::USER_GUARD)->check() || Auth::guard(self::ADMIN_GUARD)->check();
    }

    /**
     * @param $guard
     * @return mixed
     */
    private function retrieveAuthorizedUser($guard)
    {
        if ($guard) {
            return Auth::guard($guard)->user();
        }

        return Auth::guard(self::USER_GUARD)->user() ?: Auth::guard(self::ADMIN_GUARD)->user();
    }
}
