<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class HealthCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     *
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (in_array($request->get('token'), config('health.config.tokens'))) {
            return $next($request);
        }

        throw new AccessDeniedHttpException();
    }
}
