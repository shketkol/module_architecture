<?php

namespace App\Http\Middleware;

use App\Helpers\EnvironmentHelper;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'api/invitations/validate-code',
    ];

    /**
     * Increased logging for selected routes
     *
     * @var array
     */
    protected $verboseLogging = [
        'signup',
        'login',
        'api/invitations/validate-code',
    ];

    /**
     * Handle an incoming request.
     *
     * @param Request  $request
     * @param \Closure $next
     *
     * @return mixed
     *
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function handle($request, \Closure $next)
    {
        $uri = $this->inVerboseLoggingArray($request);
        if ($uri && !$this->runningUnitTests()) {
            $this->verboseLogging($request, $uri);
        }

        return parent::handle($request, $next);
    }

    /**
     * Determine if the request has a URI that should pass through increased verbose logging.
     *
     * @param Request $request
     *
     * @return false|string
     */
    protected function inVerboseLoggingArray(Request $request)
    {
        foreach ($this->verboseLogging as $uri) {
            if ($uri !== '/') {
                $uri = trim($uri, '/');
            }

            if ($request->fullUrlIs($uri) || $request->is($uri)) {
                return $uri;
            }
        }

        return false;
    }

    /**
     * @param Request $request
     * @param string  $uri
     */
    protected function verboseLogging(Request $request, string $uri)
    {
        $this->logger()->info('[CSRF middleware]', [
            'uri'                 => $uri,
            'method'              => $request->server('REQUEST_METHOD'),
            'csrf_token'          => [
                'from_input'   => $request->input('_token'),
                'from_header'  => $request->header('X-CSRF-TOKEN'),
                'chosen'       => $this->getTokenFromRequest($request),
                'from_session' => $request->session()->token(),
                'match'        => $this->tokensMatch($request),
            ],
            'headers'             => [
                'user_agent'    => $request->server('HTTP_USER_AGENT'),
                'referrer'      => $request->server('HTTP_REFERER'),
                'cache_control' => $request->server('HTTP_CACHE_CONTROL'),
                'pragma'        => $request->server('HTTP_PRAGMA'),
            ],
            'aws_ec2_instance_id' => [
                'metadata' => $this->getInstanceId(),
                'env'      => env('AWS_EC2_INSTANCE_ID'),
            ],
        ]);
    }

    /**
     * @return string
     */
    private function getInstanceId(): string
    {
        if (EnvironmentHelper::isLocalEnv()) {
            return '';
        }

        try {
            return file_get_contents('http://169.254.169.254/latest/meta-data/instance-id');
        } catch (\Throwable $exception) {
            $this->logger()->warning('AWS metadata not fetched.', [
                'reason' => $exception->getMessage(),
            ]);
        }

        return '';
    }

    /**
     * @return LoggerInterface
     */
    private function logger(): LoggerInterface
    {
        return app(LoggerInterface::class);
    }
}
