<?php

namespace App\Http\Middleware;

use App\Helpers\EnvironmentHelper;
use Closure;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CheckOpenEnv
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     *
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (EnvironmentHelper::isOpenEnv()) {
            return $next($request);
        }

        throw new NotFoundHttpException();
    }
}
