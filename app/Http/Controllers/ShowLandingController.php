<?php

namespace App\Http\Controllers;

use App\Helpers\EnvironmentHelper;
use Illuminate\View\View;

class ShowLandingController extends Controller
{
    /**
     * Show landing page.
     *
     * @return View|\Illuminate\Http\RedirectResponse
     */
    public function __invoke()
    {
        if (EnvironmentHelper::isMockedLandingEnv()) {
            return view('landing');
        }

        return redirect(config('auth.redirect.url'));
    }
}
