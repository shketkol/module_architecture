<?php

namespace App\Http\Controllers;

use App\DataTable\ExternalCardFactory;
use Illuminate\Http\Request;

class ExternalCardController extends Controller
{
    /**
     * @param ExternalCardFactory $cardFactory
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\DataTable\Exceptions\ExternalCardNotFoundException
     */
    public function __invoke(ExternalCardFactory $cardFactory, Request $request)
    {
        $card = $cardFactory->createFromRequest($request);
        return response($card->toArray());
    }
}
