<?php

namespace App\Http\Controllers;

use App\DataTable\Actions\CheckStatusAction;
use App\DataTable\DataTableFactory;
use App\DataTable\Exceptions\DataTableNotFoundException;
use App\DataTable\Exceptions\FilterNotFoundException;
use App\DataTable\Filters\DataTableFilterFactory;
use Illuminate\Http\Request;

class DataTableController extends Controller
{
    /**
     * Data Tables
     *
     * @param DataTableFactory $tableFactory
     * @param Request          $request
     *
     * @return mixed
     * @throws DataTableNotFoundException
     */
    public function index(DataTableFactory $tableFactory, Request $request)
    {
        $dataTable = $tableFactory->createFromRequest($request);
        return $dataTable->toResponse();
    }

    /**
     * Datatable filters
     *
     * @param Request                $request
     * @param DataTableFilterFactory $factory
     *
     * @return mixed
     * @throws FilterNotFoundException
     */
    public function filters(Request $request, DataTableFilterFactory $factory)
    {
        $filter = $factory->createFromRequest($request);
        return $filter->toResponse();
    }

    /**
     * Get available cards
     *
     * @param Request          $request
     * @param DataTableFactory $factory
     *
     * @return mixed
     * @throws DataTableNotFoundException
     */
    public function cards(Request $request, DataTableFactory $factory)
    {
        $dataTable = $factory->createFromRequest($request);
        return $dataTable->getCards();
    }

    /**
     * Check rows statuses
     *
     * @param string            $table
     * @param Request           $request
     * @param CheckStatusAction $action
     * @param DataTableFactory  $factory
     *
     * @return mixed
     * @throws DataTableNotFoundException
     */
    public function checkStatuses(
        string $table,
        Request $request,
        CheckStatusAction $action,
        DataTableFactory $factory
    ) {
        if ($rowsIds = $request->get('processing_ids', [])) {
            //Check datatable policy
            $factory->createFromRequest($request);

            return response()->json(['data' => $action->handle($table, $rowsIds)]);
        }
    }
}
