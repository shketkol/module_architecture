<?php

namespace App\Http;

use App\Http\Middleware\CheckOpenEnv;
use App\Http\Middleware\CanOpenAdminLogin;
use App\Http\Middleware\HealthCheckMiddleware;
use App\Services\RecaptchaService\Http\Middleware\VerifyRecaptcha;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Modules\Faq\Http\Middleware\FaqRoutes;
use Modules\Invitation\Http\Middleware\InvitationEnabled;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            'throttle:120,1,web',
        ],

        'api'                      => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            'throttle:60,1',
            'bindings',
        ],

        // It isn't possible to override throttle declared in Kernel
        // That's why throttle must be set in route middleware
        // See more at the "modules/Creative/routes/throttle.php"
        'api_with_custom_throttle' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                  => \App\Http\Middleware\Authenticate::class,
        'auth.basic'            => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings'              => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers'         => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can'                   => \Illuminate\Auth\Middleware\Authorize::class,
        'guest'                 => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'recaptcha'             => VerifyRecaptcha::class,
        'signed'                => \Modules\Daapi\Http\Middleware\ValidateSignature::class,
        'throttle'              => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified'              => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'logging'               => \Modules\Daapi\Http\Middleware\RequestLogger::class,
        'client'                => \Modules\Daapi\Http\Middleware\CheckSharedKey::class,
        'valid'                 => \Modules\Daapi\Http\Middleware\ValidRequest::class,
        'open_env'              => CheckOpenEnv::class,
        'can_open_admin_login'  => CanOpenAdminLogin::class,
        'healthcheck'           => HealthCheckMiddleware::class,
        'validate.notification' => \Modules\Daapi\Http\Middleware\ValidateNotificationJsonRequest::class,
        'validate.callback'     => \Modules\Daapi\Http\Middleware\ValidateCallbackJsonRequest::class,
        'validate.structure'    => \Modules\Daapi\Http\Middleware\ValidateStructureJsonRequest::class,
        'json.response'         => \Modules\Daapi\Http\Middleware\ForceJsonResponse::class,
        'faq.routes'            => FaqRoutes::class,
        'invitation.enabled'    => InvitationEnabled::class,
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\Authenticate::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
        \App\Http\Middleware\RedirectIfAuthenticated::class,
    ];
}
