<?php

namespace App\Http\Requests;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Support\Arr;

class SearchRequest extends Request
{
    /**
     * @var string
     */
    protected $queryParam = 'query';

    /**
     * @var string
     */
    protected $filterRuleName = 'search.filter_query';

    /**
     * @var string
     */
    protected $validationRuleName = 'search.query';

    /**
     * @var array
     */
    protected $validationRulesArray = ['nullable', 'string', 'max'];

    /**
     * @param ValidationRules $validationRules
     *
     * @return array
     */
    public function rules(ValidationRules $validationRules): array
    {
        return [
            $this->queryParam => $validationRules->only(
                $this->getValidationRuleName(),
                $this->validationRulesArray
            ),
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->replace([$this->queryParam => $this->prepareQuery()]);
    }

    /**
     * Strip all bad characters from query so user would never had an error
     * But search driver (ElasticSearch) never received bad characters either
     *
     * @return string
     */
    private function prepareQuery(): string
    {
        $query = $this->input($this->queryParam);

        /** @var ValidationRules $validationRules */
        $validationRules = app(ValidationRules::class);
        $rules = $validationRules->get($this->getFilterRuleName());
        $regex = Arr::get($rules, 'regex');

        // replace for space " " not for "" empty string
        // so strings like "Jackson-Ms" become "Jackson Ms" and not "JacksonMs"
        $query = preg_replace($regex, ' ', $query);

        // then clear some extra spaces if we got some  so "  " become " "
        $query = preg_replace('/\s\s+/', ' ', $query);

        return $query;
    }

    /**
     * @return string
     */
    protected function getFilterRuleName(): string
    {
        return sprintf('%s_%s', $this->filterRuleName, $this->getSearchDriver());
    }

    /**
     * @return string
     */
    protected function getValidationRuleName(): string
    {
        return sprintf('%s_%s', $this->validationRuleName, $this->getSearchDriver());
    }

    /**
     * @return string
     */
    protected function getSearchDriver(): string
    {
        return config('scout.driver', 'mysql');
    }
}
