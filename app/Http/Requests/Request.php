<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Request
 * @package App\Http\Requests
 */
class Request extends FormRequest
{
    /**
     * Base request data mapping
     *
     * @return array
     */
    public function toData():array
    {
        return $this->all();
    }
}
