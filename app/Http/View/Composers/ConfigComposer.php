<?php

namespace App\Http\View\Composers;

use App\Console\Commands\Frontend\Traits\Config;
use Illuminate\View\View;

class ConfigComposer
{
    use Config;

    /**
     * Variable name used in views.
     */
    const VAR_NAME = 'config';

    /**
     * View composer will be attached to these views.
     *
     * @var array
     */
    public static $views = [
        'layouts.footer.config',
    ];

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     * @throws \Exception
     */
    public function compose(View $view)
    {
        $configs = $this->getConfigs();

        $view->with(self::VAR_NAME, $configs);
    }
}
