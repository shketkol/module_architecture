<?php

namespace App;

use App\Traits\Maintenance;
use Illuminate\Foundation\Application as BaseApplication;

class Application extends BaseApplication
{
    use Maintenance;

    /**
     * Returns whether the application is down for maintenance.
     *
     * Laravel maintenance mode is overridden to use the cache instead of the filesystem,
     * allowing us to treat cache as a global distributed lock so that
     * maintenance mode is propagated to multiple servers.
     *
     * @return bool Whether the app is in maintenance mode.
     */
    public function isDownForMaintenance(): bool
    {
        return $this->isDown();
    }
}
