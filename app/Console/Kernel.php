<?php

namespace App\Console;

use App\Console\Commands\CronTest;
use App\Console\Commands\Frontend\BuildConfig;
use App\Console\Commands\Frontend\BuildFrontend;
use App\Console\Commands\Frontend\BuildRoutes;
use App\Console\Commands\Frontend\BuildTranslations;
use App\Console\Commands\Frontend\BuildValidation;
use App\Console\Commands\Generator\ModuleMakeCommand;
use App\Console\Commands\Storage\CleanupCreativesCommand;
use App\Console\Commands\Testing\SendingTestEmails;
use App\Console\Commands\UpdateStatusCommand;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Demo\Commands\AccountUpdateCallbackCommand;
use Modules\Demo\Commands\CampaignCreateCallbackCommand;
use Modules\Demo\Commands\CampaignUpdateCallbackCommand;
use Modules\Demo\Commands\CreativeUpdateCallbackCommand;
use Modules\Demo\Commands\StatusNotificationCallbackCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        BuildFrontend::class,
        BuildRoutes::class,
        BuildTranslations::class,
        BuildValidation::class,
        BuildConfig::class,

        /**
         * Create module command
         */
        ModuleMakeCommand::class,

        CronTest::class,
        UpdateStatusCommand::class,
        CleanupCreativesCommand::class,

        /**
         * For Demo env
         */
        AccountUpdateCallbackCommand::class,
        CampaignCreateCallbackCommand::class,
        CampaignUpdateCallbackCommand::class,
        CreativeUpdateCallbackCommand::class,
        StatusNotificationCallbackCommand::class,

        /**
         * Testing
         */
        SendingTestEmails::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $midnight = $this->formatTime($this->getMidnightTime());

        // Alerts (email notifications)
        $schedule->command('platform:alert:send')->everyThirtyMinutes()->withoutOverlapping()->onOneServer();

        // Delete unused creatives
        $schedule->command('platform:creative:clear')
            ->dailyAt($midnight)
            ->withoutOverlapping()
            ->onOneServer();

        if (env('TARGETING_UPDATE_ENABLED', false)) {
            $this->scheduleTargetingUpdate($schedule);
            $this->scheduleTargetingImport($schedule);
        }

        $this->reportCommands($schedule, $midnight);
        $this->broadcastCommands($schedule);
        $this->activitiesCommands($schedule);
        $this->invitationCommands($schedule);

        /**
         * For Demo env
         */
        if (config('general.demo.enabled')) {
            $this->demoCommands($schedule);
        }
    }

    /**
     * @param Schedule $schedule
     * @param string   $midnight
     */
    private function reportCommands(Schedule $schedule, string $midnight): void
    {
        // Schedule reports
        $schedule->command('platform:report:scheduled:send daily')
            ->dailyAt($midnight)
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command('platform:report:scheduled:send weekly')
            ->dailyAt($midnight)
            ->withoutOverlapping()
            ->onOneServer();

        $schedule->command('platform:report:scheduled:send monthly')
            ->monthlyOn(1, $midnight)
            ->withoutOverlapping()
            ->onOneServer();

        // Delete old report files
        $schedule->command('platform:report:delete_old')
            ->dailyAt($midnight)
            ->withoutOverlapping()
            ->onOneServer();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Get actual midnight time (application is running in UTC, while cron should work in ET).
     *
     * @return Carbon
     */
    protected function getMidnightTime(): Carbon
    {
        $targetTimezone = config('date.default_timezone_full_code'); // America/New_York
        $appTimezone = config('app.timezone'); // UTC
        $shift = config('report.midnight_shift_hours');

        return Carbon::now($targetTimezone)
            ->startOfDay()
            ->addHours($shift)
            ->setTimezone($appTimezone);
    }

    /**
     * @param Carbon $datetime
     *
     * @return string
     */
    protected function formatTime(Carbon $datetime): string
    {
        return $datetime->format('H:i');
    }

    /**
     * @param Schedule $schedule
     */
    protected function scheduleTargetingUpdate(Schedule $schedule): void
    {
        $midnight = $this->getMidnightTime();
        $minutes = 5;

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:age-groups:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:cities:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:dma:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:states:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:zipcodes:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:genres:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:audiences:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:devices:import --update')->dailyAt($time)->withoutOverlapping();
    }

    /**
     * @param Schedule $schedule
     */
    protected function scheduleTargetingImport(Schedule $schedule): void
    {
        $midnight = $this->getMidnightTime();
        // we don't need to update fresh imported records they are already the newest
        // that's why we add delay to run update first and after that run import
        $midnight->addMinutes(60);
        $minutes = 5;

        // Import campaign targeting values
        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:types:import --update')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:age-groups:import')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:cities:import')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:dma:import')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:states:import')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:zipcodes:import')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:genres:import')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:audiences:import')->dailyAt($time)->withoutOverlapping();

        $time = $this->formatTime($midnight->addMinutes($minutes));
        $schedule->command('targeting:devices:import')->dailyAt($time)->withoutOverlapping();
    }

    /**
     * @param Schedule $schedule
     */
    protected function demoCommands(Schedule $schedule): void
    {
        // Active paused users
        $schedule->command('demo:callbacks:account_update')
            ->everyMinute()
            ->withoutOverlapping();

        // Emulate campaign create callback
        $schedule->command('demo:callbacks:campaign_create')
            ->everyMinute()
            ->withoutOverlapping();

        // Emulate campaign update callback
        $schedule->command('demo:callbacks:campaign_update')
            ->everyMinute()
            ->withoutOverlapping();

        // Emulate creative update callback (Pending campaigns)
        $schedule->command('demo:callbacks:creative_update')
            ->everyMinute()
            ->withoutOverlapping();

        // Emulate creative update callback (Ready campaigns)
        $schedule->command('demo:callbacks:creative_update --ready')
            ->everyMinute()
            ->withoutOverlapping();

        // Emulate status notification (campaigns)
        $schedule->command('demo:notification:status_update --campaign')
            ->everyMinute()
            ->withoutOverlapping();

        // Emulate status notification (creatives)
        $schedule->command('demo:notification:status_update --creative')
            ->everyMinute()
            ->withoutOverlapping();

        // Cleanup advertisers and all their data
        $schedule->command('demo:cleanup')
            ->everyMinute()
            ->withoutOverlapping();
    }

    /**
     * @param Schedule $schedule
     */
    private function broadcastCommands(Schedule $schedule): void
    {
        $gap = 5; // just to be sure that cron would get broadcasts in time
        $schedule->command('broadcast:start')
            ->hourlyAt($gap)
            ->withoutOverlapping()
            ->onOneServer();
    }

    /**
     * @param Schedule $schedule
     */
    private function activitiesCommands(Schedule $schedule): void
    {
        $midnight = $this->getMidnightTime();

        // to not to conflict with other jobs
        $time = $this->formatTime($midnight->addHours(2)->addMinutes(15));

        $schedule->command('activities:cleanup')
            ->dailyAt($time)
            ->withoutOverlapping()
            ->onOneServer();

        $gap = 10; // to not to conflict with other jobs
        $schedule->command('activities:mark-as-logged-out')
            ->hourlyAt($gap) // cron period MUST be same as MarkAsLoggedOutCommand::CRON_PERIOD_MINUTES
            ->withoutOverlapping()
            ->onOneServer();
    }

    /**
     * @param Schedule $schedule
     */
    private function invitationCommands(Schedule $schedule): void
    {
        $schedule->command('platform:invite:resend')
            ->daily()
            ->withoutOverlapping()
            ->onOneServer();
    }
}
