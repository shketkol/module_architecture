<?php

namespace App\Console\Commands\Frontend\Traits;

use Illuminate\Support\Arr;

trait Config
{
    /**
     * List of all configs that should be passed to frontend.
     *
     * @var array
     */
    protected $configs = [
        'datatables',
        'activities.database.store_months',
        'analytics',
        'campaign.status',
        'campaign.wizard',
        'campaign.inventory_expiration',
        'campaign.enable_timezone_select',
        'currency',
        'date.format.element_datepicker_format',
        'date.format.moment_datepicker_format',
        'date.format.datepicker_display_format',
        'date.format.js',
        'date.format.js_time',
        'date.format.js_datetime_with_tz',
        'date.format.js_parse',
        'date.format.orderDate',
        'date.format.js_full_date_time',
        'date.default_timezone_code',
        'date.default_timezone_full_code',
        'date.schedule_broadcast',
        'format',
        'general.company_name',
        'general.company_support_email',
        'general.internal_support_email',
        'general.company_support_phone',
        'general.links',
        'general.thirdparty_links',
        'general.new_local_storage_before',
        'general.env',
        'general.demo',
        'general.launch_date',
        'general.release_version',
        'general.time_widget_interval',
        'media.videos.web',
        'recaptcha.site_key',
        'cards.icons',
        'app.timezone',
        'app.env',
        'report',
        'broadcast',
        'world-pay.front_end',
        'services.sentry',
        'cognito',
        'locations',
        'devices',
        'rules.creative',
        'user.check_update_business_interval',
        'invitation.enabled',
        'search',
    ];

    /**
     * Get all configs.
     *
     * @return array
     */
    protected function getConfigs(): array
    {
        $configs = [];

        foreach ($this->configs as $config) {
            Arr::set($configs, $config, config($config));
        }

        return $configs;
    }
}
