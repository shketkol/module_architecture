<?php

namespace App\Console\Commands\Frontend;

use App\Services\ValidationRulesService\Contracts\ValidationRules;
use Illuminate\Filesystem\Filesystem;

class BuildValidation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:frontend:validation';

    /**
     * Name of the created file.
     *
     * @var string
     */
    protected $targetFilename = 'validation.json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build validation rules for frontend (put all rules into a .json file).';

    /**
     * List of all validation rules that should be passed to FE
     * (VeeValidate knows how to handle it).
     *
     * @var array
     */
    protected $availableRules = [
        'between',
        'email',
        'field',
        'image',
        'image_ratio',
        'included',
        'max',
        'max_filesize',
        'min',
        'min_video',
        'numeric',
        'regex',
        'required',
    ];

    /**
     * List of numeric rules that should be replaced to achieve
     * the same behavior on FE.
     * Start work when parent key presents in rules array.
     *
     * @var array
     */
    protected $convertCustom = [
        'numeric' => [
            'max'     => 'max_value',
            'min'     => 'min_value',
            'numeric' => 'decimal'
        ],
        'image' => [
            'image' => 'image_file'
        ]
    ];

    /**
     * @var ValidationRules
     */
    protected $validationRules;

    /**
     * BuildValidation constructor.
     * @param Filesystem $filesystem
     * @param ValidationRules $validationRules
     */
    public function __construct(Filesystem $filesystem, ValidationRules $validationRules)
    {
        $this->validationRules = $validationRules;

        parent::__construct($filesystem);
    }

    /**
     * Export data into json file.
     *
     * @return array
     */
    public function export(): array
    {
        return $this->getValidationRules();
    }

    /**
     * Get all validation rules for all the fields.
     *
     * @return array
     */
    protected function getValidationRules(): array
    {
        return $this->filterRules($this->validationRules->all());
    }

    /**
     * Return only rules available on frontend.
     * Replace numeric rules if needed.
     *
     * @param array $rules
     * @return array
     */
    protected function filterRules(array $rules): array
    {
        $filtered = [];

        foreach ($rules as $key => $value) {
            if (is_array($value) && is_string(array_keys($value)[0])) {
                $filtered[$key] = $this->filterRules($value);
                continue;
            }

            if (in_array($key, $this->availableRules)) {
                $filtered[$key] = $value;
            }
        }

        // Replace rules to VeeValidate numeric equivalents
        foreach ($this->convertCustom as $rule => $replaceRules) {
            if (array_key_exists($rule, $filtered)) {
                $filtered = $this->convertCustom($filtered, $replaceRules);
            }
        }

        return $filtered;
    }

    /**
     * Return array with replaced numeric rules
     *
     * @param $filtered
     * @param $replaceRules
     * @return array
     */
    protected function convertCustom(array $filtered, array $replaceRules): array
    {
        $oldRules = array_keys($filtered);
        foreach ($oldRules as $rule) {
            if (array_key_exists($rule, $replaceRules)) {
                $filtered[$replaceRules[$rule]] = $filtered[$rule];
                unset($filtered[$rule]);
            }
        }

        return $filtered;
    }
}
