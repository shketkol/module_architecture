<?php

namespace App\Console\Commands\Generator;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;

/**
 * Class ModuleMakeCommand
 * Creates module default structure
 *
 * @package App\Console\Commands\Generator
 */
class ModuleMakeCommand extends Command
{
    /**
     * Module name
     *
     * @var string
     */
    protected $moduleName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create default module structure';

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $filesystem;

    /**
     * Create a new command instance.
     *
     * @param \Illuminate\Filesystem\Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
    }

    /**
     * Handle command
     */
    public function handle()
    {
        $this->moduleName = $this->formatModuleName($this->argument('name'));

        $this->makeDirectory()
            ->makeDirectory('database')
            ->makeDirectory('database/migrations')
            ->makeDirectory('database/seeds')
            ->makeDirectory('database/factories')
            ->makeDirectory('app')
            ->makeDirectory('app/Actions')
            ->makeClass('app', 'ServiceProvider')
            ->makeClass('app/Actions', 'Action')
            ->makeDirectory('app/Actions/Contracts')
            ->makeClass('app/Actions/Contracts', 'Action')
            ->makeDirectory('app/DataTransferObjects')
            ->makeClass('app/DataTransferObjects', 'Data')
            ->makeDirectory('app/Exceptions')
            ->makeClass('app/Exceptions', 'Exception')
            ->makeDirectory('app/Http')
            ->makeDirectory('app/Http/Controllers')
            ->makeClass('app/Http/Controllers', 'Controller')
            ->makeDirectory('app/Http/Requests')
            ->makeClass('app/Http/Requests', 'Request')
            ->makeDirectory('app/Http/Middleware')
            ->makeClass('app/Http/Middleware', 'Middleware')
            ->makeDirectory('app/Models')
            ->makeClass('app/Models', 'Model')
            ->makeDirectory('app/Repositories')
            ->makeClass('app/Repositories', 'Repository')
            ->makeDirectory('app/Repositories/Contracts')
            ->makeClass('app/Repositories/Contracts', 'Repository')
            ->makeDirectory('config')
            ->makeFile('config')
            ->makeDirectory('resources')
            ->makeDirectory('resources/lang')
            ->makeDirectory('resources/lang/en')
            ->makeFile('resources/lang/en', 'messages')
            ->makeDirectory('resources/views')
            ->makeFile('resources/views', 'index.blade')
            ->makeDirectory('routes')
            ->makeFile('routes', 'api')
            ->makeFile('routes', 'web');
    }

    /**
     * Formats module name
     * ex. statistic => Statistic
     *
     * @param string $moduleName
     *
     * @return string
     */
    protected function formatModuleName(string $moduleName): string
    {
        return ucfirst(strtolower(trim($moduleName)));
    }

    /**
     * Get module path
     *
     * @param string $path
     *
     * @return string
     */
    protected function getModulePath(string $path = ''): string
    {
        $targetPath = explode('/', $path);

        return rtrim(
            implode(
                DIRECTORY_SEPARATOR,
                array_merge_recursive([realpath('.'), 'modules', $this->moduleName], $targetPath)
            ),
            DIRECTORY_SEPARATOR
        );
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param string $path
     *
     * @return ModuleMakeCommand
     */
    protected function makeDirectory(string $path = ''): self
    {
        if (!$this->filesystem->isDirectory($this->getModulePath($path))) {
            $this->filesystem->makeDirectory($this->getModulePath($path), 0777, true, true);
            $this->line('Created: ' . $path);
        } else {
            $this->line('Exist: ' . $path);
        }

        return $this;
    }

    /**
     * Make class from stub
     *
     * @param string $path
     * @param string $class
     *
     * @return $this
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function makeClass(string $path, string $class): self
    {
        $stub = $this->getStub($path);
        $fullClassPath = implode(DIRECTORY_SEPARATOR, [
            $this->getModulePath($path), $this->moduleName . $class,
        ]);
        file_put_contents($fullClassPath . '.php', $stub);

        $this->line('Created class: ' . $fullClassPath);

        return $this;
    }

    /**
     * Make config file
     *
     * @param string $path
     * @param string $fileName
     *
     * @return $this
     * @throws FileNotFoundException
     */
    protected function makeFile(string $path, string $fileName = ''): self
    {
        if (empty($fileName)) {
            $fileName = strtolower($this->moduleName);
        }

        $stub = $this->getStub($path);
        $fullFilePath = implode(DIRECTORY_SEPARATOR, [
                $this->getModulePath($path), $fileName,
            ]) . '.php';

        file_put_contents($fullFilePath, $stub);

        $this->line('Created file: ' . $fullFilePath);

        return $this;
    }

    /**
     * Get stub file
     *
     * @param string $path
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub(string $path): string
    {
        $targetPath = implode(DIRECTORY_SEPARATOR, [__DIR__, 'stubs', $this->getStubNameFromPath($path)]);

        if (!file_exists($targetPath)) {
            throw new FileNotFoundException(sprintf('File %s not found', $targetPath));
        }

        $stubFile = file_get_contents($targetPath);

        return $this->parseStub($stubFile);
    }

    /**
     * Parse stub file
     *
     * @param string $stubFile
     *
     * @return mixed
     */
    protected function parseStub(string $stubFile): string
    {
        return str_replace(
            'Stub',
            $this->moduleName,
            str_replace('stub', strtolower($this->moduleName), $stubFile)
        );
    }

    /**
     * Get stub name from path
     *
     * @param string $path
     *
     * @return string
     */
    protected function getStubNameFromPath(string $path): string
    {
        return strtolower(str_ireplace(DIRECTORY_SEPARATOR, '.', $path)) . '.stub';
    }
}
