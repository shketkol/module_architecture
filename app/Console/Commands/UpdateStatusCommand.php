<?php

namespace App\Console\Commands;

use App\Actions\UpdateStatus\CampaignUpdateAction;
use App\Actions\UpdateStatus\CreativeUpdateAction;
use Illuminate\Console\Command;

class UpdateStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statuses:update {--campaign} {--creative}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update status from HAAPI';

    /**
     * @var CampaignUpdateAction
     */
    private $campaignUpdateAction;

    /**
     * @var CreativeUpdateAction
     */
    private $creativeUpdateAction;

    public function __construct(
        CampaignUpdateAction $campaignUpdateAction,
        CreativeUpdateAction $creativeUpdateAction
    ) {
        parent::__construct();

        $this->campaignUpdateAction = $campaignUpdateAction;
        $this->creativeUpdateAction = $creativeUpdateAction;
    }

    /**
     * Handle command
     */
    public function handle()
    {
        $isCampaign = $this->option('campaign');
        $isCreative = $this->option('creative');

        $this->info('Start update statuses');

        if ($isCampaign) {
            $this->info('Update campaign status start');

            $this->campaignUpdateAction->handle();

            $this->info('Update campaign status finish');
        }

        if ($isCreative) {
            $this->info('Update creative status start');

            $this->creativeUpdateAction->handle();

            $this->info('Update creative status finish');
        }

        $this->info('Finish update statuses');
    }
}
