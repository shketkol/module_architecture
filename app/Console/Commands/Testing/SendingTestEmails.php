<?php

namespace App\Console\Commands\Testing;

use App\Helpers\EnvironmentHelper;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Mail\Mailer;
use Illuminate\Notifications\AnonymousNotifiable;
use Modules\Advertiser\Notifications\Advertiser\AdvertiserActivated;
use Modules\Advertiser\Notifications\Advertiser\AdvertiserDeactivated;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Notifications\Advertiser\Canceled\CampaignCanceledByAdvertiser;
use Modules\Campaign\Notifications\Admin\Canceled\CampaignCanceledByAdvertiser as AdminCampaignCancelNotification;
use Modules\Campaign\Notifications\Admin\CannotGoLive as AdminCampaignCannotGoLiveNotification;
use Modules\Campaign\Notifications\Admin\Ordered\Ordered as AdminCampaignOrderNotification;
use Modules\Campaign\Notifications\Advertiser\Canceled\CampaignCanceledByAdmin;
use Modules\Campaign\Notifications\Advertiser\CannotGoLive;
use Modules\Campaign\Notifications\Advertiser\Completed;
use Modules\Campaign\Notifications\Advertiser\EditConfirmation\Failed;
use Modules\Campaign\Notifications\Advertiser\EditConfirmation\Success;
use Modules\Campaign\Notifications\Advertiser\Ordered\Ordered;
use Modules\Campaign\Notifications\Advertiser\Paused\CampaignPausedByAdmin;
use Modules\Campaign\Notifications\Advertiser\Paused\CampaignPausedByAdvertiser;
use Modules\Campaign\Notifications\Advertiser\Resumed\CampaignResumedByAdmin;
use Modules\Campaign\Notifications\Advertiser\Resumed\CampaignResumedByAdvertiser;
use Modules\Campaign\Notifications\Advertiser\Started;
use Modules\Campaign\Repositories\CampaignRepository;
use Modules\Creative\Notifications\Admin\ForReview;
use Modules\Creative\Notifications\Admin\Rejected as AdminCreativeRejected;
use Modules\Creative\Notifications\Advertiser\Approved as AdvertiserApproved;
use Modules\Creative\Notifications\Advertiser\Missing;
use Modules\Creative\Notifications\Advertiser\Rejected as CreativeRejected;
use Modules\Creative\Repositories\CreativeRepository;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Notifications\Invite;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\Payment\Notifications\Advertiser\TransactionIsMade;
use Modules\Report\Models\ReportType;
use Modules\Report\Notifications\MissingAdsScheduledCompleted;
use Modules\Report\Notifications\ScheduledCompleted;
use Modules\Report\Repositories\ReportRepository;
use Modules\Support\Notifications\Contact;
use Modules\User\Models\UserStatus;
use Modules\User\Notifications\Admin\AccountSubmitted;
use Modules\User\Notifications\Admin\ActiveFromBadPayment as AdminActiveFromBadPayment;
use Modules\User\Notifications\Admin\BadPayment as AdminBadPayment;
use Modules\User\Notifications\Advertiser\ActiveFromBadPayment;
use Modules\User\Notifications\Advertiser\Approved as UserApproved;
use Modules\User\Notifications\Advertiser\BadPayment;
use Modules\User\Notifications\Advertiser\BadPaymentRepeatFirst;
use Modules\User\Notifications\Advertiser\BadPaymentRepeatSecond;
use Modules\User\Notifications\Advertiser\BadPaymentRepeatThird;
use Modules\User\Notifications\Advertiser\PendingAccountIsActivated;
use Modules\User\Notifications\Advertiser\Rejected;
use Modules\User\Repositories\UserRepository;

class SendingTestEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:test:emails {email} {role?} {module?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing sending emails';

    /**
     * @var string|null
     */
    protected $role;
    protected $recipient;

    protected $campaignRepository;
    protected $userRepository;
    protected $creativeRepository;
    protected $invitationRepository;
    protected $reportRepository;
    protected $mailer;

    /**
     * @param CampaignRepository   $campaignRepository
     * @param UserRepository       $userRepository
     * @param CreativeRepository   $creativeRepository
     * @param InvitationRepository $invitationRepository
     * @param ReportRepository     $reportRepository
     * @param Mailer               $mailer
     */
    public function __construct(
        CampaignRepository $campaignRepository,
        UserRepository $userRepository,
        CreativeRepository $creativeRepository,
        InvitationRepository $invitationRepository,
        ReportRepository $reportRepository,
        Mailer $mailer
    ) {
        parent::__construct();
        $this->campaignRepository = $campaignRepository;
        $this->userRepository = $userRepository;
        $this->creativeRepository = $creativeRepository;
        $this->invitationRepository = $invitationRepository;
        $this->reportRepository = $reportRepository;
        $this->mailer = $mailer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        if (!EnvironmentHelper::isOpenEnv()) {
            return;
        }

        if (!$this->argument('email')) {
            $this->error('No test email');
            return;
        }

        $this->recipient = $this->argument('email');
        $this->role = $this->argument('role');
        $module = $this->argument('module');

        if ($module) {
            $methodName = $module . 'Emails';
            $this->$methodName();

            $this->info('Done.');
            return;
        }

        $this->campaignEmails();
        $this->advertiserEmails();
        $this->creativeEmails();
        $this->inviteEmails();
        $this->paymentEmails();
        $this->reportEmails();
        $this->supportEmails();
        $this->userEmails();

        $this->info('Done.');
    }

    /**
     * Campaign module emails
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function campaignEmails(): void
    {
        $campaign = $this->getReadyCampaign();
        if (!$this->role || $this->role === 'admin') {
            $mails = [];
            $mails[] = new AdminCampaignCancelNotification($campaign, $campaign->user);
            $mails[] = new AdminCampaignOrderNotification($campaign);
            $mails[] = new AdminCampaignCannotGoLiveNotification($campaign);

            $this->sendAdminEmails($mails);
        }

        if (!$this->role || $this->role === 'advertiser') {
            $mails = [];
            $mails[] = new CampaignCanceledByAdmin($campaign, $campaign->user);
            $mails[] = new CampaignCanceledByAdvertiser($campaign, $campaign->user);
            $mails[] = new Ordered($campaign);
            $mails[] = new CampaignPausedByAdmin($campaign, $campaign->user);
            $mails[] = new CampaignPausedByAdvertiser($campaign, $campaign->user);
            $mails[] = new CampaignResumedByAdmin($campaign, $campaign->user);
            $mails[] = new CampaignResumedByAdvertiser($campaign, $campaign->user);
            $mails[] = new CannotGoLive($campaign);
            $mails[] = new Completed($campaign);
            $mails[] = new Started($campaign);
            $mails[] = new Success($this->getLiveCampaign());
            $mails[] = new Failed($this->getLiveCampaign());

            $this->sendAdvertiserEmails($mails);
        }
    }

    /**
     * Advertiser module emails
     * @return void
     */
    private function advertiserEmails(): void
    {
        if ($this->role === 'admin') {
            return;
        }

        $mails = [];
        $advertiser = $this->userRepository->findWhere(['status_id' => UserStatus::ID_ACTIVE])->first();
        $mails[] = new AdvertiserActivated($advertiser);
        $mails[] = new AdvertiserDeactivated($advertiser);

        $this->sendAdminEmails($mails);
    }

    /**
     * Creative module emails
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function creativeEmails(): void
    {

        if (!$this->role || $this->role === 'admin') {
            $mails = [];

            $creative = $this->creativeRepository->first();
            $mails[] = new ForReview($creative);
            $mails[] = new AdminCreativeRejected($creative);

            $this->sendAdminEmails($mails);
        }

        if (!$this->role || $this->role === 'advertiser') {
            $mails = [];

            $creative = $this->creativeRepository->first();
            $mails[] = new AdvertiserApproved($creative);
            $mails[] = new CreativeRejected($creative);
            $mails[] = new Missing($this->getReadyCampaign());

            $this->sendAdvertiserEmails($mails);
        }
    }

    /**
     * Invitation module emails
     * @return void
     */
    private function inviteEmails(): void
    {
        if ($this->role === 'admin') {
            return;
        }

        try {
            $invite = $this->invitationRepository->findWhere(['status_id' => InvitationStatus::ID_INVITED])->first();
            $mail = (new Invite($invite))
                ->toMail((new AnonymousNotifiable)->route(
                    'mail',
                    $this->userRepository->findWhere(['status_id' => UserStatus::ID_ACTIVE])->first()
                ));
            $mail->resetRecipients();
            $this->mailer->to($this->recipient)->send($mail);
            $this->info('Success Invite notification.');
        } catch (\Throwable $exception) {
            $this->error('Fail Invite notification.');
        }
    }

    /**
     * Payment module emails
     * @return void
     */
    private function paymentEmails(): void
    {
        if ($this->role === 'admin') {
            return;
        }
        $mails = [];
        $campaign =  $this->getReadyCampaign();
        $mails[] = new TransactionIsMade($campaign, [
            'amount'            => 100,
            'paymentMethodName' => trans('payment::labels.ending_in', [
                'brand'     => 'Visa',
                'ending_id' => '1111',
            ]),
        ]);
        $mails[] = new BadPaymentRepeatFirst($campaign, $campaign->user);
        $mails[] = new BadPaymentRepeatSecond($campaign, $campaign->user);
        $mails[] = new BadPaymentRepeatThird($campaign, $campaign->user);

        $this->sendAdvertiserEmails($mails);
    }

    /**
     * Report module emails
     * @return void
     * @throws \ReflectionException
     */
    private function reportEmails(): void
    {
        if ($this->role === 'admin') {
            return;
        }

        $mails = [];
        $report = $this->reportRepository
            ->findWhere(['type_id' => ReportType::ID_SCHEDULED])
            ->whereNotNull('path')
            ->sortByDesc('id')
            ->first();
        $mails[] = new ScheduledCompleted($report);
        $mails[] = new MissingAdsScheduledCompleted($report);

        $this->sendAdminEmails($mails);
    }

    /**
     * Support module emails
     * @return void
     */
    private function supportEmails(): void
    {
        if ($this->role === 'advertiser') {
            return;
        }

        try {
            $campaign = $this->getReadyCampaign();
            $mail = (
                new Contact(
                    array_merge(
                        [
                            'subject' => 'Test subject',
                            'message' => 'Test message'
                        ],
                        ['user' => $campaign->user]
                    )
                ))
                ->toMail($this->userRepository->getAdmin());
            $mail->resetRecipients();
            $this->mailer->to($this->recipient)->send($mail);
            $this->info('Success Contact notification.');
        } catch (\Throwable $exception) {
            $this->error('Fail Contact notification.');
        }
    }

    /**
     * User module emails
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function userEmails(): void
    {
        if (!$this->role || $this->role === 'admin') {
            $mails = [];

            $user = $this->userRepository->findWhere(['status_id' => UserStatus::ID_ACTIVE])->first();
            $mails[] = new AccountSubmitted($user);
            $mails[] = new AdminActiveFromBadPayment($user);
            $mails[] = new AdminBadPayment($user);

            $this->sendAdminEmails($mails);
        }

        if (!$this->role || $this->role === 'advertiser') {
            $mails = [];

            $user = $this->userRepository->findWhere(['status_id' => UserStatus::ID_ACTIVE])->first();
            $mails[] = new ActiveFromBadPayment($user);
            $mails[] = new UserApproved($user);
            $mails[] = new PendingAccountIsActivated($user);
            $mails[] = new Rejected($user);
            $mails[] = new BadPayment($this->getReadyCampaign(), $user);

            $this->sendAdvertiserEmails($mails);
        }
    }

    /**
     * @param array $mails
     *
     * @return void
     * @throws \ReflectionException
     */
    private function sendAdminEmails(array $mails): void
    {
        foreach ($mails as $mail) {
            $notificationModel = (new \ReflectionClass($mail))->getShortName();
            try {
                $mail = $mail->toMail($this->userRepository->getAdmin());
                $mail->resetRecipients();
                $this->mailer->to($this->recipient)->send($mail);
                $this->info("Success {$notificationModel} notification.");
            } catch (\Throwable $exception) {
                $this->error("Fail {$notificationModel} notification.");
            }
        }
    }

    /**
     * @param array $mails
     *
     * @return void
     * @throws \ReflectionException
     */
    private function sendAdvertiserEmails(array $mails): void
    {
        $advertiser = $this->userRepository->findWhere(['status_id' => UserStatus::ID_ACTIVE])->first();
        foreach ($mails as $mail) {
            $notificationModel = (new \ReflectionClass($mail))->getShortName();
            try {
                $mail = $mail->toMail($advertiser);
                $mail->resetRecipients();
                $this->mailer->to($this->recipient)->send($mail);
                $this->info("Success {$notificationModel} notification.");
            } catch (\Throwable $exception) {
                $this->error("Fail {$notificationModel} notification.");
            }
        }
    }

    /**
     * @return Campaign
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getReadyCampaign(): Campaign
    {
        return $this->campaignRepository->byStatus([CampaignStatus::ID_READY])
            ->with(['user' => function (BelongsTo $query) {
                $query->where('account_external_id', 'not like', "%demo%");
            }])
            ->where('external_id', 'not like', "%demo%")
            ->first();
    }

    /**
     * @return Campaign
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    private function getLiveCampaign(): Campaign
    {
        return $this->campaignRepository->byStatus([CampaignStatus::ID_LIVE])
            ->with(['user' => function (BelongsTo $query) {
                $query->where('account_external_id', 'not like', "%demo%")
                    ->whereNotNull('account_external_id');
            }])
            ->where('external_id', 'not like', "%demo%")
            ->first();
    }
}
