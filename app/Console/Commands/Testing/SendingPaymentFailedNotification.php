<?php

namespace App\Console\Commands\Testing;

use App\Helpers\EnvironmentHelper;
use Modules\Notification\Commands\SendAlerts;

class SendingPaymentFailedNotification extends SendAlerts
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'platform:test:alert:fail_payment {alertId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing send payment failed emails';


    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        if (EnvironmentHelper::isClosedEnv()) {
            return;
        }

        $alert = $this->alert->find($this->argument('alertId'));
        if (!$this->check($alert)) {
            return;
        }

        $this->log->info("Alert #{$alert->id} will be sent.", [
            'alert_id'  => $alert->id,
            'type_name' => $alert->type->name,
            'item_type' => $alert->item_type,
            'item_id'   => $alert->item_id,
        ]);

        $this->send($alert);
    }
}
