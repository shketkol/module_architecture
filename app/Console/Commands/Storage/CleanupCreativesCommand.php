<?php

namespace App\Console\Commands\Storage;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Filesystem;
use Modules\Creative\Repositories\Contracts\CreativeRepository;
use Psr\Log\LoggerInterface;

class CleanupCreativesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:creative:cleanup
                            { --drop : Drop redundant creative items. May be used with `info` option }
                            { --info : Show info dump. May be used with `drop` option }
                            { --new_format : Collect only creative items in new format }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove creative items which are not present in creatives DB table from S3 storage';

    protected $acceptedFormats = ['mp4', 'mov'];
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CreativeRepository
     */
    private $repository;

    /**
     * @var Filesystem
     */
    private $storage;

    /***
     * @param LoggerInterface    $log
     * @param CreativeRepository $repository
     * @param Filesystem         $storage
     */
    public function __construct(
        LoggerInterface $log,
        CreativeRepository $repository,
        Filesystem $storage
    ) {
        parent::__construct();
        $this->logger = $log;
        $this->repository = $repository;
        $this->storage = $storage;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(): void
    {
        if (!$this->option('info') && !$this->option('drop')) {
            $this->error('No action options specified. Please specify `info`, `drop` or both options.');

            return;
        }

        $this->logger->info('Start redundant creatives S3 items collecting... (command)');
        $this->info('Start redundant creatives S3 items collecting...');

        $storageData = $this->storage->allFiles();

        $creativesData = $this->collectCreatives($storageData);

        $dbCreatives = $this->repository
            ->getOriginalKeys()
            ->get()
            ->pluck('original_key')
            ->toArray();

        $redundantItems = array_diff($creativesData, $dbCreatives);
        $redundantCount = count($redundantItems);

        if ($this->option('info')) {
            $this->info('---- Creatives info dump ----');
            $this->info('Total found in storage: ' . count($creativesData));
            $this->info('Total found in DB: ' . count($dbCreatives));
            $this->info('Total redundant: ' . $redundantCount);
            $this->info('To be deleted list: ' . json_encode(
                $redundantItems,
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            ));
        }

        if ($this->option('drop')) {
            $message = "Start redundant creatives S3 items deletion. Items to be deleted: {$redundantCount}";
            $this->logger->info("{$message} (command)");
            $this->info($message);

            $this->storage->delete($redundantItems);
        }
        $this->info('Done.');
    }

    /**
     * Will collect either new format creatives or all creatives
     * if new_format option is not specified.
     *
     * @param array $storageData
     *
     * @return array
     */
    private function collectCreatives(array $storageData): array
    {
        if ($this->option('new_format')) {
            $creativesData = array_filter($storageData, function (string $item) {
                return strpos($item, '/creatives/');
            });
        } else {
            $creativesData = array_filter($storageData, function (string $item) {
                foreach ($this->acceptedFormats as $format) {
                    if (substr(strtolower($item), -strlen($format)) === $format) {
                        return true;
                    }
                }

                return false;
            });
        }

        return $creativesData;
    }
}
