<?php

namespace App\Services\MediaService;

use App\Services\MediaService\Contracts\VideoDriver;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\Media\Frame;

/**
 * Class FfmpegDriver
 * @package App\Services\MediaService
 */
class FfmpegDriver implements VideoDriver
{
    /**
     * @var FFMpeg
     */
    public $driver;

    /**
     * @var \FFMpeg\Media\Video
     */
    public $instance;

    /**
     * @var Frame
     */
    public $frame;

    /**
     * FfmpegDriver constructor.
     */
    public function __construct()
    {
        $this->driver = FFMpeg::create([
            'ffmpeg.binaries'  => config('media.videos.ffmpeg.binaries'),
            'ffprobe.binaries' => config('media.videos.ffprobe.binaries')
        ]);
    }

    /**
     * @param string $source
     * @return \FFMpeg\Media\Audio|\FFMpeg\Media\Video
     */
    public function createInstance(string $source)
    {
        return $this->instance = $this->driver->open($source);
    }

    /**
     * @return mixed
     */
    public function makeFrame()
    {
        return $this->frame = $this->instance->frame(TimeCode::fromSeconds(config('media.videos.frame_second')));
    }

    /**
     * @return string
     */
    public function saveFrame(): string
    {
        return base64_encode($this->frame->save('', false, true));
    }
}
