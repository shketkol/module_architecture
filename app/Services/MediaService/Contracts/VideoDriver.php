<?php

namespace App\Services\MediaService\Contracts;

/**
 * Interface VideoDriver
 * @package App\Services\MediaService\Contracts
 */
interface VideoDriver
{
    /**
     * @param string $source
     * @return mixed
     */
    public function createInstance(string $source);

    /**
     * @return mixed
     */
    public function makeFrame();

    /**
     * @return string
     */
    public function saveFrame(): string;
}
