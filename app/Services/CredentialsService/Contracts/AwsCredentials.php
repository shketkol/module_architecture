<?php

namespace App\Services\CredentialsService\Contracts;

interface AwsCredentials
{
    /**
     * @return string
     */
    public function get();
}
