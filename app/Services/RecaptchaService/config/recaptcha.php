<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Is module enabled
    |--------------------------------------------------------------------------
    |
    | Here you can enable/disable the module.
    |
    */
    'enabled' => env('RECAPTCHA_ENABLED', true),

    /*
    |--------------------------------------------------------------------------
    | Site key
    |--------------------------------------------------------------------------
    |
    | Use this in the HTML code your site serves to users.
    |
    */
    'site_key' => env('RECAPTCHA_SITE_KEY', '6LdyzpwUAAAAAMfS_3XpUEnhB6U6HTq-8500AMTp'),

    /*
    |--------------------------------------------------------------------------
    | Secret key
    |--------------------------------------------------------------------------
    |
    | Use this for communication between your site and Google.
    |
    */
    'secret_key' => env('RECAPTCHA_SECRET_KEY', '6LdyzpwUAAAAADHb0Hi0rLwzxk9f-9hxvOPIP5r2'),

    /*
    |--------------------------------------------------------------------------
    | Verification URL
    |--------------------------------------------------------------------------
    |
    | URL for ReCaptcha verification.
    |
    */
    'verification_url' => env('RECAPTCHA_VERIFY_URL', 'https://www.google.com/recaptcha/api/siteverify'),

    /*
    |--------------------------------------------------------------------------
    | Min score
    |--------------------------------------------------------------------------
    |
    | This value defines minimum score that should be interpreted as a human.
    |
    */
    'min_score' => env('RECAPTCHA_MIN_SCORE', 0.5),
];
