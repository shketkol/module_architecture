<?php

namespace App\Services\RecaptchaService\Http\Middleware;

use App\Services\RecaptchaService\Contracts\RecaptchaService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VerifyRecaptcha
{
    /**
     * This constant defines the key for token form input.
     */
    const TOKEN_FORM_INPUT = 'recaptcha';

    /**
     * @var RecaptchaService
     */
    protected $service;

    /**
     * VerifyRecaptcha constructor.
     * @param RecaptchaService $service
     */
    public function __construct(RecaptchaService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->service->isEnabled() || $this->verify($request)) {
            return $next($request);
        }

        return response()->json([
            'message' => trans('recaptcha::messages.failed')
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Returns true if token is verified.
     *
     * @param Request $request
     * @return bool
     */
    protected function verify(Request $request)
    {
        $token = $request->get(self::TOKEN_FORM_INPUT);

        return $this->service->verify(is_string($token) ? $token : null);
    }
}
