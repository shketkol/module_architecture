<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Mask.
    |--------------------------------------------------------------------------
    |
    */
    'mask'    => '********',

    /*
    |--------------------------------------------------------------------------
    | Mask fields in log output.
    |--------------------------------------------------------------------------
    |
    */
    'masking' => [
        /*
        |--------------------------------------------------------------------------
        | Is masking enabled
        |--------------------------------------------------------------------------
        |
        | Here you can enable/disable the masking.
        | By default it is enabled if we don't use debug mode.
        |
        */
        'enabled' => !env('APP_DEBUG'),

        /*
        |--------------------------------------------------------------------------
        | Fields
        |--------------------------------------------------------------------------
        |
        */
        'fields'  => [
            // Request data
            'name',
            'username',
            'password',
            'password_confirmation',
            'company_name',
            'companyName',
            'company_address',
            'companyAddress',
            'line1',
            'line2',
            'city',
            'state',
            'country',
            'zipcode',
            'first_name',
            'firstName',
            'last_name',
            'lastName',
            'email',
            'phone_number',
            'phoneNumber',
            'advertiserName',

            // Headers
            'authorization',
            'Authorization',

            // Exceptions
            'args',

            // HAAPI
            'userToken',
            'resetPasswordToken',
            'preSharedKey',
            'fileName',
            'sourceUrl',
            'creativeUrl',
            's3Key',
            'accessToken',
            'callbackUrl', // signed url signature disclosure
            'sessionToken',
            'token',
            'refreshToken',

            // OAuth
            'sharedKey',
            'code',

            // WorldPay
            'auth',
            'body',
            'exp_month',
            'exp_year',
            'last4',
            'zip',
            'address',
            'session',
            'paymentToken',
            'lastFour',
            'expMonth',
            'expYear',
            'billingAddress',
            'address1',
            'address2',
            'address3',
            'postalCode',
            'countryCode',
            'cardHolderName',
            'verifications:verification',
            'tokens:token',
            'card',
        ],
    ],
];
