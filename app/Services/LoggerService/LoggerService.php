<?php

namespace App\Services\LoggerService;

use App\Exceptions\BaseException;
use App\Helpers\SessionIdentifierHelper;
use Illuminate\Http\Request;
use Illuminate\Log\LogManager;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Psr\Log\LoggerInterface;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class LoggerService implements LoggerInterface
{
    /**
     * How many items should be logged for stack trace
     */
    private const ITEMS_IN_STACK_TRACE = 5;

    /**
     * @var LogManager|LoggerInterface
     */
    private LogManager $log;

    /**
     * @var string
     */
    private string $requestId;

    /**
     * @var SessionIdentifierHelper
     */
    private SessionIdentifierHelper $identifierHelper;

    /**
     * @param LogManager              $log
     * @param SessionIdentifierHelper $identifierHelper
     */
    public function __construct(LogManager $log, SessionIdentifierHelper $identifierHelper)
    {
        $this->log = $log;
        $this->identifierHelper = $identifierHelper;
        $this->requestId = Str::uuid()->toString();
    }

    /**
     * @return LoggerInterface
     */
    public function driver(): LoggerInterface
    {
        return $this->log->driver();
    }

    /**
     * @param \Closure $callback
     *
     * @see \Illuminate\Log\Logger::listen()
     */
    public function listen(\Closure $callback): void
    {
        $this->log->listen($callback);
    }

    /**
     * Flush the existing context array.
     *
     * @return $this
     */
    public function withoutContext(): self
    {
        if (method_exists($this->log, 'withoutContext')) {
            /** @see \Illuminate\Log\Logger::withoutContext() */
            $this->log->withoutContext();
        }

        return $this;
    }

    /**
     * Add requestId to context.
     *
     * @param array $context
     *
     * @return array
     */
    private function prepareContext(array $context = []): array
    {
        Arr::set($context, 'requestId', $this->requestId);

        if (!is_null(env('AWS_CLOUDWATCH_INSTANCE_ID'))) {
            Arr::set($context, 'instanceId', env('AWS_CLOUDWATCH_INSTANCE_ID'));
        }

        if (env('APP_DEBUG')) {
            Arr::set($context, 'whoami', trim(`whoami`));
        }

        $identifier = $this->identifierHelper->getIdentifier();
        Arr::set($context, 'identifier', $identifier);

        return $context;
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function emergency($message, array $context = []): void
    {
        $this->log->emergency($message, $this->prepareContext($context));
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function alert($message, array $context = []): void
    {
        $this->log->alert($message, $this->prepareContext($context));
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function critical($message, array $context = []): void
    {
        $this->log->critical($message, $this->prepareContext($context));
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function error($message, array $context = []): void
    {
        $this->log->error($message, $this->prepareContext($context));
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function warning($message, array $context = []): void
    {
        $this->log->warning($message, $this->prepareContext($context));
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function notice($message, array $context = []): void
    {
        $this->log->notice($message, $this->prepareContext($context));
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function info($message, array $context = []): void
    {
        $this->log->info($message, $this->prepareContext($context));
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function debug($message, array $context = []): void
    {
        $this->log->debug($message, $this->prepareContext($context));
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function log($level, $message, array $context = []): void
    {
        $this->log->log($level, $message, $this->prepareContext($context));
    }

    /**
     * Log exception
     *
     * @param \Throwable $exception
     * @param bool       $logBaseException
     */
    public function logException(\Throwable $exception, bool $logBaseException = false): void
    {
        if ($exception instanceof BaseException && !$logBaseException) {
            return;
        }

        $request = Request::createFromGlobals();
        $trace = $this->getTrace($exception);
        $message = get_class($exception) . ' exception occurred:';
        $data = [
            'message' => $exception->getMessage(),
            'trace'   => $trace,
            'request' => $request->all(),
        ];


        if (!($exception instanceof BaseException)) {
            $this->error($message, $data);
            return;
        }

        $this->logBaseException($exception, $message, $data);
    }

    /**
     * @param BaseException $exception
     * @param string        $message
     * @param array         $data
     *
     * @return void
     */
    protected function logBaseException(BaseException $exception, string $message, array $data): void
    {
        switch ($exception->getLogLevel()) {
            case BaseException::LOG_LEVEL_WARNING:
                $this->warning($message, $data);
                break;
            case BaseException::LOG_LEVEL_ERROR:
            default:
                $this->error($message, $data);
        }
    }

    /**
     * @param \Throwable $exception
     *
     * @return array
     */
    private function getTrace(\Throwable $exception): array
    {
        return array_map(function ($item) {
            return Arr::except($item, ['type', 'args']);
        }, array_slice($exception->getTrace(), 0, self::ITEMS_IN_STACK_TRACE));
    }
}
