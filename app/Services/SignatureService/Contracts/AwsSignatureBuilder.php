<?php

namespace App\Services\SignatureService\Contracts;

interface AwsSignatureBuilder
{
    /**
     * @return string
     */
    public function getSignature();
}
