<?php

namespace App\Services\SignatureService;

class HashHmacSignature
{
    public $lastValue;

    public function makeHash($type, $first, $second, $rawOutput = true)
    {
        return $this->lastValue = hash_hmac($type, $first, $second, $rawOutput);
    }
}
