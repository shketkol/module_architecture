<?php

namespace App\Services\SignatureService\Exceptions;

use App\Exceptions\BaseException;

class InvalidToSignException extends BaseException
{
    /**
     * @param string $message
     *
     * @return self
     */
    public static function create(string $message): self
    {
        return new self($message);
    }
}
