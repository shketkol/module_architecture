<?php

namespace App\Services\ValidationRulesService;

use App\Helpers\FileSizeHelper;
use App\Rules\AllowedAttributes;
use App\Rules\CheckAgeGroups;
use App\Rules\CsvFormat;
use App\Rules\HaapiDate;
use App\Rules\HasAffectedAdvertisers;
use App\Rules\HasState;
use App\Rules\Included;
use App\Rules\IndexedArray;
use App\Rules\IsAdvertisers;
use App\Rules\MaxCountCsv;
use App\Rules\MaxFilesize;
use App\Rules\MinPerDayImpressions;
use App\Rules\MinVideoAttribute;
use App\Rules\OldPassword;
use App\Rules\Payment;
use App\Rules\UniqueCreative;
use App\Rules\ValidateCampaignStatus;
use App\Rules\ValidateUserStatus;
use App\Rules\ValidBroadcastDate;
use App\Rules\ValidEmail;
use App\Rules\ValidInviteCode;
use App\Services\ValidationRulesService\Contracts\ValidationRules as ValidationRulesInterface;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use Modules\Broadcast\Models\BroadcastChannel;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\CampaignStep;
use Modules\Daapi\Validation\DaapiValidationRules;
use Modules\Haapi\Mappers\Campaign\ActionMapper;
use Modules\Report\Models\ReportDays;
use Modules\Report\Models\ReportDeliveryFrequency;
use Modules\Report\Models\ReportDeliveryType;
use Modules\User\Models\UserStatus;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class ValidationRules implements ValidationRulesInterface
{
    use DaapiValidationRules;

    /**
     * From character "space" to character "~"
     * @var string
     */
    private const REGEXP_ASCII_CHARACTERS = '/^[\x20-\x7E]*$/';

    private const REGEXP_NUMBERS = '/^[0-9]+$/';

    private const MIN_DURATION = 14;
    private const MAX_DURATION = 31;

    public const MIN_DURATION_LABEL = 15;
    public const MAX_DURATION_LABEL = 30;

    /**
     * @var array
     */
    protected $customRules = [
        'allowed_attributes'       => AllowedAttributes::class,
        'haapi_date'               => HaapiDate::class,
        'included'                 => Included::class,
        'max_filesize'             => MaxFilesize::class,
        'format_csv'               => CsvFormat::class,
        'max_count_csv'            => MaxCountCsv::class,
        'min_video'                => MinVideoAttribute::class,
        'old_password'             => OldPassword::class,
        'unique_creative'          => UniqueCreative::class,
        'payment_method'           => Payment::class,
        'valid_email'              => ValidEmail::class,
        'valid_invite_code'        => ValidInviteCode::class,
        'checkAgeGroups'           => CheckAgeGroups::class,
        'has_state'                => HasState::class,
        'indexed_array'            => IndexedArray::class,
        'is_advertisers'           => IsAdvertisers::class,
        'valid_broadcast_date'     => ValidBroadcastDate::class,
        'has_affected_advertisers' => HasAffectedAdvertisers::class,
        'validate_campaign_status' => ValidateCampaignStatus::class,
        'validate_user_status'     => ValidateUserStatus::class,
        'min_per_day'              => MinPerDayImpressions::class,
    ];

    /**
     * @var array
     */
    protected $rules;

    /**
     * Validation rules.
     *
     * @var array
     */
    protected $defaultRules = [
        'campaign'    => [
            'search' => [
                'statuses'   => [
                    'array'    => true,
                    'nullable' => true,
                    'max'      => 10,
                    'min'      => 1,
                ],
                'statuses.*' => [
                    'integer' => true,
                    'max'     => 10,
                    'min'     => 1,
                ],
                'limit'      => [
                    'integer'  => true,
                    'max'      => 100,
                    'min'      => 1,
                    'nullable' => true
                ],
            ],
            'details'   => [
                'name'        => [
                    'required' => true,
                    'string'   => true,
                    'min'      => 2,
                    'max'      => 120,
                    'regex'    => self::REGEXP_ASCII_CHARACTERS,
                    'nullable' => true,
                ],
                'date'        => [
                    'start' => [
                        'required' => true,
                        'date'     => true,
                    ],
                    'end'   => [
                        'required' => true,
                        'date'     => true,
                    ],
                ],
                'timezone_id' => [
                    'required' => true,
                    'exists'   => 'timezones,id',
                ],
            ],
            'budget'    => [
                'cost'        => [
                    'required' => true,
                    'numeric'  => true,
                    'min'      => 500,
                    'max'      => 100000,
                ],
                'cpm'         => [
                    'required' => true,
                    'numeric'  => true,
                    'min'      => 1,
                    'max'      => 100000,
                ],
                'impressions' => [
                    'required' => true,
                    'numeric'  => true,
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                ],
            ],
            'creative'  => [
                'sometimes' => true,
                'required'  => true,
                'integer'   => true,
                'min'       => 1,
                'max'       => PHP_INT_MAX,
                'exists'    => 'creatives,id',
            ],
            'payment'   => [
                'required', 'payment_method',
            ],
            'promocode' => [
                'required' => true,
                'string'   => true,
                'min'      => 1,
                'max'      => 40,
            ],
            'id'        => [
                'nullable'                 => true,
                'required'                 => true,
                'integer'                  => true,
                'max'                      => PHP_INT_MAX,
                'exists'                   => 'campaigns,id',
                'validate_campaign_status' => [CampaignStatus::ID_PROCESSING],
            ],
            'wizard'    => [
                'action_name' => [
                    'nullable' => true,
                    'string'   => true,
                    'min'      => 1,
                    'max'      => 64,
                ],
                'last_page'   => [
                    'required' => true,
                    'string'   => true,
                    'min'      => 1,
                    'max'      => 64,
                ],
            ],
        ],
        'daapi_api'   => [
            'payload'    => [
                'required' => true,
                'array'    => true,
            ],
            'campaign'   => [
                'required'                 => true,
                'integer'                  => true,
                'max'                      => PHP_INT_MAX,
                'exists'                   => 'campaigns,id',
                'validate_campaign_status' => [CampaignStatus::ID_SUSPENDED],
            ],
            'advertiser' => [
                'required'             => true,
                'string'               => true,
                'exists'               => 'users,account_external_id',
                'validate_user_status' => [UserStatus::ID_INACTIVE_UNPAID],
            ],
            'event_time' => [
                'required'    => true,
                'date_format' => true,
            ]
        ],
        'targeting'   => [
            'age_group'    => [
                'required'       => true,
                'array'          => true,
                'min'            => 1,
                'checkAgeGroups' => true,
            ],
            'age_group_id' => [
                'integer' => true,
                'max'     => PHP_INT_MAX,
            ],
            'search'       => [
                'query_mysql'        => [
                    'required' => false,
                    'string'   => true,
                    'max'      => 255,
                ],
                'filter_query_mysql' => [
                    // strip anything except those values (any character from unicode and space) from query
                    // so we advertiser could search even with invalid characters
                    'regex' => '/[^\p{L}\ ]+/u',
                ],
                'values'             => [
                    'required' => true,
                    'array'    => true,
                    'min'      => 1,
                ],
                'zipcode'            => [
                    'required' => true,
                    'string'   => true,
                    'min'      => 1,
                    'max'      => 10,
                ],
            ],
            'gender'       => [
                'id' => [
                    'required' => true,
                    'integer'  => true,
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                ],
            ],
            'genre'        => [
                'ids' => [
                    'required' => true,
                    'array'    => true,
                ],
                'id'  => [
                    'required' => true,
                    'integer'  => true,
                    'exists'   => 'genres,id',
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                ],
            ],
            'device_group' => [
                'values' => [
                    'array' => true,
                ],
                'groups' => [
                    'required' => true,
                    'array'    => true,
                    'min'      => 1,
                ],
                'id'     => [
                    'required' => true,
                    'integer'  => true,
                    'exists'   => 'device_groups,id',
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                ],
            ],
            'audience'     => [
                'ids' => [
                    'required' => true,
                    'array'    => true,
                ],
                'id'  => [
                    'required' => true,
                    'integer'  => true,
                    'exists'   => 'audiences,id',
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                ],
            ],
            'location'     => [
                'id'      => [
                    'required' => true,
                    'integer'  => true,
                    'exists'   => 'locations,id',
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                ],
                'ids' => [
                    'required' => true,
                    'array'    => true,
                ],
                'zip_csv' => [
                    'required'      => true,
                    'max_filesize'  => [FileSizeHelper::KILOBYTE * 5, 'kb', 'zip_codes'],
                    'format_csv'    => '/([0-9]{5}|,|\n+$)/',
                    'max_count_csv' => 400
                ],
            ],
            'zipcode'      => [
                'ids' => [
                    'required' => true,
                    'array'    => true,
                ],
                'id'  => [
                    'required' => true,
                    'integer'  => true,
                    'exists'   => 'zipcodes,id',
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                ],
            ],
            'excluded'     => [
                'required' => true,
                'integer'  => true,
                'min'      => 0,
                'max'      => 1,
            ],
            'values'       => [
                'sometimes' => true,
                'required'  => true,
                'array'     => true,
            ],
        ],
        'support'     => [
            'contact' => [
                'subject' => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 150,
                ],
                'message' => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 65535,
                ],
            ],
        ],
        'user'        => [
            'invite_code'         => [
                'required'          => true,
                'string'            => true,
                'max'               => 20,
                'valid_invite_code' => true,
            ],
            'email'               => [
                'required'    => true,
                'string'      => true,
                'email'       => true,
                'max'         => 255,
                'valid_email' => true,
            ],
            'password'            => [
                'required'     => true,
                'string'       => true,
                'min'          => 8,
                'max'          => 64,
                'confirmed'    => true,
                'old_password' => true,
                'regex'        => '/^'                                             // start
                    . '(?=.*[a-z])'                                                // lowercase character a-z
                    . '(?=.*[A-Z])'                                                // uppercase character A-Z
                    . '(?=.*[~\^\*!@#$%_&\+\-\(\)\/\{\}\\\ "\'\.\?\[\]\|,:;<>=`])' // special symbols
                    . '(?=.*[0-9])'                                                // number
                    . '.*$/',                                                      // anything at the end
            ],
            'confirmation_code'   => [
                'required' => true,
                'string'   => true,
                'max'      => 64,
            ],
            'first_name'          => [
                'required' => true,
                'string'   => true,
                'max'      => 40,
            ],
            'last_name'           => [
                'required' => true,
                'string'   => true,
                'max'      => 40,
            ],
            'token'               => [
                'required' => true,
                'string'   => true,
                'max'      => 10000,
            ],
            'account_external_id' => [
                'required' => true,
                'string'   => true,
                'max'      => 40,
            ],
            'phone'               => [
                'min'      => 17,
                'max'      => 17,
                'required' => true,
                'string'   => true,
                'regex'    => '/^\+?' // can have "+" at beginning
                    . '('             // group start
                    . '[\(\)\-\ ]*'   // can have symbols: "(", ")", "-", " "
                    . '\d'            // must have numbers
                    . ')'             // group end
                    . '{11}$/',       // exact 11 times
            ],
            'company_name'        => [
                'required' => true,
                'string'   => true,
                'max'      => 255,
            ],
            'company_address'     => [
                'required' => true,
                'array'    => true,
            ],
            'address'             => [
                'line1'   => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 255,
                ],
                'line2'   => [
                    'required' => false,
                    'string'   => true,
                    'max'      => 255,
                ],
                'city'    => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 40,
                ],
                'state'   => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 40,
                    'exists'   => 'us_states,name',
                ],
                'country' => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 80,
                ],
                'zip'     => [
                    'required'  => true,
                    'exists'    => 'us_zipcodes,name',
                    'string'    => true,
                    'max'       => 5,
                    'regex'     => '/^[0-9]{5}$/',
                    'has_state' => true,
                ],
            ],
            'account_type_id'     => [
                'required' => true,
                'integer'  => true,
                'min'      => 1,
                'max'      => PHP_INT_MAX,
                'exists'   => 'account_types,id',
            ],
        ],
        'creative'    => [
            'general' => [
                'video_cnt'            => [
                    'required' => true,
                    'max'      => 1,
                    'min'      => 1,
                ],
                'audio_cnt'            => [
                    'required' => true,
                    'max'      => 2,
                    'min'      => 1,
                ],
                'format'               => [
                    'required' => true,
                    'included' => [
                        'MPEG-4',
                        'MOV',
                        'QuickTime'
                    ],
                ],
                'extension'            => [
                    'required' => true,
                    'included' => [
                        'mp4',
                        'mov',
                    ],
                ],
                'file_size'            => [
                    'required'     => true,
                    'max_filesize' => [FileSizeHelper::GIGABYTE, 'gb', 'creative'],
                ],
                'duration'             => [
                    'required' => true,
                    'numeric'  => true,
                    'between'  => [self::MIN_DURATION, self::MAX_DURATION],
                ],
                'codec'                => [
                    'required' => true,
                    'included' => ['avc1', 'apch', 'ap4h', 'apcs', 'apcn', 'ncpa'],
                ],
                'video_bit_rate'       => [
                    'required'  => true,
                    'min_video' => [
                        15 * FileSizeHelper::MEGABYTE,
                        'dimension' => 'b/s',
                        'format'    => MinVideoAttribute::FORMAT_MEGA,
                        'system'    => MinVideoAttribute::SYSTEM_BINARY,
                    ],
                ],
                'width'                => [
                    'required'  => true,
                    'numeric'   => true,
                    'min_video' => [
                        1280,
                        'dimension' => 'px',
                        'format'    => null,
                    ],
                ],
                'height'               => [
                    'required'  => true,
                    'numeric'   => true,
                    'min_video' => [
                        720,
                        'dimension' => 'px',
                        'format'    => null,
                    ],
                ],
                'display_aspect_ratio' => [
                    'required' => true,
                    'included' => ['16:9', '1.778'],
                ],
                'frame_rate_mode'      => [
                    'required' => true,
                    'included' => ['CFR'],
                ],
                'frame_rate'           => [
                    'required' => true,
                    'included' => [
                        [23.98, 25, 29.97, 23.976],
                        'fps',
                    ],
                ],
                'color_space'          => [
                    'required' => true,
                    'included' => ['YUV', 'YUVA'],
                ],
                'chroma_subsampling'   => [
                    'required' => true,
                    'included' => ['4:2:0', '4:2:2'],
                ],
                'bit_depth'            => [
                    'required' => true,
                    'included' => [
                        [8, 16],
                        'bits',
                    ],
                ],
                'scan_type'            => [
                    'required' => true,
                    'included' => ['Progressive'],
                ],
            ],
            'audio'   => [
                'format'        => [
                    'required' => true,
                    'included' => ['PCM', 'AAC'],
                ],
                'duration'      => [
                    'required' => true,
                    'numeric'  => true,
                    'between'  => [self::MIN_DURATION, self::MAX_DURATION],
                ],
                'bit_rate'      => [
                    'required'  => true,
                    'numeric'   => true,
                    'min_video' => [
                        192 * FileSizeHelper::KILOBYTE,
                        'dimension' => 'b/s',
                        'format'    => MinVideoAttribute::FORMAT_KILO,
                        'system'    => MinVideoAttribute::SYSTEM_BINARY,
                    ],
                ],
                'channels'      => [
                    'required' => true,
                    'numeric'  => true,
                    'included' => [2],
                ],
                'sampling_rate' => [
                    'required' => true,
                    'included' => [
                        [48000],
                        'Hz',
                    ],
                ],
                'bit_depth'     => [
                    'nullable' => true,
                    'included' => [
                        [16, 24],
                        'bits',
                    ],
                ],
            ],
            'basic'   => [
                'name'      => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 255,
                ],
                //used only for FE-REJECTED creative store
                'extension' => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 10,
                ],
                //used only for FE-REJECTED creative store
                'errors'    => [
                    'required' => true,
                    'array'    => true,
                    'min'      => 1,
                    'max'      => 100,
                ],
                //used only for FE-REJECTED creative store
                'rawParams' => [
                    'required' => true,
                    'array'    => true,
                    'min'      => 1,
                    'max'      => 5,
                ],

                's3Key'             => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 255,
                ],
                'canonical_request' => [
                    'required' => true,
                    'string'   => true,
                    'max'      => 4000,
                ],
                'hashSum'           => [
                    'required'        => true,
                    'string'          => true,
                    'unique_creative' => true,
                    'max'             => 64,
                ],
            ],
        ],
        'payment'     => [
            'name'    => [
                'required' => true,
                'string'   => true,
                'max'      => 255,
            ],
            'brand'   => [
                'required' => true,
                'string'   => true,
                'max'      => 255,
            ],
            'session' => [
                'required' => true,
                'url'      => true,
                'max'      => 4096,
            ],
        ],
        'report'      => [
            'account'                        => [
                'nullable' => true,
                'integer'  => true,
                'max'      => PHP_INT_MAX,
            ],
            'name'                           => [
                'required' => true,
                'string'   => true,
                'max'      => 100,
                'regex'    => self::REGEXP_ASCII_CHARACTERS,
            ],
            'execution_day'                  => [
                'nullable' => true,
                'integer'  => true,
                'max'      => PHP_INT_MAX,
                'included' => ReportDays::REPORT_DAYS_ID,
            ],
            'delivery_frequency'             => [
                'required' => true,
                'integer'  => true,
                'max'      => PHP_INT_MAX,
                'exists'   => 'report_delivery_frequencies,id',
                'included' => [
                    ReportDeliveryFrequency::ID_DAILY,
                    ReportDeliveryFrequency::ID_WEEKLY,
                    ReportDeliveryFrequency::ID_MONTHLY,
                ],
                'required_if' => 'delivery.type,'.ReportDeliveryType::SCHEDULED
            ],
            'emails'                         => [
                'required' => true,
                'array'    => true,
                'max'      => 100,
                'min'      => 1,
            ],
            'email'                          => [
                'required' => true,
                'email'    => true,
                'max'      => 255,
                'min'      => 1,
            ],
            'date'                           => [
                'required' => true,
                'date'     => true,
            ],
            'id'                             => [
                'required' => true,
                'integer'  => true,
                'max'      => PHP_INT_MAX,
                'exists'   => 'reports,id',
            ],
            'campaigns'                      => [
                'required' => true,
                'max'      => 100,
                'nullable' => true,
                'array'    => true,
                'present'  => true
            ],
            'campaign'                       => [
                'required' => true,
                'max'      => PHP_INT_MAX,
                'exists'   => 'campaigns,id',
            ],
            'targetings'                     => [
                'required' => true,
                'max'      => 5,
                'nullable' => true,
                'present'  => true,
                'array'    => true,
            ],
            'targeting'                      => [
                'required' => true,
                'integer'  => true,
                'max'      => 20,
                'exists'   => 'report_targeting_types,id',
            ],
            'delivery'                       => [
                'type' => [
                    'required' => true,
                    'max'      => 100,
                    'included' => [
                        ReportDeliveryType::SCHEDULED,
                        ReportDeliveryType::DOWNLOAD,
                        ReportDeliveryType::EMAIL_NOW
                    ],
                ],
            ]
        ],
        'search'      => [
            'query_mysql'        => [
                'nullable' => true,
                'string'   => true,
                'max'      => 255,
            ],
            'filter_query_mysql' => [
                // strip anything except those values (any character from unicode and space) from query
                // so we advertiser could search even with invalid characters
                'regex' => '/[^\p{L}\ ]+/u',
            ],
        ],
        'uuid'        => [
            'required' => true,
            'string'   => true,
            'regex'    => '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i',
        ],
        'invitation'  => [
            'email'                 => [
                'required'                 => true,
                'string'                   => true,
                'max'                      => 100,
                'email'                    => true,
                'unique:invitations,email' => true,
            ],
            'code'                  => [
                'nullable'                => true,
                'required'                => true,
                'string'                  => true,
                'max'                     => 20,
                'unique:invitations,code' => true,
                'exists:invitations,code' => true,
            ],
            'signup_invite_code'    => [
                'required'          => true,
                'string'            => true,
                'max'               => 20,
                'valid_invite_code' => true,
            ],
            'emails_array'          => [
                'required' => true,
                'array'    => true,
            ],
            'emails'                => [
                'required' => true,
                'string'   => true,
            ],
            'csv'                   => [
                'required'     => true,
                'max_filesize' => [FileSizeHelper::KILOBYTE * 5, 'kb', 'zip_codes'],
            ],
            'payload'               => [
                'required'      => true,
                'array'         => true,
                'indexed_array' => true,
                'min'           => 1,
                'max'           => 9999,
            ],
            'expiration_date'       => [
                'nullable'   => true,
                'haapi_date' => true,
            ],
            'identification_key'    => [
                'nullable' => true,
                'string'   => true,
                'max'      => 255,
            ],
            'account_type_category' => [
                'required' => true,
                'boolean'  => true,
            ],
        ],
        'maintenance' => [
            'maintenance_enabled' => [
                'required' => true,
                'boolean'  => true,
            ],
            'message'             => [
                'nullable' => true,
                'string'   => true,
                'max'      => 255,
            ],
        ],
        'request'     => [
            'payload' => [
                'required' => true,
                'array'    => true,
            ],
        ],
        'location'    => [
            'zipcode' => [
                'query' => [
                    'required' => true,
                    'string'   => true,
                    'regex'    => self::REGEXP_NUMBERS,
                    'min'      => 1,
                    'max'      => 5,
                ],
            ],
            'state'   => [
                'string' => true,
                'min'    => 2,
                'max'    => 2,
                'exists' => 'us_states,name',
            ],
        ],
        'broadcast'   => [
            'test_email'           => [
                'required' => true,
                'string'   => true,
                'email'    => true
            ],
            'name'                 => [
                'required' => true,
                'string'   => true,
                'max'      => 100,
                'regex'    => self::REGEXP_ASCII_CHARACTERS,
            ],
            'schedule_date'        => [
                'required'             => true,
                'date'                 => true,
                'valid_broadcast_date' => true,
                'nullable'             => true,
            ],
            'channel'              => [
                'required' => true,
                'string'   => true,
                'in'       => BroadcastChannel::CHANNELS,
            ],
            'segment_group'        => [
                'payload'     => [
                    'required' => false,
                    'array'    => true,
                ],
                'id'          => [
                    'required' => true,
                    'integer'  => true,
                    'min'      => 1,
                    'max'      => PHP_INT_MAX,
                    'exists'   => 'broadcast_segment_groups,id',
                ],
                'advertisers' => [
                    'required_if'    => 'segment_group.id,' . BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS,
                    'array'          => true,
                    'is_advertisers' => true,
                ],
            ],
            'affected_advertisers' => [
                'dates'      => [
                    'required_if'              => 'segment_group.id,' . BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS,
                    'array'                    => true,
                    'has_affected_advertisers' => true,
                ],
                'date_start' => [
                    'required_if' => 'segment_group.id,' . BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS,
                    'date'        => true,
                    'before'      => 'tomorrow',
                    'nullable'    => true,
                ],
                'date_end'   => [
                    'required_if'    => 'segment_group.id,' . BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS,
                    'date'           => true,
                    'before'         => 'tomorrow',
                    'after_or_equal' => 'date_start',
                    'nullable'       => true,
                ],
                'count'      => [
                    'required' => true,
                    'integer'  => true,
                    'min'      => 1,
                ],
            ],
            'email'                => [
                'payload' => [
                    'required_if'          => 'channel,' . BroadcastChannel::ALL . ',' . BroadcastChannel::EMAIL,
                    'required_without_all' => 'notification',
                    'array'                => true,
                ],
                'subject' => [
                    'required_if'   => 'channel,' . BroadcastChannel::ALL . ',' . BroadcastChannel::EMAIL,
                    'required_with' => 'email',
                    'string'        => true,
                    'max'           => 100,
                    'regex'         => self::REGEXP_ASCII_CHARACTERS,
                    'nullable'      => true
                ],
                'header'  => [
                    'required_if'   => 'channel,' . BroadcastChannel::ALL . ',' . BroadcastChannel::EMAIL,
                    'required_with' => 'email',
                    'string'        => true,
                    'max'           => 100,
                    'regex'         => self::REGEXP_ASCII_CHARACTERS,
                    'nullable'      => true
                ],
                'icon'    => [
                    'string'   => true,
                    'nullable' => true,
                    'exists'   => 'email_icons,name',
                ],
                'body'    => [
                    'required_if'   => 'channel,' . BroadcastChannel::ALL . ',' . BroadcastChannel::EMAIL,
                    'required_with' => 'email',
                    'string'        => true,
                    'max'           => 9999,
                    'nullable'      => true
                ],
            ],
            'notification'         => [
                'payload'  => [
                    'required_if'          =>
                        'channel,' . BroadcastChannel::ALL . ',' . BroadcastChannel::IN_APP_NOTIFICATION,
                    'array'                => true,
                    'required_without_all' => 'email',
                ],
                'category' => [
                    'id' => [
                        'required_if'   =>
                            'channel,' . BroadcastChannel::ALL . ',' . BroadcastChannel::IN_APP_NOTIFICATION,
                        'required_with' => 'notification',
                        'integer'       => true,
                        'min'           => 1,
                        'max'           => PHP_INT_MAX,
                        'exists'        => 'notification_categories,id',
                        'nullable'      => true
                    ],
                ],
                'body'     => [
                    'required_if'   =>
                        'channel,' . BroadcastChannel::ALL . ',' . BroadcastChannel::IN_APP_NOTIFICATION,
                    'required_with' => 'notification',
                    'string'        => true,
                    'max'           => 9999,
                    'nullable'      => true
                ],
            ],
        ],
        'setting'    => [
            'creative_monthly_upload_max_submits' => [
                'integer'    => true,
                'nullable'   => true,
                'numeric'    => true,
                'min'        => 0,
                'max'        => 999999,
            ],
        ],
    ];

    /**
     * ValidationRules constructor.
     */
    public function __construct()
    {
        $daapiRules = $this->getDaapiRules();
        $this->rules = array_merge($this->defaultRules, $daapiRules);
        $this->setConfiguredValidationRules();
        $this->setBroadcastRules();
    }

    /**
     * Set broadcast validation rules.
     */
    protected function setBroadcastRules(): void
    {
        $this->set(
            'broadcast.segment_group.advertisers.max',
            config(sprintf('broadcast.segment_groups.%d.max', BroadcastSegmentGroup::ID_SELECTED_ADVERTISERS))
        );

        $this->set(
            'broadcast.affected_advertisers.date_start.after_or_equal',
            sprintf('today midnight - %d month', config('activities.database.store_months'))
        );
    }

    /**
     * Set rules defined in config files.
     */
    protected function setConfiguredValidationRules()
    {
        $this->set(
            'creative.general.file_size.max_filesize',
            [config('rules.creative.max_file_size', FileSizeHelper::GIGABYTE), 'gb', 'creative']
        );
        $this->set(
            'creative.general.duration.between',
            [
                config('rules.creative.min_duration', self::MIN_DURATION),
                config('rules.creative.max_duration', self::MAX_DURATION)
            ]
        );
        $this->set(
            'creative.audio.duration.between',
            [
                config('rules.creative.min_duration', self::MIN_DURATION),
                config('rules.creative.max_duration', self::MAX_DURATION)
            ]
        );
        $this->set('targeting.location.zip_csv.max_count_csv', config('locations.max_count_zip_codes'));
        $this->set('report.missing_ads_delivery_frequency.included', config('report.missing_ads_frequency_ids'));

        $this->set('daapi_api.event_time.date_format', config('date.format.db_date_time'));

        $this->setConfiguredCampaignDatesRules();
        $this->setConfiguredCampaignWizardRules();
    }

    /**
     * Set Campaign Wizard rules
     */
    private function setConfiguredCampaignWizardRules(): void
    {
        $this->set('campaign.wizard.action_name.in', ActionMapper::getWizardActions());
        $this->set('campaign.wizard.last_page.in', array_merge(
            CampaignStep::getStepNames(),
            [ActionMapper::CAMPAIGN_DETAILS]
        ));
    }

    /**
     * Set campaign start and end dates rules.
     */
    protected function setConfiguredCampaignDatesRules(): void
    {
        $this->set(
            'campaign.details.date.start.after',
            $this->formatRelativeDate(sprintf(
                '+%d %s',
                config('campaign.wizard.date.start.min.value'),
                config('campaign.wizard.date.start.min.type')
            ))
        );

        $this->set(
            'campaign.details.date.start.before',
            $this->formatRelativeDate(sprintf(
                '+%d %s',
                config('campaign.wizard.date.start.max.value'),
                config('campaign.wizard.date.start.max.type')
            ))
        );

        $this->set('campaign.details.date.start.date_format', config('date.format.php'));
        $this->set('campaign.details.date.end.date_format', config('date.format.php'));
    }

    /**
     * Format relative date (e.g. '+1d') to absolute ET date
     *
     * @param $dateRelative
     *
     * @return Carbon
     */
    private function formatRelativeDate($dateRelative): Carbon
    {
        return Date::parse($dateRelative, config('campaign.wizard.date.timezone'));
    }

    /**
     * Get all of the rules.
     *
     * @return array
     */
    public function all(): array
    {
        return $this->rules;
    }

    /**
     * Retrieve a rule for the field.
     *
     * @param string $key
     * @param string|array|null $default
     *
     * @return mixed
     */
    public function get(string $key = null, $default = null)
    {
        return data_get($this->all(), $key, $default);
    }

    /**
     * Set a rule.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return mixed
     */
    public function set(string $key, $value)
    {
        return data_set($this->rules, $key, $value);
    }

    /**
     * Get a subset of the rules for the field.
     *
     * @param string $field
     * @param array|mixed $keys
     * @param array $extraRules
     *
     * @return array
     */
    public function only(string $field, $keys, array $extraRules = []): array
    {
        $keys = is_array($keys) ? $keys : [$keys];
        $rules = [];
        $input = $this->all();

        foreach ($keys as $key) {
            data_set($rules, $key, data_get($input, "{$field}.{$key}"));
        }

        return array_merge($this->format($rules), $extraRules);
    }

    /**
     * Format rules as a string.
     *
     * @param mixed $rules
     *
     * @return array
     */
    public function format($rules): array
    {
        $rules = is_array($rules) ? $rules : func_get_args();
        $response = [];

        foreach ($rules as $name => $value) {
            if (array_key_exists($name, $this->customRules)) {
                $response[] = $this->formCustomRule($name, $value);
            } else {
                $response[] = $this->formDefaultRule($name, $value);
            }
        }

        return $response;
    }

    /**
     * Form default rule string
     *
     * @param string $name
     * @param mixed $value
     *
     * @return string
     */
    protected function formDefaultRule(string $name, $value): string
    {
        if (is_bool($value)) {
            return $response = $value ? $name : '';
        }

        if (is_array($value)) {
            $value = implode(',', $value);
        }

        $response = "{$name}:{$value}";

        return $response;
    }

    /**
     * Form custom rule object defined in $this->customRules
     *
     * @param string $name
     * @param boolean|string $value
     *
     * @return Rule
     */
    protected function formCustomRule(string $name, $value): Rule
    {
        if (is_bool($value)) {
            $rule = $value ? new $this->customRules[$name] : '';
        } else {
            $rule = new $this->customRules[$name]($value);
        }

        return $rule;
    }
}
