<?php

namespace App\Services\ValidationRulesService;

use Illuminate\Support\Facades\Facade;

class ValidationRulesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'validation.rules';
    }
}
