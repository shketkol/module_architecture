<?php

namespace App\Services\ValidationRulesService;

use App\Services\ValidationRulesService\Contracts\ValidationRules as ValidationRulesInterface;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        'validation.rules'              => ValidationRules::class,
        ValidationRulesInterface::class => ValidationRules::class,
    ];
}
