<?php

namespace App\Services\HealthCheckService\Listeners;

use App\Services\HealthCheckService\Events\HealthCheckPerformed;
use App\Services\HealthCheckService\Metric\Contracts\CloudwatchMetricClient;

/**
 * Class HealthCheckListener
 * @package App\Services\HealthCheckService\Listeners
 */
class HealthCheckListener
{
    /**
     * Instance of AwsMetricClient
     * @object AwsMetricClient
     */
    protected $cloudwatch;

    public function __construct(CloudwatchMetricClient $cloudwatch)
    {
        $this->cloudwatch = $cloudwatch;
    }

    /**
     * Handle the event.
     */
    public function handle(): void
    {
        $this->cloudwatch->putHealthy();
    }
}
