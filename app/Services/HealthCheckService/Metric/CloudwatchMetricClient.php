<?php

namespace App\Services\HealthCheckService\Metric;

use Aws\CloudWatch\CloudWatchClient;
use Aws\Api\Client;
use Aws\Exception\AwsException;
use Psr\Log\LoggerInterface;
use App\Services\HealthCheckService\Metric\Contracts\CloudwatchMetricClient as CloudwatchMetricClientInterface;

/**
 * Class CloudwatchMetricClient
 * @package App\Services\HealthCheckService\Metric
 */
class CloudwatchMetricClient implements CloudwatchMetricClientInterface
{
    /**
     * Unhealthy metric value.
     */
    protected const UNHEALTHY = 1;
    /**
     * Healthy metric value.
     */
    protected const HEALTHY = 0;
    /**
     * @var LoggerInterface
     */
    private $log;
    /**
     * Instance of CloudWatch client.
     * @object CloudWatchClient
     */
    protected $cloudwatch;

    /**
     * AwsMetricClient constructor.
     *
     * @param LoggerInterface $log
     */
    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
        $this->cloudwatch = call_user_func($this->instantiateClient());
    }

    /**
     * Send healthy heartbeat.
     */
    public function putHealthy(): void
    {
        $this->putMetric(self::HEALTHY);
    }

    /**
     * Send Unhealthy heartbeat.
     */
    public function putUnhealthy(): void
    {
        $this->putMetric(self::UNHEALTHY);
    }

    /**
     * Send custom metric to CloudWatch.
     *
     * @param int $value
     */
    protected function putMetric(int $value = 0): void
    {
        try {
            $this->cloudwatch->putMetricData([
                'Namespace'  => config('health.config.cloudwatch.namespace'),
                'MetricData' => [
                    [
                        'MetricName'        => config('health.config.cloudwatch.metric_name'),
                        'Timestamp'         => time(),
                        'Value'             => $value,
                        'Unit'              => 'Count',
                        'StorageResolution' => config('health.config.cloudwatch.storage_resolution')
                    ]
                ]
            ]);
        } catch (AwsException $e) {
            $this->log->error($e);
        }
    }

    /**
     * Instantiate Aws/CloudWatchClient.
     * @desription This method returns closure with CloudWatchClient, so the client
     * will only be instantiated when it's needed. It's quite expensive to instantiate
     * CloudWatch client in class constructor.
     * @return \Closure
     */
    protected function instantiateClient()
    {
        return function () {
            return new CloudWatchClient([
                'region'  => config('health.config.cloudwatch.region'),
                'version' => 'latest'
            ]);
        };
    }
}
