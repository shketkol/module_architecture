<?php

namespace App\Services\HealthCheckService\Metric\Contracts;

/**
 * Interface CloudwatchMetricClient
 * @package App\Services\HealthCheckService\Metric\Contracts
 */
interface CloudwatchMetricClient
{
    /**
     * Send healthy heartbeat.
     */
    public function putHealthy(): void;

    /**
     * Send Unhealthy heartbeat.
     */
    public function putUnhealthy(): void;
}
