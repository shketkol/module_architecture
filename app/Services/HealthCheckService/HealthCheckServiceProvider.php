<?php

namespace App\Services\HealthCheckService;

use App\Services\HealthCheckService\Contracts\HealthCheckService as HealthCheckServiceInterface;
use App\Services\HealthCheckService\Metric\CloudwatchMetricClient;
use App\Services\HealthCheckService\Metric\Contracts\CloudwatchMetricClient as CloudwatchMetricClientInterface;
use Illuminate\Support\ServiceProvider;

class HealthCheckServiceProvider extends ServiceProvider
{
    /**
     * Bindings.
     *
     * @var array
     */
    public $bindings = [
        HealthCheckServiceInterface::class      => HealthCheckService::class,
        CloudwatchMetricClientInterface::class  => CloudwatchMetricClient::class
    ];
}
