<?php

namespace App\Services\HealthCheckService;

use App\Exceptions\UnhealthyTargetsException;
use App\Services\HealthCheckService\Contracts\HealthCheckService as HealthCheckServiceInterface;
use App\Services\HealthCheckService\Events\HealthCheckPerformed;
use App\Services\HealthCheckService\Events\UnhealthyTargets;

/**
 * Application self-diagnosis, heartbeat service.
 * Class HealthCheckService
 * @package App\Services\HealthCheckService
 */
class HealthCheckService implements HealthCheckServiceInterface
{
    /**
     * Instance of Pragmarx Health Service
     *
     * @var object Pragmarx
     */
    protected $service;

    /**
     * HealthCheckService constructor.
     */
    public function __construct()
    {
        $this->service = app('pragmarx.health');
    }

    /**
     * Check resource health.
     * @return void
     */
    public function checkResources(): void
    {
        $this->service->health()->each(function ($resource) {
            if (!$resource->isHealthy()) {
                event(new UnhealthyTargets());
                throw new UnhealthyTargetsException(
                    sprintf(
                        'Service %s is unhealthy. %s',
                        $resource->name,
                        optional($resource->targets->first())->result->errorMessage
                    )
                );
            }
        });

        event(new HealthCheckPerformed());
    }
}
