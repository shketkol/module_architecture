<?php
namespace App\Services\HealthCheckService\Contracts;

interface HealthCheckService
{
    /**
     * Check resource health.
     * @return void
     */
    public function checkResources(): void;
}
