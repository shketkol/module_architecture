<?php

namespace App\DataTable\Filters;

use App\DataTable\Exceptions\FilterNameNotSetException;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;

abstract class DataTableFilter
{
    /**
     * Filter name, required
     * @var string
     */
    public static $name = '';

    /**
     * DataTableFilter constructor.
     */
    public function __construct()
    {
        if (!static::$name) {
            throw new FilterNameNotSetException();
        }
    }

    /**
     * DataTableFilter constructor.
     *
     * @param \Yajra\DataTables\DataTableAbstract $dataTable
     */
    abstract public function filter(DataTableAbstract $dataTable): void;

    /**
     * Prepare keywords to perform a search request.
     *
     * @param string $values
     * @return array
     */
    public function prepareValues(string $values): array
    {
        return array_filter(
            explode(' ', strtolower($values))
        );
    }

    /**
     * Make filter
     * @param \Yajra\DataTables\DataTableAbstract $dataTable
     * @param string $field
     * @param callable $filter
     */
    protected function makeFilter(
        DataTableAbstract $dataTable,
        string $field,
        callable $filter
    ) {
        $dataTable->filterColumn($field, function (Builder $query, $values) use ($filter) {
            if (!$values) {
                return;
            }
            $valuesList = $this->prepareValues($values);
            $filter($query, $valuesList);
        });
    }

    /**
     * Get filter options
     * @return array
     */
    public function options()
    {
        return [];
    }

    /**
     * Options to response
     */
    public function toResponse()
    {
        $options = [];

        foreach ($this->options() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label,
            ];
        }

        return $options;
    }
}
