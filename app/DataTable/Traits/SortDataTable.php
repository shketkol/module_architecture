<?php

namespace App\DataTable\Traits;

use App\Repositories\Contracts\Repository;
use Illuminate\Support\Arr;
use Modules\Campaign\DataTable\Repositories\Criteria\SortNullableLast;

trait SortDataTable
{
    /**
     * @var null|string
     */
    protected $sortNullableLastColumn = null;

    /**
     * @var array
     */
    protected $sortMapping = [
        'desc' => 'DESCENDING',
        'asc'  => 'ASCENDING',
    ];

    /**
     * Check if sorting column for nullLast present in request
     *
     * @return bool
     */
    protected function checkNullableFirstSorting(): bool
    {
        $orderArray = $this->getSortColumn();

        if (!$orderArray || !$this->sortNullableLastColumn) {
            return false;
        }

        $orderColumn = $orderArray['column'];

        if (isset($this->request->columns[$orderColumn]) &&
            $this->request->columns[$orderColumn]['name'] === $this->sortNullableLastColumn
        ) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    protected function getSortColumn(): ?array
    {
        return Arr::first($this->request->order);
    }

    /**
     * @return string
     */
    protected function getSortColumnName(): ?string
    {
        $column = Arr::get($this->getSortColumn(), 'column');
        return $column !== null ? Arr::get($this->request->columns[$column], 'name') : null;
    }

    /**
     * @return string
     */
    protected function getSortColumnDirection(): ?string
    {
        $direction = Arr::get($this->getSortColumn(), 'dir');
        return $direction === 'desc' || $direction === 'asc' ? $direction : null;
    }

    /**
     * @param Repository $repository
     *
     * @return Repository
     */
    protected function applySortCriteria(Repository $repository): Repository
    {
        if ($this->checkNullableFirstSorting()) {
            $repository->pushCriteria(
                new SortNullableLast($this->sortNullableLastColumn, $this->getSortColumnDirection())
            );
        }

        return $repository;
    }

    /**
     * @return string
     */
    protected function getExternalSortColumn(): string
    {
        return array_search($this->getSortColumnName(), $this->columnMapper);
    }

    /**
     * @return string
     */
    protected function getExternalSortDirection(): string
    {
        return Arr::get($this->sortMapping, $this->getSortColumnDirection());
    }
}
