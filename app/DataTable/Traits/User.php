<?php

namespace App\DataTable\Traits;

use Illuminate\Contracts\Auth\Authenticatable;
use Modules\User\Models\User as UserModel;

trait User
{
    /**
     * @var \Modules\User\Models\User|Authenticatable
     */
    protected $user;

    /**
     * @var UserModel|Authenticatable
     */
    protected $advertiser;

    /**
     * @return UserModel|Authenticatable
     */
    public function getUser(): UserModel
    {
        return $this->user;
    }

    /**
     * @param Authenticatable|UserModel $user
     */
    public function setUser(UserModel $user): void
    {
        $this->user = $user;
    }

    /**
     * @return UserModel|Authenticatable
     */
    public function getAdvertiser(): UserModel
    {
        return $this->advertiser;
    }

    /**
     * @param UserModel|Authenticatable $advertiser
     */
    public function setAdvertiser(UserModel $advertiser): void
    {
        $this->advertiser = $advertiser;
    }
}
