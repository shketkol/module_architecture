<?php

namespace App\DataTable;

use App\DataTable\Exceptions\DataTableNameNotSetException;
use App\DataTable\Exceptions\RepositoryNotSetException;
use App\DataTable\Cards\Card;
use App\DataTable\Traits\SortDataTable;
use App\DataTable\Requests\BaseRequest;
use App\Repositories\Contracts\Repository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\DataTable\Traits\User as UserTrait;
use Modules\User\Repositories\UserRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\DataTables;

abstract class DataTable extends DataTables
{
    use UserTrait, SortDataTable;

    /**
     * Table name should be specified
     *
     * @var string
     */
    public static $name = '';

    /**
     * Add aliases according model relation.
     *
     * @var array
     */
    protected $relationAliases = [];

    /**
     * Array of filters.
     *
     * @var array
     */
    protected $filters = [];

    /**
     * List of cards
     *
     * @var array
     */
    protected $cards = [];

    /**
     * @var null
     */
    protected $transformer = null;

    /**
     * @var null
     */
    protected $repository = null;

    /**
     * @var null
     */
    private $repositoryInstance = null;

    /**
     * BaseDataTable constructor.
     *
     * @param BaseRequest $request
     * @param UserRepository $userRepository
     * @param Authenticatable $user
     *
     * @throws DataTableNameNotSetException
     */
    public function __construct(BaseRequest $request, UserRepository $userRepository, ?Authenticatable $user = null)
    {
        if (!static::$name) {
            throw new DataTableNameNotSetException();
        }

        $this->request = $request;

        $this->setUser($user);
        $this->setAdvertiser($user);

        if ($request->user_id) {
            $this->setAdvertiser($userRepository->find((int)$request->user_id));
        }

        $this->checkPolicy();
    }

    /**
     * Maps DataTable columns to models relation.
     */
    public function mapColumnRelations(): void
    {
        $columns = $this->request->get('columns');

        if (empty($columns)) {
            return;
        }

        foreach ($columns as $key => $column) {
            if (array_key_exists($column['name'], $this->relationAliases)) {
                $columns[$key]['name'] = $this->relationAliases[$column['name']];
            }
        }

        $this->request->offsetSet('columns', $columns);
    }

    /**
     * Enable filters for the data table.
     *
     * @param DataTableAbstract $dataTable
     */
    protected function enableFilters(DataTableAbstract $dataTable): void
    {
        foreach ($this->filters as $className) {
            app($className)->filter($dataTable);
        }
    }

    /**
     * Query
     *
     * @return Builder
     */
    abstract protected function createQuery(): Builder;

    /**
     * Check if user can view table
     *
     * @return bool
     */
    abstract protected function can(): bool;

    /**
     * Create response
     *
     * @return mixed
     */
    public function toResponse()
    {
        $dataTable = $this->eloquent($this->createQuery());
        $this->mapColumnRelations();
        $this->enableFilters($dataTable);

        return $dataTable->setTransformer(app($this->transformer))
            ->escapeColumns()
            ->make(true);
    }

    /**
     * Get datatable cards
     *
     * @return array
     */
    public function getCards(): array
    {
        $cards = $this->cards;
        return array_map(function ($card, $key) use ($cards) {
            /** @var Card $card */
            $card = app($card);
            $card->setAdvertiser($this->getAdvertiser());
            $card->setUser($this->getUser());
            $card->setIndex($key);

            return $card->toArray();
        }, $cards, array_keys($cards));
    }

    /**
     * Get repository
     *
     * @return Repository
     * @throws RepositoryNotSetException
     */
    protected function getRepository(): Repository
    {
        if (!$this->repository) {
            throw new RepositoryNotSetException();
        }

        if (!$this->repositoryInstance) {
            $this->repositoryInstance = app($this->repository);
        }

        return $this->repositoryInstance;
    }

    /**
     * Check data table policy
     */
    private function checkPolicy(): void
    {
        if (!$this->can()) {
            throw new AccessDeniedHttpException(Response::$statusTexts[Response::HTTP_FORBIDDEN]);
        }
    }
}
