<?php

namespace App\DataTable;

use App\DataTable\Exceptions\DataTableNotFoundException;
use Illuminate\Http\Request;
use Modules\Advertiser\DataTable\AdvertiserDataTable;
use Modules\Broadcast\DataTable\BroadcastDataTable;
use Modules\Campaign\DataTable\CampaignAdminDataTable;
use Modules\Campaign\DataTable\CampaignAdvertiserDataTable;
use Modules\Campaign\DataTable\CampaignDataTable;
use Modules\Creative\DataTable\CreativeApprovedDataTable;
use Modules\Invitation\DataTable\InvitationDataTable;
use Modules\Notification\DataTable\NotificationDataTable;
use Modules\Payment\DataTable\CampaignTransactionsDataTable;
use Modules\Creative\DataTable\CreativeAdvertiserDataTable;
use Modules\Creative\DataTable\CreativeDataTable;
use Modules\Creative\DataTable\CreativeGalleryDataTable;
use Modules\Report\DataTable\AdminReportDataTable;
use Modules\Report\DataTable\ReportDataTable;

/**
 * Class DataTableFactory
 * Simple factory, responsible for creating datatable from request
 *
 * @package App\DataTable
 */
class DataTableFactory
{
    /**
     * @var array
     */
    protected static $dataTables = [
        CampaignDataTable::class,
        CampaignAdminDataTable::class,
        CampaignAdvertiserDataTable::class,
        CampaignTransactionsDataTable::class,
        CreativeDataTable::class,
        CreativeGalleryDataTable::class,
        CreativeApprovedDataTable::class,
        AdvertiserDataTable::class,
        CreativeAdvertiserDataTable::class,
        ReportDataTable::class,
        NotificationDataTable::class,
        AdminReportDataTable::class,
        InvitationDataTable::class,
        BroadcastDataTable::class,
    ];

    /**
     * Create datatable from request
     *
     * @param Request $request
     *
     * @return DataTable
     * @throws DataTableNotFoundException
     */
    public function createFromRequest(Request $request): DataTable
    {
        if (!$request->table) {
            throw new DataTableNotFoundException();
        }

        $dataTable = array_filter(static::$dataTables, function ($tableClass) use ($request) {
            return $request->table === $tableClass::$name;
        });

        if (empty($dataTable)) {
            throw new DataTableNotFoundException();
        }

        $dataTable = array_shift($dataTable);

        return app($dataTable);
    }
}
