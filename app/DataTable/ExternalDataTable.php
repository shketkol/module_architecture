<?php

namespace App\DataTable;

use App\DataTable\Traits\SortDataTable;
use App\DataTable\Types\HaapiDataTable;
use App\DataTable\Traits\User as UserTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Modules\Haapi\Exceptions\HaapiException;
use Yajra\DataTables\DataTableAbstract;

abstract class ExternalDataTable extends DataTable
{
    use UserTrait, SortDataTable;

    /**
     * @var array
     */
    public $columnMapper = [];

    /**
     * @var string
     */
    protected $externalAction;

    /**
     * @var string
     */
    protected $payloadKey;

    /**
     * Create response
     *
     * @return mixed
     */
    public function toResponse()
    {
        $response = $this->makeRequest();
        if ($response) {
            $response = $response->getPayload();

            $resultByKey = $this->mapResponse(Arr::get($response, $this->payloadKey));
            unset($response[$this->payloadKey]);

            if (!$resultByKey) {
                $response = [];
            } else {
                Arr::set($response, 'items', $resultByKey);
            }
        }

        $dataTable = $this->collection($response);
        $dataTable->escapeColumns([]);

        return $dataTable->make(true);
    }

    /**
     * Create query
     *
     * @return Builder
     * @throws \App\DataTable\Exceptions\RepositoryNotSetException
     */
    public function createQuery(): Builder
    {
    }

    /**
     * @param array|\Illuminate\Support\Collection $collection
     *
     * @return \Yajra\DataTables\CollectionDataTable|\Yajra\DataTables\DataTableAbstract
     */
    public function collection($collection): DataTableAbstract
    {
        return HaapiDataTable::create($collection);
    }

    /**
     * @param array $response
     *
     * @return array
     */
    protected function mapResponse(array $response): array
    {
        return array_map(function ($entity) {
            foreach ($this->columnMapper as $oldValue => $newValue) {
                if (Arr::has($entity, $oldValue)) {
                    $entity[$newValue] = $entity[$oldValue];
                    unset($entity[$oldValue]);
                    continue;
                }
                $entity[$newValue] = null;
            }
            return $entity;
        }, $response);
    }

    /**
     * @return array|null
     */
    protected function makeRequest()
    {
        $action = app($this->externalAction);
        try {
            return $action->handle($this->prepareParams(), $this->user->id);
        } catch (HaapiException $exception) {
            return [];
        }
    }

    /**
     * @return array|null
     */
    protected function getFiltersValue(): ?array
    {
        $columns = $this->request->get('columns');
        $filterColumns = array_filter($columns, function ($column) {
            return Arr::get($column, 'search.value');
        });

        $filters = [];
        foreach ($filterColumns as $column) {
            if (Arr::has($column, 'search.value')) {
                $filters[$column['name']] = Arr::get($column, 'search.value');
            }
        }

        return count($filters) ? $filters : null;
    }

    /**
     * @return mixed
     */
    abstract protected function prepareParams();
}
