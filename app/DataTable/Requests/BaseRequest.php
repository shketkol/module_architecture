<?php

namespace App\DataTable\Requests;

use Yajra\DataTables\Utilities\Request;

/**
 * @property int $user_id
 */
class BaseRequest extends Request
{
    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function isRegex($index): bool
    {
        // disable regex search since it not used in the platform
        // and cause errors when invalid regex values are passed in
        return false;
    }

    /**
     * Check if a column is searchable.
     * Override base functionality to make possible to exclude filterable columns from global search
     * See: https://da-jira.sigmaukraine.com/browse/HULU-251
     *
     * @param int  $i
     * @param bool $column_search
     *
     * @return bool
     */
    public function isColumnSearchable($i, $column_search = true)
    {
        if ($column_search) {
            return
                (
                    $this->request->input("columns.$i.filterable", 'true') === 'true'
                    ||
                    $this->request->input("columns.$i.filterable", 'true') === true
                )
                && $this->columnKeyword($i) != '';
        }

        return
            $this->request->input("columns.$i.searchable", 'true') === 'true'
            ||
            $this->request->input("columns.$i.searchable", 'true') === true;
    }
}
