<?php

namespace App\DataTable\Cards;

class PlaceholderCard extends Card
{
    /**
     * Card Type
     *
     * @var string
     */
    protected $type = 'number';

    /**
     * @var int
     */
    private static $index = 0;

    /**
     * PlaceholderCard constructor.
     */
    public function __construct()
    {
        $this->title = trans('labels.widget', ['index' => ++self::$index]);
    }

    /**
     * Collect data
     *
     * @return float
     */
    public function collect(): float
    {
        return 0;
    }
}
