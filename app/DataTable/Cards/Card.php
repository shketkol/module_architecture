<?php

namespace App\DataTable\Cards;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Log;
use Modules\User\Models\User;
use App\DataTable\Traits\User as UserTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Card
 * Base Card class
 *
 * @package App\DataTableCards
 */
abstract class Card
{
    use UserTrait;

    /**
     * Card type number|chart
     *
     * @var string
     */
    protected $type;

    /**
     * Card title
     *
     * @var string
     */
    protected $title;

    /**
     * @var User|Authenticatable
     */
    protected $user;

    /**
     * @var integer
     */
    protected $order;

    /**
     * Card to array
     *
     * @return array
     */
    public function toArray(): array
    {
        try {
            $value = $this->collect();
        } catch (AccessDeniedHttpException $e) {
            $value = null;
        } catch (\Throwable $e) {
            $value = null;
            Log::logException($e);
        }

        return [
            'type'  => $this->type,
            'value' => $value,
            'title' => $this->title,
            'index' => $this->order,
        ];
    }

    /**
     * @param int $index
     */
    public function setIndex(int $index): void
    {
        $this->order = $index;
    }

    /**
     * Collect value
     *
     * @return float
     */
    abstract public function collect(): float;
}
