<?php

namespace App\DataTable\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use League\Fractal\TransformerAbstract;

abstract class DataTableTransformer extends TransformerAbstract
{
    /**
     * Mapping fields.
     *
     * @var array
     */
    protected array $mapMap = [];

    /**
     * Do transform.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return array
     */
    public function transform(Model $model): array
    {
        $mapped = [];

        foreach ($this->mapMap as $fieldName => $field) {
            $mapped[$fieldName] = Arr::get($model, $field['name'], $field['default']);
            if (isset($field['formatters'])) {
                $mapped[$fieldName] = $this->applyFormatters($mapped[$fieldName], $field['formatters']);
            }
        }

        return $mapped;
    }

    /**
     * Apply formatters
     *
     * @param string $mapped
     * @param array  $formatters
     *
     * @return string
     */
    protected function applyFormatters(string $mapped, array $formatters): string
    {
        foreach ($formatters as $formatter) {
            $mapped = $this->$formatter($mapped);
        }

        return $mapped;
    }
}
