<?php

namespace App\DataTable\Transformers\Traits;

trait MaxLength
{
    /**
     * Return default max length for string
     */
    protected function getStringMaxLength(): int
    {
        return config('format.string.maxLength');
    }

    /**
     * Add maxlength with three dots to long string
     * @param $mapped
     * @return string
     */
    protected function addMaxLength(string $mapped): string
    {
        return strlen($mapped) > $this->getStringMaxLength()
            ? substr($mapped, 0, $this->getStringMaxLength())."..."
            : $mapped;
    }
}
