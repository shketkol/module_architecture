<?php

namespace App\DataTable\Transformers\Traits;

trait StripTags
{
    /**
     * @return string
     */
    private function getAllowedTags(): string
    {
        return config('datatables.allowed_tags');
    }

    /**
     * @param string|null $html
     *
     * @return string
     */
    protected function stripTags(?string $html): string
    {
        if (is_null($html)) {
            return '';
        }

        // add space so when tags removed text wouldn't stuck together
        $html = str_replace('<', ' <', $html);
        $html = strip_tags($html, $this->getAllowedTags());
        $html = str_replace('  ', ' ', $html);

        return trim($html);
    }
}
