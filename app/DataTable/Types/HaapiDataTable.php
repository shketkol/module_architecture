<?php

namespace App\DataTable\Types;

use Illuminate\Support\Arr;
use Yajra\DataTables\CollectionDataTable;

class HaapiDataTable extends CollectionDataTable
{
    /**
     * Count total items.
     *
     * @return int
     */
    public function totalCount()
    {
        return Arr::get($this->collection, 'count', 0);
    }

    /**
     * Perform global search for the given keyword.
     *
     * @param string $keyword
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function globalSearch($keyword)
    {
        return false;
    }

    /**
     * Perform default query orderBy clause.
     */
    protected function defaultOrdering()
    {
        //
    }

    /**
     * Perform pagination.
     *
     * @return void
     */
    public function paging()
    {
        //
    }

    /**
     * Get results.
     *
     * @return mixed
     */
    public function results()
    {
        return Arr::get($this->collection, 'items');
    }

    /**
     * Perform necessary filters.
     *
     * @return int
     */
    protected function filterRecords()
    {
        return $this->filteredRecords = $this->totalRecords;
    }
}
