<?php

namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;

abstract class BaseSheet implements WithEvents
{
    /**
     * Register sheet events.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $this->styleSheet($event->sheet);
            },
        ];
    }

    /**
     * Style the sheet.
     *
     * @param Sheet $sheet
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function styleSheet(Sheet $sheet): void
    {
        //
    }

    /**
     * Add bold styles to the range.
     *
     * @param Sheet  $sheet
     * @param string $range
     *
     * @return BaseSheet
     */
    protected function styleBold(Sheet $sheet, string $range): self
    {
        $sheet->getStyle($range)->applyFromArray([
            'font' => [
                'bold' => true,
            ],
        ]);

        return $this;
    }

    /**
     * Add border styles to the range.
     *
     * @param Sheet  $sheet
     * @param string $range
     *
     * @return BaseSheet
     */
    protected function styleBorder(Sheet $sheet, string $range): self
    {
        $sheet->getStyle($range)->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => config('excel.exports.styles.border'),
                ],
            ],
        ]);

        return $this;
    }

    /**
     * Add fill (background color) styles to the range.
     *
     * @param Sheet  $sheet
     * @param string $range
     * @param string $color
     *
     * @return BaseSheet
     */
    protected function styleFill(Sheet $sheet, string $range, string $color = ''): self
    {
        $sheet->getStyle($range)->applyFromArray([
            'fill' => [
                'fillType'   => Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => $color ?: config('excel.exports.styles.fill_color.argb'),
                ],
            ],
        ]);

        return $this;
    }

    /**
     * Set the width for the column.
     *
     * @param Sheet  $sheet
     * @param string $column
     * @param int    $width
     *
     * @return BaseSheet
     */
    protected function setColumnWidth(Sheet $sheet, string $column, int $width): self
    {
        $sheet->getColumnDimension($column)->setAutosize(false)->setWidth($width);

        return $this;
    }
}
