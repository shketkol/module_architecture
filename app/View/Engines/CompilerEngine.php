<?php

namespace App\View\Engines;

use App\View\Exceptions\HaapiViewException;
use App\View\Helpers\ViewExceptionMapper;
use Exception;
use Facade\Ignition\Views\Engines\CompilerEngine as CompilerEngineBase;
use Modules\Haapi\Exceptions\HaapiException;
use Throwable;

class CompilerEngine extends CompilerEngineBase
{
    use ViewExceptionMapper;

    /**
     * Handle a view exception.
     *
     * @param Throwable $e
     * @param int $obLevel
     * @return void
     *
     * @throws Throwable
     * @throws Exception
     */
    protected function handleViewException(Throwable $e, $obLevel)
    {
        if ($e instanceof HaapiViewException) {
            $this->cleanAndThrow($e, $obLevel);
        }
        if ($e instanceof HaapiException) {
            $viewException = $this->mapToViewException($e);
            $this->cleanAndThrow($viewException, $obLevel);
        }

        parent::handleViewException($e, $obLevel);
    }

    /**
     * @param Throwable $exception
     * @param int $obLevel
     * @throws Throwable
     */
    protected function cleanAndThrow(Throwable $exception, $obLevel)
    {
        while (ob_get_level() > $obLevel) {
            ob_end_clean();
        }

        throw $exception;
    }
}
