<?php

namespace App\View\Exceptions;

use Illuminate\Http\Response;

class HaapiUnauthorizedException extends HaapiViewException
{
    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return redirect()->route('login.form');
    }
}
