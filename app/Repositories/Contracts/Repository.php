<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface Repository
 *
 * @package App\Repositories\Contracts
 */
interface Repository extends RepositoryInterface
{
    /**
     * Find model by id.
     * This method does not throw NOT FOUND exception.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findSoft(int $id, array $columns = ['*']);

    /**
     * Insert a new records into the database.
     *
     * @param array $values
     *
     * @return bool
     * @see \Illuminate\Database\Query\Builder::insert
     */
    public function insert(array $values): bool;

    /**
     * Retrieve the "count" result of the query.
     *
     * @param array  $where
     * @param string $columns
     *
     * @return int
     */
    public function count(array $where = [], $columns = '*');

    /**
     * Retrieve the "sum" result of the query.
     *
     * @param string $columns
     *
     * @return int
     */
    public function sum($columns = '*');

    /**
     * Retrieve the "avg" result of the query.
     *
     * @param string $columns
     *
     * @return int
     */
    public function avg($columns = '*');

    /**
     * Reset all criteria, scopes and model.
     *
     * @return $this
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function reset();

    /**
     * Make apply criteria public.
     *
     * @return Repository
     */
    public function applyCriteria();
}
