<?php

namespace App\Repositories;

use App\Models\Timezone;
use App\Repositories\Contracts\TimezoneRepository as TimezoneRepositoryContract;

/**
 * Class TimezoneRepository
 * @package App\Repositories
 */
class TimezoneRepository extends Repository implements TimezoneRepositoryContract
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Timezone::class;
    }
}
