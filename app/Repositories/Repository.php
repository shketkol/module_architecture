<?php

namespace App\Repositories;

use Illuminate\Container\Container as Application;
use Illuminate\Log\Logger;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * @method \Illuminate\Database\Eloquent\Builder model()
 * @property  \Illuminate\Database\Eloquent\Builder $model
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
abstract class Repository extends BaseRepository implements RepositoryInterface
{
    /**
     * @var Logger
     */
    protected Logger $log;

    /**
     * @param Application $app
     * @param Logger      $log
     */
    public function __construct(Application $app, Logger $log)
    {
        parent::__construct($app);
        $this->log = $log;
    }

    /**
     * Calling model's default functions.
     *
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array([$this->model, $method], $args);
    }

    /**
     * Retrieve the "count" result of the query.
     *
     * @param array  $where
     * @param string $columns
     *
     * @return int
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function count(array $where = [], $columns = '*')
    {
        $this->applyCriteria();
        $this->applyScope();

        return $this->model->count($columns);
    }

    /**
     * Retrieve the "exists" result of the query.
     *
     * @return bool
     */
    public function exists(): bool
    {
        $this->applyCriteria();
        $this->applyScope();

        return $this->model->exists();
    }

    /**
     * Retrieve the "sum" result of the query.
     *
     * @param string $columns
     *
     * @return int
     */
    public function sum($columns = '*')
    {
        $this->applyCriteria();
        $this->applyScope();

        return $this->model->sum($columns);
    }

    /**
     * Retrieve the "avg" result of the query.
     *
     * @param string $columns
     *
     * @return int
     */
    public function avg($columns = '*')
    {
        $this->applyCriteria();
        $this->applyScope();

        return $this->model->avg($columns);
    }

    /**
     * Reset all criteria, scopes and model.
     *
     * @return $this
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function reset()
    {
        $this->resetCriteria();
        $this->resetModel();
        $this->resetScope();
        $this->boot();

        return $this;
    }

    /**
     * Make apply criteria public.
     *
     * @return Repository
     */
    public function applyCriteria()
    {
        parent::applyCriteria();

        return $this;
    }

    /**
     * Find model by id.
     * This method does not throw NOT FOUND exception.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findSoft(int $id, array $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Insert a new records into the database.
     *
     * @param array $values
     *
     * @return bool
     * @see \Illuminate\Database\Query\Builder::insert
     */
    public function insert(array $values): bool
    {
        return $this->model()::insert($values);
    }

    /**
     * Update data in the existed record.
     *
     * @param array  $attributes
     * @param string $id
     *
     * @return bool
     * @see \Illuminate\Database\Query\Builder::insert
     */
    public function updateByExternalId(array $attributes, string $id)
    {
        return $this->model()::where('external_id', $id)->update($attributes);
    }

    /**
     * Update data in the existed record.
     *
     * @param array  $attributes
     * @param string $name
     *
     * @return bool
     * @see \Illuminate\Database\Query\Builder::insert
     */
    public function updateByName(array $attributes, string $name)
    {
        return $this->model()::where('name', $name)->update($attributes);
    }

    /**
     * Delete data in the existed record.
     *
     * @param int[] $ids
     *
     * @return bool
     * @see \Illuminate\Database\Query\Builder::insert
     */
    public function deleteByExternalId(array $ids)
    {
        return $this->model()::whereIn('external_id', $ids)->delete();
    }
}
