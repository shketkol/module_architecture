<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSending;

class MessageSendingListener
{
    /**
     * @param MessageSending $event
     */
    public function handle(MessageSending $event): void
    {
        if ($this->sesHeadersEnabled()) {
            $headers = $event->message->getHeaders();
            $headers->addTextHeader('X-SES-CONFIGURATION-SET', config('services.ses.headers'));
        }
    }

    /**
     * @return bool
     */
    protected function sesHeadersEnabled(): bool
    {
        return config('services.ses.headers_enabled') &&
               config('mail.driver') === 'ses';
    }
}
