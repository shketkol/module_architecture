<?php

namespace App\Exceptions;

class ModelNotCreatedException extends BaseException
{
    /**
     * @param string $message
     *
     * @return static
     */
    public static function create(string $message): self
    {
        return new self($message);
    }
}
