<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

class ModelNotUpdatedException extends BaseException
{
    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response()->json([
            'message' => $this->getMessage(),
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
