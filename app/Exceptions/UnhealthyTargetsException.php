<?php

namespace App\Exceptions;

use http\Client\Response;

class UnhealthyTargetsException extends BaseException
{
    /**
     * Default Status code used for improved logging.
     * Status Code 0 or >=500 - log as an ERROR
     * Exceptions with other codes (such as 422, 403 etc) being logged as WARNING
     */
    public const STATUS_CODE = 500;

    /**
     * Report the exception.
     *
     * @return Response
     */
    public function render()
    {
        return response(
            'Application could not establish connection to one or more external services',
            self::STATUS_CODE
        )->header('Content-Type', 'text/plain');
    }
}
