<?php

namespace App\Exceptions;

use App\Exceptions\Contracts\UnauthorizedException;
use App\View\Exceptions\HaapiGenericException;
use App\View\Exceptions\HaapiUnauthorizedException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Session\SessionManager;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Modules\Campaign\Exceptions\InvalidCampaignActivityRequestException;
use Modules\Daapi\Actions\Notification\Responses\InternalErrorResponse;
use Modules\Payment\Exceptions\OrderNotFoundException;
use Psr\Log\LoggerInterface;
use SM\SMException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /*** Url path part to check if url belongs to API or WEB */
    const API_PREFIX = '/api/';

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var SessionManager
     */
    private $session;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @param Container       $container
     * @param LoggerInterface $log
     * @param SessionManager  $session
     * @param UrlGenerator    $urlGenerator
     */
    public function __construct(
        Container $container,
        LoggerInterface $log,
        SessionManager $session,
        UrlGenerator $urlGenerator
    ) {
        parent::__construct($container);
        $this->log = $log;
        $this->session = $session;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the internal exception types that should not be reported.
     *
     * @var array
     */
    protected $internalDontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        ValidationWithActionException::class,
        InvalidCampaignActivityRequestException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Throwable $e
     *
     * @return void
     *
     * @throws Throwable
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function report(Throwable $e)
    {
        if ($this->shouldntReport($e)) {
            return;
        }

        if (is_callable($reportCallable = [$e, 'report'])) {
            return $this->container->call($reportCallable);
        }

        /** @var \App\Services\LoggerService\LoggerService $logger */
        $logger = $this->container->make(LoggerInterface::class);
        $logger->logException($e);

        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable               $exception
     *
     * @return \Illuminate\Http\JsonResponse|Response
     * @throws Throwable
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof TokenMismatchException) {
            return response()->json([
                'message' => trans('auth::messages.token.mismatch'),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($exception instanceof SMException) {
            return response()->json([
                'message' => trans('state-machine.transitions.error'),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($exception instanceof UnauthorizedException ||
            $exception instanceof HaapiGenericException ||
            $exception instanceof HaapiUnauthorizedException) {
            Auth::logout();
        }

        if ($exception instanceof AuthenticationException) {
            return $this->handleAuthenticationException();
        }

        if ($exception instanceof AuthorizationException && $this->isWebUrl($this->urlGenerator->current())) {
            return $this->handleAuthorizationException();
        }

        if ($exception instanceof OrderNotFoundException) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof ValidationWithActionException) {
            return response()->json([
                'is_action_validation' => true,
                'message'              => $exception->getMessage(),
                'route'                => [
                    'name'   => $exception->getActionRouteName(),
                    'method' => $exception->getActionHttpMethod(),
                    'params' => $exception->getRequestParams(),
                ],
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Internal error
        if ($exception instanceof \ErrorException || $exception instanceof \ReflectionException) {
            // comes from DAAPI
            $action = $request->route() ? $request->route()->action : null;
            if (in_array('client', Arr::get($action, 'middleware', []))) {
                /** @var InternalErrorResponse $response */
                $response = app(InternalErrorResponse::class);

                return $response->get($request);
            }
        }

        return parent::render($request, $exception);
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param \Illuminate\Validation\ValidationException $e
     * @param \Illuminate\Http\Request                   $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        if ($e->response) {
            $this->log->warning('Validation error.', [
                'response' => $e->response->getContent(),
                'uri'      => $request->route()->uri(),
            ]);
            return $e->response;
        }

        $this->log->warning('Validation error.', [
            'message' => $e->getMessage(),
            'errors'  => $e->errors(),
            'uri'     => $request->route()->uri(),
        ]);

        return $request->expectsJson()
            ? $this->invalidJson($request, $e)
            : $this->invalid($request, $e);
    }

    /**
     * Saves previous url in the session to redirect user to that page after login.
     * Returns appropriate response with message and error code.
     *
     * @return RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function handleAuthenticationException()
    {
        $currentUrl = $this->urlGenerator->current();

        if ($this->isWebUrl($currentUrl)) {
            $this->session->put('target_url', $currentUrl);
        } else {
            return \response()->json([
                'redirect' => route('login.form'),
            ], Response::HTTP_FORBIDDEN);
        }

        return \response()->redirectToRoute('login.form');
    }

    /**
     * Helps to filter out API urls
     * @return RedirectResponse
     */
    protected function handleAuthorizationException(): RedirectResponse
    {
        return \response()->redirectToRoute('campaigns.index');
    }

    /**
     * Helps to filter out API urls
     *
     * @param string $url
     *
     * @return bool
     */
    protected function isWebUrl(string $url): bool
    {
        $path = parse_url($url, PHP_URL_PATH);

        return !Str::startsWith($path, self::API_PREFIX);
    }
}
