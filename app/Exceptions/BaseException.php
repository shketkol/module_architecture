<?php

namespace App\Exceptions;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Throwable;

class BaseException extends \Exception
{
    /**
     * Default Status code used for improved logging.
     * Status Code 0 or >=500 - log as an ERROR
     * Exceptions with other codes (such as 422, 403 etc) being logged as WARNING
     */
    public const STATUS_CODE = 0;

    /**
     * Exception log levels.
     */
    public const LOG_LEVEL_ERROR = 'error';
    public const LOG_LEVEL_WARNING = 'warning';

    /**
     * Default log level.
     * @var string
     */
    protected $logLevel = self::LOG_LEVEL_ERROR;

    /**
     * BaseException constructor.
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $statusCode = $code ?: static::STATUS_CODE;

        parent::__construct($message, $statusCode, $previous);
        Log::logException($this, true);
    }

    /**
     * Create from existing exception
     * @param \Throwable $exception
     * @return \App\Exceptions\BaseException
     */
    public static function createFrom(\Throwable $exception)
    {
        return new static($exception->getMessage(), $exception->getCode(), $exception);
    }

    /**
     * @return string
     */
    public function getLogLevel(): string
    {
        if ($this->getCode() &&
            $this->getCode() < Response::HTTP_INTERNAL_SERVER_ERROR) {
            return self::LOG_LEVEL_WARNING;
        }
        return $this->logLevel;
    }
}
