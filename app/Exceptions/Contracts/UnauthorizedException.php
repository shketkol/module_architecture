<?php

namespace App\Exceptions\Contracts;

use Throwable;

interface UnauthorizedException extends Throwable
{

}
