<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

abstract class ValidationWithActionException extends BaseException
{
    /**
     * ValidationWithActionException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message, Response::HTTP_UNPROCESSABLE_ENTITY, null);
    }

    /**
     * @return string
     */
    abstract public function getActionRouteName(): string;

    /**
     * @return string
     */
    abstract public function getActionHttpMethod(): string;

    /**
     * @return array
     */
    public function getRequestParams(): array
    {
        return [];
    }
}
