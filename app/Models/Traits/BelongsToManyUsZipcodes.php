<?php

namespace App\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Location\Models\UsZipcode;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property UsZipcode[]|Collection $zipcodes
 */
trait BelongsToManyUsZipcodes
{
    /**
     * @return BelongsToMany
     */
    public function zipcodes(): BelongsToMany
    {
        return $this->belongsToMany(
            UsZipcode::class,
            'us_states_zipcodes',
            'state_id',
            'zipcode_id'
        );
    }
}
