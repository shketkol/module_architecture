<?php

namespace App\Models\Traits;

use Illuminate\Support\Collection;
use Modules\Location\Models\UsState;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property UsState[]|Collection $states
 */
trait BelongsToManyUsStates
{
    /**
     * @return BelongsToMany
     */
    public function states(): BelongsToMany
    {
        return $this->belongsToMany(
            UsState::class,
            'us_states_zipcodes',
            'zipcode_id',
            'state_id'
        );
    }
}
