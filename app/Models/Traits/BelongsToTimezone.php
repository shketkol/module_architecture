<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Timezone;

/**
 * @property Timezone $timezone
 */
trait BelongsToTimezone
{
    /**
     * @return BelongsTo
     */
    public function timezone(): BelongsTo
    {
        return $this->belongsTo(Timezone::class)->withDefault([
            'code' => '',
            'name' => '',
        ]);
    }
}
