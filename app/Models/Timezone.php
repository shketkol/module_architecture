<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string  $name
 * @property string  $code
 * @property string  $full_code
 */
class Timezone extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public const ID_EASTERN         = 1;
    public const ID_CENTRAL         = 2;
    public const ID_MOUNTAIN        = 3;
    public const ID_PACIFIC         = 4;
    public const ID_ALASKA          = 5;
    public const ID_HAWAII_ALEUTIAN = 6;

    /**
     * @var array
     */
    protected $fillable = ['name', 'code', 'full_code'];
}
