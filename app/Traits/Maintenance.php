<?php

namespace App\Traits;

use Illuminate\Support\Facades\Cache;

/**
 * Overrides Laravel's maintenance mode to use the cache instead of the filesystem
 * so that maintenance mode is propagated across all servers and job queues
 */
trait Maintenance
{
    /**
     * @return string
     */
    private function getKey(): string
    {
        return 'framework/down';
    }

    /**
     * @return bool
     */
    private function isDown(): bool
    {
        return Cache::has($this->getKey());
    }

    /**
     * @return bool
     */
    private function isUp(): bool
    {
        return !$this->isDown();
    }

    /**
     * @param array $data
     */
    private function setMaintenanceData(array $data): void
    {
        Cache::forever($this->getKey(), $data);
    }

    /**
     * Disable maintenance mode.
     */
    private function deleteMaintenanceData(): void
    {
        Cache::forget($this->getKey());
    }

    /**
     * @return array
     */
    private function getMaintenanceData(): array
    {
        return Cache::get($this->getKey(), []);
    }
}
