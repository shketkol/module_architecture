<?php

namespace App\Traits;

use App\Exceptions\InvalidArgumentException;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory as BaseHasFactory;

trait HasFactory
{
    use BaseHasFactory;

    /**
     * Create a new factory instance for the model.
     *
     * @return Factory
     * @throws InvalidArgumentException
     */
    protected static function newFactory(): Factory
    {
        // "Modules\User\Models\User"
        $fqcn = get_called_class();
        [$modules, $module, $models, $model] = explode('\\', $fqcn);

        if ($modules !== 'Modules' || $models !== 'Models') {
            throw new InvalidArgumentException(sprintf(
                'Model "%s" does not follow app structure "%s".',
                $fqcn,
                'Modules\\ModuleName\\Models\\ModelName'
            ));
        }

        // "Modules\User\Database\Factories\UserFactory"
        $factory = $modules . '\\' . $module . '\\Database\\Factories\\' . $model . 'Factory';

        return $factory::new();
    }
}
