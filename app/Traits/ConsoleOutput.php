<?php

namespace App\Traits;

use Illuminate\Console\OutputStyle;
use Modules\Targeting\Commands\Traits\Progress;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput as SymfonyConsoleOutput;

trait ConsoleOutput
{
    use Progress;

    /**
     * @var
     */
    protected $output;

    /**
     * @return OutputStyle
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function getOutput(): OutputStyle
    {
        if (is_null($this->output)) {
            $this->output = app()->make(OutputStyle::class, [
                'input'  => app()->make(ArrayInput::class, ['parameters' => []]),
                'output' => app(SymfonyConsoleOutput::class),
            ]);
        }

        return $this->output;
    }

    /**
     * @param string $message
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function info(string $message): void
    {
        $this->getOutput()->writeln(sprintf('<info>%s</info>', $message));
    }
}
