<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Modules\Daapi\Exceptions\CanNotApplyStatusException;

/**
 * @mixin Model
 */
trait State
{
    /**
     * @var \SM\StateMachine\StateMachine
     */
    protected $state = null;

    /**
     * @param string $transition
     * @param string $flow
     * @return int
     */
    public static function getStatusIdByTransactionFlow(string $transition, string $flow): int
    {
        $transition = config('daapi.status_mapping.' . $flow . '.' . Str::lower($transition));
        return config('state-machine.' . $flow . '.transitions.' . $transition . '.to');
    }

    /**
     * Get current model state.
     *
     * @return \SM\StateMachine\StateMachine
     */
    public function getState()
    {
        if (is_null($this->state)) {
            $this->state = \StateMachine::get($this, $this->flow);
        }

        return $this->state;
    }

    /**
     * Checks if state is same as $value or in one of states if value is array
     *
     * @param $value
     *
     * @return bool
     */
    public function inState($value): bool
    {
        $state = (int)$this->getState()->getState();

        return is_array($value)
            ? in_array($state, $value)
            : $state === $value;
    }

    /**
     * Checks if $transition applicable
     *
     * @param $transition
     *
     * @return bool
     * @throws \SM\SMException
     */
    public function canApply($transition): bool
    {
        return $this->getState()->can($transition);
    }

    /**
     * @param string $transition
     *
     * @return string
     */
    public function findTransition(string $transition): string
    {
        return config('daapi.status_mapping.' . $this->getState()->getGraph() . '.' . Str::lower($transition));
    }

    /**
     * @param string $transition
     *
     * @throws CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function applyHaapiStatus(string $transition): void
    {
        $transition = $this->findTransition($transition);

        $this->applyInternalStatus($transition);
    }

    /**
     * @param string $transition
     *
     * @throws CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function applyInternalStatus(string $transition): void
    {
        if ($this->status_id === $this->getStatusIdByTransaction($transition)) {
            // same status skip update
            return;
        }

        if (!$this->canApply($transition)) {
            throw CanNotApplyStatusException::create($this, $transition);
        }

        $this->getState()->apply($transition);
        $this->save();
    }

    /**
     * @param string $transaction
     * @return int
     */
    protected function getStatusIdByTransaction(string $transaction): int
    {
        return config('state-machine.' . $this->getState()->getGraph() . '.transitions.' . $transaction . '.to');
    }
}
