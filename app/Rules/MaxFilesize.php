<?php

namespace App\Rules;

use App\Helpers\FileSizeHelper;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

/**
 * @package App\Rules
 */
class MaxFilesize implements Rule
{
    /**
     * Max value should be in bytes
     *
     * @var integer
     */
    private $max;

    /**
     * Allowed format
     *
     * @var array
     */
    private $allowedFormats = ['b', 'kb', 'mb', 'gb'];

    /**
     * Format used in message
     *
     * @var string
     */
    private $format;

    /**
     * Identify if attribute is being related to creative.
     *
     * @var bool
     */
    private $errorMessageType;

    /**
     * @param mixed $param
     */
    public function __construct($param)
    {
        if (is_array($param)) {
            $this->max = Arr::get($param, 0);
            $this->format = $this->setFormat(Arr::get($param, 1));
            $this->errorMessageType = Arr::get($param, 2, false);
            return;
        }

        $this->max = $param;
        $this->format = $this->setFormat('b');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return $this->max >= $value;
    }

    /**
     * Set rule format
     *
     * @param string $format
     *
     * @return string
     */
    public function setFormat($format): string
    {
        return in_array($format, $this->allowedFormats) ? $format : 'b';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        $ruleKey = null;
        switch ($this->errorMessageType) {
            case 'creative':
                $ruleKey = 'validation.max_filesize_creative';
                break;
            case 'zip_codes':
                $ruleKey = 'targeting::messages.fail_max_file_size';
                break;
            default:
                $ruleKey = 'validation.max_filesize';
                break;
        }

        return __($ruleKey, ['format' => $this->convert()]);
    }

    /**
     * @return string
     */
    private function convert(): string
    {
        $message = $this->max . ' bytes';

        switch ($this->format) {
            case 'kb':
                $message = FileSizeHelper::toKilobytes($this->max) . ' KB';
                break;
            case 'mb':
                $message = FileSizeHelper::toMegabytes($this->max) . ' MB';
                break;
            case 'gb':
                $message = FileSizeHelper::toGigabytes($this->max) . ' GB';
                break;
        }

        return $message;
    }

    /**
     * @param array $array
     *
     * @return self
     */
    public static function __set_state(array $array)
    {
        return new self(Arr::only($array, ['max', 'allowed']));
    }
}
