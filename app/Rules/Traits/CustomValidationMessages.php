<?php

namespace App\Rules\Traits;

use Illuminate\Contracts\Translation\Translator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait CustomValidationMessages
{
    /**
     * @var Translator
     */
    private static $translatorInstance;

    /**
     * @return Translator
     */
    public function translator()
    {
        if (!self::$translatorInstance) {
            self::$translatorInstance = app(Translator::class);
        }
        return self::$translatorInstance;
    }

    /**
     * Get the custom error message from translator.
     *
     * @param  string  $key
     * @return string
     */
    protected function getCustomMessageFromTranslator($key)
    {
        if (($message = $this->translator()->get($key)) !== $key) {
            return $message;
        }

        // If an exact match was not found for the key, we will collapse all of these
        // messages and loop through them and try to find a wildcard match for the
        // given key. Otherwise, we will simply return the key's value back out.
        $shortKey = preg_replace('/^validation\.custom\./', '', $key);

        $customMessage = $this->getWildcardCustomMessages(Arr::dot(
            (array) $this->translator()->get('validation.custom')
        ), $shortKey, $key);

        return $customMessage !== $key ? $customMessage : '';
    }

    /**
     * Check the given messages for a wildcard key.
     *
     * @param  array  $messages
     * @param  string  $search
     * @param  string  $default
     * @return string
     */
    protected function getWildcardCustomMessages($messages, $search, $default)
    {
        foreach ($messages as $key => $message) {
            if ($search === $key || (Str::contains($key, ['*']) && Str::is($key, $search))) {
                return $message;
            }
        }

        return $default;
    }
}
