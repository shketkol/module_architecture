<?php

namespace App\Rules;

use App\Helpers\CsvHelper;
use Illuminate\Contracts\Validation\Rule;

/**
 * @package App\Rules
 */
class MaxCountCsv implements Rule
{
    use CsvHelper;

    /**
     * Max count
     *
     * @var integer
     */
    private $maxCount;

    public function __construct($count)
    {
        $this->maxCount = $count;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        try {
            $data = $this->parse($value);
            return $this->maxCount >= count($data);
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Validation message
     *
     * @return string
     */
    public function message(): string
    {
        return __('targeting::messages.fail_zip_codes_max_count');
    }
}
