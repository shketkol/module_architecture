<?php

namespace App\Rules;

use App\Exceptions\InvalidArgumentException;
use App\Helpers\FileSizeHelper;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

/**
 * Class MinVideoAttribute
 *
 * @package App\Rules
 */
class MinVideoAttribute implements Rule
{
    /**
     * Measurement systems
     */
    public const SYSTEM_DECIMAL = 'decimal';
    public const SYSTEM_BINARY = 'binary';

    /**
     * Formats
     */
    public const FORMAT_KILO = 'k';
    public const FORMAT_MEGA = 'm';

    /**
     * Min value should be in bytes
     *
     * @var integer
     */
    private $min;

    /**
     * Allowed format
     *
     * @var array
     */
    private $allowedFormats = [
        self::FORMAT_KILO,
        self::FORMAT_MEGA,
    ];

    /**
     * Format used in message
     *
     * @var string
     */
    private $format = null;

    /**
     * System used for values validation
     *
     * @var string
     */
    private $system = self::SYSTEM_DECIMAL;

    /**
     * Dimension used in message
     *
     * @var string
     */
    private $dimension;

    /**
     * NOTE: In case of updating logic don't forget update frontend implementation as well.
     *
     * @param mixed $param
     */
    public function __construct($param)
    {
        if (is_array($param)) {
            $this->min = $param[0];
            $this->dimension = $param['dimension'];
            $this->format = $this->setFormat($param['format']);
            $this->system = $this->setSystem(Arr::get($param, 'system'));
            return;
        }

        $this->min = $param;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return $this->min <= $value;
    }

    /**
     * Set rule format
     *
     * @param string $format
     *
     * @return string
     */
    public function setFormat(?string $format): ?string
    {
        return in_array($format, $this->allowedFormats) ? $format : null;
    }

    /**
     * Set measurement system.
     *
     * @param string $system
     *
     * @return string
     */
    private function setSystem(?string $system): ?string
    {
        return in_array($system, [self::SYSTEM_DECIMAL, self::SYSTEM_BINARY]) ? $system : self::SYSTEM_DECIMAL;
    }

    /**
     * @param string $format
     *
     * @return int
     * @throws InvalidArgumentException
     */
    private function getDivider(string $format): int
    {
        switch ($format) {
            case self::FORMAT_KILO:
                return $this->system === self::SYSTEM_BINARY ? FileSizeHelper::KILOBYTE : 1000;
            case self::FORMAT_MEGA:
                return $this->system === self::SYSTEM_BINARY ? FileSizeHelper::MEGABYTE : 1000000;
        }

        throw new InvalidArgumentException('Invalid format');
    }

    /**
     * Convert min value to readable format.
     *
     * @return int
     * @throws InvalidArgumentException
     */
    private function convert(): int
    {
        switch ($this->format) {
            case self::FORMAT_KILO:
                return (int)round($this->min / $this->getDivider($this->format));
            case self::FORMAT_MEGA:
                return (int)round($this->min / $this->getDivider($this->format));
        }

        return $this->min;
    }

    /**
     * Format dimension string
     *
     * @return string
     */
    private function getDimension(): string
    {
        if (!$this->format) {
            return $this->dimension;
        }

        return $this->format . $this->dimension;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.min_video', ['min' => $this->convert(), 'dimension' => $this->getDimension()]);
    }

    /**
     * @param array $array
     *
     * @return self
     */
    public static function __set_state(array $array)
    {
        $data = [
            0           => Arr::get($array, 'min'),
            'dimension' => Arr::get($array, 'dimension'),
            'format'    => Arr::get($array, 'format'),
        ];

        return new self($data);
    }
}
