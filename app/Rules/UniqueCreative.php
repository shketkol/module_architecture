<?php

namespace App\Rules;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Validation\Rule;
use Modules\Creative\Models\Creative;
use Modules\Creative\Repositories\CreativeRepository;

/**
 * Class UniqueCreative
 * @package App\Rules
 */
class UniqueCreative implements Rule
{
    /**
     * @var Creative|null
     */
    private $creative;

    /**
     * @var CreativeRepository
     */
    private $repository;

    /**
     * UniqueCreative constructor.
     */
    public function __construct()
    {
        $this->repository = app(CreativeRepository::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function passes($attribute, $value): bool
    {
        $this->creative = $this
            ->repository
            ->getDuplicate($value, app(Authenticatable::class)->getKey())
            ->first();
        return !$this->creative;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.unique_creative', [
            'name' => e($this->creative->name),
            'href' => route('creatives.show', ['creative' => $this->creative->id]),
        ]);
    }
}
