<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;

/**
 * @package App\Rules
 */
class AllowedAttributes implements ImplicitRule
{
    /**
     * Allowed values
     *
     * @var array
     */
    private $allowed;

    /**
     * @param mixed $allowed
     */
    public function __construct($allowed)
    {
        $this->allowed = is_array($allowed) ? $allowed : explode(',', $allowed);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        if (!is_string($attribute) || strpos($attribute, '.') !== false) {
            return false;
        }

        return in_array($attribute, $this->allowed);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __(
            'validation.custom.allowed_attributes'
        );
    }
}
