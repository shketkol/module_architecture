<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CsvFormat implements Rule
{
    /**
     * Regexp string
     *
     * @var string
     */
    private $regexp;

    /**
     * @param string $regexp
     */
    public function __construct(string $regexp)
    {
        $this->regexp = $regexp;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        $value = str_replace('"', '', $value);
        return preg_match($this->regexp, $value);
    }

    /**
     * Validation message
     *
     * @return string
     */
    public function message(): string
    {
        return __('targeting::messages.fail_file_zip_format');
    }
}
