<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Modules\Location\Models\UsState;
use Modules\Location\Models\UsZipcode;
use Modules\Location\Repositories\UsStateRepository;
use Modules\Location\Repositories\UsZipcodeRepository;

class HasState implements Rule
{
    /**
     * @var UsStateRepository
     */
    private $stateRepository;

    /**
     * @var UsZipcodeRepository
     */
    private $zipcodeRepository;

    /**
     * @var UsState|null
     */
    private $state;

    public function __construct()
    {
        $this->stateRepository = app(UsStateRepository::class);
        $this->zipcodeRepository = app(UsZipcodeRepository::class);
    }

    /**
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        /** @var UsState|null $state */
        $stateName =  $this->getStateAttribute();
        if (is_null($stateName)) {
            return false;
        }

        $state = $this->stateRepository->findWhere([['name', '=', $stateName]])->first();

        if (is_null($state)) {
            return false;
        }

        $this->state = $state;

        /** @var UsZipcode|null $zipcode */
        $zipcode = $this->zipcodeRepository->findWhere([['name', '=', $value]])->first();

        if (is_null($zipcode)) {
            return false;
        }

        return $zipcode->states->contains($state);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.location.zipcode_must_be_in_state', ['state' => optional($this->state)->name]);
    }

    /**
     * Signup and Update requests
     *
     * @return string|null
     */
    private function getStateAttribute(): ?string
    {
        return Arr::get(request()->all(), 'company_address.state') ?: Arr::get(request()->all(), 'state');
    }
}
