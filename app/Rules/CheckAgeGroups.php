<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Targeting\Commands\Traits\AgeGroupsHelper;
use Modules\Targeting\Repositories\AgeGroupRepository;

class CheckAgeGroups implements Rule
{
    use AgeGroupsHelper;

    /**
     * @var AgeGroupRepository
     */
    private $repository;

    public function __construct()
    {
        $this->repository = app(AgeGroupRepository::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param array $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        if (empty($value)) {
            return true;
        }

        $this->repository->resetCriteria();
        return !self::checkGenderMixByValues($this->repository, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __(
            'validation.custom.allowed_attributes'
        );
    }
}
