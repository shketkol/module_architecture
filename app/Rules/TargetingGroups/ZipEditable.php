<?php

namespace App\Rules\TargetingGroups;

use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Targeting\Models\Zipcode;
use Modules\User\Models\User;

class ZipEditable extends CommonTargetingEditable
{
    /**
     * @var array
     */
    protected array $locations;

    /**
     * @param Campaignable $campaign
     * @param User         $user
     * @param array        $locations
     */
    public function __construct(Campaignable $campaign, User $user, array $locations)
    {
        parent::__construct($campaign, $user);
        $this->locations = $locations;
    }

    /**
     * @param array $values
     *
     * @return bool
     */
    protected function validateLiveMode(array $values): bool
    {
        return $this->checkRule() ?
            $this->checkPassedFlow($values) :
            $this->checkDeniedFlow($values);
    }

    /**
     * @param array $values
     *
     * @return bool
     */
    private function checkPassedFlow(array $values): bool
    {
        return !($this->isAllFilled($values) && $this->isAllFilled($this->locations));
    }

    /**
     * @param array $values
     *
     * @return bool
     */
    private function checkDeniedFlow(array $values): bool
    {
        return $this->user->canAccessTargeting(Zipcode::TYPE_NAME) ?
            $this->isAllFilled($values) && $this->isAllFilled($this->locations) :
            $this->isAllFilled($values);
    }

    /**
     * @return bool
     */
    protected function checkRule(): bool
    {
        return $this->targetingRules->checkEditZipTargetings($this->user, $this->campaign);
    }
}
