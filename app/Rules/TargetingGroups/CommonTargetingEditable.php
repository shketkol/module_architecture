<?php

namespace App\Rules\TargetingGroups;

use App\Rules\TargetingGroups\Contracts\TargetingEditable;
use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\Campaign\Policies\Rules\TargetingsRules;
use Modules\User\Models\User;

abstract class CommonTargetingEditable implements TargetingEditable
{
    /**
     * @var Campaignable
     */
    protected Campaignable $campaign;

    /**
     * @var User
     */
    protected User $user;

    /**
     * @var TargetingsRules
     */
    protected TargetingsRules $targetingRules;

    /**
     * @param Campaignable    $campaign
     * @param User            $user
     * @param array           $additionalParams
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(Campaignable $campaign, User $user, array $additionalParams = [])
    {
        $this->campaign = $campaign;
        $this->user = $user;
        $this->targetingRules = app(TargetingsRules::class);
    }

    /**
     * @param array[] $values
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function validate(array $values): bool
    {
        // if the campaign in live editable state but has no revisions
        // this should not happen with normal flow
        // and would be possible if someone would skip edit page and call validate method directly
        if ($this->campaign->isLiveEditable() && !$this->campaign->isLiveEdit()) {
            return false;
        }

        return $this->campaign->isLiveEdit() ? $this->validateLiveMode($values) : $this->validateDraftMode($values);
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return trans('validation.targeting_to_account');
    }

    /**
     * @param array[] $values
     *
     * @return bool
     */
    protected function validateDraftMode(array $values): bool
    {
        return $this->checkRule() || $this->isAllFilled($values);
    }

    /**
     * @param array[] $values
     *
     * @return bool
     */
    protected function validateLiveMode(array $values): bool
    {
        return $this->checkRule() ? !$this->isAllFilled($values) : $this->isAllFilled($values);
    }

    /**
     * @return bool
     */
    abstract protected function checkRule(): bool;

    /**
     * @param array[] $values
     *
     * @return bool
     */
    protected function isAllFilled(array $values): bool
    {
        return count($values) === 0;
    }
}
