<?php

namespace App\Rules\TargetingGroups;

use Modules\Campaign\Models\Contracts\Campaignable;
use Modules\User\Models\User;

class LocationEditable extends CommonTargetingEditable
{
    /**
     * @var array
     */
    protected array $zips;

    /**
     * @param Campaignable $campaign
     * @param User         $user
     * @param array        $zips
     */
    public function __construct(Campaignable $campaign, User $user, array $zips)
    {
        parent::__construct($campaign, $user);
        $this->zips = $zips;
    }

    /**
     * @param array $values
     *
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function validateLiveMode(array $values): bool
    {
        return $this->checkRule() ?
            !($this->isAllFilled($values) && $this->isAllFilled($this->zips)) :
            $this->isAllFilled($values) && $this->isAllFilled($this->zips);
    }

    /**
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function checkRule(): bool
    {
        return $this->targetingRules->checkEditLocationTargeting($this->user, $this->campaign);
    }
}
