<?php

namespace App\Rules\TargetingGroups;

class AudienceEditable extends CommonTargetingEditable
{
    /**
     * @return bool
     */
    protected function checkRule(): bool
    {
        return $this->targetingRules->checkEditAudienceTargetings($this->user, $this->campaign);
    }
}
