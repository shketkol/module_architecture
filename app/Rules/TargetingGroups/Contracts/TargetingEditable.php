<?php

namespace App\Rules\TargetingGroups\Contracts;

interface TargetingEditable
{
    /**
     * @param array $values
     * @return bool
     */
    public function validate(array $values): bool;

    /**
     * @return string
     */
    public function getMessage(): string;
}
