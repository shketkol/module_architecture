<?php

namespace App\Rules\TargetingGroups;

class GenreEditable extends CommonTargetingEditable
{
    /**
     * @param array $values
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function validateLiveMode(array $values): bool
    {
        return $this->checkRule() || $this->isAllFilled($values);
    }

    /**
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function checkRule(): bool
    {
        return $this->targetingRules->checkEditGenreTargetings($this->user, $this->campaign);
    }
}
