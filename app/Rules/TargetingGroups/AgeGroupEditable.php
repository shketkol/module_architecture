<?php

namespace App\Rules\TargetingGroups;

use Modules\Targeting\Repositories\AgeGroupRepository;
use Prettus\Repository\Exceptions\RepositoryException;

class AgeGroupEditable extends CommonTargetingEditable
{
    /**
     * @param array $values
     *
     * @return bool
     * @throws RepositoryException
     */
    protected function validateDraftMode(array $values): bool
    {
        if ($this->checkRule()) {
            return true;
        }

        return $this->isAllFilled($values);
    }

    /**
     * @param array $values
     *
     * @return bool
     * @throws RepositoryException
     */
    protected function validateLiveMode(array $values): bool
    {
        return $this->checkRule() || $this->isAllFilled($values);
    }

    /**
     * @param array $values
     *
     * @return bool
     * @throws RepositoryException
     */
    protected function isAllFilled(array $values): bool
    {
        /** @var AgeGroupRepository $repository */
        $repository = app(AgeGroupRepository::class);
        $ageGroups = $repository
            ->onlyAllGenderCriteria()
            ->all()
            ->pluck('id')
            ->toArray();

        return count($ageGroups) === count($values) && !array_diff($values, $ageGroups);
    }

    /**
     * @return bool
     * @throws RepositoryException
     */
    protected function checkRule(): bool
    {
        return $this->targetingRules->checkEditAgeTargetings($this->user, $this->campaign);
    }
}
