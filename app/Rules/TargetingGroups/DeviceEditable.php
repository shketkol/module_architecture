<?php

namespace App\Rules\TargetingGroups;

use Modules\Targeting\Repositories\DeviceGroupRepository;

class DeviceEditable extends CommonTargetingEditable
{
    /**
     * @param array[]|int[] $values
     *
     * @return bool
     */
    protected function isAllFilled(array $values): bool
    {
        $values = array_map(function ($value): bool {
            return !empty($value['device_group_id']) ?: $value;
        }, $values);

        /** @var DeviceGroupRepository $repository */
        $repository = app(DeviceGroupRepository::class);
        $deviceGroups = $repository
            ->all()
            ->pluck('id')
            ->toArray();

        return count($deviceGroups) === count($values) && !array_diff($values, $deviceGroups);
    }

    /**
     * @return bool
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function checkRule(): bool
    {
        return $this->targetingRules->checkEditPlatformTargetings($this->user, $this->campaign);
    }
}
