<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class HaapiDate implements Rule
{
    /**
     * Allowed values
     *
     * @var array
     */
    private $allowed;

    /**
     * HaapiDate constructor.
     */
    public function __construct()
    {
        $this->allowed = [
            config('date.format.haapi_milliseconds'),
            config('date.format.haapi')
        ];
    }

    /**
     * Determine if the validation rule passes.
     * Validates haapi date string to be valid ISO-8601.
     *
     * Since PHP DateTime doesn't have native Y-m-d\TH:i:s.uO format (often used by HAAPI as Java default),
     * native laravel validation cannot be used.
     *
     * Supports both 2020-08-05T00:00:00.000+0000(Y-m-d\TH:i:s.uO), 2020-08-05T00:00:00+0000('Y-m-d\TH:i:sO')
     *
     * @param string $attribute
     * @param string $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        $passed = false;

        if (!is_string($value)) {
            return false;
        }

        foreach ($this->allowed as $dateFormat) {
            try {
                Carbon::createFromFormat($dateFormat, $value);
                $passed = true;
            } catch (\Throwable $exception) {
                //
            }
        }

        return $passed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __(
            'validation.custom.haapi_date_format'
        );
    }
}
