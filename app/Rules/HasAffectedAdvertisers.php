<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Modules\Broadcast\Actions\Affected\CountAffectedAdvertisersAction;
use Modules\Broadcast\Models\Segment\BroadcastSegmentGroup;

class HasAffectedAdvertisers implements Rule
{
    /**
     * @var CountAffectedAdvertisersAction
     */
    private $action;

    public function __construct()
    {
        $this->action = app(CountAffectedAdvertisersAction::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param array  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function passes($attribute, $value): bool
    {
        if (Arr::get(request()->get('segment_group'), 'id') !== BroadcastSegmentGroup::ID_AFFECTED_ADVERTISERS) {
            return true;
        }

        $start = (string)Arr::get($value, 'date_start');
        $end = (string)Arr::get($value, 'date_end');

        return $this->action->handle($start, $end) > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('validation.custom.has_affected_advertisers');
    }
}
