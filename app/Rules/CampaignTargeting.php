<?php

namespace App\Rules;

use App\Rules\TargetingGroups\Contracts\TargetingEditable;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Modules\Campaign\Models\Campaign;

class CampaignTargeting implements Rule
{
    /**
     * @var TargetingEditable
     */
    private TargetingEditable $validator;

    /**
     * @param string   $targetingType
     * @param Campaign $campaign Only campaign, not revision
     * @param array    $additionalParams
     */
    public function __construct(string $targetingType, Campaign $campaign, array $additionalParams = [])
    {
        $className = $this->generateClassName($targetingType);

        $this->validator = new $className($campaign, Auth::user(), $additionalParams);
    }

    /**
     * @param string $targetingType
     *
     * @return string
     */
    private function generateClassName(string $targetingType): string
    {
        $nameSpace = 'App\Rules\TargetingGroups\\';
        return $nameSpace . str_replace('_', '', ucwords($targetingType, '_') . 'Editable');
    }

    /**
     * Determine if campaign has access to manage items of the specified targeting.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        return $this->validator->validate($value);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->validator->getMessage();
    }
}
