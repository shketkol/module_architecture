<?php

namespace App\Rules;

use App\Helpers\Math\CalculationHelper;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class MinPerDayImpressions implements Rule
{
    /**
     * @var int
     */
    private int $minPerDay;

    /**
     * @var int
     */
    private int $currentDuration;

    /**
     * @var float
     */
    private float $cpm;

    /**
     * @var int
     */
    private int $impressions;

    /**
     * Determine if the validation rule passes.
     * @param string $attribute
     * @param array  $value
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function passes($attribute, $value): bool
    {
        $this->minPerDay = Arr::get($value, 'minPerDay');
        $this->cpm = Arr::get($value, 'cpm');
        $this->currentDuration = Arr::get($value, 'duration');

        $this->impressions = CalculationHelper::calculateImpressions(
            Arr::get($value, 'cpm'),
            Arr::get($value, 'cost')
        );

        return $this->currentDuration && $this->impressions / $this->currentDuration >= $this->minPerDay;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return __('campaign::messages.min_budget_message', [
            'min_per_day' => number_format((float)$this->minPerDay, 0, '', ','),
            'current_duration' => $this->currentDuration,
            'proposed_duration' => floor($this->impressions / $this->minPerDay),
            'proposed_budget' => CalculationHelper::calculateBudget(
                $this->cpm,
                $this->currentDuration * $this->minPerDay
            ),
        ]);
    }
}
