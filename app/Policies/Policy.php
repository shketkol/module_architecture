<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Model;

class Policy
{
    use HandlesAuthorization;

    /**
     * Model class.
     *
     * @var string
     */
    protected string $model;

    /**
     * Get instance for the policy.
     *
     * @param $model
     * @return Model
     */
    protected function getInstance($model): Model
    {
        if ($model instanceof $this->model) {
            return $model;
        }

        return $this->findById((int) $model);
    }

    /**
     * Find model by its id.
     *
     * @param int $id
     * @return Model
     */
    protected function findById(int $id): Model
    {
        return $this->getModel()->findOrFail($id);
    }

    /**
     * Get model by classname.
     *
     * @return Model
     */
    protected function getModel(): Model
    {
        return new $this->model;
    }
}
