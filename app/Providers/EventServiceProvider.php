<?php

namespace App\Providers;

use App\Listeners\JobProcessingListener;
use App\Listeners\MessageSendingListener;
use App\Services\HealthCheckService\Events\HealthCheckPerformed;
use App\Services\HealthCheckService\Listeners\HealthCheckListener;
use App\Services\HealthCheckService\Listeners\HealthIssueListener;
use App\Services\HealthCheckService\Events\UnhealthyTargets;
use Illuminate\Auth\Events\Logout;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Queue\Events\JobProcessing;
use Modules\Advertiser\Listeners\HaapiAccountRetrievedListener;
use Modules\Auth\Events\UserLogin;
use Modules\Notification\Listeners\MessageSentListener;
use Modules\Haapi\Events\HaapiAccountRetrieved;
use Modules\User\Listeners\Activities\StoreLoginListener;
use Modules\User\Listeners\Activities\StoreLogoutListener;
use Modules\Campaign\Events\CampaignStepUpdated;
use Modules\Campaign\Listeners\CampaignCanceledListener;
use Modules\Campaign\Listeners\CampaignCreatedListener;
use Modules\Campaign\Listeners\CampaignUpdatedListener;
use Modules\Creative\Listeners\CreativeUpdatedListener;
use Modules\Daapi\Events\User\UserApproved;
use Modules\Daapi\Events\Account\AccountUpdated;
use Modules\Daapi\Events\Campaign\CampaignCanceled;
use Modules\Daapi\Events\Campaign\CampaignCreated;
use Modules\Daapi\Events\Campaign\CampaignUpdated;
use Modules\Daapi\Events\Creative\CreativeUpdated;
use Modules\Daapi\Events\Order\OrderNotificationEvent;
use Modules\Daapi\Events\StatusNotificationEvent;
use Modules\Daapi\Listeners\Notifications\OrderNotificationListener;
use Modules\Daapi\Listeners\Notifications\StatusNotificationListener;
use Modules\Daapi\Listeners\UserLogoutListener;
use Modules\Notification\Listeners\NotifiedListener;
use Modules\Promocode\Listeners\DiscardPromocodeListener;
use Modules\User\Listeners\CompanyUpdateApprovedListener;
use Modules\User\Listeners\UserApprovedListener;
use Modules\Advertiser\Events\AdvertiserDeactivated;
use Modules\Advertiser\Events\AdvertiserActivated;
use Modules\Advertiser\Listeners\AdvertiserActivatedListener;
use Modules\Advertiser\Listeners\AdvertiserDeactivatedListener;
use Modules\User\Listeners\UserLoginListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserApproved::class            => [
            UserApprovedListener::class,
        ],
        UserLogin::class               => [
            StoreLoginListener::class,
            UserLoginListener::class,
        ],
        Logout::class                  => [
            StoreLogoutListener::class,
            UserLogoutListener::class,
        ],
        AccountUpdated::class          => [
            CompanyUpdateApprovedListener::class,
        ],
        HaapiAccountRetrieved::class   => [
            HaapiAccountRetrievedListener::class,
        ],
        AdvertiserDeactivated::class   => [
            AdvertiserDeactivatedListener::class,
        ],
        AdvertiserActivated::class     => [
            AdvertiserActivatedListener::class,
        ],
        CampaignCreated::class         => [
            CampaignCreatedListener::class,
        ],
        CampaignUpdated::class         => [
            CampaignUpdatedListener::class,
        ],
        CampaignCanceled::class        => [
            CampaignCanceledListener::class,
        ],
        CreativeUpdated::class         => [
            CreativeUpdatedListener::class,
        ],
        OrderNotificationEvent::class  => [
            OrderNotificationListener::class,
        ],
        StatusNotificationEvent::class => [
            StatusNotificationListener::class,
        ],
        NotificationSent::class        => [
            NotifiedListener::class,
        ],
        CampaignStepUpdated::class     => [
            DiscardPromocodeListener::class,
        ],
        UnhealthyTargets::class        => [
            HealthIssueListener::class,
        ],
        HealthCheckPerformed::class    => [
            HealthCheckListener::class,
        ],
        JobProcessing::class           => [
            JobProcessingListener::class,
        ],
        MessageSending::class          => [
            MessageSendingListener::class,
        ],
        MessageSent::class             => [
            MessageSentListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();

        //
    }
}
