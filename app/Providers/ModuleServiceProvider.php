<?php

namespace App\Providers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use ReflectionClass;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;

/**
 * @property string $routePrefix
 */
abstract class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Determine whether plural prefix will be used in url
     *
     * @var bool
     */
    protected $pluralRoutePrefix = true;

    /**
     * Determine whether api routes will be loaded
     *
     * @var bool
     */
    protected $loadApiRoutes = true;

    /**
     * List of all available policies.
     *
     * @var array
     */
    protected $policies = [];

    /**
     * List of all view composers.
     *
     * @var array
     */
    protected $composers = [];

    /**
     * Register any user services.
     *
     * @return void
     * @throws \ReflectionException
     */
    public function boot(): void
    {
        $this->loadViews();
        $this->loadMigrations();
        $this->loadPolicies();
        $this->loadTranslations();
    }

    /**
     * Get directory
     *
     * @return string
     * @throws \ReflectionException
     */
    protected function getDir(): string
    {
        $reflector = new ReflectionClass(get_class($this));

        return dirname($reflector->getFileName());
    }

    /**
     * Return plural prefix for url
     *
     * @return string
     */
    protected function getPluralPrefix(): string
    {
        return $this->pluralRoutePrefix ? Str::plural($this->getPrefix()) : $this->getPrefix();
    }

    /**
     * Get module prefix
     *
     * @return string
     */
    abstract protected function getPrefix(): string;

    /**
     * Load routes for the module.
     */
    protected function loadRoutes(): void
    {
        $this->app->booted(function () {
            if ($this->app->routesAreCached()) {
                return;
            }

            $prefix = $this->getPrefix();
            $pluralPrefix = $this->getPluralPrefix();
            $namespace = ucfirst($prefix);

            $this->loadWebRoutes($pluralPrefix, $namespace);

            if ($this->loadApiRoutes) {
                $this->loadApiRoutes($pluralPrefix, $namespace);
            }

            $this->app['router']->getRoutes()->refreshNameLookups();
            $this->app['router']->getRoutes()->refreshActionLookups();
        });
    }

    /**
     * @return string
     */
    protected function getRoutePrefix(): string
    {
        return property_exists($this, 'routePrefix') ? $this->routePrefix : $this->getPluralPrefix();
    }

    /**
     * Load web routes.
     *
     * @param string $pluralPrefix
     * @param string $namespace
     *
     * @throws \ReflectionException
     */
    protected function loadWebRoutes(string $pluralPrefix, string $namespace): void
    {
        $path = $this->getDir() . '/../routes/web.php';
        if (!file_exists($path)) {
            return;
        }

        Route::middleware('web')
            ->prefix($this->getRoutePrefix())
            ->name("{$pluralPrefix}.")
            ->namespace("Modules\\{$namespace}\\Http\\Controllers")
            ->group($path);
    }

    /**
     * @param string $pluralPrefix
     * @param string $namespace
     *
     * @throws \ReflectionException
     */
    protected function loadApiRoutes(string $pluralPrefix, string $namespace): void
    {
        $path = $this->getDir() . '/../routes/api.php';
        if (!file_exists($path)) {
            return;
        }

        Route::middleware('api')
            ->prefix('api/' . $this->getRoutePrefix())
            ->name("api.{$pluralPrefix}.")
            ->namespace("Modules\\{$namespace}\\Http\\Controllers\\Api")
            ->group($path);
    }

    /**
     * Load translations for the module
     *
     * @throws \ReflectionException
     */
    public function loadTranslations(): void
    {
        $path = $this->getDir() . '/../resources/lang';

        if (file_exists($path)) {
            $this->loadTranslationsFrom($path, $this->getPrefix());
        }
    }

    /**
     * Load views for the module.
     *
     * @throws \ReflectionException
     */
    protected function loadViews(): void
    {
        $this->loadViewsFrom($this->getDir() . '/../resources/views', $this->getPrefix());
    }

    /**
     * Load migrations for the module.
     *
     * @throws \ReflectionException
     */
    protected function loadMigrations(): void
    {
        $this->loadMigrationsFrom($this->getDir() . '/../database/migrations');
    }

    /**
     * Load configs for the modules.
     *
     * @param array $files
     *
     * @throws \ReflectionException
     */
    protected function loadConfigs(array $files = []): void
    {
        foreach ($files as $alias => $file) {
            $this->loadConfigFile($file, is_string($alias) ? $alias : $file);
        }
    }

    /**
     * Load config file.
     *
     * @param string $file
     * @param string $name
     *
     * @throws \ReflectionException
     */
    protected function loadConfigFile(string $file, string $name): void
    {
        $this->mergeConfigFrom($this->getDir() . '/../config/' . $file . '.php', $name);
    }

    /**
     * Register policies for the module.
     */
    protected function loadPolicies(): void
    {
        foreach ($this->policies as $name => $policy) {
            $methods = get_class_methods($policy);

            Gate::resource($name, $policy, array_combine($methods, $methods));
        }
    }

    /**
     * Load view composers for the module.
     */
    protected function loadComposers(): void
    {
        foreach ($this->composers as $composer) {
            view()->composer($composer::$views, $composer);
        }
    }


    /**
     * Get module entities.
     *
     * @return array
     * @throws \Exception
     */
    public function getEntities(): array
    {
        $module = [
            'name'  => $this->getPrefix(),
            'files' => [],
        ];

        $entityDirectory = $this->getDir() . '/../entities';
        if (File::exists($entityDirectory)) {
            $module['files'] = File::files($entityDirectory);
        }

        return $module;
    }
}
