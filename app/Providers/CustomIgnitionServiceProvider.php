<?php

namespace App\Providers;

use App\View\Engines\CompilerEngine;
use Facade\Ignition\IgnitionServiceProvider;
use Facade\Ignition\Views\Engines\PhpEngine;

class CustomIgnitionServiceProvider extends IgnitionServiceProvider
{
    /**
     * Change register view engines.
     * @return IgnitionServiceProvider
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function registerViewEngines(): IgnitionServiceProvider
    {
        if (! $this->hasCustomViewEnginesRegistered()) {
            return $this;
        }

        $this->app->make('view.engine.resolver')->register('php', function () {
            return new PhpEngine();
        });

        $this->app->make('view.engine.resolver')->register('blade', function () {
            return new CompilerEngine($this->app['blade.compiler']);
        });

        return $this;
    }
}
