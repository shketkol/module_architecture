<?php

namespace App\Providers;

use \App\Helpers\Mix;
use App\View\ViewServiceProvider;
use Illuminate\Foundation\Mix as VendorMix;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use Laravel\Passport\Passport;
use Modules\Demo\DemoServiceProvider;
use Modules\Demo\View\DemoViewServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        if ($this->app->isLocal()) {
            $this->app->instance(VendorMix::class, new Mix());
        }

        (new ViewServiceProvider($this->app))->register();

        if (config('general.demo.enabled')) {
            (new DemoServiceProvider($this->app))->register();
            (new DemoViewServiceProvider($this->app))->register();
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->forceHttps();
        Passport::hashClientSecrets();

        $mainPath = database_path('migrations');
        $directories = glob($mainPath . '/*', GLOB_ONLYDIR);
        $paths = array_merge([$mainPath], $directories);

        $this->loadMigrationsFrom($paths);
    }

    /**
     * Force https schema.
     *
     * @return void
     */
    private function forceHttps(): void
    {
        if (config('app.schema') === 'https') {
            URL::forceScheme('https');
        }
    }
}
