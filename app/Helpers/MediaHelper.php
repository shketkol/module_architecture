<?php

namespace App\Helpers;

class MediaHelper
{
    /**
     * Form image file name based on variant
     *
     * @param string $name
     * @param string $variant
     * @return string
     */
    public static function imgVariant(string $name, string $variant): string
    {
        if (strpos($name, '.') === false) {
            return $name . '_' . $variant;
        }

        $nameArray = explode('.', $name);
        $extension = array_pop($nameArray);

        return implode('.', $nameArray) . '_' . $variant . '.' . $extension;
    }

    /**
     * Cut extension from filename
     *
     * @param string $filename
     * @return string
     */
    public static function cutExtension(string $filename): string
    {
        $filenameArray = explode('.', $filename);

        if (!is_array($filenameArray)) {
            return $filename;
        }

        array_pop($filenameArray);
        return implode('.', $filenameArray);
    }
}
