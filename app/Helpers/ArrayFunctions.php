<?php

/**
 * Array map but supports keys
 * @param callable $callable
 * @param array $array
 * @return array
 */
if (! function_exists('array_map_keys')) {
    function array_map_keys(callable $callable, array $array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[$key] = $callable($value, $key);
        }

        return $result;
    }
}

/**
 * Port of ES6 Array.prototype.some function
 * @param callable $callback
 * @param $array
 * @return mixed
 */
if (! function_exists('array_some')) {
    function array_some(callable $callback, $array)
    {
        return array_reduce($array, function ($accumulator, $current) use ($callback) {
            return $accumulator || $callback($current);
        }, false);
    }
}

/**
 * Port of ES6 Array.prototype.every function
 * @param callable $callback
 * @param $array
 * @return mixed
 */
if (! function_exists('array_every')) {
    function array_every(callable $callback, $array)
    {
        return array_reduce($array, function ($accumulator, $current) use ($callback) {
            return $accumulator && $callback($current);
        }, true);
    }
}
