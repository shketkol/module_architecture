<?php

namespace App\Helpers;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Arr;

class HtmlHelper
{
    /**
     * @var array
     */
    protected $templates = [
        'email'  => '<a href="mailto:%s">%s</a>',
        'anchor' => '<a href="%s">%s</a>',
    ];

    /**
     * @return string
     */
    public function getSupportEmailLink(): string
    {
        $email = config('general.company_support_email');
        $template = Arr::get($this->templates, 'email');

        return sprintf($template, $email, e($email));
    }

    /**
     * @return string
     */
    public function getCreativeGalleryLink(): string
    {
        /** @var UrlGenerator $generator */
        $generator = app(UrlGenerator::class);
        $url = $generator->route('creatives.index');

        return $this->createAnchorElement($url, ['title' => __('creative::content.creative_gallery')]);
    }

    /**
     * @param string|null $url
     * @param array       $params
     *
     * @return string
     */
    public function createAnchorElement(?string $url = null, array $params = []): string
    {
        if (is_null($url)) {
            $url = '#';
        }

        $title = Arr::get($params, 'title', $url);
        $template = Arr::get($this->templates, 'anchor');

        return sprintf($template, $url, e($title));
    }

    /**
     * @param string|null $html
     *
     * @return string
     */
    public function purify(?string $html): string
    {
        if (is_null($html)) {
            return '';
        }

        $config = \HTMLPurifier_Config::createDefault();
        $config->loadArray(config('htmlpurifier'));
        $config->finalize();
        $purifier = new \HTMLPurifier($config);

        return $purifier->purify($html);
    }

    /**
     * @param string $tag
     * @return string
     */
    public static function getMailTagCommonStyles(string $tag): string
    {
        $stylesArray = config('mail.common_styles.' . $tag);
        $stylesString = '';

        if (!is_array($stylesArray)) {
            return $stylesString;
        }

        foreach ($stylesArray as $property => $value) {
            $stylesString .= $property . ': ' . $value . '; ';
        }

        return $stylesString;
    }
}
