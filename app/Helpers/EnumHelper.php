<?php

namespace App\Helpers;

use Illuminate\Support\Arr;
use Modules\Payment\Models\TransactionStatus;

trait EnumHelper
{
    /**
     * @return array
     */
    public function getAccountTypes(): array
    {
        return config('daapi.validation.account_types', []);
    }

    /**
     * @return array
     */
    public function getUserStatuses(): array
    {
        return config('daapi.validation.user_statuses', []);
    }

    /**
     * @return array
     */
    public function getUserTypes(): array
    {
        return config('daapi.validation.user_types', []);
    }

    /**
     * @return array
     */
    public function getCostModels(): array
    {
        return config('daapi.validation.cost_models', []);
    }

    /**
     * @return array
     */
    public function getQuantityTypes(): array
    {
        return config('daapi.validation.quantity_types', []);
    }

    /**
     * @return array
     */
    public function getPaymentStatuses(): array
    {
        return config('daapi.validation.payment_statuses', []);
    }

    /**
     * @return array
     */
    public function getAccountStatuses(): array
    {
        return $this->transformKeys(config('daapi.status_mapping.user', []));
    }

    /**
     * @return array
     */
    public function getCampaignStatuses(): array
    {
        return $this->transformKeys(config('daapi.status_mapping.campaign', []));
    }

    /**
     * @return array
     */
    public function getCreativeStatuses(): array
    {
        return $this->transformKeys(config('daapi.status_mapping.creative', []));
    }

    /**
     * @param array $enums
     * @return array
     */
    private function transformKeys(array $enums): array
    {
        $enums = array_change_key_case($enums, CASE_UPPER);

        return array_keys($enums);
    }
}
