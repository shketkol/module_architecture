<?php

namespace App\Helpers;

class EnvironmentHelper
{
    /**
     * Prods env codes
     */
    public const PRODS = ['prod', 'production'];

    /**
     * Prods env codes
     */
    public const CLOSED = ['prod', 'production', 'demo'];

    /**
     * Test env codes
     */
    public const OPEN = ['stage', 'local', 'hulu-stage'];

    /**
     * Show own landing mock envs
     */
    public const LANDING = ['stage', 'local', 'hulu-stage', 'demo'];

    /**
     * Local env codes
     */
    public const LOCAL = ['local'];

    /**
     * Testing env codes (CI and PHPUnit)
     */
    public const TESTING = ['testing'];

    /**
     * Check if it`s prod environment
     *
     * @return boolean
     */
    public static function isProdEnv(): bool
    {
        return in_array(env('APP_ENV'), self::PRODS);
    }

    /**
     * Check if it`s closed environment
     *
     * @return boolean
     */
    public static function isClosedEnv(): bool
    {
        return in_array(env('APP_ENV'), self::CLOSED);
    }

    /**
     * Check if it`s test environment
     *
     * @return boolean
     */
    public static function isTestEnv(): bool
    {
        return in_array(env('APP_ENV'), self::TESTING);
    }

    /**
     * Check if it`s open environment
     *
     * @return boolean
     */
    public static function isOpenEnv(): bool
    {
        return in_array(env('APP_ENV'), self::OPEN);
    }

    /**
     * Check if it`s local environment
     *
     * @return boolean
     */
    public static function isLocalEnv(): bool
    {
        return in_array(env('APP_ENV'), self::LOCAL);
    }

    /**
     * Check if landing mock should be used instead of redirect
     *
     * @return boolean
     */
    public static function isMockedLandingEnv(): bool
    {
        return in_array(env('APP_ENV'), self::LANDING);
    }
}
