<?php

namespace App\Helpers;

use Illuminate\Support\Arr;

trait CsvHelper
{
    /**
     * Parse csv file
     *
     * @param string $content
     *
     * @return array
     */
    public function parse(string $content): array
    {
        $parsedContent = preg_split(config('locations.csv_regexp'), $content);

        return Arr::where(array_map(function ($item) {
            return trim($item);
        }, $parsedContent), function ($value) {
            return !empty($value);
        });
    }

    /**
     * @param string $string
     *
     * @return bool
     */
    public function isValidUTF8(string $string): bool
    {
        return mb_check_encoding($string, 'UTF-8');
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public function convertToUTF8(string $string): string
    {
        return utf8_encode($string);
    }
}
