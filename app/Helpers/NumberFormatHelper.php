<?php

namespace App\Helpers;

class NumberFormatHelper
{
    /**
     * @param float $amount
     * @return string
     */
    public static function floatCurrency(float $amount): string
    {
        return '$' . number_format($amount, 2, '.', ',');
    }

    /**
     * @param int $number
     * @return string
     */
    public static function standart(int $number): string
    {
        return number_format($number, 0, '.', ',');
    }
}
