<?php

namespace App\Helpers;

use Psr\Log\LoggerInterface;

class Json
{
    /**
     * @param string $string
     *
     * @return array
     */
    public static function decode(string $string): array
    {
        $data = json_decode($string, true);

        if (json_last_error() === JSON_ERROR_NONE) {
            return $data;
        }

        $message = 'Error while decoding to JSON: ' . json_last_error_msg();

        /** @var LoggerInterface $logger */
        $logger = app(LoggerInterface::class);
        $logger->warning($message, [
            'string' => $string,
        ]);

        throw new \RuntimeException($message);
    }

    /**
     * @param mixed $data
     *
     * @return string
     */
    public static function encode($data): string
    {
        return json_encode($data);
    }

    /**
     * @param string $string
     *
     * @return bool
     */
    public static function isJson(string $string): bool
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}
