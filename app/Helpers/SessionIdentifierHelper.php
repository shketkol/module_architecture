<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use \Throwable;

class SessionIdentifierHelper
{
    use CurrentUser;

    /**
     * Hash algorithm
     */
    private const HASH_ALGORITHM = 'sha256';

    /**
     * To determine if Session Identifier is already computing
     * Needed to prevent infinite loop
     *
     * @var bool
     */
    private static $isProcessing;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        $user = $this->getCurrentUserCached();

        if (self::$isProcessing || App::runningUnitTests() || !$user) {
            return '';
        }

        try {
            self::$isProcessing = true;

            $identifier = hash(self::HASH_ALGORITHM, $user->email);
            $sessionIdentifier = $this->getSessionIdentifier($user->id);

            self::$isProcessing = false;
        } catch (Throwable $exception) {
            self::$isProcessing = false;

            return '';
        }

        return "{$identifier}.{$sessionIdentifier}";
    }

    /**
     * @param int $userId
     */
    public function setSessionIdentifier(int $userId)
    {
        Cache::put(
            $this->getKey($userId),
            Str::uuid(),
            config('session.lifetime') * Carbon::SECONDS_PER_MINUTE
        );
    }

    /**
     * @param int $userId
     *
     * @return string
     */
    private function getSessionIdentifier(int $userId): string
    {
        return Cache::get($this->getKey($userId), '');
    }

    /**
     * Generate session identifier key
     *
     * @param  $key
     * @return string
     */
    protected function getKey($key): string
    {
        return 'session_identifier_' . md5($key);
    }
}
