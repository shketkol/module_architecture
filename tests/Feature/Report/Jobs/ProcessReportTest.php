<?php

namespace Tests\Feature\Report\Jobs;

use Modules\Report\Actions\GenerateReportAction;
use Modules\Report\Jobs\ProcessReport;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminSignInMock;

class ProcessReportTest extends TestCase
{
    use AdminSignInMock;

    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();
        parent::setUp();
        $this->mockAdminSignInActionSuccess();
    }

    /**
     * @dataProvider noLiveCampaignsDataProvider
     *
     * @param array $reportStatuses
     *
     * @return void
     * @throws \App\Exceptions\BaseException
     * @throws \Modules\Daapi\Exceptions\CanNotApplyStatusException
     * @throws \SM\SMException
     */
    public function testNoLiveCampaignsReport(array $reportStatuses): void
    {
        /** @var Report $report */
        $report = Report::factory()
            ->scheduled()
            ->override([
                'status_id' => $reportStatuses['initial_status_id'],
            ])
            ->create();

        $processReport = new ProcessReport($report);
        $processReport->handle(app(GenerateReportAction::class));

        $this->assertDatabaseHas('reports', [
            'id'        => $report->getKey(),
            'status_id' => $reportStatuses['processed_status_id'],
        ]);
    }

    /**
     * @return \Generator
     */
    public function noLiveCampaignsDataProvider(): \Generator
    {
        yield [
            [
                'initial_status_id'   => ReportStatus::ID_DRAFT,
                'processed_status_id' => ReportStatus::ID_DRAFT,
            ],
            [
                'initial_status_id'   => ReportStatus::ID_FAILED,
                'processed_status_id' => ReportStatus::ID_DRAFT,
            ],
        ];
    }
}
