<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminCampaignSearchMock;
use Tests\Traits\HaapiMocks\UserGetMock;

class StoreScheduleReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport,
        UserGetMock,
        AdminCampaignSearchMock;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var User
     */
    private $advertiser;


    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $this->advertiser = $this->createTestAdvertiser();
        $this->actingAs($this->advertiser);

        $this->mockUserGetActionSuccess($this->advertiser);

        $admin = $this->createTestAdmin();
        $this->mockAdminCampaignSearchActionSuccess(new AdminCampaignSearchParam([
            'accountId' => $this->advertiser->external_id,
        ]), $admin->id);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test store schedule report.
     */
    public function testStoreScheduleReport(): void
    {
        $response = $this->post(route('api.reports.schedule.store'), [
            'day' => 3,
            'emails' => ['test-adver@mail.com'],
            'frequency' => 2,
            'name' => 'test'
        ]);

        $response->assertOk();
        $report = $this->repository->where('user_id', $this->advertiser->id)->latest('id')->first();
        $this->assertEquals('test', $report->name);
    }
}
