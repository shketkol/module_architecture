<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Report\Models\ReportType;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\TestCase;

class ShowDownloadReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var User
     */
    private $advertiser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $this->advertiser = $this->createTestAdvertiser();
        $this->actingAs($this->advertiser);

        $this->createTestReport([
            'user_id' => $this->advertiser->id,
            'type_id' => ReportType::ID_DOWNLOAD
        ]);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test show download report.
     */
    public function testShowDownloadReport(): void
    {
        $report = $this->repository->where(['user_id' => $this->advertiser->id])->first();

        $response = $this->get(route('api.reports.download.show', ['report' => $report->id]));

        $response->assertOk();
    }
}
