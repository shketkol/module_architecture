<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Models\ReportType;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminCampaignSearchMock;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\UserGetMock;

class UpdateDownloadReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport,
        UserGetMock,
        AdminSignInMock,
        AdminCampaignSearchMock;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var Report
     */
    private $report;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $advertiser = $this->createTestAdvertiser();
        $this->actingAs($advertiser);

        $this->mockUserGetActionSuccess($advertiser);

        $admin = $this->createTestAdmin();
        $this->mockAdminSignInActionSuccess();
        $this->mockAdminCampaignSearchActionSuccess(new AdminCampaignSearchParam([
            'accountId' => $advertiser->external_id,
        ]), $admin->id);

        $this->report = $this->createTestReport([
            'user_id'   => $advertiser,
            'type_id'   => ReportType::ID_DOWNLOAD,
            'status_id' => ReportStatus::ID_DRAFT,
        ]);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test update download report.
     */
    public function testUpdateDownloadReport(): void
    {
        $response = $this->patch(route('api.reports.download.update', ['report' => $this->report->id]), [
            'dateRange' => [
                'end'   => '06/10/2020',
                'start' => '06/08/2020',
            ],
            'name'      => 'test',
        ]);

        $response->assertOk();
        $report = $this->repository->where('id', $this->report->id)->first();
        $this->assertEquals('test', $report->name);
    }
}
