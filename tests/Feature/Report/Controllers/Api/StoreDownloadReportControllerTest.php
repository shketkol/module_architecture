<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminCampaignSearchMock;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\UserGetMock;

class StoreDownloadReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport,
        UserGetMock,
        AdminSignInMock,
        AdminCampaignSearchMock;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var User
     */
    private $advertiser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $this->advertiser = $this->createTestAdvertiser();
        $this->actingAs($this->advertiser);

        $this->mockUserGetActionSuccess($this->advertiser);

        $admin = $this->createTestAdmin();
        $this->mockAdminSignInActionSuccess();
        $this->mockAdminCampaignSearchActionSuccess(new AdminCampaignSearchParam([
            'accountId' => $this->advertiser->external_id,
        ]), $admin->id);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test store download report.
     */
    public function testStoreDownloadReport(): void
    {
        $response = $this->post(route('api.reports.download.store'), [
            'dateRange' => [
                'end'   => '06/10/2020',
                'start' => '06/08/2020',
            ],
            'name'      => 'test',
        ]);

        $response->assertOk();
        $report = $this->repository->where('user_id', $this->advertiser->id)->latest('id')->first();
        $this->assertEquals('test', $report->name);
    }
}
