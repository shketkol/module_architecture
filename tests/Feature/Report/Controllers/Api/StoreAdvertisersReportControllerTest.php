<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Haapi\DataTransferObjects\Account\AdminAccountSearchParam;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AccountSearchMock;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\UserGetMock;

class StoreAdvertisersReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport,
        UserGetMock,
        AdminSignInMock,
        AccountSearchMock;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var User
     */
    private $admin;


    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $this->admin = $this->createTestAdmin();
        $this->actingAs($this->admin);

        $this->mockAdminSignInActionSuccess();
        $this->mockUserGetActionSuccess($this->admin);
        $this->mockAccountsSearchActionSuccess(new AdminAccountSearchParam([]), $this->admin->id);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test store advertisers report.
     */
    public function testStoreAdvertisersReport(): void
    {
        $response = $this->post(route('api.reports.advertisers.store'), [
            'dateRange' => [],
            'name'      => 'test',
        ]);

        $response->assertOk();
        $report = $this->repository->where('user_id', $this->admin->id)->latest('id')->first();
        $this->assertEquals('test', $report->name);
    }
}
