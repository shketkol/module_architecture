<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Models\ReportType;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class ResumeReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var Report
     */
    private $report;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $advertiser = $this->createTestAdvertiser();
        $this->actingAs($advertiser);

        $this->report = $this->createTestReport([
            'name' => 'resume',
            'user_id' => $advertiser->id,
            'status_id' => ReportStatus::ID_PAUSED,
            'type_id' => ReportType::ID_SCHEDULED
        ]);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test resume report.
     */
    public function testResumeReport(): void
    {
        $report = $this->repository->where(['id' => $this->report->id])->first();

        $response = $this->patch(route('api.reports.resume', ['report' => $report->id]));

        $response->assertOk();
    }
}
