<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Haapi\DataTransferObjects\Account\AdminAccountSearchParam;
use Modules\Report\Models\Report;
use Modules\Report\Models\ReportStatus;
use Modules\Report\Models\ReportType;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AccountSearchMock;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\UserGetMock;

class UpdateAdvertisersReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport,
        UserGetMock,
        AdminSignInMock,
        AccountSearchMock;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var Report
     */
    private $report;


    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $admin = $this->createTestAdmin();
        $this->actingAs($admin);

        $this->mockAdminSignInActionSuccess();
        $this->mockUserGetActionSuccess($admin);
        $this->mockAccountsSearchActionSuccess(new AdminAccountSearchParam([]), $admin->id);

        $this->report = $this->createTestReport([
            'user_id' => $admin,
            'type_id' => ReportType::ID_ADVERTISERS,
            'status_id' => ReportStatus::ID_DRAFT,
        ]);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test update advertisers report.
     */
    public function testUpdateAdvertisersReport(): void
    {
        $response = $this->patch(route('api.reports.advertisers.update', ['report' => $this->report->id]), [
            'dateRange' => [],
            'name' => 'test'
        ]);

        $response->assertOk();
        $report = $this->repository->where('id', $this->report->id)->first();
        $this->assertEquals('test', $report->name);
    }
}
