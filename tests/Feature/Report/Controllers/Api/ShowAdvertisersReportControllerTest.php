<?php

namespace Tests\Feature\Report\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Report\Models\ReportType;
use Modules\Report\Models\Traits\CreateReport;
use Modules\Report\Repositories\ReportRepository;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\TestCase;

class ShowAdvertisersReportControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateReport;

    /**
     * @var ReportRepository
     */
    private $repository;

    /**
     * @var User
     */
    private $admin;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        //todo HULU-3278
        $this->markTestSkipped();

        parent::setUp();

        $this->admin = $this->createTestAdmin();
        $this->actingAs($this->admin, 'admin');

        $this->createTestReport([
            'user_id' => $this->admin->id,
            'type_id' => ReportType::ID_ADVERTISERS
        ]);

        $this->repository = app(ReportRepository::class);
    }

    /**
     * Test show advertisers report.
     */
    public function testShowAdvertisersReport(): void
    {
        $report = $this->repository->where(['user_id' => $this->admin->id])->first();

        $response = $this->get(route('api.reports.advertisers.show', ['report' => $report->id]));

        $response->assertOk();
    }
}
