<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Carbon\CarbonImmutable;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\Payment\Models\PaymentMethod;
use Modules\Targeting\Models\Traits\CreateAgeGroup;
use Modules\Targeting\Models\Traits\CreateGenre;
use Modules\Targeting\Models\Traits\CreateLocation;
use Modules\Targeting\Models\Traits\ScoutIndex;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Tests\TestCase;
use Modules\Campaign\Models\Traits\CreateCampaign;

class OrderCampaignControllerTest extends TestCase
{
    use DatabaseTransactions,
        ScoutIndex,
        CreateUser,
        CreateCampaign,
        CreateCreative,
        CreateGenre,
        CreateAgeGroup,
        CreateLocation;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test Order Campaign.
     */
    public function testOrderCampaign(): void
    {
        $this->markTestSkipped('Skipped until mock api');
        $advertiser = $this->createTestAdvertiser();
        $paymentMethod = PaymentMethod::factory()->create([
            'user_id' => $advertiser->getKey(),
        ]);
        $campaign = $this->createCampaignWithRelations($advertiser);

        $this->actingAs($advertiser);

        $response = $this->json(
            Request::METHOD_POST,
            route('api.campaigns.order', ['campaign' => $campaign->id]),
            ['paymentId' => $paymentMethod->id]
        );

        $response->assertOk();
    }

    /**
     * Create campaign with all required relations.
     *
     * @param User $advertiser
     *
     * @return Campaign
     */
    private function createCampaignWithRelations(User $advertiser): Campaign
    {
        $campaign = $this->createTestCampaign([
            'user_id'    => $advertiser->getKey(),
            'status_id'  => CampaignStatus::ID_DRAFT,
            'date_start' => CarbonImmutable::now()->add(3, ' day'),
        ]);

        // creatives
        $campaign->creatives()->attach(
            $this->createTestCreative(['user_id' => $advertiser->getKey()])
        );

        // targeting genres
        $genres = $this->createTestTargetingGenre([], 5);
        $payload = $this->createTargetingGenresPayload($genres);
        $this->syncCampaignTargetingGenre($campaign, $payload);

        // targeting locations
        $locations = $this->createTestTargetingLocation([], 5);
        $payload = $this->createTargetingLocationsPayload($locations);
        $this->syncCampaignTargetingLocations($campaign, $payload);

        // targeting age groups and genders
        $ageGroups = $this->createTestTargetingAgeGroup([], 5);
        $payload = $this->createTargetingAgeGroupsPayload($ageGroups);
        $this->syncCampaignTargetingAgeGroups($campaign, $payload);

        return $campaign;
    }
}
