<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use App\Models\Timezone;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Targeting\Models\Traits\CreateAgeGroup;
use Modules\Targeting\Models\Traits\CreateDeviceGroup;
use Database\Seeders\TimezoneTableSeeder;
use Modules\User\Models\Traits\CreateUser;
use Modules\Targeting\Models\Traits\ScoutIndex;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;

class ValidateCampaignControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        ScoutIndex,
        CreateCampaign,
        CreateDeviceGroup,
        CreateAgeGroup;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(TimezoneTableSeeder::class);
    }

    /**
     * Test update campaign name.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     */
    public function testUpdateCampaignName(array $data): void
    {
        $name = Arr::get($data, 'name');
        $expectedStatusCode = Arr::get($data, 'expected_result.code');

        $advertiser = $this->createTestAdvertiser();
        $campaign = $this->createTestCampaign([
            'user_id'     => $advertiser->id,
            'name'        => $name,
            'status_id'   => CampaignStatus::ID_DRAFT,
            'timezone_id' => Timezone::ID_EASTERN,
        ]);

        $ageGroups = $this->createTestTargetingAgeGroup([], 1);
        $payload = $this->createTargetingAgeGroupsPayload($ageGroups);
        $genderPayload = $this->createTargetingGendersPayload($ageGroups);
        $deviceGroups = $this->createTestDeviceGroup([], 1);
        $deviceGroupsPayload = $this->createDeviceGroupPayload($deviceGroups);

        $this->syncCampaignTargetingAgeGroups($campaign, $payload);
        $this->syncCampaignDeviceGroup($campaign, $deviceGroupsPayload);
        $this->syncTargetingGenders($campaign, $genderPayload);

        $this->actingAs($advertiser);

        $response = $this->json(Request::METHOD_GET, route('api.campaigns.validate', ['campaign' => $campaign->id]));

        $response->assertStatus($expectedStatusCode);
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // english
        yield [
            [
                'name'            => 'test campaign',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // spanish
        yield [
            [
                'name'            => 'campaña de prueba',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // albanian
        yield [
            [
                'name'            => 'fushatë provë',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // amharic
        yield [
            [
                'name'            => 'የሙከራ ዘመቻ።',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // arabic
        yield [
            [
                'name'            => 'حملة اختبار',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // armenian
        yield [
            [
                'name'            => 'թեստային արշավ',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // belorussian
        yield [
            [
                'name'            => 'тэставая кампанія',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // chinese
        yield [
            [
                'name'            => '测试活动',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // czech
        yield [
            [
                'name'            => 'testovací kampaň',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // georgian
        yield [
            [
                'name'            => 'სატესტო კამპანია',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // greek
        yield [
            [
                'name'            => 'δοκιμή καμπάνιας',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // gujarati
        yield [
            [
                'name'            => 'પરીક્ષણ ઝુંબેશ',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // hawaiian
        yield [
            [
                'name'            => 'hōʻike hoʻāʻo',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // hebrew
        yield [
            [
                'name'            => 'קמפיין מבחן',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // hindi
        yield [
            [
                'name'            => 'परीक्षण अभियान',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // icelandic
        yield [
            [
                'name'            => 'próf herferð',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // igbo
        yield [
            [
                'name'            => 'mkpọsa nnwale',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // japanese
        yield [
            [
                'name'            => 'テストキャンペーン',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // kannada
        yield [
            [
                'name'            => 'ಪರೀಕ್ಷಾ ಅಭಿಯಾನ',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // kazakh
        yield [
            [
                'name'            => 'сынақ науқаны',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // khmer
        yield [
            [
                'name'            => 'យុទ្ធនាការសាកល្បង។',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // korean
        yield [
            [
                'name'            => '테스트 캠페인',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // kyrgyz
        yield [
            [
                'name'            => 'сыноо өнөктүгү',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // lao
        yield [
            [
                'name'            => 'ຂະບວນການທົດສອບ',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // latvian
        yield [
            [
                'name'            => 'testa kampaņa',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // macedonian
        yield [
            [
                'name'            => 'тест кампања',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // malayalam
        yield [
            [
                'name'            => 'പരീക്ഷണ കാമ്പെയ്‌ൻ',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // maori
        yield [
            [
                'name'            => 'whakamātautau whakamatautau',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // marathi
        yield [
            [
                'name'            => 'चाचणी मोहीम',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // mayanmar
        yield [
            [
                'name'            => 'စမ်းသပ်မှုမဲဆွယ်စည်းရုံးရေး',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // nepali
        yield [
            [
                'name'            => 'परीक्षण अभियान',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // pashto
        yield [
            [
                'name'            => 'د ازمونې کمپاین',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // samoan
        yield [
            [
                'name'            => 'suega faʻataʻitaʻiga',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // serbian
        yield [
            [
                'name'            => 'тест кампања',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // sinhala
        yield [
            [
                'name'            => 'පරීක්ෂණ ව්‍යාපාරය',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // tajik
        yield [
            [
                'name'            => 'маъракаи санҷишӣ',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // tamil
        yield [
            [
                'name'            => 'சோதனை பிரச்சாரம்',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // telugu
        yield [
            [
                'name'            => 'పరీక్ష ప్రచారం',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // thai
        yield [
            [
                'name'            => 'ทดสอบแคมเปญ',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // urdu
        yield [
            [
                'name'            => 'ٹیسٹ مہم۔',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // vietnamese
        yield [
            [
                'name'            => 'chiến dịch thử nghiệm',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // yiddish
        yield [
            [
                'name'            => 'פּרובירן קאמפאניע',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // emoji
        yield [
            [
                'name'            => '⌛🖥⌨️',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // short name
        yield [
            [
                'name'            => 'a',
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];

        // symbols
        yield [
            [
                'name'            => '~!@#$%^&*()_-=+/\\',
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];
    }
}
