<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdvertiserDataTableControllerTest extends TestCase
{
    use DatabaseTransactions, CreateCreative, CreateCampaign, CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test advertiser campaign data table cards
     */
    public function testAdvertiserCards(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $secondAdvertiser = $this->createTestAdvertiser();
        $admin = $this->createTestAdmin();

        $this->actingAs($admin, 'admin');
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.cards', ['table' => 'advertiser-campaigns', 'user_id' => $secondAdvertiser->id])
        );
        $response->assertStatus(Response::HTTP_OK);

        $this->actingAs($advertiser);
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.cards', ['table' => 'advertiser-campaigns', 'user_id' => $secondAdvertiser->id])
        );
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * Test advertiser campaign data table
     */
    public function testAdvertiserTable(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $secondAdvertiser = $this->createTestAdvertiser();
        $admin = $this->createTestAdmin();

        $this->actingAs($admin, 'admin');

        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.cards', ['table' => 'advertiser-campaigns', 'user_id' => $secondAdvertiser->id])
        );
        $response->assertStatus(Response::HTTP_OK);

        $this->actingAs($advertiser);
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'advertiser-campaigns', 'user_id' => $secondAdvertiser->id])
        );
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
