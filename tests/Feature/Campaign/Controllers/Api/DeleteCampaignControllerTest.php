<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Tests\Traits\HaapiMocks\CampaignDraftSaveMock;

class DeleteCampaignControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCampaign,
        CampaignDraftSaveMock;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test Delete Campaign.
     *
     * @dataProvider statusDataProvider
     *
     * @param int $status
     * @param int $code
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testDeleteCampaign(int $status, int $code): void
    {
        $user = $this->createTestAdvertiser();

        $campaign = $this->createTestCampaign([
            'user_id'   => $user->getKey(),
            'status_id' => $status,
        ]);

        $this->mockCampaignDraftSaveActionSuccess($campaign, $user->id);

        $this->actingAs($user);

        $response = $this->json(Request::METHOD_DELETE, route('api.campaigns.delete', ['campaign' => $campaign->id]));

        $response->assertStatus($code);
    }

    /**
     * @return \Generator
     */
    public function statusDataProvider(): \Generator
    {
        yield [
            CampaignStatus::ID_DRAFT,
            Response::HTTP_OK,
        ];

        yield [
            CampaignStatus::ID_PENDING_APPROVAL,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_READY,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_LIVE,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_PAUSED,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_COMPLETED,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_CANCELED,
            Response::HTTP_FORBIDDEN,
        ];
    }
}
