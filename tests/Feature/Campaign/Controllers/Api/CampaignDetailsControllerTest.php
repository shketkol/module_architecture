<?php

namespace Tests\Feature\Campaign\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Targeting\Models\Traits\CreateAgeGroup;
use Modules\User\Models\Traits\CreateUser;
use Modules\Targeting\Models\Traits\CreateGenre;
use Modules\Targeting\Models\Traits\ScoutIndex;
use Modules\User\Models\Role;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Models\Traits\CreateCreative;

class CampaignDetailsControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCampaign,
        CreateCreative,
        CreateGenre,
        CreateAgeGroup,
        ScoutIndex;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test campaign details.
     */
    public function testCampaignDetails(): void
    {
        $user = $this->createTestAdvertiser();

        $campaign = $this->createTestCampaign([
            'user_id' => $user->getKey(),
            'status_id'  => CampaignStatus::ID_DRAFT,
        ]);

        $campaign->creatives()->attach(
            $this->createTestCreative(['user_id' => $user->getKey()])
        );

        $genres = $this->createTestTargetingGenre([], 5);
        $payload = $this->createTargetingGenresPayload($genres);
        $this->syncCampaignTargetingGenre($campaign, $payload);

        $ageGroups = $this->createTestTargetingAgeGroup([], 5);
        $payload = $this->createTargetingAgeGroupsPayload($ageGroups);
        $genderPayload = $this->createTargetingGendersPayload($ageGroups);

        $this->syncCampaignTargetingAgeGroups($campaign, $payload);
        $this->syncTargetingGenders($campaign, $genderPayload);

        // not campaign but uploaded by this user creative
        $this->createTestCreative(['user_id' => $user->getKey()]);

        $this->actingAs($user);

        $response = $this->json(Request::METHOD_GET, route('api.campaigns.details.show', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertNotEmpty($response->json('data'));
        $this->assertNotEmpty($response->json('data.creative'));

        $this->assertFalse($response->json('data.states.can_pause'));
        $this->assertFalse($response->json('data.states.can_cancel'));
        $this->assertFalse($response->json('data.states.can_resume'));
        $this->assertTrue($response->json('data.states.can_edit'));
        $this->assertTrue($response->json('data.states.can_delete'));
    }

    public function testCampaignDetailsAccess(): void
    {
        $admin = $this->createTestAdmin();
        $advertiser = $this->createTestAdvertiser();
        $secondAdvertiser = $this->createTestAdvertiser();

        $campaign = $this->createTestCampaign([
            'user_id' => $advertiser->getKey(),
            'status_id'  => CampaignStatus::ID_LIVE,
        ]);

        $this->actingAs($admin->assignRole(Role::ID_ADMIN_READ_ONLY), 'admin');
        $response = $this->json(Request::METHOD_GET, route(
            'api.campaigns.details.show',
            ['campaign' => $campaign->id]
        ));

        $response->assertStatus(Response::HTTP_OK);

        $this->actingAs($admin->assignRole(Role::ID_ADMIN), 'admin');
        $response = $this->json(Request::METHOD_GET, route(
            'api.campaigns.details.show',
            ['campaign' => $campaign->id]
        ));

        $response->assertStatus(Response::HTTP_OK);

        $this->actingAs($secondAdvertiser);
        $response = $this->json(Request::METHOD_GET, route(
            'api.campaigns.details.show',
            ['campaign' => $campaign->id]
        ));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
