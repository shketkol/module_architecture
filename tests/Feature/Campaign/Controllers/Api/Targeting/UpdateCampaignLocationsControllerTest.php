<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Database\Seeders\Tests\TargetingTypesTableSeeder;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Targeting\Models\Traits\CreateLocation;
use Modules\Targeting\Models\Traits\CreateZipcode;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\Feature\Campaign\Controllers\Api\Targeting\Traits\CreateDraftCampaign;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\CampaignPriceMock;

class UpdateCampaignLocationsControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateDraftCampaign,
        CreateLocation,
        CreateZipcode,
        CampaignPriceMock;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(TargetingTypesTableSeeder::class);
    }

    /**
     * Test Update Campaign Locations.
     */
    public function testUpdateCampaignLocations(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $this->mockCampaignPriceActionSuccess($advertiser->id);
        $campaign = $this->CreateDraftCampaign($advertiser);

        $targetings = $this->createTestTargetingLocation([], 5);

        // create payload to attach
        $payload = $this->createTargetingLocationsPayload($targetings);

        $this->actingAs($advertiser);

        // attach created locations
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.locations.update', ['campaign' => $campaign->id]),
            ['locations' => $payload->toArray(), 'zipcodes' => []]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $locationId = Arr::get($value, 'location_id');
            $excluded = Arr::get($value, 'excluded');

            $this->assertDatabaseHas('campaign_locations', [
                'location_id' => $locationId,
                'excluded'    => $excluded,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($locationId, $response->json("data.targeting.locations.{$key}.id"));
            $this->assertEquals($excluded, $response->json("data.targeting.locations.{$key}.excluded"));
        }

        $this->assertEquals($campaign->id, $response->json('data.id'));

        // detach half
        $payload = $payload->splice(ceil($payload->count() / 2));

        // detach created locations
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.locations.update', ['campaign' => $campaign->id]),
            ['locations' => $payload->toArray(), 'zipcodes' => []]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $locationId = Arr::get($value, 'location_id');
            $excluded = Arr::get($value, 'excluded');

            $this->assertDatabaseHas('campaign_locations', [
                'location_id' => $locationId,
                'excluded'    => $excluded,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($locationId, $response->json("data.targeting.locations.{$key}.id"));
            $this->assertEquals($excluded, $response->json("data.targeting.locations.{$key}.excluded"));
        }

        // detach all
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.locations.update', ['campaign' => $campaign->id]),
            ['locations' => [], 'zipcodes' => []]
        );

        $this->assertDatabaseMissing('campaign_locations', [
            'campaign_id' => $campaign->id,
        ]);

        $this->assertEmpty($response->json('data.targeting.locations'));
    }

    /**
     * Test Update Campaign Zipcodes as Common Advertiser.
     */
    public function testUpdateCampaignZipcodesAsCommonAdvertiser(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $this->mockCampaignPriceActionSuccess($advertiser->id);
        $campaign = $this->createDraftCampaign($advertiser);

        $targetings = $this->createTestTargetingZipcode([], 5);

        // create payload to attach
        $payload = $this->createTargetingZipcodesPayload($targetings);

        $this->actingAs($advertiser);

        // attach created zipcodes
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.locations.update', ['campaign' => $campaign->id]),
            ['zipcodes' => $payload->toArray(), 'locations' => []]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $zipcodeId = Arr::get($value, 'zipcode_id');
            $excluded = Arr::get($value, 'excluded');

            $this->assertDatabaseHas('campaign_zipcodes', [
                'zipcode_id'  => $zipcodeId,
                'excluded'    => $excluded,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($zipcodeId, $response->json("data.targeting.zipcodes.{$key}.id"));
            $this->assertEquals($excluded, $response->json("data.targeting.zipcodes.{$key}.excluded"));
        }

        $this->assertEquals($campaign->id, $response->json('data.id'));

        // detach half
        $payload = $payload->splice(ceil($payload->count() / 2));

        // detach created zipcodes
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.locations.update', ['campaign' => $campaign->id]),
            ['zipcodes' => $payload->toArray(), 'locations' => []]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $zipcodeId = Arr::get($value, 'zipcode_id');
            $excluded = Arr::get($value, 'excluded');

            $this->assertDatabaseHas('campaign_zipcodes', [
                'zipcode_id'  => $zipcodeId,
                'excluded'    => $excluded,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($zipcodeId, $response->json("data.targeting.zipcodes.{$key}.id"));
            $this->assertEquals($excluded, $response->json("data.targeting.zipcodes.{$key}.excluded"));
        }

        // detach all
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.locations.update', ['campaign' => $campaign->id]),
            ['zipcodes' => [], 'locations' => []]
        );

        $this->assertDatabaseMissing('campaign_zipcodes', [
            'campaign_id' => $campaign->id,
        ]);

        $this->assertEmpty($response->json('data.targeting.zipcodes'));
    }

    /**
     * Test Update Campaign Zipcodes as Special-Ads Advertiser.
     */
    public function testUpdateCampaignZipcodesAsSpecialAdsAdvertiser(): void
    {
        $advertiser = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        $campaign = $this->createDraftCampaign($advertiser);

        $targetings = $this->createTestTargetingZipcode([], 5);

        // create payload to attach
        $payload = $this->createTargetingZipcodesPayload($targetings);

        $this->actingAs($advertiser);

        // attach created zipcodes
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.locations.update', ['campaign' => $campaign->id]),
            ['zipcodes' => $payload->toArray(), 'locations' => []],
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
