<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Targeting\Models\Traits\CreateAgeGroup;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;
use Modules\Campaign\Models\Traits\CreateCampaign;

class IndexAgeGroupsControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateAgeGroup,
        CreateCampaign,
        CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->createTestTargetingAgeGroup([], 5);
    }

    /**
     * Test get age groups.
     */
    public function testIndexAgeGroups(): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $campaign = $this->createTestCampaign([
            'user_id' => $user->getKey(),
            'status_id'  => CampaignStatus::ID_DRAFT,
        ]);

        $response = $this->get(route('api.campaigns.targeting.ages', ['campaign' => $campaign->id]));

        $response->assertOk();
        $data = $response->json();
        foreach (Arr::get($data, 'data') as $value) {
            $this->assertDatabaseHas('age_groups', [
                'name' => Arr::get($value, 'name'),
                'gender_id' => Arr::get($value, 'gender.id'),
            ]);
        }
    }
}
