<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Targeting\Models\Genre;
use Modules\Targeting\Models\Traits\CreateGenre;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Tests\Feature\Campaign\Controllers\Api\Targeting\Traits\CreateDraftCampaign;
use Tests\TestCase;

class SearchGenresControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateGenre,
        CreateDraftCampaign,
        CreateUser;

    /**
     * @var Genre
     */
    private $genre;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->genre = $this->createTestTargetingGenre();
    }

    /**
     * Test search genres as Common Advertiser.
     */
    public function testSearchGenresAsCommonAdvertiser(): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $campaign = $this->createDraftCampaign($user);

        $response = $this->get(route('api.campaigns.targeting.genres', ['campaign' => $campaign->id]), [
            'query' => $this->genre->name,
        ]);

        $response->assertOk();
        $data = $response->json();
        foreach (Arr::get($data, 'data') as $value) {
            $this->assertDatabaseHas('genres', [
                'name' => Arr::get($value, 'name'),
            ]);
        }
    }

    /**
     * Test search genres as SpecialAds Advertiser.
     */
    public function testSearchGenresAsSpecialAdsAdvertiser(): void
    {
        $user = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        $this->actingAs($user);

        $campaign = $this->createDraftCampaign($user);

        $response = $this->get(route('api.campaigns.targeting.genres', ['campaign' => $campaign->id]), [
            'query' => $this->genre->name,
        ]);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
