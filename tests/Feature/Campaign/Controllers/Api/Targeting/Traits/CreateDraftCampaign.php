<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting\Traits;

use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\User\Models\User;

trait CreateDraftCampaign
{
    use CreateCampaign;

    /**
     * @param User $user
     *
     * @return Campaign
     */
    private function createDraftCampaign(User $user): Campaign
    {
        return $this->createTestCampaign([
            'user_id' => $user->getKey(),
            'status_id'  => CampaignStatus::ID_DRAFT,
        ]);
    }
}
