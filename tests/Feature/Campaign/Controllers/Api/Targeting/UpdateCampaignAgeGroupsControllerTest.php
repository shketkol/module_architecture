<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Database\Seeders\Tests\TargetingTypesTableSeeder;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Testing\TestResponse;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\Campaign;
use Modules\Targeting\Models\AgeGroup;
use Modules\Targeting\Models\Gender;
use Modules\Targeting\Models\Traits\CreateAgeGroup;
use Modules\Targeting\Models\Traits\CreateGender;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\User;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\Feature\Campaign\Controllers\Api\Targeting\Traits\CreateDraftCampaign;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\CampaignPriceMock;

class UpdateCampaignAgeGroupsControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateDraftCampaign,
        CreateAgeGroup,
        CreateGender,
        CampaignPriceMock;

    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * @var Collection
     */
    private $payload;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(TargetingTypesTableSeeder::class);
    }

    /**
     * Test Update Campaign Age Groups as Common Advertiser.
     */
    public function testUpdateCampaignAgeGroupsAsCommonAdvertiser(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $this->mockCampaignPriceActionSuccess($advertiser->id);
        $gender = $this->createTestTargetingGender();
        $ageGroups = $this->createTestTargetingAgeGroup(['gender_id' => $gender->id], 3);

        $response = $this->setDataAndMakeRequest($advertiser, $gender, $ageGroups);
        $this->verifySuccessResponse($response);
    }

    /**
     * Test Update Campaign Age Groups as Special-Ads Advertiser - Success.
     */
    public function testUpdateCampaignAgeGroupsAsSpecialAdsAdvertiserSuccess(): void
    {
        $this->markTestSkipped('TODO: Test should be adjusted.');
        $advertiser = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        /**
         * TODO:
         * Functionality that is being tested is dependent on Gender with name "all".
         * It should be either added with some seeder
         * or functionality should be adjusted in order to be independent from the particular record.
         */
        $gender = Gender::getAllGender();
        $ageGroups = AgeGroup::where('gender_id', $gender->id)->get();

        $response = $this->setDataAndMakeRequest($advertiser, $gender, $ageGroups);
        $this->verifySuccessResponse($response);
    }

    /**
     * Test Update Campaign Age Groups as Special-Ads Advertiser - Fail.
     */
    public function testUpdateCampaignAgeGroupsAsSpecialAdsAdvertiserFail(): void
    {
        $advertiser = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        $gender = $this->createTestTargetingGender();
        $ageGroups = $this->createTestTargetingAgeGroup(['gender_id' => $gender->id], 3);

        $response = $this->setDataAndMakeRequest($advertiser, $gender, $ageGroups);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param TestResponse $response
     *
     * @return void
     */
    private function verifySuccessResponse(TestResponse $response): void
    {
        $response->assertStatus(Response::HTTP_OK);
        foreach (Arr::get($this->payload, 'ageGroups') as $key => $value) {
            $this->assertDatabaseHas('campaign_age_groups', [
                'age_group_id' => $value,
                'campaign_id' => $this->campaign->id,
            ]);

            $this->assertEquals($value, $response->json("data.targeting.ages.{$key}.id"));
        }
        $this->assertEquals($this->campaign->id, $response->json('data.id'));
    }

    /**
     * @param User       $advertiser
     * @param Gender     $gender
     * @param Collection $ageGroups
     *
     * @return TestResponse
     */
    private function setDataAndMakeRequest(User $advertiser, Gender $gender, Collection $ageGroups): TestResponse
    {
        $this->campaign = $this->createDraftCampaign($advertiser);

        // Create payload to attach.
        $this->payload = [
            'ageGroups' => $this->createTargetingAgeGroupsPayload($ageGroups)->toArray(),
            'genderId' => $gender->id
        ];

        $this->actingAs($advertiser);

        return $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.ages.update', ['campaign' => $this->campaign->id]),
            $this->payload
        );
    }
}
