<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Database\Seeders\Tests\TargetingTypesTableSeeder;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Targeting\Models\Traits\CreateAudience;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\Feature\Campaign\Controllers\Api\Targeting\Traits\CreateDraftCampaign;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\CampaignPriceMock;

class UpdateCampaignAudiencesControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateDraftCampaign,
        CreateAudience,
        CampaignPriceMock;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(TargetingTypesTableSeeder::class);
    }

    /**
     * Test Update Campaign Audiences as Common Advertiser.
     */
    public function testUpdateCampaignAudiencesAsCommonAdvertiser(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $this->mockCampaignPriceActionSuccess($advertiser->id);
        $campaign = $this->createDraftCampaign($advertiser);

        $targetings = $this->createTestTargetingAudience([], 5);

        // create payload to attach
        $payload = $this->createTargetingAudiencePayload($targetings);

        $this->actingAs($advertiser);

        // attach created audiences
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.audiences.update', ['campaign' => $campaign->id]),
            ['audiences' => $payload->toArray()]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $audienceId = Arr::get($value, 'audience_id');

            $this->assertDatabaseHas('campaign_audiences', [
                'audience_id'    => $audienceId,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($audienceId, $response->json("data.targeting.audiences.{$key}.id"));
        }

        $this->assertEquals($campaign->id, $response->json('data.id'));

        // detach half
        $payload = $payload->splice(ceil($payload->count() / 2));

        // detach created audiences
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.audiences.update', ['campaign' => $campaign->id]),
            ['audiences' => $payload->toArray()]
        );

        $response->assertStatus(Response::HTTP_OK);

        foreach ($payload as $key => $value) {
            $audienceId = Arr::get($value, 'audience_id');

            $this->assertDatabaseHas('campaign_audiences', [
                'audience_id'    => $audienceId,
                'campaign_id' => $campaign->id,
            ]);

            $this->assertEquals($audienceId, $response->json("data.targeting.audiences.{$key}.id"));
        }

        // detach all
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.audiences.update', ['campaign' => $campaign->id]),
            ['audiences' => []]
        );

        $this->assertDatabaseMissing('campaign_audiences', [
            'campaign_id' => $campaign->id,
        ]);

        $this->assertEmpty($response->json('data.targeting.audiences'));
    }

    /**
     * Test Update Campaign Audiences as Special-Ads Advertiser.
     */
    public function testUpdateCampaignAudiencesAsSpecialAdsAdvertiser(): void
    {
        $advertiser = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        $campaign = $this->createDraftCampaign($advertiser);

        $targetings = $this->createTestTargetingAudience([], 5);

        // create payload to attach
        $payload = $this->createTargetingAudiencePayload($targetings);

        $this->actingAs($advertiser);

        // attach created audiences
        $response = $this->json(
            Request::METHOD_PATCH,
            route('api.campaigns.targeting.audiences.update', ['campaign' => $campaign->id]),
            ['audiences' => $payload->toArray()]
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
