<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Arr;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Targeting\Models\Location;
use Modules\Targeting\Models\Traits\CreateLocation;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;
use Modules\Campaign\Models\Traits\CreateCampaign;

class SearchLocationControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateLocation,
        CreateCampaign,
        CreateUser;

    /**
     * @var Location
     */
    private $location;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->location = $this->createTestTargetingLocation();
    }

    /**
     * Test search location.
     */
    public function testSearchLocation(): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $campaign = $this->createTestCampaign([
            'user_id' => $user->getKey(),
            'status_id'  => CampaignStatus::ID_DRAFT,
        ]);

        $response = $this->get(route('api.campaigns.targeting.locations', ['campaign' => $campaign->id]), [
            'query' => $this->location->name
        ]);

        $response->assertOk();
        $data = $response->json();
        foreach (Arr::get($data, 'data') as $value) {
            $this->assertDatabaseHas('locations', [
                'name' => Arr::get($value, 'name'),
            ]);
        }
    }
}
