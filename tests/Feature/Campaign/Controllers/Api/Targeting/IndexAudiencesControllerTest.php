<?php

namespace Tests\Feature\Campaign\Controllers\Api\Targeting;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Targeting\Models\Traits\CreateAudience;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Tests\Feature\Campaign\Controllers\Api\Targeting\Traits\CreateDraftCampaign;
use Tests\TestCase;

class IndexAudiencesControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateAudience,
        CreateDraftCampaign,
        CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->createTestTargetingAudience([], 5);
    }

    /**
     * Test get audiences as Common Advertiser.
     */
    public function testAudiencesAsCommonAdvertiser(): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);
        $campaign = $this->createDraftCampaign($user);

        $response = $this->get(route('api.campaigns.targeting.audiences', ['campaign' => $campaign->id]));

        $response->assertOk();
        $data = $response->json();
        $this->checkExistingAudiences(Arr::get($data, 'data'));
    }

    /**
     * Test get audiences as SpecialAds Advertiser.
     */
    public function testAudiencesAsSpecialAdsAdvertiser(): void
    {
        $user = $this->createTestAdvertiser([
            'account_type_id' => AccountType::ID_SPECIAL_ADS,
        ]);
        $this->actingAs($user);
        $campaign = $this->createDraftCampaign($user);

        $response = $this->get(route('api.campaigns.targeting.audiences', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * @param array $data
     * @return mixed
     */
    private function checkExistingAudiences(array $data)
    {
        foreach ($data as $value) {
            if (!empty($value['children'])) {
                return $this->checkExistingAudiences($value['children']);
            }
            $this->assertDatabaseHas('audiences', [
                'name' => Arr::get($value, 'name'),
            ]);
        }
    }
}
