<?php

namespace Tests\Feature\Campaign\Actions;

use Database\Seeders\Tests\TargetingDevicesSeeder;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignPermissionsTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\UserGetMock;

class GetCampaignEditRulesActionTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        UserGetMock,
        CreateCampaign;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(CampaignPermissionsTableSeeder::class);
        $this->seed(TargetingDevicesSeeder::class);
    }

    /**
     * Test campaign edit rules.
     *
     * @dataProvider rulesDataProvider
     *
     * @param int $fromStatus
     * @param array $expected
     */
    public function testCampaignEditRules(int $fromStatus, array $expected): void
    {
        $user = $this->createTestAdvertiser();
        $campaign = $this->createTestCampaign([
            'user_id'            => $user->getKey(),
            'status_changed_by'  => $user->getKey(),
            'status_id'          => $fromStatus,
            'previous_status_id' => CampaignStatus::ID_LIVE,
        ]);

        $this->actingAs($user);

        $this->mockUserGetActionSuccess($user);

        $response = $this->json(Request::METHOD_GET, route('api.campaigns.details.show', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals($expected, $response->json('data.states'));
    }

    /**
     * @return \Generator
     */
    public function rulesDataProvider(): \Generator
    {
        yield [
            CampaignStatus::ID_DRAFT,
            [
                "can_pause"                 => false,
                "can_cancel"                => false,
                "can_resume"                => false,
                "can_edit"                  => true,
                "can_delete"                => true,
                "can_duplicate"             => true,
                "can_open_advertiser_page"  => false,
                "can_manage_creative"       => true,
                "can_only_replace_creative" => false,
                "can_download_report"       => false,
                "wizard"                    => [
                    "can_edit_start_date"    => true,
                    "can_edit_end_date"      => true,
                    "can_edit_budget"        => true,
                    "can_validate_promocode" => true,
                    "targetings"             => [
                        "can_edit_age_groups" => true,
                        "can_edit_audiences"  => true,
                        "can_edit_locations"  => true,
                        "can_edit_zipcodes"   => true,
                        "can_edit_genres"     => true,
                        "can_edit_platforms"  => true,
                    ]
                ]
            ],
        ];

        yield [
            CampaignStatus::ID_PENDING_APPROVAL,
            [
                'can_pause'                 => false,
                'can_cancel'                => true,
                'can_resume'                => false,
                'can_edit'                  => false,
                'can_delete'                => false,
                'can_duplicate'             => true,
                'can_open_advertiser_page'  => false,
                'can_manage_creative'       => true,
                'can_only_replace_creative' => false,
                'can_download_report'       => false,
                'wizard'                    => [
                    'can_edit_start_date'    => false,
                    'can_edit_end_date'      => false,
                    'can_edit_budget'        => false,
                    'can_validate_promocode' => false,
                    'targetings'             => [
                        'can_edit_age_groups' => false,
                        'can_edit_audiences'  => false,
                        'can_edit_locations'  => false,
                        'can_edit_zipcodes'   => false,
                        'can_edit_genres'     => false,
                        'can_edit_platforms'  => false
                    ]
                ],
            ],
        ];

        yield [
            CampaignStatus::ID_READY,
            [
                'can_pause'                 => true,
                'can_cancel'                => true,
                'can_resume'                => false,
                'can_edit'                  => true,
                'can_delete'                => false,
                'can_duplicate'             => true,
                'can_open_advertiser_page'  => false,
                'can_manage_creative'       => false,
                'can_only_replace_creative' => false,
                'can_download_report'       => false,
                'wizard'                    => [
                    'can_edit_start_date'    => true,
                    'can_edit_end_date'      => true,
                    'can_edit_budget'        => true,
                    'can_validate_promocode' => false,
                    'targetings'             => [
                        'can_edit_age_groups' => true,
                        'can_edit_audiences'  => false,
                        'can_edit_locations'  => false,
                        'can_edit_zipcodes'   => false,
                        'can_edit_genres'     => true,
                        'can_edit_platforms'  => true
                    ]
                ],
            ]
        ];

        yield [
            CampaignStatus::ID_LIVE,
            [
                'can_pause'                 => true,
                'can_cancel'                => true,
                'can_resume'                => false,
                'can_edit'                  => true,
                'can_delete'                => false,
                'can_duplicate'             => true,
                'can_open_advertiser_page'  => false,
                'can_manage_creative'       => true,
                'can_only_replace_creative' => false,
                'can_download_report'       => true,
                'wizard'                    => [
                    'can_edit_start_date'    => false,
                    'can_edit_end_date'      => true,
                    'can_edit_budget'        => true,
                    'can_validate_promocode' => false,
                    'targetings'             => [
                        'can_edit_age_groups' => true,
                        'can_edit_audiences'  => false,
                        'can_edit_locations'  => false,
                        'can_edit_zipcodes'   => false,
                        'can_edit_genres'     => true,
                        'can_edit_platforms'  => true
                    ]
                ],
            ],
        ];

        yield [
            CampaignStatus::ID_PAUSED,
            [
                'can_pause'                 => false,
                'can_cancel'                => true,
                'can_resume'                => true,
                'can_edit'                  => true,
                'can_delete'                => false,
                'can_duplicate'             => true,
                'can_open_advertiser_page'  => false,
                'can_manage_creative'       => true,
                'can_only_replace_creative' => false,
                'can_download_report'       => true,
                'wizard'                    => [
                    'can_edit_start_date'    => true,
                    'can_edit_end_date'      => true,
                    'can_edit_budget'        => true,
                    'can_validate_promocode' => false,
                    'targetings'             => [
                        'can_edit_age_groups' => true,
                        'can_edit_audiences'  => false,
                        'can_edit_locations'  => false,
                        'can_edit_zipcodes'   => false,
                        'can_edit_genres'     => true,
                        'can_edit_platforms'  => true
                    ]
                ],
            ],
        ];

        yield [
            CampaignStatus::ID_CANCELED,
            [
                'can_pause'                 => false,
                'can_cancel'                => false,
                'can_resume'                => false,
                'can_edit'                  => false,
                'can_delete'                => false,
                'can_duplicate'             => true,
                'can_open_advertiser_page'  => false,
                'can_manage_creative'       => false,
                'can_only_replace_creative' => false,
                'can_download_report'       => true,
                'wizard'                    => [
                    'can_edit_start_date'    => false,
                    'can_edit_end_date'      => false,
                    'can_edit_budget'        => false,
                    'can_validate_promocode' => false,
                    'targetings'             => [
                        'can_edit_age_groups' => false,
                        'can_edit_audiences'  => false,
                        'can_edit_locations'  => false,
                        'can_edit_zipcodes'   => false,
                        'can_edit_genres'     => false,
                        'can_edit_platforms'  => false
                    ]
                ],
            ],
        ];

        yield [
            CampaignStatus::ID_COMPLETED,
            [
                'can_pause'                 => false,
                'can_cancel'                => false,
                'can_resume'                => false,
                'can_edit'                  => false,
                'can_delete'                => false,
                'can_duplicate'             => true,
                'can_open_advertiser_page'  => false,
                'can_manage_creative'       => false,
                'can_only_replace_creative' => false,
                'can_download_report'       => true,
                'wizard'                    => [
                    'can_edit_start_date'    => false,
                    'can_edit_end_date'      => false,
                    'can_edit_budget'        => false,
                    'can_validate_promocode' => false,
                    'targetings'             => [
                        'can_edit_age_groups' => false,
                        'can_edit_audiences'  => false,
                        'can_edit_locations'  => false,
                        'can_edit_zipcodes'   => false,
                        'can_edit_genres'     => false,
                        'can_edit_platforms'  => false
                    ]
                ],
            ],
        ];
    }
}
