<?php

namespace Tests\Feature\Campaign\Actions;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Notification as NotificationFacade;
use Modules\Campaign\Database\Seeders\CampaignPermissionsTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\CampaignCancelMock;
use Tests\Traits\HaapiMocks\CampaignGetMock;
use Tests\Traits\HaapiMocks\CampaignPauseMock;
use Tests\Traits\HaapiMocks\CampaignResumeMock;

class AdminUpdateCampaignStatusActionTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCampaign,
        CampaignPauseMock,
        CampaignResumeMock,
        CampaignCancelMock,
        CampaignGetMock;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(CampaignPermissionsTableSeeder::class);
    }

    /**
     * Test cancel/pause/resume campaign.
     *
     * @dataProvider statusSuccessDataProvider
     *
     * @param string $route
     * @param int    $fromStatus
     * @param int    $toStatus
     */
    public function testChangeCampaignStatusSuccess(
        string $route,
        int $fromStatus,
        int $toStatus
    ): void {
        NotificationFacade::fake();

        $advertiser = $this->createTestAdvertiser();
        $admin = $this->createTestAdmin();

        $campaign = $this->createTestCampaign([
            'user_id'            => $advertiser->getKey(),
            'status_changed_by'  => $admin->getKey(),
            'status_id'          => $fromStatus,
            'external_id'        => self::$externalId,
            'previous_status_id' => CampaignStatus::ID_LIVE,
        ]);

        $this->actingAs($admin, 'admin');

        $this->mockCampaignPauseActionSuccess(self::$externalId, $admin->id);
        $this->mockCampaignResumeActionSuccess(self::$externalId, $admin->id);
        $this->mockCampaignCancelActionSuccess(self::$externalId, $admin->id);
        $this->mockCampaignGetActionSuccess($campaign, "LIVE");

        $response = $this->json(Request::METHOD_PATCH, route($route, ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals($toStatus, $response->json('data.status_id'));

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->getKey(),
            'status_id' => $toStatus,
        ]);
    }

    /**
     * Test cancel/pause/resume campaign failed.
     *
     * @dataProvider statusFailDataProvider
     *
     * @param string $route
     * @param int    $fromStatus
     * @param int    $toStatus
     * @param int    $expectedStatusCode
     */
    public function testChangeCampaignStatusFail(
        string $route,
        int $fromStatus,
        int $toStatus,
        int $expectedStatusCode
    ): void {
        NotificationFacade::fake();

        $user = $this->createTestAdvertiser();
        $admin = $this->createTestAdmin();

        $campaign = $this->createTestCampaign([
            'user_id'           => $user->getKey(),
            'status_id'         => $fromStatus,
            'status_changed_by' => $admin->id,
        ]);

        $this->actingAs($admin, 'admin');

        $response = $this->json(Request::METHOD_PATCH, route($route, ['campaign' => $campaign->id]));

        $response->assertStatus($expectedStatusCode);

        $this->assertDatabaseMissing('campaigns', [
            'id'        => $campaign->getKey(),
            'status_id' => $toStatus,
        ]);
    }

    /**
     * Test cancel/pause/resume campaign by read only admin failed.
     *
     * @dataProvider statusFailAdminReadOnlyDataProvider
     *
     * @param string $route
     * @param int    $fromStatus
     * @param int    $toStatus
     */
    public function testChangeCampaignStatusReadOnlyAdminFail(
        string $route,
        int $fromStatus,
        int $toStatus
    ): void {
        NotificationFacade::fake();

        $user = $this->createTestAdvertiser();
        $readOnlyAdmin = $this->createTestReadOnlyAdmin();

        $campaign = $this->createTestCampaign([
            'user_id' => $user->getKey(),
            'status_id'  => $fromStatus,
        ]);

        $this->actingAs($readOnlyAdmin, 'admin');

        $response = $this->json(Request::METHOD_PATCH, route($route, ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertDatabaseMissing('campaigns', [
            'id'        => $campaign->getKey(),
            'status_id' => $toStatus,
        ]);
    }

    /**
     * @return \Generator
     */
    public function statusSuccessDataProvider(): \Generator
    {
        yield [
            'api.campaigns.cancel',
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_PROCESSING,
        ];

        yield [
            'api.campaigns.pause',
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_PAUSED,
        ];

        yield [
            'api.campaigns.resume',
            CampaignStatus::ID_PAUSED,
            CampaignStatus::ID_LIVE,
        ];
    }

    /**
     * @return \Generator
     */
    public function statusFailDataProvider(): \Generator
    {
        yield [
            'api.campaigns.cancel',
            CampaignStatus::ID_DRAFT,
            CampaignStatus::ID_LIVE,
            Response::HTTP_UNPROCESSABLE_ENTITY,
        ];

        yield [
            'api.campaigns.pause',
            CampaignStatus::ID_COMPLETED,
            CampaignStatus::ID_PAUSED,
            Response::HTTP_UNPROCESSABLE_ENTITY,
        ];

        yield [
            'api.campaigns.resume',
            CampaignStatus::ID_PENDING_APPROVAL,
            CampaignStatus::ID_LIVE,
            Response::HTTP_FORBIDDEN,
        ];
    }

    /**
     * @return \Generator
     */
    public function statusFailAdminReadOnlyDataProvider(): \Generator
    {
        yield [
            'api.campaigns.cancel',
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_CANCELED,
        ];

        yield [
            'api.campaigns.pause',
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_PAUSED,
        ];

        yield [
            'api.campaigns.resume',
            CampaignStatus::ID_PAUSED,
            CampaignStatus::ID_LIVE,
        ];
    }
}
