<?php

namespace Tests\Feature\Campaign\Actions;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignPermissionsTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\UserStatus;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminDataGetMock;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\CampaignCancelMock;
use Tests\Traits\HaapiMocks\CampaignGetMock;
use Tests\Traits\HaapiMocks\CampaignPauseMock;
use Tests\Traits\HaapiMocks\CampaignResumeMock;
use Tests\Traits\HaapiMocks\SelfDataGetMock;
use Tests\Traits\HaapiMocks\UserGetMock;

class UpdateCampaignStatusActionTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCampaign,
        UserGetMock,
        AdminSignInMock,
        SelfDataGetMock,
        AdminDataGetMock,
        CampaignPauseMock,
        CampaignResumeMock,
        CampaignCancelMock,
        CampaignGetMock;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(CampaignPermissionsTableSeeder::class);
    }

    /**
     * Test cancel/pause/resume campaign.
     *
     * @dataProvider statusSuccessDataProvider
     *
     * @param string $route
     * @param int    $fromStatus
     * @param int    $toStatus
     */
    public function testChangeCampaignStatusSuccess(
        string $route,
        int $fromStatus,
        int $toStatus
    ): void {
        $user = $this->createTestAdvertiser();
        $campaign = $this->createTestCampaign([
            'user_id'            => $user->getKey(),
            'status_changed_by'  => $user->getKey(),
            'status_id'          => $fromStatus,
            'external_id'        => self::$externalId,
            'previous_status_id' => CampaignStatus::ID_LIVE,
        ]);

        $this->mockAdminSignInActionSuccess();

        $this->actingAs($user);
        $this->mockCampaignPauseActionSuccess(self::$externalId, $user->id);
        $this->mockCampaignResumeActionSuccess(self::$externalId, $user->id);
        $this->mockCampaignCancelActionSuccess(self::$externalId, $user->id);
        $this->mockCampaignGetActionSuccess($campaign, "LIVE");
        $this->mockUserGetActionSuccess($user);
        $this->mockSelfDataGetActionSuccess();
        $this->mockAdminDataGetActionSuccess();

        $response = $this->json(Request::METHOD_PATCH, route($route, ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals($toStatus, $response->json('data.status_id'));

        // check database notification
        $this->assertCount(0, $user->notifications);

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->getKey(),
            'status_id' => $toStatus,
        ]);
    }

    /**
     * Test deactivated user failed.
     */
    public function testChangeCampaignStatusDeactivated(): void
    {
        $advertiser = $this->createTestAdvertiser(['status_id' => UserStatus::ID_INACTIVE]);

        $campaign = $this->createTestCampaign([
            'user_id'   => $advertiser->getKey(),
            'status_id' => CampaignStatus::ID_PAUSED,
        ]);

        $this->actingAs($advertiser);

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.resume', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertDatabaseMissing('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_LIVE,
        ]);
    }

    /**
     * Test cancel/pause/resume campaign failed.
     *
     * @dataProvider statusFailDataProvider
     *
     * @param string $route
     * @param int    $fromStatus
     * @param int    $toStatus
     * @param int    $expectedStatusCode
     */
    public function testChangeCampaignStatusFail(
        string $route,
        int $fromStatus,
        int $toStatus,
        int $expectedStatusCode
    ): void {
        $user = $this->createTestAdvertiser(['status_id' => UserStatus::ID_ACTIVE]);

        $campaign = $this->createTestCampaign([
            'user_id'           => $user->getKey(),
            'status_changed_by' => $user->getKey(),
            'status_id'         => $fromStatus,
        ]);

        $this->actingAs($user);

        $response = $this->json(Request::METHOD_PATCH, route($route, ['campaign' => $campaign->id]));

        $response->assertStatus($expectedStatusCode);

        $this->assertDatabaseMissing('campaigns', [
            'id'        => $campaign->getKey(),
            'status_id' => $toStatus,
        ]);
    }

    /**
     * @return \Generator
     */
    public function statusSuccessDataProvider(): \Generator
    {
        yield [
            'api.campaigns.cancel',
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_PROCESSING,
        ];

        yield [
            'api.campaigns.pause',
            CampaignStatus::ID_LIVE,
            CampaignStatus::ID_PAUSED,
        ];

        yield [
            'api.campaigns.resume',
            CampaignStatus::ID_PAUSED,
            CampaignStatus::ID_LIVE,
        ];
    }

    /**
     * @return \Generator
     */
    public function statusFailDataProvider(): \Generator
    {
        yield [
            'api.campaigns.cancel',
            CampaignStatus::ID_DRAFT,
            CampaignStatus::ID_LIVE,
            Response::HTTP_UNPROCESSABLE_ENTITY,
        ];

        yield [
            'api.campaigns.pause',
            CampaignStatus::ID_COMPLETED,
            CampaignStatus::ID_PAUSED,
            Response::HTTP_UNPROCESSABLE_ENTITY,
        ];

        yield [
            'api.campaigns.resume',
            CampaignStatus::ID_PENDING_APPROVAL,
            CampaignStatus::ID_LIVE,
            Response::HTTP_FORBIDDEN,
        ];
    }
}
