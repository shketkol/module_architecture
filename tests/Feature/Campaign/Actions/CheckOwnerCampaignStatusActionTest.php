<?php

namespace Tests\Feature\Campaign\Actions;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Modules\Campaign\Database\Seeders\CampaignPermissionsTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\CampaignGetMock;
use Tests\Traits\HaapiMocks\CampaignPauseMock;
use Tests\Traits\HaapiMocks\CampaignResumeMock;
use Tests\Traits\HaapiMocks\UserGetMock;

class CheckOwnerCampaignStatusActionTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        UserGetMock,
        CreateCampaign,
        CampaignGetMock,
        CampaignPauseMock,
        CampaignResumeMock;

    public const ADVERTISER_EXT_ID = 'asdf-asdf-1234-qwer-5678';
    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
        $this->seed(CampaignPermissionsTableSeeder::class);
    }

    /**
     * Test paused campaign by advertiser cannot be resumed by admin.
     */
    public function testPausedByAdvertiser(): void
    {
        Notification::fake();

        $advertiser = $this->createTestAdvertiser([
            'external_id' => self::ADVERTISER_EXT_ID,
        ]);
        $campaign = $this->createTestCampaign([
            'user_id'     => $advertiser->id,
            'status_id'   => CampaignStatus::ID_LIVE,
            'external_id' => self::$externalId,
            'date_start'  => Carbon::now()->subHour(),
            'date_end'    => Carbon::now()->addDays(3),
        ]);

        // pause as advertiser
        $this->actingAs($advertiser, 'web');

        $this->mockCampaignPauseActionSuccess(self::$externalId, $advertiser->id);
        $this->mockUserGetActionSuccess($advertiser);

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.pause', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals(CampaignStatus::ID_PAUSED, $response->json('data.status_id'));

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_PAUSED,
        ]);
        Auth::logout();

        // resume as read-only admin should fail he has no permissions
        $adminReadOnly = $this->createTestReadOnlyAdmin();
        $this->actingAs($adminReadOnly, 'admin');

        $this->mockCampaignResumeActionSuccess(self::$externalId, $adminReadOnly->id);

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.resume', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_PAUSED,
        ]);
        Auth::logout();

        // resume as full rights admin should fail
        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');

        $this->mockCampaignResumeActionSuccess(self::$externalId, $admin->id);

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.resume', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_PAUSED,
        ]);
        Auth::logout();

        // resume as advertiser should work
        $this->actingAs($advertiser, 'web');

        $this->mockCampaignResumeActionSuccess(self::$externalId, $advertiser->id);
        $this->mockCampaignGetActionSuccess($campaign, "LIVE");

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.resume', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals(CampaignStatus::ID_LIVE, $response->json('data.status_id'));

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_LIVE,
        ]);
    }

    /**
     * Test paused campaign by admin cannot be resumed by advertiser.
     */
    public function testPausedByAdmin(): void
    {
        Notification::fake();

        $advertiser = $this->createTestAdvertiser();
        $campaign = $this->createTestCampaign([
            'user_id' => $advertiser->id,
            'status_id' => CampaignStatus::ID_LIVE,
            'external_id' => self::$externalId,
            'date_start'  => Carbon::now()->subHour(),
            'date_end'    => Carbon::now()->addDays(3),
        ]);

        // pause as read-only admin should fail he has no permissions
        $adminReadOnly = $this->createTestReadOnlyAdmin();
        $this->actingAs($adminReadOnly, 'admin');

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.pause', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_LIVE,
        ]);

        // pause as full-rights admin should work
        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');

        $this->mockCampaignPauseActionSuccess(self::$externalId, $admin->id);

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.pause', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals(CampaignStatus::ID_PAUSED, $response->json('data.status_id'));

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_PAUSED,
        ]);

        // resume as advertiser should fail
        $this->actingAs($advertiser);

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.resume', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_PAUSED,
        ]);

        // resume as read-only admin should fail he has no permissions
        $this->actingAs($adminReadOnly, 'admin');

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.resume', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_PAUSED,
        ]);

        // resume as full rights admin should work
        $this->actingAs($admin, 'admin');

        $this->mockCampaignResumeActionSuccess(self::$externalId, $admin->id);
        $this->mockCampaignGetActionSuccess($campaign, "LIVE");

        $response = $this->json(Request::METHOD_PATCH, route('api.campaigns.resume', ['campaign' => $campaign->id]));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals(CampaignStatus::ID_LIVE, $response->json('data.status_id'));

        $this->assertDatabaseHas('campaigns', [
            'id'        => $campaign->id,
            'status_id' => CampaignStatus::ID_LIVE,
        ]);
    }
}
