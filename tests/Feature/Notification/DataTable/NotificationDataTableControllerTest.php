<?php

namespace Tests\Feature\Notification\DataTable;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Notification\Models\Traits\CreateNotification;
use Modules\Notification\Database\Seeders\NotificationPermissionsTableSeeder;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;

class NotificationDataTableControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateNotification;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesTableSeeder::class);
        $this->seed(NotificationPermissionsTableSeeder::class);
    }

    /**
     * Test advertiser can view notifications data-table.
     */
    public function testAdvertiserNotificationViewDataTable(): void
    {
        $advertiser = $this->createTestAdvertiser();

        $count = 5;
        $this->createTestNotifications(['notifiable_id' => $advertiser->id], $count);

        $this->actingAs($advertiser);
        $response = $this->json(Request::METHOD_GET, route('dataTable.index', ['table' => 'notifications']));
        $response->assertOk();

        $this->assertCount($count, $response->json('data'));
        $this->assertCount(6, $response->json('data.0'));
        $this->assertIsString($response->json('data.0.id'));
        $this->assertNotEmpty($response->json('data.0.category'));
        $this->assertNotEmpty($response->json('data.0.content'));
        $this->assertIsString($response->json('data.0.content_preview'));
        $this->assertIsBool($response->json('data.0.is_read'));
        $this->assertNotEmpty($response->json('data.0.created_at'));

        // another advertiser can not see those notifications
        $advertiser2 = $this->createTestAdvertiser();

        $this->actingAs($advertiser2);
        $response = $this->json(Request::METHOD_GET, route('dataTable.index', ['table' => 'notifications']));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertCount(0, $response->json('data'));
    }

    /**
     * Test admin can not view notifications data-table.
     */
    public function testAdminViewNotificationDataTable(): void
    {
        $admin = $this->createTestAdmin();
        $this->createTestNotifications();

        $this->actingAs($admin, 'admin');

        $response = $this->json(Request::METHOD_GET, route('dataTable.index', ['table' => 'notifications']));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
