<?php

namespace Tests\Feature\Creative;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Creative\Models\CreativeStatus;
use Modules\User\Models\Role;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Models\Traits\CreateCreative;

class AdvertiserTableControllerTest extends TestCase
{
    use DatabaseTransactions, CreateCreative, CreateCampaign, CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test advertiser creatives data-table cards request.
     */
    public function testCards(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $secondAdvertiser = $this->createTestAdvertiser();
        $this->createTestCreative([
            'status_id'  => CreativeStatus::ID_APPROVED,
            'user_id' => $advertiser->getKey(),
        ], 5);

        $this->createTestCreative([
            'status_id'  => CreativeStatus::ID_PENDING_APPROVAL,
            'user_id' => $secondAdvertiser->getKey(),
        ], 5);

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');
        $approved = $secondAdvertiser->creatives()->approved()->count();
        $validating = $secondAdvertiser->creatives()->where('status_id', CreativeStatus::ID_PENDING_APPROVAL)->count();
        $draft = $secondAdvertiser->creatives()->where('status_id', CreativeStatus::ID_DRAFT)->count();

        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.cards', ['table' => 'advertiser-creatives', 'user_id' => $secondAdvertiser->getKey()])
        );

        $this->assertEquals($approved, $response->json(0 . '.value'));
        $this->assertEquals($validating, $response->json(1 . '.value'));
        $this->assertEquals($draft, $response->json(2 . '.value'));
        $response->assertStatus(Response::HTTP_OK);

        $this->actingAs($secondAdvertiser);
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.cards', ['table' => 'advertiser-creatives', 'user_id' => $advertiser->getKey()])
        );
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * Test advertiser-creative data table
     */
    public function testDataTable(): void
    {
        $advertiser = $this->createTestAdvertiser();
        $secondAdvertiser = $this->createTestAdvertiser();
        $count = 10;

        $this->createTestCreative(['user_id' => $secondAdvertiser->getKey()], $count);

        $this->actingAs($advertiser);
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'advertiser-creatives', 'user_id' => $secondAdvertiser->getKey()])
        );
        $response->assertStatus(Response::HTTP_FORBIDDEN);

        $this->actingAs($advertiser->assignRole(Role::ADMIN), 'admin');
        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'advertiser-creatives', 'user_id' => $secondAdvertiser->getKey()])
        );
        $response->assertStatus(Response::HTTP_OK);
    }
}
