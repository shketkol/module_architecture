<?php

namespace Tests\Feature\Creative\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;

class DeleteCreativeControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCreative,
        CreateCampaign;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test delete creative forbidden.
     * @dataProvider statusForbiddenDataProvider
     *
     * @param int $creativeStatus
     * @param int $expectedStatusCode
     *
     * @return void
     */
    public function testDeleteCreativeForbidden(int $creativeStatus, int $expectedStatusCode): void
    {
        $this->deleteCretive($creativeStatus, $expectedStatusCode);
    }

    /**
     * Test delete creative success.
     * @dataProvider statusSuccessDataProvider
     *
     * @param int $campaignStatus
     * @param int $expectedStatusCode
     *
     * @return void
     */
    public function testDeleteCreativeSuccess(int $campaignStatus, int $expectedStatusCode): void
    {
        $this->deleteCretive($campaignStatus, $expectedStatusCode);
    }

    /**
     * delete creative.
     *
     * @param int $campaignStatus
     * @param int $expectedStatusCode
     *
     * @return void
     */
    public function deleteCretive(int $campaignStatus, int $expectedStatusCode): void
    {
        $user = $this->createTestAdvertiser();
        $this->actingAs($user);

        $creative = $this->createTestCreative([
            'status_id' => CreativeStatus::ID_DRAFT,
            'user_id'   => $user->getKey(),
        ]);

        $campaign = $this->createTestCampaign([
            'status_id' => $campaignStatus,
            'user_id'   => $user->getKey()
        ]);

        $campaign->creatives()->attach($creative->id);

        $response = $this->json(Request::METHOD_DELETE, route('api.creatives.delete', [
            'creative' => $creative->id
        ]));

        $response->assertStatus($expectedStatusCode);
    }

    /**
     * @return \Generator
     */
    public function statusSuccessDataProvider(): \Generator
    {
        yield [
            CampaignStatus::ID_DRAFT,
            Response::HTTP_OK,
        ];

        yield [
            CampaignStatus::ID_COMPLETED,
            Response::HTTP_OK,
        ];

        yield [
            CampaignStatus::ID_CANCELED,
            Response::HTTP_OK,
        ];
    }

    /**
     * @return \Generator
     */
    public function statusForbiddenDataProvider(): \Generator
    {
        yield [
            CampaignStatus::ID_LIVE,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_PENDING_APPROVAL,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_READY,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_LIVE,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_PAUSED,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_PROCESSING,
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            CampaignStatus::ID_SUSPENDED,
            Response::HTTP_FORBIDDEN,
        ];
    }
}
