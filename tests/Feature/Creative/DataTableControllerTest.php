<?php

namespace Tests\Feature\Creative;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Database\Seeders\CreativeStatusesTableSeeder;
use Modules\Creative\Models\CreativeStatus;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Models\Traits\CreateCreative;

class DataTableControllerTest extends TestCase
{
    use DatabaseTransactions, CreateUser, CreateCreative, CreateCampaign;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CreativeStatusesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }

    /**
     * Test cards request.
     *
     * @dataProvider cardsDataProvider
     *
     * @param int    $statusId Creative status id
     * @param string $title    Card title
     * @param int    $index    Position in response
     */
    public function testCards(int $statusId, string $title, int $index): void
    {
        $user = $this->createTestAdvertiser();

        $creative = $this->createTestCreative([
            'status_id'  => $statusId,
            'user_id' => $user->getKey(),
        ]);

        $campaign = $this->createTestCampaign([
            'user_id' => $user->getKey(),
        ]);

        $campaign->creatives()->attach($creative->getKey());

        $this->actingAs($user);

        $response = $this->json(Request::METHOD_GET, route('dataTable.cards', ['table' => 'creatives']));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals(trans($title), $response->json($index . '.title'));
        $this->assertEquals('number', $response->json($index . '.type'));
        $this->assertEquals(1, $response->json($index . '.value'));
    }

    /**
     * Test data table has items
     */
    public function testDataTable(): void
    {
        $user = $this->createTestAdvertiser();
        $count = 15;

        $campaigns = $this->createTestCampaign(['user_id' => $user->getKey()], $count);

        $campaigns->each(function (Campaign $campaign) use ($user) {
            $campaign->creatives()->attach($this->createTestCreative(['user_id' => $user->getKey()]));
        });

        $this->actingAs($user);

        $response = $this->json(
            Request::METHOD_GET,
            route('dataTable.index', ['table' => 'creatives-gallery'])
        );

        $response->assertStatus(Response::HTTP_OK);
        $this->assertCount($count, $response->json('data'));
    }

    /**
     * @return \Generator
     */
    public function cardsDataProvider(): \Generator
    {
        yield [
            CreativeStatus::ID_APPROVED,
            'creative::labels.creative.cards.approved_creatives',
            0,
        ];

        yield [
            CreativeStatus::ID_PENDING_APPROVAL,
            'creative::labels.creative.cards.creatives_pending_review',
            1,
        ];

        yield [
            CreativeStatus::ID_DRAFT,
            'creative::labels.creative.cards.draft_creatives',
            2,
        ];
    }
}
