<?php

namespace Tests\Feature\Advertiser\DataTable;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserPermissionsTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;

class AdvertiserDataTableControllerTest extends TestCase
{
    use DatabaseTransactions, CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(UserPermissionsTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
    }

    /**
     * Test advertiser could not view advertisers data-table.
     */
    public function testAdvertiserViewDataTable(): void
    {
        $advertiser = $this->createTestUser();

        $this->actingAs($advertiser);
        $response = $this->json(Request::METHOD_GET, route('dataTable.index', ['table' => 'advertisers']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * Test admin can view advertisers data-table.
     */
    public function testAdminViewDataTable(): void
    {
        $countAdvertisers = 5;
        for ($i = 0; $i < $countAdvertisers; $i++) {
            $this->createTestAdvertiser();
        }

        $admin = $this->createTestReadOnlyAdmin();
        $this->actingAs($admin, 'admin');

        $response = $this->json(Request::METHOD_GET, route('dataTable.index', ['table' => 'advertisers']));

        $response->assertStatus(Response::HTTP_OK);
        $this->assertGreaterThanOrEqual($countAdvertisers, $response->json('data'));

        $this->assertCount(8, $response->json('data.0'));
        $this->assertNotEmpty($response->json('data.0.id'));
        $this->assertNotEmpty($response->json('data.0.company_name'));
        $this->assertIsInt($response->json('data.0.number_of_campaigns'));
        $this->assertNotEmpty($response->json('data.0.user_status'));
    }
}
