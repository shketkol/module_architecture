<?php

namespace Tests\Feature\Advertiser\Controllers\Api;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Advertiser\Notifications\Advertiser\AdvertiserDeactivated;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Campaign\Repositories\Contracts\CampaignRepository;
use Modules\Notification\Models\Notification;
use Modules\User\Models\Role;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\UserStatus;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserPermissionsTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminAccountSetStatusMock;
use Tests\Traits\HaapiMocks\AdminUserGetMock;
use Tests\Traits\HaapiMocks\CampaignPauseMock;

class DeactivateAdvertiserControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCampaign,
        AdminAccountSetStatusMock,
        AdminUserGetMock,
        CampaignPauseMock;

    /**
     * Test admin can deactivate advertisers.
     */
    public function testAdminDeactivate(): void
    {
        $advertiser = $this->createTestAdvertiser([
            'status_id'           => UserStatus::ID_ACTIVE,
            'account_external_id' => self::$accountId,
        ]);

        CampaignStatus::whereNotIn(
            'id',
            [
                CampaignStatus::ID_LIVE,
                CampaignStatus::ID_READY,
                CampaignStatus::ID_PENDING_APPROVAL,
                CampaignStatus::ID_PROCESSING
            ]
        )
            ->get()
            ->each(function (CampaignStatus $status) use ($advertiser) {
                $this->createTestCampaign([
                    'user_id'     => $advertiser->id,
                    'status_id'   => $status->id,
                    'external_id' => self::$externalId,
                ]);
            });

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');

        $this->mockAdminAccountSetStatusActionSuccess(self::$accountId, false, $admin->id);
        $this->mockCampaignPauseActionSuccess(self::$externalId, $admin->id);

        $this->mockAdminUserGetActionSuccess(self::$accountId, $advertiser->id);

        $response = $this->json(
            Request::METHOD_POST,
            route('api.advertisers.deactivate', ['advertiser' => $advertiser->id])
        );

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(['data']);

        $this->assertEquals($response->json('data.states'), [
            'can_deactivate'          => false,
            'can_activate'            => true,
            'can_update_account_type' => true,
        ]);

        $this->assertEquals(
            $response->json('data.message'),
            trans('advertiser::messages.deactivate.success')
        );

        // check user was deactivated
        $this->assertDatabaseHas('users', [
            'id'        => $advertiser->id,
            'status_id' => UserStatus::ID_INACTIVE,
        ]);

        // check advertiser notification
        $this->assertCount(1, $advertiser->notifications);

        /** @var Notification $notification */
        $notification = $advertiser->notifications->first();
        $this->assertEquals(AdvertiserDeactivated::class, $notification->type);
        $this->assertEquals($advertiser->id, $notification->notifiable_id);

        // check live campaigns paused
        /** @var CampaignRepository $campaignRepository */
        $campaignRepository = app(CampaignRepository::class);
        $liveCampaigns = $campaignRepository->findActiveCampaignsByUser($advertiser);
        $this->assertCount(0, $liveCampaigns);
    }

    /**
     * Test advertiser and read-only admin could not deactivate advertisers.
     *
     * @dataProvider rolesDataProvider
     *
     * @param string $state
     * @param string $guard
     * @param int    $code
     */
    public function testAdvertiserDeactivateFail(string $state, string $guard, int $code): void
    {
        $user = $this->createTestUser([], $state);
        $advertiser = $this->createTestAdvertiser();

        $this->actingAs($user, $guard);
        $response = $this->json(
            Request::METHOD_POST,
            route('api.advertisers.deactivate', ['advertiser' => $advertiser->id])
        );
        $response->assertStatus($code);
    }

    /**
     * Test admin cannot deactivate user which has Live campaigns.
     *
     * @dataProvider campaignStatusesDataProvider
     *
     * @param int $status
     */
    public function testAdvertiserDeactivateFailLiveCampaign(int $status): void
    {
        $advertiser = $this->createTestAdvertiser([
            'status_id'           => UserStatus::ID_ACTIVE,
            'account_external_id' => self::$accountId,
        ]);

        $this->createTestCampaign([
            'user_id'     => $advertiser->id,
            'status_id'   => $status,
            'external_id' => self::$externalId,
        ]);

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');

        $response = $this->json(
            Request::METHOD_POST,
            route('api.advertisers.deactivate', ['advertiser' => $advertiser->id])
        );
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * @return \Generator
     */
    public function rolesDataProvider(): \Generator
    {
        yield [
            Role::ADVERTISER,
            'web',
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            Role::ADMIN_READ_ONLY,
            'admin',
            Response::HTTP_FORBIDDEN,
        ];
    }

    /**
     * @return \Generator
     */
    public function campaignStatusesDataProvider(): \Generator
    {
        yield [CampaignStatus::ID_LIVE];

        yield [CampaignStatus::ID_READY];
    }

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(UserPermissionsTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(CampaignStatusesTableSeeder::class);
    }
}
