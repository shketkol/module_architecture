<?php

namespace Tests\Feature\Advertiser\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Advertiser\Notifications\Advertiser\AdvertiserActivated;
use Modules\Notification\Models\Notification;
use Modules\User\Models\Role;
use Modules\User\Models\Traits\CreateUser;
use Modules\User\Models\UserStatus;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserPermissionsTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\Traits\HaapiMocks\AdminAccountSetStatusMock;
use Tests\Traits\HaapiMocks\AdminUserGetMock;

class ActivateAdvertiserControllerTest extends TestCase
{
    use DatabaseTransactions,
        AdminAccountSetStatusMock,
        AdminUserGetMock,
        CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(UserPermissionsTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
    }

    /**
     * Test admin can activate advertisers.
     */
    public function testAdminActivate(): void
    {
        $advertiser = $this->createTestAdvertiser([
            'status_id'           => UserStatus::ID_INACTIVE,
            'account_external_id' => self::$accountId,
        ]);

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');

        $this->mockAdminAccountSetStatusActionSuccess(self::$accountId, true, $admin->id);
        $this->mockAdminUserGetActionSuccess($advertiser->external_id, $admin->id);

        $response = $this->json(
            Request::METHOD_POST,
            route('api.advertisers.activate', ['advertiser' => $advertiser->id])
        );

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(['data']);

        $this->assertEquals($response->json('data.states'), [
            'can_deactivate'          => true,
            'can_activate'            => false,
            'can_update_account_type' => true,
        ]);

        $this->assertEquals(
            $response->json('data.message'),
            trans('advertiser::messages.activate.success')
        );

        // check user was activated
        $this->assertDatabaseHas('users', [
            'id'        => $advertiser->id,
            'status_id' => UserStatus::ID_ACTIVE,
        ]);

        // check advertiser notification
        $this->assertCount(1, $advertiser->notifications);

        /** @var Notification $notification */
        $notification = $advertiser->notifications->first();
        $this->assertEquals(AdvertiserActivated::class, $notification->type);
        $this->assertEquals($advertiser->id, $notification->notifiable_id);
    }

    /**
     * Test advertiser and read-only admin could not activate advertisers.
     *
     * @dataProvider rolesDataProvider
     *
     * @param string $state
     * @param string $guard
     * @param int    $code
     */
    public function testAdvertiserActivateFail(string $state, string $guard, int $code): void
    {
        $user = $this->createTestUser([], $state);
        $advertiser = $this->createTestAdvertiser();

        $this->actingAs($user, $guard);
        $response = $this->json(
            Request::METHOD_POST,
            route('api.advertisers.activate', ['advertiser' => $advertiser->id])
        );
        $response->assertStatus($code);
    }

    /**
     * @return \Generator
     */
    public function rolesDataProvider(): \Generator
    {
        yield [
            Role::ADVERTISER,
            'web',
            Response::HTTP_FORBIDDEN,
        ];

        yield [
            Role::ADMIN_READ_ONLY,
            'admin',
            Response::HTTP_FORBIDDEN,
        ];
    }
}
