<?php

namespace Tests\Feature\Advertiser\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Models\UserStatus;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserPermissionsTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;
use Tests\Traits\HaapiMocks\UserGetMock;

class GetAdvertiserDetailsControllerTest extends TestCase
{
    use DatabaseTransactions, CreateUser, UserGetMock;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(UserPermissionsTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
    }

    /**
     * Test advertiser could not get advertisers details.
     */
    public function testAdvertiserGetDetails(): void
    {
        $advertiser1 = $this->createTestUser();
        $advertiser2 = $this->createTestUser();

        $this->actingAs($advertiser1);
        $response = $this->json(
            Request::METHOD_GET,
            route('api.advertisers.show', ['advertiser' => $advertiser2->id])
        );
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /**
     * Test admin can get advertisers details.
     */
    public function testAdminGetDetails(): void
    {
        $advertiser = $this->createTestAdvertiser(['status_id' => UserStatus::ID_ACTIVE]);
        $this->mockAdminUserGetActionSuccess($advertiser);
        $admin = $this->createTestReadOnlyAdmin();
        $this->actingAs($admin, 'admin');

        $response = $this->json(Request::METHOD_GET, route('api.advertisers.show', ['advertiser' => $advertiser->id]));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(['data']);
        $this->assertNotEmpty($response->json('data'));
        $this->assertNotEmpty($response->json('data.company.company_name'));
        $this->assertNotEmpty($response->json('data.status'));
    }
}
