<?php

namespace Tests\Feature\User\Actions;

use Modules\User\Actions\CompleteOnboardingAction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;

class CompleteOnboardingActionTest extends TestCase
{
    use DatabaseTransactions, CreateUser;

    /**
     * User onboarding test.
     *
     * @return void
     */
    public function testOnboarding(): void
    {
        $user = $this->createTestUser();

        $this->assertFalse($user->metaData->onboarded);

        /** @var CompleteOnboardingAction $onboardingAction */
        $onboardingAction = app(CompleteOnboardingAction::class);
        $onboardingAction->handle($user);
        $user->refresh();

        $this->assertTrue($user->metaData->onboarded);
    }
}
