<?php

namespace Tests\Feature\User\Actions;

use Modules\User\Actions\AssignRolesAction;
use Modules\User\Models\Role;
use Modules\User\Database\Seeders\RolesTableSeeder;
use Modules\User\Database\Seeders\UserStatusesTableSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;

class AssignRolesActionTest extends TestCase
{
    use DatabaseTransactions, CreateUser;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(UserStatusesTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
    }

    /**
     * User assign role test.
     *
     * @return void
     */
    public function testAssign(): void
    {
        $user = $this->createTestUser();
        $this->assertCount(0, $user->roles);

        /** @var AssignRolesAction $assignRolesAction */
        $assignRolesAction = app(AssignRolesAction::class);
        $assignRolesAction->handle($user, [Role::ID_ADVERTISER]);

        $this->assertCount(1, $user->roles);
        $this->assertEquals(Role::ADVERTISER, $user->roles()->first()->name);
    }
}
