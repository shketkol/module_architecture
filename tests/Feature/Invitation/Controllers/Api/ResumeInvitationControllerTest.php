<?php

namespace Tests\Feature\Invitation\Controllers\Api;

use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Models\Traits\CreateInvite;
use Modules\Invitation\Repositories\InvitationRepository;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class ResumeInvitationControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateInvite,
        CreateUser;

    /**
     * @var InvitationRepository
     */
    private $repository;

    /**
     * @var string
     */
    private $advertiserEmail;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $faker = app(Generator::class);
        $this->advertiserEmail = $faker->unique()->email;

        $this->createTestInviteCode([
            'email'     => $this->advertiserEmail,
            'code'      => '8XtYBR3v',
            'status_id' => InvitationStatus::ID_REVOKED,
        ]);

        $this->repository = app(InvitationRepository::class);

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');
    }

    /**
     * Test resume invitation code.
     *
     * @return void
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function testResumeInvite(): void
    {
        $invite = $this->repository->byEmail($this->advertiserEmail)->first();

        $response = $this->json(Request::METHOD_POST, route('api.invitations.resume', [
            'invitation' => $invite->id,
        ]));

        $response->assertStatus(Response::HTTP_OK);
        $data = $response->json('data');
        $this->assertEquals(InvitationStatus::INVITED, Arr::get($data, 'invitation_status'));
    }
}
