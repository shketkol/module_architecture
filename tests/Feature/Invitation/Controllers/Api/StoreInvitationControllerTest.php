<?php

namespace Tests\Feature\Invitation\Controllers\Api;

use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Modules\Invitation\Models\InvitationStatus;
use Modules\Invitation\Models\Traits\CreateInvite;
use Modules\User\Models\AccountType;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class StoreInvitationControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateInvite,
        CreateUser;

    /**
     * @var string
     */
    private $advertiserEmail;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $faker = app(Generator::class);
        $this->advertiserEmail = $faker->unique()->email;

        $this->createTestInviteCode([
            'email'     => 'bulk@advertiser.com',
            'code'      => '8XtYBR3v',
            'status_id' => InvitationStatus::ID_INVITED,
        ]);

        $admin = $this->createTestAdmin();
        $this->actingAs($admin, 'admin');
    }

    /**
     * Test store invite.
     *
     * @dataProvider namesDataProvider
     *
     * @param array $data
     *
     * @return void
     */
    public function testStoreInviteCode($data): void
    {
        $payload = Arr::get($data, 'payload');
        $expectedStatusCode = Arr::get($data, 'expected_result.code');

        Arr::set($payload, 'email', $this->advertiserEmail);

        $response = $this->json(Request::METHOD_POST, route('api.invitations.store'), $payload);

        $response->assertStatus($expectedStatusCode);
    }

    /**
     * @return \Generator
     */
    public function namesDataProvider(): \Generator
    {
        // Valid data
        yield [
            [
                'payload'         => [
                    'email'                 => 'store@advertiser.com',
                    'code'                  => '8XtYBR1',
                    'account_type_category' => AccountType::ID_COMMON,
                ],
                'expected_result' => [
                    'code' => Response::HTTP_OK,
                ],
            ],
        ];

        // Invalid data
        yield [
            [
                'payload'         => [
                    'email' => 'bulk@advertiser.com',
                    'code'  => '8XtYBR1',
                ],
                'expected_result' => [
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ],
            ],
        ];
    }
}
