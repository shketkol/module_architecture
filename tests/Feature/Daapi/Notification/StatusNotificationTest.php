<?php

namespace Tests\Feature\Daapi\Notification;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Modules\Daapi\Actions\Notification\StatusHandler;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\AdminUserGetMock;

abstract class StatusNotificationTest extends TestCase
{
    use DatabaseTransactions, WithoutMiddleware, AdminSignInMock, AdminUserGetMock, CreateUser;

    /**
     * @var StatusHandler
     */
    protected $entityHandlerAction;

    /**
     * Setup environment
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->entityHandlerAction = app(StatusHandler::class);
    }

    /**
     * @param string $externalId
     * @param string $status
     *
     * @return array
     */
    abstract public function createRequest(string $externalId, string $status): array;

    /**
     * @param array $attributes
     *
     * @return Model
     */
    abstract public function createModel(array $attributes): Model;

    /**
     * Test status not found
     */
    public function testStatusNotFound(): void
    {
        $this->markTestSkipped('Temporary disabled');
        $request = $this->createRequest(Str::uuid()->toString(), 'DOES NOT MATTER');
        $response = $this->json(Request::METHOD_POST, route('notification.status'), $request);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $this->assertEquals(Arr::get($request, 'request'), $response->json('request'));

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->json('response.statusCode'));
    }

    /**
     * @dataProvider statusDataProviderForDraft
     *
     * @param array $data
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function check(array $data): void
    {
        Notification::fake();
        $fromStatusId = Arr::get($data, 'from_status_id');
        $toStatus = Arr::get($data, 'to_status');
        $expectedStatus = Arr::get($data, 'expected_result.statusCode');
        $expectedField = Arr::get($data, 'expected_result.payload.field');
        $expectedMessage = Arr::get($data, 'expected_result.payload.message');

        $model = $this->createModel(['status_id' => $fromStatusId]);

        $this->mockUserHaapi($model);

        $response = $this->json(
            Request::METHOD_POST,
            route('notification.status'),
            $this->createRequest($model->getExternalId(), $toStatus)
        );

        $response->assertStatus($expectedStatus);

        $this->assertEquals($expectedStatus, $response->json('response.statusCode'));
        $this->assertStringStartsWith($expectedMessage, $response->json('response.payload.' . $expectedField));
    }

    /**
     * @param $model
     *
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    protected function mockUserHaapi($model): void
    {
        $admin = $this->createTechnicalAdmin();

        $this->mockAdminSignInActionSuccess();
        $this->mockAdminUserGetActionSuccess($model->external_id, $admin->id);
    }
}
