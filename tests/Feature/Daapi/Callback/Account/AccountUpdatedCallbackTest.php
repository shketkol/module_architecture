<?php

namespace Tests\Feature\Daapi\Callback\Account;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class AccountUpdatedCallbackTest extends TestCase
{
    use DatabaseTransactions, CreateUser;

    /**
     * Test with correct body.
     *
     * @dataProvider accountUpdatedSuccessDataProvider
     *
     * @param array $payload Request body.
     */
    public function testAccountUpdatedCallbackSuccess(array $payload): void
    {
        $this->createTestUser([
            'company_name'        => 'Charlie Test MD',
            'account_external_id' => '8111111j-888y-000t-afaf-82222222j',
        ]);

        $response = $this->json(Request::METHOD_POST, Url::signedRoute('callbacks.account.update'), $payload);

        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @return \Generator
     */
    public function accountUpdatedSuccessDataProvider(): \Generator
    {
        yield [
            [
                "haapi" => [
                    "request"  => [
                        "id"           => "c18d868c-98c2-4578-9c29-6004cfa5541d",
                        "system"       => "danads-api",
                        "haapiVersion" => "1",
                        "type"         => "account/update",
                        "preSharedKey" => "f2e64f90-624b-4db5-aecd-cf23b3772822",
                        "userToken"    => null,
                        "callbackUrl"  => "http://dummy.url",
                        "payload"      => [
                            'account' => [
                                "accountId"      => "8111111j-888y-000t-afaf-82222222j",
                                "companyAddress" => [
                                    "line1"   => "1720 Test Avenue",
                                    "line2"   => "",
                                    "state"   => "AZ",
                                    "city"    => "Test City",
                                    "country" => "US",
                                    "zipcode" => "90404",
                                ],
                                "phoneNumber"    => "+1 (928) 453-1800",
                            ],
                        ],
                    ],
                    "response" => [
                        "id"         => "c621150c-0df3-4b58-ae00-924bb364b34d",
                        "system"     => "haapi",
                        "statusCode" => "0",
                        "statusMsg"  => "OK",
                        "payload"    => [
                            'message' => "Success",
                            "account" => [
                                "id"             => "8111111j-888y-000t-afaf-82222222j",
                                "companyName"    => "Charlie Test Changed MD",
                                "accountType"    => "ADVERTISER",
                                "phoneNumber"    => "+1 (928) 453-1800",
                                "companyAddress" => [
                                    "line1"   => "1720 Test Avenue",
                                    "line2"   => "",
                                    "state"   => "AZ",
                                    "city"    => "Test City",
                                    "country" => "US",
                                    "zipcode" => "90404",
                                ],
                                "accountStatus"  => "ACTIVE",
                            ],
                        ],
                        "received"   => "2019-07-22T10:46:53Z",
                        "duration"   => 74451,
                    ],
                ],
            ],
        ];
    }
}
