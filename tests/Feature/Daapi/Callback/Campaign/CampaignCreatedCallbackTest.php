<?php

namespace Tests\Feature\Daapi\Callback\Campaign;

use Carbon\Carbon;
use DateTime;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\URL;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Payment\Models\PaymentMethod;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;

class CampaignCreatedCallbackTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCampaign;

    private const EXT_ACCOUNT_ID = '8444444j-888y-000t-afaf-8444444j';
    private const EXT_CAMPAIGN_ID = 'u66666662-y333y-y5555y-i6666666';
    private const ORDER_ID = '666666';

    /**
     * Test with correct body.
     *
     * @dataProvider campaignCreatedSuccessDataProvider
     *
     * @param array $expectedData
     */
    public function testCampaignCreatedCallbackSuccess(array $expectedData): void
    {
        $user = $this->createTestUser([
            'account_external_id' => self::EXT_ACCOUNT_ID,
        ]);
        $campaign = $this->createTestCampaign([
            'user_id'   => $user->getKey(),
            'status_id' => CampaignStatus::ID_PROCESSING,
        ]);

        PaymentMethod::factory()->create([
            'user_id' => $user->getKey(),
        ]);

        $payload = $this->preparePayload($expectedData['statusEnum']);
        Arr::set($payload, 'haapi.response.payload.campaign.sourceId', (string) $campaign->id);
        $response = $this->json(Request::METHOD_POST, Url::signedRoute('callbacks.campaign.create'), $payload);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('campaigns', [
            'id'          => $campaign->getKey(),
            'status_id'   => $expectedData['statusId'],
            'external_id' => $expectedData['externalId'],
            'order_id'    => $expectedData['orderId'],
        ]);
    }

    /**
     * @param string $statusEnum
     *
     * @return array
     */
    private function preparePayload(string $statusEnum): array
    {
        return
            [
                "haapi" => [
                    "request"  => [
                        "id"           => "c18d868c-98c2-4578-9c29-6004cfa5541d",
                        "system"       => "danads-api",
                        "haapiVersion" => "1",
                        "type"         => "campaign/create",
                        "preSharedKey" => "f2e64f90-624b-4db5-aecd-cf23b3772822",
                        "userToken"    => null,
                        "callbackUrl"  => "http://dummy.url",
                        "payload"      => [
                            "campaign" => [
                                "sourceId"     => "to set",
                                "name"         => "33",
                                "advertiserId" => "8444444j-888y-000t-afaf-8444444j",
                                "lineItems"    => [
                                    [
                                        "sourceId"     => null,
                                        "startDate"    => Carbon::now()->addDays(3)->format(DateTime::ISO8601),
                                        "endDate"      => Carbon::now()->addMonth()->format(DateTime::ISO8601),
                                        "productId"    => "bb540ba2-b990-4c37-befd-c742bc6812d0",
                                        "targets"      => [],
                                        "unitCost"     => 32,
                                        "costModel"    => "CPM",
                                        "quantity"     => 3200000,
                                        "quantityType" => "IMPRESSIONS",
                                        "creative"     => null,
                                    ],
                                ],
                                "promoCodeName"   => null,
                            ],
                        ],
                    ],
                    "response" => [
                        "id"         => "c621150c-0df3-4b58-ae00-924bb364b34d",
                        "system"     => "haapi",
                        "statusCode" => "0",
                        "statusMsg"  => "OK",
                        "payload"    => [
                            "campaign" => [
                                "sourceId"         => "to set",
                                "id"               => self::EXT_CAMPAIGN_ID,
                                "operativeOrderId" => self::ORDER_ID,
                                "name"             => "Test create callback campaign",
                                "advertiserId"     => self::EXT_ACCOUNT_ID,
                                "agencyId"         => self::EXT_ACCOUNT_ID,
                                "status"           => $statusEnum,
                                "impressions"      => 10000,
                                "promoCode"        => null,
                                "lineItems"        => [
                                    [
                                        "sourceId"             => null,
                                        'crative'              => null,
                                        "id"                   => "u66666662-y333y-y5555y-i6666666",
                                        "startDate"            => Carbon::now()->addDays(3)->format(DateTime::ISO8601),
                                        "endDate"              => Carbon::now()->addMonth()->format(DateTime::ISO8601),
                                        "productId"            => "bb540ba2-b990-4c37-befd-c742bc6812d0",
                                        "targets"              => [],
                                        "unitCost"             => 32,
                                        "costModel"            => "CPM",
                                        "quantity"             => 3200000,
                                        "quantityType"         => "IMPRESSIONS",
                                        "deliveredImpressions" => 0,
                                        "status"               => "SUBMITTED",
                                    ],
                                ],
                                "budget" => 10000,
                                "discountedBudget" => null,
                                "displayBudget" => 999.5,
                                "displayDiscountedBudget" => null
                            ],
                        ],
                        "received"   => "2019-07-22T10:46:53Z",
                        "duration"   => 74451,
                    ],
                ],
            ];
    }

    /**
     * @return \Generator
     */
    public function campaignCreatedSuccessDataProvider(): \Generator
    {
        yield [
            [
                'externalId' => self::EXT_CAMPAIGN_ID,
                'statusId'   => CampaignStatus::ID_PENDING_APPROVAL,
                'statusEnum' => 'PENDING APPROVAL',
                'orderId'    => self::ORDER_ID,
            ],
        ];
        yield [
            [
                'externalId' => self::EXT_CAMPAIGN_ID,
                'statusId'   => CampaignStatus::ID_READY,
                'statusEnum' => 'READY',
                'orderId'    => self::ORDER_ID,
            ],
        ];
    }
}
