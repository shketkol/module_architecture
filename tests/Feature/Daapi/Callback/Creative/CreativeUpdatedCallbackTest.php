<?php

namespace Tests\Feature\Daapi\Callback\Creative;

use Carbon\Carbon;
use DateTime;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\Traits\CreateCampaign;
use Modules\Creative\Models\CreativeStatus;
use Modules\Creative\Models\Traits\CreateCreative;
use Modules\User\Models\Traits\CreateUser;
use Tests\TestCase;
use Tests\Traits\HaapiMocks\AdminDataGetMock;
use Tests\Traits\HaapiMocks\AdminSignInMock;
use Tests\Traits\HaapiMocks\AdminUserGetMock;
use Tests\Traits\HaapiMocks\CampaignGetMock;
use Tests\Traits\HaapiMocks\CreativeGetMock;
use Tests\Traits\HaapiMocks\SelfDataGetMock;

class CreativeUpdatedCallbackTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CreateCreative,
        CreateCampaign,
        AdminSignInMock,
        AdminDataGetMock,
        AdminUserGetMock,
        CampaignGetMock,
        CreativeGetMock,
        SelfDataGetMock;

    private const EXT_ACCOUNT_ID  = '8444444j-888y-000t-afaf-8444444j';
    private const EXT_CAMPAIGN_ID = 'u66666662-y333y-y5555y-i6666666';
    private const EXT_CREATIVE_ID = 'u77777772-y333y-y5555y-i7777777';

    /**
     * Test with correct body.
     *
     * @dataProvider creativeUpdatedSuccessDataProvider
     *
     * @param array $expectedData
     *
     * @throws \Modules\User\Exceptions\AdminNotFoundException
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function testCreativeUpdatedCallbackSuccess(array $expectedData): void
    {
        $user = $this->createTestUser([
            'account_external_id' => self::EXT_ACCOUNT_ID,
        ]);
        $campaign = $this->createTestCampaign([
            'user_id'     => $user->getKey(),
            'external_id' => self::EXT_CAMPAIGN_ID,
            'status_id'   => CampaignStatus::ID_PENDING_APPROVAL,
        ]);
        $creative = $this->createTestCreative([
            'user_id'   => $user->getKey(),
            'status_id' => CreativeStatus::ID_PROCESSING,
        ]);
        $campaign->creatives()->attach($creative);
        $payload = $this->preparePayload($expectedData['statusEnum']);

        $this->mockCampaignGetActionSuccess($campaign);
        $this->mockCreativeGetActionSuccess($creative);

        $admin = $this->createTechnicalAdmin();

        /** Mock Act as Admin */
        $this->mockAdminSignInActionSuccess();
        $this->mockSelfDataGetActionSuccess();
        $this->mockAdminDataGetActionSuccess();
        $this->mockAdminUserGetActionSuccess(self::EXT_ACCOUNT_ID, $admin->id);
        $response = $this->json(Request::METHOD_POST, Url::signedRoute('callbacks.creative.update'), $payload);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('creatives', [
            'id'          => $creative->getKey(),
            'status_id'   => $expectedData['statusId'],
            'external_id' => $expectedData['externalId'],
        ]);
    }

    /**
     * @param string $statusEnum
     *
     * @return array
     */
    private function preparePayload(string $statusEnum): array
    {
        return
            [
                "haapi" => [
                    "request"  => [
                        "id"           => "c18d868c-98c2-4578-9c29-6004cfa5541d",
                        "system"       => "danads-api",
                        "haapiVersion" => "1",
                        "type"         => "creative/update",
                        "preSharedKey" => "f2e64f90-624b-4db5-aecd-cf23b3772822",
                        "userToken"    => null,
                        "callbackUrl"  => "http://dummy.url",
                        "payload"      => [
                            "campaignId" => self::EXT_CAMPAIGN_ID,
                            "creative"   => [
                                "creativeCreation" => [
                                    "accountId"     => self::EXT_ACCOUNT_ID,
                                    "fileName"      => "Test Creative",
                                    "sourceUrl"     => "dummy.cl.url",
                                    "contentLength" => 123456,
                                ],
                            ],
                        ],
                    ],
                    "response" => [
                        "id"         => "c621150c-0df3-4b58-ae00-924bb364b34d",
                        "system"     => "haapi",
                        "statusCode" => "0",
                        "statusMsg"  => "OK",
                        "payload"    => [
                            "campaign" => [
                                "id"       => self::EXT_CAMPAIGN_ID,
                                "creative" => [
                                    "id"          => self::EXT_CREATIVE_ID,
                                    "fileName"    => "Test Creative",
                                    "extension"   => ".mov",
                                    "size"        => "123",
                                    "duration"    => "111",
                                    "createdAt"   => Carbon::now()->subDays(3)->format(DateTime::ISO8601),
                                    "status"      => $statusEnum,
                                    "creativeUrl" => "dummy.url",
                                    "accountId"   => self::EXT_ACCOUNT_ID,
                                ],
                            ],
                        ],
                        "received"   => "2019-07-22T10:46:53Z",
                        "duration"   => 74451,
                    ],
                ],
            ];
    }

    /**
     * @return \Generator
     */
    public function creativeUpdatedSuccessDataProvider(): \Generator
    {
        yield [
            [
                'externalId' => self::EXT_CREATIVE_ID,
                'statusId'   => CreativeStatus::ID_APPROVED,
                'statusEnum' => 'APPROVED',
            ],
        ];
        yield [
            [
                'externalId' => self::EXT_CREATIVE_ID,
                'statusId'   => CreativeStatus::ID_PENDING_APPROVAL,
                'statusEnum' => 'UNDER_REVIEW',
            ],
        ];
        yield [
            [
                'externalId' => self::EXT_CREATIVE_ID,
                'statusId'   => CreativeStatus::ID_REJECTED,
                'statusEnum' => 'REJECTED',
            ],
        ];
    }
}
