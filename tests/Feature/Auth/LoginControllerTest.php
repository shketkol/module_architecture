<?php

namespace Tests\Feature\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Modules\Auth\Events\UserLogin;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Models\Traits\CreateUser;
use Tests\Traits\Cognito\CognitoJwtValidationMock;
use Tests\Traits\Cognito\CognitoTokensHelper;
use Tests\Traits\HaapiMocks\SelfDataGetMock;

class LoginControllerTest extends TestCase
{
    use DatabaseTransactions,
        CreateUser,
        CognitoTokensHelper,
        CognitoJwtValidationMock,
        SelfDataGetMock;

    private const TOKEN_SESSION = 'session-token';
    private const EXT_ID = 'asdf-asdf-1234-qwer-5678';

    /**
     * Test with correct body.
     *
     * @dataProvider loginSuccessDataProvider
     *
     * @param array $payload Request body.
     */
    public function testLoginSuccess(array $payload): void
    {
        Event::fake();
        $this->mockUserGetByTokenActionSuccess($payload['email']);
        $this->mockJwtValidation();

        $response = $this->json(Request::METHOD_POST, route('api.auth.login'), $payload);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertNotEmpty($response->json('redirect'));

        Event::assertDispatched(UserLogin::class, function (UserLogin $event) {
            return $event->user->external_id === self::EXT_ID;
        });
    }

    //TODO Add failed scenarios.

    /**
     * Data provider with correct data and expected login results.
     *
     * @return \Generator
     */
    public function loginSuccessDataProvider(): \Generator
    {
        yield [
            [
                'token'        => $this->makeCognitoAccessToken(),
                'email'        => 'charlie.sheen@advertiser.com',
                'refreshToken' => $this->makeCognitoRefreshToken(),
            ],
        ];
    }
}
