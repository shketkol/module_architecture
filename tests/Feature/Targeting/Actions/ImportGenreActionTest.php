<?php

namespace Tests\Feature\Targeting\Actions;

use Illuminate\Support\Arr;
use Modules\Haapi\DataTransferObjects\Targeting\TargetingValuesCollection;
use Modules\Targeting\Actions\Genre\ImportGenresAction;
use Modules\Targeting\Models\Traits\ScoutIndex;
use Modules\Targeting\Repositories\Contracts\GenreRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImportGenreActionTest extends TestCase
{
    use DatabaseTransactions, ScoutIndex;

    /**
     * Test import from mocked HAAPI to DB.
     *
     * @dataProvider genresDataProvider
     *
     * @param array $data
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function testImport(array $data): void
    {
        /** @var GenreRepository|\Modules\Targeting\Repositories\GenreRepository $repository */
        $repository = app(GenreRepository::class);

        $genresBefore = $repository->count();

        /** @var ImportGenresAction $import */
        $import = app(ImportGenresAction::class);
        $collection = TargetingValuesCollection::create([$data]);
        $import->handle($collection);

        $this->assertDatabaseHas('genres', [
            'external_id' => Arr::get($data, 'id'),
            'name'        => Arr::get($data, 'name'),
        ]);

        $genresAfter = $repository->count();

        $this->assertEquals($genresBefore + 1, $genresAfter);
    }

    /**
     * @return \Generator
     */
    public function genresDataProvider(): \Generator
    {
        yield [
            [
                'id'      => '4c4d9217-0777-4e44-9098-e5006b5fe2e2',
                'name'    => 'Test Adventure/80s Action',
                'visible' => true,
            ],
        ];

        yield [
            [
                'id'      => 'd9cf5bd8-9e61-46cc-a8b6-885d607654b5',
                'name'    => 'Test Adventure/Espionage',
                'visible' => true,
            ],
        ];

        yield [
            [
                'id'      => '819a742c-60af-404c-b29e-9af3001432da',
                'name'    => 'Test Adventure/Martial Arts',
                'visible' => true,
            ],
        ];
    }
}
