<?php

namespace Tests\Haapi\Creative;

use Modules\Haapi\Actions\Creative\Contracts\CampaignCreativeUpdate;
use Modules\Haapi\DataTransferObjects\Creative\CampaignCreativeUpdateParams;
use Modules\Haapi\Exceptions\ObjectNotFoundException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class CampaignCreativeUpdateTest extends HaapiTestCase
{
    /**
     * @var CampaignCreativeUpdate
     */
    protected $update;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->update = app(CampaignCreativeUpdate::class);
    }

    /**
     * Test creative success
     */
    public function testCreativeUploadSuccess(): void
    {
        $params = new CampaignCreativeUpdateParams([
            'campaignId' => self::UPDATE_CAMPAIGN_ID,
            'creativeId' => self::UPDATE_CREATIVE_ID,
        ]);
        $response = $this->update->handle($params, $this->userHaapi->id);

        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test creative detach success
     *
     * @depends testCreativeUploadSuccess
     */
    public function testCreativeDetachSuccess(): void
    {
        $params = new CampaignCreativeUpdateParams([
            'campaignId' => self::UPDATE_CAMPAIGN_ID,
            'creativeId' => self::UPDATE_CREATIVE_ID,
        ]);

        $response = $this->update->handle($params, $this->userHaapi->id);

        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test creative user not authorized
     */
    public function testCreativeUpdateUploadNotAuthorized(): void
    {
        $params = new CampaignCreativeUpdateParams([
            'campaignId' => self::UPDATE_CAMPAIGN_ID,
            'creativeId' => self::UPDATE_CREATIVE_ID,
        ]);
        $this->expectException(UnauthorizedException::class);
        $this->update->handle($params, 0);
    }

    /**
     * Test creative account or campaign id invalid
     */
    public function testCreativeUpdateUploadIdInvalid(): void
    {
        $this->expectException(ObjectNotFoundException::class);
        $params = new CampaignCreativeUpdateParams([
            'campaignId' => '07e9f9fa-cfeb-11e9-bb65-2a2ae2dbcce4',
            'creativeId' => 'cc3d3e3d-8972-47c5-a68c-c58ee6609f65',
        ]);
        $this->update->handle($params, $this->userHaapi->id);
    }
}
