<?php

namespace Tests\Haapi\Creative;

use Illuminate\Support\Arr;
use Modules\Haapi\Actions\Creative\Contracts\CampaignCreativeUpload;
use Modules\Haapi\DataTransferObjects\Creative\CampaignCreativeUploadParams;
use Modules\Haapi\Exceptions\InvalidRequestException;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class CampaignCreativeUploadTest extends HaapiTestCase
{
    /**
     * @var CampaignCreativeUpload
     */
    protected $upload;

    /**
     * @var CampaignCreativeUploadParams
     */
    protected $params;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->upload = app(CampaignCreativeUpload::class);
    }

    /**
     * Test creative success
     */
    public function testCreativeUploadSuccess(): void
    {
        $this->setParams();
        $response = $this->upload->handle($this->params, $this->userHaapi->id);

        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test creative missing required field
     */
    public function testCreativeUploadMissingField(): void
    {
        $this->expectException(InvalidRequestException::class);
        $this->setParams([
            'fileName'      => '',
            'sourceUrl'     => '',
            'contentLength' => 0,
        ]);
        $this->upload->handle($this->params, $this->userHaapi->id);
    }

    /**
     * Test creative user not authorized
     */
    public function testCreativeUploadNotAuthorized(): void
    {
        $this->markTestSkipped('Skipped due to HD-364');
        $this->expectException(UnauthorizedException::class);

        $this->setParams();
        $this->upload->handle($this->params, 0);
    }

    /**
     * Test creative account or campaign id invalid
     */
    public function testCreativeUploadIdInvalid(): void
    {
        $this->expectException(InvalidRequestException::class);

        $this->setParams([
            'campaignId' => '',
        ]);
        $this->upload->handle($this->params, $this->userHaapi->id);
    }

    /**
     * @param array $params
     */
    protected function setParams(array $params = []): void
    {
        $accountId = Arr::get($params, 'accountId', self::ADV_ACCOUNT_ID);
        $campaignId = Arr::get($params, 'campaignId', self::STATUS_CAMPAIGN_ID);
        $fileName = Arr::get($params, 'fileName', 'test.jpg');
        $defaultSourceUrl =
            'https://s3' .
            config('filesystems.disks.s3.region') .
            '.amazonaws.com/' .
            config('filesystems.disks.s3.bucket') .
            '/' . $fileName;
        $sourceUrl = Arr::get($params, 'sourceUrl', $defaultSourceUrl);
        $contentLength = Arr::get($params, 'contentLength', 100);
        $this->params = new CampaignCreativeUploadParams(compact([
            'accountId',
            'campaignId',
            'fileName',
            'sourceUrl',
            'contentLength',
        ]));
    }
}
