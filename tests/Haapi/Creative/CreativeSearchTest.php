<?php

namespace Tests\Haapi\Creative;

use Modules\Haapi\Actions\Creative\Contracts\CreativeSearch;
use Modules\Haapi\DataTransferObjects\Creative\CreativeSearchParam;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class CreativeSearchTest extends HaapiTestCase
{
    /**
     * @var CreativeSearch
     */
    protected $creativeSearch;

    /**
     * @var CreativeSearchParam
     */
    protected $creativeSearchParam;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->creativeSearchParam = new CreativeSearchParam(['accountId' => self::ADV_ACCOUNT_ID]);
        $this->creativeSearch = app(CreativeSearch::class);
    }

    public function testCreativeSearchSuccess()
    {
        $response = $this->creativeSearch->handle($this->creativeSearchParam, $this->userHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test creative user not authorized
     */
    public function testCreativeSearchNotAuthorized()
    {
        $this->expectException(UnauthorizedException::class);
        $this->creativeSearch->handle($this->creativeSearchParam, 0);
    }
}
