<?php

namespace Tests\Haapi\Account;

use Modules\Daapi\DataTransferObjects\Types\AccountData;
use Modules\Haapi\Actions\Account\Contracts\AccountGet;
use Tests\Haapi\HaapiTestCase;

class AccountGetTest extends HaapiTestCase
{
    /**
     * @var AccountGet
     */
    protected $action;

    /**
     * Existing user id
     *
     * @var
     */
    protected $existingUserId;

    /**
     * Account id in Hulu
     *
     * @var string, UUID v4
     */
    protected $accountId;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->action = app(AccountGet::class);
        $this->accountId = self::ADV_ACCOUNT_ID;
    }

    /**
     *  Test get an account successfully
     */
    public function testGetAccountSuccess(): void
    {
        $response = $this->action->handle($this->accountId, $this->userHaapi->id);
        $this->assertInstanceOf(AccountData::class, $response);
        $this->assertEquals($response->externalId, $this->accountId);
    }
}
