<?php

namespace Tests\Haapi\Account;

use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Haapi\Actions\User\Contracts\UserGet;
use Tests\Haapi\HaapiTestCase;

/**
 * Class UserGetTest
 *
 * @package Modules\Haapi\tests\Integration\Account
 */
class UserGetTest extends HaapiTestCase
{
    /**
     * @var UserGet
     */
    protected $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(UserGet::class);
    }

    public function testGetUserSuccess()
    {
        $response = $this->action->handle($this->userHaapi->id);
        $this->assertInstanceOf(UserData::class, $response);
    }
}
