<?php

namespace Tests\Haapi\Account;

use Carbon\Carbon;
use Modules\Haapi\Actions\Account\Contracts\AccountSignUp;
use Modules\Haapi\Exceptions\ConflictException;
use Modules\User\DataTransferObjects\UserData;
use Modules\User\Models\Traits\CreateUser;
use Tests\Haapi\HaapiTestCase;

/**
 * Class AccountSignUpTest
 *
 * @package Modules\Haapi\tests\Integration\Account
 */
class AccountSignUpTest extends HaapiTestCase
{
    use CreateUser;

    /**
     * Sign up action
     *
     * @var AccountSignUp
     */
    protected $actionSignUp;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->actionSignUp = app(AccountSignUp::class);
    }

    /**
     * Sign Up user successfully
     *
     * @return UserData
     */
    public function testSignUpSuccess(): UserData
    {
        $data = $this->prepareData();
        $user = $this->createTestUser([
            'company_name' => $data->company->name,
        ]);
        $user->save();
        $response = $this->actionSignUp->handle($data);
        $this->assertEquals('0', $response->getStatusCode());

        return $data;
    }

    /**
     * Sign Up user failed
     *
     * @depends testSignUpSuccess
     *
     * @param UserData $data
     */
    public function testDuplicateEmail(UserData $data): void
    {
        $this->markTestSkipped('Unskip when HD-363 is ready');
        $this->expectException(ConflictException::class);
        $this->actionSignUp->handle($data);
    }

    /**
     * @return UserData
     */
    private function prepareData(): UserData
    {
        $data = [
            'email'          => 'integration.testing.adv' . Carbon::now()->timestamp . '@testing.com',
            'firstName'      => 'Integration Testing',
            'lastName'       => 'Admin',
            'companyName'    => 'Integration Testing Inc.',
            'phoneNumber'    => '+1 (310) 824-5847',
            'companyAddress' => [
                'line1'   => '1228 Perrysville Avenue',
                'line2'   => '',
                'city'    => 'Danville',
                'state'   => 'IL',
                'country' => 'US',
                'zipcode' => '61832',
            ],
        ];
        return UserData::fromPayload($data);
    }
}
