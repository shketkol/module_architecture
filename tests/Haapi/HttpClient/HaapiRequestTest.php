<?php

namespace Tests\Haapi\HttpClient;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Modules\Haapi\HttpClient\Builders\HaapiRequestBuilder;
use Tests\Haapi\HaapiTestCase;

/**
 * Class HaapiRequestTest
 *
 * @package Modules\Haapi\tests\Unit\Models
 */
class HaapiRequestTest extends HaapiTestCase
{

    /**
     * @var HaapiRequestBuilder
     */
    protected $builder;

    /**
     * Generated JWT preshared key
     */
    const PRESHARED_KEY = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJkYW5hZHMiLCJpYXQiOjE1Njc0MjcyMzh9.Sct2AITjUj1cPpZ0AOqqbXetI0TthR3E3OZmhBPXchY';

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->builder = app(HaapiRequestBuilder::class);
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     */
    public function testMakeMethod()
    {
        if (config('haapi.jwt.enable')) {
            Cache::shouldReceive('get')
                ->once()
                ->with('jwt-token')
                ->andReturn(self::PRESHARED_KEY);

            $ecpectedHaapiRequestData = $this->ecpectedHaapiRequestData(self::PRESHARED_KEY);
        } else {
            $ecpectedHaapiRequestData = $this->ecpectedHaapiRequestData(config('haapi.shared_key'));
        }

        $request = $this->builder->make(
            'some-type',
            [
                'key' => 'value'
            ]
        );
        $request->setId('e65982ce-66fb-4f6e-8e72-a8372a0f185d');
        $request->setHaapiVersion('1');

        $actual = $request->toArray();
        Arr::set($actual, 'haapi.request.id', Arr::get($ecpectedHaapiRequestData, 'haapi.request.id'));

        $this->assertEquals($ecpectedHaapiRequestData, $actual);
    }

    /**
     * Test adding token to a Haapi request
     *
     * @throws \Exception
     */
    public function testAddingToken()
    {
        $token = 'e65982ce-66fb-4f6e-8e72-a8372a0f185d';
        $this->builder->setToken($token);

        $requestBody = Arr::get(
            $this->builder->make(
                'some-type',
                ['key' => 'value']
            )->toArray(),
            'haapi.request'
        );
        $this->assertArrayHasKey('userToken', $requestBody);
        $this->assertEquals(Arr::get($requestBody, 'userToken'), $token);
    }

    /**
     * Test adding callback to a HaapiRequest
     *
     * @throws \Exception
     */
    public function testAddingCallBack()
    {
        $callback = 'https://exampple.com/callback';
        $this->builder->setCallbackUrl($callback);

        $requestBody = Arr::get(
            $this->builder->make(
                'some-type',
                ['key' => 'value']
            )->toArray(),
            'haapi.request'
        );
        $this->assertArrayHasKey('callbackUrl', $requestBody);
        $this->assertEquals(Arr::get($requestBody, 'callbackUrl'), $callback);
    }

    /**
     * ExpectedDate for HaapiRequest
     *
     * @param string $sharedKey
     *
     * @return array
     */
    public static function ecpectedHaapiRequestData(string $sharedKey): array
    {
        return [
            'haapi' =>  [
                'request' => [
                    'id' => 'e65982ce-66fb-4f6e-8e72-a8372a0f185d',
                    'system' => 'danads-api',
                    'haapiVersion' => '1',
                    'type' => 'some-type',
                    'preSharedKey' => $sharedKey,
                    'payload' => [
                        'key' => 'value'
                    ]
                ]
            ]
        ];
    }
}
