<?php

namespace Tests\Haapi\Helpers;

use Modules\Haapi\Actions\BaseAction;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;
use Modules\Haapi\Services\Contracts\RequestService;
use Modules\Haapi\Services\TokenService;

/**
 * This must be used for HAAPI integration tests ONLY.
 */
class TestUserSignIn extends BaseAction
{
    /**
     * Endpoint type
     */
    const REQUEST_TYPE = 'client/authorize';

    /**
     * Request service
     *
     * @var RequestService
     */
    private $requestService;

    /**
     * Token service
     *
     * @var TokenService
     */
    private $tokenService;

    /**
     * UserSignIn constructor.
     *
     * @param RequestService $requestService
     * @param TokenService   $tokenService
     */
    public function __construct(
        RequestService $requestService,
        TokenService $tokenService
    ) {
        parent::__construct();
        $this->requestService = $requestService;
        $this->tokenService = $tokenService;
    }

    /**
     * Sign in a user
     *
     * @param string $email    Username in Hulu Ad Api
     * @param string $password Password in Hulu Ad Api
     * @param int    $userId
     *
     * @return HaapiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function handle(string $email, string $password, int $userId): HaapiResponse
    {
        $response = $this->requestService->request(
            self::REQUEST_TYPE,
            [
                'clientId' => config('cognito.user_pool_web_client_id'),
                'username' => $email,
                'password' => $password
            ],
            $userId
        );
        $token = $response->get('sessionToken');

        $this->tokenService->set($userId, $token);

        return $response;
    }
}
