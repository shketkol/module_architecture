<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\Contracts\CampaignCancel;
use Tests\Haapi\HaapiTestCase;

class CampaignCancelTest extends HaapiTestCase
{
    /**
     * @var CampaignCancel
     */
    protected $action;

    /**
     * @var string
     */
    protected $campaignId;

    protected function setUp(): void
    {
        self::markTestSkipped('[TODO] Improve to use some stateless data');
        parent::setUp();
        $this->action = app(CampaignCancel::class);
        $this->campaignId = self::CAMPAIGN_ID;
    }

    /**
     * Test successful campaign cancel
     */
    public function testCampaignCancelSuccess()
    {
        $response = $this->action->handle($this->campaignId, $this->userHaapi->id);
        $this->assertEquals('0', $response->getStatusCode());
    }
}
