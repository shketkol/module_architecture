<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\Contracts\ReportAccountImpressions;
use Tests\Haapi\HaapiTestCase;

class ReportAccountImpressionsTest extends HaapiTestCase
{
    /**
     * @var ReportAccountImpressions
     */
    protected $action;

    /**
     * @var string
     */
    protected $accountId;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(ReportAccountImpressions::class);
        $this->accountId = self::ADV_ACCOUNT_ID;
    }

    /**
     * Test that action returns integer impressions
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testSuccess()
    {
        $response = $this->action->handle($this->accountId, $this->userHaapi->id);

        $this->assertIsInt($response);
    }
}
