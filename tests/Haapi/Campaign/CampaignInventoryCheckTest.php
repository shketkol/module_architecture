<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\Contracts\CampaignInventoryCheck;
use Modules\Haapi\DataTransferObjects\Campaign\InventoryCheckParams;
use Tests\Haapi\HaapiTestCase;

class CampaignInventoryCheckTest extends HaapiTestCase
{
    /**
     * @var CampaignInventoryCheck
     */
    protected $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(CampaignInventoryCheck::class);
    }

    /**
     * Test check inventory params successfully
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testSuccess()
    {
        $response = $this->action->handle($this->getInventoryCheckParams(), $this->userHaapi->id);
        $this->assertEquals("0", $response->getStatusCode());
        $payload = $response->getPayload();
        $this->assertArrayHasKey('quantity', $payload);
        $this->assertArrayHasKey('quantityType', $payload);
        $this->assertArrayHasKey('unitCost', $payload);
        $this->assertArrayHasKey('costModel', $payload);
        $this->assertArrayHasKey('statusCode', $payload);
        $this->assertArrayHasKey('availableImpressions', $payload);
        $this->assertArrayHasKey('systemFloorBudget', $payload);
        $this->assertArrayHasKey('availableBudget', $payload);
    }

    /**
     * @return InventoryCheckParams
     */
    private function getInventoryCheckParams(): InventoryCheckParams
    {
        return new InventoryCheckParams([
            'startDate' => '2022-05-30T09:30:10+0000',
            'endDate' => '2022-05-30T09:30:10+0000',
            'targets' => [
                0 => [
                    'typeId' => '0f1336b2-7bfc-11e9-b52b-9b8ffb3c3b74',
                        'values' => [
                            0 => [
                                'value' => 'a1db6841-9c3e-11e9-8714-0a3b51d4007e',
                                'exclusion' => 0
                            ]
                        ]
                    ]
                ],
            'productId' => config('daapi.campaign.productId'),
            'quantity' => 1000000,
            'quantityType' => 'IMPRESSIONS'
        ]);
    }
}
