<?php

namespace Tests\Haapi\Campaign;

use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignGet;
use Tests\Haapi\HaapiTestCase;

class CampaignGetTest extends HaapiTestCase
{
    /**
     * @var CampaignGet
     */
    protected $action;

    /**
     * @var string
     */
    protected $campaignId;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(CampaignGet::class);
        $this->campaignId = self::CAMPAIGN_ID;
    }

    /**
     * Test successful get campaign
     */
    public function testCampaignGetSuccess()
    {
        $response = $this->action->handle($this->campaignId, $this->userHaapi->id);
        $this->assertInstanceOf(CampaignData::class, $response);
        $this->assertEquals($response->id, $this->campaignId);
    }
}
