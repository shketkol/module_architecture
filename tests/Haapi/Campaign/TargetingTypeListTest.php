<?php

namespace Tests\Haapi\Campaign;

use Modules\Haapi\Actions\Campaign\Contracts\TargetingTypeList;
use Tests\Haapi\HaapiTestCase;

class TargetingTypeListTest extends HaapiTestCase
{
    /**
     * @var TargetingTypeList
     */
    protected $action;

    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(TargetingTypeList::class);
    }

    public function testListTargetingValuesSuccess()
    {
        $response = $this->action->handle(200, 0);
        $payload = $response->getPayload();
        $this->assertEquals('0', $response->getStatusCode());
        $this->assertArrayHasKey('limit', $payload);
        $this->assertArrayHasKey('next', $payload);
        $this->assertArrayHasKey('targetTypes', $payload);
    }
}
