<?php

namespace Tests\Haapi\Campaign;

use Carbon\Carbon;
use DateTime;
use Modules\Haapi\Actions\Campaign\Contracts\CampaignCreate;
use Modules\Haapi\DataTransferObjects\Campaign\CampaignParams;
use Modules\Haapi\DataTransferObjects\Campaign\LineItemParams;
use Tests\Haapi\HaapiTestCase;

class CampaignCreateTest extends HaapiTestCase
{
    /**
     * @var CampaignCreate
     */
    protected $action;

    /**
     * Setup environment
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->action = app(CampaignCreate::class);
    }

    /**
     * Test Campaign create success
     */
    public function testSuccess()
    {
        $campaignParams = $this->getCampaignParams();
        $response = $this->action->handle($campaignParams, $this->userHaapi->id);
        $this->assertEquals("0", $response->getStatusCode());
        $this->assertEquals("OK", $response->getStatusMsg());
    }

    /**
     * @return CampaignParams
     */
    private function getCampaignParams(): CampaignParams
    {
        return new CampaignParams([
            'name'         => 'New campaign',
            'advertiserId' => self::ADV_ACCOUNT_ID,
            'lineItems'    => $this->getLineItemParams(),
        ]);
    }

    /**
     * @return array
     */
    private function getLineItemParams(): array
    {
        $lineItemParams[] = new LineItemParams([
            'startDate' => Carbon::now()->addDays(3)->format(DateTime::ISO8601),
            'endDate'   => Carbon::now()->addMonth()->format(DateTime::ISO8601),
            'targets'   => [
                0 => [
                    'typeId' => '0f1336b2-7bfc-11e9-b52b-9b8ffb3c3b74',
                    'values' => [
                        0 => [
                            'value'     => 'a1db6841-9c3e-11e9-8714-0a3b51d4007e',
                            'exclusion' => 0,
                        ],
                        1 => [
                            'value'     => 'a1db90b0-9c3e-11e9-8714-0a3b51d4007e',
                            'exclusion' => 0,
                        ],
                        2 => [
                            'value'     => 'a1dbc55b-9c3e-11e9-8714-0a3b51d4007e',
                            'exclusion' => 1,
                        ],
                    ],
                ],
            ],
            'unitCost'  => 39.75,
            'quantity'  => 100,
        ]);

        return $lineItemParams;
    }
}
