<?php

namespace Tests\Haapi\Admin\Campaign;

use Modules\Haapi\Actions\Admin\Campaign\Contracts\AdminCampaignSearch;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Haapi\Exceptions\UnauthorizedException;
use Tests\Haapi\HaapiTestCase;

class AdminCampaignSearchTest extends HaapiTestCase
{
    /**
     * @var AdminCampaignSearch
     */
    protected $action;

    /**
     * Set up an environment
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->action = app(AdminCampaignSearch::class);
    }

    /**
     * Test success response
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminAccountSearchSuccess()
    {
        $response = $this->action->handle(
            new AdminCampaignSearchParam([
                'accountId' => self::ADV_ACCOUNT_ID
            ]),
            $this->adminHaapi->id
        );
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test success response
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminAccountSearchEmptySuccess()
    {
        $response = $this->action->handle(
            new AdminCampaignSearchParam([]),
            $this->adminHaapi->id
        );
        $this->assertEquals('0', $response->getStatusCode());
    }

    /**
     * Test use not authorized response
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testAdminAccountSearchNotAuthorized()
    {
        $this->expectException(UnauthorizedException::class);
        $this->action->handle(new AdminCampaignSearchParam([]), 0);
    }
}
