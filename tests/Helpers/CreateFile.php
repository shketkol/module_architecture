<?php

namespace Tests\Helpers;

use Illuminate\Http\Testing\File;

class CreateFile
{
    /**
     * Create a new fake file with content.
     * this method implemented in the Laravel 7.0
     * https://github.com/laravel/framework/blob/0b12ef19623c40e22eff91a4b48cb13b3b415b25/src/Illuminate/Http/Testing/FileFactory.php#L36
     *
     * @param  string  $name
     * @param  string  $content
     * @return File
     *
     */
    public static function createWithContent($name, $content): File
    {
        $tmpfile = tmpfile();

        fwrite($tmpfile, $content);

        return tap(new File($name, $tmpfile), function ($file) use ($tmpfile) {
            $file->sizeToReport = fstat($tmpfile)['size'];
        });
    }
}
