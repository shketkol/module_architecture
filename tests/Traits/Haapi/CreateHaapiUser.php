<?php

namespace Tests\Traits\Haapi;

use Modules\User\Models\User;
use Tests\Haapi\Helpers\TestUserSignIn;

trait CreateHaapiUser
{
    /**
     * Create test user using factory.
     *
     * @param string $email
     * @param string $password
     * @param array  $attributes
     *
     * @param bool   $login
     *
     * @return User
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    protected function createHaapiTestUser(
        string $email,
        string $password,
        array $attributes = [],
        bool $login = true
    ): User {
        $user = User::factory()->create($attributes);

        if ($login) {
            $this->login($email, $password, $user->id);
        }

        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @param int    $id
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Modules\Haapi\Exceptions\ConflictException
     * @throws \Modules\Haapi\Exceptions\ForbiddenException
     * @throws \Modules\Haapi\Exceptions\HaapiConnectivityException
     * @throws \Modules\Haapi\Exceptions\HaapiException
     * @throws \Modules\Haapi\Exceptions\InternalErrorException
     * @throws \Modules\Haapi\Exceptions\InvalidRequestException
     * @throws \Modules\Haapi\Exceptions\UnauthorizedException
     */
    public function login(string $email, string $password, int $id): void
    {
        /*** @var TestUserSignIn $userSignInAction */
        $userSignInAction = app(TestUserSignIn::class);
        $userSignInAction->handle($email, $password, $id);
    }
}
