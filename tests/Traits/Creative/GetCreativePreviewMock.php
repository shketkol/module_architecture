<?php

namespace Tests\Traits\Creative;

use Mockery;
use Modules\Creative\Actions\CreatePreviewAction;

trait GetCreativePreviewMock
{
    /**
     * @param string $s3Key
     *
     * @return void
     */
    private function mockCreatePreviewActionSuccess(string $s3Key): void
    {
        $mock = Mockery::mock(CreatePreviewAction::class, function ($mock) use ($s3Key) {
            $mock
                ->shouldReceive('handle')
                ->andReturn([
                    'original_key' => $s3Key,
                    'preview_url'  => null,
                    'poster_key'   => null,
                ]);
        });

        $this->app->instance(CreatePreviewAction::class, $mock);
    }
}
