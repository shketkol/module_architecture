<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Daapi\DataTransferObjects\Types\UserData;
use Modules\Haapi\Actions\Admin\User\AdminUserGet;

trait AdminUserGetMock
{
    /**
     * @param string $externalId
     * @param int $userId
     */
    private function mockAdminUserGetActionSuccess(string $externalId, int $userId)
    {
        $mock = Mockery::mock(AdminUserGet::class, function ($mock) use ($externalId, $userId) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getAdminUserGetResponse($externalId, $userId));
        });

        $this->app->instance(AdminUserGet::class, $mock);
    }

    /**
     * @param string $externalId
     * @param int $id
     * @return UserData
     */
    private function getAdminUserGetResponse(string $externalId, int $id): UserData
    {
        $data = $this->getAdminUserGetSuccessData($externalId);

        return new UserData(array_merge(
            Arr::get($data, 'haapi.response.payload', []),
            ['id' => $id]
        ));
    }

    /**
     * @param string $externalId
     * @return array
     */
    private function getAdminUserGetSuccessData(string $externalId): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "userId" => '1',
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "email"      => 'email@test.com',
                        "firstName"  => 'first',
                        "lastName"   => 'last',
                        "externalId" => $externalId,
                        "userStatus" => "PENDING MANUAL REVIEW",
                        "userType"   => "NORMAL",
                        "account"    => [
                            "createdAt"      => date('Y-m-d h:i:s', time()),
                            "updatedAt"      => date('Y-m-d h:i:s', time()),
                            "accountType"    => "ADVERTISER",
                            "companyName"    => 'company',
                            "phoneNumber"    => "1235670000",
                            "externalId"     => $externalId,
                            "companyAddress" => [
                                "line1"   => "0000 Broadway",
                                "line2"   => null,
                                "city"    => "Santa Monica",
                                "state"   => "CA",
                                "country" => "United States",
                                "zipcode" => "99999",
                            ],
                            "accountStatus" => "ACTIVE",
                        ],
                    ],
                ],
            ],
        ];
    }
}
