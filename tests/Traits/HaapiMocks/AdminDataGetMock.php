<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Daapi\DataTransferObjects\Types\UserDataPlain;
use Modules\Haapi\Actions\Admin\User\AdminDataGet;

trait AdminDataGetMock
{
    /**
     * @return void
     */
    private function mockAdminDataGetActionSuccess()
    {
        $mock = Mockery::mock(AdminDataGet::class, function ($mock) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getAdminDataGetResponse());
        });

        $this->app->instance(AdminDataGet::class, $mock);
    }

    /**
     * @return UserDataPlain
     */
    private function getAdminDataGetResponse(): UserDataPlain
    {
        $data = $this->getAdminDataGetSuccessData();

        return new UserDataPlain(
            Arr::get($data, 'haapi.response.payload', []),
        );
    }

    /**
     * @return array
     */
    private function getAdminDataGetSuccessData(): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "user/signin",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "userId" => '1',
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "hulu-selfserve",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "email"      => 'email@test.com',
                        "firstName"  => 'first',
                        "lastName"   => 'last',
                        "id"         => 'asdf-asdf-asdf-asdf',
                        "userStatus" => "PENDING MANUAL REVIEW",
                        "userType"   => "NORMAL",
                        "account"    => [
                            "createdAt"      => date('Y-m-d h:i:s', time()),
                            "updatedAt"      => date('Y-m-d h:i:s', time()),
                            "accountType"    => "ADVERTISER",
                            "companyName"    => 'company',
                            "phoneNumber"    => "1235670000",
                            "id"             => 'asdf-asdf-asdf-asdf',
                            "companyAddress" => [
                                "line1"   => "0000 Broadway",
                                "line2"   => null,
                                "city"    => "Santa Monica",
                                "state"   => "CA",
                                "country" => "United States",
                                "zipcode" => "99999",
                            ],
                            "accountStatus" => "ACTIVE",
                        ],
                    ],
                ],
            ],
        ];
    }
}
