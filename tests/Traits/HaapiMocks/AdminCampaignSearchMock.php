<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Haapi\Actions\Admin\Campaign\AdminCampaignSearch;
use Modules\Haapi\DataTransferObjects\Campaign\AdminCampaignSearchParam;
use Modules\Haapi\HttpClient\Responses\HaapiResponse;

trait AdminCampaignSearchMock
{
    /**
     * @param AdminCampaignSearchParam $params
     * @param int $userId
     *
     * @return void
     */
    private function mockAdminCampaignSearchActionSuccess(AdminCampaignSearchParam $params, int $userId): void
    {
        $mock = Mockery::mock(AdminCampaignSearch::class, function ($mock) use ($params, $userId) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getAdminCampaignSearchResponse());
        });

        $this->app->instance(AdminCampaignSearch::class, $mock);
    }

    /**
     * @return HaapiResponse
     */
    private function getAdminCampaignSearchResponse(): HaapiResponse
    {
        $data = $this->getAdminCampaignSearchData();

        $response = new HaapiResponse();

        $body = Arr::get($data, 'haapi.response');
        $statusCode = Arr::get($body, 'statusCode');
        $statusMsg = Arr::get($body, 'statusMsg');
        $responseId = Arr::get($body, 'id');

        $response->setBody($body);
        $response->setPayload(Arr::get($body, 'payload'));
        $response->setId($responseId);
        $response->setStatusCode($statusCode);
        $response->setStatusMsg($statusMsg);
        $response->setDuration(Arr::get($body, 'duration'));
        $response->setReceived(Arr::get($body, 'received'));

        return $response;
    }

    /**
     * @return array
     */
    private function getAdminCampaignSearchData(): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "test-uuid",
                    "system"       => "test-identifier",
                    "haapiVersion" => 1,
                    "type"         => "account/search",
                    "preSharedKey" => "test-uuid-preshared",
                    "payload"      => [
                        "start" => 0,
                        "limit" => 100,
                        "accountId" => '7777',
                        "impressionCountStartTime" => null,
                        "impressionCountEndTime" => null,
                        "campaignStartBefore" => '06/10/2020',
                        "campaignEndAfter" => '06/08/2020'
                    ],
                ],
                "response" => [
                    "id"         => "test-uuid",
                    "system"     => "haapi",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "duration"   => 219,
                    "received"   => date('Y-m-d h:i:s', time()),
                    "payload"    => [
                        "next"     => null,
                        "campaigns" => [

                        ]
                    ],
                ],
            ],
        ];
    }
}
