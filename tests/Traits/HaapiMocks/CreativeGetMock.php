<?php

namespace Tests\Traits\HaapiMocks;

use Illuminate\Support\Arr;
use Mockery;
use Modules\Campaign\Models\Campaign;
use Modules\Creative\Models\Creative;
use Modules\Daapi\DataTransferObjects\Types\CampaignData;
use Modules\Daapi\DataTransferObjects\Types\CreativeData;
use Modules\Haapi\Actions\Campaign\CampaignGet;
use Modules\Haapi\Actions\Creative\CreativeGet;

trait CreativeGetMock
{
    /**
     * @param Creative $creative
     */
    private function mockCreativeGetActionSuccess(Creative $creative)
    {
        $mock = Mockery::mock(CreativeGet::class, function ($mock) use ($creative) {
            $mock
                ->shouldReceive('handle')
                ->andReturn($this->getCreativeGetResponse($creative));
        });

        $this->app->instance(CreativeGet::class, $mock);
    }


    /**
     * @param Creative $creative
     * @return CreativeData
     */
    private function getCreativeGetResponse(Creative $creative): CreativeData
    {
        $data = $this->getCreativeGetSuccessData($creative);
        return new CreativeData(
            Arr::get($data, 'haapi.response.payload.creative', [])
        );
    }

    /**
     * @param Creative $creative
     * @return array
     */
    private function getCreativeGetSuccessData(Creative $creative): array
    {
        return [
            "haapi" => [
                "request"  => [
                    "id"           => "f3674e9a-364e-4251-96e1-722c0014f7ab",
                    "system"       => "hulu-ss-portal",
                    "haapiVersion" => "1",
                    "type"         => "creative/get",
                    "preSharedKey" => "f2e64f90-624b-4db5-aecd-cf23b3772822",
                    "userToken"    => "b201ed65-909e-4c54-a2d5-dc9cdc139b8e",
                    "callbackUrl"  => null,
                    "payload"      => [
                        "creativeId" => $creative->external_id
                    ]
                ],
                "response" => [
                    "id"         => 'f3674e9a-364e-4251-96e1-722c0014f7ab',
                    "system"     => "haapi",
                    "statusCode" => "0",
                    "statusMsg"  => "OK",
                    "payload"    => [
                        "creative" => [
                            "id"          => $creative->external_id,
                            "fileName"    => "Escape - Parallel Parking 30",
                            "creativeUrl" => "https://hulu-ads-us-east-1-stg1-selfservice-assets.s3.amazonaws.com/df41a803-bc5a-4c39-9f28-7921b2da50ca/Escape%20-%20Parallel%20Parking%2030",
                            "status"      => "PROCESSING",
                            "reason"      => null,
                            "comment"     => null,
                            "accountId"   => "df41a803-bc5a-4c39-9f28-7921b2da50ca",
                            "createdAt"   => "2019-07-25T06:37:09.000+0000"
                        ]
                    ],
                    "received"   => "2019-07-25T11:23:17Z",
                    "duration"   => 82,
                ]
            ]
        ];
    }
}
