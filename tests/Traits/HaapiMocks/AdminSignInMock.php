<?php

namespace Tests\Traits\HaapiMocks;

use Mockery;
use Modules\Haapi\Actions\User\TechAdminClientAuthorize;

trait AdminSignInMock
{
    /**
     * @return void
     */
    private function mockAdminSignInActionSuccess()
    {
        $mock = Mockery::mock(TechAdminClientAuthorize::class, function ($mock) {
            $mock
                ->shouldReceive('handle')
                ->andReturn('session_token');
        });

        $this->app->instance(TechAdminClientAuthorize::class, $mock);
    }
}
