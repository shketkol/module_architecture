<?php

namespace Tests;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Http\Response;
use Modules\Auth\Services\AuthService\Actions\ValidateCognitoJwtAction;
use Modules\Auth\Services\AuthService\Contracts\AuthService;
use Tests\Traits\Cognito\CognitoTokensHelper;

abstract class TestCase extends BaseTestCase
{
    use DatabaseTransactions,
        CreatesApplication,
        CognitoTokensHelper;

    /**
     *  Save last response
     *
     * @var Response|null A Response instance
     */
    static $lastResponse;

    /**
     *  Modify to save response
     *
     * @param string $method
     * @param string $uri
     * @param array  $parameters
     * @param array  $cookies
     * @param array  $files
     * @param array  $server
     * @param string $content
     *
     * @return \Illuminate\Http\Response
     */
    public function call($method, $uri, $parameters = [], $cookies = [], $files = [], $server = [], $content = null)
    {
        $response = parent::call($method, $uri, $parameters, $cookies, $files, $server, $content);

        static::$lastResponse = $response;

        return $response;
    }

    /**
     * If test failed show full response body.
     */
    protected function tearDown(): void
    {
        if ($this->hasFailed()) {
            try {
                echo sprintf(' Full response body: "%s ".', static::$lastResponse->getContent());
            } catch (\Throwable $exception) {
                echo sprintf(' Cannot log response body ');
            }
        }

        parent::tearDown();
    }

    /**
     * Set the currently logged in user for the application.
     *
     * @param Authenticatable $user
     * @param null $driver
     *
     * @param null $token
     * @return BaseTestCase
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function actingAs(Authenticatable $user, $driver = null, $token = null)
    {
        $result = parent::actingAs($user, $driver);

        /** @var AuthService $authService */
        $authService = $this->app->make(AuthService::class);

        $token = $token ?: $this->makeCognitoTokens();
        $authService->setTokens($user->getKey(), $token);

        return $result;
    }
}
