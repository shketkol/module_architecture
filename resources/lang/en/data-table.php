<?php
/**
 * Translations for data tables
 */
return [
  'next' => 'Next',
  'previous' => 'Previous',
  'entries' => 'Showing :fromPage&ndash;:toPage of :recordsFiltered entries'
];
