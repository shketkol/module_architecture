<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'               => 'Double-check your email address and/or password and try again.',
    'throttle'             => 'Too many login attempts. Please try again in :seconds seconds.',
    'cognito_session'      => 'Your session has expired. Please try again.',
    'cognito_invalid_code' => 'Invalid code provided, please request a code again.',
];
