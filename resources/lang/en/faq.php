<?php

return [
    'labels'   => [
        'faq'                => 'FAQ',
        'user_guide_and_faq' => 'User Guide and FAQ',
        'help'               => 'Help',
    ],
    'messages' => [
        'image'            => [
            'validation' => [
                'ratio' => 'Image width and height should be :width * :height pixels',
                'size'  => 'Image size must not exceed :size',
                'image' => 'File should be an image',
            ],
        ],
    ]
];
