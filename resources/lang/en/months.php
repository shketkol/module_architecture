<?php

return [
    'january'   => 'January',
    'february'  => 'February',
    'april'     => 'April',
    'may'       => 'May',
    'june'      => 'June',
    'july'      => 'July',
    'august'    => 'August',
    'september' => 'September',
    'october'   => 'October',
    'november'  => 'November',
    'december'  => 'December',
];
