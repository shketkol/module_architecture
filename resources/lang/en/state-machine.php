<?php

return [
    'transitions' => [
        'error' => 'Status cannot be changed.',
    ],
];
