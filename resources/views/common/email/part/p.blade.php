<p style="{{\App\Helpers\HtmlHelper::getMailTagCommonStyles('p')}}">
    @if(isset($renderHtml) && $renderHtml)
        {!! $text !!}
    @else
        {{ $text }}
    @endif
</p>
