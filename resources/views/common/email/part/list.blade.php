@if(isset($numeric) && $numeric)
    <ol>
@else
    <ul>
@endif
    @foreach($listItems as $item)
        <li>
            @include('common.email.part.p', [
                'text' => $item,
                'renderHtml' => isset($renderHtml) && $renderHtml
            ])
        </li>
    @endforeach
@if(isset($numeric) && $numeric)
    </ol>
@else
    </ul>
@endif
