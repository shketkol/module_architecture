<p style="{{\App\Helpers\HtmlHelper::getMailTagCommonStyles('p')}}">
    {!! __('emails.if_you_have_any_questions', [
        'contact_us' => view('common.email.part.link', [
            'link' => route('users.profile.contact'),
            'text' => __('support::actions.contact_us'),
        ])->render(function ($content) {
            return trim($content);
        }),
    ]) !!}
</p>
