<div style="margin-top: 0; margin-bottom: 15px;">
    <!--[if mso]>
    <v:rect xmlns:v="urn:schemas-microsoft-com:vml"
        xmlns:w="urn:schemas-microsoft-com:office:word"
        href="{{$link}}"
        style="
            height:36px;
            v-text-anchor:middle;
            width: 235px;"
        arcsize="0"
        strokecolor="#030303"
        fillcolor="#030303">
        <w:anchorlock/>
        <center>
            <table width="100%"
                border="0"
                cellpadding="0"
                cellspacing="0"
                valign="middle"
                style="
                    width:100%;
                    height:36px;
                    color:#ffffff;
                    font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
                    font-size:16px;"
            >
                <tr>
                    <td style="
                            font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
                            padding-top:3px;
                            padding-bottom:0px;
                            padding-left:20px;
                            padding-right:20px;
                            text-align:center"
                    >
                      <span style="
                              text-transform:uppercase;
                              color:#ffffff;
                              font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
                              font-size:16px;
                              line-height:26px;
                              text-align:center;
                              text-decoration:none;
                              -webkit-text-size-adjust:none;
                              text-transform:uppercase;"
                      >
                            {{ $text }}
                      </span>
                    </td>
                </tr>
            </table>
        </center>
    </v:rect>
    <![endif]-->
    <a href="{{$link}}"
        style="padding-left: 20px;
            padding-right: 20px;
            line-height:36px;
            background-color:#030303;
            border:1px solid #030303;
            color:#ffffff;
            display:inline-block;
            font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
            font-size:16px;
            text-align:center;
            text-decoration:none;
            -webkit-text-size-adjust:none;
            mso-hide:all;"
    >
        <table border="0"
            cellpadding="0"
            cellspacing="0"
            valign="middle"
            style="height:36px;
                color:#ffffff;
                font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
                font-size:16px;
                mso-hide:all;"
        >
            <tr>
                <td style="font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
                    padding-top:0;
                    text-transform:uppercase;
                    padding-bottom:0;
                    padding-left:13px;
                    padding-right:13px;
                    text-align:center;"
                >
                      <span style="color:#ffffff;
                          font-family: 'Graphik-regular', Helvetica-Light, Helvetica, Arial, sans-serif;
                          font-size:16px;
                          line-height:36px;
                          text-align:center;
                          text-decoration:none;
                          -webkit-text-size-adjust:none;"
                      >
                          {{ $text }}
                      </span>
                </td>
            </tr>
        </table>
    </a>
</div>
