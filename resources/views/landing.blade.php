@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <landing-page />
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/landing.js') }}"></script>
@endpush
