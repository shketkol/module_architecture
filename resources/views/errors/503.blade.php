@extends('layouts.app')

@section('content')
    <div class="vue-app">
        <maintenance-page message="{{ !empty($exception->getMessage()) ? $exception->getMessage() :  __('messages.default_maintenance_message')  }}"></maintenance-page>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/auth.js') }}"></script>
@endpush
