@extends('errors::minimal')

@section('title', __('Invalid Request'))
@section('code', '422')
@section('message', __('Invalid Request'))
