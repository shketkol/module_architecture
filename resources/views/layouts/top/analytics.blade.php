@if ($tealiumEnabled)
    <script type="text/javascript" nonce="7AHKHG47Z">
        @if ($spaMode)
            window.utag_cfg_ovrd = { noview : true };
        @endif

        let utag_data = {};

        (function(a,b,c,d){
            a='{{ config('analytics.services.tealium.url') }}' + '{{ config('analytics.services.tealium.id') }}' + '/utag.js';
            b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
            a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
        })();
    </script>
@endif
