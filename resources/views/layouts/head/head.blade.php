<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @auth
        <meta name="user-id" content="{{ Auth::user()->id }}">
    @endauth

    <title>{{ env('APP_TITLE', 'Hulu Ad Manager') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/styles.css') }}" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png" />

    @stack('head')
</head>
