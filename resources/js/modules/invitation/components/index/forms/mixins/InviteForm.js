import { mapMutations } from 'vuex';

export default {
  /**
   * Component methods
   */
  methods: {
    /**
     * Map mutations
     */
    ...mapMutations('invitation', { pending: 'switchStoreInvitationPending' }),

    /**
     * Save api call
     */
    save () {
      this.formData.account_type_category = !!this.formData.account_type_category;
      return this.store(this.formData);
    },

    /**
     * Validate email
     */
    validate () {
      return this.$validateScope(this.scope);
    },
  },
}
