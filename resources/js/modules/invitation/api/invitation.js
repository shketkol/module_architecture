import axios from 'axios';
import route from 'Foundation/services/routing/route';

export default {
  /**
   * Store individual invite
   *
   * @returns {AxiosPromise<any>}
   */
  storeInvitation (data) {
    return axios.post(
      route('api.invitations.store'),
      data
    );
  },

  /**
   * Store bulk invites
   *
   * @returns {AxiosPromise<any>}
   */
  storeBulkInvitation (data) {
    return axios.post(
      route('api.invitations.storeBulk'),
      data
    );
  },

  /**
   * Validate bulk invites
   *
   * @returns {AxiosPromise<any>}
   */
  validateBulkInvitation (data) {
    return axios.post(
      route('api.invitations.validateBulk'),
      data
    );
  },

  /**
   * Validate csv file
   *
   * @returns {AxiosPromise<any>}
   */
  validateCsv (data) {
    return axios.post(
      route('api.invitations.validateCsv'),
      data
    );
  },

  /**
   * Resend invite
   *
   * @returns {AxiosPromise<any>}
   */
  resendInvitation (invitation) {
    return axios.post(
      route('api.invitations.resend', { invitation })
    );
  },

  /**
   * Revoke invite
   *
   * @returns {AxiosPromise<any>}
   */
  revokeInvitation (invitation) {
    return axios.post(
      route('api.invitations.revoke', { invitation })
    );
  },

  /**
   * Resume invite
   *
   * @returns {AxiosPromise<any>}
   */
  resumeInvitation (invitation) {
    return axios.post(
      route('api.invitations.resume', { invitation })
    );
  },

  /**
   * Delete invite.
   *
   * @param invitation
   * @returns {AxiosPromise}
   */
  destroy (invitation) {
    return axios.delete(
      route('api.invitations.destroy', { invitation })
    );
  },
};
