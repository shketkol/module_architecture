import 'Foundation/bootstrap';
import TheAdminInvitationIndex from "Modules/invitation/pages/admin/TheAdminInvitationIndex";
import store from './store';
import Vue from 'vue';

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    TheAdminInvitationIndex,
  },

  /**
   * Store
   */
  store,
}));
