import cloneDeep from 'lodash/cloneDeep';
import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { invitation as api } from '../../api';

/**
 * Types
 */
const types = {
  STORE_INDIVIDUAL_INVITATION: storeHelper.createAsyncMutation('STORE_INDIVIDUAL_INVITATION'),
  STORE_BULK_INVITATION: storeHelper.createAsyncMutation('STORE_BULK_INVITATION'),
  VALIDATE_BULK_INVITATION: storeHelper.createAsyncMutation('VALIDATE_BULK_INVITATION'),
  VALIDATE_CSV: storeHelper.createAsyncMutation('VALIDATE_CSV'),
  REVOKE_INVITATION: storeHelper.createAsyncMutation('REVOKE_INVITATION'),
  RESUME_INVITATION: storeHelper.createAsyncMutation('RESUME_INVITATION'),
  DELETE_INVITATION: storeHelper.createAsyncMutation('DELETE_INVITATION'),
  RESEND_INVITATION: storeHelper.createAsyncMutation('RESEND_INVITATION'),
};

/**
 * Default invitation data state
 * @type {{}}
 */
const defaultInvitationData = {
  email: null,
  code: null,
  account_type_category: false
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.STORE_INDIVIDUAL_INVITATION,
    types.STORE_BULK_INVITATION,
    types.VALIDATE_BULK_INVITATION,
    types.VALIDATE_CSV,
    types.REVOKE_INVITATION,
    types.RESUME_INVITATION,
    types.DELETE_INVITATION,
    types.RESEND_INVITATION,
  ]),

  /**
   * Advertiser email data.
   */
  invitationData: cloneDeep(defaultInvitationData),

  bulkInvite: {
    emails: '',
    account_type_category: false
  },

  /**
   * Pending store invitation.
   */
  storeInvitationPending: false
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Store individual invite code
   *
   * @param state, payload
   */
  storeIndividualInvitation: storeHelper.createAction(types.STORE_INDIVIDUAL_INVITATION, {
    call: api.storeInvitation,
  }),

  /**
   * Store bulk invite code
   *
   * @param state, payload
   */
  storeBulkInvitation: storeHelper.createAction(types.STORE_BULK_INVITATION, {
    call: api.storeBulkInvitation,
  }),

  /**
   * Validate bulk invite code
   *
   * @param state, payload
   */
  validateBulkInvitation: storeHelper.createAction(types.VALIDATE_BULK_INVITATION, {
    call: api.validateBulkInvitation,
  }),

  /**
   * Validate CSV
   *
   * @param state, payload
   */
  validateCsv: storeHelper.createAction(types.VALIDATE_CSV, {
    call: api.validateCsv,
  }),

  /**
   * Revoke invite code
   *
   * @param state, payload
   */
  revokeInvitation: storeHelper.createAction(types.REVOKE_INVITATION, {
    call: api.revokeInvitation,
  }),

  /**
   * Resume invite code
   *
   * @param state, payload
   */
  resumeInvitation: storeHelper.createAction(types.RESUME_INVITATION, {
    call: api.resumeInvitation,
  }),

  /**
   * Resend invite code
   *
   * @param state, payload
   */
  resendInvitation: storeHelper.createAction(types.RESEND_INVITATION, {
    call: api.resendInvitation,
  }),

  /**
   * Delete the invite.
   */
  deleteInvitation: storeHelper.createAction(types.DELETE_INVITATION, {
    call: api.destroy,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Store individual invite code mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.STORE_INDIVIDUAL_INVITATION, {
    success(state) {
      state.invitationData = cloneDeep(defaultInvitationData);
    }
  }),

  /**
   * Store bulk invite code mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.STORE_BULK_INVITATION, {
    success(state) {
      state.bulkInvite = {
        emails: '',
        account_type_category: false
      }
    }
  }),

  /**
   * Validate bulk invite code mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.VALIDATE_BULK_INVITATION, {
    success(state) {
      state.bulkInvite.emails = ''
    }
  }),

  /**
   * Validate CSV file
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.VALIDATE_CSV),

  /**
   * Resend invite code mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.RESEND_INVITATION),

  /**
   * Revoke invite code mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.REVOKE_INVITATION),

  /**
   * Resume invite code mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.RESUME_INVITATION),

  /**
   * Delete the invitation.
   */
  ...storeHelper.createMutation(types.DELETE_INVITATION),

  /**
   * Clear download report data
   *
   * @param state
   */
  switchStoreInvitationPending(state, value) {
    state.storeInvitationPending = value;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


