import invitation from './modules/invitation'
import Vue from 'vue';
import Vuex from 'vuex';
import notifications from "Modules/notification/store/modules/notifications";


Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    invitation,
    notifications
  },
});

export default store;
