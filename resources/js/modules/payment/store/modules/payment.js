import { trackActionEvent } from "Foundation/services/track/track";
import storeHelper from 'Foundation/helpers/store';
import {getField, updateField} from 'vuex-map-fields';
import { payment as api } from '../../api';
import find from "lodash/find";
import findIndex from "lodash/findIndex";
import translator from '../../../../foundation/services/translations/locale';

/**
 * Types
 */
const types = {
  INDEX_PAYMENT: storeHelper.createAsyncMutation('INDEX_PAYMENT'),
  STORE_PAYMENT: storeHelper.createAsyncMutation('STORE_PAYMENT'),
  REPLACE_PAYMENT: storeHelper.createAsyncMutation('REPLACE_PAYMENT'),
  SHOW_PAYMENT: storeHelper.createAsyncMutation('SHOW_PAYMENT'),
  DELETE_PAYMENT: storeHelper.createAsyncMutation('DELETE_PAYMENT'),
  TRANSACTION_DETAILS: storeHelper.createAsyncMutation('TRANSACTION_DETAILS'),
  RETRY_PAYMENT: storeHelper.createAsyncMutation('RETRY_PAYMENT'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Index payment loading.
   */
  [types.INDEX_PAYMENT.loadingKey]: null,

  /**
   * Store payment loading.
   */
  [types.STORE_PAYMENT.loadingKey]: null,

  /**
   * Replace payment loading.
   */
  [types.REPLACE_PAYMENT.loadingKey]: null,

  /**
   * Show payment loading.
   */
  [types.SHOW_PAYMENT.loadingKey]: null,

  /**
   * Delete payment loading.
   */
  [types.DELETE_PAYMENT.loadingKey]: null,

  /**
   * Create state for mutation.
   */
  ...storeHelper.createMutationState([
    types.TRANSACTION_DETAILS,
    types.RETRY_PAYMENT,
  ]),

  /**
   * Payment method.
   */
  payment: {
    name: null,
    session: null,
    zip: null,
    exp_mont: null,
    exp_year: null,
    last4: null,
    permissions: null,
    address: null,
    city: null,
  },

  elements: null,
  paymentMethods: [],
  selectedPayment: null,
  transaction: null,
  userPermissions: null,
  indexPaymentError: null,
  indexPaymentApplicationError: false
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Get payment object from state by id.
   *
   * @param state
   * @returns {(undefined|object)}
   */
  getPayment (state) {
    return (id) => {
      return find(state.paymentMethods, {id});
    };
  },

  /**
   * Get active payment method or null.
   * @param state
   * @returns {(Object|null)}
   */
  activePayment (state) {
    if (state.paymentMethods.length === 0) {
      return null;
    }
    return state.paymentMethods[0];
  }
};

/**
 * Actions
 */
const actions = {
  /**
   * Index payment methods information.
   *
   * @param commit
   * @returns {Promise<*|undefined>}
   */
  index: storeHelper.createAction(types.INDEX_PAYMENT, {
    call: api.index
  }),

  /**
   * Store payment methods information.
   *
   * @param commit
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async store({commit, state}) {
    commit(types.STORE_PAYMENT.PENDING);
    try {
      const response = await api.store(state.payment);
      commit(types.STORE_PAYMENT.SUCCESS, response.data);
      trackActionEvent('add_credit_card');
      return Promise.resolve(response);
    } catch (error) {
      commit(types.STORE_PAYMENT.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Replace payment methods information.
   *
   * @param commit
   * @param state
   * @returns {Promise<*|undefined>}
   */
  async replace({commit, state}) {
    commit(types.REPLACE_PAYMENT.PENDING);
    try {
      const response = await api.replace(state.payment);
      commit(types.REPLACE_PAYMENT.SUCCESS, response.data);
      trackActionEvent('add_credit_card');
      return Promise.resolve(response);
    } catch (error) {
      commit(types.REPLACE_PAYMENT.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Show payment method information.
   *
   * @param commit
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async show({commit}, data) {
    commit(types.SHOW_PAYMENT.PENDING);
    try {
      const response = await api.show(data);
      commit(types.SHOW_PAYMENT.SUCCESS, response);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.SHOW_PAYMENT.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Delete payment method information.
   *
   * @param commit
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async delete({commit}, data) {
    commit(types.DELETE_PAYMENT.PENDING);
    try {
      const response = await api.delete(data);
      commit(types.DELETE_PAYMENT.SUCCESS, response);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.DELETE_PAYMENT.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Clear payment object
   *
   * @param state
   */
  clearPayment ({state}) {
    for(let field in state.payment) {
      state.payment[field] = null;
    }
  },

  /**
   * Get order details
   *
   * @param state, orderId
   */
  getOrderInfo: storeHelper.createAction(types.TRANSACTION_DETAILS, {
    call (orderId) {
      return api.loadOrderInfo(orderId);
    },
  }),

  /**
   * Update specific payment method.
   * @param state
   * @param commit
   * @param paymentMethod
   */
  updatePaymentMethod ({ state, commit }, paymentMethod) {
    const paymentMethods = state.paymentMethods;
    const paymentKey = findIndex(paymentMethods, ['id', paymentMethod.id]);
    // If method was found then remove it before pushing the updated one.
    if (paymentKey !== -1) {
      paymentMethods.splice(paymentKey, 1);
    }
    paymentMethods.push(paymentMethod);
    commit('setPaymentMethods', paymentMethods);
  },

  /**
   * Retry payment.
   */
  retryPayment: storeHelper.createAction(types.RETRY_PAYMENT, {
    call: api.retryPayment,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Payment methods was load.
   */
  ...storeHelper.createMutation(types.INDEX_PAYMENT, {
    success(state, payload) {
      state.paymentMethods = payload.data;
      state.userPermissions = payload.permissions;
      state[types.INDEX_PAYMENT.loadingKey] = false;
      state.indexPaymentError = null;
    },
    failure(state, {data}) {
      state[types.INDEX_PAYMENT.loadingKey] = false;
      state[types.INDEX_PAYMENT.ERROR] = data.message !== undefined && data.message.length > 0 ?
        data.message :
        translator.get('payment::messages.cannot_be_obtained');
      state.indexPaymentApplicationError = data.error_type === undefined;
    }
  }),

  /**
   * Payment method was stored.
   *
   * @param state, payload
   */
  [types.STORE_PAYMENT.SUCCESS](state, payload) {
    state.paymentMethods.push(payload.data);
    state.userPermissions = payload.permissions;
    state[types.STORE_PAYMENT.loadingKey] = false;
  },

  /**
   * Payment method is storing.
   *
   * @param state
   */
  [types.STORE_PAYMENT.PENDING](state) {
    state[types.STORE_PAYMENT.loadingKey] = true;
  },

  /**
   * Payment method was not stored.
   *
   * @param state
   */
  [types.STORE_PAYMENT.FAILURE](state) {
    state[types.STORE_PAYMENT.loadingKey] = false;
  },

  /**
   * Payment method was replaced.
   *
   * @param state
   * @param payload
   */
  [types.REPLACE_PAYMENT.SUCCESS](state, payload) {
    state.paymentMethods = [ payload.data ];
    state.userPermissions = payload.permissions;
    state.indexPaymentError = null;
    state[types.REPLACE_PAYMENT.loadingKey] = false;
  },

  /**
   * Payment method is replacing.
   *
   * @param state
   */
  [types.REPLACE_PAYMENT.PENDING](state) {
    state[types.REPLACE_PAYMENT.loadingKey] = true;
  },

  /**
   * Payment method was not replaced.
   *
   * @param state
   */
  [types.REPLACE_PAYMENT.FAILURE](state) {
    state[types.REPLACE_PAYMENT.loadingKey] = false;
  },

  /**
   * Payment method shown.
   *
   * @param state, payload
   */
  [types.SHOW_PAYMENT.SUCCESS](state, payload) {
    state.payment = payload.data.data;
    state[types.SHOW_PAYMENT.loadingKey] = false;
  },

  /**
   * Show payment method is loading.
   *
   * @param state
   */
  [types.SHOW_PAYMENT.PENDING](state) {
    state[types.SHOW_PAYMENT.loadingKey] = true;
  },

  /**
   * Payment method show was not loaded.
   *
   * @param state
   */
  [types.SHOW_PAYMENT.FAILURE](state) {
    state[types.SHOW_PAYMENT.loadingKey] = false;
  },

  /**
   * Payment method deleted.
   *
   * @param state
   */
  [types.DELETE_PAYMENT.SUCCESS](state) {
    state[types.DELETE_PAYMENT.loadingKey] = false;
  },

  /**
   * Show payment method is deleting.
   *
   * @param state
   */
  [types.DELETE_PAYMENT.PENDING](state) {
    state[types.DELETE_PAYMENT.loadingKey] = true;
  },

  /**
   * Payment method show was not deleted.
   *
   * @param state
   */
  [types.DELETE_PAYMENT.FAILURE](state) {
    state[types.DELETE_PAYMENT.loadingKey] = false;
  },

  /**
   * Create mutation for transaction details
   *
   */
  ...storeHelper.createMutation(types.TRANSACTION_DETAILS, {
    success(state, { data }) {
      state.transaction = data;
    },
  }),

  /**
   * Create mutation for retry payment action.
   */
  ...storeHelper.createMutation(types.RETRY_PAYMENT),

  /**
   * Mutation for setting payment methods.
   * @param state
   * @param paymentMethods
   */
  setPaymentMethods (state, paymentMethods) {
    state.paymentMethods = paymentMethods;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


