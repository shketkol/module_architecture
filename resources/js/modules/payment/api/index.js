import axios from 'axios';
import Router from 'Foundation/services/routing/router';
import routes from 'Static/routes.json';

const router = new Router(routes);

const payment = {
  /**
   * Index payment method
   * @param data
   * @returns {AxiosPromise}
   */
  index () {
    return axios.get(router.route('api.payments.payment-method.index'));
  },

  /**
   * Store payment method
   * @param data
   * @returns {AxiosPromise}
   */
  store (data) {
    return axios.post(router.route('api.payments.payment-method.store'), data);
  },

  /**
   * Replace payment method
   * @param data
   * @returns {AxiosPromise}
   */
  replace (data) {
    return axios.patch(router.route('api.payments.payment-method.replace'), data);
  },

  /**
   * Show paymentMethod data
   * @param id
   * @returns {AxiosPromise<any>}
   */
  show (id) {
    return axios.get(router.route('api.payments.payment-method.show', { paymentMethod: id }));
  },

  /**
   * Delete paymentMethod data
   * @param id
   * @returns {AxiosPromise<any>}
   */
  delete (id) {
    return axios.delete(router.route('api.payments.payment-method.destroy', { paymentMethod: id }));
  },

  /**
   * Retry payment.
   * @param id
   * @returns {AxiosPromise<any>}
   */
  retryPayment (id) {
    return axios.post(router.route('api.payments.payment-method.retry', { paymentMethod: id }));
  },

  /**
   * Get order info data
   * @param id
   * @returns {AxiosPromise<any>}
   */
  loadOrderInfo (id) {
    return axios.get(router.route('api.payments.order.show', { orderId: id }));
  },
};

export {
  payment
}
