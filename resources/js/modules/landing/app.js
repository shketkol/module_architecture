import 'Foundation/bootstrap';
import 'core-js/modules/es.array.find';
import 'core-js/modules/es.array.includes';
import LandingPage from './pages/TheLanding';
import store from '../auth/store';
import Vue from 'vue';

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    LandingPage,
  },

  /**
   * Vuex.
   */
  store,
}));
