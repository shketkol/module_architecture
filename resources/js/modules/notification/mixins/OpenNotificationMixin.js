import NotificationModal from 'Modules/notification/components/NotificationModal';

export default {
  /**
   * Child  components.
   */
  components: { NotificationModal },

  /**
   * Component's state.
   */
  data() {
    return {
      notification: null,
    };
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Handles notification modal's opening.
     */
    openNotification (notification) {
      this.notification = notification;
    },
  },
}
