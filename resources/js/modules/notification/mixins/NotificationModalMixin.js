export default {
  /**
   * Component's state.
   */
  data() {
    return {
      notification: null,
    };
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Set notification data.
     */
    setNotificationData(text, id, isRead) {
      this.notification = {text, id, isRead};
    },
  },
}
