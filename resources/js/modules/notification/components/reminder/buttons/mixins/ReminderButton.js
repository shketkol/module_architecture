import { get } from 'lodash';

export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * Reminder object.
     */
    reminder: {
      required: true,
      type: Object,
    },

    /**
     * Call to acction button object.
     */
    cta: {
      required: true,
      type: Object,
    },
  },

  computed: {
    /**
     * Get CTA title if exist.
     * @returns {(string|undefined)}
     */
    ctaTitle () {
      return get(this.cta, 'title')
    },
  },
};
