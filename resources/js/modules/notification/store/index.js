import notifications from './modules/notifications';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    notifications,
  },
});

export default store;
