import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { notifications as api } from '../../api';
import NotificationsMapper from '../../api/mappers/NotificationsMapper';
import { isArray, findIndex } from 'lodash';

/**
 * Default state for notifications widget.
 */
const defaultNotificationsWidget = {
  notified: null,
  list: null,
  total: null,
};

/**
 * Types
 */
const types = {
  LOAD_NOTIFICATIONS: storeHelper.createAsyncMutation('LOAD_NOTIFICATIONS'),
  MARK_READ: storeHelper.createAsyncMutation('MARK_READ'),
  MARK_ALL_READ: storeHelper.createAsyncMutation('MARK_ALL_READ'),
  MARK_NOTIFIED: storeHelper.createAsyncMutation('MARK_NOTIFIED'),
  DELETE_REMINDER: storeHelper.createAsyncMutation('DELETE_REMINDER'),
};

/**
 * State
 */
const state = {
  /**
   * Create state for mutation.
   */
  ...storeHelper.createMutationState([
    types.LOAD_NOTIFICATIONS,
    types.MARK_ALL_READ,
    types.DELETE_REMINDER,
  ]),

  /**
   * Notifications.
   */
  notificationsWidget: { ...defaultNotificationsWidget },

  /**
   * Reminders.
   */
  reminders: [],

  /**
   * Notifications id to read.
   */
  notificationIdToRead: null,
};

/**
 * Getters.
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Load last notifications from API.
   */
  loadNotifications: storeHelper.createAction(types.LOAD_NOTIFICATIONS, {
    call: api.lastNotifications.get,
    map: NotificationsMapper.mapWidgetsDataToClient,
  }),

  /**
   * Marks notification as read.
   * @param commit
   * @param id
   */
  markNotificationAsRead: storeHelper.createAction(types.MARK_READ, {
    call (id) {
      return api.markAsRead.patch(id);
    },
    after ({ commit, dispatch }, payload, id) {
      dispatch('loadNotifications');

      commit('readNotifications', id);
    },
  }),

  /**
   * Marks all notifications as read.
   * @param commit
   * @param id
   */
  markAllNotificationAsRead: storeHelper.createAction(types.MARK_ALL_READ, {
    call () {
      return api.markAllAsRead.patch();
    },
    after ({ commit, dispatch }) {
      dispatch('loadNotifications');
      commit('readAllNotifications');
    },
  }),

  /**
   * Marks widget as read.
   * @param commit
   * @param id
   */
  markAsNotified: storeHelper.createAction(types.MARK_NOTIFIED, {
    call () {
      return api.userNotified.patch();
    }
  }),

  /**
   * @param {number} reminderId
   */
  deleteReminder: storeHelper.createAction(types.DELETE_REMINDER, {
    call (reminderId) {
      return api.deleteReminder(reminderId);
    },
  }),

  /**
   * Clear notification id.
   * @param commit
   */
  clearNotificationIdToRead({ commit }) {
    commit('clearNotificationIdToRead');
  },
};

/**
 * Mutations
 */
const mutations = {

  /**
   * Clear notification id.
   * @param state
   */
  clearNotificationIdToRead(state) {
    state.notificationIdToRead = null;
  },

  /**
   * Marks notification as read.
   */
  readNotifications(state, id) {
    if (isArray(state.notificationsWidget.list)) {
      state.notificationsWidget.list = state.notificationsWidget.list.map(notification => {
        return {
          ...notification,
          is_read: notification.id === id || notification.is_read,
        };
      });
    }
    state.notificationIdToRead = id;
  },

  /**
   * Marks all notifications as read.
   */
  readAllNotifications(state) {
    if (isArray(state.notificationsWidget.list)) {
      state.notificationsWidget.list = state.notificationsWidget.list.map(notification => {
        return {
          ...notification,
          is_read: true,
        };
      });
    }
  },

  /**
   * Show schedule report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.MARK_NOTIFIED, {
    success(state, { data }) {
      state.notificationsWidget.notified = data.notified;
    }
  }),

  /**
   * Load notification mutation.
   */
  ...storeHelper.createMutation(types.LOAD_NOTIFICATIONS, {
    success(state, { notifications, reminders }) {
      state.notificationsWidget = notifications;
      state.reminders = reminders;
    },
  }),

  /**
   * Mark notification as read.
   */
  ...storeHelper.createMutation(types.MARK_READ),

  /**
   * Mark notification as read.
   */
  ...storeHelper.createMutation(types.MARK_ALL_READ),

  /**
   * Basic mutations for deleting reminders.
   */
  ...storeHelper.createMutation(types.DELETE_REMINDER, {
    success(state, {data: {deleted_id}}) {
      const keyToDelete = findIndex(state.reminders, reminder => reminder.id === deleted_id);
      if (keyToDelete !== -1) {
        state.reminders.splice(keyToDelete,1);
      }
    },
  }),

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};
