import axios from "axios";
import route from 'Foundation/services/routing/route'

const notifications = {
  /**
   * Fetch last notifications.
   */
  lastNotifications: {
    get() {
      return axios.get(route('api.notifications.widget'));
    },
  },

  /**
   * Mark notification as read.
   */
  markAsRead: {
    patch(notification) {
      return axios.patch(route('api.notifications.read', { notification }));
    },
  },

  /**
   * Mark notification as read.
   */
  markAllAsRead: {
    patch() {
      return axios.patch(route('api.notifications.read.all'));
    },
  },

  /**
   * Mark widget as read.
   */
  userNotified: {
    patch() {
      return axios.patch(route('api.notifications.user-notified'));
    }
  },

  /**
   * Delete the specified resource.
   *
   * @param {number} reminder
   * @returns {AxiosPromise}
   */
  deleteReminder(reminder) {
    return axios.delete(route('api.notifications.delete-reminder', {reminder}));
  },
};

export {
  notifications,
};
