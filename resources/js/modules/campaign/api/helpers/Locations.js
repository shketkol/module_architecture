import { find, orderBy} from "lodash";

export default class Locations {
  /**
   * Returns sorted locations with failed locations on the top.
   * @param data
   * @param values
   * @returns {[]}
   */
  static sortValues (data, values) {
    data.forEach((item) => {
      let element = null;
      if (item.label) {
        element = find(values, ['label', item.label]);
      } else {
        element = find(values, ['label', item]);
      }

      if (element === undefined) {
        values.push(item)
      }
    });

    return orderBy(values, ['label'], ['asc']);
  }
}
