import { valueTypes } from '../../helpers/revision';

export default class TargetingMapper {
  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapAudiencesObjectToApi (data) {
    return data.map((item) => {
      return { audience_id: item.value }
    });
  }

  /**
   * Returns object that should be used by client.
   *
   * @param data
   * @returns {{}}
   */
  static mapAudiencesObjectToClient (data) {
    return data.map((item) => {
      return {
        value: item.id,
        name: item.name,
        path: item.path,
        type: item.type,
      }
    });
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapGendersOptions (data) {
    let genders = [];

    data.forEach(function(item) {
      const index = genders.findIndex(function(element) {
        return element.id === item.gender.id
      })

      if (index === -1) {
        let gender = item.gender;
        gender.ages = [];

        genders.push(gender);

        genders[genders.length - 1].ages.push(item);
      } else {
        genders[index].ages.push(item);
      }
    })

    return genders.map((obj) => {
      return{
        value: obj.id,
        label: obj.name,
        ages:  obj.ages
      };
    });
  }

  /**
   * Returns object that received from BE
   *
   * @param data
   * @returns {{}}
   */
  static mapChosenGenderOption (data) {
    return data.map((obj) => {
      return {
        value: obj.id,
        label: obj.name,
        type: obj.type
      }
    });
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapGendersObjectToApi (data) {
    return data
  }

  /**
   * Returns object that should be used in application.
   *
   * @param data
   * @returns {{}}
   */
  static mapGroupsOptions (data) {
    return data.map((obj) => {
      return {
        value: obj.id,
        label: obj.name,
        type: obj.type
      }
    });
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapGroupsObjectToApi (data) {
    return data.map((obj) => {
      return obj.value;
    });
  }

  /**
   * Returns object that should be used in application.
   *
   * @param data
   * @returns {{}}
   */
  static mapPlatformsOptions (data) {
    return data.map((obj) => {
      return obj;
    });
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapPlatformsObjectToApi (data) {
    return data.map((obj) => {
      return obj;
    });
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapLocationsOptions (options) {
    return options.map(option => ({
        value: option.id ? option.id : null,
        label: option.name ? option.name : option,
        type: option.type ? option.type : valueTypes.NEW,
        error: !option.id,
        included: true
      })
    );
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapLocationsObjectToApi (data) {
    return data.map(({ included, value }) => {
      return {
        location_id: value,
        excluded: new Number(!included),
      };
    });
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapZipObjectToApi (data) {
    return data.map(({ included, value }) => {
      return {
        zipcode_id: value,
        excluded: new Number(!included),
      };
    });
  }

  /**
   * Returns object that should be used in select.
   *
   * @param data
   * @returns {{}}
   */
  static mapGenresOptions (data) {
    return data.map((option) => {
      return {
        value: option.id,
        label: option.name
      };
    });
  }

  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapGenresObjectToApi (data) {
    return data.map((option) => {
      return {
        excluded: new Number(!option.included),
        genre_id: option.value,
      };
    });
  }

  /**
   * Returns object that should be used as chosen value.
   *
   * @param data
   * @returns {{}}
   */
  static mapChosenGenresObject (data) {
    return data.map((option) => {
      return {
        included: !option.excluded,
        value: option.id,
        label: option.name,
        type: option.type
      };
    });
  }

  /**
   * Returns object that should be used as chosen value.
   *
   * @param data
   * @returns {{}}
   */
  static mapChosenLocationsObject (data) {
    return data.map((option) => {
      return {
        included: !option.excluded,
        value: option.id,
        label: option.name,
        error: false,
        type: option.type
      };
    });
  }

  /**
   * Map device groups obtained from index endpoint.
   *
   * @param data
   * @returns {Array}
   */
  static mapDeviceGroupsOptionsToClient (data) {
    return TargetingMapper.mapDeviceGroupsToClient(data.data);
  }

  /**
   * Map device groups for FE needs.
   *
   * @param data
   * @returns {Array}
   */
  static mapDeviceGroupsToClient (data) {
    return data.map((option) => {
      return {
        value: option.id,
        name: option.name,
        label: option.label,
        description: option.description,
        type: option.type
      };
    });
  }

  /**
   * Returns device objects that should be sent to API.
   *
   * @param data
   * @returns {Array}
   */
  static mapDevicesObjectToApi (data) {
    return data.map((option) => {
      return {
        device_group_id: option.value,
        excluded: 0,
      };
    });
  }

  /**
   * Fill targeting values
   *
   * @param state
   * @param data
   * @returns {{}}
   */
  static fillValues (state, data) {
    state.gender = (Array.isArray(data.gender) && data.gender.length > 0)
      ? TargetingMapper.mapChosenGenderOption(data.gender)
      : state.gender;
    state.ages = data.ages ? TargetingMapper.mapGroupsOptions(data.ages) : state.ages;

    state.genres = data.genres ? TargetingMapper.mapChosenGenresObject(data.genres) : state.genres;
    state.deviceGroups = data.deviceGroups ? TargetingMapper.mapDeviceGroupsToClient(data.deviceGroups) : state.deviceGroups;
    state.audiences = data.audiences ? TargetingMapper.mapAudiencesObjectToClient(data.audiences) : state.audiences;

    state.locations.zip = data.zipcodes ?
      TargetingMapper.mapChosenLocationsObject(data.zipcodes) :
      state.locations.zip;

    state.locations.city = data.locations ?
      TargetingMapper.mapChosenLocationsObject(data.locations) :
      state.locations.city;
    return state;
  }
}
