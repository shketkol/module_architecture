import { formatToApi } from 'Foundation/helpers/moment';

export default class CampaignMapper {
  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapObjectToApi (data) {
    return {
      id: data.id,
      name: data.name,
      date: data.date ? {
        start: formatToApi(data.date.start),
        end: formatToApi(data.date.end),
      } : undefined,
      timezone_id: data.timezone_id,
      cost: data.cost,
      discounted_cost: data.discounted_cost,
      cpm: data.cpm,
      discounted_cpm: data.discounted_cpm,
      impressions: data.impressions,
      promocode: data.promocode,
    };
  }

  /**
   * Returns object that should be used in application.
   *
   * @param data
   * @returns {{}}
   */
  static mapApiToObject ({ data }) {
    const campaign = data;
    return Object.assign({}, campaign, {
      date: {
        start: campaign.date_start ? campaign.date_start : null,
        end: campaign.date_end ? campaign.date_end : null,
      },
    });
  }
}
