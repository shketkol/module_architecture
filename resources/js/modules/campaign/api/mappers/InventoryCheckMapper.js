import { get } from 'lodash';

export default class InventoryCheckMapper {
  /**
   * Returns object that should be used in application.
   *
   * @param data
   * @returns {{}}
   */
  static mapApiToObject ({ data }) {
    return data.data;
  }

  /**
   * Returns object that should be used in application.
   *
   * @param data
   * @returns {{}}
   */
  static mapApiToObjectDynamic (data) {
    return {
      id: get(data, 'id', null),
      data: {
        type: get(data, 'data.type'),
        message: get(data, 'data.message'),
        value: get(data, 'data.value'),
        indicatorPosition: get(data, 'data.indicator_position', 0),
      },
      suggestions: {
        message: get(data, 'suggestions.message'),
        action: {
          steps: get(data, 'suggestions.action.steps', {}),
          budget: get(data, 'suggestions.action.budget'),
          value: get(data, 'suggestions.action.value'),
        },
        payload: get(data, 'suggestions.payload', null),
        help: get(data, 'suggestions.help', {}),
      },
      updated: get(data, 'updated'),
    }
  }
}

