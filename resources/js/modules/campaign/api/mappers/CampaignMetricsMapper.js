import { filter } from "lodash";
import { getDeviceImage } from "Foundation/components/chart/icons/devices";

export default class CampaignMetricsMapper {
  /**
   * Returns object that should be used in application.
   *
   * @param data
   * @returns {{}}
   */
  static mapDevicesApiToObject ( { data }) {
    data = data.generalImpressionsByDevice;

    data = filter(data, item => item.percentage > 0);

    const mappedData = {
      labels: [],
      datasets: [
        {
          backgroundColor: [],
          data: [],
        }
      ],
      custom: {
        images: []
      }
    };

    data.forEach(item => {
      const image = item.image;
      image.image = getDeviceImage(item.key);
      mappedData.labels.push(item.label);
      mappedData.datasets[0].backgroundColor.push(item.color);
      mappedData.datasets[0].data.push(item.percentage);
      mappedData.custom.images.push(image);
    });

    return mappedData;
  }

  /**
   * Returns object that should be used in application.
   *
   * @param data
   * @returns []
   */
  static mapGenresApiToObject ({ data }) {
    return data.generalImpressionsByGenre.map(item => ({
      title: item.name,
      percentage: item.percentage,
      displayValue: item.percentage + '%',
    }));
  }
}
