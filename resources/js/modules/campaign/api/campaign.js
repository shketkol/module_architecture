import axios from 'axios';
import route from './../../../foundation/services/routing/route';
import qs from 'qs';
import TargetingMapper from "Modules/campaign/api/mappers/TargetingMapper";

const CancelToken = axios.CancelToken;
let inventoryCheckGetRequestCancelToken;

export default {
  /**
   * Method cancels inventory check Get request
   */
  cancelInventoryCheckGetRequest() {
    inventoryCheckGetRequestCancelToken();
  },

  /**
   * Store new campaign.
   * @param data
   * @return {AxiosPromise<any>}
   */
  store (data) {
    return axios.post(route('api.campaigns.store'), data);
  },

  /**
   * Get all information about the campaign.
   * @param id
   * @return {AxiosPromise<any>}
   */
  show (id) {
    return axios.get(route('api.campaigns.details.show', { campaign: id }));
  },

  /**
   * Get campaign permissions.
   *  @param campaign
   *  @return {AxiosPromise<any>}
   */
  getPermissions (campaign) {
    return axios.get(route('api.campaigns.permissions', { campaign }));
  },

  statistic: {
    /**
     * Get statistic (Cost, Performance and Delivered impressions) of the campaign.
     * @param id
     * @return {AxiosPromise<any>}
     */
    show (id) {
      return axios.get(route('api.campaigns.details.statistic.show', { campaign: id }));
    },
  },

  /**
   * Get frequency data.
   */
  frequency: {
    /**
     * Get frequency of the campaign.
     * @param id
     * @return {AxiosPromise<any>}
     */
    show (id) {
      return axios.get(route('api.campaigns.frequency', { campaign: id }));
    },
  },

  metrics: {
    /**
     * Get campaigns impressions by genre.
     * @param id
     * @return {AxiosPromise<any>}
     */
    genres (id) {
      return axios.get(route('api.campaigns.metrics.genres', { campaign: id }));
    },

    /**
     * Get campaigns impressions by device.
     * @param id
     * @return {AxiosPromise<any>}
     */
    devices (id) {
      return axios.get(route('api.campaigns.metrics.devices', { campaign: id }));
    },
  },

  /**
   * Load info for edit page.
   * @param id
   * @return {AxiosPromise<any>}
   */
  edit (id) {
    return axios.get(route('api.campaigns.edit', { campaign: id }));
  },

  /**
   * Update full campaign
   */
  update () {
    //
  },

  /**
   * Clone campaign
   * @param id
   * @return Promise
   */
  clone (id) {
    return axios.get(route('api.campaigns.clone', { campaign: id }));
  },

  /**
   * Remove campaign
   * @param id
   * @return Promise
   */
  remove (id) {
    return axios.delete(route('api.campaigns.delete', { campaign: id }));
  },

  /**
   * Validate campaign.
   *
   * @param id
   * @returns {AxiosPromise<any>}
   */
  validate (id) {
    return axios.get(route('api.campaigns.validate', { campaign: id }));
  },

  /**
   * name section
   */
  name: {
    /**
     * Update name.
     * @param id
     * @param data
     * @return {AxiosPromise<any>}
     */
    update (id, data) {
      return axios.patch(route('api.campaigns.details.name.update', { campaign: id }), data);
    },
  },

  /**
   * schedule section
   */
  schedule: {
    /**
     * Update schedule.
     * @param id
     * @param data
     * @return {AxiosPromise<any>}
     */
    update (id, data) {
      return axios.patch(route('api.campaigns.details.schedule.update', { campaign: id }), data);
    },
  },

  /**
   * Targeting section.
   */
  targeting: {
    /**
     * Ages.
     */
    ages: {
      /**
       * Get all ages.
       *
       * @param id
       * @returns {AxiosPromise<any>}
       */
      show (id) {
        return axios.get(route('api.campaigns.targeting.ages', { campaign: id }));
      },

      /**
       * Get age groups targeting list depending on gender.
       *
       * @param campaignId
       * @param ageGroups
       * @returns {AxiosPromise<any>}
       */
      get (campaignId, ageGroups) {
        return axios.get(
          route('api.campaigns.targeting.ages', {
            campaign: campaignId,
            'genderId': TargetingMapper.mapGendersObjectToApi(ageGroups)
          })
        );
      },

      /**
       * Update campaign ages.
       *
       * @param id
       * @param ageGroups
       * @returns {AxiosPromise<any>}
       */
      update (id, ageGroups) {
        return axios.patch(route('api.campaigns.targeting.ages.update', { campaign: id}),
            {
              ageGroups: ageGroups.ages,
              genderId: ageGroups.gender,
              paramsSerializer: params => {
                return qs.stringify(params)
              }
            }
        );
      },
    },

    /**
     * Locations.
     */
    locations: {
      /**
      * Search locations
      * @return {AxiosPromise<any>}
      */
      searchLocations (campaignId, query) {
        return axios.get(
          route('api.campaigns.targeting.locations', {
            campaign: campaignId,
            query
          }),
        );
      },

      /**
       * Search zip codes
       * @return {AxiosPromise<any>}
       */
      searchZip (campaign, zipcodes) {
        return axios.post(
          route('api.campaigns.targeting.zipcodes', { campaign }),
          { zipcodes: zipcodes }
        );
      },

      /**
       * Upload CSV file
       * @return {AxiosPromise<any>}
       */
      uploadCsv (campaign, data) {
        return axios.post(
          route('api.campaigns.targeting.upload.csv', { campaign }),
          data,
          {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }
        );
      },

      /**
       * Update campaign locations.
       *
       * @param campaign
       * @param zips
       * @param cities
       * @returns {AxiosPromise<any>}
       */
      update (campaign, zips, cities) {
        return axios.patch(route('api.campaigns.targeting.locations.update', { campaign, }),
          {
            zipcodes: zips,
            locations: cities,
          }
        );
      },
    },

    /**
     * Genres.
     */
    genres: {
      /**
       * Get genres targeting list
       * @return {AxiosPromise<any>}
       */
      search (campaignId, needle) {
        return axios.get(route('api.campaigns.targeting.genres', {
          campaign: campaignId,
          query: needle
        }));
      },
      /**
       * Update campaign genres.
       *
       * @param campaign
       * @param values
       * @returns {AxiosPromise<any>}
       */
      update (campaign, values) {
        return axios.patch(
              route('api.campaigns.targeting.genres.update', { campaign: campaign }),
              {genres: values}
            );
      },
    },

    /**
     * Audiences.
     */
    audiences: {
      /**
       * Get genres targeting list
       * @return {AxiosPromise<any>}
       */
      get (campaignId) {
        return axios.get(route('api.campaigns.targeting.audiences', {
          campaign: campaignId,
        }));
      },

      /**
       * Update campaign audiences.
       *
       * @param id
       * @param values
       * @returns {AxiosPromise<any>}
       */
      update (id, values) {
        return axios.patch(
          route('api.campaigns.targeting.audiences.update', { campaign: id }),
          {audiences: values}
        );
      },
    },

    /**
     * Devices.
     */
    devices: {
      /**
       * Get devices targeting list
       *
       * @return {AxiosPromise<any>}
       */
      get(campaignId) {
        return axios.get(
          route('api.campaigns.targeting.devices', { campaign: campaignId })
        );
      },

      /**
       * Update campaign devices.
       *
       * @param campaign
       * @param values
       * @returns {AxiosPromise<any>}
       */
      update (campaign, values) {
        return axios.patch(
          route('api.campaigns.targeting.devices.update', { campaign: campaign }),
          {values: values}
        );
      },
    },
  },

  /**
   * Budget section
   */
  budget: {
    /**
     * CPM
     */
    cpm: {
      /**
       * Get CPM for the campaign.
       * @return {AxiosPromise<any>}
       */
      get (id) {
        return axios.get(route('api.campaigns.budget.cpm.show', { campaign: id }));
      },
    },

    /**
     * Update budget.
     * @param id
     * @param data
     * @return {AxiosPromise<any>}
     */
    update (id, data) {
      return axios.patch(route('api.campaigns.budget.update', { campaign: id }), data);
    },

    /**
     * Get Billing date
     */
    billingDate: {
      /**
       * Get predicted value.
       * @param id
       * @return {AxiosPromise<any>}
       */
      get (id) {
        return axios.get(
          route('api.campaigns.billingDate.get', { campaign: id })
        );
      },
    },

  },

  /**
   * Inventory Check
   */
  inventoryCheck: {
    /**
     * Get predicted value.
     * @param id
     * @return {AxiosPromise<any>}
     */
    get (id) {
      return axios.get(
        route('api.campaigns.inventoryCheck.get', { campaign: id }),
        {
          cancelToken: new CancelToken(cancelFunction => {
            // An executor function receives a cancel function as a parameter
            inventoryCheckGetRequestCancelToken = cancelFunction;
          })
        }
      );
    },

    /**
     * Get dynamic inventory data.
     * @param id
     * @return {AxiosPromise<any>}
     */
    getDynamic (id) {
      return axios.get(route('api.campaigns.dynamicInventoryCheck.get', { campaign: id }))
    },
  },

  /**
   * Order campaign.
   * @param campaignId
   * @param paymentId
   * @return {AxiosPromise<any>}
   */
  order ({ campaignId, paymentId }) {
    return axios.post(route('api.campaigns.order', { campaign: campaignId }), {paymentId: paymentId});
  },

  /**
   * Update campaign.
   * @param campaignId
   * @return {AxiosPromise<any>}
   */
  updateCampaign ({ campaignId }) {
    return axios.post(route('api.campaigns.update', { campaign: campaignId }));
  },

  /**
   * Campaign status
   */
  status: {
    /**
     * Update campaign status
     * @param id
     * @param status
     */
    update ({ id, status }) {
      return axios.patch(route(`api.campaigns.${status}`, { campaign: id }));
    }
  },

  /**
   * Creative.
   */
  creative: {
    /**
     * Step.
     */
    step: {
      /**
       * Set creative step is completed.
       *
       * @param id
       * @returns {AxiosPromise<any>}
       */
      update (id) {
        return axios.patch(route('api.campaigns.creative.step.update', { campaign: id }));
      },
    },
  },

  /**
   * Campaign promocode
   */
  promocode: {
    /**
     * Validate campaign promocode
     * @param id
     * @param promocode
     */
    validate (id, promocode) {
      return axios.post(route('api.campaigns.promocode.validate', {campaign: id}), {promocode: promocode});
    },

    /**
     * Delete applied promocode in the campaign
     * @param id
     */
    delete (id) {
      return axios.post(route('api.campaigns.promocode.delete', {campaign: id}));
    }
  },

  /**
   * Send campaign activity step for analytic
   * @param data
   */
  sendActivity (data) {
    return axios.post(route('api.campaigns.activity', {campaign: data.id}), {
      action_name: data.action_name,
      last_page: data.last_page
    });
  },

  /**
   * Get revision by id.
   * @param id
   * @return {AxiosPromise<any>}
   */
  getRevision (id) {
    return axios.get(route('api.campaigns.revision.show', { revision: id }));
  },
};
