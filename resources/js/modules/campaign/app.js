import 'Foundation/bootstrap';
import 'Foundation/services/plyr';
import AdminIndexPage from './pages/admin/TheAdminIndex';
import CreatePage from './pages/TheCreate';
import EditPage from './pages/TheEdit';
import IndexPage from './pages/TheIndex.vue';
import routes from './router/routes';
import { getRouter } from 'Foundation/services/routing/factory';
import store from './store';
import Vue from 'vue';

(new Vue({

  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * App name.
   */
  name: 'Campaign',

  /**
   * Child components.
   */
  components: {
    AdminIndexPage,
    CreatePage,
    EditPage,
    IndexPage,
  },

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),

  /**
   * Store
   */
  store,
}));
