import {isSafari} from "Foundation/helpers/browser-detect";

export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Temporary fix for campaigns headers shifting in Safari.
     */
    onTableLoaded () {
      if (isSafari) {
        const tableHeader = document.querySelector('.campaigns__holder .el-table__header');
        if (tableHeader) {
          tableHeader.style.tableLayout = 'initial';
          setTimeout(() => tableHeader.style.tableLayout = 'fixed', 10);
        }
      }
    },
  }
};
