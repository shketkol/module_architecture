import { mapState } from 'vuex';

export default {
  /**
   * Computed props.
   */
  computed: {
    /**
     * Map state.
     */
    ...mapState('details', { 'campaignDetails': state => state.campaign }),
    ...mapState('states', { campaignStates: 'states' }),

    ...mapState('metrics', {
      campaignStateImpressions: state => state.campaignStatistic.deliveredImpressions,
    }),

    /**
     * True if user can edit the campaign.
     * @returns {boolean}
     */
    canEdit () {
      return this.campaignStates.can_edit;
    },

    /**
     * True if use can delete the campaign.
     * @returns {boolean}
     */
    canDelete () {
      return this.campaignStates.can_delete;
    },

    /**
     * Check if the campaign is in status (paused, canceled).
     * @returns {boolean}
     */
    canShowStatusChangedAt () {
      return ['paused', 'canceled'].includes(this.campaignDetails.status);
    },

    /**
     * Check if campaign report can be downloaded.
     * @returns {boolean}
     */
    canDownloadReport () {
      return (this.campaignStates.can_download_report && this.campaignStateImpressions > 0);
    },

    /**
     * Check if the campaign has delivered impressions.
     * @returns {boolean}
     */
    statisticAvailable () {
      return this.campaignStates.can_download_report;
    },
  },
};
