import { isEmpty } from 'lodash';

/**
 * Revisions value types.
 */
export const valueTypes = {
  NEW: 'new',
  CURRENT: 'current',
  DELETED: 'deleted'
};

/**
 * Get diff between 2 arrays by prop.
 */
export const differenceWithByProp = (firstArray, secondArray, prop) => {
  return firstArray.filter(first => secondArray.filter(second => second[prop] === first[prop]).length === 0);
};

/**
 * Get current values with current or new type.
 */
export const getCurrentValues = (data) => {
  if (isEmpty(data)) {
    return data;
  }

  return data.filter((item) => {
    return item.type === valueTypes.CURRENT || item.type === valueTypes.NEW;
  })
}

/**
 * Create first letter uppercase.
 */
export const uppercaseFirstLetter = (value) => {
  return value.charAt(0).toUpperCase() + value.slice(1);
}

export default {
  valueTypes,
  uppercaseFirstLetter,
  differenceWithByProp,
  getCurrentValues
}
