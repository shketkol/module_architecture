export default {
  /**
   * Props.
   */
  props: {
    /**
     * Daily impressions display value.
     */
    dailyImpressions: {
      type: Number,
      default: 0,
    },

    /**
     * Min daily impressions display value.
     */
    minDailyImpressions: {
      type: Number,
      default: 0,
    },

    /**
     * Suggested budget display value.
     */
    suggestedBudget: {
      type: String,
      default: '0',
    },

    /**
     * Suggested schedule duration.
     */
    suggestedScheduleDuration: {
      type: Number,
      default: 0,
    },

    /**
     * If list should be shown.
     */
    showList: {
      type: Boolean,
      default: true,
    },

    /**
     * If learn more list should be shown.
     */
    showLearnMore: {
      type: Boolean,
      default: false,
    },

    /**
     * Current step
     */
    step: {
      type: String,
      default: '',
    }
  },

  /**
   * Props.
   */
  methods: {
    /**
     * Handles budget navigation click.
     */
    handleBudgetNavigation (event) {
      event.preventDefault();
      this.$emit('handleBudgetNavigation', event);
    },

    /**
     * Handles schedule navigation click.
     */
    handleScheduleNavigation (event) {
      event.preventDefault();
      this.$emit('handleScheduleNavigation', event);
    },

    /**
     * Handles Learn more navigation click..
     */
    handleLearnMoreNavigation () {
      this.$emit('handleLearnMoreNavigation');
    }
  }
};
