import {mapActions, mapState} from 'vuex';
import { mapFields } from "vuex-map-fields";
import { calculateImpressions } from 'Foundation/helpers/cost';

export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Map fields.
     */
    ...mapFields('wizard/budget', ['cpm']),
    ...mapState('wizard/steps', ['groups']),

    /**
     * Returns cost in correct format.
     *
     * @returns {string}
     */
    formattedBudget () {
      return this.$formatCurrency(this.roundedBudget, this.$user.currency.code);
    },

    /**
     * Rounded budget should be used.
     */
    roundedBudget () {
      return this.$roundCurrency(this.availableBudget);
    },

    /**
     * Route for "Budget" step.
     */
    budgetRoute () {
      return this.groups.budget.items.budget.route;
    },
  },

  /**
   * Component methods
   */
  methods: {
    /**
     * Map actions.
     */
    ...mapActions('wizard/budget', ['updateCostWithImpressions']),

    /**
     * Update budget data.
     */
    async updateBudgetData() {
      await this.updateCostWithImpressions({
        cost: this.roundedBudget,
        impressions: calculateImpressions(this.roundedBudget, this.cpm),
        cpm: this.cpm,
      });
    }
  }
};
