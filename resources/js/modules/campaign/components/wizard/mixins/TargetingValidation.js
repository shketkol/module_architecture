import { mapActions, mapGetters, mapMutations, mapState } from 'vuex';
import ActionConfirmation from 'Foundation/components/modals/ActionConfirmation';
import Vue from 'vue';

export default {
  /**
   * Common data.
   */
  data () {
    return {
      confirmationModal: null,
      watcher: null,
    }
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex (getters).
     */
    ...mapGetters('wizard/targeting', ['targetingError']),
    ...mapState('wizard', {campaignId: 'id'}),
  },

  /**
   * created
   */
  created () {
    this.watcher = this.$watch('targetingError', function (value) {
      if (value && value.message !== undefined) {
        if (!this.confirmationModal) {
          this.initConfirmationModal()
        } else {
          this.confirmationModal.visible = true
        }
      }
    }, {deep:true})
  },

  /**
   * deactivated.
   */
  deactivated() {
    this.onDestroy()
  },

  /**
   * Component methods
   */
  methods: {
    /**
     * Vuex (actions).
     */
    ...mapActions('wizard', ['refreshCampaign',]),
    ...mapActions('wizard/inventory', ['getDynamicInventoryCheck']),

    /**
     * Vuex (mutations).
     */
    ...mapMutations('wizard/targeting', [
      'clearTargetingErrors',
    ]),

    /**
     * Mount confirmation modal into DOM.
     */
    initConfirmationModal () {
      const template = Vue.extend(ActionConfirmation);

      this.confirmationModal = new template({
        propsData: {
          isValidationWithAction: true,
          title: this.$trans('targeting::labels.targeting_options'),
          visible: true,
          route:  this.targetingError.route,
          onlySubmit: true,
          submitLabel: this.$trans('actions.got_it'),
        },
      });

      const slotNode = this.confirmationModal.$createElement(
        Vue.compile(`<div class="change-status__content">${this.targetingError.message}</div>`)
      );

      this.confirmationModal.$slots.default = [slotNode];
      this.confirmationModal.$on('submitted', this.handleRemoveTargeting);
      this.confirmationModal.$on('close', this.closeModal);
      this.confirmationModal.$mount();
    },

    /**
     * Close modal without confirmation handler
     */
     closeModal () {
      this.clearTargetingErrors();
      this.confirmationModal.visible = false;
    },

    /**
     * Remove targetings handler
     */
    async handleRemoveTargeting() {
      this.clearTargetingErrors();
      await this.refreshCampaign();
      this.$call(this.getDynamicInventoryCheck(this.campaignId), {muteException: true, notify: false})
    },

    /**
     * destroy watcher
     */
    onDestroy () {
      this.watcher();
      if (this.confirmationModal) {
        this.confirmationModal.$destroy();
      }
    },
  }
};
