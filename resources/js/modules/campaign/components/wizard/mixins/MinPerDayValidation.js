import { mapGetters, mapState } from 'vuex';
import { calculateCost} from 'Foundation/helpers/cost';
import Vue from 'vue';
import UnderMinPerDayContent from '../common/UnderMinPerDayContent';
import ActionConfirmation from 'Foundation/components/modals/ActionConfirmation';
import { Notification } from 'Foundation/components/notice/index';

export default {
  /**
   * Common data.
   */
  data () {
    return {
      confirmationModal: null,
    }
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Map vuex state.
     */
    ...mapState('wizard/budget', ['impressions', 'cpm', 'minDailyImpressions']),

    /**
     * Map vuex getters.
     */
    ...mapGetters('wizard/details', ['getCampaignDuration']),
    ...mapGetters('wizard/steps', ['getStep']),
    ...mapGetters('wizard/budget', ['dailyImpressions', 'isUnderMinPerDay']),

    /**
     * Suggested schedule duration.
     */
    suggestedScheduleDuration () {
      return +Math.floor(this.impressions / this.minDailyImpressions);
    },

    /**
     * Suggested budget.
     */
    suggestedBudget () {
      return this.$formatCurrency(
        calculateCost(this.cpm, this.getCampaignDuration * this.minDailyImpressions),
        this.$user.currency.code
      );
    },
  },

  methods: {
    /**
     * Prepare content to render.
     */
    createUnderMinPerDayContent (options = { showList: true, showLearnMore: false }) {

      Vue.component('UnderMinPerDayContent', UnderMinPerDayContent);

      return this.$root.$createElement(
        Vue.compile(
          '<under-min-per-day-content ' +
          `:suggested-schedule-duration=${this.suggestedScheduleDuration} ` +
          `:min-daily-impressions=${this.minDailyImpressions} ` +
          `suggested-budget="${this.suggestedBudget}" ` +
          `:daily-impressions=${this.dailyImpressions} ` +
          `:show-list=${options.showList} ` +
          `:show-learn-more=${options.showLearnMore} ` +
          ' />'
        )
      );
    },

    /**
     * Inventory warning modal initialization
     */
    initInventoryWarningModal () {
      const template = Vue.extend(ActionConfirmation);

      this.confirmationModal = new template({
        propsData: {
          title: this.$trans('campaign::messages.low_average_impressions'),
          visible: true,
          onlySubmit: true,
          submitLabel: this.$trans('actions.got_it'),
          customClass: 'under-min-impressions'
        },
      });

      const slotNode = this.createUnderMinPerDayContent();

      this.confirmationModal.$slots.default = [slotNode];
      this.confirmationModal.$on('close', this.closeModal);
      this.confirmationModal.$on('submit', this.closeModal);
      this.confirmationModal.$mount();
      this.$nextTick(() => {
        slotNode.componentInstance.$children[0].$on('handleBudgetNavigation', this.handleBudgetNavigation);
        slotNode.componentInstance.$children[0].$on('handleScheduleNavigation', this.handleScheduleNavigation);
      });

      Notification.closeAll();
    },

    /**
     * Init Under Min Per Day Notification toast
     */
    initUnderMinPerDayNotification () {
      const slotNode = this.createUnderMinPerDayContent({ showList: false, showLearnMore: true })
      const notification = Notification({
        triangleIcon: true,
        title: this.$trans('campaign::messages.low_average_impressions'),
        customClass: 'custom-notification',
        dangerouslyUseHTMLString: true,
        message: true,
      });

      notification.$slots.default = [slotNode];

      this.$nextTick(() => {
        slotNode.componentInstance.$children[0].$on('handleLearnMoreNavigation', this.initInventoryWarningModal);
      })
    },

    /**
     * Close modal without confirmation handler
     */
    closeModal () {
      this.confirmationModal.visible = false;
    },

    /**
     * Handle budget navigation
     */
    handleBudgetNavigation (event) {
      if (this.confirmationModal) {
        this.closeModal()
      }

      if (event.target.tagName === 'A') {
        this.$pushRoute(this.getStep('budget.budget').route);
      }
    },

    /**
     * Handle schedule navigation
     */
    handleScheduleNavigation(event) {
      if (this.confirmationModal) {
        this.closeModal()
      }

      if (event.target.tagName === 'A') {
        this.$pushRoute(this.getStep('details.schedule').route);
      }
    },
  }
};
