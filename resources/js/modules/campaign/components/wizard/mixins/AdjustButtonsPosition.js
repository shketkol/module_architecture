import { throttle } from 'lodash';

const BUTTONS_ADJUSTMENT_INTERVAL = 20; // In milliseconds

export default {
  /**
   * Mounted hook.
   */
  mounted () {
    this.adjustButtonsPosition();
    this.setWindowScrollHandler();
    this.setWindowResizeHandler();
  },

  /**
   * Destroyed hook.
   */
  destroyed () {
    this.removeWindowScrollHandler();
    this.removeWindowResizeHandler();
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Setting scroll handler listener.
     */
    setWindowScrollHandler () {
      window.addEventListener('scroll', this.adjustButtonsPositionThrottle);
    },

    /**
     * Setting window resize handler listener.
     */
    setWindowResizeHandler () {
      window.addEventListener('resize', this.adjustButtonsPositionThrottle);
    },

    /**
     * Removing scroll handler listener.
     */
    removeWindowScrollHandler () {
      window.removeEventListener('scroll', this.adjustButtonsPositionThrottle);
    },

    /**
     * Removing scroll handler listener.
     */
    removeWindowResizeHandler () {
      window.removeEventListener('resize', this.adjustButtonsPositionThrottle);
    },

    /**
     * Adjust buttons position depending of window size and the scroller position.
     */
    adjustButtonsPosition () {
      if (window.innerWidth <= 1300) {
        this.$refs.buttons.style.left = 619 - window.scrollX + 'px';
      } else {
        this.$refs.buttons.style.left = '';
      }
    },

    /**
     * Throttled adjustButtonsPosition() function.
     */
    adjustButtonsPositionThrottle: throttle(function () {
      this.adjustButtonsPosition();
    }, BUTTONS_ADJUSTMENT_INTERVAL),
  },
};
