export default {
  /**
   * Mounted hook.
   */
  mounted () {
    this.setEventListeners();
  },

  /**
   * Before-destroy hook.
   */
  beforeDestroy () {
    this.removeEventListeners();
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Set event listeners.
     */
    setEventListeners () {
      this.$refs.stepsWrapper.$el.querySelectorAll('a.creation-steps__inner-link').forEach(el => {
        el.addEventListener('focus', this.menuItemsFocusListener);
      });

      this.$refs.stepsWrapper.$el.querySelectorAll('div.el-step__title').forEach(el => {
        el.addEventListener('mouseenter', this.groupLinkMouseEnterListener);
      });

      this.$refs.groupLinks.forEach(el => {
        el.addEventListener('focus', this.groupLinkFocusListener);
        el.addEventListener('blur', this.groupLinkBlurListener);
      });

      this.$refs.steps.forEach(el => {
        el.$el.addEventListener('mouseleave', this.stepMouseLeaveListener);
      });
    },

    /**
     * Set event listeners that were set.
     */
    removeEventListeners () {
      this.$refs.stepsWrapper.$el.querySelectorAll('a.creation-steps__inner-link').forEach(el => {
        el.removeEventListener('focus', this.menuItemsFocusListener);
      });

      this.$refs.stepsWrapper.$el.querySelectorAll('div.el-step__title').forEach(el => {
        el.removeEventListener('mouseenter', this.groupLinkMouseEnterListener);
      });

      this.$refs.groupLinks.forEach(el => {
        el.removeEventListener('focus', this.groupLinkFocusListener);
        el.removeEventListener('blur', this.groupLinkBlurListener);
      });

      this.$refs.steps.forEach(el => {
        el.$el.removeEventListener('mouseleave', this.stepMouseLeaveListener);
      });
    },

    /**
     * Menu item listener for "focus" event.
     * @param {Event} event
     */
    menuItemsFocusListener (event) {
      event.target.closest('.creation-steps__inner').classList.add('focused');
    },

    /**
     * Group link listener for "focus" event.
     * @param {Event} event
     */
    groupLinkFocusListener (event) {
      const key = event.target.id.replace('progressLink-', '');
      this.hideAllMenuGroups(document.getElementById(`progressGroup-${key}`));
    },

    /**
     * Group link listener for "blur" event.
     */
    groupLinkBlurListener () {
      this.hideAllMenuGroups();
    },

    /**
     * Group link listener for "mouseenter" event.
     */
    groupLinkMouseEnterListener () {
      this.hideAllMenuGroups();
    },

    /**
     * Menu group listener fo "mouseleave" event.
     */
    stepMouseLeaveListener () {
      this.hideAllMenuGroups();
    },

    /**
     * Hide all opened groups except one specified by "except" param.
     * @param {(HTMLElement|null)} except
     */
    hideAllMenuGroups (except = null) {
      this.$refs.stepsWrapper.$el.querySelectorAll('.creation-steps__inner').forEach(el => {
        el.classList.remove('focused');
      });
      if (except) {
        except.classList.add('focused')
      }
    },
  },
};
