import { mapGetters, mapMutations } from 'vuex';
import CampaignMixin from '../../../mixins/CampaignMixin';

export default {
  /**
   * Component props
   */
  props: {
    /**
     * Audience item.
     */
    item: {
      type: Object,
      required: true,
    },
  },

  /**
   * Mixins.
   */
  mixins: [
    CampaignMixin
  ],

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex (getters).
     */
    ...mapGetters('wizard/targeting', {
      values: 'audiences',
      getAudiencesCountCurrentValues: 'getAudiencesCountCurrentValues',
      getAudiencesCurrentValues: 'getAudiencesCurrentValues'
    })
  },

  /**
   * Watchers
   */
  watch: {
    /**
     * Item watcher
     */
    item : {
      handler () {
        this.setChecked();
      },
      deep: true,
    },

    /**
     * Checked watcher
     */
    checked (value) {
      this.$emit('checked-change', value ? 1 : -1);
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Map mutations.
     */
    ...mapMutations('wizard/targeting', ['setAudiencesValues']),

    /**
     * Check if value is present in local data
     */
    setChecked () {
      if (Array.isArray(this.values) && this.values.some(audience => audience.value === this.item.value)) {
        return this.checked = true;
      }

      return this.checked = false;
    },
  },
};
