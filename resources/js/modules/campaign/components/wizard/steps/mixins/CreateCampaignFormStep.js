import {mapActions, mapState, mapGetters, mapMutations} from 'vuex';
import { Popover } from 'Foundation/components/notice';
import { TextField, Radio, SelectField, CheckboxGroup } from 'Foundation/components/form';
import SetCampaignActivityStep from './SetCampaignActivityStep';
import { debounce } from "lodash";
import TargetingValidation from '../../mixins/TargetingValidation';
import MinPerDayValidation from "../../mixins/MinPerDayValidation";

export default {
  /**
   * Child components.
   */
  components: {
    CheckboxGroup,
    Popover,
    Radio,
    SelectField,
    TextField,
  },

  /**
   * Mixins.
   */
  mixins: [SetCampaignActivityStep, TargetingValidation, MinPerDayValidation],

  /**
   * Props.
   */
  props: {
    buttonsDisabled: {
      type: Boolean,
      default: null,
    },
  },

  /**
   * Data.
   */
  data () {
    return {
      doNativeCheckFilled: true,
      shouldTriggerAutoSave: true,
      shouldTriggerInventory: false,
      saveDebounceTimeout: 1500,// 1.5 seconds
      stepLastValue: null,
      underMinPerDayToastShown: false,
    }
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Map state.
     */
    ...mapState('wizard/inventory', ['dynamicInventoryPendingArray']),
    ...mapState('wizard', { campaignId: 'id', liveEditMode: 'liveEditMode' }),
    ...mapState('states', { campaignStates: 'states' }),

    /**
     * Map getters.
     */
    ...mapGetters('wizard/steps', ['getStep']),

    /**
     * Check if user has special ads category.
     * @returns {boolean}
     */
    isSpecialAds () {
      return this.$user.states.is_special_ads;
    },
  },

  /**
   * Created hook.
   */
  created() {
    this.$nextTick(this.focus);
  },

  /**
   * Mounted hook.
   */
  mounted () {
    this.addStepButtonsHandlers();
    this.setActivityMethod();
    this.saveStepDebounced = debounce(this.saveStep, this.saveDebounceTimeout);
  },

  /**
   * Before route leave hook.
   *
   * @param to
   * @param from
   * @param next
   */
  beforeRouteLeave (to, from, next) {
    this.onLeave()
      .then(() => { next(); })
      .catch(() => { next(false); })
      .finally(() => {
        if (this.key === 'budget.budget' && !this.underMinPerDayToastShown && this.isUnderMinPerDay) {
          this.initUnderMinPerDayNotification();
        }
      });
  },

  /**
   * Methods.
   */
  methods: {
    ...mapActions('wizard', {
      updateStep: 'steps/updateStep',
      updateFilled: 'steps/updateFilled',
      sendActivity: 'steps/sendActivity',
      getDynamicInventoryCheck: 'inventory/getDynamicInventoryCheck',
    }),

    /**
     * Map mutations.
     */
    ...mapMutations('states', ['fillStates']),

    /**
     * Focus element.
     */
    focus () {
      const ref = this.focused ? this.$refs[this.focused] : null;

      if (ref) {
        ref.focus();
      }
    },

    /**
     * This method is called each time the step is changed to next.
     * @returns {*}
     */
    beforeNext () {
      return this.validate();
    },

    /**
     * Validate step.
     *
     * @returns {*}
     */
    validate () {
      return this.$validateScope();
    },

    /**
     * Fires on finishing step transition.
     * Could be overwritten within concrete steps.
     */
    onEnterStep () {},

    /**
     * This method is called each time step is left.
     *
     * @returns {Promise<any>}
     */
    onLeave () {
      const saveMethod = this.shouldTriggerInventory ? this.saveWithInventory : this.saveStep;

      if (!this.getStep(this.key).saved && !this.savePending) {
        this.saveStepDebounced.cancel();
        this.disableButtons();
        return saveMethod().catch(error => {
          this.enableButtons();
          throw error;
        });
      } else {
        if (this.dynamicInventoryPendingArray.indexOf(this.key) === -1) {
          this.checkDynamicInventory()
        }
      }

      return Promise.resolve(true);
    },

    /**
     * Dynamic inventory entry point.
     *
     * @returns {Promise<any>}
     */
    checkDynamicInventory () {
      const closure = (stepKey) => {
        this.dynamicInventoryPendingArray.push(stepKey)
        return this.$call(this.getDynamicInventoryCheck(this.campaignId), {notify: false, muteException: true})
          .then(() => {
            const index = this.dynamicInventoryPendingArray.indexOf(stepKey);
            if (index > -1) {
              this.dynamicInventoryPendingArray.splice(index, 1);
            }
          });
      }

      if (this.shouldTriggerInventory) {
        return closure(this.key);
      }

      return Promise.resolve(true);
    },

    /**
     * Save + Dynamic inventory entry point.
     *
     * @returns {Promise<any>}
     */
    saveWithInventory () {
      return this.saveStep().then(() => {
          this.checkDynamicInventory();
      }).catch();
    },

    /**
     * Save step state.
     *
     * @returns {Promise<any>}
     */
    saveStep () {
      return this.save()
        .then(({ data }) => {
          if (data) {
            this.fillStates(data.data.states);
          }
        })
        .then(this.setSaved)
        .then(this.setDone);
    },

    /**
     * This method is called each time user changes data on the step.
     * @param value
     */
    onUpdate (value = null) {
      this.setUnsaved();

      if (this.doNativeCheckFilled) {
        this.checkFilled(value);
      }

      if (this.shouldTriggerAutoSave) {
        this.saveStepDebounced();
      }
    },

    /**
     * Update the "filled" state of the step by it's value.
     * @param {object} payload
     */
    checkFilled (payload) {
      if (this.key) {
        this.updateFilled({
          path: this.key,
          value: payload,
        });
      }
    },

    /**
     * Default save method (it should be changed for all steps).
     * @returns {Promise<boolean>}
     */
    save () {
      return Promise.resolve(true);
    },

    /**
     * Mark the step as "done".
     */
    setDone () {
      if (this.key) {
        this.updateStep({
          path: `${this.key}.done`,
          value: true,
        })
      }
    },

    /**
     * Mark the step as "saved".
     */
    setSaved () {
      if (this.key) {
        this.setLatestActivity(this.activity)

        this.updateStep({
          path: `${this.key}.saved`,
          value: true,
        });
      }
    },

    /**
     * Mark the step as "unsaved".
     */
    setUnsaved () {
      if (this.key) {
        this.updateStep({
          path: `${this.key}.saved`,
          value: false,
        });
      }
    },

    /**
     * Disables buttons before saving.
     */
    disableButtons () {
      this.$emit('buttons-disabled-changed', true);
    },

    /**
     * Enables buttons after saving.
     */
    enableButtons () {
      this.$emit('buttons-disabled-changed', false);
    },

    /**
     * Get campaign-step component and add handlers for its prev/next events.
     * NOTE: This will work only if <campaign-step> is located at the first level of the particular wizards step.
     */
    addStepButtonsHandlers () {
      const campaignStepComponent = this.$children.find(child => (child.$options.name === 'CampaignStep'));
      if (campaignStepComponent) {
        campaignStepComponent.$on('next', () => this.$emit('next'));
        campaignStepComponent.$on('previous', () => this.$emit('previous'));
      }
    },
  },
};
