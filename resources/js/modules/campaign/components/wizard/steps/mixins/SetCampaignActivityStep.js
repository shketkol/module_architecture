import {mapActions, mapMutations, mapState} from 'vuex';
import {isDemo} from 'Modules/demo/helpers/demo';

export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Map state.
     */
    ...mapState('wizard', {
      lastActivity: 'lastActivity',
      firstInit: 'firstInit',
      liveEditMode: 'liveEditMode'
    }),

    /**
     * Activity value.
     */
    activity() {
      return this.key;
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Mutations.
     */
    ...mapMutations('wizard', [
      'setLatestActivity',
      'setFirstInit'
    ]),

    /**
     * Actions.
     */
    ...mapActions('wizard', {
      sendActivity: 'steps/sendActivity',
    }),

    /**
     * Set last activity.
     */
    setActivityMethod() {
      if (this.firstInit) {
        this.sendActivityAction(this.lastActivity, this.key, this.campaignId);
      }
      this.setFirstInit(true);
    },

    /**
     * Send activity action.
     * @param actionName
     * @param lastPage
     * @param campaignId
     */
    sendActivityAction (actionName, lastPage, campaignId) {
      if (this.$user.states.is_pending || this.$user.states.is_inactive || isDemo() || this.liveEditMode) {
        return;
      }

      this.$call(this.sendActivity({
        action_name: actionName,
        last_page: lastPage,
        id: campaignId
      }), {muteException: true, notify: false})
    }
  }
}
