import CreateCampaignFormStep from './CreateCampaignFormStep';
import { mapValues, uniqBy } from 'lodash';
import { valueTypes } from '../../../../helpers/revision';

export default {
  /**
   * Mixins.
   */
  mixins: [
    CreateCampaignFormStep,
  ],

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Get all options for the select.
     */
    formattedOptions () {
      if (!this.options) {
        return {};
      }

      return mapValues(uniqBy(this.options, 'value'), option => {
        return {
          label: option.label,
          value: {
            value: option.value,
            label: option.label,
            included: true,
            type: valueTypes.NEW
          },
        };
      });
    },

    /**
     * If current position is not budget or schedule steps
     */
    isNoWarningSteps () {
      return this.$router.currentRoute.name === this.getStep('budget.budget').route.name ||
        this.$router.currentRoute.name === this.getStep('details.schedule').route.name
    },
  },

  /**
   * Watchers.
   */
  watch: {
    /**
     * Step value watcher.
     */
    value: {
      deep: true,
      handler: 'onUpdate',
    },

    /**
     * Notification should be shown.
     */
    isUnderMinPerDay (value) {
      if (!value) {
        this.underMinPerDayToastShown = false;
      }

      if (value && !this.isNoWarningSteps) {
        this.initUnderMinPerDayNotification();
        this.underMinPerDayToastShown = true;
      }
    }
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * This method is called when search box (select) is opened or closed.
     *
     * @param visible
     */
    onVisibleChange (visible) {
      if (!visible) {
        this.options = [];
      }
    }
  },
};
