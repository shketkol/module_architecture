import { mapState } from 'vuex';

export default {
  /**
   * Computed.
   */
  computed: {
    /**
     * Vuex (state).
     */
    ...mapState('wizard', ['liveEditMode', 'existDraftReview']),
  },
}
