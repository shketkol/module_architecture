import {mapState} from 'vuex';

export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex (state).
     */
    ...mapState('wizard/budget', [
      'cost',
      'impressions',
      'cpm',
    ]),
  },

  methods: {
    /**
     * Returns cost in correct format.
     *
     * @returns {string}
     */
    formattedCost (value) {
      const currency = this.$user.currency.code;

      return `${this.$formatCurrency(value, currency)}`;
    },

    /**
     * Returns impressions in correct format.
     * @returns {*}
     */
    formattedImpressions (value) {
      return this.$formatDecimal(value);
    },

    /**
     * Returns CPM in correct format.
     * @returns {*}
     */
    formattedCpm (value) {
      const currency = this.$user.currency.code;

      return `${this.$formatCurrency(value, currency)}`;
    },
  }
}
