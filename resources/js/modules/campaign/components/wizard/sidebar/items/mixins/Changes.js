import { mapState } from 'vuex';
import { isEmpty, filter } from 'lodash';
import { valueTypes } from '../../../../../helpers/revision';

const CURRENT_CLASS_ITEM = 'current';

export default {
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex (state).
     */
    ...mapState('wizard', { liveEditMode: 'liveEditMode' }),

    /**
     * Get live edit mode.
     */
    getLiveEditMode () {
      return this.liveEditMode;
    },

    /**
     * Get custom class.
     */
    customClass () {
      if (!this.getLiveEditMode) {
         return null;
      }

      return this.countChanges === 0 ? CURRENT_CLASS_ITEM : null;
    },

    /**
     * Can show count changes.
     */
    canShowCountChanges () {
      return this.countChanges > 0;
    },

    /**
     * Get count changes.
     */
    countChanges () {
      return this.getCountChanges((this.item.value && this.item.value.items !== undefined) ? this.item.value.items : this.item.value)
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Get count changes elements.
     */
    getCountChanges(items) {
      let result = 0;

      if (!this.getLiveEditMode) {
        return result;
      }

      if (!isEmpty(items) && Array.isArray(items)) {
        const found = filter(items, (option => option.type === valueTypes.NEW || option.type === valueTypes.DELETED));
        result = found ? found.length : result;
      }

      if (!isEmpty(this.item.key) && this.item.key === 'genders' && result > 1) {
        result--;
      }

      return result;
    }
  }
}
