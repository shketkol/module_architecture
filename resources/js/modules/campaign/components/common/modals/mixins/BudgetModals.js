import { mapState } from 'vuex';

export default {
  /**
   * Reactive properties.
   */
  data () {
    return {
      /**
       * Info modal data.
       */
      showWarningModal: false,

      /**
       * Progress modal data.
       */
      progressModalData: {
        status: false,
        resolver: null,
        instantClose: false,
      },
    };
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Vuex (state).
     */
    ...mapState('wizard/inventory', {
      message: 'message',
      messageType: 'messageType',
      modalType: 'type',
      typeFlow: 'typeFlow'
    }),
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * This method opens progress modal.
     */
    showProgressModal() {
      this.progressModalData = {
        status: true,
        resolver: null,
        instantClose: false,
      };
    },

    /**
     * Set inventory checked after response type was success
     */
    setInventoryCheckedAction () {
      if (this.typeFlow === 'success') {
        return true;
      }
      return false;
    },

    /**
     * This method triggers close process of progress modal.
     */
    processCloseProgressModal() {
      return new Promise(resolve => {
        this.closeProgressModal(resolve);
      });
    },

    /**
     * This method closes progress modal.
     */
    closeProgressModal(resolver = null, instantClose = false) {
      this.progressModalData = {
        status: false,
        resolver,
        instantClose,
      };
    },

    /**
     * This method opens inventory check modal.
     */
    showInventoryCheckModal() {
      this.showWarningModal = true;
    },

    /**
     * Method closes modal and triggers showing error notification
     */
    closeProgressModalWithErrNotification() {
      this.$notify.error({
        message: this.$trans('messages.went_wrong'),
      });
      this.closeProgressModal(null , true);
    },

    /**
     * Check type flow.
     */
    checkTypeFlow () {
      this.showWarningModal = !(this.typeFlow === null || this.typeFlow === 'success');
    }
  },
}
