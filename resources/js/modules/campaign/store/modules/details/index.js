import storeHelper from 'Foundation/helpers/store';
import { campaign as api } from '../../../api';
import { getField } from "vuex-map-fields";
import cloneDeep from 'lodash/cloneDeep';
import TargetingMapper from "Modules/campaign/api/mappers/TargetingMapper";
import translator from '../../../../../foundation/services/translations/locale';
import { isEmpty } from 'lodash';

/**
 * Types
 */
const types = {
  LOAD_DETAILS: storeHelper.createAsyncMutation('LOAD_DETAILS'),
  UPDATE_STATUS: storeHelper.createAsyncMutation('UPDATE_STATUS'),
  CLONE_CAMPAIGN: storeHelper.createAsyncMutation('CLONE_CAMPAIGN'),
  LOAD_REVISION: storeHelper.createAsyncMutation('LOAD_REVISION'),
};

const defaultTargeting = {
    gender: {
      value: null,
      label: null,
    },
    ages: [],
    locations: {
      zip: null,
      city: null,
      fail: []
    },
    genres: null,
    audiences: null,
    deviceGroups: null,
};

/**
 * Campaign details state
 * @type {{campaign: {}}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.CLONE_CAMPAIGN,
    types.LOAD_DETAILS,
    types.UPDATE_STATUS,
    types.LOAD_REVISION,
  ]),

  /**
   * Campaign details
   */
  campaign: {
    budget: null,
    cost: null,
    discounted_cost: null,
    discount_money: null,
    cpm: null,
    discounted_cpm: null,
    date_end: null,
    date_start: null,
    id: null,
    impressions: null,
    name: null,
    status_id: null,
    status: null,
    timezone_id: null,
    targeting: cloneDeep(defaultTargeting),
    creative: {
      created_at: null,
      duration: null,
      extension: null,
      filesize: null,
      height: null,
      id: null,
      oreder_id: null,
      name: null,
      width: null,
    },
    promocode: null,
    countUploadedAds: 0,
    exist_draft_review: false,
    revisions: {
      items: [],
      current: null
    },
    targeting_message: null,
    live_edit_mode: false,
  },

  /**
   * Async mutation state.
   */
  pendingBeforeRedirect: false,
};

const getters = {
  /**
   * Get selected gender label (translated).
   * @param state
   * @returns {string|null}
   */
  selectedGenderLabels (state) {
    if (!Array.isArray(state.campaign.targeting.gender)) {
      return null;
    }

    return state.campaign.targeting.gender.map((item) => {
      return {
        label: translator.get(`targeting::labels.genders.${item.label}`),
        type: item.type
      }
    });
  },

  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {

  /**
   * Handle all errors
   */
  handleError ({ commit }, payload) {
    commit(types.LOAD_DETAILS.FAILURE, payload);
  },

  /**
   * Clone campaign action.
   */
  load: storeHelper.createAction(types.LOAD_DETAILS, {
    call: api.show,
    after: ({ commit }, payload) => {
      commit('states/fillStates', payload.data.states, { root: true });
      commit('details/setLiveEditMode', !isEmpty(payload.data.revisions.current) && payload.data.revisions.current.status.name !== 'live', { root: true });
    }
  }),

  /**
   * Update campaign status.
   */
  updateStatus: storeHelper.createAction(types.UPDATE_STATUS, {
    call: api.status.update,
    after: ({ commit }, payload) => {
      commit('states/fillStates', payload.data.states, { root: true });
    }
  }),

  /**
   * Remove campaign
   * @param state
   * @param campaignId
   */
  async remove (state, campaignId) {
    try {
      const response = await api.remove(campaignId);
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  },

  /**
   * Clone campaign action.
   */
  clone: storeHelper.createAction(types.CLONE_CAMPAIGN, {
    call: api.clone,
  }),

  /**
   * This action cancels inventory check Get request
   */
  cancelInventoryCheckRequest() {
    api.cancelInventoryCheckGetRequest();
  },

  /**
   * Clone campaign action.
   */
  loadRevision: storeHelper.createAction(types.LOAD_REVISION, {
    call: api.getRevision,
    after: ({ commit }, payload) => {
      commit('details/setLiveEditMode', payload.data.revisions.current.status.name !== 'live', { root: true });
    }
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Load campaign details
   */
  ...storeHelper.createMutation(types.LOAD_DETAILS, {
    success (state, payload) {
      state.campaign = payload.data;
      state.campaign.targeting =
        TargetingMapper.fillValues(defaultTargeting, payload.data.targeting);
    },
    fail (state) {
      state.success = false;
    }
  }),

  /**
   * Clone campaign mutation
   */
  ...storeHelper.createMutation(types.CLONE_CAMPAIGN, {
    success(state) {
      state.pendingBeforeRedirect = true;
    },
  }),

  /**
   * Update campaign status mutation
   */
  ...storeHelper.createMutation(types.UPDATE_STATUS, {
    success(state, payload) {
      state.campaign = payload.data;
      state.campaign.targeting =
        TargetingMapper.fillValues(defaultTargeting, payload.data.targeting);
    },
  }),

  /**
   * Fill creative state
   * @param state
   * @param payload
   */
  fillCreative: (state, payload) => {
    state.campaign.status_id = payload.status_id;
    state.campaign.status = payload.status;
    state.campaign.creative = payload.creative
  },

  /**
   * Set campaign data
   *
   * @param state
   * @param id
   * @param name
   */
  setCampaignData(state, id, name) {
    state.campaign.id = id;
    state.campaign.name = name;
  },

  /**
   * remove Creative
   *
   * @param state
   */
  removeCreative (state) {
    state.campaign.creative = null;
  },

  /**
   * Load revision mutation
   */
  ...storeHelper.createMutation(types.LOAD_REVISION, {
    success(state, payload) {
      state.campaign = payload.data;
      state.campaign.targeting = TargetingMapper.fillValues(defaultTargeting, payload.data.targeting);
    },
  }),

  /**
   * Set live edit mode mutation.
   */
  setLiveEditMode (state, payload) {
    state.liveEditMode = payload;
  },
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


