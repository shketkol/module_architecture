import storeHelper from 'Foundation/helpers/store';
import { campaign as api } from '../../../api';
import cloneDeep from 'lodash/cloneDeep';
import CampaignMetricsMapper from "Modules/campaign/api/mappers/CampaignMetricsMapper";

/**
 * Types.
 */
const types = {
  LOAD_STATISTIC: storeHelper.createAsyncMutation('LOAD_STATISTIC'),
  LOAD_GENRES: storeHelper.createAsyncMutation('LOAD_GENRES'),
  LOAD_DEVICES: storeHelper.createAsyncMutation('LOAD_DEVICES'),
  LOAD_FREQUENCY: storeHelper.createAsyncMutation('LOAD_FREQUENCY'),
};

const campaignStatisticDefault = {
  cost: null,
  deliveredImpressions: null,
  performance: {
    total: null,
    startDate: null,
    endDate: null,
    performance: [],
  },
};

const generalImpressionsByDeviceDefault = {
  labels: [],
  datasets: [
    {
      backgroundColor: [],
      data: [],
    }
  ],
  custom: {
    images: []
  }
};

/**
 * Default frequency data.
 */
const frequencyDefault = {
  frequency: null,
  reach: null,
};

/**
 * Campaign metrics state.
 * @type {{campaign: {}}}
 */
const state = {

  /**
   * Campaign impressions by genre.
   */
  generalImpressionsByGenre: [],

  /**
   * Campaign impressions by device.
   */
  generalImpressionsByDevice: cloneDeep(generalImpressionsByDeviceDefault),

  /**
   * Campaign statistic.
   */
  campaignStatistic: cloneDeep(campaignStatisticDefault),

  /**
   * Campaign frequency.
   */
  campaignFrequency: cloneDeep(frequencyDefault),

  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.LOAD_GENRES,
    types.LOAD_DEVICES,
    types.LOAD_STATISTIC,
    types.LOAD_FREQUENCY,
  ]),
};

/**
 * Actions.
 */
const actions = {
  loadStatistic: storeHelper.createAction(types.LOAD_STATISTIC, {
    call: api.statistic.show,
  }),

  loadGenres: storeHelper.createAction(types.LOAD_GENRES, {
    call: api.metrics.genres,
    map: CampaignMetricsMapper.mapGenresApiToObject,
  }),

  loadDevices: storeHelper.createAction(types.LOAD_DEVICES, {
    call: api.metrics.devices,
    map: CampaignMetricsMapper.mapDevicesApiToObject,
  }),

  /**
   * Load frequency data.
   */
  loadFrequency: storeHelper.createAction(types.LOAD_FREQUENCY, {
    call: api.frequency.show
  }),
};

/**
 * Mutations.
 * @type {{}}
 */
const mutations = {
  ...storeHelper.createMutation(types.LOAD_STATISTIC, {
    success(state, {data}) {
      state.campaignStatistic = data;
    },
  }),

  ...storeHelper.createMutation(types.LOAD_GENRES, {
    success(state, data) {
      state.generalImpressionsByGenre = data;
    },
  }),

  ...storeHelper.createMutation(types.LOAD_DEVICES, {
    success(state, data) {
      state.generalImpressionsByDevice = data;
    },
  }),

  /**
   * Load frequency mutation.
   */
  ...storeHelper.createMutation(types.LOAD_FREQUENCY, {
    success(state, {data}) {
      state.campaignFrequency.frequency = data.frequency;
      state.campaignFrequency.reach = data.uniqueDevices;
    },
  }),

  resetMetricsStorage(state) {
    state.generalImpressionsByDevice = cloneDeep(generalImpressionsByDeviceDefault);
    state.generalImpressionsByGenre = [];
    state.campaignStatistic = cloneDeep(campaignStatisticDefault);
  }
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
};


