import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { campaign as api } from '../../../api';
import flatMap from 'lodash/flatMap';
import { fillTargetingErrorsWrapper } from 'Modules/campaign/store/modules/wizard/targeting/parts/validation';

/**
 * Types
 */
const types = {
  VALIDATE_CAMPAIGN: storeHelper.createAsyncMutation('VALIDATE_CAMPAIGN'),
  CLEAR_ERRORS: 'CLEAR_ERRORS',
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Validate campaign loading.
   */
  [types.VALIDATE_CAMPAIGN.loadingKey]: null,

  /**
   * Validation errors.
   */
  errors: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Get error message.
   *
   * @param state
   * @returns {array}
   */
  errors (state) {
    return flatMap(state.errors, errors => errors);
  },
};

/**
 * Actions
 */
const actions = {
  /**
   * Validate campaign on backend.
   *
   * @param commit
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  validateCampaign: storeHelper.createAction(types.VALIDATE_CAMPAIGN, {
    call: api.validate,
    afterFail({commit}, {data}) {
      fillTargetingErrorsWrapper(commit, data);
    }
  }),

  /**
   * Clear all errors.
   *
   * @param commit
   */
  clearErrors ({ commit }) {
    commit(types.CLEAR_ERRORS);
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Success.
   *
   * @param state
   */
  ...storeHelper.createMutation(types.VALIDATE_CAMPAIGN, {
    failure(state, {data}) {
      state.errors = data.errors;
    }
  }),

  /**
   * Clear all errors.
   *
   * @param state
   */
  [types.CLEAR_ERRORS] (state) {
    state.errors = null;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


