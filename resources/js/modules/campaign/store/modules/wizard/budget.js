import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { campaign } from '../../../api';

import CampaignMapper from '../../../api/mappers/CampaignMapper';

const api = campaign.budget;

/**
 * Types
 */
const types = {
  GET_CPM: storeHelper.createAsyncMutation('GET_CPM'),
  GET_BILLING_DATE: storeHelper.createAsyncMutation('GET_BILLING_DATE'),
  UPDATE_BUDGET: storeHelper.createAsyncMutation('UPDATE_BUDGET'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Get CPM for the campaign.
   */
  [types.GET_CPM.loadingKey]: null,

  /**
   * Get Billing Date for the campaign.
   */
  [types.GET_BILLING_DATE.loadingKey]: null,

  /**
   * Update campaign budget.
   */
  [types.UPDATE_BUDGET.loadingKey]: null,

  cost: null,
  impressions: null,
  cpm: null,
  billingDate: null,
  budgetFailMessage: null,
  minDailyImpressions: null,
  minRequiredBudget: window.config.campaign.wizard.budget.min,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Average daily impressions value
   */
  dailyImpressions (state, getters, rootState, rootGetters) {

    const duration = rootGetters["wizard/details/getCampaignDuration"];

    if (!duration || !state.impressions) {
      return null;
    }

    return Math.floor(state.impressions / duration);
  },

  /**
   * If under min per day warning must be shown.
   */
  isUnderMinPerDay (state, getters) {
    if (!getters.dailyImpressions || !state.minDailyImpressions) {
      return false;
    }

    return getters.dailyImpressions < state.minDailyImpressions;
  }
};

/**
 * Actions
 */
const actions = {
  /**
   * Get default cpm for campaign creation
   */
  async getCpm({ commit }, id) {
    commit(types.GET_CPM.PENDING);

    try {
      const response = await api.cpm.get(id);
      const { data: { data } } = response;
      commit(types.GET_CPM.SUCCESS, data);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.GET_CPM.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update Campaign Budget information.
   *
   * @param commit
   * @param rootState
   * @param state
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async updateBudget({ commit, rootState, state }) {
    commit(types.UPDATE_BUDGET.PENDING);

    try {
      const response = await api.update(rootState.wizard.id, CampaignMapper.mapObjectToApi({
        cost: +state.cost,
      }));
      const data = CampaignMapper.mapApiToObject(response);
      commit('fill', data.data);
      commit(types.UPDATE_BUDGET.SUCCESS);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_BUDGET.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Get Billing date
   * @param commit
   * @param wizard
   * @returns {Promise<void>}
   */
  async getBillingDate({ commit, rootState: { wizard } }) {
    commit(types.GET_BILLING_DATE.PENDING);

    try {
      const response = await api.billingDate.get(wizard.id);
      const { data: { data: { billingDate } } } = response;
      commit(types.GET_BILLING_DATE.SUCCESS, billingDate);
      return Promise.resolve(billingDate);
    } catch (error) {
      commit(types.GET_BILLING_DATE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Updates cost and impressions with new data.
   */
  updateCostWithImpressions({ commit, dispatch }, payload) {
    commit('fill', payload);
    dispatch('updateBudget');
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  [types.GET_BILLING_DATE.SUCCESS](state, payload) {
    state.billingDate = payload;
    state[types.GET_BILLING_DATE.loadingKey] = false;
  },

  [types.GET_BILLING_DATE.PENDING](state) {
    state[types.GET_BILLING_DATE.loadingKey] = true;
  },

  [types.GET_BILLING_DATE.FAILURE](state) {
    state[types.GET_BILLING_DATE.loadingKey] = false;
  },

  [types.GET_CPM.SUCCESS](state, payload) {
    state.cpm = payload.cpm;
    state.minDailyImpressions = payload.min_daily_impressions;
    state[types.GET_CPM.loadingKey] = false;
  },

  [types.GET_CPM.PENDING](state) {
    state[types.GET_CPM.loadingKey] = true;
  },

  [types.GET_CPM.FAILURE](state) {
    state[types.GET_CPM.loadingKey] = false;
  },

  /**
   * Campaign budget updated successfully.
   *
   * @param state
   */
  [types.UPDATE_BUDGET.SUCCESS](state) {
    state[types.UPDATE_BUDGET.loadingKey] = false;
  },

  /**
   * Campaign budget updating.
   *
   * @param state
   */
  [types.UPDATE_BUDGET.PENDING](state) {
    state[types.UPDATE_BUDGET.loadingKey] = true;
  },

  /**
   * Campaign budget was not updated.
   *
   * @param state
   */
  [types.UPDATE_BUDGET.FAILURE](state) {
    state[types.UPDATE_BUDGET.loadingKey] = false;
  },

  /**
   * Fill the state.
   *
   * @param state
   * @param data
   */
  fill (state, data) {
    state.cost = data.cost.toString();
    state.impressions = data.impressions;
    state.cpm = data.cpm;
    state.minDailyImpressions = data.min_daily_impressions || state.minDailyImpressions;
    state.minRequiredBudget = data.min_required_budget;
  },

  /**
   * Fill error budget message.
   *
   * @param state
   * @param message
   */
  fillErrorBudgetMessage (state, message) {
    state.budgetFailMessage = message
  },

  /**
   * Clear error budget message.
   *
   * @param state
   * @param message
   */
  clearErrorBudgetMessage (state) {
    state.budgetFailMessage = null
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


