import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { campaign as api } from '../../../api';
import CampaignMapper from '../../../api/mappers/CampaignMapper';
import { cloneDeep, findIndex } from 'lodash';
import moment from 'moment';
import { getCurrentValues, valueTypes } from '../../../helpers/revision';

/**
 * Types
 */
const types = {
  UPDATE_SCHEDULE: storeHelper.createAsyncMutation('UPDATE_SCHEDULE'),
  UPDATE_NAME: storeHelper.createAsyncMutation('UPDATE_NAME'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Update campaign schedule loading.
   */
  [types.UPDATE_SCHEDULE.loadingKey]: null,

  /**
   * Update campaign name loading.
   */
  [types.UPDATE_NAME.loadingKey]: null,

  /**
   * Campaign name.
   */
  name: null,

  /**
   * Campaign status.
   */
  status: null,

  /**
   * Schedule.
   */
  date: {
    start: [{
      type: valueTypes.NEW,
      value: null
    }],
    end: [{
      type: valueTypes.NEW,
      value: null
    }],
  },
  /**
   * Timezone id.
   */
  timezone_id: null,

  /**
   * Timezone
   */
  timezone: null,

  /**
   * Campaign's response message.
   */
  message: null,

  /**
   * Campaign's step.
   */
  step: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Get current campaign duration.
   */
  getCampaignDuration (state, getters) {
    if (!state.date.start || !state.date.end) {
      return 0;
    }

    const startDate = moment.utc(getters['getCurrentStartDate']).startOf('day');
    const endDate = moment.utc(getters['getCurrentEndDate']).endOf('day');

    return Math.ceil(moment.duration(endDate.diff(startDate)).asDays());
  },

  /**
   * Get current campaign start date.
   */
  getCurrentStartDate (state) {
    let { date } = cloneDeep(state);
    return getCurrentValues(date.start)[0].value;
  },

  /**
   * Get current campaign start date.
   */
  getCurrentEndDate (state) {
    let { date } = cloneDeep(state);
    return getCurrentValues(date.end)[0].value;
  },
};

/**
 * Actions
 */
const actions = {
  /**
   * Load account info from API.
   *
   * @param commit
   * @param rootState
   * @param state
   * @returns {Promise<*|undefined>}
   */
  async updateName({ commit, rootState, state }) {
    commit(types.UPDATE_NAME.PENDING);

    try {
      const response = await api.name.update(rootState.wizard.id, CampaignMapper.mapObjectToApi({
        name: state.name,
      }));
      commit(types.UPDATE_NAME.SUCCESS);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_NAME.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update schedule.
   *
   * @param commit
   * @param rootState
   * @param state
   * @returns {Promise<*|undefined>}
   */
  async updateSchedule({ commit, rootState, state }) {
    commit(types.UPDATE_SCHEDULE.PENDING);

    try {
      let { date, timezone_id } = cloneDeep(state);
      date.start = getCurrentValues(date.start)[0].value;
      date.end = getCurrentValues(date.end)[0].value;

      const response = await api.schedule.update(rootState.wizard.id, CampaignMapper.mapObjectToApi({
        date,
        timezone_id
      }));
      commit(types.UPDATE_SCHEDULE.SUCCESS, response);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_SCHEDULE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Campaign name is successfully loaded.
   *
   * @param state
   */
  [types.UPDATE_NAME.SUCCESS] (state) {
    state[types.UPDATE_NAME.loadingKey] = false;
  },

  /**
   * Campaign name is loading.
   *
   * @param state
   */
  [types.UPDATE_NAME.PENDING](state) {
    state[types.UPDATE_NAME.loadingKey] = true;
  },

  /**
   * Campaign name was not loaded.
   *
   * @param state
   */
  [types.UPDATE_NAME.FAILURE](state) {
    state[types.UPDATE_NAME.loadingKey] = false;
  },

  /**
   * Campaign date_range information was successfully stored.
   *
   * @param state
   * @param timezone
   */
  [types.UPDATE_SCHEDULE.SUCCESS](state, { data: { data: { timezone } } }) {
    state[types.UPDATE_SCHEDULE.loadingKey] = false;
    state.timezone = timezone;
  },

  /**
   * Campaign date_range information is storing.
   *
   * @param state
   */
  [types.UPDATE_SCHEDULE.PENDING](state) {
    state[types.UPDATE_SCHEDULE.loadingKey] = true;
  },

  /**
   * Campaign date_range information was not stored.
   *
   * @param state
   */
  [types.UPDATE_SCHEDULE.FAILURE](state) {
    state[types.UPDATE_SCHEDULE.loadingKey] = false;
  },

  /**
   * Fill the state.
   * @param state
   * @param status
   * @param name
   * @param date
   * @param timezone_id
   * @param timezone
   * @param message
   * @param step
   */
  fill (state, { status, name, date, timezone_id, timezone, message, step }) {
    state.status = status;
    state.name = name;
    state.date = date;
    state.timezone_id = timezone_id;
    state.timezone = timezone;
    state.message = message;
    state.step = step;
  },

  /**
   * Set new daterange.
   * @param state
   * @param payload
   */
  setDateRange (state, payload) {
    const replaceDate = (data, value, type) => {
      data.pop()
      data.push({
        value: value,
        type: type,
      })
    }
    const getDateByDateString = (date) => {
      if (date === null) {
        return date;
      }

      if (date instanceof Date) {
        return moment(date).format('yyyy-MM-DD');
      }

      return date.split('T')[0]
    }

    const filterNewDate = (data, date) => {
      let oldDate = data[0].value;
      if (oldDate === null) {
        replaceDate(data, date, valueTypes.NEW)
        return;
      }

      date = getDateByDateString(date)
      oldDate = getDateByDateString(oldDate)

      if (date !== oldDate) {
        if (data[0].type === valueTypes.CURRENT) {
          replaceDate(data, data[0].value, valueTypes.DELETED)
          data.push({
            value: date,
            type: valueTypes.NEW,
          })
          return;
        }
        replaceDate(data, date, valueTypes.NEW)
      }
    };

    for (let item in state.date) {
      const countItems = state.date[item].length;
      if (countItems === 1) {
        filterNewDate(state.date[item], payload[item]);
        continue;
      }

      const indexDeleted = findIndex(state.date[item], (el => el.type === valueTypes.DELETED));
      const indexNew = findIndex(state.date[item], (el => el.type === 'new'));

      if (getDateByDateString(state.date[item][indexDeleted].value) === getDateByDateString(payload[item])) {
        state.date[item][indexDeleted].type = valueTypes.CURRENT;
        state.date[item].splice(indexNew, 1);
        continue;
      }

      state.date[item][indexNew].value = payload[item];
    }
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


