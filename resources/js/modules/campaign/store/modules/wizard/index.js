import wizard from './wizard';
import budget from './budget';
import creative from './creative';
import inventory from './inventory';
import details from './details';
import review from './review';
import steps from './steps';
import promocode from './promocode';
import targeting from './targeting/index';
import storeHelper from 'Foundation/helpers/store';
import { campaign as api } from '../../../api';
import { getField, updateField } from 'vuex-map-fields';
import CampaignMapper from '../../../api/mappers/CampaignMapper';
import { fillTargetingErrorsWrapper } from 'Modules/campaign/store/modules/wizard/targeting/parts/validation';

/**
 * Types
 */
const types = {
  EDIT_CAMPAIGN: storeHelper.createAsyncMutation('EDIT_CAMPAIGN'),
  REFRESH_CAMPAIGN: storeHelper.createAsyncMutation('REFRESH_CAMPAIGN'),
  STORE_CAMPAIGN: storeHelper.createAsyncMutation('STORE_CAMPAIGN'),
  DELETE_CAMPAIGN: storeHelper.createAsyncMutation('DELETE_CAMPAIGN'),
  ORDER_CAMPAIGN: storeHelper.createAsyncMutation('ORDER_CAMPAIGN'),
  UPDATE_CAMPAIGN: storeHelper.createAsyncMutation('UPDATE_CAMPAIGN'),
};

/**
 * Nested Modules
 */
const modules = {
  wizard,
  budget,
  creative,
  inventory,
  details,
  review,
  steps,
  targeting,
  promocode,
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.EDIT_CAMPAIGN,
    types.REFRESH_CAMPAIGN,
    types.STORE_CAMPAIGN,
    types.DELETE_CAMPAIGN,
    types.ORDER_CAMPAIGN,
    types.UPDATE_CAMPAIGN,
  ]),

  /**
   * Campaign process status.
   */
  success: false,

  /**
   * Campaign id.
   */
  id: null,

  /**
   * True if edit wizard is opened.
   */
  edit: false,

  /**
   * True if edit campaign data is loaded.
   */
  editDataIsLoaded: false,

  /**
   * Counted upload ads.
   */
  countUploadedAds: 0,

  /**
   * Last activity value.
   */
  lastActivity: null,

  /**
   * Fist init page.
   */
  firstInit: false,

  /**
   * Timestamp where last successful inventory was performed.
   */
  lastSuccessfulInventoryTimestamp: null,

  /**
   * Is live edit mode
   */
  liveEditMode: false,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {

  /**
   * Fill campaign full info
   *
   * @returns {Promise<*|undefined>}
   */
  fillCampaign ({ commit, dispatch }, data) {
    dispatch('fillCampaignDetails', data);
    commit('steps/fill', data);
    commit('creative/fill', data.creative);
    commit('states/fillStates', data.states , { root: true });
    dispatch('checkStepsFilledStates');
  },

  /**
   * Fill campaign general info
   *
   * @returns {Promise<*|undefined>}
   */
  fillCampaignDetails ({ commit }, data) {
    commit('details/fill', data);
    commit('budget/fill', data);
    commit('targeting/fill', data.targeting);
    commit('promocode/fill', data);
  },

  /**
   * Gather data and update steps "filled" states considering gathered data.
   * @param state
   * @param dispatch
   * @param getters
   */
  checkStepsFilledStates ({ state, dispatch, getters }) {
    const valuesToValidate = {
      'details.name' : state.details.name,
      'details.schedule' : state.details.date,
      'targeting.ages' : getters['targeting/ages'],
      'targeting.locations' : getters['targeting/locations'],
      'targeting.genres' : getters['targeting/genres'],
      'targeting.audiences' : getters['targeting/audiences'],
      'targeting.devices' : getters['targeting/deviceGroups'],
      'creative.creative': state.creative.creative,
      'budget.budget': state.budget.cost,
    };
    for (const path in valuesToValidate) {
      dispatch('steps/updateFilled', {
        path,
        value: valuesToValidate[path],
      });
    }
  },

  /**
   * Load Campaign info from API.
   */
  editCampaign: storeHelper.createAction(types.EDIT_CAMPAIGN, {
    call: api.edit,
    map: CampaignMapper.mapApiToObject,
    after: ({ dispatch }, data) => {
      dispatch('fillCampaign', data);
    },
  }),

  /**
   * Create the campaign.
   */
  storeCampaign: storeHelper.createAction(types.STORE_CAMPAIGN, {
    callMap: CampaignMapper.mapObjectToApi,
    call: api.store,
    map: CampaignMapper.mapApiToObject,
    after: ({ commit }, payload) => {
      commit('states/fillStates', payload.states , { root: true });
    }
  }),

  /**
   * Order filled Campaign.
   */
  orderCampaign: storeHelper.createAction(types.ORDER_CAMPAIGN, {
    call: api.order,
    afterFail: ({ commit }, { data }) => {
      fillTargetingErrorsWrapper(commit, data)
    }
  }),

  /**
   * Update Campaign.
   */
  updateCampaign: storeHelper.createAction(types.UPDATE_CAMPAIGN, {
    call: api.updateCampaign,
    afterFail: ({ commit }, { data }) => {
      fillTargetingErrorsWrapper(commit, data)
    }
  }),

  /**
   * Refresh Campaign from API.
   *
   * @param commit
   * @param dispatch
   * @param state
   * @returns {Promise<*|undefined>}
   */
  async refreshCampaign ({ commit, dispatch, state }) {

    //do not initiate an extra request for the campaign edit mode until the main data is loaded
    if ((state.edit && !state.editDataIsLoaded) || !state.id) {
      return;
    }

    commit(types.REFRESH_CAMPAIGN.PENDING);

    try {
      const response = await api.edit(state.id);
      const data = CampaignMapper.mapApiToObject(response.data);
      dispatch('fillCampaignDetails', data);
      commit('creative/fill', data.creative);
      dispatch('checkStepsFilledStates');
      commit(types.REFRESH_CAMPAIGN.SUCCESS, data);
      return Promise.resolve(data);
    }
    catch (error) {
      commit(types.REFRESH_CAMPAIGN.FAILURE);
      return Promise.reject(error);
    }
  },

  /**
   * Update business information.
   *
   * @param commit
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async deleteCampaign ({commit, state}) {
    commit(types.DELETE_CAMPAIGN.PENDING);

    try {
      const response = await api.delete(state.campaign.id);
      commit(types.DELETE_CAMPAIGN.SUCCESS);
      return Promise.resolve(response)
    }
    catch (error) {
      commit(types.DELETE_CAMPAIGN.FAILURE, error && error.response);
      return Promise.reject(error)
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Edit campaign mutations.
   */
  ...storeHelper.createMutation(types.EDIT_CAMPAIGN, {
    success (state, payload) {
      state.id = payload.id;
      state.editDataIsLoaded = true;
      state.liveEditMode = payload.live_edit_mode;
      state.existDraftReview = payload.exist_draft_review;
      state.countUploadedAds = payload.countUploadedAds;
      state.lastSuccessfulInventoryTimestamp = payload.successful_inventory_timestamp;
    },
    pending (state) {
      state.edit = true;
    },
  }),

  /**
   * Store campaign mutations.
   */
  ...storeHelper.createMutation(types.STORE_CAMPAIGN, {
    success (state, payload) {
      state.id = payload.id;
    },
  }),

  /**
   * Order campaign mutations.
   */
  ...storeHelper.createMutation(types.ORDER_CAMPAIGN, {
    success (state) {
      state.success = true;
    },
  }),

  /**
   * Update campaign mutations.
   */
  ...storeHelper.createMutation(types.UPDATE_CAMPAIGN, {
    success (state) {
      state.success = true;
    },
  }),

  /**
   * Refresh campaign mutations.
   */
  ...storeHelper.createMutation(types.REFRESH_CAMPAIGN, {
    success (state, payload) {
      state.lastSuccessfulInventoryTimestamp = payload.successful_inventory_timestamp;
    }
  }),

  /**
   * Delete campaign mutations.
   */
  ...storeHelper.createMutation(types.DELETE_CAMPAIGN),

  /**
   * Set latest activity.
   */
  setLatestActivity (state, data) {
    state.lastActivity = data;
  },

  /**
   * Set fist init page.
   */
  setFirstInit (state, data) {
    state.firstInit = data;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  modules,
  types,
  state,
  getters,
  actions,
  mutations,
}
