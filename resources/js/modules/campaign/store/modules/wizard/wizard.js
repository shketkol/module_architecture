/**
 * State
 * @type {{}}
 */
const state = {
  buttonsState: {
    disabled: true,
    nextAvailable: false,
    previousAvailable: false,
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Set buttons state.
   *
   * @param state
   * @param data
   */
  setButtonsState (state, data) {
    state.buttonsState = data;
  },
};

export default {
  namespaced: true,
  state,
  mutations
};
