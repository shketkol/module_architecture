import { campaign as api } from '../../../api';
import { getField, updateField } from 'vuex-map-fields';

/**
 * State
 * @type {{}}
 */
const state = {
  creative: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Returns true if there is a creative attached to the campaign.
   *
   * @param state
   * @returns {boolean}
   */
  hasCreative (state) {
    return !!state.creative;
  },
};

/**
 * Actions.
 *
 * @type {{updateCampaignStep({rootState: *}): Promise<*|undefined>}}
 */
const actions = {
  /**
   * Update campaign creative step.
   *
   * @param rootState
   * @returns {Promise<*>}
   */
  async updateCampaignStep({ rootState }) {
    try {
      const response = await api.creative.step.update(rootState.wizard.id);
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {

  /**
   * Fill the state.
   *
   * @param state
   * @param data
   */
  fill (state, data) {
    state.creative = data;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};


