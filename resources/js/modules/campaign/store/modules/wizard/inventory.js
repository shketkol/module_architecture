import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { campaign as api } from '../../../api';
import InventoryCheck from '../../../api/mappers/InventoryCheckMapper';
import InventoryCheckMapper from "../../../api/mappers/InventoryCheckMapper";
import { get } from 'lodash';
import { fillTargetingErrorsWrapper } from 'Modules/campaign/store/modules/wizard/targeting/parts/validation';

/**
 * Types
 */
const types = {
  GET_INVENTORY_CHECK: storeHelper.createAsyncMutation('GET_INVENTORY_CHECK'),
  GET_DYNAMIC_INVENTORY_CHECK: storeHelper.createAsyncMutation('GET_DYNAMIC_INVENTORY_CHECK'),
  SET_MESSAGES: 'SET_MESSAGES',
};

/**
 * State
 * @type {{}}
 */
const state = {
  ...storeHelper.createMutationState([
    types.GET_DYNAMIC_INVENTORY_CHECK,
  ]),

  /**
   * Inventory Check loading.
   */
  [types.GET_INVENTORY_CHECK.loadingKey]: null,

  /**
   * Summary for the inventory check loading states.
   */
  dynamicInventoryPendingArray: [],

  /**
   * Dynamic inventory check data
   */
  inventory: {
    id: null,
    data: {
      type: null,
      message: '',
      value: '-',
      indicatorPosition: 0,
    },
    suggestions: {
      message: null,
      action: {
        steps: {},
        budget: null,
        value: null
      },
      payload: null,
      help: {}
    },
    updated: null
  },

  /**
   * Messages with types.
   */
  messages: [],

  /**
   * Message type.
   */
  messageType: '',

  /**
   * Modal type.
   */
  type: null,

  /**
   * Type flow.
   */
  typeFlow: null,

  /**
   * Modal payload.
   */
  payload: {},
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Summary of the dynamic inventory loading states.
   */
  dynamicInventoryLoading: (state) => {
    return !!state.dynamicInventoryPendingArray.length;
  }
};

/**
 * Actions
 */
const actions = {
  /**
   * Perform dynamic inventory check call
   *
   * @param state, payload
   */
  getDynamicInventoryCheck: storeHelper.createAction(types.GET_DYNAMIC_INVENTORY_CHECK, {
    call: api.inventoryCheck.getDynamic,
    map: InventoryCheckMapper.mapApiToObjectDynamic,
    after: ({ commit }, data) => {
      if (data.data.type === 'unprocessable' && !!get(data, 'suggestions.action.budget')) {
        commit('wizard/budget/fillErrorBudgetMessage', get(data, 'suggestions.message'), { root: true });
      }
    },
    afterFail: ({ commit }, { data }) => {
      fillTargetingErrorsWrapper(commit, data)
    }
  }),

  /**
   * Load account info from API.
   *
   * @param commit
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  async getInventoryCheck({ commit, rootState }) {
    commit(types.GET_INVENTORY_CHECK.PENDING);

    try {
      const response = await api.inventoryCheck.get(rootState.wizard.id);
      commit(
        types.GET_INVENTORY_CHECK.SUCCESS,
        InventoryCheck.mapApiToObject(response)
      );
      return Promise.resolve(response);
    } catch (error) {
      commit(types.GET_INVENTORY_CHECK.FAILURE, error && error.response);
      fillTargetingErrorsWrapper(commit, error.response.data)
      return Promise.reject(error);
    }
  },

  /**
   * Set the message with specified type.
   * @param dispatch
   * @param payload
   */
  setMessageWithType ({ dispatch }, payload) {
    dispatch('setMessages', [payload]);
  },

  /**
   * Clear messages.
   * @param dispatch
   */
  clearMessages({ dispatch }) {
    dispatch('setMessages', []);
  },

  /**
   * Set messages.
   * @param commit
   * @param {Array} payload Object of type {message: (string), type: (string)}
   */
  setMessages ({ commit }, payload) {
    commit(types.SET_MESSAGES, payload);
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Success.
   *
   * @param state
   * @param payload
   */
  [types.GET_DYNAMIC_INVENTORY_CHECK.SUCCESS] (state, payload) {
    if (state.inventory.id < payload.id || payload.data.type === 'unprocessable') {
      state.inventory = payload;
    }
    state[types.GET_DYNAMIC_INVENTORY_CHECK.loadingKey] = false;
  },

  /**
   * Pending.
   *
   * @param state
   */
  [types.GET_DYNAMIC_INVENTORY_CHECK.PENDING] (state) {
    state[types.GET_DYNAMIC_INVENTORY_CHECK.loadingKey] = true;
  },

  /**
   * Failure.
   *
   * @param state
   */
  [types.GET_DYNAMIC_INVENTORY_CHECK.FAILURE] (state) {
    state[types.GET_DYNAMIC_INVENTORY_CHECK.loadingKey] = false;
  },

  /**
   * Success.
   *
   * @param state
   * @param payload
   */
  [types.GET_INVENTORY_CHECK.SUCCESS] (state, payload) {
    if (['danger', 'success'].includes(payload.typeFlow) || payload.type === 'fail') {
      state.messages = [{
        message: payload.message,
        type: payload.messageType,
      }];
    }

    state.messageType = payload.messageType;
    state.type = payload.type;
    state.typeFlow = payload.typeFlow;

    if (payload.payload) {
      state.payload = payload.payload;
    }

    state.payload.title = payload.title;

    state[types.GET_INVENTORY_CHECK.loadingKey] = false;
  },

  /**
   * Pending.
   *
   * @param state
   */
  [types.GET_INVENTORY_CHECK.PENDING] (state) {
    state.messages = [];

    state[types.GET_INVENTORY_CHECK.loadingKey] = true;
  },

  /**
   * Failure.
   *
   * @param state
   */
  [types.GET_INVENTORY_CHECK.FAILURE] (state) {
    state[types.GET_INVENTORY_CHECK.loadingKey] = false;
  },

  /**
   * Fields mapping.
   */
  updateField,

  /**
   * Set message with provided type.
   */
  [types.SET_MESSAGES] (state, payload) {
    state.messages = payload;
  },
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


