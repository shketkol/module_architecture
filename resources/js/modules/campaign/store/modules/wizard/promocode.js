import { trackActionEvent } from "Foundation/services/track/track";
import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { campaign } from '../../../api';

/**
 * Types
 */
const types = {
  VALIDATE_PROMOCODE: storeHelper.createAsyncMutation('VALIDATE_PROMOCODE'),
  DELETE_PROMOCODE: 'DELETE_PROMOCODE',
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Validate campaign promocode.
   */
  [types.VALIDATE_PROMOCODE.loadingKey]: false,

  /**
   * If promocode was applied
   */
  applied: false,

  /**
   * Promocode data.
   */
  promocode: {
    code: '',
    discount: 0,
    discount_money: 0,
    type: ''
  }
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Validate Campaign Promocode.
   *
   * @param commit
   * @param rootState
   * @param promocode
   * @returns {Promise<*|undefined>}
   */
  async validatePromocode ({commit, rootState}, promocode) {
    commit(types.VALIDATE_PROMOCODE.PENDING);

    try {
      const response = await campaign.promocode.validate(rootState.wizard.id, promocode);
      commit(types.VALIDATE_PROMOCODE.SUCCESS, response.data.data);
      trackActionEvent('add_promo_code');
      return Promise.resolve(response);
    } catch (error) {
      commit(types.VALIDATE_PROMOCODE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Delete applied promocode in the campaign
   *
   * @param state, payload
   */
  async deletePromocode ({commit, rootState}) {
    commit(types.DELETE_PROMOCODE);

    try {
      const response = await campaign.promocode.delete(rootState.wizard.id);
      return Promise.resolve(response);
    } catch (error) {
      return Promise.reject(error);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Campaign promocode validated successfully.
   *
   * @param state
   * @param payload
   */
  [types.VALIDATE_PROMOCODE.SUCCESS] (state, payload) {
    state[types.VALIDATE_PROMOCODE.loadingKey] = false;
    state.promocode = payload.promocode;
    state.applied = true;

    state.promocode.discount = payload.promocode.value;
    state.promocode.discount_money = payload.discount_money;
  },

  /**
   * Campaign promocode validating.
   *
   * @param state
   */
  [types.VALIDATE_PROMOCODE.PENDING] (state) {
    state[types.VALIDATE_PROMOCODE.loadingKey] = true;
  },

  /**
   * Campaign promocode was not validated.
   *
   * @param state
   */
  [types.VALIDATE_PROMOCODE.FAILURE] (state) {
    state[types.VALIDATE_PROMOCODE.loadingKey] = false;
  },

  /**
   * Campaign promocode was discarded.
   *
   * @param state
   */
  discardPromocode (state) {
    state.applied = false;
    state.promocode = {
      code: '',
      discount: 0,
      discount_money: 0,
      type: ''
    };
  },

  /**
   * Change applied status
   *
   * @param state
   * @param  status
   */
  setApplied(state, status ) {
    state.applied = status;
  },

  /**
   * Fill the state.
   *
   * @param state
   * @param data
   */
  fill (state, data) {
    if (data.promocode === null) {
      return;
    }

    state.promocode = data.promocode;
    state.promocode.discount = data.promocode.value;
    state.promocode.discount_money = data.discount_money;
  },

  /**
   * Delete applied promocode in the campaign.
   */
  [types.DELETE_PROMOCODE] (state) {
    state.applied = false;
    state.promocode = {
      code: '',
      discount: 0,
      discount_money: 0,
      type: ''
    };
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};
