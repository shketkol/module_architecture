import storeHelper from 'Foundation/helpers/store';
import { filter, isEmpty } from 'lodash';
import { getField, updateField } from 'vuex-map-fields';
import { campaign as api } from '../../../../../api';
import TargetingMapper from 'Modules/campaign/api/mappers/TargetingMapper';
import translator from 'Foundation/services/translations/locale';
import Locations from '../../../../../api/helpers/Locations';
import { getCurrentValues, uppercaseFirstLetter } from '../../../../../helpers/revision';
import { createCountCurrentGetter, createCurrentGetter, createTargetingMutation } from './helper';

/**
 * Helper function. It is used to divide included and excluded targeting.
 * @param items
 * @param included
 * @returns {Array}
 */
const filterIncluded = (items, included) => {
  if (items === null) {
    return [];
  }
  return filter(items, item => included ? item.included : !item.included);
};

/**
 * Types
 */
const types = {
  UPDATE_AGES: storeHelper.createAsyncMutation('UPDATE_AGES'),
  UPDATE_GROUPS: storeHelper.createAsyncMutation('UPDATE_GROUPS'),
  UPDATE_PLATFORMS: storeHelper.createAsyncMutation('UPDATE_PLATFORMS'),
  UPDATE_LOCATIONS: storeHelper.createAsyncMutation('UPDATE_LOCATIONS'),
  UPDATE_GENRES: storeHelper.createAsyncMutation('UPDATE_GENRES'),
  UPDATE_DEVICES: storeHelper.createAsyncMutation('UPDATE_DEVICES'),
  UPDATE_AUDIENCES: storeHelper.createAsyncMutation('UPDATE_AUDIENCES'),
  SEARCH_ZIP: storeHelper.createAsyncMutation('SEARCH_ZIP'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.UPDATE_AGES,
    types.UPDATE_GROUPS,
    types.UPDATE_PLATFORMS,
    types.UPDATE_LOCATIONS,
    types.UPDATE_GENRES,
    types.UPDATE_DEVICES,
    types.UPDATE_AUDIENCES,
    types.SEARCH_ZIP,
  ]),

  /**
   * Campaign.
   */
  targeting: {
    gender: null,
    ages: [],
    locations: {
      zip: null,
      city: null,
      fail: []
    },
    genres: null,
    deviceGroups: null,
    audiences: null,
  },

  forceQuitZipSearch: false,
  maxCountDevices: 3
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Get max devices.
   */
  getMaxCountDevices (state) {
    return state.maxCountDevices;
  },

  /**
   * Get selected gender label (translated).
   * @param state
   * @returns {string|null}
   */
  selectedGenderLabels (state) {
    if (!state.targeting.gender) {
      return null;
    }

    return state.targeting.gender.map((item) => {
      return {
        label: translator.get(`targeting::labels.genders.${item.label}`),
        type: item.type
      }
    });
  },

  /**
   * Get selected gender value.
   * @param state
   * @returns {Number|Null}
   */
  selectedGenderId (state) {
    return state.targeting.gender.value;
  },

  /**
   * Get all selected genders.
   * @param state
   * @returns {Array}
   */
  genders (state) {
    return state.targeting.gender;
  },

  /**
   * Get all selected ages.
   * @param state
   * @returns {Array}
   */
  ages (state) {
    return state.targeting.ages;
  },

  /**
   * Get all selected locations.
   * @param state
   * @returns {Array}
   */
  locations (state) {
    return state.targeting.locations;
  },

  /**
   * Get fail zip codes.
   * @param state
   * @returns {Array}
   */
  failZipCodes (state) {
    return state.targeting.locations.fail;
  },

  /**
   * Return only included locations.
   * @param state
   * @returns {Array}
   */
  includedLocations (state) {
    return filterIncluded(state.targeting.locations.city, true);
  },

  /**
   * Return only excluded locations.
   * @param state
   * @returns {Array}
   */
  excludedLocations (state) {
    return filterIncluded(state.targeting.locations.city, false);
  },

  /**
   * Return only included zip codes.
   * @param state
   * @returns {Array}
   */
  includedZipCodes (state) {
    return filterIncluded(state.targeting.locations.zip, true);
  },

  /**
   * Return only excluded zip codes.
   * @param state
   * @returns {Array}
   */
  excludedZipCodes (state) {
    return filterIncluded(state.targeting.locations.zip, false);
  },

  /**
   * Get all selected genres.
   * @param state
   * @returns {Array}
   */
  genres (state) {
    return state.targeting.genres;
  },

  /**
   * Get all selected device groups.
   * @param state
   * @returns {Array}
   */
  deviceGroups (state) {
    return state.targeting.deviceGroups;
  },

  /**
   * Return only included genres.
   * @param state
   * @returns {Array}
   */
  includedGenres (state) {
    return filterIncluded(state.targeting.genres, true);
  },

  /**
   * Return only excluded genres.
   * @param state
   * @returns {Array}
   */
  excludedGenres (state) {
    return filterIncluded(state.targeting.genres, false);
  },

  /**
   * Get all selected audiences.
   * @param state
   * @returns {(Array|null)}
   */
  audiences (state) {
    return state.targeting.audiences;
  },

  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Update targeting ages with gender information for the existing campaign.
   *
   * @param commit
   * @param rootState
   * @param payload
   * @returns {Promise<*|undefined>}
   */
  async updateAges({ commit, rootState }, payload) {
    commit(types.UPDATE_AGES.PENDING);

    try {
      const response = await api.targeting.ages.update(rootState.wizard.id, payload);
      commit(types.UPDATE_AGES.SUCCESS);
      commit('wizard/budget/fill', response.data.data, { root: true });
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_AGES.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update campaign locations targeting.
   */
  updateLocations: storeHelper.createAction(types.UPDATE_LOCATIONS, {
    call ({ campaign, values }) {
      return api.targeting.locations.update(
        campaign,
        TargetingMapper.mapZipObjectToApi(values.zip),
        TargetingMapper.mapLocationsObjectToApi(values.city),
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Update campaign genres targeting.
   */
  updateGenres: storeHelper.createAction(types.UPDATE_GENRES, {
    call ({ campaign, values }) {
      return api.targeting.genres.update(
        campaign,
        TargetingMapper.mapGenresObjectToApi(values)
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Update campaign devices targeting.
   */
  updateDeviceGroups: storeHelper.createAction(types.UPDATE_DEVICES, {
    call ({ campaign, values }) {
      return api.targeting.devices.update(
        campaign,
        TargetingMapper.mapDevicesObjectToApi(values)
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Update campaign audiences targeting.
   */
  updateAudiences: storeHelper.createAction(types.UPDATE_AUDIENCES, {
    call ({ campaign, values }) {
      return api.targeting.audiences.update(
        campaign,
        TargetingMapper.mapAudiencesObjectToApi(values)
      );
    },
    after: ({ commit }, data) => {
      commit('wizard/budget/fill', data.data, { root: true });
    }
  }),

  /**
   * Search Zip codes from HULU.
   *
   * @param commit
   * @param rootState
   * @param codes
   * @returns {Promise<*|undefined>}
   */
  async searchZip({ commit, rootState }, codes) {
    commit(types.SEARCH_ZIP.PENDING);

    try {
      const response = await api.targeting.locations.searchZip(rootState.wizard.id, codes);
      if (rootState.wizard.targeting.forceQuitZipSearch) {
        throw new Error('Zip code search has been cancelled');
      }
      commit(types.SEARCH_ZIP.SUCCESS, response.data.data);
      commit('setZipValues', {
        items: Locations.sortValues(
          TargetingMapper.mapLocationsOptions(response.data.data.found),
          getCurrentValues(state.targeting.locations.zip)
        )
      });
      commit('setFailValues', {
        items: Locations.sortValues(
          TargetingMapper.mapLocationsOptions(response.data.data.not_found),
          getCurrentValues(state.targeting.locations.fail)
        )
      });
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.SEARCH_ZIP.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Upload csv file
   *
   * @param commit
   * @param rootState
   * @param file
   * @returns {Promise<*|undefined>}
   */
  async uploadCsv({ commit, rootState }, file) {
    commit(types.SEARCH_ZIP.PENDING);

    try {
      const response = await api.targeting.locations.uploadCsv(rootState.wizard.id, file);

      if (rootState.wizard.targeting.forceQuitZipSearch) {
        throw new Error('Zip codes upload has been cancelled');
      }

      commit(types.SEARCH_ZIP.SUCCESS, response.data.data);
      commit('setZipValues', {items: Locations.sortValues(TargetingMapper.mapLocationsOptions(response.data.data.found), state.targeting.locations.zip)});
      commit('setFailValues', {items: Locations.sortValues(TargetingMapper.mapLocationsOptions(response.data.data.not_found), state.targeting.locations.fail)});
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.SEARCH_ZIP.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Fill targeting genders information.
   *
   * @param state
   * @param data
   */
  fill (state, data) {
    state.targeting = TargetingMapper.fillValues(state.targeting, data);
    state.maxCountDevices = data.max_count_devices;
  },

  /**
   * Campaign targeting genders options is successfully updated
   *
   * @param state
   */
  [types.UPDATE_AGES.SUCCESS](state) {
    state[types.UPDATE_AGES.loadingKey] = false;
  },

  /**
   * Campaign targeting genders options updating
   *
   * @param state
   */
  [types.UPDATE_AGES.PENDING](state) {
    state[types.UPDATE_AGES.loadingKey] = true;
  },

  /**
   * Campaign targeting genders options was not updated.
   *
   * @param state
   */
  [types.UPDATE_AGES.FAILURE](state) {
    state[types.UPDATE_AGES.loadingKey] = false;
  },

  /**
   * Campaign targeting groups options is successfully updated
   *
   * @param state
   */
  [types.UPDATE_GROUPS.SUCCESS](state) {
    state[types.UPDATE_GROUPS.loadingKey] = false;
  },

  /**
   * Campaign targeting groups options updating
   *
   * @param state
   */
  [types.UPDATE_GROUPS.PENDING](state) {
    state[types.UPDATE_GROUPS.loadingKey] = true;
  },

  /**
   * Campaign targeting groups options was not updated.
   *
   * @param state
   */
  [types.UPDATE_GROUPS.FAILURE](state) {
    state[types.UPDATE_GROUPS.loadingKey] = false;
  },

  /**
   * Campaign targeting platforms options is successfully updated
   *
   * @param state
   */
  [types.UPDATE_PLATFORMS.SUCCESS](state) {
    state[types.UPDATE_PLATFORMS.loadingKey] = false;
  },

  /**
   * Campaign targeting platforms options updating
   *
   * @param state
   */
  [types.UPDATE_PLATFORMS.PENDING](state) {
    state[types.UPDATE_PLATFORMS.loadingKey] = true;
  },

  /**
   * Campaign targeting platforms options was not updated.
   *
   * @param state
   */
  [types.UPDATE_PLATFORMS.FAILURE](state) {
    state[types.UPDATE_PLATFORMS.loadingKey] = false;
  },

  /**
   * Create mutation for targeting locations
   *
   */
  ...storeHelper.createMutation(types.UPDATE_LOCATIONS),

  /**
   * Update genres mutations.
   */
  ...storeHelper.createMutation(types.UPDATE_GENRES),

  /**
   * Update devices mutations.
   */
  ...storeHelper.createMutation(types.UPDATE_DEVICES),

  /**
   * Update audiences mutations.
   */
  ...storeHelper.createMutation(types.UPDATE_AUDIENCES),

  /**
   * Create mutation for targeting zip codes
   *
   */
  ...storeHelper.createMutation(types.SEARCH_ZIP),

  /**
   * Fields mapping.
   */
  updateField,
};

/**
 * Create dynamic current targeting getter and muttations.
 */
for (let item in state.targeting) {
  if (!isEmpty(state.targeting[item])) {
    for (let innerItem in state.targeting[item]) {
      getters[`get${uppercaseFirstLetter(innerItem)}CurrentValues`] = (state) => { return createCurrentGetter(innerItem, state.targeting[item][innerItem])};
      getters[`get${uppercaseFirstLetter(innerItem)}CountCurrentValues`] = (state) => { return createCountCurrentGetter(state.targeting[item][innerItem])};
      mutations[`set${uppercaseFirstLetter(innerItem)}Values`] = (state, payload) => { return createTargetingMutation(`${item}.${innerItem}`, state, payload)}
    }
    continue;
  }

  getters[`get${uppercaseFirstLetter(item)}CurrentValues`] = (state) => { return createCurrentGetter(item, state.targeting[item])};
  getters[`get${uppercaseFirstLetter(item)}CountCurrentValues`] = (state) => { return createCountCurrentGetter(state.targeting[item])};
  mutations[`set${uppercaseFirstLetter(item)}Values`] = (state, payload) => { return createTargetingMutation(item, state, payload)}
}

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};
