import { concat, find, findIndex, isEmpty, set, get, cloneDeep } from 'lodash';
import { differenceWithByProp, getCurrentValues, valueTypes } from '../../../../../helpers/revision';

/**
 * Get new targeting values.
 */
export const getResultValues = (newValuesArray, currentValues) => {
  if (isEmpty(currentValues)) {
    return newValuesArray.map(el => ({ ...el, type: valueTypes.NEW }));
  }

  let currentItems = cloneDeep(currentValues);

  let newInclude = {
    item: null,
    index: null
  };

  /**
   * This block update targeting type include/exclude.
   */
  const markInclude = (optionValue, item, currentItems, i) => {
    if (optionValue.included !== item.included) {
      const index = findIndex(currentValues, (option => option.value === item.value && option.included !== item.included));
      if (item.type === valueTypes.NEW && index === -1) {
        item.included = optionValue.included;
        return;
      }
      if (item.type === valueTypes.NEW && index !== -1) {
        if (currentItems[index].type === valueTypes.DELETED) {
          currentItems[index].type = valueTypes.CURRENT;
        }
        currentItems.splice(i, 1);
        return;
      }

      if (index === -1 && item.type !== valueTypes.NEW) {
        if (item.type === valueTypes.CURRENT) {
          item.type = valueTypes.DELETED;
        }
        newInclude.item = cloneDeep(item);
        newInclude.item.included = optionValue.included;
        newInclude.item.type = valueTypes.NEW;
        newInclude.index = i;
      }

      return;
    }

    if (item.type === valueTypes.DELETED) {
      item.type = valueTypes.CURRENT;
    }
  }

  /**
   * This block add/remove current or deleted targeting.
   */
  for (let i = 0; i < currentItems.length; i++) {
    const item = currentItems[i]

    let optionValue = find(newValuesArray, (option => option.value === item.value));
    if (optionValue === undefined) {
      if (item.type === valueTypes.NEW) {
        currentItems.splice(i, 1);
        i--;

        continue;
      }
      item.type = valueTypes.DELETED;

      continue;
    }

    if (optionValue.included !== undefined) {
      markInclude(optionValue, item, currentItems, i);
      continue;
    }

    if (item.type === valueTypes.DELETED) {
      item.type = valueTypes.CURRENT;
    }
  }

  /**
   * This block add only new targeting.
   */
  let values = differenceWithByProp(newValuesArray, currentItems, 'value');
  values = values.map(el => ({ ...el, type: valueTypes.NEW }));

  if (newInclude.item) {
    currentItems.splice(newInclude.index, 0, newInclude.item);
  }

  return concat(currentItems, values);
}

/**
 * Set ages and comparing new and current values by labels.
 */
export const setAges = (item, state, payload) => {
  const newItems = payload.items;
  const currentItems = cloneDeep(state.targeting.ages);

  currentItems.forEach((item, i) => {
    let optionValue = find(newItems, (option => option.label === item.label));
    if (optionValue === undefined) {
      if (item.type === valueTypes.NEW) {
        currentItems.splice(i, 1)
      } else {
        item.type = valueTypes.DELETED
      }
      return;
    }

    /**
     * Set current type for deleted item and set value from newItems.
     */
    item.value = optionValue.value;
    if ( item.type === valueTypes.DELETED) {
      item.type = valueTypes.CURRENT;
    }
  })

  /**
   * This block add only new targeting.
   */
  let values = differenceWithByProp(newItems, currentItems, 'value');
  values = values.map(el => ({ ...el, type: valueTypes.NEW }));

  state.targeting.ages = concat(currentItems, values);
}

/**
 * Get gender value.
 */
export const getGenderCurrentValue = (items) => {
  return !isEmpty(getCurrentValues(items)) ? getCurrentValues(items)[0].value : null;
}

/**
 * Set new targeting values.
 */
export const setValues = (item, state, payload) => {
  set(
    state.targeting,
    item,
    getResultValues(
      payload.items,
      get(state.targeting, item)
    ));
}

/**
 * Set new audience item.
 */
export const setAudiences = (item, state, payload) => {
  let data = payload.item;

  if (isEmpty(data)) {
    state.targeting.audiences = data;
    return;
  }

  const index = state.targeting.audiences.findIndex(audience => audience.value === data.value);

  if (payload.checked) {
    if (index === -1) {
      data.type = valueTypes.NEW;
      state.targeting.audiences.push(data);
    } else {
      if (state.targeting.audiences[index].type === valueTypes.DELETED) {
        state.targeting.audiences[index].type = valueTypes.CURRENT;
      }
    }

    return;
  }

  if (typeof state.targeting.audiences[index] !== 'undefined' && state.targeting.audiences[index].type === valueTypes.NEW) {
    state.targeting.audiences.splice(index, 1);
    return;
  }

  if (typeof state.targeting.audiences[index] !== 'undefined' && state.targeting.audiences[index].type === valueTypes.CURRENT) {
    state.targeting.audiences[index].type = valueTypes.DELETED;
  }
}

/**
 * Mapper for getters.
 */
const currentGettersMapping = {
  ages: getCurrentValues,
  audiences: getCurrentValues,
  deviceGroups: getCurrentValues,
  gender: getGenderCurrentValue,
  genres: getCurrentValues,
  city: getCurrentValues,
  fail: getCurrentValues,
  zip:getCurrentValues,
};

/**
 * Mapper for mutations.
 */
const setValuesMapping = {
  ages: setAges,
  gender: setValues,
  deviceGroups: setValues,
  genres: setValues,
  audiences: setAudiences,
  locations: {
    city: setValues,
    fail: setValues,
    zip: setValues,
  }
};

/**
 * Create current targeting getter.
 */
export const createCurrentGetter = (item, items) => {
  return currentGettersMapping[item](items);
}

/**
 * Create count current targeting getter.
 */
export const createCountCurrentGetter = (items) => {
  if (isEmpty(items)) {
    return 0;
  }

  return getCurrentValues(items).length;
}

/**
 * Create targeting mutation.
 */
export const createTargetingMutation = (path, state, payload) => {
  return get(setValuesMapping, path)(path, state, payload);
}


export default {
  getResultValues,
  createCurrentGetter,
  createCountCurrentGetter,
  createTargetingMutation,
}
