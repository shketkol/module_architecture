import options from './parts/options';
import values from './parts/values';
import validation from './parts/validation';
import { merge } from 'lodash';

export default merge(values, options, validation);
