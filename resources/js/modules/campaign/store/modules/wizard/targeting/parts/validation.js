import { getField, updateField } from 'vuex-map-fields';
import { isUndefined } from 'lodash';

export const fillTargetingErrorsWrapper = (commit, payload) => {
  if (!isUndefined(payload.is_action_validation) && payload.is_action_validation) {
    commit('wizard/targeting/fillTargetingErrors', payload, { root: true })
  }
}

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Targeting errors.
   */
  targetingError: {},
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  /**
   * Get targeting action-error options.
   *
   * @param state
   * @returns {array}
   */
  targetingError (state) {
    return state.targetingError;
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Fill the targeting error state.
   *
   * @param state
   * @param data
   */
  fillTargetingErrors (state, data) {
    state.targetingError = data;
  },

  /**
   * Clear all targeting errors.
   *
   * @param state
   */
  clearTargetingErrors (state) {
    state.targetingError = {};
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  fillTargetingErrorsWrapper
};


