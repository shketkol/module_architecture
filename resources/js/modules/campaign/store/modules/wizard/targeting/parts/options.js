import storeHelper from 'Foundation/helpers/store';
import TargetingMapper from '../../../../../api/mappers/TargetingMapper';
import { getField, updateField } from 'vuex-map-fields';
import { campaign } from '../../../../../api';
import { valueTypes } from '../../../../../helpers/revision';

/**
 * Types
 */
const types = {
  LOAD_AGES_OPTIONS: storeHelper.createAsyncMutation('LOAD_AGES_OPTIONS'),
  SEARCH_LOCATIONS: storeHelper.createAsyncMutation('LOAD_LOCATIONS_OPTIONS'),
  SEARCH_GENRES: storeHelper.createAsyncMutation('LOAD_GENRES_OPTIONS'),
  LOAD_AUDIENCES_OPTIONS: storeHelper.createAsyncMutation('LOAD_AUDIENCES_OPTIONS'),
  LOAD_DEVICES_OPTIONS: storeHelper.createAsyncMutation('LOAD_DEVICES_OPTIONS'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.LOAD_AGES_OPTIONS,
    types.SEARCH_LOCATIONS,
    types.SEARCH_GENRES,
    types.LOAD_AUDIENCES_OPTIONS,
    types.LOAD_DEVICES_OPTIONS,
  ]),

  options: {
    /**
     * Genders.
     */
    genders: [],

    /**
     * Ages.
     */
    ages: [],

    /**
     * Locations.
     */
    locations: [],

    /**
     * Genres.
     */
    genres: [],

    /**
     * Audiences.
     */
    audiences: [],

    /**
     * Device groups.
     */
    deviceGroups: [],
  },
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Genders.
   * @param state
   * @returns {*}
   */
  genderOptions (state) {
    return state.options.genders;
  },

  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Load AGES from HULU.
   *
   * @param commit
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  async loadAges({ commit, rootState }) {
    commit(types.LOAD_AGES_OPTIONS.PENDING);

    try {
      const response = await campaign.targeting.ages.show(rootState.wizard.id);
      commit(types.LOAD_AGES_OPTIONS.SUCCESS, TargetingMapper.mapGendersOptions(response.data.data));
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.LOAD_AGES_OPTIONS.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Load Audiences from HULU.
   *
   * @param commit
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  async loadAudiencesOptions({ commit, rootState }) {
    commit(types.LOAD_AUDIENCES_OPTIONS.PENDING);

    try {
      const response = await campaign.targeting.audiences.get(rootState.wizard.id);

      commit(types.LOAD_AUDIENCES_OPTIONS.SUCCESS, response.data.data);
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.LOAD_AUDIENCES_OPTIONS.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Search Locations from HULU.
   *
   * @param commit
   * @param rootState
   * @param needle
   * @returns {Promise<*|undefined>}
   */
  async searchLocations({ commit, rootState }, needle) {
    commit(types.SEARCH_LOCATIONS.PENDING);

    try {
      const response = await campaign.targeting.locations.searchLocations(rootState.wizard.id, needle);
      commit(types.SEARCH_LOCATIONS.SUCCESS, TargetingMapper.mapLocationsOptions(response.data.data));
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.SEARCH_LOCATIONS.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Search Genres from HULU.
   *
   * @param commit
   * @param rootState
   * @param needle
   * @returns {Promise<*|undefined>}
   */
  async searchGenres({commit, rootState}, needle) {
    commit(types.SEARCH_GENRES.PENDING);
    try {
      const response = await campaign.targeting.genres.search(rootState.wizard.id, needle);
      commit(types.SEARCH_GENRES.SUCCESS, TargetingMapper.mapGenresOptions(response.data.data));
      return Promise.resolve(response.data);
    } catch (error) {
      commit(types.SEARCH_GENRES.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Load possible device options.
   */
  loadDevicesOptions: storeHelper.createAction(types.LOAD_DEVICES_OPTIONS, {
    call: campaign.targeting.devices.get,
    map: TargetingMapper.mapDeviceGroupsOptionsToClient,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Campaign targeting genders options is successfully loaded.
   *
   * @param state
   */
  [types.LOAD_AGES_OPTIONS.SUCCESS](state, payload) {
    state.options.genders = payload.map(el => ({ ...el, type: valueTypes.NEW }));
    state[types.LOAD_AGES_OPTIONS.loadingKey] = false;
  },

  /**
   * Campaign targeting genders options is loading.
   *
   * @param state
   */
  [types.LOAD_AGES_OPTIONS.PENDING](state) {
    state[types.LOAD_AGES_OPTIONS.loadingKey] = true;
  },

  /**
   * Campaign targeting genders options was not loaded.
   *
   * @param state
   */
  [types.LOAD_AGES_OPTIONS.FAILURE](state) {
    state[types.LOAD_AGES_OPTIONS.loadingKey] = false;
  },

  /**
   * Create mutation for targeting locations
   *
   */
  ...storeHelper.createMutation(types.SEARCH_LOCATIONS, {
    success(state, payload) {
      state.options.locations = payload;
    },
  }),

  /**
   * Search of targeting genres is successful.
   *
   * @param state
   */
  [types.SEARCH_GENRES.SUCCESS](state, payload) {
    state.options.genres = payload;
    state[types.SEARCH_GENRES.loadingKey] = false;
  },

  /**
   * Searching of targeting genres locations.
   *
   * @param state
   */
  [types.SEARCH_GENRES.PENDING](state) {
    state[types.SEARCH_GENRES.loadingKey] = true;
  },

  /**
   * Search of targeting genres is unsuccessful.
   *
   * @param state
   */
  [types.SEARCH_GENRES.FAILURE](state) {
    state[types.SEARCH_GENRES.loadingKey] = false;
  },

  /**
   * Search of targeting genres is successful.
   *
   * @param state
   */
  [types.LOAD_AUDIENCES_OPTIONS.SUCCESS](state, payload) {
    state.options.audiences = payload;
    state[types.LOAD_AUDIENCES_OPTIONS.loadingKey] = false;
  },

  /**
   * Campaign targeting audiences options is loading.
   *
   * @param state
   */
  [types.LOAD_AUDIENCES_OPTIONS.PENDING](state) {
    state[types.LOAD_AUDIENCES_OPTIONS.loadingKey] = true;
  },

  /**
   * Search of targeting audiences is unsuccessful.
   *
   * @param state
   */
  [types.LOAD_AUDIENCES_OPTIONS.FAILURE](state) {
    state[types.LOAD_AUDIENCES_OPTIONS.loadingKey] = false;
  },

  /**
   * Fill device groups.
   */
  ...storeHelper.createMutation(types.LOAD_DEVICES_OPTIONS, {
    success(state, data) {
      state.options.deviceGroups = data;
    },
  }),

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};
