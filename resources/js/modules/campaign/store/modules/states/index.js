import { getField } from 'vuex-map-fields';
import storeHelper from 'Foundation/helpers/store';
import { campaign as api } from '../../../../campaign/api';

/**
 * Types
 */
const types = {
  GET_PERMISSIONS: storeHelper.createAsyncMutation('GET_PERMISSIONS'),
};

/**
 * Campaign permissions state
 * @type {{{}}}
 */
const state = {
  /**
   * Create state for mutation.
   */
  ...storeHelper.createMutationState([
    types.GET_PERMISSIONS,
  ]),

  states: {
    can_pause: false,
    can_cancel: false,
    can_resume: false,
    can_edit: false,
    can_delete: false,
    can_duplicate: false,
    can_manage_creative: false,
    can_only_replace_creative: false,
    can_download_report: false,
    wizard: {
      can_edit_start_date: false,
      can_edit_end_date: false,
      can_edit_budget: false,
      can_validate_promocode: false,
      targetings: {
        can_edit_age_groups: false,
        can_edit_audiences: false,
        can_edit_locations: false,
        can_edit_zipcodes: false,
        can_edit_genres: false,
        can_edit_platforms: false,
      }
    }
  }
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

const actions = {
  /**
   * Get campaign state resource.
   */
  getCampaignPermissionsState: storeHelper.createAction(types.GET_PERMISSIONS, {
    call: api.getPermissions
  }),
}

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Fill campaign permissions state
   * @param state
   * @param payload
   */
  fillStates: (state, payload) => {
    state.states = payload;
  },

  /**
   * Get permissions mutation
   */
  ...storeHelper.createMutation(types.GET_PERMISSIONS, {
    success (state, payload) {
      state.states = payload.data;
    }
  }),
};

export default {
  namespaced: true,
  actions,
  state,
  mutations,
  getters,
};


