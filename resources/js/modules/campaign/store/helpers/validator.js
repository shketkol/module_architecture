import { Validator } from 'vee-validate';
import { getTrasnformedRules } from 'Foundation/helpers/validation-rules-transformer';

const validator = new Validator();

const validationRules = getTrasnformedRules();

export default {
  /**
   * Name validator.
   * @param value
   * @returns {Promise<VerifyResult>}
   */
  validateName (value) {
    return validator.verify(value, validationRules.campaign.details.name);
  },

  /**
   * Date-range validator.
   * @param value
   * @returns {Promise<any>}
   */
  validateDateRange (value) {
    const rules = [
      validator.verify(value.start, validationRules.campaign.details.date.start),
      validator.verify(value.end, validationRules.campaign.details.date.end)
    ];

    return new Promise((resolve) => {
      Promise.all(rules).then((results) => {
        let isValid = true;
        results.forEach((item) => {
          isValid = item.valid && isValid;
        });
        resolve({ valid: isValid, errors: [] });
      });
    });
  },

  /**
   * AgeGroup validator.
   * @param value
   * @returns {Promise<VerifyResult>}
   */
  validateAgeGroups (value) {
    return validator.verify(value, validationRules.targeting.age_group);
  },

  /**
   * Genres validator.
   * @param value
   * @returns {boolean}
   */
  validateGenres (value) {
    return Array.isArray(value);
  },

  /**
   * Devices validator.
   * @param value
   * @returns {boolean}
   */
  validateDevices (value) {
    return validator.verify(value, validationRules.targeting.device_group.groups);
  },

  /**
   * Audiences validator.
   * @param value
   * @returns {boolean}
   */
  validateAudiences (value) {
    return Array.isArray(value);
  },

  /**
   * Locations validator.
   * @param value
   * @returns {boolean}
   */
  validateLocations (value) {
    return Array.isArray(value.zip) && Array.isArray(value.city);
  },

  /**
   * Creative validator.
   * @param value
   * @returns {Promise<any>}
   */
  validateCreative (value) {
    return validator.verify(value, 'required');
  },

  /**
   * Budget validator (based on the campaign cost).
   * @param {number} cost
   * @returns {boolean}
   */
  validateBudget (cost) {
    return !!+cost;
  },
};
