import Vue from 'vue';
import Vuex from 'vuex';
import wizard from './modules/wizard';
import details from './modules/details';
import states from './modules/states';
import metrics from './modules/metrics';
import creative from 'Modules/creative/store/modules/creative';
import payment from 'Modules/payment/store/modules/payment';
import permissions from 'Modules/user/store/modules/permissions';
import notifications from 'Modules/notification/store/modules/notifications';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    wizard,
    creative,
    permissions,
    payment,
    details,
    states,
    metrics,
    notifications,
  },
});

export default store;
