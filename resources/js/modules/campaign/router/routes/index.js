import Campaign from './campaign';
import concat from 'lodash/concat';
import Wizard from './wizard';

export default concat(Campaign, Wizard);
