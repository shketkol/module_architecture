import CampaignBudget from '../../components/wizard/steps/budget/CampaignBudget';
import CampaignCreative from '../../components/wizard/steps/creative/CampaignCreative';
import CampaignAges from '../../components/wizard/steps/targeting/CampaignAges';
import CampaignGenres from '../../components/wizard/steps/targeting/CampaignGenres';
import CampaignDevices from '../../components/wizard/steps/targeting/CampaignDevices';
import CampaignLocations from '../../components/wizard/steps/targeting/CampaignLocations';
import CampaignName from '../../components/wizard/steps/details/CampaignName';
import CampaignReview from '../../components/wizard/steps/review-and-payment/CampaignReview';
import CampaignSchedule from '../../components/wizard/steps/details/CampaignSchedule';
import CampaignSuccess from '../../components/wizard/steps/success/CampaignSuccess';
import concat from 'lodash/concat';
import CampaignAudiences from 'Modules/campaign/components/wizard/steps/targeting/CampaignAudiences';
import {isDemo} from 'Modules/demo/helpers/demo';
import DemoOrder from 'Modules/demo/modules/campaign/components/wizard/steps/review-and-payment/Order';
import OrderPage from '../../components/wizard/steps/review-and-payment/Order';

const Order = isDemo() ? DemoOrder : OrderPage;

const create = [
  {
    path: '/campaigns/create/details/name',
    name: 'campaigns.create.details.name',
    component: CampaignName,
  },
  {
    path: '/campaigns/create/details/schedule',
    name: 'campaigns.create.details.schedule',
    component: CampaignSchedule,
  },
  {
    path: '/campaigns/create/targeting/ages',
    name: 'campaigns.create.targeting.ages',
    component: CampaignAges,
  },
  {
    path: '/campaigns/create/targeting/locations',
    name: 'campaigns.create.targeting.locations',
    component: CampaignLocations,
  },
  {
    path: '/campaigns/create/targeting/genres',
    name: 'campaigns.create.targeting.genres',
    component: CampaignGenres,
  },
  {
    path: '/campaigns/create/targeting/devices',
    name: 'campaigns.create.targeting.devices',
    component: CampaignDevices,
  },
  {
    path: '/campaigns/create/targeting/audiences',
    name: 'campaigns.create.targeting.audiences',
    component: CampaignAudiences,
  },
  {
    path: '/campaigns/create/budget',
    name: 'campaigns.create.budget',
    component: CampaignBudget,
  },
  {
    path: '/campaigns/create/creative',
    name: 'campaigns.create.creative',
    component: CampaignCreative,
  },
  {
    path: '/campaigns/create/review-and-payment/review',
    name: 'campaigns.create.reviewAndPayment.review',
    component: CampaignReview,
  },
  {
    path: '/campaigns/create/review-and-payment/payment',
    name: 'campaigns.create.reviewAndPayment.payment',
    component: Order,
  },
  {
    path: '/campaigns/create/success',
    name: 'campaigns.create.success',
    component: CampaignSuccess,
  },
];

export default concat(create, create.map(route => {
  return {
    path: route.path.replace('/create', '/:campaignId/edit'),
    name: route.name.replace('create', 'edit'),
    component: route.component,
  };
}));
