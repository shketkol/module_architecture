import 'Foundation/bootstrap';
import 'Foundation/services/amplify'
import InvalidResetPasswordTokenPage from './pages/TheInvalidResetPasswordToken';
import RegisterFormPage from './pages/TheRegisterForm';
import LockPage from './pages/TheBrowserLock';
import MaintenancePage from './pages/TheMaintenance';
import LoginFailedPage from './pages/TheLoginFailed';
import { getRouter } from 'Foundation/services/routing/factory';
import routes from './router/routes';
import store from './store';
import Vue from 'vue';
import AdminLoginRestrictedPage from "Modules/auth/pages/TheAdminLoginRestricted";
import { isDemo } from 'Modules/demo/helpers/demo';
import DemoTheLoginForm from 'Modules/demo/modules/auth/pages/TheLoginForm';
import TheLoginForm from './pages/TheLoginForm';
import DemoTheForgotPassword from 'Modules/demo/modules/auth/pages/TheForgotPassword';
import TheForgotPassword from './pages/TheForgotPassword';

Vue.component('LoginFormPage', isDemo() ? DemoTheLoginForm : TheLoginForm);
Vue.component('ForgotPasswordPage', isDemo() ? DemoTheForgotPassword : TheForgotPassword);

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    InvalidResetPasswordTokenPage,
    RegisterFormPage,
    LockPage,
    LoginFailedPage,
    AdminLoginRestrictedPage,
    MaintenancePage
  },

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),

  /**
   * Store
   */
  store,
}));
