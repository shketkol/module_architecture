import RegisterAddress from '../../components/register/RegisterAddress';
import RegisterCompanyName from '../../components/register/RegisterCompanyName';
import RegisterEmail from '../../components/register/RegisterEmail';
import RegisterPhone from '../../components/register/RegisterPhone';
import RegisterSuccess from '../../components/register/RegisterSuccess';
import { isDemo } from 'Modules/demo/helpers/demo';
import DemoRegisterName from 'Modules/demo/modules/auth/components/register/RegisterName';
import RegisterName from '../../components/register/RegisterName';

const RegisterPage = isDemo() ? DemoRegisterName : RegisterName;

export default [
  {
    path: '/signup',
    name: 'register',
    component: RegisterPage,
  },
  {
    path: '/signup/email',
    name: 'register.email',
    component: RegisterEmail,
  },
  {
    path: '/signup/company-name',
    name: 'register.companyName',
    component: RegisterCompanyName,
  },
  {
    path: '/signup/phone',
    name: 'register.phone',
    component: RegisterPhone,
  },
  {
    path: '/signup/address',
    name: 'register.address',
    component: RegisterAddress,
  },
  {
    path: '/signup/success',
    name: 'register.success',
    component: RegisterSuccess,
  }
]
