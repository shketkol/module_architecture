import { browserUnsupported } from 'Foundation/helpers/browser-detect'

export default {

  /**
   * Computed properties.
   */
  computed: {

    /**
     * Whether it stage or not.
     */
    isStage() {
      return window.location.host.indexOf('stage') !== -1;
    },

    /**
     * Whether the user's browser unsupported.
     */
    isBrowserUnsupported() {
      return browserUnsupported()
    },
  },

  methods: {
    /**
     * Whether unsupported browser lock page should be shown.
     */
    processBrowserLock() {
      if (this.isBrowserUnsupported) {
        window.location.href = this.$getRoute('lock');
      }
    },
  },

  /**
   * Created.
   */
  created() {
    this.processBrowserLock();
  },
};
