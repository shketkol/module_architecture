import { merge, omit } from 'lodash';
import pick from 'lodash/pick';

const VALIDATION_SCOPE_PASSWORD = 'password';

export default {
  /**
   * Reactive data.
   */
  data () {
    return {
      validateOnInput: false,
      rawNewPassword: '',
      rawNewPasswordConfirmation: '',
    };
  },
  /**
   * Computed properties.
   */
  computed: {
    /**
     * Validation rules.
     */
    validation () {
      return {
        password: pick(this.validationRules.user.password, 'required'),
      };
    },

    /**
     * Validation rules for "Password" field.
     * @returns {*}
     */
    newPasswordValidation () {
      return this.validationRules.user.password;
    },

    /**
     * Validation rules for "Confirm password" field.
     * @returns {*}
     */
    newPasswordConfirmationValidation () {
      const passwordConfirmationRules = omit(this.newPasswordValidation, ['min', 'regex']);
      return merge({ confirmed: this.newPassword }, passwordConfirmationRules);
    },

    /**
     * We need separate passwords scope to detach password validation from the rest of the form.
     * @returns {string}
     */
    passwordScope () {
      return VALIDATION_SCOPE_PASSWORD;
    }
  },

  /**
   * Watchers.
   */
  watch: {
    /**
     * Trim and validate password field.
     */
    rawNewPassword (val) {
      this.newPassword = val.trim();
      if (this.validateOnInput) {
        this.validateNewPassword();
        if (this.newPasswordConfirmation) {
          this.validateNewPasswordConfirmation();
        }
      }
    },

    /**
     * Trim and validate password confirmation field.
     */
    rawNewPasswordConfirmation (val) {
      this.newPasswordConfirmation = val.trim();
      if (this.validateOnInput) {
        this.validateNewPasswordConfirmation();
      }
    },
  },

  methods: {
    /**
     * @returns {Promise<boolean>}
     */
    validateNewPassword (customErrorMessage = null) {
      return this.verifyField(
        'new_password',
        this.newPassword,
        'newPasswordValidation',
        customErrorMessage
      );
    },

    /**
     * @returns {Promise<boolean>}
     */
    validateNewPasswordConfirmation () {
      return this.verifyField(
        'new_password_confirmation',
        this.newPasswordConfirmation,
        'newPasswordConfirmationValidation'
      );
    },

    /**
     * Validate particular password value.
     * @param {string} field
     * @param {string} value
     * @param {string} validationRules
     * @param {null} customErrorMessage
     * @returns {Promise<boolean>}
     */
    verifyField (field, value, validationRules, customErrorMessage = null) {
      return this.$validator.verify(value, this[validationRules], {name: field}).then(({ errors }) => {
        if (errors.length) {
          this.$refs[field].addError(customErrorMessage ? customErrorMessage : errors[0]);
          return false;
        } else {
          this.$refs[field].resetFieldErrors();
          return true;
        }
      });
    },
  },
};
