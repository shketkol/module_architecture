import { getField, updateField } from 'vuex-map-fields';
import Auth from '@aws-amplify/auth';
import storeHelper from 'Foundation/helpers/store';
import {login as api} from "Modules/auth/api";

/**
 * Types
 */
const types = {
  COGNITO_LOGIN: storeHelper.createAsyncMutation('COGNITO_LOGIN'),
  LOGIN: storeHelper.createAsyncMutation('LOGIN'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.COGNITO_LOGIN,
    types.LOGIN,
  ]),

  /**
   * Cognito user object
   */
  user: null,

  /**
   * Form values.
   */
  form: {
    email: null,
    password: null,
  },
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */

const actions = {
  /**
   * Login user to the system.
   *
   * @param commit
   * @param state
   * @returns {Promise<*>}
   */
  async cognitoLogin ({ commit, state }) {

    commit(types.COGNITO_LOGIN.PENDING);

    try {
      const response = await Auth.signIn(
        state.form.email,
        state.form.password.trim()
      );
      commit(types.COGNITO_LOGIN.SUCCESS, response);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.COGNITO_LOGIN.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Login
   *
   * @param state, payload
   */
  platformLogin: storeHelper.createAction(types.LOGIN, {
    call: api.login,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Cognito login mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.COGNITO_LOGIN, {
    success(state, payload) {
      state.user = payload;
    }
  }),

  /**
   * Login mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.LOGIN),

  /**
   * Reset login fields
   *
   * @param state
   */
  resetLoginFields(state) {
    state.form.email = null;
    state.form.password = null;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


