import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { login as api } from '../../api';
import Auth from '@aws-amplify/auth';

/**
 * Types
 */
const types = {
  CREATE_PASSWORD: storeHelper.createAsyncMutation('CREATE_PASSWORD'),
  FIRST_LOGIN: storeHelper.createAsyncMutation('FIRST_LOGIN'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.FIRST_LOGIN,
    types.CREATE_PASSWORD
  ]),

  /**
   * Form.
   */
  form: {
    password: null,
    passwordConfirmation: null,
  },

  /**
   * Cognito logged user object.
   */
  loggedUser: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {

  async cognitoCreatePassword ({ commit, state }, payload) {
    commit(types.CREATE_PASSWORD.PENDING);
    try {
      const response = await Auth.completeNewPassword(payload, state.form.password);
      commit(types.CREATE_PASSWORD.SUCCESS, response);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.CREATE_PASSWORD.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * First login
   *
   * @param state, payload
   */
  firstLogin: storeHelper.createAction(types.FIRST_LOGIN, {
    call: api.firstLogin,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Cognito create password mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.CREATE_PASSWORD, {
    success(state, payload) {
      state.loggedUser = payload;
    }
  }),

  /**
   * First login mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.FIRST_LOGIN),

  /**
   * Reset password fields
   *
   * @param state
   */
  resetPasswords(state) {
    state.form.password = null;
    state.form.passwordConfirmation = null;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


