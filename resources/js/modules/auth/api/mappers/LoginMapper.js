export default class LoginMapper {
  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @returns {{}}
   */
  static mapObjectToApi (data) {
    return {
      'token': data.signInUserSession.accessToken.jwtToken,
      'refreshToken': data.signInUserSession.refreshToken.token,
    };
  }
}
