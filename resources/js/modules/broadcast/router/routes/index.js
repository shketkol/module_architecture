import concat from 'lodash/concat';
import Broadcast from './broadcast';

export default concat(Broadcast);
