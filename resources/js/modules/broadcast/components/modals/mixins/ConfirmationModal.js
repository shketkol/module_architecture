const TYPE_CANCEL = 'cancel';
const TYPE_SEND_NOW = 'send_now';
const TYPE_TEST_EMAIL = 'test_email';

export default {
  /**
   * Computed properties
   */
  computed: {
    /**
     * Confirmation modal title.
     */
    confirmationModalTitle () {
      if (this.showInputTestEmailModal) {
        return this.$trans('broadcast::labels.send_test_email_title');
      }

      if (this.showSendNowModal) {
        return this.$trans('broadcast::labels.send_now_modal_title');
      }

      return this.$trans('broadcast::labels.close_modal_title');
    },

    /**
     * Confirmation modal submit label.
     */
    confirmationModalSubmitLabel () {
      if (this.showInputTestEmailModal) {
        return this.$trans('broadcast::labels.send_email_button');
      }

      if (this.showSendNowModal) {
        return this.$trans('broadcast::actions.send_now_button');
      }

      return this.$trans('actions.yes');
    },

    /**
     * Confirmation modal cancel label.
     */
    confirmationModalCancelLabel () {
      if (this.showInputTestEmailModal || this.showSendNowModal) {
        return this.$trans('actions.cancel');
      }

      return this.$trans('actions.no');
    },

    /**
     * Confirmation modal warning message.
     */
    confirmationModalWarningMessage () {
      if (this.showInputTestEmailModal) {
        return this.$trans('broadcast::messages.enter_test_email');
      }

      if (this.showSendNowModal) {
        return this.$trans('broadcast::messages.send_now_modal_warning_message');
      }

      return this.$trans('broadcast::messages.close_modal_body');
    },

    /**
     * Confirmation modal notice message.
     */
    confirmationModalNoticeMessage () {
      if (this.showSendNowModal) {
        return this.$trans('broadcast::messages.send_now_modal_notice_message');
      }

      return '';
    },

    /**
     * Confirmation modal loading.
     */
    confirmationModalLoading () {
      return this.sendNowBroadcastPending || this.sendTestBroadcastPending;
    },

    /**
     * Return boolean if confirmation modal equals 'test_email'.
     */
    showInputTestEmailModal () {
      return this.typeConfirmModal === TYPE_TEST_EMAIL;
    },

    /**
     * Return boolean if confirmation modal equals 'send_now'.
     */
    showSendNowModal () {
      return this.typeConfirmModal === TYPE_SEND_NOW;
    }
  },

  /**
   * Watch.
   */
  watch: {
    /**
     * Watch typeConfirmModal property.
     */
    typeConfirmModal (value) {
      if (value === TYPE_TEST_EMAIL) {
        this.testEmail = null;
        this.$clearErrors();
      }
    }
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * On before close event.
     */
    handleClose (done) {
      if (JSON.stringify(this.broadcastData) !== JSON.stringify(this.initialData)) {
        this.showConfirmModal = true;
        this.typeConfirmModal = TYPE_CANCEL;
        return;
      }

      return done();
    },

    /**
     * Init send test email modal.
     */
    initTestEmailModal () {
      this.showConfirmModal = true;
      this.typeConfirmModal = TYPE_TEST_EMAIL;
    },

    /**
     * Init send now modal.
     */
    initSendNowModal () {
      this.showConfirmModal = true;
      this.typeConfirmModal = TYPE_SEND_NOW;
    },

    /**
     * Confirmation modal submit handler
     */
    confirmationModalSubmitHandler () {
      if (this.showInputTestEmailModal) {
        return this.sendTestEmail();
      }

      if (this.showSendNowModal) {
        return this.sendNowHandler();
      }

      return this.close();
    },

    /**
     * Confirmation modal cancel handler
     */
    confirmationModalCancelHandler () {
      this.showConfirmModal = false;
      this.typeConfirmModal = null;
    },
  }
}
