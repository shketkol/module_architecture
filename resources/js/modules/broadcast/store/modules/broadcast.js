import cloneDeep from 'lodash/cloneDeep';
import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import {broadcast as api} from "Modules/broadcast/api";
import BroadcastMapper from "Modules/broadcast/api/mappers/BroadcastMapper";

/**
 * Types
 */
const types = {
  GET_BROADCAST_PARAMS: storeHelper.createAsyncMutation('GET_BROADCAST_PARAMS'),
  STORE_BROADCAST: storeHelper.createAsyncMutation('STORE_BROADCAST'),
  SHOW_BROADCAST: storeHelper.createAsyncMutation('SHOW_BROADCAST'),
  UPDATE_BROADCAST: storeHelper.createAsyncMutation('UPDATE_BROADCAST'),
  SCHEDULE_BROADCAST: storeHelper.createAsyncMutation('SCHEDULE_BROADCAST'),
  UNSCHEDULE_BROADCAST: storeHelper.createAsyncMutation('UNSCHEDULE_BROADCAST'),
  CLONE_BROADCAST: storeHelper.createAsyncMutation('CLONE_BROADCAST'),
  SEND_TEST_BROADCAST: storeHelper.createAsyncMutation('SEND_TEST_BROADCAST'),
  SEND_NOW_BROADCAST: storeHelper.createAsyncMutation('SEND_NOW_BROADCAST'),
  GET_COUNT_AFFECTED_ADVERTISERS: storeHelper.createAsyncMutation('GET_COUNT_AFFECTED_ADVERTISERS'),
};

/**
 * Default broadcast data state
 * @type {{}}
 */
const defaultBroadcastData = {
  id: null,
  name: null,
  date: null,
  channel: null,
  email: {
    subject: '',
    header: '',
    icon: '',
    body: '',
  },
  inApp: {
    category: null,
    body: '',
  },
  segment: {
    id: null,
    advertisers: [],
    dates: {
      start: null,
      end: null
    },
    affected_count: 0
  }
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.GET_BROADCAST_PARAMS,
    types.STORE_BROADCAST,
    types.SHOW_BROADCAST,
    types.UPDATE_BROADCAST,
    types.SCHEDULE_BROADCAST,
    types.UNSCHEDULE_BROADCAST,
    types.CLONE_BROADCAST,
    types.SEND_TEST_BROADCAST,
    types.SEND_NOW_BROADCAST,
    types.GET_COUNT_AFFECTED_ADVERTISERS,
  ]),

  /**
   * Broadcast email data.
   */
  broadcastData: cloneDeep(defaultBroadcastData),

  /**
   * Initial broadcast data.
   */
  initialData: cloneDeep(defaultBroadcastData),

  /**
   * Email icons for email form.
   */
  emailIcons: [],

  /**
   * Notification categories for in-app form.
   */
  notificationCategories: [],

  /**
   * Channels notification.
   */
  channels: [],

  /**
   * Segments.
   */
  segments: [],

  /**
   * Broadcast states.
   */
  states: {},

  /**
   * Check loaded params.
   */
  paramsLoaded: false,

  /**
   * Test email address.
   */
  testEmail: null,
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Get broadcast params
   *
   * @param state, payload
   */
  getParams: storeHelper.createAction(types.GET_BROADCAST_PARAMS, {
    call: api.getParams,
  }),

  /**
   * Store broadcast
   *
   * @param state, payload
   */
  store: storeHelper.createAction(types.STORE_BROADCAST, {
    call: api.store,
  }),

  /**
   * Show broadcast
   *
   * @param state, payload
   */
  show: storeHelper.createAction(types.SHOW_BROADCAST, {
    call: api.show,
  }),

  /**
   * Update broadcast
   *
   * @param state, payload
   */
  update: storeHelper.createAction(types.UPDATE_BROADCAST, {
    call: api.update,
  }),

  /**
   * Schedule broadcast
   *
   * @param state, payload
   */
  schedule: storeHelper.createAction(types.SHOW_BROADCAST, {
    call: api.schedule,
  }),

  /**
   * Unschedule broadcast
   *
   * @param state, payload
   */
  unschedule: storeHelper.createAction(types.UNSCHEDULE_BROADCAST, {
    call: api.unchedule
  }),

  /**
   * Clone broadcast
   *
   * @param state, payload
   */
  clone: storeHelper.createAction(types.CLONE_BROADCAST, {
    call: api.clone
  }),

  /**
   * Send test broadcast
   *
   * @param state, payload
   */
  testEmailBroadcast: storeHelper.createAction(types.SEND_TEST_BROADCAST, {
    call: api.testEmailBroadcast
  }),

  /**
   * Send now broadcast
   *
   * @param state, payload
   */
  sendNow: storeHelper.createAction(types.SEND_NOW_BROADCAST, {
    call: api.sendNow
  }),

  /**
   * Send now broadcast
   *
   * @param state, payload
   */
  getCountAffectedAdvertisers: storeHelper.createAction(types.GET_COUNT_AFFECTED_ADVERTISERS, {
    call: api.countAffectedAdvertisers
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Get broadcast params mutation.
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.GET_BROADCAST_PARAMS, {
    success(state, { data }) {
      state.emailIcons = data.emailIcons;
      state.notificationCategories = data.notificationCategories;
      state.channels = data.channels;
      state.broadcastData.channel = data.channels[0];
      state.segments = data.segments;
      state.broadcastData.segment.id = data.segments[0];
      state.paramsLoaded = true;
      state.initialData = state.broadcastData;
    }
  }),

  /**
   * Store broadcast
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.STORE_BROADCAST, {
    success(state, { data }) {
      state.broadcastData = BroadcastMapper.mapObjectFormApi(data);
      state.initialData = BroadcastMapper.mapObjectFormApi(data);
      state.states = data.states;
    }
  }),

  /**
   * Update broadcast
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.UPDATE_BROADCAST, {
    success(state, { data }) {
      state.broadcastData = BroadcastMapper.mapObjectFormApi(data);
      state.initialData = BroadcastMapper.mapObjectFormApi(data);
    }
  }),

  /**
   * Show broadcast
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SHOW_BROADCAST, {
    success(state, { data }) {
      state.broadcastData = BroadcastMapper.mapObjectFormApi(data);
      state.initialData = BroadcastMapper.mapObjectFormApi(data);
      state.states = data.states;
    }
  }),

  /**
   * Schedule broadcast
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SCHEDULE_BROADCAST, {
    success(state) {
      state.broadcastData = cloneDeep(defaultBroadcastData);
      state.initialData = cloneDeep(defaultBroadcastData);
    }
  }),

  /**
   * Unschedule broadcast
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.UNSCHEDULE_BROADCAST, {
    success(state, { data }) {
      state.states = data.states;
    }
  }),

  /**
   * Reset broadcast data
   *
   * @param state
   */
  reset(state) {
    state.broadcastData = cloneDeep(defaultBroadcastData);
    state.initialData = cloneDeep(defaultBroadcastData);
  },

  /**
   * Clone broadcast mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.CLONE_BROADCAST, {
    success(state, { data }) {
      state.broadcastData = BroadcastMapper.mapObjectFormApi(data);
      state.initialData = BroadcastMapper.mapObjectFormApi(data);
      state.states = data.states;
    }
  }),

  /**
   * Send test broadcast mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SEND_TEST_BROADCAST, {
    success(state) {
      state.testEmail = null;
    }
  }),

  /**
   * Send now broadcast mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.SEND_NOW_BROADCAST, {
    success(state, { data }) {
      state.broadcastData = BroadcastMapper.mapObjectFormApi(data);
      state.initialData = BroadcastMapper.mapObjectFormApi(data);
      state.states = data.states;
    }
  }),

  /**
   * Get count affected advertisers mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.GET_COUNT_AFFECTED_ADVERTISERS, {
    success(state, { data }) {
      state.broadcastData.segment.affected_count = data.count;
    }
  }),

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


