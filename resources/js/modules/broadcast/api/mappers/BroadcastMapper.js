import { formatToApi } from 'Foundation/helpers/moment';
import moment from 'moment-timezone';
import DateTimeFormat from 'Foundation/services/intl/DateTimeFormat';

const dateHelper = new DateTimeFormat();

export default class BroadcastMapper {
  /**
   * Returns object that should be sent to API.
   *
   * @param data
   * @param emailContent
   * @param inAppContent
   * @returns {{}}
   */
  static mapObjectToApi (data, emailContent = false, inAppContent = false) {
    let obj = {
      name: data.name,
      schedule_date: this.getFormattedDates(data.date, true),
      channel: data.channel,
      segment_group: {
        id: data.segment.id,
        advertisers: data.segment.advertisers.map((item) => {
          return item.value
        }),
        dates: this.getDateObjectForApi(data.segment.dates)
      }
    };

    if (emailContent && (
      data.email.subject ||
      data.email.header ||
      data.email.icon ||
      data.email.body
    )) {
      obj.email = {
        subject: data.email.subject,
        header: data.email.header,
        icon: data.email.icon,
        body: data.email.body ? data.email.body.replace('<p></p>', '') : null,
      };
    }

    if (inAppContent && (
      data.inApp.category ||
      data.inApp.body
    )) {
      obj.notification = {
        notification_category_id: data.inApp.category ? data.inApp.category : null,
        body: data.inApp.body ? data.inApp.body.replace('<p></p>', '') : null,
      };
    }

    return obj;
  }

  /**
   * Returns object that should be sent form API.
   *
   * @param data
   * @returns {{}}
   */
  static mapObjectFormApi (data) {
    return {
      id: data.id,
      name: data.name,
      date: data.schedule_date ?
        dateHelper.formatDate(data.schedule_date, {format: window.config.date.format.js_full_date_time}) :
        null,
      inApp: data.notification ? {
        category: data.notification.category.id,
        body: data.notification.body,
      } : {
        category: null,
        body: '',
      },
      email: data.email ? data.email : {
        subject: '',
        header: '',
        icon: '',
        body: '',
      },
      segment: {
        id: data.groups.id,
        advertisers: data.groups.recipients ? data.groups.recipients.map((item) => {
          return {
            value: item.user_id,
            label: item.company_name
          }
        }) : [],
        dates: {
          start: data.groups.dates ?
            dateHelper.formatDate(data.groups.dates.date_start, {format: window.config.date.format.js_full_date_time}) :
            null,
          end: data.groups.dates ?
            dateHelper.formatDate(data.groups.dates.date_end, {format: window.config.date.format.js_full_date_time}) :
            null,
        },
        affected_count: data.groups.affected_count ? data.groups.affected_count : 0
      },
      channel: data.channel,
    }
  }

  /**
   * Returns formatted date.
   *
   * @param date
   * @param setTime
   */
  static getFormattedDates(date, setTime = false) {

    if (date) {
      date = moment(date).tz(window.config.date.default_timezone_full_code, true);

      if (setTime) {
        date = date.set({minute: 0, second: 0})
      }

      return formatToApi(date.toDate(), true);
    }

    return date;
  }

  /**
   * Returns date object for api.
   *
   * @param dates
   */
  static getDateObjectForApi(dates) {

    let today = moment();
    let endDate = null;

    if (dates && dates.end) {
      endDate = moment(dates.end)
        .tz(window.config.date.default_timezone_full_code, true)
        .set({hour: 23, minute: 59, second: 59})
        .format(window.config.date.format.js_full_date_time);
    }

    if (dates && dates.end && moment(dates.end).isSame(today, 'd')) {
      today = today.tz(window.config.date.default_timezone_full_code);
      endDate = today.toDate();
    }

    return {
      date_start: dates && dates.start ? this.getFormattedDates(dates.start) : null,
      date_end: endDate ? this.getFormattedDates(endDate) : null
    };
  }
}
