import axios from 'axios';
import route from 'Foundation/services/routing/route';
import BroadcastMapper from 'Modules/broadcast/api/mappers/BroadcastMapper';

export default {
  /**
   * Get broadcast params
   *
   * @returns {AxiosPromise<any>}
   */
  getParams () {
    return axios.get(route('api.broadcast.params'));
  },

  /**
   * Store broadcast
   * @param data
   *
   * @returns {AxiosPromise<any>}
   */
  store (data) {
    return axios.post(
      route('api.broadcast.store'),
      BroadcastMapper.mapObjectToApi(data.form, data.emailContent, data.inAppContent)
    );
  },

  /**
   * Show broadcast
   * @param id
   *
   * @returns {AxiosPromise<any>}
   */
  show (id) {
    return axios.get(route('api.broadcast.show', {broadcast: id}));
  },

  /**
   * Update broadcast
   * @param data
   *
   * @returns {AxiosPromise<any>}
   */
  update (data) {
    return axios.patch(
      route('api.broadcast.edit', {broadcast: data.id}),
      BroadcastMapper.mapObjectToApi(data.form, data.emailContent, data.inAppContent)
    );
  },

  /**
   * Schedule broadcast
   * @param data
   *
   * @returns {AxiosPromise<any>}
   */
  schedule (data) {
    return axios.patch(
      route('api.broadcast.schedule', {broadcast: data.id}),
      BroadcastMapper.mapObjectToApi(data.form, data.emailContent, data.inAppContent));
  },

  /**
   * Unschedule broadcast
   * @param id
   *
   * @returns {AxiosPromise<any>}
   */
  unchedule (id) {
    return axios.patch(route('api.broadcast.unschedule', {broadcast: id}));
  },

  /**
   * Clone broadcast
   * @param id
   *
   * @returns {AxiosPromise<any>}
   */
  clone (id) {
    return axios.get(route('api.broadcast.clone', {broadcast: id}));
  },

  /**
   * Send test broadcast
   * @param data
   *
   * @returns {AxiosPromise<any>}
   */
  testEmailBroadcast (data) {
    return axios.post(
      route('api.broadcast.send.test.email', {broadcast: data.id}),
      {
        test_email: data.testEmail
      }
    );
  },

  /**
   * Send now broadcast
   * @param id
   *
   * @returns {AxiosPromise<any>}
   */
  sendNow (id) {
    return axios.get(route('api.broadcast.sendNow', {broadcast: id}));
  },

  /**
   * Get count affected advertiser
   * @param date
   *
   * @returns {AxiosPromise<any>}
   */
  countAffectedAdvertisers (date) {
    return axios.post(route('api.broadcast.count.affected.advertisers'), date);
  },
};
