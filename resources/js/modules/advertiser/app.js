import 'Foundation/bootstrap';
import 'Foundation/services/plyr';
import IndexPage from './pages/TheIndex';
import ShowPage from './pages/TheShow';
import { getRouter } from 'Foundation/services/routing/factory';
import routes from './router/routes';
import store from './store';
import Vue from 'vue';
import UnderMinPerDayContent from "../campaign/components/wizard/common/UnderMinPerDayContent";

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    IndexPage,
    ShowPage,
    UnderMinPerDayContent
  },

  /**
   * Vuex.
   */
  store,

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),
}));
