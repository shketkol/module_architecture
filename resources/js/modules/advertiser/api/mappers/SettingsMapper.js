export default class SettingsMapper {
  /**
   *
   * @param data
   * @returns {*}
   */
   static mapSettings = (data) => {
      return data.reduce(function (result, item) {
        result[item.name] = item.value;
        return result;
      }, {});
  };
}
