import SettingsMapper from './SettingsMapper';

export default class AdvertiserMapper {
  static mapApiToObject (response) {
    const data = response.data.data;

    return {
      id: data.id,
      name: `${data.first_name} ${data.last_name}`,
      email: data.email,
      companyName: data.company ? data.company.company_name : null,
      phone: data.phone_number,
      address: data.company ? `${data.company.address_line1} ${data.company.address_line2}` : null,
      location: data.company ? `${data.company.city}, ${data.company.state} ${data.company.zipcode}` : null,
      status: data.status ? data.status.name : null,
      states: data.states,
      advertiserSfdcUrl: data.advertiserSfdcUrl ? data.advertiserSfdcUrl : null,
      brandSfdcUrl: data.brandSfdcUrl ? data.brandSfdcUrl : null,
      contactSfdcUrl: data.contactSfdcUrl ? data.contactSfdcUrl : null,
      specialAccountType: data.specialAccountType ? data.specialAccountType : false,
      accountTypes: data.account_types ? data.account_types : {},
      settings: data.settings ? SettingsMapper.mapSettings(data.settings) : {},
      countCreativeSubmitted: data.count_creative_submitted ? data.count_creative_submitted: null
    };
  }
}
