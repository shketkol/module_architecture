import axios from 'axios';
import route from 'Foundation/services/routing/route';

export default {
  /**
   * Get the specified resource.
   *
   * @param advertiser
   * @returns {AxiosPromise<any>}
   */
  show (advertiser) {
    return axios.get(route('api.advertisers.show', { advertiser }));
  },

  /**
   * Activate the advertiser.
   *
   * @param advertiser
   * @returns {AxiosPromise<any>}
   */
  activate (advertiser) {
    return axios.post(route('api.advertisers.activate', { advertiser }));
  },

  /**
   * Deactivate the advertiser.
   *
   * @param advertiser
   * @returns {AxiosPromise<any>}
   */
  deactivate (advertiser) {
    return axios.post(route('api.advertisers.deactivate', { advertiser }));
  },

  /**
   * Search the advertisers.
   *
   * @param needle
   * @returns {AxiosPromise<any>}
   */
  search (needle) {
    return axios.get(
      route('api.advertisers.search', { query: needle })
    );
  },

  /**
   * Change account type.
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  changeAccountType (data) {
    return axios.post(route('api.advertisers.account-type', {advertiser: data.advertiserId} ), data.data);
  },

  /**
   * Update advertiser limit settings.
   *
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  saveSettingLimitsAdvertiser (data) {
    return axios.post(route('api.advertisers.setting.store', {advertiser: data.advertiserId} ), data.data);
  },

  /**
   * Get settings advertiser.
   *
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  getSettingsAdvertiser (data) {
    return axios.get(route('api.advertisers.setting.show',  {advertiser: data.advertiserId} ));
  },
};
