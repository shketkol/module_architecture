import {mapActions} from "vuex";

export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Map actions
     */
    ...mapActions('show', ['changeAccountType']),

    /**
     * Update account type event.
     * @param value
     */
    async updateAccountType (value) {
      const accountTypeId = value ? this.advertiser.accountTypes.special : this.advertiser.accountTypes.common;
      await this.$call(this.changeAccountType({
        advertiserId: this.advertiser.id,
        data: {
          account_type_id: accountTypeId
        }
      }));

      this.visibleModal = false;
    }
  }
}
