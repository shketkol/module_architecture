import ReportIndex from 'Modules/report/components/index/ReportIndex';
import ReportForm from '../../components/forms/ReportForm';

export default [
  {
    path: '/reports/create',
    name: 'reports.create',
    components: {
      default: ReportIndex,
      details: ReportForm,
    }
  },
  {
    path: '/reports/:type/:reportId',
    name: 'reports.show',
    components: {
      default: ReportIndex,
      details: ReportForm,
    }
  },
  {
    path: '/reports',
    name: 'reports.index',
    component: ReportIndex,
  }
];
