import deliveryFrequency from './modules/delivery-frequency';
import targetings from './modules/targetings';
import report from './modules/report'
import checkStatuses from 'Foundation/components/dataTable/store/check-statuses'
import notifications from 'Modules/notification/store/modules/notifications';
import search from 'Modules/advertiser/store/modules/search';
import layoutReport from 'Foundation/layouts/single-page/store/modules/report';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    deliveryFrequency,
    report,
    notifications,
    search,
    checkStatuses,
    targetings,
    layoutReport
  },
});

export default store;
