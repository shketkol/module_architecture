import storeHelper from 'Foundation/helpers/store';
import { targetings as api } from '../../api';
import {getField} from 'vuex-map-fields';

/**
 * Types
 */
const types = {
  INDEX_TARGETINGS: storeHelper.createAsyncMutation('INDEX_TARGETINGS'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Targetings list data.
   */
  targetingsList: [],
};

const getters = {
  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Get targetinds list
   *
   * @param state, orderId
   */
  list: storeHelper.createAction(types.INDEX_TARGETINGS, {
    call () {
      return api.index();
    },
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Targetinds list mutation
   *
   * @param state, orderId
   */
  ...storeHelper.createMutation(types.INDEX_TARGETINGS, {
    success(state, { data }) {
      state.targetingsList = data;
    },
  }),
};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


