import cloneDeep from 'lodash/cloneDeep';
import storeHelper from 'Foundation/helpers/store';
import { getField } from 'vuex-map-fields';
import { report as api } from '../../api';
import translator from 'Foundation/services/translations/locale';
import moment from 'moment';
import { camelCase, startCase, replace, set } from 'lodash';

/**
 * Types
 */
const types = {
  STORE_REPORT: storeHelper.createAsyncMutation('STORE_REPORT'),
  UPDATE_REPORT: storeHelper.createAsyncMutation('UPDATE_REPORT'),
  DELETE_REPORT: storeHelper.createAsyncMutation('DELETE_REPORT'),
  SHOW_SCHEDULED_REPORT: storeHelper.createAsyncMutation('SHOW_SCHEDULED_REPORT'),
  SHOW_DOWNLOAD_REPORT: storeHelper.createAsyncMutation('SHOW_DOWNLOAD_REPORT'),
  SHOW_ADVERTISERS_REPORT: storeHelper.createAsyncMutation('SHOW_ADVERTISERS_REPORT'),
  SHOW_PENDING_CAMPAIGNS_REPORT: storeHelper.createAsyncMutation('SHOW_PENDING_CAMPAIGNS_REPORT'),
  SHOW_MISSING_ADS_REPORT: storeHelper.createAsyncMutation('SHOW_MISSING_ADS_REPORT'),
  SHOW_CAMPAIGN_DETAILED_REPORT: storeHelper.createAsyncMutation('SHOW_CAMPAIGN_DETAILED_REPORT'),
  SHOW_CAMPAIGN_SUMMARY_REPORT: storeHelper.createAsyncMutation('SHOW_CAMPAIGN_SUMMARY_REPORT'),
};

/**
 * Default delivery params
 */
const defaultDeliveryParams = {
  type: null,
  schedulePrams: {
    frequency: null,
    day: null,
    emails: [window.userConfig.email]
  }
};

/**
 * Default campaigns summary report
 */
const defaultCampaignSummaryReport = {
  name: null,
  campaign: null,
  delivery: cloneDeep(defaultDeliveryParams),
};

/**
 * Default campaigns detailed report
 */
const defaultCampaignDetailedReport = {
  name: null,
  campaigns: [],
  targetings: [],
  dateRange: {
    start: null,
    end: null,
  },
  delivery: cloneDeep(defaultDeliveryParams)
};

/**
 * Default scheduled report
 */
const defaultScheduledReport = {
  name: null,
  advertiser: defaultAdvertiser,
  delivery: cloneDeep(defaultDeliveryParams)
}

/**
 * Default download report
 */
const defaultDownloadReport = {
  name: null,
  dateRange: {
    start: null,
    end: null,
  },
  advertisers: defaultAdvertiser,
};

/**
 * Default advertisers report
 */
const defaultAdvertisersReport = {
  name: null
};

/**
 * Default pending campaigns report
 */
const defaultPendingCampaignsReport = {
  name: translator.get('report::labels.pending_campaigns'),
  dateRange: {
    start: moment(window.config.general.launch_date, 'YYYY-MM-DD').endOf('day').format(),
    end: moment().endOf('day').format()
  },
  delivery: cloneDeep(defaultDeliveryParams)
};

/**
 * Default missing ads report
 */
const defaultMissingAdsReport = {
  name: null,
  delivery: cloneDeep(defaultDeliveryParams)
};

/**
 * Default advertiser option
 * @type {{}}
 */
const defaultAdvertiser = {
  label: translator.get('report::labels.all_advertisers'),
  value: null,
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Async mutation state.
   */
  ...storeHelper.createMutationState([
    types.STORE_REPORT,
    types.SHOW_SCHEDULED_REPORT,
    types.SHOW_DOWNLOAD_REPORT,
    types.SHOW_ADVERTISERS_REPORT,
    types.SHOW_PENDING_CAMPAIGNS_REPORT,
    types.SHOW_MISSING_ADS_REPORT,
    types.UPDATE_REPORT,
    types.DELETE_REPORT,
    types.SHOW_CAMPAIGN_DETAILED_REPORT,
    types.SHOW_CAMPAIGN_SUMMARY_REPORT,
  ]),

  /**
   * Advertiser reports.
   */
  advertiserReports: {
    summary: cloneDeep(defaultCampaignSummaryReport),
    detailed: cloneDeep(defaultCampaignDetailedReport),
  },

  /**
   * Advertiser reports.
   */
  adminReports: {
    scheduled: cloneDeep(defaultScheduledReport),
    download: cloneDeep(defaultDownloadReport),
    advertisers: cloneDeep(defaultAdvertisersReport),
    pendingCampaigns: cloneDeep(defaultPendingCampaignsReport),
    missingAds: cloneDeep(defaultMissingAdsReport),
  },
};

const getters = {
  /**
   * @param state
   * @returns {boolean}
   */
  isPendingState (state) {
    return state.storeReportPending ||
      state.updateReportPending ||
      state.showScheduleReportPending ||
      state.showDownloadReportPending ||
      state.showAdvertisersReportPending ||
      state.showPendingCampaignsReportPending ||
      state.showMissingAdsReportPending ||
      state.showCampaignDetailedReportPending ||
      state.showCampaignSummaryReportPending;
  },

  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
let actions = {
  /**
   * Delete the report.
   */
  deleteReport: storeHelper.createAction(types.DELETE_REPORT, {
    call: api.destroy,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
let mutations = {
  /**
   * Store report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.STORE_REPORT),

  /**
   * Update report mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.UPDATE_REPORT),

  /**
   * Delete the report.
   */
  ...storeHelper.createMutation(types.DELETE_REPORT),
};

/**
 * Mapper report types.
 */
const reportTypes = {
  campaign_summary: 'advertiserReports.summary',
  campaign_detailed: 'advertiserReports.detailed',
  scheduled: 'adminReports.scheduled',
  download: 'adminReports.download',
  advertisers: 'adminReports.advertisers',
  pending_campaigns: 'adminReports.pendingCampaigns',
  missing_ads: 'adminReports.missingAds',
}

for (let item in reportTypes) {
  let actionName = `store${replace(startCase(camelCase(item)), ' ', '')}Report`;
  /**
   * Store report
   */
  actions[actionName] = storeHelper.createAction(types.STORE_REPORT, {
    call: api[actionName],
  });

  /**
   * Show report
   */
  actionName = `show${replace(startCase(camelCase(item)), ' ', '')}Report`;
  actions[actionName] = storeHelper.createAction(types[`SHOW_${item.toUpperCase()}_REPORT`], {
    call: api[actionName],
  });

  /**
   * Update report
   */
  actionName = `update${replace(startCase(camelCase(item)), ' ', '')}Report`;
  actions[actionName] = storeHelper.createAction(types.UPDATE_REPORT, {
    call: api[actionName],
  });

  mutations = {...mutations, ...storeHelper.createMutation(types[`SHOW_${item.toUpperCase()}_REPORT`], {
      success(state, { data }) {
        set(state, reportTypes[item], data);
      }
  })};

  /**
   * Clear advertisers report data
   *
   * @param state
   */
  mutations[`clear${replace(startCase(camelCase(item)), ' ', '')}Report`] = (state) => {
    set(state, reportTypes[item], cloneDeep(eval(`default${replace(startCase(camelCase(item)), ' ', '')}Report`)));
  };
}

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
  getters,
};


