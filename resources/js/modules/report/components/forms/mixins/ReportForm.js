import moment from 'moment';
import { mapMutations } from 'vuex';
import { mapFields } from 'vuex-map-fields';

export default {
  /**
   * Data properties.
   */
  data() {
    return {
      currentId: null
    }
  },

  /**
   * Computed properties
   */
  computed: {
    /**
     * Map fields
     */
    // TODO Change this solution to the event pattern
    ...mapFields('layoutReport', ['reloadTable']),

    /**
     * Picker options for datepicker.
     *
     * @returns {{disabledDate: (default.methods.checkDateIsDisabled|(function(*=): boolean))}}
     */
    pickerOptions () {
      return {
        disabledDate: this.checkDateIsDisabled,
      };
    },

    /**
     * Validation rules.
     */
    validation () {
      return {
        complete_date_range: null,
        ...this.validationRules.report.date,
      };
    },
  },

  /**
   * Created hook
   */
  async created () {
    if (this.currentId) {
      await this.show(this.currentId);
    }
  },

  /**
   * Before destroy hook
   */
  beforeDestroy () {
    this.clear();
    this.$validator.errors.clear(this.scope);
  },

  /**
   * Component methods
   */
  methods: {
    /**
     * Map mutations
     */
    // TODO Change this solution to the event pattern
    ...mapMutations('layoutReport', { reloadTableMutation: 'reloadTable' }),

    /**
     * Submit form.
     */
    async submit () {
      const validated = await this.validate();

      if (!validated) {
        return;
      }

      this.$call(
        this.save(),
        { scope: this.scope, muteException: true }
      )
        .then(async () => {
          if (!this.currentId) {
            this.$trackAction('create_report');
          }
          // TODO Change this solution to the event pattern
          await this.reloadTableMutation();
          this.reloadTable = false;
        })
    },

    /**
     * Save api call
     */
    save () {
      if (this.currentId) {
        return this.update({
          id: this.currentId,
          data: this.formData,
        });
      }

      return this.store(this.formData);
    },

    /**
     * Returns true if date should be available to select in datepicker.
     * @param date
     * @returns {boolean}
     */
    checkDateIsDisabled (date) {
      const momentDate = moment(date);
      return momentDate.isAfter(moment());
    },

    /**
     * Set date range.
     * @param value
     */
    setDateRange (value) {
      this.formData.dateRange = value;
    },

    /**
     * Validate form.
     *
     * @returns {*}
     */
    validate () {
      return this.$validateScope(this.scope);
    },

    /**
     * Set delivery data.
     */
    setDeliveryData (value) {
      if (this.formData.delivery) {
        this.formData.delivery = value;
      }
    },
  },
}
