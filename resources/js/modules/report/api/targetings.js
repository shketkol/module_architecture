import axios from 'axios';
import route from 'Foundation/services/routing/route';

export default {
  /**
   * Get list of all available targetings options.
   *
   * @returns {AxiosPromise<any>}
   */
  index () {
    return axios.get(
      route('api.reports.reportTargetingTypes.index')
    );
  },
};
