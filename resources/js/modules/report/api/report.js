import axios from 'axios';
import route from 'Foundation/services/routing/route';
import { camelCase, replace, startCase } from 'lodash';

const actions = {
  /**
   * Pause the report.
   *
   * @param report
   * @returns {AxiosPromise<any>}
   */
  pause (report) {
    return axios.patch(route('api.reports.pause', { report }));
  },

  /**
   * Resume the report.
   *
   * @param report
   * @returns {AxiosPromise<any>}
   */
  resume (report) {
    return axios.patch(route('api.reports.resume', { report }));
  },

  /**
   * Delete the specified resource.
   *
   * @param report
   * @returns {AxiosPromise}
   */
  destroy (report) {
    return axios.delete(route('api.reports.destroy', { report }));
  },
}

/**
 * Mapper report types.
 */
const reportTypes = [
  'campaign_summary',
  'campaign_detailed',
  'scheduled',
  'download',
  'advertisers',
  'pending_campaigns',
  'missing_ads',
]

reportTypes.forEach((item) => {
  let actionName = `store${replace(startCase(camelCase(item)), ' ', '')}Report`;

  actions[actionName] = (data) => {
    return axios.post(
      route(`api.reports.${replace(item, '_', '-')}.store`),
      data
    );
  }

  actionName = `show${replace(startCase(camelCase(item)), ' ', '')}Report`;
  actions[actionName] = (id) => {
    return axios.get(route(`api.reports.${replace(item, '_', '-')}.show`, {report: id}));
  }

  actionName = `update${replace(startCase(camelCase(item)), ' ', '')}Report`;
  actions[actionName] = (data) => {
    return axios.patch(
      route(`api.reports.${replace(item, '_', '-')}.update`, {report: data.id}),
      data.data
    );
  }
})

export default actions;
