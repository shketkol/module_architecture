import { formatToApi } from 'Foundation/helpers/moment';

export default class ReportMapper {
  /**
   * Returns object that should be sent to API.
   *
   * @param report
   * @returns {{}}
   */
  static mapObjectToApi (report) {
    return Object.assign({}, report, {
      dateRange: {
        start: formatToApi(report.dateRange && report.dateRange.start),
        end: formatToApi(report.dateRange && report.dateRange.end),
      },
    });
  }
}
