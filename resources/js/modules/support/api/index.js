import axios from 'axios';
import route from './../../../foundation/services/routing/route';

export default {
  /**
   * Send contact us email.
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  contact (data) {
    return axios.post(route('api.support.contact'), data);
  },
};
