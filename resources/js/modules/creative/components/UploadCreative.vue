<template>
  <div class="upload-creative">
    <modal-creative-upload-limit
      :visible.sync="showCreativesLimitModal"
      :message="uploadCreativePolicy.reason"
    />
    <upload-area
      v-show="!showUploadProgress"
      :disabled="isCreativeProcessing"
      @change="initiateUploadProcess"
    />
    <upload-progress
      ref="uploader"
      :title="progressTitle"
      :visible="showUploadProgress"
      :modal-mode="modalMode"
      @abort="abortProcess"
      @pause="pauseProcess"
      @resume="resumeProcess"
    />
    <errors-modal
      :visible="isErrorsVisible"
      :validation-errors="formErrors"
      @proceed-upload="proceedUpload"
      @close="clearAllErrors"
    />
  </div>
</template>

<script>
  import { flatMap, throttle } from 'lodash';
  import { mapFields } from 'vuex-map-fields';
  import { mapActions, mapState, mapMutations, mapGetters } from 'vuex';
  import { formatMediaInfo } from '../helpers/media-info-formatter';
  import UploadProgress from './UploadProgress';
  import Errors from './modals/Errors';
  import Validation from '../mixins/Validation';
  import Loading from 'Foundation/mixins/Loading';
  import FileHash from 'Foundation/mixins/File/FileHash';
  import MediaInfo from 'Foundation/mixins/File/MediaInfo';
  import ModalCreativeUploadLimit from 'Modules/advertiser/components/common/modals/ModalCreativeUploadLimit';
  import UploadArea from './UploadArea';
  import {
    UPLOAD_PHASE_CLEAR,
    UPLOAD_PHASE_VALIDATION,
    UPLOAD_PHASE_UPLOAD,
    UI_STATE_RELEASED,
    UI_STATE_PAUSED,
    UI_STATE_ABORTED
  } from 'Modules/creative/constants';

  /**
   * Minimal interval between progress bar updates (in milliseconds).
   */
  const MIN_INTERVAL_BETWEEN_PROGRESS_UPDATES = 200;

  export default {
    /**
     * Component name
     */
    name: 'UploadCreative',

    /**
     * Used components
     */
    components: {
      modalCreativeUploadLimit: ModalCreativeUploadLimit,
      uploadArea: UploadArea,
      uploadProgress: UploadProgress,
      errorsModal: Errors
    },

    /**
     * Mixins
     */
    mixins: [
      Validation,
      Loading,
      FileHash,
      MediaInfo,
    ],

    /**
     * Component properties.
     */
    props: {
      /**
       * Campaign id
       */
      campaignId: {
        type: Number,
        default: null,
      },

      /**
       * Identify id "upload-creative" is used within modal
       */
      modalMode: {
        type: Boolean,
        default: false,
      },
    },

    /**
     * Data values
     */
    data() {
      return {
        retries: 0,
        showCreativesLimitModal: false,
        progressPaused: false,
        validated: false,
        mediaInfoData: {},
        readingMediaInfo: false,
        skipDuplicateCheck: false,
      }
    },

    /**
     * Computed properties
     */
    computed: {
      /**
       * Fields mapping
       */
      ...mapFields('creative', {
        file: 'file',
        data: 'file.data',
        name: 'file.name',
        file_path: 'file.file_path',
        extension: 'file.extension',
        file_size: 'file.fileSize',
        duration: 'file.duration',
        width: 'file.width',
        height: 'file.height',
        hashSum: 'file.hashSum',
      }),

      /**
       * Fields mapping
       */
      ...mapFields('creative/evaporate', {
        uploadStats: 'uploadStats',
        bucket: 'config.s3Bucket',
        maxRetries: 'config.maxUploadRetries'
      }),

      /**
       * Creative State mapping
       */
      ...mapState('creative', [
        'uploadCreativePending',
        'uploadPhase',
        'uiState',
      ]),

      /**
       * Upload State mapping
       */
      ...mapState('creative/evaporate', [
        'getUploadConfigurationPending',
      ]),

      /**
       * Permissions State mapping
       */
      ...mapState('permissions', {
        uploadCreativePolicy: state => state.permissions.uploadCreative,
      }),

      /**
       * Vuex (getters).
       */
      ...mapGetters('creative', [
        'isCreativeProcessing',
      ]),

      /**
       * Check if upload bar should be shown.
       * @returns {boolean}
       */
      showUploadProgress () {
        return this.uploadStats.visible || this.uploadPhase === UPLOAD_PHASE_VALIDATION;
      },

      /**
       * Get title of the progress modal box.
       */
      progressTitle () {
        switch (this.uploadPhase) {
          case UPLOAD_PHASE_VALIDATION:
            return this.$trans('creative::labels.validating');
          case UPLOAD_PHASE_UPLOAD:
            return this.$trans('creative::labels.uploading');
          default:
            return '';
        }
      },

      /**
       * Show loader on progress modal during loading of mediaInfo module and upload-configuration.
       * @returns {boolean}
       */
      processingValidation () {
        return this.readingMediaInfo || this.getUploadConfigurationPending;
      },

      /**
       * @returns {boolean}
       */
      isUploadProcessing () {
        return this.uploadPhase !== UPLOAD_PHASE_CLEAR;
      },

      /**
       * @returns {boolean}
       */
      isErrorsVisible () {
        return this.errorsAny && this.uiState === UI_STATE_RELEASED;
      },
    },

    /**
     * Watchers
     */
    watch: {
      /**
       * Fired when validation is failed.
       */
      errorsAny (val) {
        if (val) {
          this.trackErrors();
        }
      },

      'uploadCreativePolicy.can': function (val) {
        this.showCreativesLimitModal = !val;
      },
    },

    /**
     * Component methods
     */
    methods: {
      /**
       * Map creative actions
       */
      ...mapActions('creative', [
        'store',
        'storeRejected',
      ]),

      ...mapActions('creative/evaporate', {
        startUpload: 'start',
        abortUpload: 'abort',
        pauseUpload: 'pause',
        resumeUpload: 'resume',
        handleError: 'handleError',
        getUploadConfiguration: 'getUploadConfiguration',
      }),

      /**
       * Map campaign actions
       */
      ...mapActions('permissions', ['checkUploadCreative']),

      /**
       * Map mutations
       */
      ...mapMutations('creative', [
        'clearFileData',
        'setUploadPhase',
        'setUiState',
      ]),

      /**
       * Map mutations
       */
      ...mapMutations('creative/evaporate', [
        'clearUploadStats',
      ]),

      /**
       * Initialize upload process.
       *
       * Check if user has permission to upload creative
       * Calculate hashSum based on file-contents.
       * Pre-validate basic file params.
       * Validate meta-tags params when file metadata loaded.
       * Initialize and start s3Upload.
       *
       * @returns {(boolean|undefined)}
       */
      async initiateUploadProcess(e) {
        this.resetState();

        this.validated = await this.initiateValidationPhase(e.raw);

        if (!this.validated) {
          this.resetUploadPhase();
          return;
        }

        if (this.canUpload()) {
          await this.initiateUploadPhase();
        }
      },

      /**
       * Check if upload phase can be started.
       * @returns {boolean}
       */
      canUpload () {
        return this.uiState === UI_STATE_RELEASED && this.validated;
      },

      /**
       * Validates the creative file.
       * @returns {boolean}
       */
      async initiateValidationPhase (file) {
        this.setUploadPhase(UPLOAD_PHASE_VALIDATION);

        if (file === undefined) {
          return false;
        }

        // Check permissions for creatives upload
        await this.checkUploadCreative();
        if (!this.uploadCreativePolicy.can) {
          this.showCreativesLimitModal = true;
          return false;
        }

        try {
          this.hashSum = await this.getFileHash(file, this.updateProgress);
        } catch (error) {
          this.$log('An error occurred during generating file hash of the creative file', error);
          return false;
        }

        // Fill basic file data.
        this.fillFile(file);
        try {
          await this.setMediaInfo(file);
        } catch (error) {
          this.$log('An error occurred during processing Creative file', error);
        }

        // Validate creative.
        await this.verify();
        if (this.errorsAny) {
          await this.handleRejectedCreative();
          return false;
        }

        try {
          await this.setUploadConfiguration(this.hashSum);
        } catch (e) {
          return false;
        }

        return true;
      },

      /**
       * Initiate upload phase.
       */
      async initiateUploadPhase () {
        this.setUploadPhase(UPLOAD_PHASE_UPLOAD);
        this.resetProgress();
        this.$trackAction('ad_upload');
        this.retries = 0;

        //get upload configuration any way in case if file is force-uploaded
        if (this.skipDuplicateCheck) {
          try {
            await this.setUploadConfiguration();
          } catch (e) {
            return;
          }
        }
        this.upload();
      },


      /**
       * Set upload configuration
       */
      setUploadConfiguration (fileHash = null) {
        return this.$call(
          this.getUploadConfiguration({ hashSum: fileHash }),
          { notify: false, scope: this.scope }
        ).catch((e) => {
          this.resetUploadPhase();
          throw e;
        });
      },

      /**
       * Set media-info data.
       * @param {File} file
       * @returns {Promise}
       */
      setMediaInfo (file) {
        return new Promise((resolve, reject) => {
          this.readingMediaInfo = true;
          this.getMediaInfo(file)
            .then(mediaInfo => {
              this.mediaInfoData = formatMediaInfo(mediaInfo, this.file);
              this.width = this.mediaInfoData.video.Width;
              this.height = this.mediaInfoData.video.Height;
              this.duration = this.mediaInfoData.video.Duration;
              resolve();
            })
            .catch(error => {
              reject(error);
            })
            .finally(() => {
              this.readingMediaInfo = false;
            })
          ;
        });
      },

      /**
       * Initialize s3 multipart upload process.
       */
      upload () {
        return this.startUpload().then(
           (awsS3ObjectKey) => {
             this.storePlatformEntity(awsS3ObjectKey)
          },
          () => {
            //retry if token expired or unrecoverable upload error occurred.
            if (this.retries < this.maxRetries && !this.uploadStats.cancelled) {
              this.pauseUpload();
              this.upload();
              this.retries++;
            } else {
              this.skipDuplicateCheck = false;
            }
          }
        ).then().finally(() => this.resetState());
      },

      /**
       * Store uploaded video as platform entity
       */
      storePlatformEntity (awsObjectKey) {
        this.store({
          campaignId: this.campaignId,
          s3FileKey: awsObjectKey
        }).then((r) => {
          this.$notify.success({
            message: this.$trans('creative::messages.success_title')
          });

          this.$emit('finish', r.data.data);

        }).catch((e) => {
          this.$notify.error({
            message: this.$trans('creative::messages.validation_error')
          });
          this.handleBeErrors(e.data.errors)
        }).finally(() => {
          localStorage.removeItem('awsUploads');
          this.skipDuplicateCheck = false;
        });
      },

      /**
       * Process and prevalidation for uploaded file
       */
      processFile (file) {
        return new Promise((resolve, reject) => {
          let reader = document.createElement('video');
          reader.onloadedmetadata = async () => {
            this.fillReader(reader);
            resolve();
          };
          reader.onerror = reject;
          reader.src = URL.createObjectURL(file);
        })
      },

      /**
       * Proceed upload with ignoring duplicate check
       */
      async proceedUpload () {
        this.resetUploadPhase();
        this.setUiState(UI_STATE_RELEASED);
        this.validated = true;
        this.skipDuplicateCheck = true;
        await this.initiateUploadPhase();
      },

      /**
       * Clear process variables on new upload
       */
      resetState() {
        this.resetUploadPhase();
        this.setUiState(UI_STATE_RELEASED);
        this.validated = false;
        this.skipDuplicateCheck = false;
        this.mediaInfoData = {};
        this.clearUploadStats();
        this.clearFileData();
        this.clearAllErrors();
      },

      /**
       * Fill and validate vuex file instance before reading metadata
       */
      fillFile(file) {
        this.name = file.name;
        this.data = file;
        this.extension = this.formExtension(file.name);
        this.file_path = file.file_path;
        this.file_size = file.size;
      },

      /**
       * Fill and validate vuex file instance after reading metadata
       */
      fillReader(reader) {
        this.duration = reader.duration;
        this.width = reader.videoWidth;
        this.height = reader.videoHeight;
      },

      /**
       * @return {string} file extension
       */
      formExtension(name) {
        const parts = name.split('.');
        return parts[parts.length - 1].toLowerCase();
      },

      /**
       * Handle rejected creative (check possibility to save errors on BE)
       */
      async handleRejectedCreative () {
        try {
          if (
            this.file.extension &&
            this.file.duration &&
            this.file.width &&
            this.file.height &&
            this.file.fileSize &&
            this.file.hashSum
          ) {
            return await this.$call(this.storeRejected({
              'file': this.file,
              'rawData': this.mediaInfoData,
              'errors': this.formErrors,
              campaign_id: this.campaignId
            }), { notify: false })
              .then(() => {
                this.$emit('rejected');
                this.resetUploadPhase();
              });
          } else {
            this.$logDebug('Rejected creative cannot be stored in DB', this.mediaInfoData);
          }
        } catch (error) {
          return error;
        }
      },

      /**
       * Track validation errors.
       */
      trackErrors () {
        this.$trackError('Creative wasn\'t uploaded', flatMap(this.formErrors));
      },

      /**
       * Reset progress bar state.
       */
      resetProgress () {
        if (this.$refs.uploader) {
          this.$refs.uploader.resetProgress(this.uploadStats);
        }
      },

      /**
       * Reset state of upload phase
       */
      resetUploadPhase () {
        this.setUploadPhase(UPLOAD_PHASE_CLEAR);
      },

      /**
       * Update progress bar state.
       * @param {number} percent
       */
      updateProgress: throttle(function (percent) {
        this.uploadStats.percent = percent;
      }, MIN_INTERVAL_BETWEEN_PROGRESS_UPDATES),

      /**
       * Abort upload process from UI.
       */
      abortProcess () {
        if (this.uploadPhase === UPLOAD_PHASE_UPLOAD) {
          this.abortUpload();
        }
        this.setUiState(UI_STATE_ABORTED);
        this.resetUploadPhase();
      },

      /**
       * Pause upload process during user confirmation.
       */
      pauseProcess () {
        this.setUiState(UI_STATE_PAUSED);
        if (this.uploadPhase === UPLOAD_PHASE_UPLOAD) {
          this.pauseUpload();
        }
      },

      /**
       * Resume upload process after user denial of aborting process.
       */
      resumeProcess () {
        this.setUiState(UI_STATE_RELEASED);
        this.progressPaused = false;

        if (
          this.uploadPhase === UPLOAD_PHASE_VALIDATION &&
          this.validated
        ) {
          this.initiateUploadPhase();
          return;
        }

        if (this.uploadPhase === UPLOAD_PHASE_UPLOAD) {
          this.resumeUpload();
        }
      },
    },
  }
</script>
