import 'Foundation/bootstrap';
import 'Foundation/services/plyr';
import TheCreativesIndex from './pages/TheCreativesIndex';
import { getRouter } from 'Foundation/services/routing/factory';
import routes from './router/routes';
import Vue from 'vue';
import store from './store';

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    creativesIndex: TheCreativesIndex,
  },

  /**
   * Store
   */
  store,

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),
}));
