/**
 * Upload process phases states.
 */
export const UPLOAD_PHASE_CLEAR = 'clear';
export const UPLOAD_PHASE_VALIDATION = 'validation';
export const UPLOAD_PHASE_UPLOAD = 'upload';

/**
 * Upload process UI states.
 */
export const UI_STATE_RELEASED = 'released';
export const UI_STATE_PAUSED = 'paused';
export const UI_STATE_ABORTED = 'aborted';
