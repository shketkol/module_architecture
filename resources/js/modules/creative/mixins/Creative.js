import filesizeConverter from 'filesize';
import { mapState } from 'vuex';

export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * Creative video details
     */
    videoData: {
      type: Object,
      default () {
        return null;
      }
    },

    /**
     * Creative video details
     */
    isPreviewMode: {
      type: Boolean,
      default: false
    }
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Map state
     */
    ...mapState('creative', [
      'permissions',
      'showCreativeData',
    ]),

    /**
     * Indicate component source of data (internal - used for Data Tables, storage - for single preview mode)
     */
    dataSource () {
      if (this.localData) {
        return this.localData;
      }
      return this.isPreviewMode ? this.showCreativeData : this.videoData;
    },

    /**
     * Computed id
     */
    id () {
      if (this.dataSource !== undefined) {
        return this.creativeData.id;
      }
      return null;
    },

    /**
     * Form url for video preview
     */
    preview () {
      let preview = '#';

      if (!this.dataSource) {
        return preview;
      }

      const previewUrl = this.dataSource.preview_url;

      if (previewUrl !== undefined && previewUrl) {
        preview = previewUrl;
      }

      return preview;
    },

    /**
     * Computed poster url
     */
    poster () {
      let poster = '/images/content/creative-img.png';
      if (this.dataSource &&
        this.dataSource.poster_key !== undefined &&
        this.dataSource.poster_key) {
        poster = this.dataSource.poster_key;
      }

      return poster;
    },

    /**
     * Format creative video filesize
     */
    filesize () {
      let filesize = '';
      if (this.dataSource && this.dataSource.file_size !== undefined) {
        filesize = filesizeConverter(this.dataSource.file_size);
      }
      return filesize;
    },

    /**
     * Computed duration
     */
    duration () {
      let timeString = '00:00:00:000';
      if (this.dataSource && this.dataSource.duration !== undefined) {
        let date = new Date(null);
        let time = this.dataSource.duration.toString().split('.');

        const seconds = time.shift();
        const milliseconds = time.pop() || null;

        date.setSeconds(seconds, milliseconds);

        timeString = date.toISOString().substr(11, 12).replace('.', ':');
      }

      return timeString;
    },

    /**
     * Computed name
     */
    name () {
      let name = '';

      if (this.dataSource) {
        name = this.dataSource.name;
      }
      return name;
    },

    /**
     * Computed extension
     */
    extension () {
      let extension = '';
      if (this.dataSource) {
        extension = this.dataSource.extension;
      }
      return extension;
    },

    /**
     * Computed status
     */
    status () {
      let status = '';
      if (this.dataSource) {
        status = this.dataSource.creative_status;
      }
      return status;
    },

    /**
     * isProRes indicator
     */
    isProRes () {
      if (!this.dataSource) {
        return false;
      }
      return this.dataSource.is_pro_res;
    },

    /**
     * Upload date
     */
    uploaded () {
      let date = '';
      if (this.dataSource) {
        date = this.$formatDate(
          this.dataSource.upload_date,
          { clientTimezone: true, useTimezone: false }
          );
      }
      return date;
    },

    /**
     * Check if creative can be added to campaign
     */
    checkStatus () {
      return this.dataSource.creative_status === 'rejected';
    },

    /**
     * Check if creative can be deleted
     * @returns {boolean}
     */
    canBeDeleted () {
      return this.permissions.canDelete;
    },

    /**
     * Allow to download creative
     */
    allowDownloadCreative () {
      return this.$user.isAdmin && this.status !== 'rejected';
    },
  },
}
