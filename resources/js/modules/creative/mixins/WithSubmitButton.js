import { get } from 'lodash';
import { mapActions } from "vuex";

export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * Show data table for chosen user
     */
    withSubmitButton: {
      type: Boolean,
      default: false,
    },
  },

  /**
   * Reactive Data.
   */
  data () {
    return {
      submitButtonLoading: false,
      localData: null,
    };
  },

  /**
   * Computed props.
   */
  computed: {
    submitPermissions () {
      return {
        can: get(this.dataSource, 'permissions.canSubmit.can'),
        reason: get(this.dataSource, 'permissions.canSubmit.reason'),
      };
    },

    /**
     * Is "Submit creative" button is visible
     * @returns {boolean}
     */
    submitButtonVisible () {
      return this.withSubmitButton &&
        this.status === 'draft';
    },

    /**
     * Is "Submit creative" button in disabled state.
     * @returns {boolean}
     */
    submitButtonDisabled () {
      return !this.submitPermissions.can || this.submitButtonLoading;
    },
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Vuex actions.
     */
    ...mapActions('creative', [
      'submitCreative'
    ]),

    /**
     * Submit creative for review
     */
    submit () {
      this.submitButtonLoading = true;
      this.$call(this.submitCreative(this.videoData.id))
        .then(({data: {data: {creative}}}) => {
          this.localData = creative;
        })
        .catch((error) => {
          this.handleSubmitError(error);
        })
        .finally(() => {
          this.submitButtonLoading = false;
        })
      ;
    },

    /**
     * Handle errors during submit
     * @param {*} error
     */
    handleSubmitError (error) {
      // Other statuses are handled by api-service.
      if (error.status !== 403) {
        return;
      }

      const message = get(error, 'data.message', this.$trans('user::messages.operation_prohibited'));
      this.$notify.error({
        message: message,
      });

      const creative = get(error, 'data.creative');
      if (creative) {
        this.localData = creative;
      }
    },
  },
}
