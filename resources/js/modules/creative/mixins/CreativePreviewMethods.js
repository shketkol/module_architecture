export default {
  /**
   * Component properties
   */
  props: {
    /**
     * If FE routing should be used
     */
    useRouting: {
      type: Boolean,
      default: false,
    },
  },

  /**
   * Component data
   */
  data () {
    return {
      includePreview: false,
      previewVisible: false,
      creativePreviewId: null,
    };
  },

  /**
   * Component watcher
   */
  watch: {
    /**
     * Add possibility to use history route in browser
     */
    $route (to) {
      if (!this.useRouting) {
        return;
      }

      if (!to.params.creative) {
        this.closePreview();
      }
    },

    /**
     * Show logic for the preview modal will create preview modal instance and then use native visible animation
     */
    includePreview (visible) {
      if (visible) {
        this.$nextTick(() => {
          this.previewVisible = true;
        });
      }
    },

    /**
     * Hide logic for the preview modal will hide preview modal and then destroy component
     */
    previewVisible (visible) {
      if (!visible) {
        //destroy component when hide animation is over;
        setTimeout(() => {
          this.includePreview = false;
          //Fix double scroll for triple modal
          if (this.showGalleryModal !== undefined && this.showGalleryModal) {
            this.$nextTick(() => {
              document.body.classList.add('el-popup-parent--hidden');
            });
          }
        }, 300)
      }
    },
  },

  /**
   * Component methods
   */
  methods: {
    /**
     * Show preview modal action
     */
    async showPreview (id) {
      this.creativePreviewId = +id;
      if (this.useRouting && this.$router.currentRoute.params.creative != id) {
        this.$pushRoute({ name: 'creatives.show', params: { creative: id } })
      }
      this.includePreview = true;
    },

    /**
     * Close preview modal action
     */
    closePreview () {
      this.previewVisible = false;
      if (this.useRouting) {
        this.$pushRoute({ name: 'creatives.index' })
      }
    },
  }
};
