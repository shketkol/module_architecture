import Vue from 'vue';
import Vuex from 'vuex';
import creative from './modules/creative';
import permissions from 'Modules/user/store/modules/permissions';
import notifications from 'Modules/notification/store/modules/notifications';

Vue.use(Vuex);

const store = new Vuex.Store({
  /**
   * Modules.
   */
  modules: {
    creative,
    notifications,
    permissions,
  },
});

export default store;
