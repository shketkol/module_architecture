import storeHelper from 'Foundation/helpers/store';
import {getField, updateField} from 'vuex-map-fields';
import { errors as api} from '../../api'

/**
 * Types
 */
const types = {
  LOAD_ERRORS: storeHelper.createAsyncMutation('LOAD_ERRORS'),
};

/**
 * State
 * @type {{storeUserPending: boolean, success: boolean}}
 */
const state = {
  /**
   * Detect if creative errors is loaded
   */
  [types.LOAD_ERRORS.loadingKey]: false,

  /**
   * Validation errors
   */
  validationErrors: null,

  /**
   * Validation success
   */
  success: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField
};

/**
 * Actions
 */
const actions = {
  /**
   * Load errors
   *
   * @param commit
   * @param payload
   * @returns {Promise<*|undefined>}
   */
  async load({commit}, payload) {
    commit(types.LOAD_ERRORS.PENDING);
    try {
      const response = await api.load(payload);
      commit(types.LOAD_ERRORS.SUCCESS, response.data);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.LOAD_ERRORS.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  }
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Load creative errors success
   * @param state
   */
  [types.LOAD_ERRORS.SUCCESS]: (state, payload) => {
    state.validationErrors = payload.errors;
    state.success = payload.success;
    state[types.LOAD_ERRORS.loadingKey] = false;
  },

  /**
   * Load creative errors pending
   * @param state
   */
  [types.LOAD_ERRORS.PENDING](state) {
    state[types.LOAD_ERRORS.loadingKey] = true;
  },

  /**
   * Load errors failure
   * @param state
   */
  [types.LOAD_ERRORS.FAILURE](state) {
    state[types.LOAD_ERRORS.loadingKey] = false;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};


