import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { creative as api } from '../../api';
import sparkMD5 from "spark-md5";
import sha256 from "js-sha256";
import Evaporate from "evaporate";

/**
 * Types
 */
const types = {
  UPLOAD_CREATIVE: storeHelper.createAsyncMutation('UPLOAD_CREATIVE'),
  GET_UPLOAD_CONFIGURATION: storeHelper.createAsyncMutation('GET_UPLOAD_CONFIGURATION'),
};

/**
 * State
 * @type {{storeUserPending: boolean, success: boolean}}
 */
const state = {
  /**
   * Create state for mutation.
   */
  ...storeHelper.createMutationState([
    types.GET_UPLOAD_CONFIGURATION,
  ]),

  /**
   * User permissions
   */
  permissions: {
    canDelete: false,
  },

  /**
   * Upload status
   */
  uploadStats: {
    visible: false,
    percent: 0,
    speed: '0',
    completedSize: 0,
    cancelled: false
  },

  /**
   * Uploader instance
   */
  uploader: null,

  /**
   * Creative upload config
   */
  config: null,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {

  uploadFilePath: (state, getters, rootState) => {
    return state.config.s3Bucket  + '/' +  rootState.creative.file.file_path + '.' + rootState.creative.file.extension;
  },

  uploadConfig: (state, getters, rootState) => {
    let config = {
      file: rootState.creative.file.data,
      name: rootState.creative.file.file_path + '.' + rootState.creative.file.extension,
      progress: (percent, stats) => {
        let humanPercent = Math.round(percent * 100);
        if (humanPercent < state.uploadStats.percent) {
          return;
        }
        state.uploadStats.percent = humanPercent;
        state.uploadStats.speed = stats.readableSpeed;
      },
      beforeSigner: (xhr, url) => {
        xhr.open('GET', url + '&publicKeyId=' + state.config.awsAccessKey);
      },
    };

    //add headers if IAM role is used
    if (state.config.awsSecurityKey) {
      config = {
        ...config,
        xAmzHeadersAtInitiate: {
          'x-amz-security-token': state.config.awsSecurityKey,
        },
        xAmzHeadersCommon: {
          'x-amz-security-token': state.config.awsSecurityKey,
        }
      }
    }

    return config;
  },

  /**
   * Fields mapping.
   */
  getField
};

/**
 * Actions
 */
const actions = {
  /**
   * Check availability to upload files.
   */
  getUploadConfiguration: storeHelper.createAction(types.GET_UPLOAD_CONFIGURATION, {
    async call (payload) {
      const response = await api.getUploadCredentials(payload.hashSum);
      return response.data;
    },
    after (state, data) {
      state.rootState.creative.file.file_path = data.uploadPath;
    },
  }),

  /**
   * Handle all errors
   */
  handleError({commit}, payload) {
    commit(types.UPLOAD_CREATIVE.FAILURE, payload);
  },

  /**
   * Create Evaporate Instance
   */
  async createEvaporateInstance ({ state }) {
    const clientTimeStamp = Math.floor(new Date().getTime() / 1000);
    let config = {
      signerUrl: state.config.authUrl,
      aws_key: state.config.awsAccessKey,
      bucket: state.config.s3Bucket,
      logging: state.config.logEnabled,
      cloudfront: true,
      computeContentMd5: true,
      awsRegion: state.config.awsDefaultRegion,
      s3FileCacheHoursAgo: state.config.cacheHours,
      onlyRetryForSameFileName: false,
      cryptoMd5Method: (d) => btoa(sparkMD5.ArrayBuffer.hash(d, true)),
      sendCanonicalRequestToSignerUrl: true,
      cryptoHexEncodedHash256: function (data) { return sha256(data, 'hex'); },
    };

    if (Math.abs(clientTimeStamp - state.config.serverTimestamp) > 1800) {
      config.timeUrl = state.config.timeUrl;
    }

    return Promise.resolve(Evaporate.create(config));
  },

  /**
   * Start S3 file upload
   */
  async start({commit, state, dispatch, getters}) {
    let config = getters.uploadConfig;

    config.error = (msg) => {
      commit(types.UPLOAD_CREATIVE.FAILURE, msg);
    };
    config.cancelled = (msg) => {
      state.uploadStats.cancelled = true;
      commit(types.UPLOAD_CREATIVE.FAILURE, msg);
    };
    config.complete = () => {
      commit(types.UPLOAD_CREATIVE.SUCCESS);
    };

    commit(types.UPLOAD_CREATIVE.PENDING);
    state.uploadStats.cancelled = false;

    state.uploader = await dispatch('createEvaporateInstance');

    return state.uploader.add(config)
  },

  /**
   * Abort S3 file upload
   */
  abort({commit, state, getters}) {
    if (state.uploader) {
      state.uploader.cancel(getters.uploadFilePath, {force: true});
    }
    commit(types.UPLOAD_CREATIVE.FAILURE);
  },

  /**
   * Pause S3 file upload
   */
  pause({state, getters}) {
    if (state.uploader) {
      state.uploader.pause(getters.uploadFilePath, {force: true});
    }
  },

  /**
   * Resume S3 file upload
   */
  resume({state, getters}) {
    if (state.uploader) {
      state.uploader.resume(getters.uploadFilePath);
    }
  },
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Create mutation for retrieve upload credentials.
   *
   */
  ...storeHelper.createMutation(types.GET_UPLOAD_CONFIGURATION, {
    success(state, data) {
      state.config = data;
    }
  }),

  /**
   * Upload crative success
   * @param state
   */
  [types.UPLOAD_CREATIVE.SUCCESS]: (state) => {
    state[types.UPLOAD_CREATIVE.loadingKey] = false;
    state.uploadStats.visible = false;
  },

  /**
   * Upload creative pending
   * @param state
   */
  [types.UPLOAD_CREATIVE.PENDING] (state) {
    state[types.UPLOAD_CREATIVE.loadingKey] = true;
    state.uploadStats.visible = true;
  },

  /**
   * Upload creative items failure
   * @param state
   */
  [types.UPLOAD_CREATIVE.FAILURE] (state) {
    state[types.UPLOAD_CREATIVE.loadingKey] = false;
    state.uploadStats.visible = false;
  },

  /**
   * Clear upload stats
   * @param state
   */
  clearUploadStats (state) {
    state.uploadStats = {
      visible: false,
      notified: false,
      cancelled: false,
      percent: 0,
      speed: '0',
      completedSize: 0
    };
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};


