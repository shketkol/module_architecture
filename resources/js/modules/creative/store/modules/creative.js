import storeHelper from 'Foundation/helpers/store';
import { getField, updateField } from 'vuex-map-fields';
import { creative as api } from '../../api';
import errors from './errors';
import evaporate from './evaporate';
import { UPLOAD_PHASE_CLEAR, UI_STATE_RELEASED } from 'Modules/creative/constants';

/**
 * Types
 */
const types = {
  STORE_CREATIVE: storeHelper.createAsyncMutation('STORE_CREATIVE'),
  SUBMIT_CREATIVE: storeHelper.createAsyncMutation('SUBMIT_CREATIVE'),
  SHOW_CREATIVE: storeHelper.createAsyncMutation('SHOW_CREATIVE'),
  ATTACH_CREATIVE: storeHelper.createAsyncMutation('ATTACH_CREATIVE'),
  DETACH_CREATIVE: storeHelper.createAsyncMutation('DETACH_CREATIVE'),
  REPLACE_CREATIVE: storeHelper.createAsyncMutation('REPLACE_CREATIVE'),
  DELETE_CREATIVE: storeHelper.createAsyncMutation('DELETE_CREATIVE'),
  STORE_REJECTED_CREATIVE: storeHelper.createAsyncMutation('STORE_REJECTED_CREATIVE'),
};

/**
 * Nested Modules
 */
const modules = {
  errors,
  evaporate
};

/**
 * State
 * @type {{storeUserPending: boolean, success: boolean}}
 */
const state = {
  /**
   * Create state for mutation.
   */
  ...storeHelper.createMutationState([
    types.REPLACE_CREATIVE,
    types.STORE_REJECTED_CREATIVE,
    types.SUBMIT_CREATIVE,
    types.STORE_CREATIVE,
  ]),

  /**
   * Store VideoFile status
   */
  success: false,

  /**
   * Detect if file is stored as Creative
   */
  [types.STORE_CREATIVE.loadingKey]: false,

  /**
   * Show creative data
   */
  [types.SHOW_CREATIVE.loadingKey]: false,

  /**
   * Delete creative
   */
  [types.DELETE_CREATIVE.loadingKey]: false,

  /**
   * Detect if file is attached fom campaign
   */
  [types.ATTACH_CREATIVE.loadingKey]: false,

  /**
   * Detect if file is detached fom campaign
   */
  [types.DETACH_CREATIVE.loadingKey]: false,

  /**
   * Detect if creative is deleted
   */
  [types.DETACH_CREATIVE.loadingKey]: false,

  /**
   * Video file instance
   */
  file: {
    data: {},
    name: null,
    file_path: null,
    extension: null,
    fileSize: null,
    duration: null,
    width: null,
    height: null,
    hashSum: null
  },

  /**
   * User permissions
   */
  showCreativeData: null,

  /**
   * User permissions
   */
  permissions: {
    canDelete: false,
  },

  uploadPhase: UPLOAD_PHASE_CLEAR,

  uiState: UI_STATE_RELEASED,
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Fields mapping.
   */
  getField,

  isCreativeProcessing: state =>
      state.attachCreativePending ||
      state.detachCreativePending ||
      state.replaceCreativePending ||
      state.storeCreativePending ||
      state.uploadPhase !== UPLOAD_PHASE_CLEAR,
};

/**
 * Actions
 */
const actions = {
  /**
   * Show creative details action
   * @param commit
   * @param payload
   * @returns {Promise<Promise<Promise<any>|Promise<never>|undefined>|Promise<AxiosResponse<any>>>}
   */
  async show({commit}, payload) {
    commit(types.SHOW_CREATIVE.PENDING);
    try {
      const response = await api.show(payload);
      commit(types.SHOW_CREATIVE.SUCCESS, response.data);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.SHOW_CREATIVE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Validate and store s3 link on BE
   */
  async store({commit, state}, data) {
    commit(types.STORE_CREATIVE.PENDING);
    try {
      data = {
        file: state.file,
        s3FileKey: data.s3FileKey,
        campaignId: data.campaignId
      };
      const response = await api.store(data);
      commit(types.STORE_CREATIVE.SUCCESS, response.data.data);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.STORE_CREATIVE.FAILURE, error);
      return Promise.reject(error.response);
    }
  },

  /**
   * Store rejected creative and validation errors on BE
   */
  storeRejected: storeHelper.createAction(types.STORE_REJECTED_CREATIVE, {
    call: api.storeRejected,
  }),

  /**
   * Attach Creative to campaign
   *
   * @param commit
   * @param payload
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  async attachToCampaign({commit}, payload) {
    commit(types.ATTACH_CREATIVE.PENDING);
    try {
      const response = await api.attach(payload.campaign, {creativeId: payload.creativeId});
      commit(types.ATTACH_CREATIVE.SUCCESS);
      commit('details/fillCreative', response.data.data, { root: true });
      commit('states/fillStates', response.data.data.states, { root: true });
      commit('wizard/creative/fill', response.data.data.creative, { root: true });

      return Promise.resolve(response);
    } catch (error) {
      commit(types.ATTACH_CREATIVE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Detach from campaign
   *
   * @param commit
   * @param payload
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  async detachFromCampaign({commit}, payload) {
    commit(types.DETACH_CREATIVE.PENDING);
    try {
      const response = await api.detach(payload);
      commit(types.DETACH_CREATIVE.SUCCESS);
      commit('details/fillCreative', response.data.data, { root: true });
      commit('wizard/creative/fill', response.data.data.creative, { root: true });
      commit('states/fillStates', response.data.data.states, { root: true });

      return Promise.resolve(response);
    } catch (error) {
      commit(types.DETACH_CREATIVE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Replace creative
   *
   * @param commit
   * @param payload
   * @param rootState
   * @returns {Promise<*|undefined>}
   */
  async replaceCreative({commit}, payload) {
    commit(types.REPLACE_CREATIVE.PENDING);
    try {
      const response = await api.replace(payload.campaign, {creativeId: payload.creativeId});
      commit('details/fillCreative', response.data.data, { root: true });
      commit('states/fillStates', response.data.data.states, { root: true });
      commit('wizard/creative/fill', response.data.data.creative, { root: true });

      commit(types.REPLACE_CREATIVE.SUCCESS);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.REPLACE_CREATIVE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Delete creative
   *
   * @param commit
   * @param dispatch
   * @param rootState
   * @param payload
   * @returns {Promise<*|undefined>}
   */
  async delete({ commit, dispatch, rootState }, payload) {
    commit(types.DELETE_CREATIVE.PENDING);
    try {
      const response = await api.delete(payload);
      commit(types.DELETE_CREATIVE.SUCCESS);
      if (rootState.wizard !== undefined) {
        dispatch('wizard/refreshCampaign', null, { root: true });
      }

      if (rootState.details !== undefined) {
        commit('details/removeCreative', null, { root: true });
        if (rootState.details.campaign.revisions.current) {
          dispatch('details/loadRevision', rootState.details.campaign.revisions.current.id, { root: true });
        }
      }
      return Promise.resolve(response);
    } catch (error) {
      commit(types.DELETE_CREATIVE.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Submit creative
   */
  submitCreative: storeHelper.createAction(types.SUBMIT_CREATIVE, {
    call: api.submit,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Store creative success
   * @param state
   */
  [types.STORE_CREATIVE.SUCCESS]: (state, payload) => {
    state.creative_id = payload.id;
    state[types.STORE_CREATIVE.loadingKey] = false;
    state.success = true;
  },

  /**
   * Store creative pending
   * @param state
   */
  [types.STORE_CREATIVE.PENDING](state) {
    state[types.STORE_CREATIVE.loadingKey] = true;
    state.success = false;
  },

  /**
   * Store creative items failure
   * @param state
   */
  [types.STORE_CREATIVE.FAILURE](state) {
    state[types.STORE_CREATIVE.loadingKey] = false;
    state.success = false;
  },

  /**
   * Store rejected creative mutation
   *
   * @param state, payload
   */
  ...storeHelper.createMutation(types.STORE_REJECTED_CREATIVE),

  /**
   * Attach creative success
   * @param state
   */
  [types.ATTACH_CREATIVE.SUCCESS]: (state) => {
    state[types.ATTACH_CREATIVE.loadingKey] = false;
  },

  /**
   * Attach creative pending
   * @param state
   */
  [types.ATTACH_CREATIVE.PENDING](state) {
    state[types.ATTACH_CREATIVE.loadingKey] = true;
  },

  /**
   * Attach creative items failure
   * @param state
   */
  [types.ATTACH_CREATIVE.FAILURE](state) {
    state[types.ATTACH_CREATIVE.loadingKey] = false;
  },

  /**
   * Detach creative success
   * @param state
   */
  [types.DETACH_CREATIVE.SUCCESS]: (state) => {
    state[types.DETACH_CREATIVE.loadingKey] = false;
  },

  /**
   * Detach creative pending
   * @param state
   */
  [types.DETACH_CREATIVE.PENDING](state) {
    state[types.DETACH_CREATIVE.loadingKey] = true;
  },

  /**
   * Detach creative items failure
   * @param state
   */
  [types.DETACH_CREATIVE.FAILURE](state) {
    state[types.DETACH_CREATIVE.loadingKey] = false;
  },

  /**
   * Delete creative success
   * @param state
   */
  [types.DELETE_CREATIVE.SUCCESS]: (state) => {
    state[types.DELETE_CREATIVE.loadingKey] = false;
  },

  /**
   * Delete creative pending
   * @param state
   */
  [types.DELETE_CREATIVE.PENDING](state) {
    state[types.DELETE_CREATIVE.loadingKey] = true;
  },

  /**
   * Delete creative failure
   * @param state
   */
  [types.DELETE_CREATIVE.FAILURE](state) {
    state[types.DELETE_CREATIVE.loadingKey] = false;
  },

  /**
   * Show creative details success
   * @param state
   * @param permissions
   */
  [types.SHOW_CREATIVE.SUCCESS] (state, { data }) {
    state[types.SHOW_CREATIVE.loadingKey] = false;
    state.permissions = data.permissions;
    state.showCreativeData = data;
  },

  /**
   * Show creative details loading
   * @param state
   */
  [types.SHOW_CREATIVE.PENDING](state) {
    state[types.SHOW_CREATIVE.loadingKey] = true;
    state.permissions.canDelete = false;
  },

  /**
   * Show creative details error
   * @param state
   */
  [types.SHOW_CREATIVE.FAILURE](state) {
    state[types.SHOW_CREATIVE.loadingKey] = false;
  },

  /**
   * Create mutation for replace creative.
   */
  ...storeHelper.createMutation(types.REPLACE_CREATIVE),

  /**
   * Set of mutations for creative submit.
   */
  ...storeHelper.createMutation(types.SUBMIT_CREATIVE),

  /**
   * Clear show creative storage
   */
  clearShowCreativeData () {
    this.state.showCreativeData = null;
  },

  /**
   * Clear file
   * @param state
   */
  clearFileData (state) {
    state.file = {
      data: {},
      name: null,
        file_path: null,
        extension: null,
        fileSize: null,
        duration: null,
        width: null,
        height: null,
        hashSum: null
    };
  },

  /**
   * Set upload phase
   * @param state
   * @param phase
   */
  setUploadPhase (state, phase) {
    state.uploadPhase = phase;
  },

  /**
   * Set upload UI state
   * @param state
   * @param uiState
   */
  setUiState (state, uiState) {
    state.uiState = uiState;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  modules,
  state,
  getters,
  actions,
  mutations,
};


