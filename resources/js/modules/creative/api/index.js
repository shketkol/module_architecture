import axios from 'axios';
import Router from '../../../foundation/services/routing/router';
import routes from 'Static/routes.json';
import CreativeMapper from "./mappers/CreativeMapper";

const router = new Router(routes);

const creative = {
  /**
   * Show creative data
   * @param id
   * @returns {AxiosPromise<any>}
   */
  show (id) {
    return axios.get(router.route('api.creatives.show', { creative: id }));
  },

  /**
   * Get Upload credentials
   *
   * @param hashSum
   * @returns {AxiosPromise}
   */
  getUploadCredentials (hashSum) {
    return axios.get(router.route('api.creatives.configuration', { hashSum: hashSum }));
  },

  /**
   * Store creative link
   * @param data
   * @returns {AxiosPromise}
   */
  store (data) {
    return axios.post(
      router.route('api.creatives.store'),
      CreativeMapper.mapObjectToApi(data)
    );
  },

  /**
   * Store rejected creative result
   * @param data
   * @returns {AxiosPromise}
   */
  storeRejected (data) {
    return axios.post(
      router.route('api.creatives.store-rejected'),
      data
    );
  },

  /**
   * Attach creative to the campaign.
   * @param id
   * @param data
   * @return {AxiosPromise<any>}
   */
  attach (id, data) {
    return axios.patch(router.route('api.campaigns.creative.update', { campaign: id }), data);
  },

  /**
   * Detach creative from the campaign.
   * @param id
   * @return {AxiosPromise<any>}
   */
  detach (id) {
    return axios.patch(router.route('api.campaigns.creative.update', {campaign: id}), {creative_id: null});
  },

  /**
   * Replace campaign creative
   * @param id
   * @return {AxiosPromise<any>}
   */
  replace (id, data) {
    return axios.patch(router.route('api.campaigns.creative.replace', {campaign: id}), data);
  },

  /**
   * Delete creative
   * @param id
   * @return {AxiosPromise<any>}
   */
  delete (id) {
    return axios.delete(router.route('api.creatives.delete', { creative: id }));
  },

  /**
   * Submit creative for review
   * @param id
   * @return {AxiosPromise<any>}
   */
  submit (id) {
    return axios.get(router.route('api.creatives.submit', { creative: id }));
  },
};

const errors = {
  load (id) {
    return axios.get(router.route('api.creatives.errors.index', { creative: id }));
  }
};

export {
  creative,
  errors
}
