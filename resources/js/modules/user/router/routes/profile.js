import TransactionHistoryTable from '../../../payment/components/TransactionHistoryTable';
import { isDemo } from "Modules/demo/helpers/demo";
import DemoSettingsTab from 'Modules/demo/modules/user/components/profile/settings/SettingsTab';
import ProfileSettingsTab from '../../components/profile/settings/SettingsTab';

const SettingsTab = isDemo() ? DemoSettingsTab : ProfileSettingsTab;

export default [
  {
    path: '/users/profile/transactions',
    component: TransactionHistoryTable,
    name: 'profile.transactionHistory',
    children: [
      {
        path: ':orderId',
        name: 'profile.transactionHistory.show',
        component: TransactionHistoryTable,
      },
    ]
  },
  {
    path: '/users/profile/settings',
    component: SettingsTab,
    name: 'profile.settings',
    children: [
      {
        path: 'contact',
        name: 'profile.settings.contact',
        component: SettingsTab,
      },
    ]
  },
]
