import Onboarding from './onboarding';
import Profile from './profile';
import { concat } from 'lodash';

export default concat(Onboarding, Profile);
