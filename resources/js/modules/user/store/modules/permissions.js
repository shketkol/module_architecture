import storeHelper from 'Foundation/helpers/store';
import { permissions as api } from 'Modules/user/api';
import PermissionMapper from 'Modules/user/api/mappers/PermissionMapper';

/**
 * @returns {{can: boolean, reason: string}}
 */
const getDefaultPermissionState = (defaultCan = false) => ({
  can: defaultCan,
  reason: '',
});

/**
 * Types
 */
const types = {
  SUBMIT_CREATIVES: storeHelper.createAsyncMutation('SUBMIT_CREATIVES'),
  UPLOAD_CREATIVE: storeHelper.createAsyncMutation('UPLOAD_CREATIVE'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Mutation state.
   */
  ...storeHelper.createMutationState([
    types.SUBMIT_CREATIVES,
    types.UPLOAD_CREATIVE,
  ]),

  /**
   * Permissions.
   */
  permissions: {
    submitCreatives: getDefaultPermissionState(),
    uploadCreative: getDefaultPermissionState(true),
  },
};

/**
 * Actions
 */
const actions = {
  checkSubmitCreatives: storeHelper.createAction(types.SUBMIT_CREATIVES, {
    call: api.canSubmitCreatives,
    map: PermissionMapper.mapApiToObject,
  }),

  checkUploadCreative: storeHelper.createAction(types.UPLOAD_CREATIVE, {
    call: api.canUploadCreative,
    map: PermissionMapper.mapApiToObject,
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  ...storeHelper.createMutation(types.SUBMIT_CREATIVES, {
    success(state, permission) {
      state.permissions.submitCreatives = permission;
    },
  }),

  ...storeHelper.createMutation(types.UPLOAD_CREATIVE, {
    success(state, permission) {
      state.permissions.uploadCreative = permission;
    },
  }),

};

export default {
  namespaced: true,
  types,
  state,
  actions,
  mutations,
};
