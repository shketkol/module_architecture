import storeHelper from 'Foundation/helpers/store';
import {getField, updateField} from 'vuex-map-fields';
import {profile as api} from '../../api';
import ProfileAccountMapper from '../../api/mappers/ProfileAccountMapper';
import Auth from '@aws-amplify/auth';

/**
 * @param {*} data
 * @param {function} commit
 */
const setUserData = (data, commit) => {
  if (data.data !== null) {
    const mappedData = ProfileAccountMapper.mapApiToObject(data);
    commit('setUserState', mappedData);
  }
};

/**
 * Types
 */
const types = {
  LOAD_ACCOUNT: storeHelper.createAsyncMutation('LOAD_ACCOUNT'),
  UPDATE_PERSONAL: storeHelper.createAsyncMutation('UPDATE_PERSONAL'),
  UPDATE_BUSINESS: storeHelper.createAsyncMutation('UPDATE_BUSINESS'),
  CHANGE_PASSWORD: storeHelper.createAsyncMutation('CHANGE_PASSWORD'),
  CHECK_LOCK_UPDATE: storeHelper.createAsyncMutation('CHECK_LOCK_UPDATE'),
};

/**
 * State
 * @type {{}}
 */
const state = {
  /**
   * Mutation state.
   */
  ...storeHelper.createMutationState([
    types.LOAD_ACCOUNT,
    types.UPDATE_PERSONAL,
    types.UPDATE_BUSINESS,
    types.CHANGE_PASSWORD,
    types.CHECK_LOCK_UPDATE,
  ]),

  /**
   * User.
   */
  user: {
    firstName: null,
    lastName: null,
    email: null,
    companyName: null,
    phone: null,
    address: {
      line1: null,
      line2: null,
      city: null,
      state: null,
      zip: null,
    },
  },

  /**
   * User permissions.
   */
  userPermissions: {
    update_business: null,
  },
};

/**
 * Getters.
 *
 * @type {{getField: *}}
 */
const getters = {
  /**
   * Check if Business Info data is editable.
   * @param {*} state
   * @returns {boolean}
   */
  canUpdateBusiness (state) {
    return state.userPermissions.update_business !== null ? state.userPermissions.update_business : true;
  },

  /**
   * Fields mapping.
   */
  getField,
};

/**
 * Actions
 */
const actions = {
  /**
   * Load account info from API.
   *
   * @param commit
   * @returns {Promise<*|undefined>}
   */
  async loadAccount({commit}) {
    commit(types.LOAD_ACCOUNT.PENDING);

    try {
      const response = await api.show();
      setUserData(response.data, commit);
      commit(types.LOAD_ACCOUNT.SUCCESS);

      return Promise.resolve(response);
    } catch (error) {
      commit(types.LOAD_ACCOUNT.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update personal information.
   *
   * @param commit
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async updatePersonalInfo({commit}, data) {
    commit(types.UPDATE_PERSONAL.PENDING);

    try {
      const response = await api.personal.update(data);
      commit(types.UPDATE_PERSONAL.SUCCESS);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_PERSONAL.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update password.
   *
   * @param commit
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async changePassword({commit}, data) {
    commit(types.CHANGE_PASSWORD.PENDING);

    try {
      const user = await Auth.currentAuthenticatedUser();
      const response = await Auth.changePassword(
        user,
        data.password.trim(),
        data.new_password
      );
      commit(types.CHANGE_PASSWORD.SUCCESS);
      return Promise.resolve(response);
    } catch (error) {
      commit(types.CHANGE_PASSWORD.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Update business information.
   *
   * @param commit
   * @param data
   * @returns {Promise<*|undefined>}
   */
  async updateBusinessInfo({commit}, data) {
    commit(types.UPDATE_BUSINESS.PENDING);
    try {
      const response = await api.business.update(data);
      setUserData(response.data, commit);
      commit(types.UPDATE_BUSINESS.SUCCESS);

      return Promise.resolve(response);
    } catch (error) {
      commit(types.UPDATE_BUSINESS.FAILURE, error && error.response);
      return Promise.reject(error);
    }
  },

  /**
   * Get state of lock_update flag.
   */
  checkLockUpdate: storeHelper.createAction(types.CHECK_LOCK_UPDATE, {
    call: api.checkLockUpdate,
    after: ({ commit }, data) => {
      setUserData(data, commit);
    },
  }),
};

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  /**
   * Setup user state.
   *
   * @param state
   * @param payload
   */
  setUserState: (state, payload) => {
    state.user = payload.user;
    state.userPermissions = payload.userPermissions;
  },

  /**
   * Account info is successfully loaded.
   *
   * @param state
   */
  [types.LOAD_ACCOUNT.SUCCESS]: (state) => {
    state[types.LOAD_ACCOUNT.loadingKey] = false;
  },

  /**
   * Account info is loading.
   *
   * @param state
   */
  [types.LOAD_ACCOUNT.PENDING](state) {
    state[types.LOAD_ACCOUNT.loadingKey] = true;
  },

  /**
   * Account info was not loaded.
   *
   * @param state
   */
  [types.LOAD_ACCOUNT.FAILURE](state) {
    state[types.LOAD_ACCOUNT.loadingKey] = false;
  },

  /**
   * Personal information was successfully updated.
   *
   * @param state
   */
  [types.UPDATE_PERSONAL.SUCCESS]: (state) => {
    state[types.UPDATE_PERSONAL.loadingKey] = false;
  },

  /**
   * Personal information is updating.
   *
   * @param state
   */
  [types.UPDATE_PERSONAL.PENDING](state) {
    state[types.UPDATE_PERSONAL.loadingKey] = true;
  },

  /**
   * Personal information was not updated.
   *
   * @param state
   */
  [types.UPDATE_PERSONAL.FAILURE](state) {
    state[types.UPDATE_PERSONAL.loadingKey] = false;
  },

  /**
   * Business information was successfully updated.
   *
   * @param state
   */
  [types.UPDATE_BUSINESS.SUCCESS]: (state) => {
    state[types.UPDATE_BUSINESS.loadingKey] = false;
  },

  /**
   * Business information is updating.
   *
   * @param state
   */
  [types.UPDATE_BUSINESS.PENDING](state) {
    state[types.UPDATE_BUSINESS.loadingKey] = true;
  },

  /**
   * Business information was not updated.
   *
   * @param state
   */
  [types.UPDATE_BUSINESS.FAILURE](state) {
    state[types.UPDATE_BUSINESS.loadingKey] = false;
  },

  /**
   * Password was changed
   *
   * @param state
   */
  [types.CHANGE_PASSWORD.SUCCESS]: (state) => {
    state[types.CHANGE_PASSWORD.loadingKey] = false;
  },

  /**
   * Password is changing.
   *
   * @param state
   */
  [types.CHANGE_PASSWORD.PENDING](state) {
    state[types.CHANGE_PASSWORD.loadingKey] = true;
  },

  /**
   * Password was not changed.
   *
   * @param state
   */
  [types.CHANGE_PASSWORD.FAILURE](state) {
    state[types.CHANGE_PASSWORD.loadingKey] = false;
  },

  ...storeHelper.createMutation(types.CHECK_LOCK_UPDATE),

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  getters,
  actions,
  mutations,
};


