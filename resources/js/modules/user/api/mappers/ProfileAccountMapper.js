export default class ProfileAccountMapper {
  static mapApiToObject ({ data }) {
    return {
      user: {
        firstName: data.first_name,
        lastName: data.last_name,
        email: data.email,
        password: null,
        companyName: data.company.company_name,
        phone: data.phone_number,
        paymentMethods: data.payment_methods,
        address: {
          line1: data.company.address_line1,
          line2: data.company.address_line2,
          city: data.company.city,
          state: data.company.state,
          zip: data.company.zipcode,
        },
      },
      userPermissions: data.permissions,
    };
  }
}
