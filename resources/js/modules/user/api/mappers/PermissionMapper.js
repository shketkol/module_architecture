export default class PermissionMapper {
  static mapApiToObject ({ data }) {
    return {
      can: data.can,
      reason: data.reason,
    };
  }
}
