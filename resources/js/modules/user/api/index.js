import axios from 'axios';
import Router from '../../../foundation/services/routing/router';
import routes from 'Static/routes.json';
import location from './location';
import permissions from './permissions';
import route from 'App/foundation/services/routing/route';

const router = new Router(routes);

const profile = {
  show() {
    return axios.get(router.route('api.users.profile'));
  },

  checkLockUpdate () {
    return axios.get(route('api.users.check-lock-update'));
  },

  personal: {
    update(data) {
      return axios.patch(router.route('api.users.update'), data)
    },
  },

  business: {
    update(data) {
      return axios.patch(router.route('api.users.company.update'), data)
    },
  },
};

const onboarding = {
  /**
   * Complete the onboarding.
   *
   * @returns {AxiosPromise<any>}
   */
  complete () {
    return axios.post(router.route('users.onboarding.complete'));
  },
};

export {
  profile,
  onboarding,
  location,
  permissions,
}
