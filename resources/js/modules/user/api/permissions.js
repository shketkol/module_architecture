import axios from 'axios';
import route from 'Foundation/services/routing/route';

export default {
  /**
   * Check if user can upload creatives.
   * @return {AxiosPromise<any>}
   */
  canUploadCreative () {
    return axios.get(route('api.users.permission.create-creative'));
  },

  /**
   * Check if user can submit creatives.
   * @return {AxiosPromise<any>}
   */
  canSubmitCreatives () {
    return axios.get(route('api.users.permission.submit-creatives'));
  },
}
