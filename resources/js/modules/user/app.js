import 'Foundation/bootstrap';
import 'Foundation/services/amplify'
import OnboardingPage from './pages/TheOnboarding';
import ProfilePage from './pages/TheProfile';
import { getRouter } from 'Foundation/services/routing/factory';
import routes from './router/routes';
import store from './store';
import Vue from 'vue';

(new Vue({
  /**
   * DOM Element selector.
   */
  el: '.vue-app',

  /**
   * Child components.
   */
  components: {
    OnboardingPage,
    ProfilePage,
  },

  /**
   * Vue router.
   */
  router: getRouter({
    mode: 'history',
    routes,
  }),

  /**
   * Vuex.
   */
  store,
}));
