import BaseProperties from './BaseProperties';

export default {
  /**
   * Mixins.
   */
  mixins: [
    BaseProperties,
  ],

  /**
   * Props.
   */
  props: {
    /**
     * Is autocomplete enabled.
     */
    autocomplete: {
      type: String,
      default: 'off',
      validator (value) {
        return ['on', 'off', 'new-password'].includes(value);
      },
    },
    /**
     * Is autofocus on the component enabled.
     */
    autofocus: {
      type: Boolean,
      default: false,
    },
    /**
     * Whether to show 'clear' button.
     */
    clearable: {
      type: Boolean,
      default: false,
    },
    /**
     * Label text for input.
     */
    label: {
      type: String,
      default: '',
    },
    /**
     * Same as 'max' native attribute.
     */
    max: {
      type: Number,
      default: Number.MAX_SAFE_INTEGER,
    },
    /**
     * Same as 'maxlength' native attribute.
     */
    maxlength: {
      type: Number,
      default: Number.MAX_SAFE_INTEGER,
    },
    /**
     * Same as 'min' native attribute.
     */
    min: {
      type: Number,
      default: 0,
    },
    /**
     * Same as 'minlength' native attribute.
     */
    minlength: {
      type: Number,
      default: 0,
    },
    /**
     * A placeholder of input.
     */
    placeholder: {
      type: String,
      default: '',
    },
    /**
     * Is label shown.
     */
    showLabel: {
      type: Boolean,
      default: true,
    },
    /**
     * Tab index.
     */
    tabindex: {
      type: [String, Number],
      default: null,
    },
    /**
     * Value.
     */
    value: {
      type: [String, Number, Object, Date],
      default: '',
    }
  },
};
