import { isDate } from 'element-ui/src/utils/date-util';

export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Check if date-range value is valid.
     * @param {*} value
     * @returns {Boolean}
     */
    isValidRange (value) {
      if (this.disableStartDate) {
        return Array.isArray(value) &&
          value &&
          this.isValidDate(value[1]) &&
          (
            value[1] === null ||
            value[0] <= value[1]
          );
      }

      return Array.isArray(value) &&
        value &&
        this.isValidDate(value[0]) &&
        this.isValidDate(value[1]) &&
        (
          value[0] === null ||
          value[1] === null ||
          value[0] <= value[1]
        );
    },

    isCompleteRange (value) {
      return this.isValidRange(value) &&
        value[0] !== null &&
        value[1] !== null &&
        value[0] <= value[1]
        ;
    },

    /**
     * Check if provided date is valid.
     * @param {*} date
     * @returns {Boolean} NOTE: null is considered as a valid state of date.
     */
    isValidDate (date) {
      if (date === '') {
        return false;
      }
      if (date === null || date === undefined) {
        return true;
      }
      return isDate(date) && (
        typeof this.disabledDate === 'function'
          ? !this.disabledDate(date)
          : true
      );
    },
  },
};
