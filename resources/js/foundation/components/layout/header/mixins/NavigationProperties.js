export default {
  /**
   * Props.
   */
  props: {
    /**
     * True if a component should be disabled.
     */
    menu: {
      type: String,
      default: '',
    },
  },
};
