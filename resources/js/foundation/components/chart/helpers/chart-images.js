/**
 * Min image width for "100%" value.
 * @type {number}
 */
const MIN_100_WIDTH = 112;

/**
 * Generate images for the chart.
 * @param {Array} data
 * @param {string} urlToFontsCss
 * @returns {Promise}
 */
const generateChartImages = async (data, urlToFontsCss) => {
  const cssRules = await fontToDataURI(urlToFontsCss);
  return new Promise((resolve) => {
    const images = [];
    data.forEach((item) => {
      images.push(generateChartImage(item, cssRules))
    });
    resolve(Promise.all(images));
  });
};

/**
 * Generate chart image.
 * @param {object} image
 * @param {Array} cssRules
 * @returns {Promise<any>}
 */
const generateChartImage = (image, cssRules) => {
  return new Promise((resolve, reject) => {
    image = adjustImageDimensions(image);
    const svgData = getChartImageSvg(image);
    const svgDoc = new DOMParser().parseFromString(svgData, 'image/svg+xml');
    const svgNS = 'http://www.w3.org/2000/svg';
    const defs = svgDoc.createElementNS(svgNS, 'defs');
    const style = svgDoc.createElementNS(svgNS, 'style');
    style.innerHTML = cssRules.join('\n');
    defs.appendChild(style);
    svgDoc.documentElement.appendChild(defs);
    const str = new XMLSerializer().serializeToString(svgDoc.documentElement);
    const svg = new Blob([str], {type: 'image/svg+xml;charset=utf-8'});
    const reader = new FileReader();

    reader.onload = function() {
      resolve({
        src: reader.result,
        width: image.width,
        height: image.height,
      });
    };
    reader.onerror = reject;
    // Converts the blob to base64 and calls onload.
    reader.readAsDataURL(svg);
  });
};

/**
 * Get template for the chart image.
 * @param {string} image
 * @param {number} width
 * @param {number} height
 * @param {string} color
 * @param {number} value
 * @param {string} label
 * @returns {string}
 */
const getChartImageSvg = ({image, width, height, color, value, label}) => {
  return `
    <svg xmlns="http://www.w3.org/2000/svg" width="${width}" height="${height}">
      <foreignObject width="100%" height="100%">
        <div xmlns="http://www.w3.org/1999/xhtml" style="text-align: center;">
          <div style="font-family: Graphik; font-size:41px; font-weight: 700; color: ${color}; padding-bottom: 10px;">${value}%</div>
          ${image}
          <div style="font-family: Graphik; font-size:14px; font-weight: 500; color:#000; text-transform: uppercase; padding-top: 10px;">${label}</div>
        </div>
      </foreignObject>
    </svg>
  `;
};

/**
 * Get fonts as dataURI objects.
 * [from https://stackoverflow.com/questions/42402584/how-to-use-google-fonts-in-canvas-when-drawing-dom-objects-in-svg/42405731#42405731]
 * @param {string} url An url pointing to an embed Font stylesheet.
 * @returns {Promise<any[]>}
 */
const fontToDataURI = (url) => {
  return fetch(url) // first fecth the embed stylesheet page
    .then(resp => resp.text()) // we only need the text of it
    .then(text => {
      // Now we need to parse the CSSruleSets contained.
      // But chrome doesn't support styleSheets in DOMParsed docs...
      let styleEl = document.createElement('style');
      styleEl.innerHTML = text;
      document.head.appendChild(styleEl);
      let styleSheet = styleEl.sheet;

      // This will help us to keep track of the rules and the original urls.
      let FontRule = rule => {
        let src = rule.style.getPropertyValue('src') || rule.style.cssText.match(/url\(.*?\)/g)[0];
        if (!src) return null;
        let url = src.split('url(')[1].split(')')[0];
        return {
          rule: rule,
          src: src,
          url: url.replace(/"/g, '')
        };
      };
      let fontRules = [],
        fontProms = [];

      // Iterate through all the cssRules of the embedded doc.
      // Edge doesn't make CSSRuleList enumerable...
      for (let i = 0; i < styleSheet.cssRules.length; i++) {
        let r = styleSheet.cssRules[i];
        let fR = FontRule(r);
        if (!fR) {
          continue;
        }
        fontRules.push(fR);
        fontProms.push(
          // Fetch the actual font-file (.woff)
          fetch(fR.url)
            .then(resp => resp.blob())
            .then(blob => {
              return new Promise(resolve => {
                // We have to return it as a dataURI because for whatever reason, browser are afraid of blobURI in <img> too...
                let f = new FileReader();
                f.onload = () => resolve(f.result);
                f.readAsDataURL(blob);
              })
            })
            .then(dataURL => {
              // Now that we have our dataURI version, we can replace the original URI with it and we return the full rule's cssText.
              return fR.rule.cssText.replace(fR.url, dataURL);
            })
        )
      }
      // Clean up.
      document.head.removeChild(styleEl);
      // Wait for all this has been done.
      return Promise.all(fontProms);
    });
};

/**
 * To avoid cutting the value of "100%" we need to adjust the size of the svg.
 * @param {*} image
 * @returns {*}
 */
const adjustImageDimensions = (image) => {
  const width = (image.value === 100 && image.width < MIN_100_WIDTH) ? MIN_100_WIDTH : image.width;
  if (width !== image.width) {
    image.height = image.height * width / image.width;
    image.width = width;
  }
  return image;
};

export {
  generateChartImages,
}
