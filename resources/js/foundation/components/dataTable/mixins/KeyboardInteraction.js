export default {

  /**
   * Mixin props
   */
  props: {
    /**
     * True - if keyboard events should be handled
     * False - subscribe on the blur event only
     */
    useKeyboardEvents: {
      type: Boolean,
      default: true
    }
  },

  /**
   * Additional component data
   */
  data () {
    return {
      rows: null,
      enterListener: null,
      blurListener: null
    }
  },

  /**
   * Before destroy hook
   */
  beforeDestroy() {
    this.removeKeyboardEvents();
  },

  /**
   * Methods
   */
  methods: {
    /**
     * Apply missed attributes for keyboard interaction
     */
    applyKeyboardEvents () {

      if (!this.$refs.dataTable) {
        return;
      }

      this.enterListener = (event) => {
        //trigger active row details opening by pressing enter-key
        if (event.key === 'Enter' && !event.target.hasAttribute('disabled')) {
          event.target.dispatchEvent(new Event('click'));
        }
      }

      this.blurListener = () => {
        if (!this.$refs.dataTable) {
          return;
        }
        this.$refs.dataTable.setCurrentRow(null);
      }

      this.removeKeyboardEvents();

      this.$nextTick(() => {
        this.rows = this.$refs.dataTable.$refs.bodyWrapper.getElementsByTagName("tr");

        for (let row of this.rows) {
          if (this.useKeyboardEvents) {
            row.setAttribute("tabindex", 0);
            row.addEventListener('keypress', this.enterListener);
          }

          row.addEventListener('blur', this.blurListener);
        }
      })
    },

    /**
     * Remove missed attributes for keyboard events for an old elements.
     */
    removeKeyboardEvents () {
      if (this.rows) {
        for (let oldRow of this.rows) {
          if (this.useKeyboardEvents) {
            oldRow.removeEventListener('keypress', this.enterListener);
          }
          oldRow.removeEventListener('blur', this.blurListener);
        }
      }
    }
  }
};
