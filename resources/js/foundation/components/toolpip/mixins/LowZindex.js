const LOW_ZINDEX_VAL = 10;

export default {
  /**
   * Available properties.
   */
  props: {
    /**
     * Verifies if we need low z-index for popper.
     */
    lowZindex: {
      type: Boolean,
      default: false,
    },
  },

  /**
   * Computed properties.
   */
  computed: {
    /**
     * Z-index value for popper element.
     * @returns {(number|boolean)}
     */
    zindex () {
      return this.lowZindex ? LOW_ZINDEX_VAL : false;
    },
  },
};
