import GuestLayoutLinks from "Foundation/components/layout/header/mixins/GuestLayoutLinks";

export default {
  /**
   * Component mixins.
   */
  mixins: [GuestLayoutLinks],

  /**
   * Computed properties.
   */
  computed: {
    /**
     * List of all links.
     */
    links () {
      return [
        {
          href: this.termsOfUseLink,
          label: this.$trans('labels.terms_of_use'),
          target: '_blank',
        },
        {
          href: this.betaTermsOfUseLink,
          label: this.$trans('labels.beta_terms_of_use'),
          target: '_blank',
        },
        {
          href: this.privacyPolicyLink,
          label: this.$trans('labels.privacy_policy'),
          target: '_blank',
        }
      ];
    },

    /**
     * Filtered list of links.
     *
     * @returns {*}
     */
    visibleLinks () {
      return this.links.filter(link => (link.hidden === undefined || !link.hidden));
    },
  },
};
