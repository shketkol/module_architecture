import { Mark, Plugin } from 'tiptap';
import { updateMark, removeMark, pasteRule } from 'tiptap-commands';
import { getMarkAttrs } from 'tiptap-utils';

export default class LinkWithTitle extends Mark {
  /**
   * Name getter.
   */
  get name() {
    return 'link';
  }

  /**
   * Schema getter.
   */
  get schema() {
    return {
      attrs: {
        href: {
          default: null,
          title: null,
        },
      },
      inclusive: false,
      parseDOM: [
        {
          tag: 'a[href]',
          getAttrs: dom => ({
            href: dom.getAttribute('href'),
            title: dom.getAttribute('href'),
          }),
        },
      ],
      toDOM: node => ['a', {
        ...node.attrs,
        rel: 'noopener noreferrer nofollow',
        title: node.attrs.href,
        target: '_blank',
      }, 0],
    }
  }

  /**
   * Commands.
   */
  commands({type}) {
    return attrs => {
      if (attrs.href) {
        return updateMark(type, attrs);
      }

      return removeMark(type);
    }
  }

  /**
   * Paste rules.
   */
  pasteRules({ type }) {
    return [
      pasteRule(
        /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-zA-Z]{2,}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g,
        type,
        url => ({ href: url }),
      ),
    ];
  }

  /**
   * Plugins getter.
   */
  get plugins() {
    return [
      new Plugin({
        props: {
          handleClick: (view, pos, event) => {
            const { schema } = view.state;
            const attrs = getMarkAttrs(view.state, schema.marks.link);

            if (attrs.href && event.target instanceof HTMLAnchorElement) {
              event.stopPropagation();
              window.open(attrs.href);
            }
          },
        },
      }),
    ];
  }
}
