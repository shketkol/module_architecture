/**
 * Format card expiry from month/year params
 *
 * @param month
 * @param year
 */
const formatCardExpiry = (month, year) => {
  return `${month.toString().padStart(2, '0')}/${year.toString().substring(2)}`;
};

export {
  formatCardExpiry,
}
