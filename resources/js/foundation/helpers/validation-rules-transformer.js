import isObject from 'lodash/isObject';
import transform from 'lodash/transform';
import Validation from 'Static/validation.json';

/**
 * Adjust Laravel rules to be used in vee-validator.
 * @returns {*}
 */
const getTrasnformedRules = () => {
  function deepMap(obj, iterator, context) {
    return transform(obj, function(result, val, key) {
      result[key] = isObject(val) ?
        deepMap(val, iterator, context) :
        iterator.call(context, val, key, obj);
    });
  }

  return deepMap(Validation, (v, k) => {
    if (k === 'regex') {
      v = new RegExp(v.substring(1, v.length-1));
    }

    return v;
  });
};

export {
  getTrasnformedRules,
};
