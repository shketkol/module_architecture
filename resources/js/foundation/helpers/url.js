/**
 * Get value of get-parameter with the specified name.
 * e.g. > ?action=view&section=info&id=123&debug&testy[]=true&testy[]=false&testy[]
 *      > queryGet('section')
 *      > "info"
 *
 * @param name
 * @returns {string}
 */
const queryGet = name => {
  return (new URLSearchParams(window.location.search)).get(name);
};

/**
 * Set get-parameter in url.
 * e.g. > ?action=view
 *      > querySet('section', 'info')
 *      > ?action=view&section=info
 *
 * @param name
 * @param value
 * @returns {string}
 */
const querySet = (name, value) => {
  const urlSearchParams = new URLSearchParams(window.location.search);

  urlSearchParams.set(name, value);

  return `?${urlSearchParams}`;
};

export {
  queryGet,
  querySet,
};
