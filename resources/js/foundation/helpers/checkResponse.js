import {isFireFox} from './browser-detect';

const checkAbortedRequestError = (error) => {
  // NOTE: Firefox throws "Request aborted" error for each request that's being run when user/application leaves current page
  return isFireFox() && error.message === 'Request aborted' && error.response === undefined;
};


export {
  checkAbortedRequestError
}
