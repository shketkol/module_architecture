/**
 * Whether the user's browser unsupported.
 */
const browserUnsupported = () => {
  const userAgent = navigator.userAgent.toLowerCase();
  return userAgent.indexOf('opr/') !== -1 || // opera
    userAgent.indexOf('edg/') !== -1 || // edge
    userAgent.indexOf('edge') !== -1 || // edge
    userAgent.indexOf('trident') !== -1; // ie
};

/**
 * Whether the user's browser is safari.
 */
const isSafari = () => {
  return /^((?!chrome|android).)*safari/i.test(navigator.userAgent.toLowerCase());
};

/**
 * Whether the user's browser is fireFox.
 */
const isFireFox = () => {
  return /firefox/i.test(navigator.userAgent.toLowerCase());
};

export {
  browserUnsupported, isSafari, isFireFox
}
