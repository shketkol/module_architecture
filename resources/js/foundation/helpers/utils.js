const attachClassToDocumentBody = (visible, className) => {
  const classList = document.body.classList;
  if (visible) {
    classList.add(className);
  } else {
    if (classList.contains(className)) {
      classList.remove(className);
    }
  }
};

/**
 * Check if "value" is an empty array.
 * @param value
 * @returns {boolean}
 */
const isEmptyArray = (value) => Array.isArray(value) && !value.length;

/**
 * Check if "value" is not an empty array.
 * @param value
 * @returns {boolean}
 */
const isNotEmptyArray = (value) => Array.isArray(value) && value.length > 0;

export {
  attachClassToDocumentBody,
  isEmptyArray,
  isNotEmptyArray,
};
