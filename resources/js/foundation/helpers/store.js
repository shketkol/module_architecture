import camelCase from 'lodash/camelCase';
import mapValues from 'lodash/mapValues';
import mapKeys from 'lodash/mapKeys';
import isArray from 'lodash/isArray';
import Log from 'Foundation/services/log/log';
import MutationErrorException from 'Foundation/exceptions/MutationErrorException';
import { checkAbortedRequestError } from './checkResponse';

const log = new Log();

/**
 * Create async mutation helper
 * @param type
 * @returns {{loadingKey, SUCCESS: string, errorsKey, statusKey, PENDING: string, FAILURE: string}}
 */
const createAsyncMutation = (type) => ({
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  PENDING: `${type}_PENDING`,
  loadingKey: camelCase(`${type}_PENDING`),
  ERROR: camelCase(`${type}_ERROR`),
});

/**
 * Create state for async mutation.
 * @param types
 * @returns {{}}
 */
const createMutationState = (types) => {
  const state = {};

  if (isArray(types)) {
    types.forEach(type => {
      state[type.loadingKey] = false;
      state[type.ERROR] = null;
    });
  } else {
    state[types.loadingKey] = false;
    state[types.ERROR] = null;
  }

  return state;
};

/**
 * Create state for async mutation.
 * @param type
 * @returns {string}
 */
const typeActionFail = (type) => {
  return type.replace('_FAILURE', '');
};

/**
 * Create async action helper.
 * @param type
 * @param callbacks
 * @returns {Function}
 */
const createAction = (type, callbacks) => (async (state, payload) => {
  state.commit(type.PENDING);

  try {
    // Map request data.
    let requestData = payload;
    const callMap = callbacks.callMap;
    if (callMap && callMap instanceof Function) {
      requestData = callMap(requestData, payload);
    }

    // Do request via "call" callback.
    const response = await callbacks.call(requestData);

    // Map response data.
    let data = response.data;
    const map = callbacks.map;
    if (map && map instanceof Function) {
      data = map(data, payload);
    }

    state.commit(type.SUCCESS, data, payload);

    // Run "after" hook.
    const after = callbacks.after;
    if (after && after instanceof Function) {
      after(state, data, payload);
    }

    return Promise.resolve(response);
  } catch (error) {
    // Check if request aborted
    if (checkAbortedRequestError(error)) {
      return Promise.reject(error);
    }

    if (!(error instanceof MutationErrorException) && error.request === undefined) {
      log.set(`Fail in the action: ${typeActionFail(type.FAILURE)}`, error);
    }

    state.commit(type.FAILURE, error && error.response);

    const afterFail = callbacks.afterFail;
    if (afterFail && afterFail instanceof Function) {
      afterFail(state, error && error.response);
    }
    return Promise.reject(error);
  }
});

/**
 * Helper to create unified mutations and avoid copy pasted code.
 */

/**
 * Mapping for mutations and loading.
 * @type {{PENDING: boolean, SUCCESS: boolean, FAILURE: boolean}}
 */
const mutationLoadings = {
  'PENDING': true,
  'SUCCESS': false,
  'FAILURE': false,
};

/**
 * Create mutation helper.
 * @param type
 * @param callbacks
 * @returns {{}}
 */
const createMutation = (type, callbacks = {}) => ({
  ...mapKeys(mapValues(mutationLoadings, (loading, key) => {
    return (state, payload) => {
      try {
        const callback = callbacks[key.toLowerCase()];
        if (callback && callback instanceof Function) {
          callback(state, payload);
        }

        state[type.loadingKey] = loading;
      } catch (error) {
        throw new MutationErrorException(`Fail in the mutation: ${typeActionFail(type.FAILURE)}`);
      }
    };
  }), (value, key) => (type[key])),
});

export default {
  createAction,
  createAsyncMutation,
  createMutation,
  createMutationState,
}
