import { isString } from 'lodash';

const TOP_NAVIGATION_GROUP = 0;

/**
 * @returns {number}
 */
const getTopNavigationIndex = () => TOP_NAVIGATION_GROUP;

/**
 * Remove tabindexes from specific sub-items of the parent element.
 * @param {HTMLElement} parentEl
 * @param {string} subNodeSelectors
 */
const removeTabIndexesFromSubNodes = (parentEl, subNodeSelectors) => {
  const subNodes = getSubNodes(parentEl, subNodeSelectors);
  removeTabIndexes(subNodes);
};

/**
 * Remove tabindex attribute from the list of elements.
 * @param {(Array|NodeList)} elements
 */
const removeTabIndexes = (elements) => {
  if (!elements.length) {
    return;
  }
  elements.forEach((item => item.removeAttribute('tabindex')));
};

/**
 * Set tabindexes for the provided elements.
 * @param elements
 * @param tabIndex
 */
const setTabIndexes = (elements, tabIndex) => {
  if (!elements.length) {
    return;
  }

  elements.forEach(item => item.setAttribute('tabindex', tabIndex));
};

/**
 * Get subnodes
 * @param parentEl
 * @param subNodeSelectors
 * @returns {(false|NodeList)}
 */
const getSubNodes = (parentEl, subNodeSelectors) => {
  if (!(parentEl instanceof HTMLElement)) {
    return false;
  }
  if (!isString(subNodeSelectors) || !subNodeSelectors.trim()) {
    return false;
  }
  return parentEl.querySelectorAll(subNodeSelectors);
};


export {
  setTabIndexes,
  getTopNavigationIndex,
  removeTabIndexesFromSubNodes,
};
