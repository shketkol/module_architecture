/**
 * Calculate impressions amount based on cost and cpm.
 *
 * @param {(number|string)} cost
 * @param {number} cpm
 */
const calculateImpressions = (cost, cpm) => {
  cost = +cost;
  if (Number.isNaN(cost)) {
    return 0;
  }
  return Math.floor((cost * 1000 / cpm).toFixed(2));
};

/**
 * Calculates cost based on impressions and CPM.
 *
 * @param {number} impressions
 * @param {number} cpm
 */
const calculateCost = (impressions, cpm) => {
  return Math.ceil(impressions * (cpm / 1000) * 100) / 100;
};

export {
  calculateImpressions,
  calculateCost,
}
