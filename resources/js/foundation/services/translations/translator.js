import { get as getValue, has as hasValue, forEach } from 'lodash';

export default class Translator {
  /**
   * @param locales
   * @param locale
   */
  constructor (locales, locale) {
    this.locale = locale;
    this.locales = locales[locale];
  }

  /**
   * Check if translation message exists
   *
   * @param key
   */
  has (key) {
    return hasValue(this.locales, key);
  }

  /**
   * Get translation message
   *
   * @param key
   * @param params
   */
  get (key, params = null) {
    const message = getValue(this.locales, key);

    if (params) {
      return this.compile(message, params);
    }

    return message;
  }

  /**
   * Get pluralised value.
   *
   * @param key
   * @param number
   * @param replace
   * @returns {*}
   */
  choice (key, number, replace = {}) {
    const message = getValue(this.locales, key);
    const replaceLength = Object.keys(replace).length;
    if (!message) {
      return key;
    }
    const messages = message.split('|');
    const chosenMessage = parseInt(number, 10) > 1 ? messages[1] : messages[0];

    return replaceLength > 0 ? this.compile(chosenMessage, replace) : chosenMessage;
  }

  /**
   * Get raw translation.
   * @param key
   */
  raw (key) {
    return getValue(this.locales, key)
  }

  /**
   * Compile message
   *
   * @param message
   * @param params
   * @returns {*}
   */
  compile (message, params) {
    message = (typeof message === 'string') ? message : '';

    forEach(params, (value, key) => {
      message = message.replace(':' + key, value);
    });

    return message;
  }
}
