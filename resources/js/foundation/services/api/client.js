import { forEach, head } from 'lodash';
import Log from 'Foundation/services/log/log';
import { checkAbortedRequestError } from '../../helpers/checkResponse';

const HTTP_BAD_REQUEST = 400;
const HTTP_UNAUTHORIZED = 401;
const HTTP_FORBIDDEN = 403;
const HTTP_UNAUTHORIZED_HAAPI = 451; //401 code causing basic auth logout
const HTTP_UNPROCESSABLE_ENTITY = 422;
const HTTP_INTERNAL_SERVER_ERROR = 500;

const log = new Log();

export default class Client {
  /**
   * Constructor.
   *
   * @param vm
   */
  constructor (vm) {
    this.vm = vm;
  }

  /**
   * Handle backend response.
   *
   * @param action
   * @param params
   * @returns {Promise<any>}
   */
  call (action, params) {
    params = { scope: '__global__', notify: true, muteException: false, ...params };

    return new Promise((resolve, reject) => {
      action
        .then(response => {
          this.handleSuccess(response, params);
          resolve(response);
        })
        .catch((error) => {
          // Resolved request error in cases using raw axios request instead vuex action
          if (checkAbortedRequestError(error)) {
            return;
          }

          if (error.response) {
            this.handleError(error.response, params, error.message);
            error.response = { handled: true, ...error.response };
            if (!params.muteException) {
              reject(error.response);
            }
            return;
          }

          if (error.message) {
            this.handleMessageError(error.message);
          }

          if (!params.muteException) {
            reject(error);
          }
        });
    });
  }

  /**
   * Request is successful.
   *
   * @param message
   * @param params
   */
  handleSuccess ({ data }, params) {
    if (!data) {
      return;
    }
    const message = data.data ? data.data.message : data.message;
    if (message && params.notify) {
      this.vm.$notify.success({
        title: data.data ? data.data.title : null,
        message,
      });
    }
  }

  /**
   * Request failed.
   *
   * @param response
   * @param params
   * @param message
   * @returns {*}
   */
  handleError (response, params, message) {
    if (response.status >= HTTP_FORBIDDEN && response.data && response.data.redirect) {
      window.location = response.data.redirect;
    }

    if (!response || !response.status) {
      log.set(message, response);
      return this.handleUndefinedError(params);
    }

    if (response.status >= HTTP_INTERNAL_SERVER_ERROR) {
      message += ` (route: ${response.config.url})`;
      log.set(message, response);
      return this.handleServerError(response, params);
    }

    if (response.status >= HTTP_BAD_REQUEST) {
      return this.handleClientError(response, params);
    }
  }

  /**
   * Undefined error handling.
   */
  handleUndefinedError (params) {
    if (params.notify) {
      this.vm.$notify.error({
        message: this.vm.$trans('messages.went_wrong'),
      });
    }
  }

  /**
   * Undefined error handling.
   */
  handleMessageError (message) {
    this.vm.$notify.error({
      message: message,
    });
  }

  /**
   * Internal server error (5xx status codes).
   */
  handleServerError (response, params) {
    return this.handleUndefinedError(params);
  }

  /**
   * Client error (4xx status codes).
   *
   * @param response
   * @param scope
   */
  handleClientError (response, params = { scope: '__global__', notify: true }) {

    if (response.status === HTTP_UNPROCESSABLE_ENTITY) {
      this.handleValidationError(response, params);
    }

    if (response.status === HTTP_UNAUTHORIZED || response.status === HTTP_UNAUTHORIZED_HAAPI) {
      this.vm.$redirectToRoute('login.form');
    }
  }

  /**
   * Request failed due to validation errors (422).
   *
   * @param data
   * @param scope
   */
  handleValidationError ({ data }, params = { scope: '__global__', notify: true }) {

    if (data.is_action_validation !== undefined && data.is_action_validation) {
      return;
    }

    const messages = [];

    forEach(data.errors ? data.errors : data, (errors, name) => {
      const error = this.getErrorMessage(errors);

      this.addValidationError(name, error, params.scope);

      messages.push(error);
    });

    const message = data.message || (data.data && data.data.message);

    if (params.notify) {
      this.vm.$notify.error({
        dangerouslyUseHTMLString: true,
        message: messages.length ? messages.join('<br>') : message || this.vm.$trans('messages.check_form')
      });
    }
  }

  /**
   * Get error message
   *
   * @param error
   * @returns {string}
   */
  getErrorMessage (error) {
    if (typeof error === 'string') {
      return error;
    }

    if (Array.isArray(error)) {
      error = head(error);
    }

    if (typeof error === 'object') {
      error = Object.values(error)[0];
    }

    return error;
  }

  /**
   * Add new validation error to the error bag.
   *
   * @param field
   * @param msg
   * @param scope
   */
  addValidationError (field, msg, scope = '__global__') {
    this.vm.$validator.errors.add({
      field,
      msg,
      scope: scope,
      rule: 'backend',
    });
  }
}
