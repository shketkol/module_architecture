import pick from 'lodash/pick';
import Vue from 'vue';
import { getTrasnformedRules } from 'Foundation/helpers/validation-rules-transformer';

Vue.use({
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({
      computed: {
        validationRules () {
          return getTrasnformedRules();
        }
      },

      /**
       * Before create hook.
       * This method is used to create one validator instance for all of the components
       * (including components from libraries (e.g. ElementUI)).
       */
      beforeCreate () {
        if (!this.$parent) {
          return;
        }

        this.$validator = this.$parent.$validator;
      },

      /**
       * Methods.
       */
      methods: {
        /**
         * Makes frontend validation of the whole form.
         *
         * Example of usage:
         *
         * ----- Validate all fields -----
         *
         * $validator.validate();
         *
         * ----- Validate a field that has a matching name with the provided selector -----
         *
         * $validator.validate('field');
         *
         * ----- Validate a field within a scope -----
         *
         * $validator.validate('scope.field');
         *
         * ----- Validate all fields within this scope -----
         *
         * $validator.validate('scope.*');
         *
         * ----- Validate all fields without a scope. ----
         *
         * $validator.validate('*');
         */
        $validate (payload = null) {
          return this.$validator.validate(payload);
        },

        /**
         * Validate a concrete scope.
         */
        $validateScope (scope = '__global__') {
          return this.$validate(`${scope}.*`);
        },

        /**
         * Validate a concrete field.
         */
        $validateField (field) {
          return this.$validate(field);
        },

        /**
         * Validate a concrete field within a scope.
         */
        $validateFieldInScope (field, scope = '__global__') {
          return this.$validate(`${scope}.${field}`);
        },

        /**
         * Validate all fields.
         */
        $validateAll () {
          return this.$validate();
        },

        /**
         * Validate all fields without scope.
         */
        $validateAllWithoutScope () {
          return this.$validate('*');
        },

        /**
         * Clear errors
         */
        $clearErrors () {
          this.$validator.errors.clear();
        },

        /**
         * Get custom validation rules from the validator dictionary.
         *
         * @param field
         * @param rules
         * @returns {*}
         */
        $getCustomValidationMessages (field, rules = []) {
          const messages = this.$validator.dictionary.container[this.$translator.locale].custom[field];

          if (rules instanceof Array) {
            return rules.length ? pick(messages, rules) : messages;
          }

          return messages[rules];
        }
      },
    });
  },
});
