import { isNull, get } from 'lodash';

/**
 * Get pages path from the route or from window.location if the route is empty.
 * @param route
 * @returns {string}
 */
const getPagePath = (route) => {
  return isNull(route)
    ? window.location.pathname
    : route.path;
};

export default class DataProvider {
  /**
   * Calculate the category name from the route.
   * @param {(object|null)} route Vue Route object
   * @returns {string}
   */
  getPageCategoryFromRoute (route = null) {
    const splittedPath = getPagePath(route).split('/');
    return splittedPath.length > 1 ? splittedPath[1] : '';
  }

  /**
   * Calculate the page name from the route.
   * @param {(object|null)} route Vue Route object.
   * @returns {string}
   */
  getPageNameFromRoute (route = null) {
    const splittedPath = getPagePath(route).split('/');
    if (splittedPath.length < 2) {
      return '';
    }
    return splittedPath.slice(2,splittedPath.length).join('-');
  }

  /**
   * Get the first name.
   * @returns {string}
   */
  getFirstName () {
    return get(window, 'userConfig.firstName', '');
  }

  /**
   * Get the last name.
   * @returns {string}
   */
  getLastName () {
    return get(window, 'userConfig.lastName', '');
  }

  /**
   * Get the email.
   * @returns {string}
   */
  getEmail () {
    return get(window, 'userConfig.email', '');
  }

  /**
   * Get the currency.
   * @returns {string}
   */
  getCurrencyCode () {
    return get(window, 'userConfig.currency.code', '');
  }

  /**
   * Get the company name.
   * @returns {string}
   */
  getCompanyName () {
    return get(window, 'userConfig.company_name', '');
  }
}
