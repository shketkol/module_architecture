import EventData from './events/event-data';

export default class Tracker {

  constructor (services = []) {
    this.services = services;
  }

  /**
   * Set event data and send it to tracker.
   * @param {string} eventType
   * @param {(string|null)} eventName
   * @param {(objects|null)} route Route-like object (at least "path" property should be set).
   * @param {object} payload
   */
  track (eventType, eventName = null, route = null, payload = {}) {
    if (!this.isAnalyticsEnabled()) {
      return;
    }

    const eventData = new EventData(eventType, eventName, route, payload);
    this.services.forEach((service) => {
      service.setEvent(eventData);
      service.handleEvent();
    });
  }

  /**
   * Check if analytic events should be handled.
   * @returns {boolean}
   */
  isAnalyticsEnabled () {
    return window.config.analytics.enabled &&
      !window.userConfig.isAdmin
     ;
  }
}
