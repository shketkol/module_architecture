import BaseService from './base-service';

export default class TealiumService extends BaseService {
  /**
   * Verify if service is enabled.
   * @returns {boolean}
   */
  isEnabled () {
    return window.config.analytics.services.tealium.enabled &&
      window.config.analytics.services.tealium.spa_mode;
  }
}
