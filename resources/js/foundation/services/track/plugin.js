import { trackActionEvent, trackErrorEvent } from "./track";

export default {
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({
      /**
       * Available methods.
       */
      methods: {
        /**
         * Track user action.
         * @param {string} eventName
         * @param {object} payload
         */
        $trackAction (eventName, payload) {
          trackActionEvent(eventName, payload);
        },

        /**
         * Track error event.
         * @param message
         * @param {Array} reasons
         */
        $trackError (message, reasons = []) {
          trackErrorEvent(message, reasons);
        },
      },
    });
  },
};
