import BaseEvent from './base-event';
import DataProvider from '../data-provider';
import { getMapper } from '../mappers/tealium-mapper';

/**
 * Interval between attempts to send event-data (in milliseconds).
 * @type {number}
 */
const INTERVAL_PERIOD = 200;

export default class TealiumEvent extends BaseEvent {

  constructor (eventData) {
    super(eventData);
    this.dataProvider = new DataProvider();
    this.intervalId = null;
  }

  /** @inheritdoc */
  getMapper () {
    return getMapper(this.getMapType());
  }

  /** @inheritdoc */
  getBasicPayload () {
    return {
      event_name: this.getEventName(),
      page_name: this.dataProvider.getPageNameFromRoute(this.route),
      page_category: this.dataProvider.getPageCategoryFromRoute(this.route),
    };
  }

  /** @inheritdoc */
  getUserPayload () {
    return {
      first_name: this.dataProvider.getFirstName(),
      last_name: this.dataProvider.getLastName(),
      email: this.dataProvider.getEmail(),
      currency_code: this.dataProvider.getCurrencyCode(),
      company_name: this.dataProvider.getCompanyName(),
    };
  }

  /**
   * Initiate sending event-data when tracking script will be loaded.
   */
  send () {
    const payload = this.getPayload();
    if (this.attemptToSend(payload)) {
      return;
    }

    this.intervalId = setInterval(() => {
      if (this.attemptToSend(payload)) {
        this.clearInterval();
      }
    }, INTERVAL_PERIOD);
  }

  /**
   * Check if Tealium script was loaded and send event data.
   * @param payload
   * @returns {boolean}
   */
  attemptToSend (payload) {
    if (
      window.utag !== undefined &&
      window.utag.view instanceof Function
    ) {
      this.sendEvent(payload);
      return true;
    }
    return false;
  }

  /**
   * Clear interval of sending event-data attempts.
   */
  clearInterval () {
    clearInterval(this.intervalId);
  }

  /**
   * Send event-data.
   * @param payload
   * @abstract
   */
  sendEvent (payload) { // eslint-disable-line no-unused-vars
    throw new Error('sendEvent() must be implemented by the subclass');
  }
}
