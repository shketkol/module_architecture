import TealiumEvent from './tealium-event';

export default class TealiumEventView extends TealiumEvent {

  constructor (eventData) {
    super(eventData);
    this.eventName = 'screen_view';
  }

  /**
   * Send event-data.
   */
  sendEvent (payload) {
    window.utag.view(payload);
  }
}
