import BaseEventFactory from './base-event-factory';
import { EVENT_TYPE_VIEW, EVENT_TYPE_ACTION, EVENT_TYPE_ERROR } from './base-event';
import TealiumEventView from './tealium-event-view';
import TealiumEventLink from './tealium-event-link';
import TealiumEventError from './tealium-event-error';

export default class TealiumEventFactory extends BaseEventFactory{
  /**
   * @param {EventData} eventData
   * @return {TealiumEventView}
   */
  getEvent (eventData) {
    switch (eventData.eventType) {
      case EVENT_TYPE_VIEW:
        return new TealiumEventView(eventData);

      case EVENT_TYPE_ACTION:
        return new TealiumEventLink(eventData);

      case EVENT_TYPE_ERROR:
        return new TealiumEventError(eventData);

      default:
        throw new Error(`Tealium event "${eventData.eventType}" isn't supported`);
    }
  }
}
