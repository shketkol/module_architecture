import Tracker from './tracker';
import { EVENT_TYPE_VIEW, EVENT_TYPE_ACTION, EVENT_TYPE_ERROR } from './events/base-event';
import TealiumEventFactory from './events/tealium-event-factory';
import TealiumService from './services/tealium-service';

// Set tracker-oriented event factories and particular trackers.
const tealiumEventFactory = new TealiumEventFactory();
const tealiumService = new TealiumService(tealiumEventFactory);

const tracker = new Tracker([tealiumService]);

/**
 * Track view event.
 * @param {(object|null)} route
 */
export function trackViewEvent (route) {
  tracker.track(EVENT_TYPE_VIEW, null, route);
}

/**
 * Track action event.
 * @param eventName
 * @param {object} payload
 */
export function trackActionEvent (eventName, payload = {}) {
  tracker.track(EVENT_TYPE_ACTION, eventName, null, payload);
}

/**
 * Track error event.
 * @param message
 * @param {Array} reasons
 */
export function trackErrorEvent (message, reasons = []) {
  tracker.track(EVENT_TYPE_ERROR, null, null, {
    error_message: message,
    error_reason: reasons,
  });
}
