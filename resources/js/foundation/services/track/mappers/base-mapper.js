export default class BaseMapper {
  /**
   * @param {(string|null)} type
   */
  constructor (type = null) {
    this.type = type;
  }

  /**
   * Get mappings for payload items.
   * Returns the object where the keys are common keys to identify the specific data for some events
   * and the values are either analytic-based keys that are expected by analytic service
   * or the object that consists of 2 properties:
   *  - "key": analytic-based key;
   *  - "cb": callback function that should be applied on the value before sending.
   * @returns {object}
   */
  getDefaultMap () {
    return {};
  }

  /**
   * Get map.
   * @returns {{}}
   */
  getMap () {
    return this.getDefaultMap();
  }

  /**
   * Apply mapping and callbacks to the payload values.
   * @param rawPayload
   * @returns {object}
   */
  adjustPayload (rawPayload) {
    const map = this.getMap();
    let payload = {};
    for (const key in map) {
      if (rawPayload[key] === undefined) {
        continue;
      }

      let val = rawPayload[key];
      let adjustedKey = map[key];
      if (typeof map[key].cb === 'function') {
        val = map[key].cb(val);
        adjustedKey = map[key].key;
      }

      payload[adjustedKey] = val;
    }
    return payload;
  }
}
