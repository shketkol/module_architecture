import BaseMapper from './base-mapper';
import {sha256} from "js-sha256";

export const MAP_TYPE_LINK = 'link';

class TealiumMapper extends BaseMapper {

  /** @inheritdoc */
  getDefaultMap () {
    return {
      event_name: 'tealium_event',
      page_category: 'page_category',
      page_name: 'page_name',
      first_name: {
        key: 'hashed_fn',
        cb: getHash
      },
      last_name: {
        key: 'hashed_ln',
        cb: getHash
      },
      email: {
        key: 'hashed_email',
        cb: getHash
      },
      currency_code: 'currency_code',
      company_name: 'company_name',
      campaign_id: 'campaign_id',
      total_budget: 'total_budget',
      discounted_cost: 'discounted_cost',
      payment_method: 'payment_method',
      error_message: 'error_message',
      error_reason: 'error_reason',
    };
  }

  /** @inheritdoc */
  getMap () {
    const map = this.getDefaultMap();
    if (this.type === MAP_TYPE_LINK) {
      map.event_name = 'event_name';
    }
    return map;
  }
}

/**
 * Get the hash of the value.
 * @param value
 * @returns {*}
 */
function getHash (value) {
  return sha256(value);
}

/**
 * Get mapper for specified event type.
 * @param {(string|null)} type
 * @returns {TealiumMapper}
 */
export function getMapper (type) {
  return new TealiumMapper(type);
}
