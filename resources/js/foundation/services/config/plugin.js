import Vue from 'vue';

Vue.use({
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({
      /**
       * Computed properties.
       */
      computed: {
        /**
         * Config helper.
         */
        $config () {
          return window.config;
        },
      },
    });
  },
});
