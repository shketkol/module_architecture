import VueRouter from 'vue-router';
import {trackViewEvent} from "Foundation/services/track/track";

/**
 * Provide router with predefined behaviour.
 * @param {object} args
 * @returns {VueRouter}
 */
export function getRouter (args = {}) {
  const router = new VueRouter({
    ...args,
    scrollBehavior (to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    }
  });

  router.afterEach((to) => {
    trackViewEvent(to);
  });
  return router
}
