import { forEach } from 'lodash';

export default class Router {
  /**
   * Constructor.
   *
   * @param routes
   */
  constructor (routes) {
    this.routes = this.parseRoutes(routes);
    this.baseUrl = routes.baseUrl;
  }

  /**
   * Parse JSON file with routes to JS object.
   *
   * @param routes
   * @returns {any}
   */
  parseRoutes (routes) {
    return JSON.parse(routes.routes);
  }

  /**
   * Create a URL for the route passed by name.
   *
   * @param name
   * @param params
   * @param absolute
   * @returns {*}
   */
  route (name, params = {}, absolute = false) {
    const route = this.getByName(name);

    if (!route) {
      window.console.error(`Routes Error: the ${name} alias does not exist.`);

      return null;
    }

    return this.toRoute(route, params, absolute);
  }

  /**
   * Find route in collection by its name.
   *
   * @param name
   * @returns {*}
   */
  getByName (name) {
    const route = this.routes[name];

    if (typeof route === 'undefined' ||
      Object.prototype.hasOwnProperty.call(route, 'domain') === false ||
      !this.baseUrl) {
      return null;
    }

    return route;
  }

  /**
   * Create a URL for the specified route.
   *
   * @param route
   * @param params
   * @param absolute
   * @returns {string}
   */
  toRoute (route, params = {}, absolute = false) {
    const domain = route.domain || this.baseUrl;
    const url = (absolute ? '//' + domain : '/') + route.uri;

    return this.addParams(url, params) + this.addGetParams(params);
  }

  /**
   * Substitute route parameters.
   *
   * @param url
   * @param params
   * @returns {*}
   */
  addParams (url, params = '') {
    return url.replace(/\{([^}]+)\}/gi, tag => {
      const key = tag.replace(/\{|\}/gi, '');

      if (typeof params[key] === 'undefined') {
        // For optional routes parameters.
        if (key.indexOf('?') !== -1) {
          const optionalKey = key.replace('?', '');

          return (typeof params[optionalKey] === 'undefined' ? '' : params[optionalKey]);
        }
        window.console.error(`Error: ${key} key is required for route ${name}`);
      }

      const param = encodeURI(params[key]);

      delete params[key];

      return param;
    }).replace(/\/$/g, '');
  }

  /**
   * Add GET parameters part for the URL.
   *
   * @param params
   * @returns {string}
   */
  addGetParams (params = {}) {
    // No GET params.
    if (!Object.keys(params).length) {
      return '';
    }

    const parts = [];

    forEach(params, (value, name) => {
      if (value === null || value === undefined) {
        return;
      }

      parts.push(`${name}=${value}`);
    });

    return '?' + parts.join('&');
  }
}
