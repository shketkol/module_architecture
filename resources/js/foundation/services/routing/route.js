import routes from 'Static/routes.json';
import Router from './router';

const router = new Router(routes);

export default (name, params = {}, absolute = false) => {
  return router.route(name, params, absolute);
};
