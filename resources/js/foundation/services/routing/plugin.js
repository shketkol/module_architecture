import route from './route';

export default {
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({
      /**
       * Available methods.
       */
      methods: {
        /**
         * Get route url with alias.
         * @param name
         * @param params
         * @param absolute
         */
        $getRoute (name, params, absolute) {
          return route(name, params, absolute);
        },

        /**
         * Redirect to the specified href.
         *
         * @param href
         */
        $redirect (href) {
          window.location.href = href;
        },

        /**
         * Redirect to the specified route.
         *
         * @param name
         * @param params
         * @param absolute
         */
        $redirectToRoute(name, params, absolute) {
          this.$redirect(this.$getRoute(name, params, absolute));
        },

        /**
         * Push FE route
         *
         * @param route
         */
        $pushRoute(route) {
          if (!this.$router || this.checkDiffRoutes(route)) {
            return Promise.resolve();
          }

          return this.$router.push(route).catch((error) => {
            if (error === undefined || error) {
              return;
            }
            throw error;
          });
        },

        /**
         * Check diff between 2 routes
         *
         * @param route
         * @return boolean
         */
        checkDiffRoutes (route) {
          if (this.$router.currentRoute.name !== route.name) {
            return false;
          }

          //Check each params
          for (let item in this.$router.currentRoute.params) {
            if (this.$router.currentRoute.params[item] !== route.params[item]) {
              return false;
            }
          }

          return true;
        }
      },
    });
  },
};
