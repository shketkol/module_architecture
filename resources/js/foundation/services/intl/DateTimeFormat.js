import Config from 'Foundation/services/config/index';
import moment from 'moment';

export default class DateTimeFormat {
  /**
   * DateTimeFormat constructor.
   *
   * @param locale
   */
  constructor (locale) {
    this.locale = locale;
  }

  /**
   * Format date for the specified locale.
   * Set "options.useTimezone = true" to format date with timezone offset ('2020-03-30T23:59:59.000-0500') without shifting
   *
   * @param date
   * @param options
   * @returns {*}
   */
  formatDate (date, options = {
    useTimezone: true,
    clientTimezone: false,
    format: Config.date.format.js
  }) {

    if (options.clientTimezone === undefined) {
      options.clientTimezone = false;
    }

    if (options.useTimezone === undefined) {
      options.useTimezone = true;
    }

    if (options.format === undefined) {
      options.format = Config.date.format.js;
    }

    if (!moment(date, Config.date.format.js_parse).isValid()) {
      return date;
    }

    if (options.useTimezone || date instanceof Date) {
      return moment.parseZone(date).format(options.format);
    }

    if (options.clientTimezone) {
      return moment(date).format(options.format);
    }

    return moment.utc(date).format(options.format);
  }
}
