export default class NumberFormat {
  /**
   * NumberFormat constructor.
   *
   * @param locale
   */
  constructor (locale) {
    this.locale = locale;
  }

  /**
   * Format integer for the specified locale.
   *
   * @param value
   * @returns {*}
   */
  formatInt (value) {
    return new Intl.NumberFormat(this.locale, { maximumFractionDigits: 0 }).format(value);
  }

  /**
   * Format decimal for the specified locale.
   *
   * @param value
   * @param formatParams
   * @returns {*}
   */
  formatDecimal (value, formatParams) {
    return new Intl.NumberFormat(this.locale, formatParams).format(value);
  }

  /**
   * Format percent for the specified locale.
   *
   * @param value
   * @param displayFraction
   * @returns {*}
   */
  formatPercent (value, displayFraction = false) {
    return new Intl.NumberFormat(this.locale, {
      style: 'percent',
      minimumFractionDigits: displayFraction ? 2 : 0,
    }).format(value);
  }

  /**
   * Format currency for the specified locale.
   *
   * @param value
   * @param currency
   * @param currencyDisplay
   * @returns {*}
   */
  formatCurrency (value, currency, currencyDisplay) {
    return new Intl.NumberFormat(this.locale, {
      style: 'currency',
      currency,
      currencyDisplay,
    }).format(value);
  }
}
