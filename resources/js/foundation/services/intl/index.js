import DateTimeFormat from './DateTimeFormat';
import NumberFormat from './NumberFormat';
import Vue from 'vue';
import { ceil } from 'Foundation/helpers/number';

const locale = 'en-US';

const numberFormat = new NumberFormat(locale);
const dateTimeFormat = new DateTimeFormat(locale);

Vue.use({
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({
      /**
       * Methods.
       */
      methods: {
        /**
         * Format integer.
         *
         * @param value
         * @returns {*}
         */
        $formatInt (value) {
          return numberFormat.formatInt(value);
        },

        /**
         * Format decimal.
         *
         * @param value
         * @param formatParams
         * @returns {*}
         */
        $formatDecimal (value, formatParams = {}) {
          return numberFormat.formatDecimal(value, formatParams);
        },

        /**
         * Format percent.
         *
         * @param value
         * @param displayFraction
         * @returns {*}
         */
        $formatPercent (value, displayFraction = false) {
          return numberFormat.formatPercent(value, displayFraction);
        },

        /**
         * Format currency.
         *
         * @param value
         * @param currency
         * @param currencyDisplay
         * @returns {*}
         */
        $formatCurrency (value, currency, currencyDisplay) {
          return numberFormat.formatCurrency(value, currency, currencyDisplay);
        },

        /**
         * Format date.
         *
         * @param date
         * @param options
         * @param empty
         * @returns {*}
         */
        $formatDate (
          date,
          options = { useTimezone: true, clientTimezone: false },
          empty = ''
        ) {
          if (!date) {
            return empty;
          }
          return dateTimeFormat.formatDate(date, options);
        },

        /**
         * Format number with prefixes.
         * Is used to add K or M prefixes.
         * e.g. 1 234 -> 1K.
         * e.g. 1 234 567 -> 1M.
         * @param value
         * @returns {*}
         */
        $formatNumberPrefix (value) {
          const prefixes = [
            {
              sign: 'M',
              value: Math.pow(10, 6),
            },
            {
              sign: 'K',
              value: Math.pow(10, 3),
            }
          ];

          for (let prefix of prefixes) {
            if (value >= prefix.value) {
              return `${Math.floor(value / prefix.value).toFixed(0)}${prefix.sign}`;
            }
          }

          return value;
        },

        /**
         * Round currency value.
         *
         * @param value
         * @returns {*}
         */
        $roundCurrency (value) {
          return ceil(value, this.$user.currency.precision);
        },
      },
    });
  },
});
