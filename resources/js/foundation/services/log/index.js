import Vue from 'vue';
import Log from 'Foundation/services/log/log';
import {Severity} from "@sentry/types/dist/severity";

const log = new Log();

Vue.use({
  /**
   * Install plugin.
   * @param Vue
   * @returns {*}
   */
  install (Vue) {
    Vue.mixin({

      /**
       * Available methods.
       */
      methods: {
        /**
         * Save sentry log.
         *
         * @param message
         * @param data
         * @param level
         */
        $log (message, data, level = Severity.Error) {
          log.set(message, data, level);
        },

        /**
         * Save info sentry log.
         *
         * @param message
         * @param data
         */
        $logInfo (message, data) {
          log.set(message, data, Severity.Info);
        },

        /**
         * Save debug sentry log.
         *
         * @param message
         * @param data
         */
        $logDebug (message, data) {
          log.set(message, data, Severity.Debug);
        },
      },
    });
  },
});
