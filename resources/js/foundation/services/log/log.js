import * as Sentry from '@sentry/browser';

export default class Log {
  set (message, data, level) {
    Sentry.withScope(scope => {
      scope.setExtra('data', data);
      scope.setLevel(level);
      Sentry.captureException(new Error(message));
    });
  }
}
