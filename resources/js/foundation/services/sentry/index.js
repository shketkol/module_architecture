import Vue from 'vue'
import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

if (window.config.services.sentry.is_enabled) {
  Sentry.init({
    dsn: window.config.services.sentry.dsn,
    integrations: [new Integrations.Vue({Vue, attachProps: true, logErrors: true})],
    environment: window.config.general.env,
    release: String(window.config.general.release_version),
    beforeSend(event, hint) {
      if (hint.originalException !== undefined &&
        hint.originalException.handled !== undefined &&
        hint.originalException.handled) {
        return null;
      }

      return event;
    },
  });

  if (window.userConfig && window.userConfig.identifier) {
    Sentry.configureScope(function(scope) {
      scope.setUser({id: window.userConfig.identifier});
    });
  }
}
