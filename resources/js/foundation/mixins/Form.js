export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Allow send form.
     */
    onSubmit (pending) {
      if (pending) {
        return;
      }

      return this.action();
    }
  }
}
