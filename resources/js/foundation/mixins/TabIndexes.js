import { setTabIndexes, removeTabIndexesFromSubNodes } from 'Foundation/helpers/tab-indexes';

export default {
  /**
   * Mounted hook.
   */
  mounted () {
    this.removeUnusedTabindexes();
    this.adjustTabIndexes();
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Set tabindexes for all "focusable" elements within the component.
     */
    adjustTabIndexes () {
      setTabIndexes(
        this.$el.querySelectorAll('a, input, select, textarea, button'),
        this.getComponentTabIndex()
      );
    },

    /**
     * Remove tabindexes from elements that shouldn't be focused.
     */
    removeUnusedTabindexes () {
      removeTabIndexesFromSubNodes(this.$el, 'li.el-menu-item');
    },

    /**
     * Get component's tabindex. Should be overridden by the component.
     * @returns {number}
     */
    getComponentTabIndex () {
      return 0;
    }
  },
}
