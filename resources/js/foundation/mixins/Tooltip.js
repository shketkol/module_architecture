export default {
  /**
   * Props
   */
  props: {
    /**
     * Tooltip.
     */
    tooltip: {
      type: String,
      default: '',
    },
  },

  /**
   * Computed props.
   */
  computed: {
    /**
     * Check whether tooltip is set.
     * @return {String}
     */
    hasTooltip () {
      return !!this.tooltip;
    },
  },
}
