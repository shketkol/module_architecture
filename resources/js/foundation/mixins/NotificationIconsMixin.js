import IconPlay from "Foundation/components/icons/IconPlay";
import IconCreditCard from "Foundation/components/icons/IconCreditCard";
import IconUser from "Foundation/components/icons/IconUser";
import IconReport from "Foundation/components/icons/IconReport";
import IconMegaphone from "Foundation/components/icons/IconMegaphone";

export default {
  /**
   * Data.
   */
  data() {
    return {
      categoryIconComponents: {
        transaction: IconCreditCard,
        account: IconUser,
        report: IconReport,
        campaign: IconMegaphone,
        ad: IconPlay,
      },
    };
  },

  /**
   * Computed props.
   */
  computed: {
    /**
     * Category icon component.
     */
    categoryIconComponent() {
      return this.categoryIconComponents[this.category] || null;
    },
  },
}
