import MediaInfoFactory from 'mediainfo.js';

export default {
  /**
   * Methods.
   */
  methods: {
    /**
     * Get media info from file.
     * @param {File} file
     * @returns {Promise<any>}
     */
    getMediaInfo (file) {
      return new Promise((resolve, reject) => {
        MediaInfoFactory({ format: 'object' }, (mediainfo) => {
          const getSize = () => file.size;

          const readChunk = (chunkSize, offset) =>
            new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.onload = (event) => {
                if (event.target.error) {
                  reject(event.target.error)
                }
                resolve(new Uint8Array(event.target.result));
              };
              reader.readAsArrayBuffer(file.slice(offset, offset + chunkSize));
            });

          mediainfo
            .analyzeData(getSize, readChunk)
            .then(({media: {track}}) => {
              resolve(track);
            })
            .catch((error) => {
              reject(error);
            });
        });
      });
    },
  },
}
