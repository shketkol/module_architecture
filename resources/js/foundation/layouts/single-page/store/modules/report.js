import {updateField} from 'vuex-map-fields';

// TODO Change this solution to the event pattern
/**
 * Types
 */
const types = {
  RELOAD_TABLE: 'reloadTable',
};

/**
 * State
 * @type {{}}
 */
const state = {
  reloadTable: false
}

/**
 * Mutations
 * @type {{}}
 */
const mutations = {
  [types.RELOAD_TABLE]: (state) => {
    state.reloadTable = true;
  },

  /**
   * Fields mapping.
   */
  updateField,
};

export default {
  namespaced: true,
  types,
  state,
  mutations,
};
