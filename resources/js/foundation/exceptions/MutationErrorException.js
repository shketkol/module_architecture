/**
 * Mutation Error Exception
 */
export default class MutationErrorException extends Error {
  constructor(message) {
    super(message);
    this.name = "MutationErrorException";
  }
}
