import './element';
import './utils';
import './validation';
import './filters';
import './vue-the-mask';
import './vue-scrollto';
import './filters';
import './directives';
import 'es6-promise/auto';
import assign from 'es6-object-assign';
import axios from 'axios';
import moment from 'moment';
import Vue from 'vue';

window.axios = axios;
window.moment = moment;
window.Vue = Vue;
assign.polyfill();

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

const token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  window.console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
