import { Validator } from 'vee-validate';
import translator from 'Foundation/services/translations/locale';

/**
 * Validates that file is an image
 */
Validator.extend('image_file', {
  /**
   * Get message
   * @returns {string}
   */
  getMessage: () => {
    return translator.get('faq.messages.image.validation.image');
  },
  /**
   * Validate
   * @param value
   * @returns {number}
   */
  validate: (value) => {
    const formats = ['image/png', 'image/jpeg'];
    return ~formats.indexOf(value.raw.type);
  }
});

/**
 * Image ratio validator
 */
Validator.extend('image_ratio', {
  /**
   * Get message
   * @param field
   * @param params
   * @returns {*}
   */
  getMessage: (field, params) => {
    return translator.get('faq.messages.image.validation.ratio', {
      width: params[0],
      height: params[1],
    });
  },

  /**
   * Validate
   * @param value
   * @param params
   * @returns {boolean}
   */
  validate: (value, params) => {
    if (typeof  value.width === 'undefined' && typeof  value.height === 'undefined') {
      return true;
    }
    return value.width === Number(params[0]) && value.height === Number(params[1]);
  }
});
