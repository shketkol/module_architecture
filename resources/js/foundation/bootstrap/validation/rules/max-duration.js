import moment from 'moment';
import translator from 'Foundation/services/translations/locale';
import { Validator } from 'vee-validate';

Validator.extend('max_duration', {
  /**
   * Get validation message.
   *
   * @param field
   * @param { value, type }
   */
  getMessage(field, { value, type }) {
    let processedType = type;
    if (type === 'month') {
      processedType = translator.get(value > 1 ? 'validation.months' : 'validation.month');
    }

    return translator.get('validation.max_duration', {
      attribute: translator.get('labels.end_date'),
      value,
      type: processedType,
    });
  },

  /**
   * Validate an attribute.
   *
   * @param value
   * @param params
   */
  validate(value, params) {
    const start = moment(value[0]);
    const end = moment(value[1]);
    const diff = end.diff(start, params.type);

    return diff < params.value;
  },
});
