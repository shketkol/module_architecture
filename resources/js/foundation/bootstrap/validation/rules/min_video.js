import { get } from 'lodash';
import translator from 'Foundation/services/translations/locale';
import { Validator } from 'vee-validate';

const PARAMS_KEY_MIN = 0;
const PARAMS_KEY_DIMENSION = 'dimension';
const PARAMS_KEY_FORMAT = 'format';
const PARAMS_KEY_SYSTEM = 'system';

const DIVIDER_DEC_KILO = 1000;
const DIVIDER_DEC_MEGA = 1000000;

const DIVIDER_BIN_KILO = 1024;
const DIVIDER_BIN_MEGA = 1048576;

const SYSTEM_DECIMAL = 'decimal';
const SYSTEM_BINARY = 'binary';

/**
 * Get "dimension" parameter.
 *
 * @param {Array} params
 * @returns {string}
 */
const getDimension = (params) => {
  const dimension = get(params, PARAMS_KEY_DIMENSION);
  const format = getFormat(params);
  if (!format) {
    return dimension;
  }
  return `${format}${dimension}`;
};

/**
 * Get "format" parameter.
 *
 * @param {Array} params
 * @returns {(string|null)}
 */
const getFormat = (params) => {
  return get(params, PARAMS_KEY_FORMAT);
};

/**
 * Get "system" parameter.
 *
 * @param {Array} params
 * @returns {(string)}
 */
const getSystem = (params) => {
  return get(params, PARAMS_KEY_SYSTEM, SYSTEM_DECIMAL);
};

/**
 * @param {string} format
 * @param {string} system
 * @returns {number}
 */
const getDivider = (format, system) => {
  switch (format) {
    case 'k':
      return system === SYSTEM_BINARY ? DIVIDER_BIN_KILO : DIVIDER_DEC_KILO;
    case 'm':
      return system === SYSTEM_BINARY ? DIVIDER_BIN_MEGA : DIVIDER_DEC_MEGA;
  }
  throw new Error('Wrong format');
};

/**
 * Get converted min value.
 *
 * @param {Array} params
 * @returns {number}
 */
const getConvertedMinValue = (params) => {
  const min = get(params, PARAMS_KEY_MIN);
  const format = getFormat(params);
  const system = getSystem(params);
  if (['k', 'm'].includes(format)) {
    return Math.round(min / getDivider(format, system));
  }
  return min;
};

Validator.extend('min_video', {
  /**
   * Validate an attribute.
   *
   * @param {number} value
   * @param {Array} params
   * @returns {boolean}
   */
  validate (value, params) {
    const min = get(params, PARAMS_KEY_MIN);
    return min <= value;
  },

  /**
   * Get validation message.
   * @param {string} field
   * @param {Array} params
   * @returns {string}
   */
  getMessage (field, params) {
    return translator.get('validation.min_video', {
      attribute: field,
      min: getConvertedMinValue(params),
      dimension: getDimension(params),
    });
  },
});
