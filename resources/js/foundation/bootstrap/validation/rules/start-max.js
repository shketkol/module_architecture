import moment from 'moment';
import translator from 'Foundation/services/translations/locale';
import { Validator } from 'vee-validate';

Validator.extend('start_max', {
  /**
   * Get validation message.
   *
   */
  getMessage() {
    return translator.get('validation.custom')['date.start']['before'];
  },

  /**
   * Validate an attribute.
   *
   * @param value
   * @param params
   */
  validate(value, params) {
    const start = moment(value[0]);
    const diff = start.diff(moment(), params.type);

    return diff < params.value;
  },
});
