import { get } from 'lodash';
import translator from 'Foundation/services/translations/locale';
import { Validator } from 'vee-validate';

const PARAMS_INDEX_ALLOWED = 0;
const PARAMS_INDEX_DIMENSION = 1;

/**
 * Get "allowed" parameter.
 * @param {Array} params
 * @returns {Array}
 */
const getAllowed = (params) => {
  return Array.isArray(params[PARAMS_INDEX_ALLOWED]) ? params[PARAMS_INDEX_ALLOWED] : params;
};

/**
 * Get "dimension" parameter.
 * @param {Array} params
 * @returns {string}
 */
const getDimension = (params) => {
  return Array.isArray(params[PARAMS_INDEX_ALLOWED]) ? get(params, PARAMS_INDEX_DIMENSION, '') : '';
};

Validator.extend('included', {
  /**
   * Validate an attribute.
   *
   * @param {number|string} value
   * @param {Array} params
   * @returns {boolean}
   */
  validate (value, params) {
    const allowed = getAllowed(params);

    return allowed.includes(value);
  },

  /**
   * Get validation message.
   * @param {string} field
   * @param {Array} params
   * @returns {string}
   */
  getMessage (field, params) {
    const allowed = getAllowed(params);
    const allowedTranslated = [];
    allowed.forEach(item => {
      const translationKey = 'validation.creative.included_params.' + item;
      allowedTranslated.push(
        translator.has(translationKey)
          ? translator.get(translationKey)
          : item
      );
    });

    const dimension = getDimension(params);
    const translation = dimension ? 'validation.included_with_dimension' : 'validation.included';

    return translator.get(translation, {
      attribute: field,
      values: allowedTranslated.join(', '),
      dimension: dimension,
    });
  },
});
