import { Validator } from 'vee-validate';
import translator from 'Foundation/services/translations/locale';
import NumberFormat from 'Foundation/services/intl/NumberFormat';

const numberFormat = new NumberFormat('en-US');
/**
 * Validator for formatted values such as money or decimals with commas
 */
Validator.extend('formatted_min_value', {
  /**
   * Get message
   * @param field
   * @param params
   * @returns {string}
   */
  getMessage: (field, params) => {
    return translator.get('validation.formatted_min_value', {
      field: field,
      value: numberFormat.formatDecimal(params[0]),
    });
  },
  /**
   * Validate value
   * @param value
   * @param params
   * @returns {boolean}
   */
  validate: (value, params) => {
    return Number(value.replace(/[^0-9.-]+/g, '')) >= params[0];
  }
});

/**
 * Validator for formatted max value
 */
Validator.extend('formatted_max_value', {
  /**
   * Get validation message
   * @param field
   * @param params
   * @returns {string}
   */
  getMessage: (field, params) => {
    return translator.get('validation.formatted_max_value', {
      field: field,
      value: numberFormat.formatDecimal(params[0]),
    });
  },
  /**
   * Validate value
   * @param value
   * @param params
   * @returns {boolean}
   */
  validate: (value, params) => {
    return Number(value.replace(/[^0-9.-]+/g, '')) <= params[0];
  }
});

/**
 * Validator for decimals
 */
Validator.extend('formatted_decimal', {
  /**
   * Get message
   * @param field
   * @returns {string}
   */
  getMessage: (field) => {
    return translator.get('validation.formatted_decimal', {
      field: field,
    });
  },
  /**
   * Validate value
   * @param value
   * @returns {boolean}
   */
  validate: (value) => {
    return !isNaN(Number(value.replace(/[^0-9.-]+/g, '')));
  }
});

