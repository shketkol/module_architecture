import translator from 'Foundation/services/translations/locale';
import filesize from 'filesize';
import { Validator } from 'vee-validate';

/**
 * Format params.
 * @param {(Array|number|object)} params
 * @returns {*}
 */
function formatParams (params) {
  if (Array.isArray(params) && (params.length >= 2)) {
    return params[0];
  }
  return params;
}

Validator.extend('max_filesize', {
  /**
   * Get validation message.
   *
   * @param {string} field
   * @param {(Array|number|object)} params
   * @returns {string}
   */
  getMessage (field, params) {
    return translator.get('validation.max_filesize_creative', {
      format: filesize(formatParams(params))
    });
  },

  /**
   * Validate an attribute.
   *
   * @param {(number|object)} max
   * @param {(Array|number|object)} params
   * @returns {boolean}
   */
  validate (max, params) {
    let maxValue = formatParams(params);

    if (typeof max === 'number') {
      return maxValue >= max;
    }

    return maxValue > max.size;
  },
});
