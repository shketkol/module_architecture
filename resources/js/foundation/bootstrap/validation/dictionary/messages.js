import translator from 'Foundation/services/translations/locale';

export default {
  required (attribute) {
    return translator.get('validation.required', { attribute });
  },
  max (attribute, [max]) {
    return translator.get('validation.max.string', { attribute, max });
  },
};
