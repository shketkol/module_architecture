import custom from './custom';
import messages from './messages';
import translator from 'Foundation/services/translations/locale';
import { Validator } from 'vee-validate';

Validator.localize(translator.locale, {
  custom,
  messages,
});
