import Vue from 'vue';
import truncate from 'lodash/truncate';
import escape from 'lodash/escape';

const filters = {
  install(Vue) {
    /**
     * Strip html tags
     */
    Vue.filter('stripTags', (value) => {
      return escape(value.replace(/<[^>]*>?/gm, ' '));
    });

    /**
     * Truncate string and don't break words
     */
    Vue.filter('truncate', (value, length) => {
      return truncate(value, {
        length: length,
        separator: ' ',
      });
    });

    Vue.filter('firstLetter', value => {
      return value && value[0] || '';
    });
  }
};

Vue.use(filters);
