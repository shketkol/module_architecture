<?php

return [
    'images' => [
        'quality' => 90,

        'default_extension' => 'jpg',

        'variants' => [
            'medium' => [
                'width' => 960
            ],
            'small'  => [
                'width' => 200
            ]
        ]
    ],

    'videos' => [

        'driver' => env('VIDEO_DRIVER', 'ffmpeg'),

        'ffmpeg' => [
            'binaries' => env('FFMPEG_BINARIES', '/usr/bin/ffmpeg')
        ],

        'ffprobe' => [
            'binaries' => env('FFPROBE_BINARIES', '/usr/bin/ffprobe')
        ],

        'frame_second' => 3,

        'web' => [
            'default_size' => 720,
            'default_type' => 'video/mp4'
        ]
    ]
];
