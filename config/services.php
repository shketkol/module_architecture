<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain'   => env('MAILGUN_DOMAIN'),
        'secret'   => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key'             => env('AWS_SES_ACCESS_KEY_ID'),
        'secret'          => env('AWS_SES_SECRET_ACCESS_KEY'),
        'region'          => env('AWS_SES_DEFAULT_REGION', 'us-east-1'),
        'headers_enabled' => env('AWS_SES_HEADERS_ENABLED', true),
        'headers'         => env('AWS_SES_HEADERS'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model'   => \Modules\User\Models\User::class,
        'key'     => env('STRIPE_KEY'),
        'secret'  => env('STRIPE_SECRET'),
        'webhook' => [
            'secret'    => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'sentry' => [
        'dsn'        => env('SENTRY_DSN', 'https://b671d256796d49f39ccf2b62cd940dd0@sentry.io/1543045'),
        'is_enabled' => env('SENTRY_FORCE_ENABLED', false) || \App\Helpers\EnvironmentHelper::isProdEnv(),
    ],
];
