<?php

use Illuminate\Support\Arr;

$emails = [
    'stage'   => 'danadstest@gmail.com',
    'local'   => 'danadstest@gmail.com',
    'testing' => 'danadstest@gmail.com',
    'default' => 'AdResearch@hulu.com',
];

$env   = env('APP_ENV', 'local');
$email = Arr::get($emails, $env, $emails['default']);

return [
    'env'                      => $env,
    'company_name'             => env('COMPANY_NAME', 'Hulu'),
    'company_full_name'        => env('COMPANY_FULL_NAME', 'Hulu Ad Manager'),
    'admin_support_email'      => env('ADMIN_SUPPORT_EMAIL', 'danadstest@gmail.com'),
    'company_support_email'    => env('COMPANY_SUPPORT_EMAIL', $email),
    'internal_support_email'   => env('INTERNAL_SUPPORT_EMAIL', 'support@admanager.hulu.com'),
    'company_support_phone'    => env('COMPANY_SUPPORT_PHONE', '(XXX) XXX-XXXX'),
    'company_admin_emails'     => env('COMPANY_ADMIN_EMAIL'),
    'company_business_address' => env('COMPANY_BUSINESS_ADDRESS', 'Hulu, LLC. 2500 Broadway, 2nd Floor, Santa&nbsp;Monica,&nbsp;CA&nbsp;90404'),
    'links'                    => [
        'terms_of_use'             => env('TERMS_OF_USE_LINK', 'https://home.admanager.hulu.com/terms/'),
        'advertising_policies'     => env('ADVERTISING_POLICIES_LINK', 'https://home.admanager.hulu.com/terms/#3-ads-and-campaigns'),
        'beta_terms_of_use'        => env('BETA_TERMS_OF_USE_LINK', 'https://home.admanager.hulu.com/hulu-ad-manager-beta-terms/'),
        'privacy_policy'           => env('PRIVACY_POLICY_LINK', 'https://secure.hulu.com/privacy'),
        'technical_specifications' => env('TECHNICAL_SPECIFICATIONS_LINK', 'https://faq.admanager.hulu.com/Content/Ads/ad-prep-tech-specs.htm'),
        'how_it_works'             => env('HOW_IT_WORKS_LINK', 'https://home.admanager.hulu.com/how-it-works/'),
        'success_stories'          => env('SUCCESS_STORIES_LINK', 'https://home.admanager.hulu.com/stories/'),
        'resources'                => env('RESOURCES_LINK', 'https://home.admanager.hulu.com/resources/'),
        'insights'                 => env('INSIGHTS_LINK', 'https://home.admanager.hulu.com/insights/'),
        'creative_hub'             => env('CREATIVE_HUB_LINK', 'https://home.admanager.hulu.com/creative-hub/'),
        'faq'                      => [
            'unauthorized'           => env('FAQ_LINK', 'https://home.admanager.hulu.com/faq/'),
            'authorized'             => env('FAQ_AUTHORIZED_LINK', 'https://faq.admanager.hulu.com/'),
            'special_ads'            => env('FAQ_SPECIAL_ADS_LINK', 'https://faq.admanager.hulu.com/Content/Ads/ads-special.htm'),
            'special_ads_categories' => env('FAQ_SPECIAL_ADS_CATEGORIES_LINK', 'https://faq.admanager.hulu.com/Content/Ads/ads-special.htm#Special'),
            'faq_search_suffix'      => env('FAQ_HULU_SEARCH_LINK', 'Content/Search.htm'),
            'ad_approval'            => env('FAQ_AD_APPROVAL_LINK', 'https://faq.admanager.hulu.com/Content/Ads/ad-prep-approval.htm'),
            'duplicate'              => env('FAQ_DUPLICATE_INK', 'https://faq.admanager.hulu.com/Content/Campaigns/cmp-cre-duplicate.htm'),
        ],
        'ad_manager'               => [
            'href'  => env('AD_MANAGER_LINK', 'http://www.hulu.com/advertising/Ad'),
            'title' => env('AD_MANAGER_TITLE', 'www.hulu.com/advertising/AdManager'),
        ],
        'instagram'                => 'https://instagram.com/hulu/',
        'facebook'                 => 'https://www.facebook.com/hulu',
        'twitter'                  => 'https://twitter.com/hulu',
        'help'                     => 'https://help.hulu.com/',
    ],
    'thirdparty_links'         => [
        'download' => [
            'chrome'  => 'https://www.google.com/chrome/',
            'firefox' => 'https://www.mozilla.org/en-US/firefox/new/',
        ],
    ],
    'new_local_storage_before' => env('RENEW_LOCAL_STORAGE_BEFORE', false),
    'demo'                     => [
        'enabled' => env('DEMO_ENABLED', $env === 'demo'),
    ],
    'launch_date'              => '2020-04-15',
    'release_version'          => env('RELEASE_VERSION', 1.6),
    'time_widget_interval'     => env('TIME_WIDGET_INTERVAL', 180000),
];
