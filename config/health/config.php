<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Health Monitor Title
    |--------------------------------------------------------------------------
    |
    | This is the title of the health check panel, that shows up at the top-left
    | corner of the window. Feel free to edit this value to suit your needs.
    |
    */
    'title' => 'Health Check',

    /*
    |--------------------------------------------------------------------------
    | Health Check Access Tokens Array
    |--------------------------------------------------------------------------
    |
    | Contain array of heath-check access keys
    |
    |
    */
    'tokens' => [
        env('HEALTH_CHECK_EXTERNAL_TOKEN', 'As8z6rnPbJbuGOGZP0aIRbA6F2qfYMFH'), //used by HAAPI
        env('HEALTH_CHECK_INTERNAL_TOKEN', 'g531rbmBYUoYv6R03qk7yZaO7skmEsBZ')  //used by internal services
    ],

    /*
    |--------------------------------------------------------------------------
    | Health Monitor Resources
    |--------------------------------------------------------------------------
    |
    | Below is the list of resources the health checker will look into.
    | And the path to where the resources yaml files are located.
    |
    */
    'resources' => [

        /*
        |--------------------------------------------------------------------------
        | Health Monitor Resources Path
        |--------------------------------------------------------------------------
        |
        | This value determines the path to where the resources yaml files are
        | located. By default, all resources are in config/health/resources
        |
        */
        'path' => config_path('health/resources'),

        /*
        |--------------------------------------------------------------------------
        | Health Monitor Enabled Resources
        |--------------------------------------------------------------------------
        |
        | Below is the list of resources currently enabled for your laravel application.
        | The default enabled resources are picked for the common use-case. However,
        | you are free to uncomment certain resource or add your own as you wish.
        |
        */
        'enabled' => [
            'Cache',
            'Database',
            'Queue',
            'CloudStorage',
            'Https'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Health Monitor Sort Key
    |--------------------------------------------------------------------------
    |
    | This value determines how the resources cards in your panel is sorted. By
    | default, we sort by slug, but you may use other supported values below
    |
    | Options: 'abbreviation', 'slug', 'name'
    */
    'sort_by' => 'slug',

    /*
    |--------------------------------------------------------------------------
    | Health Monitor Caching
    |--------------------------------------------------------------------------
    |
    | Below is the list of configurations for health monitor caching mechanism
    |
    */
    'cache' => [
        /*
        |--------------------------------------------------------------------------
        | Health Monitor Caching Key
        |--------------------------------------------------------------------------
        |
        | This value determines the key to use for caching the results of health
        | monitor. Please feel free to update this to suit your own convention
        |
        */
        'key' => 'health-resources',

        /*
        |--------------------------------------------------------------------------
        | Health Monitor Caching Duration
        |--------------------------------------------------------------------------
        |
        | This determines how long the results of each check should stay cached in
        | your application. When your application is in "debug" mode caching is
        | automatically disabled, otherwise we default to caching every minute
        |
        | Options:
        |   0 = Cache Forever
        |   false = Disables caching
        |   30 = (integer) Minutes to cache
        */
        'minutes' => false,
    ],

    'database' => false,

    'services' => [
        'ping' => [
            'bin' => env('HEALTH_PING_BIN', '/sbin/ping'),
        ],

        'composer' => [
            'bin' => env('HEALTH_COMPOSER_BIN', 'composer'),
        ],
    ],

    'assets'                => false,
    'cache_files_base_path' => false,
    'notifications'         => [
        'enabled' => true,

        'notify_on' => [
            'panel' => true,
            'check' => true,
            'string' => true,
            'resource' => false,
        ],
        'channels' => ['custom']
    ],
    'alert'                 => false,
    'style'                 => false,
    'views'                 => false,
    'string'                => false,
    'routes'                => false,
    'urls'                  => false,
    'cloudwatch' => [
        'region'             => env('AWS_FAILOVER_REGION', 'us-west-2'),
        'namespace'          => env('AWS_METRIC_NAMESPACE', 'da-hulu-stage'),
        'metric_name'        => env('AWS_METRIC_NAME', 'Unhealthy count'),
        'storage_resolution' => env('AWS_METRIC_STORAGE_RESOLUTION', 1)
    ]
];
