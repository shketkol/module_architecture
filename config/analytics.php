<?php
return [
    'enabled'  => env('ANALYTICS_ENABLED', false),
    'services' => [
        'tealium' => [
            'url'      => env('ANALYTICS_TEALIUM_URL', 'https://tags.tiqcdn.com/utag/hulu/admanager/'),
            'id'       => env('ANALYTICS_TEALIUM_ID', 'qa'),
            'enabled'  => env('ANALYTICS_TEALIUM_ENABLED', false),
            'spa_mode' => env('ANALYTICS_TEALIUM_SPA_ENABLED', false)
        ],
    ],
];
