<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Date Format
    |--------------------------------------------------------------------------
    |
    | These configs include format for dates.
    |
    */

    'format'                     => [
        /*
        |--------------------------------------------------------------------------
        | PHP Format
        |--------------------------------------------------------------------------
        |
        | Used on backend. Docs: https://www.php.net/manual/en/datetime.formats.date.php
        |
        */
        'php'                       => 'm/d/Y',

        /*
        |--------------------------------------------------------------------------
        | JS Format for Date
        |--------------------------------------------------------------------------
        |
        | Used on frontend. Docs: https://momentjs.com/docs/#/displaying/
        |
        */
        'js'                        => 'MM/DD/YYYY',

        /*
        |--------------------------------------------------------------------------
        | JS Format for DateTime
        |--------------------------------------------------------------------------
        |
        | Used on frontend. Docs: https://momentjs.com/docs/#/displaying/
        |
        */
        'js_datetime_with_tz'       => 'MM/DD/YYYY h A [(ET)]',

        /*
        |--------------------------------------------------------------------------
        | JS Time Format
        |--------------------------------------------------------------------------
        |
        | Used on frontend. Docs: https://momentjs.com/docs/#/displaying/
        |
        */
        'js_time'       => 'h:mma',

        /*
        |--------------------------------------------------------------------------
        | moment.js parse formats array
        |--------------------------------------------------------------------------
        |
        | Used to check string date on frontend. Docs: https://momentjs.com/docs/#/displaying/
        |
        */
        'js_parse'                  => [
            'DD-MM-YYYY',
        ],

        /*
        |--------------------------------------------------------------------------
        | Datepicker format (Element library)
        |--------------------------------------------------------------------------
        |
        | NOTE: Should correspond to moment_datepicker_format
        | Used on Element.io elements. Docs: https://element.eleme.io/#/en-US/component/date-picker#date-formats
        |
        */
        'element_datepicker_format' => 'MM/dd/yy',

        /*
        |--------------------------------------------------------------------------
        | Datepicker format (Moment.js library)
        |--------------------------------------------------------------------------
        |
        | NOTE: Should correspond to element_datepicker_format
        | Used on Element.io elements. Docs: https://element.eleme.io/#/en-US/component/date-picker#date-formats
        |
        */
        'moment_datepicker_format'  => 'MM/DD/YY',

        /*
        |--------------------------------------------------------------------------
        | Date format for displaying date in datepickers input.
        |--------------------------------------------------------------------------
        |
        | Format for showing date in datepicker. Docs: https://momentjs.com/docs/#/displaying
        |
        */
        'datepicker_display_format' => 'ddd, MM/DD/YY',

        /*
        |--------------------------------------------------------------------------
        | JS Format
        |--------------------------------------------------------------------------
        |
        | Used on frontend. Docs: https://momentjs.com/docs/#/displaying/
        |
        */
        'orderDate'                 => 'MMM D, gggg',

        /*
        |--------------------------------------------------------------------------
        | HAAPI Java Format
        |--------------------------------------------------------------------------
        |
        | Used on HAAPI.
        |
        */
        'haapi'                     => \DateTime::ISO8601,

        /*
        |--------------------------------------------------------------------------
        | HAAPI Java Format + milliseconds
        |--------------------------------------------------------------------------
        |
        | Used on HAAPI.
        |
        */
        'haapi_milliseconds'        => 'Y-m-d\TH:i:s.uO',

        /*
        |--------------------------------------------------------------------------
        | Daapi Format
        |--------------------------------------------------------------------------
        |
        | Used on Daapi.
        |
        */
        'daapi'                     => 'Y-m-d',

        /*
        |--------------------------------------------------------------------------
        | API format
        |--------------------------------------------------------------------------
        |
        | Format used for API response.
        |
        */
        'api'                       => 'm-d-Y',

        /*
        |--------------------------------------------------------------------------
        | Datetime format
        |--------------------------------------------------------------------------
        |
        | Used to generate reports.
        | e.g. Aug 14, 2019 10:07 AM
        |
        */
        'date_time'                 => 'M j, Y, g:i A',

        /*
        |--------------------------------------------------------------------------
        | Datetime format
        |--------------------------------------------------------------------------
        |
        | Used to generate reports to show 24h-format time.
        | e.g. 11/8/20 16:31
        |
        */
        'date_time_24h'             => 'm/d/Y H:i:s',

        /*
        |--------------------------------------------------------------------------
        | Datetime format
        |--------------------------------------------------------------------------
        |
        | Used in database.
        | e.g. 2020-05-24 12:44:59
        |
        */
        'db_date_time'              => 'Y-m-d H:i:s',

        /*
        |--------------------------------------------------------------------------
        | Full datetime format
        |--------------------------------------------------------------------------
        |
        | Used in js.
        | e.g. 2020-05-24 12:44:59
        |
        */
        'js_full_date_time'         => 'YYYY-MM-DD HH:mm:ss',
    ],

    /*
    |--------------------------------------------------------------------------
    | Default Timezone code.
    |--------------------------------------------------------------------------
    |
    | Used on frontend.
    |
    */
    'default_timezone_code'      => 'ET',

    /*
   |--------------------------------------------------------------------------
   | Default Timezone full code.
   |--------------------------------------------------------------------------
   |
   | Used on frontend.
   | Used as default campaigns timezone.
   | Used in cron to run jobs on 'America/New_York' Midnight.
   |
   */
    'default_timezone_full_code' => 'America/New_York',

    /*
    | Hard-coded time used in front-end since for now application does not allow to choose broadcast time
    | The date could only be selected if it had not yet passed at 12:00 in the New York
    */
    'schedule_broadcast'         => [
        'hour'   => 12,
        'minute' => 0,
        'sec'    => 0,
    ],
];
