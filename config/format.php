<?php

/**
 * Config for various formatting parameters number, string, date etc
 */
return [
    /**
     * String formatting
     */
    'string' => [
        'maxLength' => 50
    ]
];
